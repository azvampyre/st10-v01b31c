package org.videolan.libvlc;

import android.media.AudioTrack;
import android.util.Log;

public class Aout
{
  private static final String TAG = "LibVLC/aout";
  private AudioTrack mAudioTrack;
  
  public void init(int paramInt1, int paramInt2, int paramInt3)
  {
    Log.d("LibVLC/aout", paramInt1 + ", " + paramInt2 + ", " + paramInt3 + "=>" + paramInt2 * paramInt3);
    this.mAudioTrack = new AudioTrack(3, paramInt1, 12, 2, Math.max(AudioTrack.getMinBufferSize(paramInt1, 12, 2), paramInt2 * paramInt3 * 2), 1);
  }
  
  public void pause()
  {
    this.mAudioTrack.pause();
  }
  
  public void playBuffer(byte[] paramArrayOfByte, int paramInt)
  {
    if (this.mAudioTrack.getState() == 0) {}
    for (;;)
    {
      return;
      if (this.mAudioTrack.write(paramArrayOfByte, 0, paramInt) != paramInt) {
        Log.w("LibVLC/aout", "Could not write all the samples to the audio device");
      }
      this.mAudioTrack.play();
    }
  }
  
  public void release()
  {
    if (this.mAudioTrack != null) {
      this.mAudioTrack.release();
    }
    this.mAudioTrack = null;
  }
}


