package org.videolan.libvlc;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.Surface;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class LibVLC
{
  public static final int AOUT_AUDIOTRACK = 1;
  public static final int AOUT_AUDIOTRACK_JAVA = 0;
  public static final int AOUT_OPENSLES = 2;
  private static final String TAG = "VLC/LibVLC";
  private static LibVLC sInstance;
  private int aout;
  private String chroma;
  private boolean iomx = false;
  private Aout mAout;
  private StringBuffer mDebugLogBuffer;
  private long mInternalMediaPlayerInstance = 0L;
  private boolean mIsBufferingLog = false;
  private boolean mIsInitialized;
  private long mLibVlcInstance = 0L;
  private long mMediaListInstance = 0L;
  private long mMediaListPlayerInstance = 0L;
  private String subtitlesEncoding = "";
  private boolean timeStretching;
  private boolean verboseMode;
  
  static
  {
    try
    {
      if (Build.VERSION.SDK_INT <= 10) {
        System.loadLibrary("iomx-gingerbread");
      }
    }
    catch (Throwable localThrowable)
    {
      try
      {
        for (;;)
        {
          System.loadLibrary("vlcjni");
          return;
          if (Build.VERSION.SDK_INT <= 13)
          {
            System.loadLibrary("iomx-hc");
            continue;
            localThrowable = localThrowable;
            Log.w("VLC/LibVLC", "Unable to load the iomx library: " + localThrowable);
          }
          else
          {
            System.loadLibrary("iomx-ics");
          }
        }
      }
      catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
      {
        for (;;)
        {
          Log.e("VLC/LibVLC", "Can't load vlcjni library: " + localUnsatisfiedLinkError);
          System.exit(1);
        }
      }
      catch (SecurityException localSecurityException)
      {
        for (;;)
        {
          Log.e("VLC/LibVLC", "Encountered a security issue when loading vlcjni library: " + localSecurityException);
          System.exit(1);
        }
      }
    }
  }
  
  private LibVLC()
  {
    if (LibVlcUtil.isGingerbreadOrLater()) {}
    for (int i = 2;; i = 0)
    {
      this.aout = i;
      this.timeStretching = false;
      this.chroma = "";
      this.verboseMode = true;
      this.mIsInitialized = false;
      this.mAout = new Aout();
      return;
    }
  }
  
  public static String PathToURI(String paramString)
  {
    if (paramString == null) {
      throw new NullPointerException("Cannot convert null path!");
    }
    return nativeToURI(paramString);
  }
  
  private native void detachEventHandler();
  
  public static LibVLC getExistingInstance()
  {
    try
    {
      LibVLC localLibVLC = sInstance;
      return localLibVLC;
    }
    finally {}
  }
  
  public static LibVLC getInstance()
    throws LibVlcException
  {
    try
    {
      if (sInstance == null)
      {
        LibVLC localLibVLC = new org/videolan/libvlc/LibVLC;
        localLibVLC.<init>();
        sInstance = localLibVLC;
      }
      return sInstance;
    }
    finally {}
  }
  
  private native long getLengthFromLocation(long paramLong, String paramString);
  
  private native byte[] getThumbnail(long paramLong, String paramString, int paramInt1, int paramInt2);
  
  private native boolean hasVideoTrack(long paramLong, String paramString);
  
  private native void nativeDestroy();
  
  private native void nativeInit()
    throws LibVlcException;
  
  public static native boolean nativeIsPathDirectory(String paramString);
  
  public static native void nativeReadDirectory(String paramString, ArrayList<String> paramArrayList);
  
  public static native String nativeToURI(String paramString);
  
  private native void playIndex(long paramLong, int paramInt);
  
  private native int readMedia(long paramLong, String paramString, boolean paramBoolean);
  
  private native String[] readMediaMeta(long paramLong, String paramString);
  
  private native TrackInfo[] readTracksInfo(long paramLong, String paramString);
  
  