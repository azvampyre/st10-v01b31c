package org.videolan.libvlc;

public class LibVlcException
  extends Exception
{
  private static final long serialVersionUID = -1909522348226924189L;
  
  public LibVlcException() {}
  
  public LibVlcException(String paramString)
  {
    super(paramString);
  }
  
  public LibVlcException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
  
  public LibVlcException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


