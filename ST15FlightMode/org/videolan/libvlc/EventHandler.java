package org.videolan.libvlc;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;

public class EventHandler
{
  public static final int MediaListItemAdded = 512;
  public static final int MediaListItemDeleted = 514;
  public static final int MediaPlayerEncounteredError = 266;
  public static final int MediaPlayerEndReached = 265;
  public static final int MediaPlayerPaused = 261;
  public static final int MediaPlayerPlaying = 260;
  public static final int MediaPlayerPositionChanged = 268;
  public static final int MediaPlayerRecordableChanged = 275;
  public static final int MediaPlayerRecordingFinished = 276;
  public static final int MediaPlayerSnapshotTaken = 272;
  public static final int MediaPlayerStopped = 262;
  public static final int MediaPlayerVout = 274;
  private static EventHandler mInstance;
  private ArrayList<Handler> mEventHandler = new ArrayList();
  
  public static EventHandler getInstance()
  {
    if (mInstance == null) {
      mInstance = new EventHandler();
    }
    return mInstance;
  }
  
  public void addHandler(Handler paramHandler)
  {
    if (!this.mEventHandler.contains(paramHandler)) {
      this.mEventHandler.add(paramHandler);
    }
  }
  
  public void callback(int paramInt, Bundle paramBundle)
  {
    paramBundle.putInt("event", paramInt);
    for (paramInt = 0;; paramInt++)
    {
      if (paramInt >= this.mEventHandler.size()) {
        return;
      }
      Message localMessage = Message.obtain();
      localMessage.setData(paramBundle);
      ((Handler)this.mEventHandler.get(paramInt)).sendMessage(localMessage);
    }
  }
  
  public void removeHandler(Handler paramHandler)
  {
    this.mEventHandler.remove(paramHandler);
  }
}


