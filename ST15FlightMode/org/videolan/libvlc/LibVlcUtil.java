package org.videolan.libvlc;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class LibVlcUtil
{
  private static String[] CPU_archs = { "*Pre-v4", "*v4", "*v4T", "v5T", "v5TE", "v5TEJ", "v6", "v6KZ", "v6T2", "v6K", "v7", "*v6-M", "*v6S-M", "*v7E-M", "*v8" };
  private static final int ELF_HEADER_SIZE = 52;
  private static final int EM_386 = 3;
  private static final int EM_ARM = 40;
  private static final int EM_MIPS = 8;
  private static final int SECTION_HEADER_SIZE = 40;
  private static final int SHT_ARM_ATTRIBUTES = 1879048195;
  public static final String TAG = "LibVlc/Util";
  private static String errorMsg = null;
  private static boolean isCompatible = false;
  
  public static String getErrorMsg()
  {
    return errorMsg;
  }
  
  private static String getString(ByteBuffer paramByteBuffer)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramByteBuffer.limit());
    for (;;)
    {
      if (paramByteBuffer.remaining() <= 0) {}
      char c;
      do
      {
        return localStringBuilder.toString();
        c = (char)paramByteBuffer.get();
      } while (c == 0);
      localStringBuilder.append(c);
    }
  }
  
  private static int getUleb128(ByteBuffer paramByteBuffer)
  {
    int i = 0;
    int k;
    int j;
    do
    {
      k = paramByteBuffer.get();
      j = i << 7 | k & 0x7F;
      i = j;
    } while ((k & 0x80) > 0);
    return j;
  }
  
  public static boolean hasCompatibleCPU(Context paramContext)
  {
    boolean bool;
    if ((errorMsg != null) || (isCompatible)) {
      bool = isCompatible;
    }
    for (;;)
    {
      return bool;
      ElfData localElfData = readLib(paramContext.getApplicationInfo().dataDir + "/lib/libvlcjni.so");
      if (localElfData == null)
      {
        Log.e("LibVlc/Util", "WARNING: Unable to read libvlcjni.so; cannot check device ABI!");
        Log.e("LibVlc/Util", "WARNING: Cannot guarantee correct ABI for this build (may crash)!");
        bool = true;
        continue;
      }
      String str = Build.CPU_ABI;
      paramContext = "none";
      Object localObject = paramContext;
      if (Build.VERSION.SDK_INT >= 8) {}
      try
      {
        localObject = (String)Build.class.getDeclaredField("CPU_ABI2").get(null);
        StringBuilder localStringBuilder = new StringBuilder("machine = ");
        label136:
        int m;
        int i1;
        int i;
        int i3;
        int i2;
        int n;
        int k;
        int j;
        if (localElfData.e_machine == 40)
        {
          paramContext = "arm";
          Log.i("LibVlc/Util", paramContext);
          Log.i("LibVlc/Util", "arch = " + localElfData.att_arch);
          Log.i("LibVlc/Util", "fpu = " + localElfData.att_fpu);
          m = 0;
          i1 = 0;
          i = 0;
          i3 = 0;
          i2 = 0;
          n = 0;
          if (!str.equals("x86")) {
            break label330;
          }
          k = 1;
          j = i3;
        }
        label235:
        int i4;
        try
        {
          paramContext = new java/io/FileReader;
          paramContext.<init>("/proc/cpuinfo");
          localObject = new java/io/BufferedReader;
          ((BufferedReader)localObject).<init>(paramContext);
          n = k;
          i4 = j;
          j = i;
          for (;;)
          {
            str = ((BufferedReader)localObject).readLine();
            if (str == null)
            {
              paramContext.close();
              if ((localElfData.e_machine != 3) || (n != 0)) {
                break label610;
              }
              errorMsg = "x86 build on non-x86 device";
              isCompatible = false;
              bool = false;
              break;
              if (localElfData.e_machine == 3)
              {
                paramContext = "x86";
                break label136;
              }
              paramContext = "mips";
              break label136;
              label330:
              if ((str.equals("armeabi-v7a")) || (((String)localObject).equals("armeabi-v7a")))
              {
                j = 1;
                i = 1;
                k = n;
                break label235;
              }
              if (!str.equals("armeabi"))
              {
                j = i3;
                k = n;
                if (!((String)localObject).equals("armeabi")) {
                  break label235;
                }
              }
              i = 1;
              j = i3;
              k = n;
              break label235;
            }
            k = j;
            i3 = i4;
            if (i4 == 0)
            {
              k = j;
              i3 = i4;
              if (str.contains("ARMv7"))
              {
                i3 = 1;
                k = 1;
              }
            }
            i = k;
            if (i3 == 0)
            {
              i = k;
              if (k == 0)
              {
                i = k;
                if (str.contains("ARMv6")) {
                  i = 1;
                }
              }
            }
            k = n;
            if (str.contains("clflush size")) {
              k = 1;
            }
            int i5 = i2;
            if (str.contains("microsecond timers")) {
              i5 = 1;
            }
            int i6 = m;
            if (m == 0)
            {
              i6 = m;
              if (str.contains("neon")) {
                i6 = 1;
              }
            }
            j = i;
            i4 = i3;
            i2 = i5;
            m = i6;
            n = k;
            if (i1 == 0)
            {
              bool = str.contains("vfp");
              j = i;
              i4 = i3;
              i2 = i5;
              m = i6;
              n = k;
              if (bool)
              {
                i1 = 1;
                j = i;
                i4 = i3;
                i2 = i5;
                m = i6;
                n = k;
              }
            }
          }
        }
        catch (IOException paramContext)
        {
          paramContext.printStackTrace();
          errorMsg = "IOException whilst reading cpuinfo flags";
          isCompatible = false;
          bool = false;
        }
        label610:
        if ((localElfData.e_machine == 40) && (n != 0))
        {
          errorMsg = "ARM build on x86 device";
          isCompatible = false;
          bool = false;
          continue;
        }
        if ((localElfData.e_machine == 8) && (i2 == 0))
        {
          errorMsg = "MIPS build on non-MIPS device";
          isCompatible = false;
          bool = false;
          continue;
        }
        if ((localElfData.e_machine == 40) && (i2 != 0))
        {
          errorMsg = "ARM build on MIPS device";
          isCompatible = false;
          bool = false;
          continue;
        }
        if ((localElfData.e_machine == 40) && (localElfData.att_arch.startsWith("v7")) && (i4 == 0))
        {
          errorMsg = "ARMv7 build on non-ARMv7 device";
          isCompatible = false;
          bool = false;
          continue;
        }
        if (localElfData.e_machine == 40)
        {
          if ((localElfData.att_arch.startsWith("v6")) && (j == 0))
          {
            errorMsg = "ARMv6 build on non-ARMv6 device";
            isCompatible = false;
            bool = false;
            continue;
          }
          if ((localElfData.att_fpu) && (i1 == 0))
          {
            errorMsg = "FPU-enabled build on non-FPU device";
            isCompatible = false;
            bool = false;
            continue;
          }
        }
        errorMsg = null;
        isCompatible = true;
        bool = true;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Context localContext = paramContext;
        }
      }
    }
  }
  
  public static boolean isFroyoOrLater()
  {
    if (Build.VERSION.SDK_INT >= 8) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isGingerbreadOrLater()
  {
    if (Build.VERSION.SDK_INT >= 9) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isHoneycombOrLater()
  {
    if (Build.VERSION.SDK_INT >= 11) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isICSOrLater()
  {
    if (Build.VERSION.SDK_INT >= 14) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isJellyBeanOrLater()
  {
    if (Build.VERSION.SDK_INT >= 16) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private static boolean readArmAttributes(RandomAccessFile paramRandomAccessFile, ElfData paramElfData)
    throws IOException
  {
    boolean bool2 = true;
    byte[] arrayOfByte = new byte[paramElfData.sh_size];
    paramRandomAccessFile.seek(paramElfData.sh_offset);
    paramRandomAccessFile.readFully(arrayOfByte);
    paramRandomAccessFile = ByteBuffer.wrap(arrayOfByte);
    paramRandomAccessFile.order(paramElfData.order);
    boolean bool1;
    if (paramRandomAccessFile.get() != 65) {
      bool1 = false;
    }
    for (;;)
    {
      return bool1;
      label159:
      label249:
      do
      {
        int i = paramRandomAccessFile.position();
        int j = paramRandomAccessFile.getInt();
        if (getString(paramRandomAccessFile).equals("aeabi"))
        {
          int m;
          int n;
          int k;
          for (;;)
          {
            bool1 = bool2;
            if (paramRandomAccessFile.position() >= i + j) {
              break;
            }
            m = paramRandomAccessFile.position();
            n = paramRandomAccessFile.get();
            k = paramRandomAccessFile.getInt();
            if (n == 1) {
              break label159;
            }
            paramRandomAccessFile.position(m + k);
          }
          for (;;)
          {
            n = getUleb128(paramRandomAccessFile);
            if (n == 6)
            {
              n = getUleb128(paramRandomAccessFile);
              paramElfData.att_arch = CPU_archs[n];
            }
            for (;;)
            {
              if (paramRandomAccessFile.position() < m + k) {
                break label249;
              }
              break;
              if (n == 27)
              {
                getUleb128(paramRandomAccessFile);
                paramElfData.att_fpu = true;
              }
              else
              {
                n %= 128;
                if ((n == 4) || (n == 5) || (n == 32) || ((n > 32) && ((n & 0x1) != 0))) {
                  getString(paramRandomAccessFile);
                } else {
                  getUleb128(paramRandomAccessFile);
                }
              }
            }
          }
        }
      } while (paramRandomAccessFile.remaining() > 0);
      bool1 = bool2;
    }
  }
  
  private static boolean readHeader(RandomAccessFile paramRandomAccessFile, ElfData paramElfData)
    throws IOException
  {
    boolean bool2 = false;
    byte[] arrayOfByte = new byte[52];
    paramRandomAccessFile.readFully(arrayOfByte);
    boolean bool1 = bool2;
    if (arrayOfByte[0] == Byte.MAX_VALUE)
    {
      bool1 = bool2;
      if (arrayOfByte[1] == 69)
      {
        bool1 = bool2;
        if (arrayOfByte[2] == 76)
        {
          bool1 = bool2;
          if (arrayOfByte[3] == 70)
          {
            if (arrayOfByte[4] == 1) {
              break label70;
            }
            bool1 = bool2;
          }
        }
      }
    }
    return bool1;
    label70:
    if (arrayOfByte[5] == 1) {}
    for (paramRandomAccessFile = ByteOrder.LITTLE_ENDIAN;; paramRandomAccessFile = ByteOrder.BIG_ENDIAN)
    {
      paramElfData.order = paramRandomAccessFile;
      paramRandomAccessFile = ByteBuffer.wrap(arrayOfByte);
      paramRandomAccessFile.order(paramElfData.order);
      paramElfData.e_machine = paramRandomAccessFile.getShort(18);
      paramElfData.e_shoff = paramRandomAccessFile.getInt(32);
      paramElfData.e_shnum = paramRandomAccessFile.getShort(48);
      bool1 = true;
      break;
    }
  }
  
  