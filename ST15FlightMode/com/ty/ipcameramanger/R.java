package com.ty.ipcameramanger;

public final class R
{
  public static final class drawable
  {
    public static final int ic_launcher = 2130837647;
  }
  
  public static final class string
  {
    public static final int action_settings = 2131296257;
    public static final int app_name = 2131296256;
    public static final int hello_world = 2131296258;
  }
  
  public static final class style
  {
    public static final int AppBaseTheme = 2131230721;
    public static final int AppTheme = 2131230722;
  }
}