package com.appunite.ffmpeg;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class FFmpegSurfaceView
  extends SurfaceView
  implements FFmpegDisplay, SurfaceHolder.Callback
{
  private boolean mCreated = false;
  private FFmpegPlayer mMpegPlayer = null;
  
  public FFmpegSurfaceView(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public FFmpegSurfaceView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public FFmpegSurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = getHolder();
    paramContext.setFormat(1);
    paramContext.addCallback(this);
  }
  
  public void setMpegPlayer(FFmpegPlayer paramFFmpegPlayer)
  {
    if (this.mMpegPlayer != null) {
      throw new RuntimeException("setMpegPlayer could not be called twice");
    }
    this.mMpegPlayer = paramFFmpegPlayer;
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    if (this.mCreated) {
      surfaceDestroyed(paramSurfaceHolder);
    }
    paramSurfaceHolder = paramSurfaceHolder.getSurface();
    this.mMpegPlayer.render(paramSurfaceHolder);
    this.mCreated = true;
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    this.mMpegPlayer.renderFrameStop();
    this.mCreated = false;
  }
  
  public static enum ScaleType
  {
    CENTER_CROP,  CENTER_INSIDE,  FIT_XY;
  }
}


