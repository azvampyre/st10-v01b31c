package com.appunite.ffmpeg;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class JniReader
{
  private static final String TAG = JniReader.class.getCanonicalName();
  private int position;
  private byte[] value = new byte[16];
  
  public JniReader(String paramString, int paramInt)
  {
    Log.d(TAG, String.format("Reading: %s", new Object[] { paramString }));
    try
    {
      byte[] arrayOfByte = "dupadupadupadupa".getBytes("UTF-8");
      paramString = MessageDigest.getInstance("MD5");
      paramString.update(arrayOfByte);
      System.arraycopy(paramString.digest(), 0, this.value, 0, 16);
      this.position = 0;
      return;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new RuntimeException(paramString);
    }
    catch (NoSuchAlgorithmException paramString)
    {
      throw new RuntimeException(paramString);
    }
  }
  
  public int check(int paramInt)
  {
    return 0;
  }
  
  public int read(byte[] paramArrayOfByte)
  {
    int j = this.position + paramArrayOfByte.length;
    int i = j;
    if (j >= this.value.length) {
      i = this.value.length;
    }
    i -= this.position;
    System.arraycopy(this.value, this.position, paramArrayOfByte, 0, i);
    this.position += i;
    return i;
  }
  
  public long seek(long paramLong, int paramInt)
  {
    return -1L;
  }
  
  public int write(byte[] paramArrayOfByte)
  {
    return 0;
  }
}


