package com.appunite.ffmpeg;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class SeekerView
  extends View
{
  private int mBarColor;
  private int mBarMinHeight;
  private int mBarMinWidth;
  private Paint mBarPaint = new Paint();
  private Rect mBarRect = new Rect();
  private int mBorderColor;
  private int mBorderPadding;
  private Paint mBorderPaint = new Paint();
  private Rect mBorderRect = new Rect();
  private int mBorderWidth;
  private int mCurrentValue = 10;
  private int mMaxValue = 100;
  private OnProgressChangeListener mOnProgressChangeListener = null;
  
  public SeekerView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SeekerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SeekerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.SeekerView, paramInt, 0);
    float f = getResources().getDisplayMetrics().density;
    this.mBorderWidth = paramContext.getDimensionPixelSize(R.styleable.SeekerView_borderWidth, (int)(1.0F * f + 0.5F));
    this.mBorderColor = paramContext.getColor(R.styleable.SeekerView_barColor, -16711681);
    this.mBorderPadding = paramContext.getColor(R.styleable.SeekerView_borderPadding, (int)(1.0F * f + 0.5F));
    this.mBarMinHeight = paramContext.getDimensionPixelSize(R.styleable.SeekerView_barMinHeight, (int)(10.0F * f + 0.5F));
    this.mBarMinWidth = paramContext.getDimensionPixelSize(R.styleable.SeekerView_barMinWidth, (int)(50.0F * f + 0.5F));
    this.mBarColor = paramContext.getColor(R.styleable.SeekerView_barColor, -16776961);
    this.mBorderPaint.setDither(true);
    this.mBorderPaint.setColor(this.mBorderColor);
    this.mBorderPaint.setStyle(Paint.Style.STROKE);
    this.mBorderPaint.setStrokeJoin(Paint.Join.ROUND);
    this.mBorderPaint.setStrokeCap(Paint.Cap.ROUND);
    this.mBorderPaint.setStrokeWidth(this.mBorderWidth);
    this.mBarPaint.setDither(true);
    this.mBarPaint.setColor(this.mBarColor);
    this.mBarPaint.setStyle(Paint.Style.FILL);
    this.mBarPaint.setStrokeJoin(Paint.Join.ROUND);
    this.mBarPaint.setStrokeCap(Paint.Cap.ROUND);
    this.mMaxValue = paramContext.getInt(R.styleable.SeekerView_maxValue, this.mMaxValue);
    this.mCurrentValue = paramContext.getInt(R.styleable.SeekerView_currentValue, this.mCurrentValue);
  }
  
  private void calculateBarRect()
  {
    int k = getWidth();
    int j = getHeight();
    int i = this.mBorderWidth + this.mBorderPadding;
    float f = this.mCurrentValue / this.mMaxValue;
    k = (int)((k - i) * f);
    this.mBarRect.set(i, i, k, j - i);
  }
  
  public int currentValue()
  {
    return this.mCurrentValue;
  }
  
  public int maxValue()
  {
    return this.mMaxValue;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    paramCanvas.drawRect(this.mBorderRect, this.mBorderPaint);
    paramCanvas.drawRect(this.mBarRect, this.mBarPaint);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramBoolean)
    {
      this.mBorderRect.set(0, 0, paramInt3 - paramInt1, paramInt4 - paramInt2);
      calculateBarRect();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int k = this.mBorderWidth;
    int n = this.mBorderPadding;
    int i = this.mBarMinWidth;
    int i1 = this.mBorderWidth;
    int j = this.mBorderPadding;
    int m = this.mBarMinHeight;
    setMeasuredDimension(ViewCompat.resolveSizeAndState((k + n) * 2 + i, paramInt1, 0), ViewCompat.resolveSizeAndState((i1 + j) * 2 + m, paramInt2, 0));
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int j = paramMotionEvent.getActionMasked();
    boolean bool2 = super.onTouchEvent(paramMotionEvent);
    int i = 0;
    boolean bool1 = false;
    if (j == 0) {
      i = 1;
    }
    for (;;)
    {
      if (i != 0)
      {
        float f1 = paramMotionEvent.getX();
        j = this.mBorderWidth + this.mBorderPadding;
        i = getWidth() - j * 2;
        float f2 = f1 - j;
        f1 = f2;
        if (f2 < 0.0F) {
          f1 = 0.0F;
        }
        f2 = f1;
        if (f1 > i) {
          f2 = i;
        }
        f1 = f2 / i;
        this.mCurrentValue = ((int)(this.mMaxValue * f1));
        if (this.mOnProgressChangeListener != null) {
          this.mOnProgressChangeListener.onProgressChange(bool1, this.mCurrentValue, this.mMaxValue);
        }
        calculateBarRect();
        invalidate();
        bool2 = true;
      }
      return bool2;
      if (j == 2)
      {
        i = 1;
      }
      else if (j == 1)
      {
        i = 1;
        bool1 = true;
      }
    }
  }
  
  public void setCurrentValue(int paramInt)
  {
    this.mCurrentValue = paramInt;
    invalidate();
  }
  
  public void setMaxValue(int paramInt)
  {
    this.mMaxValue = paramInt;
    invalidate();
  }
  
  public void setOnProgressChangeListener(OnProgressChangeListener paramOnProgressChangeListener)
  {
    this.mOnProgressChangeListener = paramOnProgressChangeListener;
  }
  
  public static abstract interface OnProgressChangeListener
  {
    public abstract void onProgressChange(boolean paramBoolean, int paramInt1, int paramInt2);
  }
}


