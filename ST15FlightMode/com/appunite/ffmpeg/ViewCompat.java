package com.appunite.ffmpeg;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.MeasureSpec;

public class ViewCompat
{
  public static final int MEASURED_SIZE_MASK = 16777215;
  public static final int MEASURED_STATE_MASK = -16777216;
  public static final int MEASURED_STATE_TOO_SMALL = 16777216;
  
  public static int resolveSize(int paramInt1, int paramInt2)
  {
    if (Build.VERSION.SDK_INT >= 11) {}
    for (paramInt1 = View.resolveSize(paramInt1, paramInt2);; paramInt1 = resolveSizeAndState(paramInt1, paramInt2, 0) & 0xFFFFFF) {
      return paramInt1;
    }
  }
  
  @TargetApi(11)
  public static int resolveSizeAndState(int paramInt1, int paramInt2, int paramInt3)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      paramInt1 = View.resolveSizeAndState(paramInt1, paramInt2, paramInt3);
      return paramInt1;
    }
    int i = paramInt1;
    int j = View.MeasureSpec.getMode(paramInt2);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    switch (j)
    {
    default: 
      paramInt1 = i;
    }
    for (;;)
    {
      paramInt1 = 0xFF000000 & paramInt3 | paramInt1;
      break;
      continue;
      if (paramInt2 < paramInt1)
      {
        paramInt1 = paramInt2 | 0x1000000;
      }
      else
      {
        continue;
        paramInt1 = paramInt2;
      }
    }
  }
}


