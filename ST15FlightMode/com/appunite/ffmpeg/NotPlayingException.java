package com.appunite.ffmpeg;

public class NotPlayingException
  extends Exception
{
  private static final long serialVersionUID = 1L;
  
  public NotPlayingException() {}
  
  public NotPlayingException(String paramString)
  {
    super(paramString);
  }
  
  public NotPlayingException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


