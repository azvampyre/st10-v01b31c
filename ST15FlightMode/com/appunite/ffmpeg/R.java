package com.appunite.ffmpeg;

public final class R
{
  public static final class attr
  {
    public static final int barColor = 2130771971;
    public static final int barMinHeight = 2130771972;
    public static final int barMinWidth = 2130771973;
    public static final int borderColor = 2130771968;
    public static final int borderPadding = 2130771970;
    public static final int borderWidth = 2130771969;
    public static final int currentValue = 2130771975;
    public static final int maxValue = 2130771974;
  }
  
  public static final class id
  {
    public static final int is_playing = 2131689983;
    public static final int is_recording = 2131689985;
    public static final int play = 2131689981;
    public static final int record = 2131689984;
    public static final int stop = 2131689982;
    public static final int video_view = 2131689980;
  }
  
  public static final class layout
  {
    public static final int test = 2130903110;
  }
  
  public static final class style
  {
    public static final int SeekerStyle = 2131230720;
  }
  
  public static final class styleable
  {
    public static final int[] SeekerView = { 2130771968, 2130771969, 2130771970, 2130771971, 2130771972, 2130771973, 2130771974, 2130771975 };
    public static final int SeekerView_barColor = 3;
    public static final int SeekerView_barMinHeight = 4;
    public static final int SeekerView_barMinWidth = 5;
    public static final int SeekerView_borderColor = 0;
    public static final int SeekerView_borderPadding = 2;
    public static final int SeekerView_borderWidth = 1;
    public static final int SeekerView_currentValue = 7;
    public static final int SeekerView_maxValue = 6;
  }
}


