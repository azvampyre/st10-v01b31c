package com.appunite.ffmpeg;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.view.Surface;
import java.util.HashMap;
import java.util.Map;

public class FFmpegPlayer
{
  public static final int NO_STREAM = -2;
  public static final int UNKNOWN_STREAM = -1;
  private final Activity activity;
  private long mCurrentTimeUs;
  private boolean mIsFinished = false;
  private int mNativePlayer;
  private final RenderedFrame mRenderedFrame = new RenderedFrame();
  private FFmpegStreamInfo[] mStreamsInfos = null;
  private long mVideoDurationUs;
  private FFmpegListener mpegListener = null;
  private String previousUrl;
  private SetDataSourceTask setDataSourceTask;
  private Runnable updateTimeRunnable = new Runnable()
  {
    public void run()
    {
      if (FFmpegPlayer.this.mpegListener != null) {
        FFmpegPlayer.this.mpegListener.onFFUpdateTime(FFmpegPlayer.this.mCurrentTimeUs, FFmpegPlayer.this.mVideoDurationUs, FFmpegPlayer.this.mIsFinished);
      }
      if (FFmpegPlayer.this.mIsFinished) {
        FFmpegPlayer.this.stop();
      }
    }
  };
  
  static
  {
    if (new NativeTester().isNeon())
    {
      System.loadLibrary("ffmpeg-neon");
      System.loadLibrary("ffmpeg-jni-neon");
    }
    for (;;)
    {
      return;
      System.loadLibrary("ffmpeg");
      System.loadLibrary("ffmpeg-jni");
    }
  }
  
  public FFmpegPlayer(FFmpegDisplay paramFFmpegDisplay, Activity paramActivity)
  {
    this.activity = paramActivity;
    int i = initNative();
    if (i != 0) {
      throw new RuntimeException(String.format("Could not initialize player: %d", new Object[] { Integer.valueOf(i) }));
    }
    if (paramFFmpegDisplay != null) {
      paramFFmpegDisplay.setMpegPlayer(this);
    }
  }
  
  private native void deallocNative();
  
  private native long getVideoDurationNative();
  
  private native int initNative();
  
  private native int isPlayingNative();
  
  private native int isRecordingNative();
  
  private void onUpdateTime(long paramLong1, long paramLong2, boolean paramBoolean)
  {
    this.mCurrentTimeUs = paramLong1;
    this.mVideoDurationUs = paramLong2;
    this.mIsFinished = paramBoolean;
    this.activity.runOnUiThread(this.updateTimeRunnable);
  }
  
  private native void pauseNative()
    throws NotPlayingException;
  
  private AudioTrack prepareAudioTrack(int paramInt1, int paramInt2)
  {
    int i = paramInt2;
    for (;;)
    {
      if (i == 1) {
        paramInt2 = 4;
      }
      try
      {
        for (;;)
        {
          AudioTrack localAudioTrack = new AudioTrack(3, paramInt1, paramInt2, 2, AudioTrack.getMinBufferSize(paramInt1, paramInt2, 2), 1);
          return localAudioTrack;
          if (i == 2) {
            paramInt2 = 12;
          } else if (i == 3) {
            paramInt2 = 28;
          } else if (i == 4) {
            paramInt2 = 204;
          } else if (i == 5) {
            paramInt2 = 236;
          } else if (i == 6) {
            paramInt2 = 252;
          } else if (i == 8) {
            paramInt2 = 1020;
          } else {
            paramInt2 = 12;
          }
        }
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        if (i > 2)
        {
          i = 2;
          continue;
        }
        if (i > 1)
        {
          i = 1;
          continue;
        }
        throw localIllegalArgumentException;
      }
    }
  }
  
  private Bitmap prepareFrame(int paramInt1, int paramInt2)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
    this.mRenderedFrame.height = paramInt2;
    this.mRenderedFrame.width = paramInt1;
    return localBitmap;
  }
  
  private native void resumeNative()
    throws NotPlayingException;
  
  private native void seekNative(long paramLong)
    throws NotPlayingException;
  
  private native int setDataSourceNative(String paramString, Map<String, String> paramMap, int paramInt1, int paramInt2, int paramInt3);
  
  private void setStreamsInfo(FFmpegStreamInfo[] paramArrayOfFFmpegStreamInfo)
  {
    this.mStreamsInfos = paramArrayOfFFmpegStreamInfo;
  }
  
  private void setVideoListener(FFmpegListener paramFFmpegListener)
  {
    setMpegListener(paramFFmpegListener);
  }
  
  private native int startRecordNative(String paramString);
  
  private native void stopNative();
  
  private native int stopRecordNative();
  
  protected void finalize()
    throws Throwable
  {
    deallocNative();
    super.finalize();
  }
  
  public FFmpegListener getMpegListener()
  {
    return this.mpegListener;
  }
  
  protected FFmpegStreamInfo[] getStreamsInfo()
  {
    return this.mStreamsInfos;
  }
  
  public boolean isPlaying()
  {
    boolean bool = true;
    if (isPlayingNative() == 1) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  public boolean isRecording()
  {
    boolean bool = true;
    if (isRecordingNative() == 1) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  public void pause()
  {
    new PauseTask(this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Void[0]);
  }
  
  public native void render(Surface paramSurface);
  
  public native void renderFrameStart();
  
  public native void renderFrameStop();
  
  public void resume()
  {
    new ResumeTask(this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Void[0]);
  }
  
  public void seek(long paramLong)
  {
    new SeekTask(this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Long[] { Long.valueOf(paramLong) });
  }
  
  public void setDataSource(String paramString)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("stimeout", "3000000");
    setDataSource(paramString, localHashMap, -1, -1, -2);
  }
  
  public void setDataSource(String paramString, Map<String, String> paramMap, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.setDataSourceTask != null) {
      if (paramString.equals(this.previousUrl)) {
        if (this.setDataSourceTask.getStatus() != AsyncTask.Status.RUNNING) {
          break label41;
        }
      }
    }
    for (;;)
    {
      return;
      this.setDataSourceTask.cancel(true);
      label41:
      this.previousUrl = paramString;
      this.setDataSourceTask = new SetDataSourceTask(this);
      this.setDataSourceTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Object[] { paramString, paramMap, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3) });
    }
  }
  
  public void setMpegListener(FFmpegListener paramFFmpegListener)
  {
    this.mpegListener = paramFFmpegListener;
  }
  
  public int startRecord(String paramString)
  {
    return startRecordNative(paramString);
  }
  
  public void stop()
  {
    new StopTask(this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Void[0]);
  }
  
  public int stopRecord()
  {
    return stopRecordNative();
  }
  
  private static class PauseTask
    extends AsyncTask<Void, Void, NotPlayingException>
  {
    private final FFmpegPlayer player;
    
    public PauseTask(FFmpegPlayer paramFFmpegPlayer)
    {
      this.player = paramFFmpegPlayer;
    }
    
    protected NotPlayingException doInBackground(Void... paramVarArgs)
    {
      try
      {
        this.player.pauseNative();
        paramVarArgs = null;
      }
      catch (NotPlayingException paramVarArgs)
      {
        for (;;) {}
      }
      return paramVarArgs;
    }
    
    protected void onPostExecute(NotPlayingException paramNotPlayingException)
    {
      if (this.player.mpegListener != null) {
        this.player.mpegListener.onFFPause(paramNotPlayingException);
      }
    }
  }
  
  static class RenderedFrame
  {
    public Bitmap bitmap;
    public int height;
    public int width;
  }
  
  private static class ResumeTask
    extends AsyncTask<Void, Void, NotPlayingException>
  {
    private final FFmpegPlayer player;
    
    public ResumeTask(FFmpegPlayer paramFFmpegPlayer)
    {
      this.player = paramFFmpegPlayer;
    }
    
    protected NotPlayingException doInBackground(Void... paramVarArgs)
    {
      try
      {
        this.player.resumeNative();
        paramVarArgs = null;
      }
      catch (NotPlayingException paramVarArgs)
      {
        for (;;) {}
      }
      return paramVarArgs;
    }
    
    protected void onPostExecute(NotPlayingException paramNotPlayingException)
    {
      if (this.player.mpegListener != null) {
        this.player.mpegListener.onFFResume(paramNotPlayingException);
      }
    }
  }
  
  private static class SeekTask
    extends AsyncTask<Long, Void, NotPlayingException>
  {
    private final FFmpegPlayer player;
    
    public SeekTask(FFmpegPlayer paramFFmpegPlayer)
    {
      this.player = paramFFmpegPlayer;
    }
    
    protected NotPlayingException doInBackground(Long... paramVarArgs)
    {
      try
      {
        this.player.seekNative(paramVarArgs[0].longValue());
        paramVarArgs = null;
      }
      catch (NotPlayingException paramVarArgs)
      {
        for (;;) {}
      }
      return paramVarArgs;
    }
    
    protected void onPostExecute(NotPlayingException paramNotPlayingException)
    {
      if (this.player.mpegListener != null) {
        this.player.mpegListener.onFFSeeked(paramNotPlayingException);
      }
    }
  }
  
  private static class SetDataSourceTask
    extends AsyncTask<Object, Void, FFmpegPlayer.SetDataSourceTaskResult>
  {
    private final FFmpegPlayer player;
    
    public SetDataSourceTask(FFmpegPlayer paramFFmpegPlayer)
    {
      this.player = paramFFmpegPlayer;
    }
    
    protected FFmpegPlayer.SetDataSourceTaskResult doInBackground(Object... paramVarArgs)
    {
      int k = -1;
      String str = (String)paramVarArgs[0];
      Map localMap = (Map)paramVarArgs[1];
      Integer localInteger2 = (Integer)paramVarArgs[2];
      Integer localInteger1 = (Integer)paramVarArgs[3];
      paramVarArgs = (Integer)paramVarArgs[4];
      int i;
      int j;
      if (localInteger2 == null)
      {
        i = -1;
        if (localInteger1 != null) {
          break label117;
        }
        j = -1;
        label56:
        if (paramVarArgs != null) {
          break label126;
        }
        label60:
        i = this.player.setDataSourceNative(str, localMap, i, j, k);
        paramVarArgs = new FFmpegPlayer.SetDataSourceTaskResult(null);
        if (i >= 0) {
          break label135;
        }
        paramVarArgs.error = new FFmpegError(i);
      }
      for (paramVarArgs.streams = null;; paramVarArgs.streams = this.player.getStreamsInfo())
      {
        return paramVarArgs;
        i = localInteger2.intValue();
        break;
        label117:
        j = localInteger1.intValue();
        break label56;
        label126:
        k = paramVarArgs.intValue();
        break label60;
        label135:
        paramVarArgs.error = null;
      }
    }
    
    protected void onPostExecute(FFmpegPlayer.SetDataSourceTaskResult paramSetDataSourceTaskResult)
    {
      if (this.player.mpegListener != null) {
        this.player.mpegListener.onFFDataSourceLoaded(paramSetDataSourceTaskResult.error, paramSetDataSourceTaskResult.streams);
      }
    }
  }
  
  private static class SetDataSourceTaskResult
  {
    FFmpegError error;
    FFmpegStreamInfo[] streams;
  }
  
  private static class StopTask
    extends AsyncTask<Void, Void, Void>
  {
    private final FFmpegPlayer player;
    
    public StopTask(FFmpegPlayer paramFFmpegPlayer)
    {
      this.player = paramFFmpegPlayer;
    }
    
    protected Void doInBackground(Void... paramVarArgs)
    {
      this.player.stopNative();
      return null;
    }
    
    protected void onPostExecute(Void paramVoid)
    {
      if (this.player.mpegListener != null) {
        this.player.mpegListener.onFFStop();
      }
    }
  }
}


