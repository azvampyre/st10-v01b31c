package com.appunite.ffmpeg;

public class FpsCounter
{
  private int counter = 0;
  private final int frameCount;
  boolean start = true;
  private long startTime = 0L;
  private String tick = "- fps";
  
  public FpsCounter(int paramInt)
  {
    this.frameCount = paramInt;
  }
  
  public String tick()
  {
    if (this.start)
    {
      this.start = false;
      this.startTime = System.nanoTime();
    }
    int i = this.counter;
    this.counter = (i + 1);
    if (i < this.frameCount) {}
    for (String str = this.tick;; str = this.tick)
    {
      return str;
      long l = System.nanoTime();
      double d = this.frameCount * 1.0E9D / (l - this.startTime);
      this.startTime = l;
      this.counter = 0;
      this.tick = String.format("%.2f fps", new Object[] { Double.valueOf(d) });
    }
  }
}


