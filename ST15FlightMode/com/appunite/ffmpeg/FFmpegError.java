package com.appunite.ffmpeg;

public class FFmpegError
  extends Throwable
{
  private static final long serialVersionUID = 1L;
  
  public FFmpegError(int paramInt)
  {
    super(String.format("FFmpegPlayer error %d", new Object[] { Integer.valueOf(paramInt) }));
  }
}


