package com.appunite.ffmpeg;

public abstract interface FFmpegListener
{
  public abstract void onFFDataSourceLoaded(FFmpegError paramFFmpegError, FFmpegStreamInfo[] paramArrayOfFFmpegStreamInfo);
  
  public abstract void onFFPause(NotPlayingException paramNotPlayingException);
  
  public abstract void onFFResume(NotPlayingException paramNotPlayingException);
  
  public abstract void onFFSeeked(NotPlayingException paramNotPlayingException);
  
  public abstract void onFFStop();
  
  public abstract void onFFUpdateTime(long paramLong1, long paramLong2, boolean paramBoolean);
}


