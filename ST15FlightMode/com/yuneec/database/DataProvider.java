package com.yuneec.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;
import java.util.List;

public class DataProvider
  extends ContentProvider
{
  public static final int ALL_MODELS = 1;
  public static final String AUTHORITY = "com.yuneec.databaseprovider";
  public static final int CHANNEL_MAP = 3;
  public static final Uri CHMAP_URI;
  public static final int DR = 7;
  public static final int DR_CURVE = 8;
  public static final Uri DR_CURVE_URI;
  public static final Uri DR_URI;
  public static final String EXTRA_PARAMS = "params";
  public static final Uri FMODE_URI;
  public static final int F_MODE = 4;
  public static final String MIME_ITEM = "vnd.yuneec.model";
  public static final String MIME_TYPE_ALL = "vnd.android.cursor.dir/vnd.yuneec.model";
  public static final String MIME_TYPE_SINGLE = "vnd.android.cursor.item/vnd.yuneec.model";
  public static final Uri MODEL_URI = Uri.parse("content://com.yuneec.databaseprovider/model");
  public static final int PARAMS = 10;
  public static final String PARAMS_CHANGE_MODE = "change_mode";
  public static final String PARAMS_FUNCTION = "function";
  public static final String PARAMS_F_MODE = "fmode";
  public static final String PARAMS_F_MODE_1 = "f_mode1";
  public static final String PARAMS_F_MODE_2 = "f_mode2";
  public static final String PARAMS_F_MODE_3 = "f_mode3";
  public static final String PARAMS_HARDWARE = "hardware";
  public static final String PARAMS_MASTER_CHANNEL = "master_channel";
  public static final String PARAMS_MASTER_ID = "master_id";
  public static final String PARAMS_MIXING_BRIEF = "mixing_brief";
  public static final String PARAMS_MODE = "mode";
  public static final String PARAMS_MODEL = "model";
  public static final String PARAMS_PARENT_ID = "parent_id";
  public static final String PARAMS_RESET_MIXING = "reset_mixing";
  public static final String PARAMS_SWITCHES_MAP = "switches_map";
  public static final String PARAMS_SW_STATE = "sw_state";
  public static final Uri PARAMS_URI = Uri.parse("content://com.yuneec.databaseprovider/params/");
  public static final String PATH_ALL_MODEL = "model";
  public static final String PATH_CHMAP = "channel_map/#";
  public static final String PATH_DR = "dr_data/#";
  public static final String PATH_DR_CURVE = "dr_curve/#";
  public static final String PATH_FMODE = "f_mode/#";
  public static final String PATH_PARAMS = "params/";
  public static final String PATH_SERVO = "servo/#";
  public static final String PATH_SINGLE_MODEL = "model/#";
  public static final String PATH_THR = "thr_data/#";
  public static final String PATH_THR_CURVE = "thr_curve/#";
  public static final int SERVO = 9;
  public static final Uri SERVO_URI;
  public static final int SINGLE_MODEL = 2;
  private static final String TAG = "DataProvider";
  public static final int THR = 5;
  public static final int THR_CURVE = 6;
  public static final Uri THR_CURVE_URI;
  public static final Uri THR_URI;
  private static final UriMatcher uriMatcher;
  private DBOpenHelper mOpenHelper;
  
  static
  {
    CHMAP_URI = Uri.parse("content://com.yuneec.databaseprovider/channel_map/#");
    FMODE_URI = Uri.parse("content://com.yuneec.databaseprovider/f_mode/#");
    THR_URI = Uri.parse("content://com.yuneec.databaseprovider/thr_data/#");
    THR_CURVE_URI = Uri.parse("content://com.yuneec.databaseprovider/thr_curve/#");
    DR_URI = Uri.parse("content://com.yuneec.databaseprovider/dr_data/#");
    DR_CURVE_URI = Uri.parse("content://com.yuneec.databaseprovider/dr_curve/#");
    SERVO_URI = Uri.parse("content://com.yuneec.databaseprovider/servo/#");
    uriMatcher = new UriMatcher(-1);
    uriMatcher.addURI("com.yuneec.databaseprovider", "model", 1);
    uriMatcher.addURI("com.yuneec.databaseprovider", "model/#", 2);
    uriMatcher.addURI("com.yuneec.databaseprovider", "params/", 10);
    uriMatcher.addURI("com.yuneec.databaseprovider", "channel_map/#", 3);
    uriMatcher.addURI("com.yuneec.databaseprovider", "f_mode/#", 4);
    uriMatcher.addURI("com.yuneec.databaseprovider", "thr_data/#", 5);
    uriMatcher.addURI("com.yuneec.databaseprovider", "thr_curve/#", 6);
    uriMatcher.addURI("com.yuneec.databaseprovider", "dr_data/#", 7);
    uriMatcher.addURI("com.yuneec.databaseprovider", "dr_curve/#", 8);
    uriMatcher.addURI("com.yuneec.databaseprovider", "servo/#", 9);
  }
  
  private String checkChannelMapInput(Uri paramUri, ContentValues paramContentValues, boolean paramBoolean)
  {
    paramUri = paramUri.getQueryParameter("master_channel");
    if (paramUri == null) {
      paramUri = "you must assgin a channel to insert/modify";
    }
    for (;;)
    {
      return paramUri;
      String str1 = paramContentValues.getAsString("channel");
      String str2 = paramContentValues.getAsString("hardware");
      paramContentValues = paramContentValues.getAsString("function");
      if ((paramBoolean) && ((str1 == null) || (str2 == null) || (paramContentValues == null))) {
        paramUri = "insert action must contain channel, function and hardware";
      } else if ((str1 != null) && (!str1.equals(paramUri))) {
        paramUri = "channel in ContentValues is not match with the values in Uri";
      } else if ((str1 != null) && (!paramBoolean)) {
        paramUri = "channel can not be modified once inserted";
      } else {
        paramUri = "OK";
      }
    }
  }
  
  private boolean needInitChannelMap(ContentValues paramContentValues)
  {
    boolean bool = false;
    if (100 != 0) {
      bool = true;
    }
    return bool;
  }
  
  private boolean onUpdateFModeKey(SQLiteDatabase paramSQLiteDatabase, String paramString, long paramLong)
  {
    Object localObject = ContentUris.withAppendedId(MODEL_URI, paramLong);
    Cursor localCursor = getContext().getContentResolver().query((Uri)localObject, new String[] { "f_mode_key" }, null, null, null);
    if (DataProviderHelper.isCursorValid(localCursor))
    {
      localObject = localCursor.getString(localCursor.getColumnIndex("f_mode_key"));
      if ((paramString != null) && (!paramString.equals(localObject))) {
        this.mOpenHelper.onUpdateFmodeKey(paramLong, paramString, paramSQLiteDatabase);
      }
      localCursor.close();
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    String str;
    int i;
    switch (uriMatcher.match(paramUri))
    {
    case 4: 
    case 6: 
    case 8: 
    default: 
      throw new IllegalArgumentException("Disallowed uri:" + paramUri);
    case 2: 
      str = "models";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      i = this.mOpenHelper.getWritableDatabase().delete(str, paramString, paramArrayOfString);
      getContext().getContentResolver().notifyChange(paramUri, null);
    }
    for (;;)
    {
      return i;
      str = "channel_map";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "thr_data";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "dr_data";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "servo_data";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      i = -1;
    }
  }
  
  public String getType(Uri paramUri)
  {
    switch (uriMatcher.match(paramUri))
    {
    default: 
      throw new IllegalArgumentException("Unkown uri:" + paramUri);
    }
    for (paramUri = "vnd.android.cursor.dir/vnd.yuneec.model";; paramUri = "vnd.android.cursor.item/vnd.yuneec.model") {
      return paramUri;
    }
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getWritableDatabase();
    Uri localUri = Uri.parse("DEFAULT");
    String str;
    long l;
    switch (uriMatcher.match(paramUri))
    {
    case 2: 
    case 4: 
    case 6: 
    case 8: 
    default: 
      throw new IllegalArgumentException("Disallowed uri:" + paramUri);
    case 1: 
      localUri = MODEL_URI;
      str = "models";
      if (localUri.equals(MODEL_URI)) {
        localSQLiteDatabase.beginTransaction();
      }
      break;
    case 3: 
    case 5: 
    case 7: 
    case 9: 
      try
      {
        l = localSQLiteDatabase.insertOrThrow(str, null, paramContentValues);
        int i = getContext().getSharedPreferences("flight_setting_value", 0).getInt("mode_select_value", 2);
        if (needInitChannelMap(paramContentValues)) {
          this.mOpenHelper.initChannelMap(l, localSQLiteDatabase, i);
        }
        localSQLiteDatabase.setTransactionSuccessful();
      }
      catch (Throwable paramContentValues)
      {
        for (;;)
        {
          label189:
          Log.e("DataProvider", paramContentValues.getMessage());
          l = -1L;
          localSQLiteDatabase.endTransaction();
        }
      }
      finally
      {
        localSQLiteDatabase.endTransaction();
      }
      if (l > 0L)
      {
        paramUri = ContentUris.withAppendedId(localUri, l);
        getContext().getContentResolver().notifyChange(paramUri, null);
      }
      break;
    }
    for (;;)
    {
      return paramUri;
      localUri = CHMAP_URI;
      str = "channel_map";
      break;
      str = "thr_data";
      paramContentValues.put("_pid", (String)paramUri.getPathSegments().get(1));
      break;
      str = "dr_data";
      paramContentValues.put("_pid", (String)paramUri.getPathSegments().get(1));
      break;
      str = "servo_data";
      paramContentValues.put("_pid", (String)paramUri.getPathSegments().get(1));
      break;
      l = localSQLiteDatabase.insert(str, null, paramContentValues);
      break label189;
      Log.e("DataProvider", "Fail to insert a row into :" + paramUri);
      paramUri = null;
    }
  }
  
  public boolean onCreate()
  {
    this.mOpenHelper = new DBOpenHelper(getContext());
    return true;
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    Object localObject2 = null;
    SQLiteQueryBuilder localSQLiteQueryBuilder = new SQLiteQueryBuilder();
    Object localObject1 = localObject2;
    switch (uriMatcher.match(paramUri))
    {
    default: 
      Log.e("DataProvider", "Unkown uri:" + paramUri);
      localObject1 = localObject2;
    case 10: 
      return (Cursor)localObject1;
    case 2: 
      localSQLiteQueryBuilder.appendWhere("_id=" + (String)paramUri.getPathSegments().get(1));
    case 1: 
      localSQLiteQueryBuilder.setTables("models");
    }
    for (;;)
    {
      localObject1 = localSQLiteQueryBuilder.query(this.mOpenHelper.getReadableDatabase(), paramArrayOfString1, paramString1, paramArrayOfString2, null, null, paramString2);
      ((Cursor)localObject1).setNotificationUri(getContext().getContentResolver(), paramUri);
      break;
      localSQLiteQueryBuilder.setTables("channel_map");
      if ("true".equals(paramUri.getQueryParameter("parent_id")))
      {
        localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1));
      }
      else
      {
        localSQLiteQueryBuilder.appendWhere("_id=" + (String)paramUri.getPathSegments().get(1));
        continue;
        localSQLiteQueryBuilder.setTables("f_mode");
        if ("true".equals(paramUri.getQueryParameter("parent_id")))
        {
          localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1));
        }
        else
        {
          localSQLiteQueryBuilder.appendWhere("_id=" + (String)paramUri.getPathSegments().get(1));
          continue;
          localSQLiteQueryBuilder.setTables("thr_data");
          if ("true".equals(paramUri.getQueryParameter("parent_id")))
          {
            localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1));
          }
          else
          {
            localSQLiteQueryBuilder.appendWhere("_id=" + (String)paramUri.getPathSegments().get(1));
            continue;
            localSQLiteQueryBuilder.setTables("thr_curve");
            localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1));
            continue;
            localSQLiteQueryBuilder.setTables("dr_data");
            localObject1 = paramUri.getQueryParameter("function");
            if ("true".equals(paramUri.getQueryParameter("parent_id")))
            {
              localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1) + " and " + "function" + "='" + (String)localObject1 + "'");
            }
            else
            {
              localSQLiteQueryBuilder.appendWhere("_id=" + (String)paramUri.getPathSegments().get(1));
              continue;
              localSQLiteQueryBuilder.setTables("dr_curve");
              localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1));
              continue;
              localSQLiteQueryBuilder.setTables("servo_data");
              localObject1 = paramUri.getQueryParameter("function");
              if ("true".equals(paramUri.getQueryParameter("parent_id"))) {
                localSQLiteQueryBuilder.appendWhere("_pid=" + (String)paramUri.getPathSegments().get(1) + " and " + "function" + "='" + (String)localObject1 + "'");
              } else {
                localSQLiteQueryBuilder.appendWhere("_id=" + (String)paramUri.getPathSegments().get(1));
              }
            }
          }
        }
      }
    }
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getWritableDatabase();
    Object localObject = Uri.parse("DEFAULT");
    String str;
    switch (uriMatcher.match(paramUri))
    {
    case 4: 
    default: 
      throw new IllegalArgumentException("Disallowed uri:" + paramUri);
    case 1: 
      str = "models";
      localObject = MODEL_URI;
      if (((Uri)localObject).equals(MODEL_URI)) {
        if ((paramContentValues.containsKey("f_mode_key")) && (!onUpdateFModeKey(localSQLiteDatabase, paramContentValues.getAsString("f_mode_key"), Long.parseLong((String)paramUri.getPathSegments().get(1)))))
        {
          Log.e("DataProvider", "onUpdateFModeKey failed, remove field 'f_mode_key'");
          paramContentValues.remove("f_mode_key");
        }
      }
      break;
    }
    for (int i = localSQLiteDatabase.update(str, paramContentValues, paramString, paramArrayOfString);; i = localSQLiteDatabase.update(str, paramContentValues, paramString, paramArrayOfString))
    {
      getContext().getContentResolver().notifyChange(paramUri, null);
      for (;;)
      {
        return i;
        paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
        str = "models";
        localObject = MODEL_URI;
        break;
        str = "channel_map";
        paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
        if (!"true".equals(paramUri.getQueryParameter("change_mode"))) {
          break;
        }
        DBOpenHelper.updateChannelMap(localSQLiteDatabase, Integer.parseInt(paramUri.getQueryParameter("mode")));
        i = 0;
      }
      str = "thr_data";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "thr_curve";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "dr_data";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "dr_curve";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = "servo_data";
      paramString = "_id=" + (String)paramUri.getPathSegments().get(1);
      break;
      str = paramUri.getQueryParameter("params");
      localObject = paramUri.getQueryParameter("mode");
      paramString = "_pid=" + (String)localObject;
      paramContentValues.put("_pid", Long.valueOf(Long.parseLong((String)localObject)));
      localObject = PARAMS_URI;
      break;
    }
  }
}


