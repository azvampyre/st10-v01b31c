package com.yuneec.database;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.SystemClock;
import android.util.Log;
import com.yuneec.channelsettings.DRData;
import com.yuneec.channelsettings.DRData.CurveParams;
import com.yuneec.channelsettings.ServoData;
import com.yuneec.channelsettings.ThrottleData;
import com.yuneec.channelsettings.ThrottleData.ThrCurve;
import com.yuneec.flight_settings.ChannelMap;
import java.util.ArrayList;

public class DataProviderHelper
{
  public static final int MODEL_AIRPLANE = 1;
  public static final int MODEL_GLIDER = 3;
  public static final int MODEL_HELICOPTER = 2;
  public static final int MODEL_MULTICOPTER = 4;
  public static final int MODEL_TYPE_AIRPLANE_BASE = 100;
  public static final int MODEL_TYPE_GLIDER_BASE = 300;
  public static final int MODEL_TYPE_HELICOPTER_BASE = 200;
  public static final int MODEL_TYPE_LAST = 500;
  public static final int MODEL_TYPE_MULITCOPTER_BASE = 400;
  private static final String TAG = "DataProviderHelper";
  
  public static void changeMode(Context paramContext, int paramInt)
  {
    Uri localUri = ContentUris.withAppendedId(DataProvider.CHMAP_URI, 0L).buildUpon().appendQueryParameter("change_mode", "true").appendQueryParameter("mode", String.valueOf(paramInt)).build();
    paramContext.getContentResolver().update(localUri, null, null, null);
  }
  
  public static String[] getAllChannelsAlias(Context paramContext, long paramLong)
  {
    Object localObject = ContentUris.withAppendedId(DataProvider.CHMAP_URI, paramLong);
    localObject = paramContext.getContentResolver().query((Uri)localObject, new String[] { "alias" }, null, null, null);
    paramContext = null;
    if (isCursorValid((Cursor)localObject))
    {
      int i = 0;
      paramContext = new String[12];
      do
      {
        paramContext[i] = ((Cursor)localObject).getString(((Cursor)localObject).getColumnIndex("alias"));
        i++;
      } while (((Cursor)localObject).moveToNext());
      ((Cursor)localObject).close();
    }
    return paramContext;
  }
  
  public static String getChannelAlias(Context paramContext, long paramLong, int paramInt)
  {
    Object localObject = ContentUris.withAppendedId(DataProvider.CHMAP_URI, paramLong);
    paramContext = paramContext.getContentResolver();
    String str = "channel=" + paramInt;
    localObject = paramContext.query((Uri)localObject, new String[] { "alias" }, str, null, null);
    paramContext = null;
    if (isCursorValid((Cursor)localObject))
    {
      paramContext = ((Cursor)localObject).getString(((Cursor)localObject).getColumnIndex("alias"));
      ((Cursor)localObject).close();
    }
    return paramContext;
  }
  
  public static long getFmodeID(Context paramContext, long paramLong, int paramInt)
  {
    paramContext = paramContext.getContentResolver().query(ContentUris.withAppendedId(DataProvider.FMODE_URI, paramLong).buildUpon().appendQueryParameter("parent_id", "true").appendQueryParameter("fmode", String.valueOf(paramInt)).build(), new String[] { "_id" }, null, null, null);
    if (!isCursorValid(paramContext))
    {
      Log.e("DataProviderHelper", "getFmodeID, Cursor is invalid");
      paramLong = -1L;
    }
    for (;;)
    {
      return paramLong;
      paramLong = paramContext.getLong(paramContext.getColumnIndex("_id"));
      paramContext.close();
    }
  }
  
  public static int getFmodeKeyFromDatabase(Context paramContext, long paramLong)
  {
    int i = -1;
    paramContext = paramContext.getContentResolver();
    if (paramLong != -2L)
    {
      paramContext = paramContext.query(ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong), new String[] { "_id", "f_mode_key" }, null, null, null);
      if (!isCursorValid(paramContext)) {
        break label74;
      }
      i = paramContext.getInt(paramContext.getColumnIndex("f_mode_key"));
      paramContext.close();
    }
    for (;;)
    {
      return i;
      label74:
      i = -1;
    }
  }
  
  public static String getFuncNameByHw(Context paramContext, long paramLong, String paramString)
  {
    Object localObject = null;
    paramContext = paramContext.getContentResolver();
    Uri localUri = ContentUris.withAppendedId(DataProvider.CHMAP_URI, paramLong);
    paramString = "hardware='" + paramString + "'";
    paramString = paramContext.query(localUri, new String[] { "function" }, paramString, null, null);
    if (!isCursorValid(paramString)) {
      paramContext = (Context)localObject;
    }
    for (;;)
    {
      return paramContext;
      paramContext = paramString.getString(paramString.getColumnIndex("function"));
      paramString.close();
    }
  }
  
  public static ArrayList<ModelBrief> getModelsByType(Context paramContext, int paramInt1, int paramInt2)
  {
    ModelBrief localModelBrief = null;
    Object localObject = paramContext.getContentResolver();
    switch (paramInt1)
    {
    default: 
      Log.e("DataProviderHelper", "Unknown Type");
      paramContext = localModelBrief;
    }
    for (;;)
    {
      return paramContext;
      int i = 100;
      paramInt1 = 200;
      for (;;)
      {
        Uri localUri = DataProvider.MODEL_URI;
        paramContext = "type>" + i + " AND " + "type" + "<" + paramInt1 + " AND " + "fpv" + "=" + paramInt2;
        localObject = ((ContentResolver)localObject).query(localUri, new String[] { "_id", "name", "fpv", "icon", "type", "f_mode_key", "analog_min", "switch_min" }, paramContext, null, null);
        if ((localObject != null) && (((Cursor)localObject).moveToFirst())) {
          break label248;
        }
        paramContext = localModelBrief;
        if (localObject == null) {
          break;
        }
        ((Cursor)localObject).close();
        paramContext = localModelBrief;
        break;
        i = 200;
        paramInt1 = 300;
        continue;
        i = 300;
        paramInt1 = 400;
        continue;
        i = 400;
        paramInt1 = 500;
      }
      label248:
      paramContext = new ArrayList();
      do
      {
        localModelBrief = new ModelBrief();
        localModelBrief._id = ((Cursor)localObject).getLong(((Cursor)localObject).getColumnIndex("_id"));
        localModelBrief.name = ((Cursor)localObject).getString(((Cursor)localObject).getColumnIndex("name"));
        localModelBrief.iconResourceId = ((Cursor)localObject).getInt(((Cursor)localObject).getColumnIndex("icon"));
        localModelBrief.type = ((Cursor)localObject).getInt(((Cursor)localObject).getColumnIndex("type"));
        localModelBrief.fpv = ((Cursor)localObject).getInt(((Cursor)localObject).getColumnIndex("fpv"));
        localModelBrief.f_mode_key = ((Cursor)localObject).getInt(((Cursor)localObject).getColumnIndex("f_mode_key"));
        localModelBrief.analog_min = ((Cursor)localObject).getInt(((Cursor)localObject).getColumnIndex("analog_min"));
        localModelBrief.switch_min = ((Cursor)localObject).getInt(((Cursor)localObject).getColumnIndex("switch_min"));
        paramContext.add(localModelBrief);
      } while (((Cursor)localObject).moveToNext());
      ((Cursor)localObject).close();
    }
  }
  
  private static void getThrCurvePoints(ContentResolver paramContentResolver, ThrottleData paramThrottleData)
  {
    paramContentResolver = paramContentResolver.query(ContentUris.withAppendedId(DataProvider.THR_CURVE_URI, paramThrottleData.id).buildUpon().appendQueryParameter("parent_id", "true").build(), new String[] { "_id", "sw_state", "pot0", "pot1", "pot2", "pot3", "pot4" }, null, null, null);
    if (!isCursorValid(paramContentResolver))
    {
      Log.e("DataProviderHelper", "getThrCurvePoints(), Cursor is invalid");
      return;
    }
    int i = 0;
    for (;;)
    {
      if ((paramContentResolver.isAfterLast()) || (i >= paramContentResolver.getCount()))
      {
        paramContentResolver.close();
        break;
      }
      int j = paramContentResolver.getInt(paramContentResolver.getColumnIndex("sw_state"));
      i = j;
      if (j >= 0)
      {
        i = j;
        if (j < 3)
        {
          paramThrottleData.thrCurve[j].id = paramContentResolver.getLong(paramContentResolver.getColumnIndex("_id"));
          paramThrottleData.thrCurve[j].sw_state = j;
          paramThrottleData.thrCurve[j].curvePoints[0] = paramContentResolver.getFloat(paramContentResolver.getColumnIndex("pot0"));
          paramThrottleData.thrCurve[j].curvePoints[1] = paramContentResolver.getFloat(paramContentResolver.getColumnIndex("pot1"));
          paramThrottleData.thrCurve[j].curvePoints[2] = paramContentResolver.getFloat(paramContentResolver.getColumnIndex("pot2"));
          paramThrottleData.thrCurve[j].curvePoints[3] = paramContentResolver.getFloat(paramContentResolver.getColumnIndex("pot3"));
          paramThrottleData.thrCurve[j].curvePoints[4] = paramContentResolver.getFloat(paramContentResolver.getColumnIndex("pot4"));
          paramContentResolver.moveToNext();
          i = j;
        }
      }
    }
  }
  
  public static boolean isCursorValid(Cursor paramCursor)
  {
    if ((paramCursor == null) || (!paramCursor.moveToFirst())) {
      if (paramCursor != null) {
        paramCursor.close();
      }
    }
    for (boolean bool = false;; bool = true) {
      return bool;
    }
  }
  
  public static ChannelMap[] readChannelMapFromDatabase(Context paramContext, long paramLong)
  {
    Object localObject = null;
    ChannelMap[] arrayOfChannelMap = new ChannelMap[15];
    paramContext = paramContext.getContentResolver().query(ContentUris.withAppendedId(DataProvider.CHMAP_URI, paramLong).buildUpon().appendQueryParameter("parent_id", "true").build(), new String[] { "_id", "channel", "function", "hardware", "alias" }, null, null, null);
    if (!isCursorValid(paramContext)) {
      Log.e("DataProviderHelper", "readChannelMapFromDatabase----Cursor is invalid");
    }
    for (paramContext = (Context)localObject;; paramContext = (Context)localObject)
    {
      return paramContext;
      if (paramContext.getCount() == 12) {
        break;
      }
      Log.e("DataProviderHelper", "readChannelMapFromDatabase----Get unordered data");
    }
    int i = 0;
    for (;;)
    {
      if (paramContext.isAfterLast())
      {
        paramContext.close();
        paramContext = arrayOfChannelMap;
        break;
      }
      arrayOfChannelMap[i] = new ChannelMap();
      arrayOfChannelMap[i].id = paramContext.getLong(paramContext.getColumnIndex("_id"));
      arrayOfChannelMap[i].channel = paramContext.getInt(paramContext.getColumnIndex("channel"));
      arrayOfChannelMap[i].function = paramContext.getString(paramContext.getColumnIndex("function"));
      arrayOfChannelMap[i].hardware = paramContext.getString(paramContext.getColumnIndex("hardware"));
      arrayOfChannelMap[i].alias = paramContext.getString(paramContext.getColumnIndex("alias"));
      i++;
      paramContext.moveToNext();
    }
  }
  
  public static DRData[] readDRDataFromDatabase(Context paramContext, long paramLong, int paramInt)
  {
    paramLong = getFmodeID(paramContext, paramLong, paramInt);
    return new DRData[] { readSingleInputDR(paramContext, paramLong, "ail"), readSingleInputDR(paramContext, paramLong, "ele"), readSingleInputDR(paramContext, paramLong, "rud") };
  }
  
  public static ServoData[] readServoDataFromDatabase(Context paramContext, long paramLong, int paramInt)
  {
    String[] arrayOfString = paramContext.getResources().getStringArray(2131361797);
    ServoData[] arrayOfServoData = new ServoData[arrayOfString.length];
    paramLong = getFmodeID(paramContext, paramLong, paramInt);
    for (paramInt = 0;; paramInt++)
    {
      if (paramInt >= arrayOfServoData.length) {
        return arrayOfServoData;
      }
      arrayOfServoData[paramInt] = readSingleServo(paramContext, paramLong, arrayOfString[paramInt]);
    }
  }
  
  private static DRData readSingleInputDR(Context paramContext, long paramLong, String paramString)
  {
    DRData localDRData = new DRData();
    paramContext = paramContext.getContentResolver();
    paramString = paramContext.query(ContentUris.withAppendedId(DataProvider.DR_URI, paramLong).buildUpon().appendQueryParameter("parent_id", "true").appendQueryParameter("function", paramString).build(), new String[] { "_id", "function", "switch" }, null, null, null);
    if (!isCursorValid(paramString)) {
      Log.e("DataProviderHelper", "ReadThrDataFromDatabase, Cursor is invalid");
    }
    for (paramContext = null;; paramContext = null)
    {
      return paramContext;
      localDRData.id = paramString.getLong(paramString.getColumnIndex("_id"));
      localDRData.func = paramString.getString(paramString.getColumnIndex("function"));
      localDRData.sw = paramString.getString(paramString.getColumnIndex("switch"));
      paramString.close();
      paramContext = paramContext.query(ContentUris.withAppendedId(DataProvider.DR_CURVE_URI, localDRData.id).buildUpon().appendQueryParameter("parent_id", "true").build(), new String[] { "_id", "sw_state", "rate1", "rate2", "expo1", "expo2", "offset" }, null, null, null);
      if (isCursorValid(paramContext)) {
        break;
      }
      Log.e("DataProviderHelper", "ReadThrCurveFromDatabase, Cursor is invalid");
    }
    int i = 0;
    for (;;)
    {
      if ((paramContext.isAfterLast()) || (i >= paramContext.getCount()))
      {
        paramContext.close();
        paramContext = localDRData;
        break;
      }
      localDRData.curveparams[i].id = paramContext.getLong(paramContext.getColumnIndex("_id"));
      localDRData.curveparams[i].sw_state = paramContext.getInt(paramContext.getColumnIndex("sw_state"));
      localDRData.curveparams[i].rate1 = paramContext.getFloat(paramContext.getColumnIndex("rate1"));
      localDRData.curveparams[i].rate2 = paramContext.getFloat(paramContext.getColumnIndex("rate2"));
      localDRData.curveparams[i].expo1 = paramContext.getFloat(paramContext.getColumnIndex("expo1"));
      localDRData.curveparams[i].expo2 = paramContext.getFloat(paramContext.getColumnIndex("expo2"));
      localDRData.curveparams[i].offset = paramContext.getFloat(paramContext.getColumnIndex("offset"));
      i++;
      paramContext.moveToNext();
    }
  }
  
  private static ServoData readSingleServo(Context paramContext, long paramLong, String paramString)
  {
    Object localObject = null;
    ServoData localServoData = new ServoData();
    paramContext = paramContext.getContentResolver().query(ContentUris.withAppendedId(DataProvider.SERVO_URI, paramLong).buildUpon().appendQueryParameter("parent_id", "true").appendQueryParameter("function", paramString).build(), new String[] { "_id", "function", "sub_trim", "reverse", "speed", "travel_l", "travel_r" }, null, null, null);
    if (!isCursorValid(paramContext))
    {
      Log.e("DataProviderHelper", "ReadThrDataFromDatabase, Cursor is invalid");
      paramContext = (Context)localObject;
      return paramContext;
    }
    localServoData.id = paramContext.getLong(paramContext.getColumnIndex("_id"));
    localServoData.func = paramContext.getString(paramContext.getColumnIndex("function"));
    localServoData.subTrim = paramContext.getInt(paramContext.getColumnIndex("sub_trim"));
    if (paramContext.getInt(paramContext.getColumnIndex("reverse")) != 0) {}
    for (boolean bool = true;; bool = false)
    {
      localServoData.reverse = bool;
      localServoData.speed = paramContext.getInt(paramContext.getColumnIndex("speed"));
      localServoData.travelL = paramContext.getInt(paramContext.getColumnIndex("travel_l"));
      localServoData.travelR = paramContext.getInt(paramContext.getColumnIndex("travel_r"));
      paramContext.close();
      paramContext = localServoData;
      break;
    }
  }
  
  public static ThrottleData readThrDataFromDatabase(Context paramContext, long paramLong, int paramInt)
  {
    ThrottleData localThrottleData = new ThrottleData();
    paramLong = getFmodeID(paramContext, paramLong, paramInt);
    paramContext = paramContext.getContentResolver();
    Cursor localCursor = paramContext.query(ContentUris.withAppendedId(DataProvider.THR_URI, paramLong).buildUpon().appendQueryParameter("parent_id", "true").build(), new String[] { "_id", "switch", "expo", "cut_switch", "cut_value_1", "cut_value_2" }, null, null, null);
    if (!isCursorValid(localCursor))
    {
      Log.e("DataProviderHelper", "ReadThrDataFromDatabase, Cursor is invalid");
      paramContext = null;
      return paramContext;
    }
    localThrottleData.id = localCursor.getLong(localCursor.getColumnIndex("_id"));
    localThrottleData.sw = localCursor.getString(localCursor.getColumnIndex("switch"));
    if (localCursor.getInt(localCursor.getColumnIndex("expo")) != 0) {}
    for (boolean bool = true;; bool = false)
    {
      localThrottleData.expo = bool;
      localThrottleData.cut_sw = localCursor.getString(localCursor.getColumnIndex("cut_switch"));
      localThrottleData.cut_value1 = localCursor.getInt(localCursor.getColumnIndex("cut_value_1"));
      localThrottleData.cut_value2 = localCursor.getInt(localCursor.getColumnIndex("cut_value_2"));
      localCursor.close();
      getThrCurvePoints(paramContext, localThrottleData);
      paramContext = localThrottleData;
      break;
    }
  }
  
  public static void resetMixingChannel(Context paramContext, long paramLong)
  {
    Uri localUri = DataProvider.PARAMS_URI.buildUpon().appendQueryParameter("params", "reset_mixing").appendQueryParameter("model", String.valueOf(paramLong)).build();
    paramLong = SystemClock.uptimeMillis();
    paramContext.getContentResolver().delete(localUri, null, null);
    Log.d("DataProviderHelper", "reset mixing channel use:" + (SystemClock.uptimeMillis() - paramLong));
  }
  
  public static void setChannelAlias(Context paramContext, long paramLong, int paramInt, String paramString)
  {
    Uri localUri = ContentUris.withAppendedId(DataProvider.CHMAP_URI, paramLong).buildUpon().appendQueryParameter("master_channel", String.valueOf(paramInt)).build();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("alias", paramString);
    paramContext.getContentResolver().update(localUri, localContentValues, null, null);
  }
  
  public static void writeChannelMapFromDatabase(Context paramContext, ChannelMap[] paramArrayOfChannelMap)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    paramContext = new ContentValues();
    if (paramArrayOfChannelMap.length != 12) {
      Log.e("DataProviderHelper", "writeChannelMapFromDatabase-----Get unordered data");
    }
    for (;;)
    {
      return;
      for (int i = 0; i < paramArrayOfChannelMap.length; i++)
      {
        if (paramContext.size() != 0) {
          paramContext.clear();
        }
        paramContext.put("channel", Integer.valueOf(paramArrayOfChannelMap[i].channel));
        paramContext.put("function", paramArrayOfChannelMap[i].function);
        paramContext.put("hardware", paramArrayOfChannelMap[i].hardware);
        paramContext.put("alias", paramArrayOfChannelMap[i].alias);
        localContentResolver.update(ContentUris.withAppendedId(DataProvider.CHMAP_URI, paramArrayOfChannelMap[i].id), paramContext, null, null);
      }
    }
  }
  
  public static void writeDRDataToDatabase(Context paramContext, DRData[] paramArrayOfDRData)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    paramContext = new ContentValues();
    int i = 0;
    if (i >= paramArrayOfDRData.length) {
      return;
    }
    paramContext.clear();
    paramContext.put("function", paramArrayOfDRData[i].func);
    paramContext.put("switch", paramArrayOfDRData[i].sw);
    localContentResolver.update(ContentUris.withAppendedId(DataProvider.DR_URI, paramArrayOfDRData[i].id), paramContext, null, null);
    for (int j = 0;; j++)
    {
      if (j >= 3)
      {
        i++;
        break;
      }
      paramContext.clear();
      paramContext.put("sw_state", Integer.valueOf(paramArrayOfDRData[i].curveparams[j].sw_state));
      paramContext.put("rate1", Float.valueOf(paramArrayOfDRData[i].curveparams[j].rate1));
      paramContext.put("rate2", Float.valueOf(paramArrayOfDRData[i].curveparams[j].rate2));
      paramContext.put("expo1", Float.valueOf(paramArrayOfDRData[i].curveparams[j].expo1));
      paramContext.put("expo2", Float.valueOf(paramArrayOfDRData[i].curveparams[j].expo2));
      paramContext.put("offset", Float.valueOf(paramArrayOfDRData[i].curveparams[j].offset));
      localContentResolver.update(ContentUris.withAppendedId(DataProvider.DR_CURVE_URI, paramArrayOfDRData[i].curveparams[j].id), paramContext, null, null);
    }
  }
  
  public static void writeServoDataToDatabase(Context paramContext, ServoData[] paramArrayOfServoData)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    ContentValues localContentValues = new ContentValues();
    paramContext = paramContext.getResources().getStringArray(2131361797);
    int i = 0;
    if (i >= paramContext.length) {
      return;
    }
    localContentValues.clear();
    localContentValues.put("function", paramArrayOfServoData[i].func);
    localContentValues.put("sub_trim", Integer.valueOf(paramArrayOfServoData[i].subTrim));
    if (paramArrayOfServoData[i].reverse) {}
    for (int j = 1;; j = 0)
    {
      localContentValues.put("reverse", Integer.valueOf(j));
      localContentValues.put("speed", Integer.valueOf(paramArrayOfServoData[i].speed));
      localContentValues.put("travel_l", Integer.valueOf(paramArrayOfServoData[i].travelL));
      localContentValues.put("travel_r", Integer.valueOf(paramArrayOfServoData[i].travelR));
      localContentResolver.update(ContentUris.withAppendedId(DataProvider.SERVO_URI, paramArrayOfServoData[i].id), localContentValues, null, null);
      i++;
      break;
    }
  }
  
  public static void writeThrDataToDatabase(Context paramContext, ThrottleData paramThrottleData)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    paramContext = new ContentValues();
    paramContext.put("switch", paramThrottleData.sw);
    paramContext.put("expo", Boolean.valueOf(paramThrottleData.expo));
    paramContext.put("cut_switch", paramThrottleData.cut_sw);
    paramContext.put("cut_value_1", Integer.valueOf(paramThrottleData.cut_value1));
    paramContext.put("cut_value_2", Integer.valueOf(paramThrottleData.cut_value2));
    localContentResolver.update(ContentUris.withAppendedId(DataProvider.THR_URI, paramThrottleData.id), paramContext, null, null);
    for (int i = 0;; i++)
    {
      if (i >= 3) {
        return;
      }
      paramContext.clear();
      paramContext.put("sw_state", Integer.valueOf(paramThrottleData.thrCurve[i].sw_state));
      paramContext.put("pot0", Float.valueOf(paramThrottleData.thrCurve[i].curvePoints[0]));
      paramContext.put("pot1", Float.valueOf(paramThrottleData.thrCurve[i].curvePoints[1]));
      paramContext.put("pot2", Float.valueOf(paramThrottleData.thrCurve[i].curvePoints[2]));
      paramContext.put("pot3", Float.valueOf(paramThrottleData.thrCurve[i].curvePoints[3]));
      paramContext.put("pot4", Float.valueOf(paramThrottleData.thrCurve[i].curvePoints[4]));
      localContentResolver.update(ContentUris.withAppendedId(DataProvider.THR_CURVE_URI, paramThrottleData.thrCurve[i].id), paramContext, null, null);
    }
  }
}


