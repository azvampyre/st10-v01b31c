package com.yuneec.uartcontroller15;

public final class R
{
  public static final class drawable
  {
    public static final int ic_launcher = 2130837647;
  }
  
  public static final class id
  {
    public static final int bind = 2131689474;
    public static final int bindAModel = 2131689476;
    public static final int button1 = 2131689489;
    public static final int button2 = 2131689490;
    public static final int button3 = 2131689491;
    public static final int button4 = 2131689492;
    public static final int buttonStartCali = 2131689488;
    public static final int finishBind = 2131689477;
    public static final int listView1 = 2131689479;
    public static final int read = 2131689475;
    public static final int seekBar1 = 2131689480;
    public static final int seekBar2 = 2131689481;
    public static final int seekBar3 = 2131689482;
    public static final int seekBar4 = 2131689483;
    public static final int seekBar5 = 2131689484;
    public static final int seekBar6 = 2131689485;
    public static final int seekBar7 = 2131689486;
    public static final int seekBar8 = 2131689487;
    public static final int testBit = 2131689478;
    public static final int tog_run = 2131689493;
  }
  
  public static final class layout
  {
    public static final int activity_main = 2130903041;
  }
  
  public static final class string
  {
    public static final int app_name = 2131296256;
  }
  
  public static final class style
  {
    public static final int AppBaseTheme = 2131230721;
    public static final int AppTheme = 2131230722;
  }
}


