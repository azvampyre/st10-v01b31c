package com.yuneec.test;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.CalibrationRawData;
import java.util.ArrayList;

public class ReadCalibrationData
  extends Activity
  implements View.OnClickListener
{
  private static final int C_GREEN = -14503604;
  private static final int C_RED = -3407872;
  private static final int J1_TRAVEL = 1920;
  private static final int J2_TRAVEL = 2500;
  private static final int J3_TRAVEL = 1920;
  private static final int J4_TRAVEL = 2500;
  private static final int K1_TRAVEL = 4095;
  private static final int K2_TRAVEL = 4095;
  private static final int K3_TRAVEL = 4000;
  private static final int K4_TRAVEL = 1200;
  private static final int K5_TRAVEL = 1200;
  private static final int ST10_K_TRAVEL = 1000;
  private static final String TAG = "setValue";
  private int b1_max_value = -1;
  private int b1_min_value = -1;
  private int b2_max_value = -1;
  private int b2_min_value = -1;
  private int b3_max_value = -1;
  private int b3_min_value = -1;
  Button btn_finish;
  Button btn_reset;
  private boolean isReset = false;
  private int j1_max_value = 0;
  private int j1_min_value = 4096;
  private int j2_max_value = 0;
  private int j2_min_value = 4096;
  private int j3_max_value = 0;
  private int j3_min_value = 4096;
  private int j4_max_value = 0;
  private int j4_min_value = 4096;
  private int k1_max_value = 0;
  private int k1_min_value = 4096;
  private int k2_max_value = 0;
  private int k2_min_value = 4096;
  private int k3_max_value = 0;
  private int k3_min_value = 4096;
  private int k4_max_value = 0;
  private int k4_min_value = 4096;
  private int k5_max_value = 0;
  private int k5_min_value = 4096;
  private UARTController mController;
  private Handler mUartHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
      {
        paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        if (ReadCalibrationData.this.isReset) {
          ReadCalibrationData.this.resetAll();
        }
        switch (paramAnonymousMessage.what)
        {
        }
      }
      for (;;)
      {
        return;
        paramAnonymousMessage = (UARTInfoMessage.CalibrationRawData)paramAnonymousMessage;
        if ("ST10".equals("ST10")) {
          ReadCalibrationData.this.setValue10(paramAnonymousMessage);
        } else if ("ST10".equals("ST12")) {
          ReadCalibrationData.this.setValue12(paramAnonymousMessage);
        } else if ("ST10".equals("ST15")) {
          ReadCalibrationData.this.setValue(paramAnonymousMessage);
        }
      }
    }
  };
  TextView rcd_b1_status;
  TextView rcd_b1_value;
  TextView rcd_b2_status;
  TextView rcd_b2_value;
  TextView rcd_b3_status;
  TextView rcd_b3_value;
  TextView rcd_j1_status;
  TextView rcd_j1_value;
  TextView rcd_j2_status;
  TextView rcd_j2_value;
  TextView rcd_j3_status;
  TextView rcd_j3_value;
  TextView rcd_j4_status;
  TextView rcd_j4_value;
  TextView rcd_k1_status;
  TextView rcd_k1_value;
  TextView rcd_k2_status;
  TextView rcd_k2_value;
  TextView rcd_k3_status;
  TextView rcd_k3_value;
  TextView rcd_k4_status;
  TextView rcd_k4_value;
  TextView rcd_k5_status;
  TextView rcd_k5_value;
  TextView rcd_s1_status;
  TextView rcd_s1_value;
  TextView rcd_s2_status;
  TextView rcd_s2_value;
  TextView rcd_s3_status;
  TextView rcd_s3_value;
  TextView rcd_s4_status;
  TextView rcd_s4_value;
  TextView rcd_t1_status;
  TextView rcd_t1_value;
  TextView rcd_t2_status;
  TextView rcd_t2_value;
  TextView rcd_t3_status;
  TextView rcd_t3_value;
  TextView rcd_t4_status;
  TextView rcd_t4_value;
  private int s1_max_value = -1;
  private int s1_mid_value = -1;
  private int s1_min_value = -1;
  private int s2_max_value = -1;
  private int s2_mid_value = -1;
  private int s2_min_value = -1;
  private int s3_max_value = -1;
  private int s3_mid_value = -1;
  private int s3_min_value = -1;
  private int s4_max_value = -1;
  private int s4_min_value = -1;
  
  private void setBStatus(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    }
    for (;;)
    {
      return;
      if ((this.b1_max_value != -1) && (this.b1_min_value != -1))
      {
        this.rcd_b1_status.setBackgroundColor(-14503604);
        this.rcd_b1_status.setText(2131296575);
      }
      if (paramInt2 == 0)
      {
        this.b1_min_value = paramInt2;
      }
      else if (paramInt2 == 1)
      {
        this.b1_max_value = paramInt2;
        continue;
        if ((this.b2_max_value != -1) && (this.b2_min_value != -1))
        {
          this.rcd_b2_status.setBackgroundColor(-14503604);
          this.rcd_b2_status.setText(2131296575);
        }
        if (paramInt2 == 0)
        {
          this.b2_min_value = paramInt2;
        }
        else if (paramInt2 == 1)
        {
          this.b2_max_value = paramInt2;
          continue;
          if ((this.b3_max_value != -1) && (this.b3_min_value != -1))
          {
            this.rcd_b3_status.setBackgroundColor(-14503604);
            this.rcd_b3_status.setText(2131296575);
          }
          if (paramInt2 == 0) {
            this.b3_min_value = paramInt2;
          } else if (paramInt2 == 1) {
            this.b3_max_value = paramInt2;
          }
        }
      }
    }
  }
  
  private void setJStatus(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    }
    for (;;)
    {
      return;
      if (paramInt2 > this.j1_max_value) {
        this.j1_max_value = paramInt2;
      }
      if (paramInt2 < this.j1_min_value) {
        this.j1_min_value = paramInt2;
      }
      if (this.j1_max_value - this.j1_min_value >= 1920)
      {
        this.rcd_j1_status.setBackgroundColor(-14503604);
        this.rcd_j1_status.setText(2131296575);
        continue;
        if (paramInt2 > this.j2_max_value) {
          this.j2_max_value = paramInt2;
        }
        if (paramInt2 < this.j2_min_value) {
          this.j2_min_value = paramInt2;
        }
        if (this.j2_max_value - this.j2_min_value >= 2500)
        {
          this.rcd_j2_status.setBackgroundColor(-14503604);
          this.rcd_j2_status.setText(2131296575);
          continue;
          if (paramInt2 > this.j3_max_value) {
            this.j3_max_value = paramInt2;
          }
          if (paramInt2 < this.j3_min_value) {
            this.j3_min_value = paramInt2;
          }
          if (this.j3_max_value - this.j3_min_value >= 1920)
          {
            this.rcd_j3_status.setBackgroundColor(-14503604);
            this.rcd_j3_status.setText(2131296575);
            continue;
            if (paramInt2 > this.j4_max_value) {
              this.j4_max_value = paramInt2;
            }
            if (paramInt2 < this.j4_min_value) {
              this.j4_min_value = paramInt2;
            }
            if (this.j4_max_value - this.j4_min_value >= 2500)
            {
              this.rcd_j4_status.setBackgroundColor(-14503604);
              this.rcd_j4_status.setText(2131296575);
            }
          }
        }
      }
    }
  }
  
  private void setKStatus(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    }
    for (;;)
    {
      return;
      if (paramInt2 > this.k1_max_value) {
        this.k1_max_value = paramInt2;
      }
      if (paramInt2 < this.k1_min_value) {
        this.k1_min_value = paramInt2;
      }
      if (("ST10".equals("ST10")) || ("ST10".equals("ST12")))
      {
        if (this.k1_max_value - this.k1_min_value >= 1000)
        {
          this.rcd_k1_status.setBackgroundColor(-14503604);
          this.rcd_k1_status.setText(2131296575);
        }
      }
      else if (("ST10".equals("ST15")) && (this.k1_max_value - this.k1_min_value >= 4095))
      {
        this.rcd_k1_status.setBackgroundColor(-14503604);
        this.rcd_k1_status.setText(2131296575);
        continue;
        if (paramInt2 > this.k2_max_value) {
          this.k2_max_value = paramInt2;
        }
        if (paramInt2 < this.k2_min_value) {
          this.k2_min_value = paramInt2;
        }
        if (("ST10".equals("ST10")) || ("ST10".equals("ST12")))
        {
          if (this.k2_max_value - this.k2_min_value >= 1000)
          {
            this.rcd_k2_status.setBackgroundColor(-14503604);
            this.rcd_k2_status.setText(2131296575);
          }
        }
        else if (("ST10".equals("ST15")) && (this.k2_max_value - this.k2_min_value >= 4095))
        {
          this.rcd_k2_status.setBackgroundColor(-14503604);
          this.rcd_k2_status.setText(2131296575);
          continue;
          if (paramInt2 > this.k3_max_value) {
            this.k3_max_value = paramInt2;
          }
          if (paramInt2 < this.k3_min_value) {
            this.k3_min_value = paramInt2;
          }
          if (this.k3_max_value - this.k3_min_value >= 4000)
          {
            this.rcd_k3_status.setBackgroundColor(-14503604);
            this.rcd_k3_status.setText(2131296575);
            continue;
            if (paramInt2 > this.k4_max_value) {
              this.k4_max_value = paramInt2;
            }
            if (paramInt2 < this.k4_min_value) {
              this.k4_min_value = paramInt2;
            }
            if (this.k4_max_value - this.k4_min_value >= 1200)
            {
              this.rcd_k4_status.setBackgroundColor(-14503604);
              this.rcd_k4_status.setText(2131296575);
              continue;
              if (paramInt2 > this.k5_max_value) {
                this.k5_max_value = paramInt2;
              }
              if (paramInt2 < this.k5_min_value) {
                this.k5_min_value = paramInt2;
              }
              if (this.k5_max_value - this.k5_min_value >= 1200)
              {
                this.rcd_k5_status.setBackgroundColor(-14503604);
                this.rcd_k5_status.setText(2131296575);
              }
            }
          }
        }
      }
    }
  }
  
  private void setSStatus(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    }
    for (;;)
    {
      return;
      if ((this.s1_max_value != -1) && (this.s1_min_value != -1))
      {
        this.rcd_s1_status.setBackgroundColor(-14503604);
        this.rcd_s1_status.setText(2131296575);
      }
      if (paramInt2 == 0)
      {
        this.s1_min_value = paramInt2;
      }
      else if (paramInt2 == 1)
      {
        this.s1_max_value = paramInt2;
        continue;
        if ("ST10".equals("ST12"))
        {
          if ((this.s2_max_value != -1) && (this.s2_min_value != -1) && (this.s2_mid_value != -1))
          {
            this.rcd_s2_status.setBackgroundColor(-14503604);
            this.rcd_s2_status.setText(2131296575);
          }
          if (paramInt2 == 0) {
            this.s2_min_value = paramInt2;
          } else if (paramInt2 == 1) {
            this.s2_mid_value = paramInt2;
          } else if (paramInt2 == 2) {
            this.s2_max_value = paramInt2;
          }
        }
        else if ("ST10".equals("ST15"))
        {
          if ((this.s2_max_value != -1) && (this.s2_min_value != -1) && (this.s2_mid_value != -1))
          {
            this.rcd_s1_status.setBackgroundColor(-14503604);
            this.rcd_s1_status.setText(2131296575);
          }
          if (paramInt2 == 0)
          {
            this.s2_min_value = paramInt2;
          }
          else if (paramInt2 == 1)
          {
            this.s2_max_value = paramInt2;
            continue;
            if ((this.s3_max_value != -1) && (this.s3_min_value != -1) && (this.s3_mid_value != -1))
            {
              this.rcd_s3_status.setBackgroundColor(-14503604);
              this.rcd_s3_status.setText(2131296575);
            }
            if (paramInt2 == 0)
            {
              this.s3_min_value = paramInt2;
            }
            else if (paramInt2 == 1)
            {
              this.s3_mid_value = paramInt2;
            }
            else if (paramInt2 == 2)
            {
              this.s3_max_value = paramInt2;
              continue;
              if ((this.s4_max_value != -1) && (this.s4_min_value != -1))
              {
                this.rcd_s4_status.setBackgroundColor(-14503604);
                this.rcd_s4_status.setText(2131296575);
              }
              if (paramInt2 == 0) {
                this.s4_min_value = paramInt2;
              } else if (paramInt2 == 1) {
                this.s4_max_value = paramInt2;
              }
            }
          }
        }
      }
    }
  }
  
  private void setValue(UARTInfoMessage.CalibrationRawData paramCalibrationRawData)
  {
    if (paramCalibrationRawData != null) {
      switch (paramCalibrationRawData.rawData.size())
      {
      }
    }
    for (;;)
    {
      return;
      this.rcd_b2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(14)));
      setBStatus(2, ((Integer)paramCalibrationRawData.rawData.get(14)).intValue());
      this.rcd_b1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(13)));
      setBStatus(1, ((Integer)paramCalibrationRawData.rawData.get(13)).intValue());
      this.rcd_s4_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(12)));
      setSStatus(4, ((Integer)paramCalibrationRawData.rawData.get(12)).intValue());
      this.rcd_s3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(11)));
      setSStatus(3, ((Integer)paramCalibrationRawData.rawData.get(11)).intValue());
      this.rcd_s2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(10)));
      setSStatus(2, ((Integer)paramCalibrationRawData.rawData.get(10)).intValue());
      this.rcd_s1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(9)));
      setSStatus(1, ((Integer)paramCalibrationRawData.rawData.get(9)).intValue());
      this.rcd_k5_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(8)));
      setKStatus(5, ((Integer)paramCalibrationRawData.rawData.get(8)).intValue());
      this.rcd_k4_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(7)));
      setKStatus(4, ((Integer)paramCalibrationRawData.rawData.get(7)).intValue());
      this.rcd_k3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(6)));
      setKStatus(3, ((Integer)paramCalibrationRawData.rawData.get(6)).intValue());
      this.rcd_k2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(5)));
      setKStatus(2, ((Integer)paramCalibrationRawData.rawData.get(5)).intValue());
      this.rcd_k1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(4)));
      setKStatus(1, ((Integer)paramCalibrationRawData.rawData.get(4)).intValue());
      this.rcd_j4_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(3)));
      setJStatus(4, ((Integer)paramCalibrationRawData.rawData.get(3)).intValue());
      this.rcd_j3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(2)));
      setJStatus(3, ((Integer)paramCalibrationRawData.rawData.get(2)).intValue());
      this.rcd_j2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(1)));
      setJStatus(2, ((Integer)paramCalibrationRawData.rawData.get(1)).intValue());
      this.rcd_j1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(0)));
      setJStatus(1, ((Integer)paramCalibrationRawData.rawData.get(0)).intValue());
    }
  }
  
  private void setValue10(UARTInfoMessage.CalibrationRawData paramCalibrationRawData)
  {
    Log.i("setValue", "@@setValue10@@raw_data.rawData.size()=" + paramCalibrationRawData.rawData.size() + ",J1.get(0)=" + paramCalibrationRawData.rawData.get(0) + ",J2.get(1)=" + paramCalibrationRawData.rawData.get(1) + ",J3.get(2)=" + paramCalibrationRawData.rawData.get(2) + ",J4.get(3)=" + paramCalibrationRawData.rawData.get(3) + ",K1.get(4)=" + paramCalibrationRawData.rawData.get(4) + ",K2.get(5)=" + paramCalibrationRawData.rawData.get(5) + ",S1.get(6)=" + paramCalibrationRawData.rawData.get(6) + ",B1.get(7)=" + paramCalibrationRawData.rawData.get(7) + ",B2.get(8)=" + paramCalibrationRawData.rawData.get(8) + ",B3.get(9)=" + paramCalibrationRawData.rawData.get(9));
    if (paramCalibrationRawData != null)
    {
      this.rcd_b3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(9)));
      setBStatus(3, ((Integer)paramCalibrationRawData.rawData.get(9)).intValue());
      this.rcd_b2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(8)));
      setBStatus(2, ((Integer)paramCalibrationRawData.rawData.get(8)).intValue());
      this.rcd_b1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(7)));
      setBStatus(1, ((Integer)paramCalibrationRawData.rawData.get(7)).intValue());
      this.rcd_s1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(6)));
      setSStatus(1, ((Integer)paramCalibrationRawData.rawData.get(6)).intValue());
      this.rcd_k2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(5)));
      setKStatus(2, ((Integer)paramCalibrationRawData.rawData.get(5)).intValue());
      this.rcd_k1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(4)));
      setKStatus(1, ((Integer)paramCalibrationRawData.rawData.get(4)).intValue());
      this.rcd_j4_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(3)));
      setJStatus(4, ((Integer)paramCalibrationRawData.rawData.get(3)).intValue());
      this.rcd_j3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(2)));
      setJStatus(3, ((Integer)paramCalibrationRawData.rawData.get(2)).intValue());
      this.rcd_j2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(1)));
      setJStatus(2, ((Integer)paramCalibrationRawData.rawData.get(1)).intValue());
      this.rcd_j1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(0)));
      setJStatus(1, ((Integer)paramCalibrationRawData.rawData.get(0)).intValue());
    }
    for (;;)
    {
      return;
      Log.i("setValue", "####raw_data == null####");
    }
  }
  
  private void setValue12(UARTInfoMessage.CalibrationRawData paramCalibrationRawData)
  {
    Log.i("setValue", "@@setValue12@@raw_data.rawData.size()=" + paramCalibrationRawData.rawData.size() + ",J1.get(0)=" + paramCalibrationRawData.rawData.get(0) + ",J2.get(1)=" + paramCalibrationRawData.rawData.get(1) + ",J3.get(2)=" + paramCalibrationRawData.rawData.get(2) + ",J4.get(3)=" + paramCalibrationRawData.rawData.get(3) + ",K3.get(4)=" + paramCalibrationRawData.rawData.get(4) + ",K1.get(5)=" + paramCalibrationRawData.rawData.get(5) + ",K2.get(6)=" + paramCalibrationRawData.rawData.get(6) + ",S1.get(7)=" + paramCalibrationRawData.rawData.get(7) + ",S2.get(8)=" + paramCalibrationRawData.rawData.get(8) + ",B1.get(9)=" + paramCalibrationRawData.rawData.get(9) + ",B2.get(10)=" + paramCalibrationRawData.rawData.get(10) + ",B3.get(11)=" + paramCalibrationRawData.rawData.get(11));
    if (paramCalibrationRawData != null)
    {
      this.rcd_b3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(11)));
      setBStatus(3, ((Integer)paramCalibrationRawData.rawData.get(11)).intValue());
      this.rcd_b2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(10)));
      setBStatus(2, ((Integer)paramCalibrationRawData.rawData.get(10)).intValue());
      this.rcd_b1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(9)));
      setBStatus(1, ((Integer)paramCalibrationRawData.rawData.get(9)).intValue());
      this.rcd_s2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(8)));
      setSStatus(2, ((Integer)paramCalibrationRawData.rawData.get(8)).intValue());
      this.rcd_s1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(7)));
      setSStatus(1, ((Integer)paramCalibrationRawData.rawData.get(7)).intValue());
      this.rcd_k3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(6)));
      setKStatus(3, ((Integer)paramCalibrationRawData.rawData.get(6)).intValue());
      this.rcd_k2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(5)));
      setKStatus(2, ((Integer)paramCalibrationRawData.rawData.get(5)).intValue());
      this.rcd_k1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(4)));
      setKStatus(1, ((Integer)paramCalibrationRawData.rawData.get(4)).intValue());
      this.rcd_j4_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(3)));
      setJStatus(4, ((Integer)paramCalibrationRawData.rawData.get(3)).intValue());
      this.rcd_j3_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(2)));
      setJStatus(3, ((Integer)paramCalibrationRawData.rawData.get(2)).intValue());
      this.rcd_j2_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(1)));
      setJStatus(2, ((Integer)paramCalibrationRawData.rawData.get(1)).intValue());
      this.rcd_j1_value.setText(String.valueOf(paramCalibrationRawData.rawData.get(0)));
      setJStatus(1, ((Integer)paramCalibrationRawData.rawData.get(0)).intValue());
    }
    for (;;)
    {
      return;
      Log.i("setValue", "####raw_data == null####");
    }
  }
  
  public void onClick(View paramView)
  {
    if (paramView.equals(this.btn_finish)) {
      finish();
    }
    for (;;)
    {
      return;
      if (paramView.equals(this.btn_reset)) {
        this.isReset = true;
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    if ("ST10".equals("ST10"))
    {
      setContentView(2130903101);
      this.rcd_j1_value = ((TextView)findViewById(2131689844));
      this.rcd_j2_value = ((TextView)findViewById(2131689847));
      this.rcd_j3_value = ((TextView)findViewById(2131689850));
      this.rcd_j4_value = ((TextView)findViewById(2131689853));
      this.rcd_k1_value = ((TextView)findViewById(2131689886));
      this.rcd_k2_value = ((TextView)findViewById(2131689889));
      this.rcd_s1_value = ((TextView)findViewById(2131689868));
      this.rcd_b1_value = ((TextView)findViewById(2131689880));
      this.rcd_b2_value = ((TextView)findViewById(2131689883));
      this.rcd_b3_value = ((TextView)findViewById(2131689903));
      this.rcd_b3_status = ((TextView)findViewById(2131689904));
      this.rcd_j1_status = ((TextView)findViewById(2131689845));
      this.rcd_j2_status = ((TextView)findViewById(2131689848));
      this.rcd_j3_status = ((TextView)findViewById(2131689851));
      this.rcd_j4_status = ((TextView)findViewById(2131689854));
      this.rcd_k1_status = ((TextView)findViewById(2131689887));
      this.rcd_k2_status = ((TextView)findViewById(2131689890));
      this.rcd_s1_status = ((TextView)findViewById(2131689869));
      this.rcd_b1_status = ((TextView)findViewById(2131689881));
      this.rcd_b2_status = ((TextView)findViewById(2131689884));
      if (!"ST10".equals("ST12")) {
        break label469;
      }
      this.rcd_s2_value = ((TextView)findViewById(2131689871));
      this.rcd_s2_status = ((TextView)findViewById(2131689872));
      this.rcd_k3_value = ((TextView)findViewById(2131689892));
      this.rcd_k3_status = ((TextView)findViewById(2131689893));
    }
    for (;;)
    {
      this.isReset = true;
      this.btn_finish = ((Button)findViewById(2131689900));
      this.btn_reset = ((Button)findViewById(2131689901));
      this.btn_finish.setOnClickListener(this);
      this.btn_reset.setOnClickListener(this);
      return;
      if ("ST10".equals("ST12"))
      {
        setContentView(2130903102);
        break;
      }
      if (!"ST10".equals("ST15")) {
        break;
      }
      setContentView(2130903100);
      break;
      label469:
      if ("ST10".equals("ST15"))
      {
        this.rcd_k3_value = ((TextView)findViewById(2131689892));
        this.rcd_k4_value = ((TextView)findViewById(2131689895));
        this.rcd_k5_value = ((TextView)findViewById(2131689898));
        this.rcd_s2_value = ((TextView)findViewById(2131689871));
        this.rcd_s3_value = ((TextView)findViewById(2131689874));
        this.rcd_s4_value = ((TextView)findViewById(2131689877));
        this.rcd_t1_value = ((TextView)findViewById(2131689856));
        this.rcd_t2_value = ((TextView)findViewById(2131689859));
        this.rcd_t3_value = ((TextView)findViewById(2131689862));
        this.rcd_t4_value = ((TextView)findViewById(2131689865));
        this.rcd_k3_status = ((TextView)findViewById(2131689893));
        this.rcd_k4_status = ((TextView)findViewById(2131689896));
        this.rcd_k5_status = ((TextView)findViewById(2131689899));
        this.rcd_s2_status = ((TextView)findViewById(2131689872));
        this.rcd_s3_status = ((TextView)findViewById(2131689875));
        this.rcd_s4_status = ((TextView)findViewById(2131689878));
        this.rcd_t1_status = ((TextView)findViewById(2131689857));
        this.rcd_t2_status = ((TextView)findViewById(2131689860));
        this.rcd_t3_status = ((TextView)findViewById(2131689863));
        this.rcd_t4_status = ((TextView)findViewById(2131689866));
      }
    }
  }
  
  protected void onPause()
  {
    super.onPause();
    this.mController.stopReading();
    this.mController.registerReaderHandler(null);
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    this.mController.registerReaderHandler(this.mUartHandler);
    this.mController.startReading();
  }
  
  public void resetAll()
  {
    this.j1_max_value = 0;
    this.j2_max_value = 0;
    this.j3_max_value = 0;
    this.j4_max_value = 0;
    this.j1_min_value = 4096;
    this.j2_min_value = 4096;
    this.j3_min_value = 4096;
    this.j4_min_value = 4096;
    this.k1_max_value = 0;
    this.k1_min_value = 4096;
    this.k2_max_value = 0;
    this.k2_min_value = 4096;
    this.k3_max_value = 0;
    this.k3_min_value = 4096;
    this.s1_max_value = -1;
    this.s1_mid_value = -1;
    this.s1_min_value = -1;
    this.s2_max_value = -1;
    this.s2_mid_value = -1;
    this.s2_min_value = -1;
    this.b1_max_value = -1;
    this.b2_max_value = -1;
    this.b1_min_value = -1;
    this.b2_min_value = -1;
    this.b3_max_value = -1;
    this.b3_min_value = -1;
    this.rcd_j1_status.setBackgroundColor(-3407872);
    this.rcd_j1_status.setText(2131296574);
    this.rcd_j2_status.setBackgroundColor(-3407872);
    this.rcd_j2_status.setText(2131296574);
    this.rcd_j3_status.setBackgroundColor(-3407872);
    this.rcd_j3_status.setText(2131296574);
    this.rcd_j4_status.setBackgroundColor(-3407872);
    this.rcd_j4_status.setText(2131296574);
    this.rcd_k1_status.setBackgroundColor(-3407872);
    this.rcd_k1_status.setText(2131296574);
    this.rcd_k2_status.setBackgroundColor(-3407872);
    this.rcd_k2_status.setText(2131296574);
    this.rcd_s1_status.setBackgroundColor(-3407872);
    this.rcd_s1_status.setText(2131296574);
    this.rcd_b1_status.setBackgroundColor(-3407872);
    this.rcd_b1_status.setText(2131296574);
    this.rcd_b2_status.setBackgroundColor(-3407872);
    this.rcd_b2_status.setText(2131296574);
    this.rcd_b3_status.setBackgroundColor(-3407872);
    this.rcd_b3_status.setText(2131296574);
    if ("ST10".equals("ST12"))
    {
      this.b3_max_value = -1;
      this.b3_min_value = -1;
      this.k3_max_value = 0;
      this.k3_min_value = 4096;
      this.s2_max_value = -1;
      this.s2_mid_value = -1;
      this.s2_min_value = -1;
      this.rcd_b3_status.setBackgroundColor(-3407872);
      this.rcd_b3_status.setText(2131296574);
      this.rcd_k3_status.setBackgroundColor(-3407872);
      this.rcd_k3_status.setText(2131296574);
      this.rcd_s2_status.setBackgroundColor(-3407872);
      this.rcd_s2_status.setText(2131296574);
    }
    for (;;)
    {
      this.isReset = false;
      return;
      if ("ST10".equals("ST15"))
      {
        this.k3_max_value = 0;
        this.k4_max_value = 0;
        this.k5_max_value = 0;
        this.k3_min_value = 4096;
        this.k4_min_value = 4096;
        this.k5_min_value = 4096;
        this.s2_max_value = -1;
        this.s3_max_value = -1;
        this.s4_max_value = -1;
        this.s2_mid_value = -1;
        this.s3_mid_value = -1;
        this.s2_min_value = -1;
        this.s3_min_value = -1;
        this.s4_min_value = -1;
        this.rcd_k3_status.setBackgroundColor(-3407872);
        this.rcd_k3_status.setText(2131296574);
        this.rcd_k4_status.setBackgroundColor(-3407872);
        this.rcd_k4_status.setText(2131296574);
        this.rcd_k5_status.setBackgroundColor(-3407872);
        this.rcd_k5_status.setText(2131296574);
        this.rcd_s2_status.setBackgroundColor(-3407872);
        this.rcd_s2_status.setText(2131296574);
        this.rcd_s3_status.setBackgroundColor(-3407872);
        this.rcd_s3_status.setText(2131296574);
        this.rcd_s4_status.setBackgroundColor(-3407872);
        this.rcd_s4_status.setText(2131296574);
      }
    }
  }
}


