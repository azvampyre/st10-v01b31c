package com.yuneec.test;

import android.os.Handler;
import android.util.Log;
import com.yuneec.uartcontroller.UARTController;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import org.json.JSONObject;

public class Transfer
  implements Runnable
{
  private static final int MAX_BUFFER_BYTES = 128;
  public static final int TYPE_DATA_FLIGHT = 1001;
  public static final int TYPE_DATA_GPS = 1002;
  public static final int TYPE_DATA_TELE = 1003;
  private Socket client;
  private int dataLength = 0;
  private BufferedInputStream mRece = null;
  private BufferedOutputStream mSend = null;
  private UARTController mUBSUARTController = null;
  private Handler mUSBHandler = null;
  private byte[] outBytesBuf = null;
  byte[] tmpByte = new byte[4];
  private boolean transferRunning = true;
  
  public Transfer(Socket paramSocket)
  {
    this.client = paramSocket;
  }
  
  static byte generateCRC8(byte[] paramArrayOfByte, int paramInt)
  {
    byte b = 119;
    int i;
    for (paramInt = 0;; paramInt++)
    {
      if (paramInt >= paramArrayOfByte.length) {
        return b;
      }
      b = (byte)(paramArrayOfByte[paramInt] & 0x77 ^ b);
      i = 0;
      if (i < 8) {
        break;
      }
    }
    if ((b & 0x1) != 0) {}
    for (b = (byte)((byte)(b >> 1) ^ 0x77);; b = (byte)(b >> 1))
    {
      i++;
      break;
    }
  }
  
  private String rtnStrResult(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (String str = "REQ_OK";; str = "REQ_ERROR") {
      return str;
    }
  }
  
  public void run()
  {
    int i;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      try
      {
        localObject1 = new java/io/BufferedOutputStream;
        ((BufferedOutputStream)localObject1).<init>(this.client.getOutputStream());
        this.mSend = ((BufferedOutputStream)localObject1);
        localObject1 = new java/io/BufferedInputStream;
        ((BufferedInputStream)localObject1).<init>(this.client.getInputStream());
        this.mRece = ((BufferedInputStream)localObject1);
        this.outBytesBuf = new byte[''];
        if (!this.transferRunning)
        {
          if (this.mUSBHandler != null) {
            this.mUSBHandler = null;
          }
          if (this.mSend != null) {
            this.mSend.close();
          }
          if (this.mRece != null) {
            this.mRece.close();
          }
          if (this.mRece != null) {
            this.outBytesBuf = null;
          }
          if (this.tmpByte != null) {
            this.tmpByte = null;
          }
          return;
        }
      }
      catch (Exception localException1)
      {
        Object localObject1;
        byte[] arrayOfByte;
        localException1.printStackTrace();
        continue;
        i = this.mRece.read(localException2, 0, localException2.length);
        localObject2 = new java/lang/String;
        ((String)localObject2).<init>(localException2, 0, i, "utf-8");
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("TEST Rece Data:[");
        Log.i("TEST", (String)localObject2 + "]");
        if (this.mUBSUARTController == null) {
          break label693;
        }
      }
      arrayOfByte = new byte[''];
      try
      {
        if (this.client.isConnected()) {
          continue;
        }
        Log.i("TEST", "TEST Service socket is Disconnected");
      }
      catch (Exception localException2)
      {
        localException2.printStackTrace();
        this.transferRunning = false;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("TEST Exception!!! ");
        Log.i("TEST", localException2);
      }
      continue;
      localObject3 = new org/json/JSONObject;
      ((JSONObject)localObject3).<init>((String)localObject2);
      localObject2 = ((JSONObject)localObject3).getString("type");
      if (localObject2 != null) {
        break;
      }
      Log.i("TEST", "TEST ERROR read data is wrong!");
    }
    boolean bool;
    if (((String)localObject2).equals("setting"))
    {
      i = ((JSONObject)localObject3).getInt("rf_power");
      bool = this.mUBSUARTController.writeTransmitRate(i);
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("TEST send Data setting : [");
      Log.i("TEST", bool + "]");
      this.mSend.write(rtnStrResult(bool).getBytes(), 0, rtnStrResult(bool).getBytes().length);
      this.mSend.flush();
    }
    for (;;)
    {
      break;
      if (((String)localObject2).equals("getting"))
      {
        localObject3 = String.valueOf(this.mUBSUARTController.readTransmitRate());
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("TEST send Data getting : [");
        Log.i("TEST", (String)localObject3 + "]");
        this.mSend.write(((String)localObject3).getBytes(), 0, ((String)localObject3).getBytes().length);
        this.mSend.flush();
      }
      else if (((String)localObject2).equals("enter"))
      {
        int j = ((JSONObject)localObject3).getInt("rf_channel");
        i = ((JSONObject)localObject3).getInt("rf_mode");
        bool = this.mUBSUARTController.PCenterTestRF(j, i);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("TEST send Data enter : [");
        Log.i("TEST", bool + "]");
        this.mSend.write(rtnStrResult(bool).getBytes(), 0, rtnStrResult(bool).getBytes().length);
        this.mSend.flush();
      }
      else if (((String)localObject2).equals("exit"))
      {
        bool = this.mUBSUARTController.enterRun(true);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("TEST send Data exit : [");
        Log.i("TEST", bool + "]");
        this.mSend.write(rtnStrResult(bool).getBytes(), 0, rtnStrResult(bool).getBytes().length);
        this.mSend.flush();
      }
      else
      {
        Log.i("TEST", "TEST ERROR read type is wrong!");
        continue;
        label693:
        Log.i("TEST", "TEST ERROR mUBSUARTController is null!");
        Thread.sleep(1000L);
      }
    }
  }
  
  public void sendData(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (this.mSend != null)
    {
      this.mSend.write(paramArrayOfByte, paramInt1, paramInt2);
      this.mSend.flush();
    }
  }
  
  public void setData(byte[] paramArrayOfByte, int paramInt)
  {
    if (paramArrayOfByte.length >= 123) {
      Log.i("TEST", "ERROR setData length is over 256 bytes, is " + paramArrayOfByte.length);
    }
    for (;;)
    {
      return;
      if ((this.mSend == null) || (this.mRece == null) || (this.outBytesBuf == null))
      {
        Log.i("TEST", "ERROR setData mSend == null || mRece == null || outBytesBuf == null");
      }
      else
      {
        this.tmpByte[0] = 100;
        this.tmpByte[1] = ((byte)paramArrayOfByte.length);
        if ((paramInt >= 1001) && (paramInt <= 1003))
        {
          if (paramInt == 1001)
          {
            this.tmpByte[2] = 1;
            label108:
            int i = generateCRC8(paramArrayOfByte, paramArrayOfByte.length);
            System.arraycopy(this.tmpByte, 0, this.outBytesBuf, 0, 3);
            System.arraycopy(paramArrayOfByte, 0, this.outBytesBuf, 3, paramArrayOfByte.length);
            this.outBytesBuf[(paramArrayOfByte.length + 3)] = i;
            this.dataLength = (paramArrayOfByte.length + 4);
          }
          try
          {
            this.mSend.write(this.outBytesBuf, 0, this.dataLength);
            this.mSend.flush();
            Thread.sleep(10L);
          }
          catch (IOException paramArrayOfByte)
          {
            paramArrayOfByte.printStackTrace();
            continue;
            if (paramInt == 1002)
            {
              this.tmpByte[2] = 2;
              break label108;
            }
            if (paramInt != 1003) {
              break label108;
            }
            this.tmpByte[2] = 3;
            break label108;
          }
          catch (InterruptedException paramArrayOfByte)
          {
            paramArrayOfByte.printStackTrace();
          }
        }
        else
        {
          Log.i("TEST", "ERROR setData typeData=" + paramInt);
        }
      }
    }
  }
  
  public void setUARTService(UARTController paramUARTController)
  {
    this.mUBSUARTController = paramUARTController;
  }
  
  public void stopTransfer()
  {
    this.transferRunning = false;
  }
}


