package com.yuneec.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.yuneec.uartcontroller.UARTController;
import java.io.File;
import java.io.FilenameFilter;

public class UpdateMain
  extends Activity
  implements View.OnClickListener, AdapterView.OnItemClickListener
{
  private static final String FIRMWARELOCATION = Environment.getExternalStorageDirectory() + "/firmware";
  private static final String FIRMWARELOCATION_SD = "/storage/sdcard1/firmware";
  private static final String TAG = "rcfirmwareupdater";
  private Button mBtnFinish;
  private Button mBtnUpdateRF;
  private Button mBtnUpdateTX;
  private FilenameFilter mFirmwareFilter = new FilenameFilter()
  {
    public boolean accept(File paramAnonymousFile, String paramAnonymousString)
    {
      boolean bool2 = false;
      boolean bool1 = bool2;
      if (paramAnonymousString != null)
      {
        bool1 = bool2;
        if (paramAnonymousString.lastIndexOf(".bin") != -1) {
          bool1 = true;
        }
      }
      return bool1;
    }
  };
  private Dialog mGuardDialog;
  private ProgressDialog mProgressDialog;
  private ListView mRFFileList;
  private String mSelectedRFFile;
  private TextView mSelectedRFLabel;
  private String mSelectedTXFile;
  private TextView mSelectedTXLabel;
  private ListView mTXFileList;
  private UARTController mUARTController;
  
  private File[] combineTwoList(File[] paramArrayOfFile1, File[] paramArrayOfFile2)
  {
    File[] arrayOfFile1;
    int i;
    label32:
    File[] arrayOfFile2;
    if ((paramArrayOfFile1 != null) || (paramArrayOfFile2 != null)) {
      if (paramArrayOfFile1 != null) {
        if (paramArrayOfFile2 != null)
        {
          arrayOfFile1 = new File[paramArrayOfFile1.length + paramArrayOfFile2.length];
          if (paramArrayOfFile1 == null) {
            break label123;
          }
          i = 0;
          if (i < paramArrayOfFile1.length) {
            break label94;
          }
          arrayOfFile2 = arrayOfFile1;
          if (paramArrayOfFile2 != null)
          {
            i = 0;
            label48:
            if (i < paramArrayOfFile2.length) {
              break label107;
            }
            arrayOfFile2 = arrayOfFile1;
          }
        }
      }
    }
    for (;;)
    {
      return arrayOfFile2;
      arrayOfFile1 = new File[paramArrayOfFile1.length];
      break;
      if (paramArrayOfFile2 != null)
      {
        arrayOfFile1 = new File[paramArrayOfFile2.length];
        break;
      }
      arrayOfFile1 = new File[0];
      break;
      label94:
      arrayOfFile1[i] = paramArrayOfFile1[i];
      i++;
      break label32;
      label107:
      arrayOfFile1[(paramArrayOfFile1.length + i)] = paramArrayOfFile2[i];
      i++;
      break label48;
      label123:
      arrayOfFile2 = arrayOfFile1;
      if (paramArrayOfFile2 != null)
      {
        for (i = 0;; i++)
        {
          arrayOfFile2 = arrayOfFile1;
          if (i >= paramArrayOfFile2.length) {
            break;
          }
          arrayOfFile1[i] = paramArrayOfFile2[i];
        }
        arrayOfFile2 = new File[0];
      }
    }
  }
  
  private File[] loadList(String paramString)
  {
    paramString = new File(paramString);
    if (!paramString.exists()) {
      Log.i("rcfirmwareupdater", "dir is not existed :" + FIRMWARELOCATION);
    }
    for (paramString = null;; paramString = paramString.listFiles(this.mFirmwareFilter)) {
      return paramString;
    }
  }
  
  private void showNoSelectionDialog(String paramString)
  {
    new AlertDialog.Builder(this).setTitle(getString(2131296615)).setMessage(paramString).setPositiveButton(17039370, null).create().show();
  }
  
  public void onClick(View paramView)
  {
    if (paramView.equals(this.mBtnUpdateTX)) {
      if (TextUtils.isEmpty(this.mSelectedTXFile))
      {
        Log.e("rcfirmwareupdater", "No TX File selected");
        showNoSelectionDialog(getString(2131296611));
      }
    }
    for (;;)
    {
      return;
      this.mProgressDialog = ProgressDialog.show(this, getString(2131296613), getString(2131296273), false, false);
      new UpdateTask(null).execute(new String[] { "TX", this.mSelectedTXFile });
      continue;
      if (paramView.equals(this.mBtnUpdateRF))
      {
        if (TextUtils.isEmpty(this.mSelectedRFFile))
        {
          Log.e("rcfirmwareupdater", "No RF File selected");
          showNoSelectionDialog(getString(2131296612));
        }
        else
        {
          this.mProgressDialog = ProgressDialog.show(this, getString(2131296614), getString(2131296273), false, false);
          new UpdateTask(null).execute(new String[] { "RF", this.mSelectedRFFile });
        }
      }
      else if (paramView.equals(this.mBtnFinish)) {
        finish();
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903115);
    getWindow().addFlags(128);
    this.mBtnUpdateTX = ((Button)findViewById(2131690014));
    this.mBtnUpdateRF = ((Button)findViewById(2131690016));
    this.mBtnFinish = ((Button)findViewById(2131690017));
    this.mTXFileList = ((ListView)findViewById(2131690008));
    this.mRFFileList = ((ListView)findViewById(2131690011));
    this.mSelectedTXLabel = ((TextView)findViewById(2131690013));
    this.mSelectedRFLabel = ((TextView)findViewById(2131690015));
    this.mBtnUpdateTX.setOnClickListener(this);
    this.mBtnUpdateRF.setOnClickListener(this);
    this.mBtnFinish.setOnClickListener(this);
    File[] arrayOfFile2 = loadList(FIRMWARELOCATION + "/tx");
    Object localObject = loadList(FIRMWARELOCATION + "/rf");
    File[] arrayOfFile1 = loadList("/storage/sdcard1/firmware/tx");
    paramBundle = loadList("/storage/sdcard1/firmware/rf");
    arrayOfFile1 = combineTwoList(arrayOfFile2, arrayOfFile1);
    paramBundle = combineTwoList((File[])localObject, paramBundle);
    localObject = new FileAdapter(this, 17367043, 16908308, arrayOfFile1);
    paramBundle = new FileAdapter(this, 17367043, 16908308, paramBundle);
    this.mTXFileList.setAdapter((ListAdapter)localObject);
    this.mRFFileList.setAdapter(paramBundle);
    this.mTXFileList.setOnItemClickListener(this);
    this.mRFFileList.setOnItemClickListener(this);
    this.mUARTController = UARTController.getInstance();
    if (this.mUARTController == null)
    {
      localObject = UARTController.getPackagenameByProcess(this);
      paramBundle = new AlertDialog.Builder(this);
      if ("ST10".equals("ST10"))
      {
        this.mGuardDialog = paramBundle.setTitle(2131296601).setMessage(getResources().getString(2131296602, new Object[] { localObject })).setNeutralButton(17039370, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            paramAnonymousDialogInterface.dismiss();
            UpdateMain.this.finish();
          }
        }).setOnCancelListener(new DialogInterface.OnCancelListener()
        {
          public void onCancel(DialogInterface paramAnonymousDialogInterface)
          {
            UpdateMain.this.finish();
          }
        }).create();
        this.mGuardDialog.show();
      }
    }
    for (;;)
    {
      return;
      if ("ST10".equals("ST12"))
      {
        this.mGuardDialog = paramBundle.setTitle(2131296603).setMessage(getResources().getString(2131296604, new Object[] { localObject })).setNeutralButton(17039370, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            paramAnonymousDialogInterface.dismiss();
            UpdateMain.this.finish();
          }
        }).setOnCancelListener(new DialogInterface.OnCancelListener()
        {
          public void onCancel(DialogInterface paramAnonymousDialogInterface)
          {
            UpdateMain.this.finish();
          }
        }).create();
        break;
      }
      if (!"ST10".equals("ST15")) {
        break;
      }
      this.mGuardDialog = paramBundle.setTitle(2131296599).setMessage(getResources().getString(2131296600, new Object[] { localObject })).setNeutralButton(17039370, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.dismiss();
          UpdateMain.this.finish();
        }
      }).setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          UpdateMain.this.finish();
        }
      }).create();
      break;
      this.mUARTController.startReading();
      paramBundle = this.mUARTController.getTxBootloaderVersion();
      ((TextView)findViewById(2131690012)).setText("Bootloader:" + paramBundle);
      this.mUARTController.stopReading();
    }
  }
  
  protected void onDestroy()
  {
    if (this.mGuardDialog != null) {
      this.mGuardDialog.dismiss();
    }
    if (this.mUARTController != null) {
      this.mUARTController = null;
    }
    super.onDestroy();
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (paramAdapterView.equals(this.mTXFileList))
    {
      paramAdapterView = (File)paramAdapterView.getItemAtPosition(paramInt);
      this.mSelectedTXFile = paramAdapterView.getAbsolutePath();
      this.mSelectedTXLabel.setText(paramAdapterView.getName());
    }
    for (;;)
    {
      return;
      if (paramAdapterView.equals(this.mRFFileList))
      {
        paramAdapterView = (File)paramAdapterView.getItemAtPosition(paramInt);
        this.mSelectedRFFile = paramAdapterView.getAbsolutePath();
        this.mSelectedRFLabel.setText(paramAdapterView.getName());
      }
    }
  }
  
  private static class FileAdapter
    extends ArrayAdapter<File>
  {
    private int mFieldId;
    private LayoutInflater mInflater;
    private int mResource;
    
    public FileAdapter(Context paramContext, int paramInt1, int paramInt2, File[] paramArrayOfFile)
    {
      super(paramInt1, paramInt2, paramArrayOfFile);
      this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
      this.mResource = paramInt1;
      this.mFieldId = paramInt2;
    }
    
    