package com.yuneec.test;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.yuneec.uartcontroller.UARTController;

public class TransmitterTest
  extends Activity
  implements View.OnClickListener
{
  private Button mBtTransmitTest;
  private Button mChangeRateBtn;
  private UARTController mController;
  private int mCurrentRate = -1;
  private TextView mCurrentRateTxt;
  private EditText mEditTxt;
  private Button mReadRateBtn;
  private boolean mTransmitTesting;
  
  private void changeRate()
  {
    String str = this.mEditTxt.getText().toString();
    if (!str.isEmpty())
    {
      this.mCurrentRate = Integer.parseInt(str);
      if (this.mController != null)
      {
        boolean bool = this.mController.writeTransmitRate(this.mCurrentRate);
        Log.i("wangkang", "res=" + bool);
      }
    }
  }
  
  private void readRate()
  {
    int i = -1;
    if (this.mController != null) {
      i = this.mController.readTransmitRate();
    }
    if (i != -1) {
      this.mCurrentRateTxt.setText(getString(2131296590, new Object[] { Integer.valueOf(i) }));
    }
    for (;;)
    {
      return;
      this.mCurrentRateTxt.setText(getString(2131296590, new Object[] { "Unknown" }));
    }
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      return;
      if (this.mController != null) {
        if (this.mTransmitTesting)
        {
          this.mController.exitTransmitTest(true);
          this.mBtTransmitTest.setText(2131296588);
          this.mTransmitTesting = false;
        }
        else
        {
          this.mController.enterTransmitTest(true);
          this.mBtTransmitTest.setText(2131296589);
          this.mTransmitTesting = true;
          continue;
          changeRate();
          continue;
          readRate();
        }
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903113);
    getWindow().addFlags(128);
    this.mBtTransmitTest = ((Button)findViewById(2131690001));
    this.mBtTransmitTest.setOnClickListener(this);
    this.mCurrentRateTxt = ((TextView)findViewById(2131690002));
    this.mChangeRateBtn = ((Button)findViewById(2131690006));
    this.mChangeRateBtn.setOnClickListener(this);
    this.mReadRateBtn = ((Button)findViewById(2131690003));
    this.mReadRateBtn.setOnClickListener(this);
    this.mEditTxt = ((EditText)findViewById(2131690005));
    this.mTransmitTesting = false;
  }
  
  protected void onPause()
  {
    super.onPause();
    if ((this.mController != null) && (this.mTransmitTesting))
    {
      this.mController.exitTransmitTest(true);
      this.mBtTransmitTest.setText(2131296589);
      this.mTransmitTesting = false;
    }
    if (this.mController != null) {
      this.mController = null;
    }
    this.mCurrentRate = -1;
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    Button localButton = this.mBtTransmitTest;
    if (this.mTransmitTesting) {}
    for (int i = 2131296589;; i = 2131296588)
    {
      localButton.setText(i);
      this.mCurrentRateTxt.setText(getString(2131296590, new Object[] { "Unknown" }));
      return;
    }
  }
}


