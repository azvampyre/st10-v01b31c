package com.yuneec.test;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.CalibrationState;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalibrationTest
  extends Activity
{
  private static final String TAG = "CalibrationTest";
  private Button calibration_btn;
  private int color_green;
  private Intent intent;
  private boolean isShowFinish = false;
  private UARTController mController;
  private Handler mUartHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      int i;
      if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
      {
        paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        if (paramAnonymousMessage.what == 18)
        {
          paramAnonymousMessage = (UARTInfoMessage.CalibrationState)paramAnonymousMessage;
          int j = paramAnonymousMessage.hardware_state.size();
          if (j > 0)
          {
            i = 0;
            if (i < j) {
              break label52;
            }
          }
        }
      }
      return;
      label52:
      Log.i("CalibrationTest", "****[mUartHandler],stateMap.get(" + i + ")=" + CalibrationTest.this.stateMap.get(Integer.valueOf(i)) + ", cs.hardware_state.get(" + i + ")=" + paramAnonymousMessage.hardware_state.get(i) + "****");
      if (!((Boolean)CalibrationTest.this.stateMap.get(Integer.valueOf(i))).booleanValue()) {
        if (((Integer)paramAnonymousMessage.hardware_state.get(i)).intValue() != 0)
        {
          if (((Integer)paramAnonymousMessage.hardware_state.get(i)).intValue() != 4) {
            break label1303;
          }
          CalibrationTest.this.ragMap.put(Integer.valueOf(i), (Integer)paramAnonymousMessage.hardware_state.get(i));
          if (!"ST10".equals("ST10")) {
            break label523;
          }
          switch (i)
          {
          default: 
            label264:
            CalibrationTest.this.updateInterface();
          }
        }
      }
      for (;;)
      {
        i++;
        break;
        CalibrationTest.this.stateMap.put(Integer.valueOf(0), Boolean.valueOf(true));
        CalibrationTest.this.test_j1.setBackgroundColor(CalibrationTest.this.color_green);
        break label264;
        CalibrationTest.this.stateMap.put(Integer.valueOf(1), Boolean.valueOf(true));
        CalibrationTest.this.test_j2.setBackgroundColor(CalibrationTest.this.color_green);
        break label264;
        CalibrationTest.this.stateMap.put(Integer.valueOf(2), Boolean.valueOf(true));
        CalibrationTest.this.test_j3.setBackgroundColor(CalibrationTest.this.color_green);
        break label264;
        CalibrationTest.this.stateMap.put(Integer.valueOf(3), Boolean.valueOf(true));
        CalibrationTest.this.test_j4.setBackgroundColor(CalibrationTest.this.color_green);
        break label264;
        CalibrationTest.this.stateMap.put(Integer.valueOf(4), Boolean.valueOf(true));
        CalibrationTest.this.test_k1.setBackgroundColor(CalibrationTest.this.color_green);
        break label264;
        CalibrationTest.this.stateMap.put(Integer.valueOf(5), Boolean.valueOf(true));
        CalibrationTest.this.test_k2.setBackgroundColor(CalibrationTest.this.color_green);
        break label264;
        label523:
        if ("ST10".equals("ST12")) {
          switch (i)
          {
          default: 
            break;
          case 0: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(0), Boolean.valueOf(true));
            CalibrationTest.this.test_j1.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          case 1: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(1), Boolean.valueOf(true));
            CalibrationTest.this.test_j2.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          case 2: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(2), Boolean.valueOf(true));
            CalibrationTest.this.test_j3.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          case 3: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(3), Boolean.valueOf(true));
            CalibrationTest.this.test_j4.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          case 4: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(4), Boolean.valueOf(true));
            CalibrationTest.this.test_k1.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          case 5: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(5), Boolean.valueOf(true));
            CalibrationTest.this.test_k2.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          case 6: 
            CalibrationTest.this.stateMap.put(Integer.valueOf(6), Boolean.valueOf(true));
            CalibrationTest.this.test_k3.setBackgroundColor(CalibrationTest.this.color_green);
            break;
          }
        }
        if (!"ST10".equals("ST15")) {
          break label264;
        }
        switch (i)
        {
        default: 
          break;
        case 0: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(0), Boolean.valueOf(true));
          CalibrationTest.this.test_j1.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 1: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(1), Boolean.valueOf(true));
          CalibrationTest.this.test_j2.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 2: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(2), Boolean.valueOf(true));
          CalibrationTest.this.test_j3.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 3: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(3), Boolean.valueOf(true));
          CalibrationTest.this.test_j4.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 4: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(4), Boolean.valueOf(true));
          CalibrationTest.this.test_k1.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 5: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(5), Boolean.valueOf(true));
          CalibrationTest.this.test_k2.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 6: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(6), Boolean.valueOf(true));
          CalibrationTest.this.test_k3.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 7: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(7), Boolean.valueOf(true));
          CalibrationTest.this.test_k4.setBackgroundColor(CalibrationTest.this.color_green);
          break;
        case 8: 
          CalibrationTest.this.stateMap.put(Integer.valueOf(8), Boolean.valueOf(true));
          CalibrationTest.this.test_k5.setBackgroundColor(CalibrationTest.this.color_green);
          break;
          label1303:
          Log.v("CalibrationTest", "---error--" + i + paramAnonymousMessage.hardware_state.get(i));
          break;
          CalibrationTest.this.updateInterface();
        }
      }
    }
  };
  private Button page2_next;
  public Map<Integer, Integer> ragMap = new HashMap();
  public Map<Integer, Boolean> stateMap = new HashMap();
  private TextView test_j1;
  private TextView test_j2;
  private TextView test_j3;
  private TextView test_j4;
  private TextView test_k1;
  private TextView test_k2;
  private TextView test_k3;
  private TextView test_k4;
  private TextView test_k5;
  
  private void finishCalibrationTest()
  {
    this.calibration_btn.setText(2131296264);
    this.mController.exitFactoryCalibration(false);
    initTempValue();
    this.isShowFinish = false;
  }
  
  private void initTempValue()
  {
    for (int i = 0;; i++)
    {
      if (i >= 10) {
        return;
      }
      this.ragMap.put(Integer.valueOf(i), Integer.valueOf(0));
      this.stateMap.put(Integer.valueOf(i), Boolean.valueOf(false));
      if (!this.isShowFinish) {
        updateInterface();
      }
    }
  }
  
  private void startCalibrationTest()
  {
    this.calibration_btn.setText(2131296265);
    this.mController.enterFactoryCalibration(false);
    this.calibration_btn.setEnabled(false);
    initTempValue();
    this.isShowFinish = true;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if ("ST10".equals("ST10"))
    {
      setContentView(2130903050);
      getWindow().addFlags(128);
      this.intent = new Intent(this, ReadCalibrationData.class);
      this.test_k1 = ((TextView)findViewById(2131689521));
      this.test_k2 = ((TextView)findViewById(2131689522));
      if (!"ST10".equals("ST12")) {
        break label265;
      }
      this.test_k3 = ((TextView)findViewById(2131689523));
    }
    for (;;)
    {
      this.test_j1 = ((TextView)findViewById(2131689526));
      this.test_j2 = ((TextView)findViewById(2131689527));
      this.test_j3 = ((TextView)findViewById(2131689528));
      this.test_j4 = ((TextView)findViewById(2131689529));
      this.page2_next = ((Button)findViewById(2131689530));
      this.calibration_btn = ((Button)findViewById(2131689531));
      this.calibration_btn.setText(2131296264);
      this.page2_next.setOnClickListener(new CalibrationButtonListener(null));
      this.calibration_btn.setOnClickListener(new CalibrationButtonListener(null));
      this.color_green = Color.parseColor("#007800");
      initTempValue();
      return;
      if ("ST10".equals("ST12"))
      {
        setContentView(2130903051);
        break;
      }
      if (!"ST10".equals("ST15")) {
        break;
      }
      setContentView(2130903049);
      break;
      label265:
      if ("ST10".equals("ST15"))
      {
        this.test_k4 = ((TextView)findViewById(2131689524));
        this.test_k5 = ((TextView)findViewById(2131689525));
      }
    }
  }
  
  protected void onPause()
  {
    super.onPause();
    finishCalibrationTest();
    this.mController.stopReading();
    this.mController.registerReaderHandler(null);
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    this.mController.registerReaderHandler(this.mUartHandler);
    this.mController.startReading();
  }
  
  public void updateInterface()
  {
    if ("ST10".equals("ST10")) {
      if ((((Boolean)this.stateMap.get(Integer.valueOf(0))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(1))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(2))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(3))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(4))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(5))).booleanValue())) {
        if (this.isShowFinish) {
          this.calibration_btn.setEnabled(true);
        }
      }
    }
    for (;;)
    {
      return;
      if (this.isShowFinish)
      {
        this.calibration_btn.setEnabled(false);
        continue;
        if ("ST10".equals("ST12"))
        {
          if ((((Boolean)this.stateMap.get(Integer.valueOf(0))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(1))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(2))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(3))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(4))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(5))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(6))).booleanValue()))
          {
            if (this.isShowFinish) {
              this.calibration_btn.setEnabled(true);
            }
          }
          else if (this.isShowFinish) {
            this.calibration_btn.setEnabled(false);
          }
        }
        else if ("ST10".equals("ST15")) {
          if ((((Boolean)this.stateMap.get(Integer.valueOf(0))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(1))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(2))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(3))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(4))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(5))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(6))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(7))).booleanValue()) && (((Boolean)this.stateMap.get(Integer.valueOf(8))).booleanValue()))
          {
            if (this.isShowFinish) {
              this.calibration_btn.setEnabled(true);
            }
          }
          else if (this.isShowFinish) {
            this.calibration_btn.setEnabled(false);
          }
        }
      }
    }
  }
  
  private class CalibrationButtonListener
    implements View.OnClickListener
  {
    private CalibrationButtonListener() {}
    
    public void onClick(View paramView)
    {
      switch (paramView.getId())
      {
      }
      for (;;)
      {
        return;
        CalibrationTest.this.finish();
        CalibrationTest.this.startActivity(CalibrationTest.this.intent);
        continue;
        if (!CalibrationTest.this.isShowFinish) {
          CalibrationTest.this.startCalibrationTest();
        } else {
          CalibrationTest.this.finishCalibrationTest();
        }
      }
    }
  }
}


