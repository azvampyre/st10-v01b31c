package com.yuneec.test;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.yuneec.uartcontroller.UARTController;

public class ControllerTest
  extends Activity
  implements View.OnClickListener
{
  private Intent intent;
  private Button m4_read;
  private TextView m4_version;
  private UARTController mController;
  private Button page1_next;
  private Button ti_read;
  private TextView ti_version;
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      return;
      String str;
      if (this.mController != null)
      {
        str = getResources().getString(2131296569, new Object[] { this.mController.getTransmitterVersion() });
        paramView = str;
        if (TextUtils.isEmpty(str)) {
          paramView = getResources().getString(2131296568);
        }
        this.m4_version.setText(paramView);
        this.page1_next.setEnabled(true);
      }
      else
      {
        this.m4_version.setText(getResources().getString(2131296569, new Object[] { "UART occupied" }));
        this.page1_next.setEnabled(false);
        continue;
        if (this.mController != null)
        {
          str = getResources().getString(2131296570, new Object[] { this.mController.getRadioVersion() });
          paramView = str;
          if (TextUtils.isEmpty(str)) {
            paramView = getResources().getString(2131296568);
          }
          this.ti_version.setText(paramView);
          this.page1_next.setEnabled(true);
        }
        else
        {
          this.ti_version.setText(getResources().getString(2131296570, new Object[] { "UART occupied" }));
          this.page1_next.setEnabled(false);
          continue;
          finish();
          startActivity(this.intent);
        }
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903054);
    getWindow().addFlags(128);
    this.m4_version = ((TextView)findViewById(2131689544));
    this.ti_version = ((TextView)findViewById(2131689548));
    this.page1_next = ((Button)findViewById(2131689550));
    this.m4_read = ((Button)findViewById(2131689545));
    this.ti_read = ((Button)findViewById(2131689549));
    this.intent = new Intent(this, CalibrationTest.class);
    this.page1_next.setOnClickListener(this);
    this.m4_read.setOnClickListener(this);
    this.ti_read.setOnClickListener(this);
  }
  
  protected void onPause()
  {
    super.onPause();
    if (this.mController != null)
    {
      this.mController.stopReading();
      this.mController = null;
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    if (this.mController != null) {
      this.mController.startReading();
    }
  }
}


