package com.yuneec.test;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.Signal;

public class SignalTest
  extends Activity
{
  private static final String TAG = "SignalTest";
  private Runnable getSignalRunnable = new Runnable()
  {
    public void run()
    {
      SignalTest.this.mController.getSignal(false, SignalTest.this.mCurrentLine);
      Log.i("SignalTest", "Get signal value: Line-" + SignalTest.this.mCurrentLine);
      SignalTest.this.mHandler.postDelayed(SignalTest.this.getSignalRunnable, 1000L);
    }
  };
  private UARTController mController;
  CountAVGTask mCountTask = null;
  private int mCurrentLine = 1;
  private Handler mHandler = new Handler();
  private RadioGroup mLineGroup;
  private Switch mSignalSwitch;
  private Handler mUartHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (SignalTest.this.mController == null) {}
      for (;;)
      {
        return;
        if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
        {
          paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
          if (paramAnonymousMessage.what == 22)
          {
            paramAnonymousMessage = (UARTInfoMessage.Signal)paramAnonymousMessage;
            SignalTest.this.tx_signal.setText(SignalTest.this.getString(2131296583, new Object[] { "TX ", Integer.valueOf(paramAnonymousMessage.tx_signal) }));
            SignalTest.this.rf_signal.setText(SignalTest.this.getString(2131296583, new Object[] { "RF ", Integer.valueOf(paramAnonymousMessage.rf_signal) }));
            if (paramAnonymousMessage.tx_signal != 127) {
              SignalTest.this.countTXAverage(paramAnonymousMessage.tx_signal);
            }
            if (paramAnonymousMessage.rf_signal != 127) {
              SignalTest.this.countRFAverage(paramAnonymousMessage.rf_signal);
            }
            if (SignalTest.this.times < SignalTest.this.perTimes)
            {
              paramAnonymousMessage = SignalTest.this;
              paramAnonymousMessage.times += 1;
            }
            else
            {
              SignalTest.this.compareTXtarget();
              SignalTest.this.compareRFTarget();
            }
          }
        }
      }
    }
  };
  private int perTimes = 200;
  private TextView rf_avg;
  private float rf_avg_value = 0.0F;
  private TextView rf_result;
  private TextView rf_signal;
  private EditText rf_target;
  private EditText rf_target2;
  private int rf_target_value = 0;
  private EditText timeTxt;
  private int times = 0;
  private TextView tx_avg;
  private float tx_avg_value = 0.0F;
  private TextView tx_result;
  private TextView tx_signal;
  private EditText tx_target;
  private EditText tx_target2;
  private int tx_target_value = 0;
  
  public static void UartControllerStandBy(UARTController paramUARTController)
  {
    paramUARTController.stopReading();
    paramUARTController.registerReaderHandler(null);
  }
  
  private void compareRFTarget()
  {
    if (this.rf_avg_value < this.rf_target_value - 2)
    {
      this.rf_result.setText(2131296574);
      this.rf_result.setTextColor(-65536);
    }
    for (;;)
    {
      this.rf_result.setVisibility(0);
      return;
      this.rf_result.setText(2131296575);
      this.rf_result.setTextColor(-16711936);
    }
  }
  
  private void compareTXtarget()
  {
    if (this.tx_avg_value < this.tx_target_value - 2)
    {
      this.tx_result.setText(2131296574);
      this.tx_result.setTextColor(-65536);
    }
    for (;;)
    {
      this.tx_result.setVisibility(0);
      return;
      this.tx_result.setText(2131296575);
      this.tx_result.setTextColor(-16711936);
    }
  }
  
  private void countRFAverage(int paramInt)
  {
    if (this.perTimes > 1) {}
    for (this.rf_avg_value = ((paramInt + this.rf_avg_value) / 2.0F);; this.rf_avg_value = paramInt)
    {
      this.rf_avg.setText(getString(2131296585, new Object[] { String.format("%.1f", new Object[] { Float.valueOf(this.rf_avg_value) }) }));
      return;
    }
  }
  
  private void countTXAverage(int paramInt)
  {
    if (this.perTimes > 1) {}
    for (this.tx_avg_value = ((paramInt + this.tx_avg_value) / 2.0F);; this.tx_avg_value = paramInt)
    {
      this.tx_avg.setText(getString(2131296585, new Object[] { String.format("%.1f", new Object[] { Float.valueOf(this.tx_avg_value) }) }));
      return;
    }
  }
  
  private static boolean ensureAwaitState(UARTController paramUARTController)
  {
    return paramUARTController.correctTxState(1, 3);
  }
  
  private static boolean ensureBindState(UARTController paramUARTController)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (!paramUARTController.enterBind(true))
    {
      bool1 = bool2;
      if (!paramUARTController.correctTxState(2, 3)) {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  private void restoreViews()
  {
    this.tx_signal.setText(getString(2131296583, new Object[] { "TX ", "unknow" }));
    this.tx_avg.setText(getString(2131296585, new Object[] { Double.valueOf(0.0D) }));
    this.rf_signal.setText(getString(2131296583, new Object[] { "RF ", "unknow" }));
    this.rf_avg.setText(getString(2131296585, new Object[] { Double.valueOf(0.0D) }));
    this.tx_result.setVisibility(4);
    this.rf_result.setVisibility(4);
    this.mLineGroup.check(2131689910);
    this.mLineGroup.getChildAt(0).setEnabled(true);
    this.mLineGroup.getChildAt(1).setEnabled(true);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903104);
    getWindow().addFlags(128);
    this.tx_target = ((EditText)findViewById(2131689914));
    this.rf_target = ((EditText)findViewById(2131689917));
    this.tx_target2 = ((EditText)findViewById(2131689915));
    this.rf_target2 = ((EditText)findViewById(2131689918));
    this.tx_signal = ((TextView)findViewById(2131689921));
    this.tx_avg = ((TextView)findViewById(2131689922));
    this.tx_result = ((TextView)findViewById(2131689923));
    this.rf_signal = ((TextView)findViewById(2131689925));
    this.rf_avg = ((TextView)findViewById(2131689926));
    this.rf_result = ((TextView)findViewById(2131689927));
    this.timeTxt = ((EditText)findViewById(2131689929));
    this.mLineGroup = ((RadioGroup)findViewById(2131689909));
    this.mLineGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        if (paramAnonymousInt == 2131689910)
        {
          SignalTest.this.tx_target.setEnabled(true);
          SignalTest.this.rf_target.setEnabled(true);
          SignalTest.this.tx_target2.setEnabled(false);
          SignalTest.this.rf_target2.setEnabled(false);
          SignalTest.this.mCurrentLine = 1;
        }
        for (;;)
        {
          return;
          if (paramAnonymousInt == 2131689911)
          {
            SignalTest.this.tx_target2.setEnabled(true);
            SignalTest.this.rf_target2.setEnabled(true);
            SignalTest.this.tx_target.setEnabled(false);
            SignalTest.this.rf_target.setEnabled(false);
            SignalTest.this.mCurrentLine = 2;
          }
        }
      }
    });
    this.mSignalSwitch = ((Switch)findViewById(2131689930));
    this.mSignalSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean) {
          if ((SignalTest.this.mController != null) && (SignalTest.ensureBindState(SignalTest.this.mController)))
          {
            SignalTest.this.mController.startBind(false);
            if (SignalTest.this.mCurrentLine != 1) {
              break label367;
            }
            paramAnonymousCompoundButton = SignalTest.this.tx_target.getText().toString();
            if ((paramAnonymousCompoundButton != null) && (!paramAnonymousCompoundButton.isEmpty()))
            {
              SignalTest.this.tx_target_value = Integer.parseInt(paramAnonymousCompoundButton);
              if (SignalTest.this.tx_target_value < -128) {
                SignalTest.this.tx_target_value = -128;
              }
              if (SignalTest.this.tx_target_value > 127) {
                SignalTest.this.tx_target_value = 127;
              }
            }
            Log.i("SignalTest", "get TX target:" + SignalTest.this.tx_target_value);
            if (SignalTest.this.mCurrentLine != 1) {
              break label386;
            }
            paramAnonymousCompoundButton = SignalTest.this.rf_target.getText().toString();
            label185:
            if ((paramAnonymousCompoundButton != null) && (!paramAnonymousCompoundButton.isEmpty()))
            {
              SignalTest.this.rf_target_value = Integer.parseInt(paramAnonymousCompoundButton);
              if (SignalTest.this.rf_target_value < -128) {
                SignalTest.this.rf_target_value = -128;
              }
              if (SignalTest.this.rf_target_value > 127) {
                SignalTest.this.rf_target_value = 127;
              }
            }
            Log.i("SignalTest", "get RF target:" + SignalTest.this.rf_target_value);
            paramAnonymousCompoundButton = SignalTest.this.timeTxt.getText().toString();
            if (!paramAnonymousCompoundButton.isEmpty()) {
              break label405;
            }
            SignalTest.this.perTimes = 200;
            label310:
            SignalTest.this.times = 0;
            SignalTest.this.mHandler.post(SignalTest.this.getSignalRunnable);
            SignalTest.this.mLineGroup.getChildAt(0).setEnabled(false);
            SignalTest.this.mLineGroup.getChildAt(1).setEnabled(false);
          }
        }
        for (;;)
        {
          return;
          label367:
          paramAnonymousCompoundButton = SignalTest.this.tx_target2.getText().toString();
          break;
          label386:
          paramAnonymousCompoundButton = SignalTest.this.rf_target2.getText().toString();
          break label185;
          label405:
          SignalTest.this.perTimes = Integer.parseInt(paramAnonymousCompoundButton);
          break label310;
          if (SignalTest.this.mController != null) {
            SignalTest.ensureAwaitState(SignalTest.this.mController);
          }
          SignalTest.this.restoreViews();
          SignalTest.this.mHandler.removeCallbacks(SignalTest.this.getSignalRunnable);
          SignalTest.this.mLineGroup.getChildAt(0).setEnabled(true);
          SignalTest.this.mLineGroup.getChildAt(1).setEnabled(true);
          SignalTest.this.times = 0;
        }
      }
    });
  }
  
  protected void onPause()
  {
    super.onPause();
    this.mHandler.removeCallbacks(this.getSignalRunnable);
    if (this.mController != null)
    {
      if (!ensureAwaitState(this.mController)) {
        Log.e("SignalTest", "fail to change to await");
      }
      UartControllerStandBy(this.mController);
      this.mController = null;
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    restoreViews();
    this.mController = UARTController.getInstance();
    if (this.mController != null)
    {
      this.mController.registerReaderHandler(this.mUartHandler);
      this.mController.startReading();
    }
  }
  
  private class CountAVGTask
    extends AsyncTask<Integer, Void, Void>
  {
    private int rfTarget;
    private int txTarget;
    
    public CountAVGTask(int paramInt1, int paramInt2)
    {
      this.txTarget = paramInt1;
      this.rfTarget = paramInt2;
    }
    
    protected Void doInBackground(Integer... paramVarArgs)
    {
      paramVarArgs[0].intValue();
      paramVarArgs[1].intValue();
      return null;
    }
    
    protected void onPostExecute(Void paramVoid)
    {
      super.onPostExecute(paramVoid);
    }
  }
}


