package com.yuneec.test;

import android.app.Activity;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class GpsTestActivity
  extends Activity
  implements GpsStatus.Listener, LocationListener
{
  private static final String KEY_AVERAGE = "average";
  private static final String KEY_CURRENT = "current";
  private static final String KEY_INDEX = "index";
  private static final String KEY_PRN = "PRN";
  private static final String TAG = GpsTestActivity.class.getSimpleName();
  private SimpleAdapter listAdapter;
  private LocationManager mLocationManager;
  private long recordTimes = 0L;
  private SparseArray<SatelliteRecordData> satelliteSemaphoreData;
  private ListView satellitesList;
  private ArrayList<HashMap<String, String>> satellitesListViewData;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903040);
    paramBundle = (LinearLayout)findViewById(2131689472);
    ((TextView)paramBundle.findViewById(2131689607)).setText(2131296595);
    ((TextView)paramBundle.findViewById(2131689608)).setText(2131296596);
    ((TextView)paramBundle.findViewById(2131689609)).setText(2131296597);
    ((TextView)paramBundle.findViewById(2131689610)).setText(2131296598);
    this.satellitesList = ((ListView)findViewById(2131689473));
    this.mLocationManager = ((LocationManager)getSystemService("location"));
    this.satellitesListViewData = new ArrayList();
    this.satelliteSemaphoreData = new SparseArray();
    this.listAdapter = new SimpleAdapter(this, this.satellitesListViewData, 2130903068, new String[] { "index", "PRN", "current", "average" }, new int[] { 2131689607, 2131689608, 2131689609, 2131689610 });
    this.satellitesList.setAdapter(this.listAdapter);
  }
  
  protected void onDestroy()
  {
    this.satellitesListViewData.clear();
    this.satelliteSemaphoreData.clear();
    super.onDestroy();
  }
  
  public void onGpsStatusChanged(int paramInt)
  {
    Object localObject1 = this.mLocationManager.getGpsStatus(null);
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((GpsStatus)localObject1).getSatellites().iterator();
    this.recordTimes += 1L;
    label38:
    if (!((Iterator)localObject2).hasNext()) {
      this.satellitesListViewData.clear();
    }
    for (paramInt = 0;; paramInt++)
    {
      if (paramInt >= this.satelliteSemaphoreData.size())
      {
        this.listAdapter.notifyDataSetChanged();
        break;
        localObject1 = (GpsSatellite)((Iterator)localObject2).next();
        if (!((GpsSatellite)localObject1).usedInFix()) {
          break label38;
        }
        paramInt = ((GpsSatellite)localObject1).getPrn();
        float f2 = ((GpsSatellite)localObject1).getSnr();
        float f1 = f2;
        Log.i(TAG, "prn: " + paramInt + ", snr: " + f2);
        localObject1 = (SatelliteRecordData)this.satelliteSemaphoreData.get(paramInt);
        if (localObject1 == null)
        {
          localObject1 = new SatelliteRecordData();
          ((SatelliteRecordData)localObject1).recordData.addLast(Float.valueOf(f2));
          this.satelliteSemaphoreData.put(paramInt, localObject1);
          ((SatelliteRecordData)localObject1).recordTimes = this.recordTimes;
          ((SatelliteRecordData)localObject1).currentData = f2;
          ((SatelliteRecordData)localObject1).averageData = f1;
          break label38;
        }
        if (((SatelliteRecordData)localObject1).recordData.size() > 30) {
          ((SatelliteRecordData)localObject1).recordData.removeFirst();
        }
        ((SatelliteRecordData)localObject1).recordData.addLast(Float.valueOf(f2));
        f1 = 0.0F;
        Iterator localIterator = ((SatelliteRecordData)localObject1).recordData.iterator();
        for (;;)
        {
          if (!localIterator.hasNext())
          {
            f1 /= ((SatelliteRecordData)localObject1).recordData.size();
            break;
          }
          f1 += ((Float)localIterator.next()).floatValue();
        }
      }
      int i = this.satelliteSemaphoreData.keyAt(paramInt);
      localObject1 = (SatelliteRecordData)this.satelliteSemaphoreData.get(i);
      localObject2 = new HashMap();
      ((HashMap)localObject2).put("index", String.valueOf(paramInt + 1));
      ((HashMap)localObject2).put("PRN", String.valueOf(i));
      ((HashMap)localObject2).put("current", String.valueOf(((SatelliteRecordData)localObject1).currentData));
      ((HashMap)localObject2).put("average", String.format("%.2f", new Object[] { Float.valueOf(((SatelliteRecordData)localObject1).averageData) }));
      this.satellitesListViewData.add(localObject2);
      if (this.recordTimes - ((SatelliteRecordData)localObject1).recordTimes > 5L)
      {
        Log.i(TAG, "Remove previousSatelliteData");
        this.satelliteSemaphoreData.remove(i);
      }
    }
  }
  
  public void onLocationChanged(Location paramLocation) {}
  
  protected void onPause()
  {
    super.onPause();
    this.mLocationManager.removeUpdates(this);
    this.mLocationManager.removeGpsStatusListener(this);
  }
  
  public void onProviderDisabled(String paramString) {}
  
  public void onProviderEnabled(String paramString) {}
  
  protected void onResume()
  {
    super.onResume();
    this.mLocationManager.addGpsStatusListener(this);
    this.mLocationManager.requestLocationUpdates("gps", 0L, 0.0F, this);
    onGpsStatusChanged(1);
  }
  
  public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {}
  
  class SatelliteRecordData
  {
    public float averageData;
    public float currentData;
    public LinkedList<Float> recordData = new LinkedList();
    public long recordTimes = 0L;
    
    SatelliteRecordData() {}
  }
}


