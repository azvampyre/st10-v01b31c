package com.yuneec.test;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.yuneec.uartcontroller.UARTController;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ADBTestActivity
  extends Activity
  implements View.OnClickListener
{
  private static final int SERVER_PORT = 10086;
  private static Boolean mainThreadFlag = Boolean.valueOf(true);
  static String tmpStr = "{\"type\":\"setting\", \"rf_power\":10}";
  static String tmpStr1 = "{\"type\":\"getting\"}";
  static String tmpStr2 = "{\"type\":\"enter\", \"rf_channel\":15}";
  static String tmpStr3 = "{\"type\":\"exit\"}";
  private boolean DEBUG = false;
  private Button mBtnTest1;
  private Button mBtnTest2;
  private Button mBtnTest3;
  private Button mBtnTest4;
  private CommandHandler mCommandHandler;
  private EditText mEdtChannel;
  private EditText mEdtMode;
  private EditText mEdtPower;
  private Socket mPreSocket = null;
  private Socket mSocket = null;
  private Thread mThreaderMain = null;
  private Thread mThreaderSub = null;
  private Transfer mTransfer = null;
  private UARTController mUARTController = null;
  private ServerSocket serverSocket = null;
  
  private void destoryFun()
  {
    mainThreadFlag = Boolean.valueOf(false);
    try
    {
      if (this.mUARTController != null)
      {
        this.mUARTController.destory();
        this.mUARTController = null;
      }
      if (this.mTransfer != null) {
        this.mTransfer.stopTransfer();
      }
      if (this.mSocket != null) {
        this.mSocket.close();
      }
      if (this.serverSocket != null) {
        this.serverSocket.close();
      }
      this.mSocket = null;
      this.mTransfer = null;
      this.serverSocket = null;
      if ((this.mThreaderSub != null) && (this.mThreaderSub.isAlive())) {
        this.mThreaderSub.interrupt();
      }
      if ((this.mThreaderMain != null) && (this.mThreaderMain.isAlive())) {
        this.mThreaderMain.interrupt();
      }
      this.mThreaderSub = null;
      this.mThreaderMain = null;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
      }
    }
    Log.i("TEST", "TEST Sevice onDestroy!!!");
    this.mCommandHandler.getLooper().quit();
  }
  
  public void onClick(View paramView)
  {
    int i;
    boolean bool;
    if (this.DEBUG)
    {
      if (!paramView.equals(this.mBtnTest1)) {
        break label87;
      }
      i = 10;
      if (this.mEdtPower != null) {
        i = Integer.parseInt(this.mEdtPower.getText().toString());
      }
      bool = this.mUARTController.writeTransmitRate(i);
      Log.i("TEST", "TEST Setting power:" + i + ", mResult=" + bool);
    }
    for (;;)
    {
      return;
      label87:
      if (paramView.equals(this.mBtnTest2))
      {
        i = this.mUARTController.readTransmitRate();
        Log.i("TEST", "TEST Getting result=" + i);
      }
      else if (paramView.equals(this.mBtnTest3))
      {
        int k = 10;
        int m = 10;
        int j = k;
        i = m;
        if (this.mEdtChannel != null)
        {
          j = k;
          i = m;
          if (this.mEdtMode != null)
          {
            j = Integer.parseInt(this.mEdtChannel.getText().toString());
            i = Integer.parseInt(this.mEdtMode.getText().toString());
          }
        }
        bool = this.mUARTController.PCenterTestRF(j, i);
        Log.i("TEST", "TEST Enter Channel:" + j + ", Mode=" + i + ", result=" + bool);
      }
      else if (paramView.equals(this.mBtnTest4))
      {
        bool = this.mUARTController.enterRun(false);
        Log.i("TEST", "TEST Exit result=" + bool);
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    Log.i("TEST", "TEST --onCreate--");
    super.onCreate(paramBundle);
    setContentView(2130903094);
  }
  
  protected void onDestroy()
  {
    destoryFun();
    super.onDestroy();
  }
  
  protected void onPause()
  {
    destoryFun();
    super.onPause();
  }
  
  protected void onRestart()
  {
    super.onRestart();
    Log.i("TEST", "TEST --onRestart--");
  }
  
  protected void onResume()
  {
    super.onResume();
    Log.i("TEST", "TEST --onResume--");
    if (this.DEBUG)
    {
      this.mBtnTest1 = ((Button)findViewById(2131689489));
      this.mBtnTest1.setOnClickListener(this);
      this.mBtnTest1.setEnabled(true);
      this.mBtnTest2 = ((Button)findViewById(2131689490));
      this.mBtnTest2.setOnClickListener(this);
      this.mBtnTest2.setEnabled(true);
      this.mBtnTest3 = ((Button)findViewById(2131689491));
      this.mBtnTest3.setOnClickListener(this);
      this.mBtnTest3.setEnabled(true);
      this.mBtnTest4 = ((Button)findViewById(2131689492));
      this.mBtnTest4.setOnClickListener(this);
      this.mBtnTest4.setEnabled(true);
      this.mEdtPower = ((EditText)findViewById(2131689825));
      this.mEdtPower.setEnabled(true);
      this.mEdtChannel = ((EditText)findViewById(2131689827));
      this.mEdtChannel.setEnabled(true);
      this.mEdtMode = ((EditText)findViewById(2131689829));
      this.mEdtMode.setEnabled(true);
    }
    Log.i("TEST", "TEST Sevice is onCreate!!!");
    HandlerThread localHandlerThread = new HandlerThread("usb-handler");
    localHandlerThread.start();
    this.mCommandHandler = new CommandHandler(localHandlerThread.getLooper());
    if (this.mUARTController == null) {
      this.mUARTController = UARTController.getInstance();
    }
    if (this.mThreaderMain == null)
    {
      this.mThreaderMain = new Thread()
      {
        public void run()
        {
          for (;;)
          {
            try
            {
              Log.i("TEST", "TEST Device doListen()...");
              ADBTestActivity.mainThreadFlag = Boolean.valueOf(true);
              localObject2 = ADBTestActivity.this;
              localObject1 = new java/net/ServerSocket;
              ((ServerSocket)localObject1).<init>(10086);
              ((ADBTestActivity)localObject2).serverSocket = ((ServerSocket)localObject1);
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>("TEST Device get serverSocket=");
              Log.i("TEST", ADBTestActivity.this.serverSocket + ", mainThreadFlag=" + ADBTestActivity.mainThreadFlag);
              if (!ADBTestActivity.mainThreadFlag.booleanValue()) {
                return;
              }
            }
            catch (IOException localIOException)
            {
              Object localObject2;
              Object localObject1;
              localIOException.printStackTrace();
              continue;
            }
            ADBTestActivity.this.mSocket = ADBTestActivity.this.serverSocket.accept();
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>("TEST Device accept socket=");
            Log.i("TEST", ADBTestActivity.this.mSocket);
            if (ADBTestActivity.this.mSocket != null)
            {
              if ((ADBTestActivity.this.mPreSocket != null) && (ADBTestActivity.this.mPreSocket.isConnected()))
              {
                ADBTestActivity.this.mPreSocket.close();
                Log.i("TEST", "TEST Service mPreSocket is closed!!");
              }
              if (ADBTestActivity.this.mTransfer != null) {
                ADBTestActivity.this.mTransfer.stopTransfer();
              }
              if ((ADBTestActivity.this.mThreaderSub != null) && (ADBTestActivity.this.mThreaderSub.isAlive()))
              {
                ADBTestActivity.this.mThreaderSub.interrupt();
                Log.i("TEST", "TEST Service mThreaderSub is closed!!");
              }
              ADBTestActivity.this.mPreSocket = ADBTestActivity.this.mSocket;
              localObject1 = ADBTestActivity.this;
              localObject2 = new com/yuneec/test/Transfer;
              ((Transfer)localObject2).<init>(ADBTestActivity.this.mSocket);
              ((ADBTestActivity)localObject1).mTransfer = ((Transfer)localObject2);
              if (ADBTestActivity.this.mTransfer != null) {
                ADBTestActivity.this.mTransfer.setUARTService(ADBTestActivity.this.mUARTController);
              }
              localObject2 = ADBTestActivity.this;
              localObject1 = new java/lang/Thread;
              ((Thread)localObject1).<init>(ADBTestActivity.this.mTransfer);
              ((ADBTestActivity)localObject2).mThreaderSub = ((Thread)localObject1);
              ADBTestActivity.this.mThreaderSub.start();
            }
          }
        }
      };
      this.mThreaderMain.start();
    }
  }
  
  private class CommandHandler
    extends Handler
  {
    public CommandHandler(Looper paramLooper)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      if ((paramMessage.arg1 != 1) && (paramMessage.arg2 != 0)) {
        Log.i("TEST", "TEST msg.arg1:" + paramMessage.arg1 + ", msg.arg2:" + paramMessage.arg2);
      }
      for (;;)
      {
        return;
        switch (paramMessage.what)
        {
        default: 
          Log.i("TEST", "TEST msg.what ERROR : " + paramMessage.what);
          break;
        case 1001: 
          if (ADBTestActivity.this.mTransfer != null) {
            ADBTestActivity.this.mTransfer.setData((byte[])paramMessage.obj, 1001);
          }
          break;
        case 1002: 
          if (ADBTestActivity.this.mTransfer != null) {
            ADBTestActivity.this.mTransfer.setData((byte[])paramMessage.obj, 1002);
          }
          break;
        case 1003: 
          if (ADBTestActivity.this.mTransfer != null) {
            ADBTestActivity.this.mTransfer.setData((byte[])paramMessage.obj, 1003);
          }
          break;
        }
      }
    }
  }
}


