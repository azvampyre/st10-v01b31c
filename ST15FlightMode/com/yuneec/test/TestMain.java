package com.yuneec.test;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;

public class TestMain
  extends Activity
  implements View.OnClickListener
{
  private Button mADBTest;
  private Button mControllerTest;
  private Button mGpsTest;
  private Button mSignalTest;
  private Button mTransmitTest;
  private Switch mWifiSwitch;
  private TextView text_wifi_test;
  private WifiManager wifiManager = null;
  
  private void wifiSelect()
  {
    if (isWiFiActive(this))
    {
      this.mWifiSwitch.setChecked(false);
      this.text_wifi_test.setText("wifi开启中");
    }
    for (;;)
    {
      return;
      this.mWifiSwitch.setChecked(true);
      this.text_wifi_test.setText("wifi关闭中");
    }
  }
  
  public boolean isWiFiActive(Context paramContext)
  {
    return ((WifiManager)paramContext.getApplicationContext().getSystemService("wifi")).isWifiEnabled();
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      return;
      startActivity(new Intent(this, TransmitterTest.class));
      continue;
      startActivity(new Intent(this, ControllerTest.class));
      continue;
      startActivity(new Intent(this, SignalTest.class));
      continue;
      startActivity(new Intent(this, GpsTestActivity.class));
      continue;
      startActivity(new Intent(this, ADBTestActivity.class));
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903111);
    getWindow().addFlags(128);
    this.mTransmitTest = ((Button)findViewById(2131689988));
    this.mTransmitTest.setOnClickListener(this);
    this.mControllerTest = ((Button)findViewById(2131689990));
    this.mControllerTest.setOnClickListener(this);
    this.mSignalTest = ((Button)findViewById(2131689995));
    this.mSignalTest.setOnClickListener(this);
    this.mGpsTest = ((Button)findViewById(2131689992));
    this.mGpsTest.setOnClickListener(this);
    this.mADBTest = ((Button)findViewById(2131689997));
    this.mADBTest.setOnClickListener(this);
    this.text_wifi_test = ((TextView)findViewById(2131689991));
    this.wifiManager = ((WifiManager)super.getSystemService("wifi"));
    this.mWifiSwitch = ((Switch)findViewById(2131689993));
    this.mWifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean)
        {
          TestMain.this.wifiManager.setWifiEnabled(false);
          TestMain.this.text_wifi_test.setText("wifi关闭中");
        }
        for (;;)
        {
          return;
          TestMain.this.wifiManager.setWifiEnabled(true);
          TestMain.this.text_wifi_test.setText("wifi开启中");
        }
      }
    });
    wifiSelect();
  }
  
  protected void onPause()
  {
    super.onPause();
  }
  
  protected void onResume()
  {
    wifiSelect();
    super.onResume();
  }
}


