package com.yuneec.channelsettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.Toast;
import com.yuneec.curve.BrokenLineView;
import com.yuneec.curve.CurveView.OnCurveTouchListener;
import com.yuneec.curve.CurveView.onAllPointsUpdateListener;
import com.yuneec.curve.SplineView;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.ChannelMap;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.MixedData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.ButtonPicker;
import com.yuneec.widget.ButtonPicker.OnPickerListener;
import com.yuneec.widget.ButtonSeekBar;
import com.yuneec.widget.ButtonSeekBar.onButtonSeekChangeListener;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.OneButtonPopDialog;
import com.yuneec.widget.TwoButtonPopDialog;
import java.util.ArrayList;
import java.util.Arrays;

public class ThrottleCurveFragment
  extends Fragment
  implements ButtonSeekBar.onButtonSeekChangeListener
{
  private static final float[] CURVE_X = { -100.0F, -50.0F, 0.0F, 50.0F, 100.0F };
  private static final String TAG = "ThrottleCurveFragment";
  private static final float[] THR_DATA_DEFAULT_CURVE_POINTS = { 0.0F, 25.0F, 50.0F, 75.0F, 100.0F };
  private static final boolean THR_DATA_DEFAULT_EXPO = false;
  private static final String THR_DATA_DEFAULT_SW = "INH";
  private static final String THR_DATA_DEFAULT_THR_CUT_SW = "INH";
  private static final int THR_DATA_DEFAULT_THR_CUT_VALUE1 = 0;
  private static final int THR_DATA_DEFAULT_THR_CUT_VALUE2 = 50;
  private static MyProgressDialog mProgressDialog;
  private BrokenLineView mBlv;
  private int mCurrentSWstate = 0;
  private Switch mExpo_sw;
  private long mModel_id;
  private ButtonSeekBar mSeekPos1;
  private ButtonSeekBar mSeekPos2;
  private ButtonSeekBar mSeekPos3;
  private ButtonSeekBar mSeekPos4;
  private ButtonSeekBar mSeekPos5;
  private ServoData mServoData;
  private SplineView mSpv;
  private int mStickLastPosValue = -100;
  private ButtonPicker mSwitch_bp;
  private ButtonPicker mThrCut_bp;
  private ThrottleData mThrData = new ThrottleData();
  
  private void formatThrottleData()
  {
    for (int i = 0;; i++)
    {
      if (i >= 3)
      {
        this.mThrData.expo = false;
        this.mThrData.sw = "INH";
        this.mThrData.cut_sw = "INH";
        this.mThrData.cut_value1 = 0;
        this.mThrData.cut_value2 = 50;
        return;
      }
      this.mThrData.thrCurve[i].curvePoints[0] = THR_DATA_DEFAULT_CURVE_POINTS[0];
      this.mThrData.thrCurve[i].curvePoints[1] = THR_DATA_DEFAULT_CURVE_POINTS[1];
      this.mThrData.thrCurve[i].curvePoints[2] = THR_DATA_DEFAULT_CURVE_POINTS[2];
      this.mThrData.thrCurve[i].curvePoints[3] = THR_DATA_DEFAULT_CURVE_POINTS[3];
      this.mThrData.thrCurve[i].curvePoints[4] = THR_DATA_DEFAULT_CURVE_POINTS[4];
    }
  }
  
  private float[] getCurrentCurvePoints(int paramInt)
  {
    float[] arrayOfFloat = new float[5];
    int i;
    if ((paramInt >= 0) && (paramInt < 3))
    {
      i = 0;
      if (i < arrayOfFloat.length) {}
    }
    for (;;)
    {
      return arrayOfFloat;
      arrayOfFloat[i] = this.mThrData.thrCurve[paramInt].curvePoints[i];
      i++;
      break;
      Log.e("ThrottleCurveFragment", "getCurrentCurvePoints ----Invalid switch state");
    }
  }
  
  public static ArrayList<Integer> getCurvePoints(Context paramContext, ThrottleData.ThrCurve[] paramArrayOfThrCurve, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i >= 3) {
      return localArrayList;
    }
    if (paramBoolean) {}
    for (int[] arrayOfInt = SplineView.getAllPointsChValue(paramContext, 125.0F, -25.0F, CURVE_X, paramArrayOfThrCurve[i].curvePoints);; arrayOfInt = BrokenLineView.getAllPointsChValue(paramContext, 125.0F, -25.0F, CURVE_X, paramArrayOfThrCurve[i].curvePoints))
    {
      localArrayList.addAll(Arrays.asList(Utilities.get17PointsValueForFightControl(125.0F, -25.0F, arrayOfInt)));
      i++;
      break;
    }
  }
  
  private int getSwitchState()
  {
    this.mThrData.sw.equals("INH");
    return 0;
  }
  
  public static MixedData getThrCutData(ThrottleData paramThrottleData)
  {
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = Utilities.getFmodeState();
    localMixedData.mChannel = 1;
    localMixedData.mhardware = Utilities.getHardwareIndexT(paramThrottleData.cut_sw);
    localMixedData.mHardwareType = Utilities.getHardwareType(paramThrottleData.cut_sw);
    localMixedData.mPriority = 2;
    localMixedData.mDr_switch = 0;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(paramThrottleData.cut_value1));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(paramThrottleData.cut_value2));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    return localMixedData;
  }
  
  public static MixedData getThrottleData(Context paramContext, long paramLong, ThrottleData paramThrottleData)
  {
    ChannelMap[] arrayOfChannelMap = DataProviderHelper.readChannelMapFromDatabase(paramContext, paramLong);
    MixedData localMixedData = new MixedData();
    int i = Utilities.getFmodeState();
    localMixedData.mFmode = i;
    localMixedData.mChannel = 1;
    localMixedData.mhardware = Utilities.getHardwareIndexT(arrayOfChannelMap[0].hardware);
    localMixedData.mHardwareType = Utilities.getHardwareType(arrayOfChannelMap[0].hardware);
    localMixedData.mPriority = 1;
    localMixedData.mDr_switch = Utilities.getHardwareIndexT(paramThrottleData.sw);
    localMixedData.mMixedType = 0;
    localMixedData.mCurvePoint = getCurvePoints(paramContext, paramThrottleData.thrCurve, paramThrottleData.expo);
    paramContext = ServoSetupFragment.getServoSetup(DataProviderHelper.readServoDataFromDatabase(paramContext, paramLong, i), arrayOfChannelMap[0].function);
    if (paramContext != null)
    {
      localMixedData.mSpeed = paramContext.speed;
      localMixedData.mReverse = paramContext.reverse;
    }
    return localMixedData;
  }
  
  private void initThrottleData()
  {
    this.mModel_id = getActivity().getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    this.mThrData = DataProviderHelper.readThrDataFromDatabase(getActivity(), this.mModel_id, Utilities.getFmodeState());
  }
  
  private void initiation(View paramView)
  {
    this.mBlv = ((BrokenLineView)paramView.findViewById(2131689574));
    this.mBlv.setOnCurveTouchListener(new CurveView.OnCurveTouchListener()
    {
      public void onCurveValueChanged(int paramAnonymousInt)
      {
        ButtonSeekBar localButtonSeekBar = null;
        ThrottleCurveFragment.this.mThrData.thrCurve[ThrottleCurveFragment.this.mCurrentSWstate].curvePoints[paramAnonymousInt] = ThrottleCurveFragment.this.mBlv.getPointValue(paramAnonymousInt);
        switch (paramAnonymousInt)
        {
        default: 
          Log.e("ThrottleCurveFragment", "Invalid point index");
        }
        for (;;)
        {
          if (localButtonSeekBar != null) {
            localButtonSeekBar.setProgress((int)(ThrottleCurveFragment.this.mThrData.thrCurve[ThrottleCurveFragment.this.mCurrentSWstate].curvePoints[paramAnonymousInt] * 10.0F));
          }
          return;
          localButtonSeekBar = ThrottleCurveFragment.this.mSeekPos1;
          continue;
          localButtonSeekBar = ThrottleCurveFragment.this.mSeekPos2;
          continue;
          localButtonSeekBar = ThrottleCurveFragment.this.mSeekPos3;
          continue;
          localButtonSeekBar = ThrottleCurveFragment.this.mSeekPos4;
          continue;
          localButtonSeekBar = ThrottleCurveFragment.this.mSeekPos5;
        }
      }
      
      public void onCurveValueChanged(int paramAnonymousInt, float paramAnonymousFloat) {}
    });
    this.mSpv = ((SplineView)paramView.findViewById(2131689573));
    this.mSpv.setOnCurveTouchListener(new CurveView.OnCurveTouchListener()
    {
      public void onCurveValueChanged(int paramAnonymousInt)
      {
        ThrottleCurveFragment.this.mThrData.thrCurve[ThrottleCurveFragment.this.mCurrentSWstate].curvePoints[paramAnonymousInt] = ThrottleCurveFragment.this.mSpv.getPointValue(paramAnonymousInt);
      }
      
      public void onCurveValueChanged(int paramAnonymousInt, float paramAnonymousFloat) {}
    });
    this.mSpv.setOnAllPointsUpdateListener(new CurveView.onAllPointsUpdateListener()
    {
      public void onUpdated(int[] paramAnonymousArrayOfInt) {}
    });
    int j = getResources().getInteger(2131492866);
    int i = getResources().getInteger(2131492867);
    this.mBlv.setOutputValueMax(j);
    this.mBlv.setOutputValueMin(i);
    this.mSpv.setOutputValueMax(j);
    this.mSpv.setOutputValueMin(i);
    this.mSeekPos1 = ((ButtonSeekBar)paramView.findViewById(2131689575));
    this.mSeekPos1.setOnButtonSeekChangeListener(this);
    this.mSeekPos1.initiation(getString(2131296443), j * 10, i * 10);
    this.mSeekPos2 = ((ButtonSeekBar)paramView.findViewById(2131689576));
    this.mSeekPos2.setOnButtonSeekChangeListener(this);
    this.mSeekPos2.initiation(getString(2131296444), j * 10, i * 10);
    this.mSeekPos3 = ((ButtonSeekBar)paramView.findViewById(2131689577));
    this.mSeekPos3.setOnButtonSeekChangeListener(this);
    this.mSeekPos3.initiation(getString(2131296445), j * 10, i * 10);
    this.mSeekPos4 = ((ButtonSeekBar)paramView.findViewById(2131689578));
    this.mSeekPos4.setOnButtonSeekChangeListener(this);
    this.mSeekPos4.initiation(getString(2131296446), j * 10, i * 10);
    this.mSeekPos5 = ((ButtonSeekBar)paramView.findViewById(2131689579));
    this.mSeekPos5.setOnButtonSeekChangeListener(this);
    this.mSeekPos5.initiation(getString(2131296447), j * 10, i * 10);
    this.mExpo_sw = ((Switch)paramView.findViewById(2131689581));
    this.mExpo_sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        ThrottleCurveFragment.this.mThrData.expo = paramAnonymousBoolean;
        ThrottleCurveFragment.this.setCurve(ThrottleCurveFragment.this.mThrData.expo);
      }
    });
    this.mSwitch_bp = ((ButtonPicker)paramView.findViewById(2131689563));
    this.mSwitch_bp.initiation(new String[] { "INH", "S2", "S3", "S4" }, "INH");
    this.mSwitch_bp.setOnPickerListener(new ButtonPicker.OnPickerListener()
    {
      public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
      {
        ThrottleCurveFragment.this.mThrData.sw = paramAnonymousString;
        Log.d("ThrottleCurveFragment", "Switch = " + paramAnonymousString);
      }
    });
    this.mThrCut_bp = ((ButtonPicker)paramView.findViewById(2131689584));
    this.mThrCut_bp.initiation(new String[] { "INH", "B1", "B2" }, "INH");
    this.mThrCut_bp.setOnPickerListener(new ButtonPicker.OnPickerListener()
    {
      public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
      {
        ThrottleCurveFragment.this.mThrData.cut_sw = paramAnonymousString;
        Log.d("ThrottleCurveFragment", "Switch = " + paramAnonymousString);
      }
    });
    paramView.findViewById(2131689566).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ThrottleCurveFragment.this.resetData();
      }
    });
    paramView.findViewById(2131689565).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ThrottleCurveFragment.this.saveData();
        ThrottleCurveFragment.this.sendData();
      }
    });
  }
  
  private void refreshScreen()
  {
    setCurve(this.mThrData.expo);
    int i = getSwitchState();
    this.mSeekPos1.setProgress((int)(this.mThrData.thrCurve[i].curvePoints[0] * 10.0F));
    this.mSeekPos2.setProgress((int)(this.mThrData.thrCurve[i].curvePoints[1] * 10.0F));
    this.mSeekPos3.setProgress((int)(this.mThrData.thrCurve[i].curvePoints[2] * 10.0F));
    this.mSeekPos4.setProgress((int)(this.mThrData.thrCurve[i].curvePoints[3] * 10.0F));
    this.mSeekPos5.setProgress((int)(this.mThrData.thrCurve[i].curvePoints[4] * 10.0F));
    this.mExpo_sw.setChecked(this.mThrData.expo);
    this.mSwitch_bp.setStringValue(this.mThrData.sw);
    this.mThrCut_bp.setStringValue(this.mThrData.cut_sw);
  }
  
  private void resetData()
  {
    final TwoButtonPopDialog localTwoButtonPopDialog = new TwoButtonPopDialog(getActivity());
    localTwoButtonPopDialog.setTitle("Reset");
    localTwoButtonPopDialog.adjustHeight(380);
    localTwoButtonPopDialog.setMessage(getResources().getString(2131296461));
    localTwoButtonPopDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ThrottleCurveFragment.this.formatThrottleData();
        ThrottleCurveFragment.this.refreshScreen();
        localTwoButtonPopDialog.dismiss();
      }
    });
    localTwoButtonPopDialog.setNegativeButton(2131296277, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localTwoButtonPopDialog.cancel();
      }
    });
    localTwoButtonPopDialog.show();
  }
  
  private void saveData()
  {
    DataProviderHelper.writeThrDataToDatabase(getActivity(), this.mThrData);
  }
  
  private void sendData()
  {
    MixedData[] arrayOfMixedData;
    if (!this.mThrData.cut_sw.equals("INH"))
    {
      arrayOfMixedData = new MixedData[1];
      arrayOfMixedData[0] = getThrottleData(getActivity(), this.mModel_id, this.mThrData);
    }
    for (;;)
    {
      sendDataToTranslator(arrayOfMixedData);
      return;
      arrayOfMixedData = new MixedData[2];
      arrayOfMixedData[0] = getThrottleData(getActivity(), this.mModel_id, this.mThrData);
      arrayOfMixedData[1] = getThrCutData(this.mThrData);
    }
  }
  
  private void sendDataToTranslator(MixedData[] paramArrayOfMixedData)
  {
    mProgressDialog = MyProgressDialog.show(getActivity(), null, getResources().getString(2131296305), false, false);
    new SendDataTask(null).execute(paramArrayOfMixedData);
  }
  
  private void setCurve(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mSpv.setVisibility(0);
      this.mSpv.setParams(CURVE_X, getCurrentCurvePoints(this.mCurrentSWstate));
      this.mBlv.setVisibility(8);
    }
    for (;;)
    {
      return;
      this.mBlv.setVisibility(0);
      this.mBlv.setParams(CURVE_X, getCurrentCurvePoints(this.mCurrentSWstate));
      this.mSpv.setVisibility(8);
    }
  }
  
  private void showFailedToSendDialog()
  {
    final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(getActivity());
    localOneButtonPopDialog.setTitle("Failure");
    localOneButtonPopDialog.adjustHeight(380);
    localOneButtonPopDialog.setMessage(getResources().getString(2131296473));
    localOneButtonPopDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localOneButtonPopDialog.dismiss();
      }
    });
    localOneButtonPopDialog.show();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initThrottleData();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130903057, null);
    initiation(paramLayoutInflater);
    return paramLayoutInflater;
  }
  
  public void onPause()
  {
    super.onPause();
  }
  
  public void onProgressChanged(ButtonSeekBar paramButtonSeekBar, int paramInt, boolean paramBoolean)
  {
    int i = -1;
    if (paramBoolean)
    {
      if (!paramButtonSeekBar.equals(this.mSeekPos1)) {
        break label63;
      }
      i = 0;
    }
    for (;;)
    {
      if (i != -1)
      {
        this.mThrData.thrCurve[this.mCurrentSWstate].curvePoints[i] = (paramInt / 10.0F);
        setCurve(this.mThrData.expo);
      }
      return;
      label63:
      if (paramButtonSeekBar.equals(this.mSeekPos2)) {
        i = 1;
      } else if (paramButtonSeekBar.equals(this.mSeekPos3)) {
        i = 2;
      } else if (paramButtonSeekBar.equals(this.mSeekPos4)) {
        i = 3;
      } else if (paramButtonSeekBar.equals(this.mSeekPos5)) {
        i = 4;
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
    refreshScreen();
  }
  
  public void setHardwarePos(int paramInt, String paramString)
  {
    int i = paramInt;
    if (this.mServoData.reverse) {
      i = 4095 - paramInt;
    }
    if ((Math.abs(i - this.mStickLastPosValue) > 50) || ((i > 2037) && (i <= 2057) && ((this.mStickLastPosValue <= 2037) || (this.mStickLastPosValue > 2057))))
    {
      if (!this.mThrData.expo) {
        break label90;
      }
      this.mSpv.refreshStick(i);
    }
    for (;;)
    {
      this.mStickLastPosValue = i;
      return;
      label90:
      this.mBlv.refreshStick(i);
    }
  }
  
  public void setUserVisibleHint(boolean paramBoolean)
  {
    super.setUserVisibleHint(paramBoolean);
    if (paramBoolean) {
      this.mServoData = ServoSetupFragment.getServoSetup(DataProviderHelper.readServoDataFromDatabase(getActivity(), this.mModel_id, 0), "Thr");
    }
    for (;;)
    {
      return;
      this.mStickLastPosValue = -100;
    }
  }
  
  private class SendDataTask
    extends AsyncTask<MixedData, Void, Boolean>
  {
    private SendDataTask() {}
    
    protected Boolean doInBackground(MixedData... paramVarArgs)
    {
      boolean bool1 = true;
      boolean bool2 = true;
      UARTController localUARTController = ((ChannelSettings)ThrottleCurveFragment.this.getActivity()).getUARTController();
      int i;
      if (paramVarArgs != null)
      {
        i = 0;
        bool1 = bool2;
      }
      for (;;)
      {
        if (i >= paramVarArgs.length) {
          return Boolean.valueOf(bool1);
        }
        if (!localUARTController.syncMixingData(true, paramVarArgs[i], 1))
        {
          Log.e("ThrottleCurveFragment", "Failed to send Throttle data at " + i);
          bool1 = false;
        }
        i++;
      }
    }
    
    protected void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      ThrottleCurveFragment.mProgressDialog.dismiss();
      if (!paramBoolean.booleanValue()) {
        ThrottleCurveFragment.this.showFailedToSendDialog();
      }
      for (;;)
      {
        return;
        MyToast.makeText(ThrottleCurveFragment.this.getActivity(), ThrottleCurveFragment.this.getString(2131296462), 0, 1).show();
      }
    }
  }
}


