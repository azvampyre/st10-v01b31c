package com.yuneec.channelsettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import com.yuneec.curve.CurveView.OnCurveTouchListener;
import com.yuneec.curve.Expo2LineView;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.ChannelMap;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.MixedData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.widget.ButtonPicker;
import com.yuneec.widget.ButtonPicker.OnPickerListener;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.OneButtonPopDialog;
import com.yuneec.widget.TwoButtonPopDialog;
import java.util.ArrayList;
import java.util.Arrays;

public class DR_Fragment
  extends Fragment
{
  private static final float DR_DATA_DEFAULT_EXPO1 = 0.0F;
  private static final float DR_DATA_DEFAULT_EXPO2 = 0.0F;
  private static final float DR_DATA_DEFAULT_OFFSET = 0.0F;
  private static final float DR_DATA_DEFAULT_RATE1 = -100.0F;
  private static final float DR_DATA_DEFAULT_RATE2 = 100.0F;
  private static final String[] FUNCS = { "Ail", "Ele", "Rud" };
  private static final String TAG = "DR_Fragment";
  private int mCurrentSWstate;
  private DRData[] mDRData;
  private DualPickerGroup mDR_pg;
  private Expo2LineView mElv;
  private DualPickerGroup mExpo_pg;
  private RadioGroup mFuncGrp;
  private int mFuncIndex = 0;
  private long mModel_id;
  private MyProgressDialog mProgressDialog;
  private ServoData mServoData;
  private int mStickLastPosValue = -100;
  private ButtonPicker mSwitch_bp;
  
  private void formatDRData(DRData paramDRData)
  {
    paramDRData.curveparams[this.mCurrentSWstate].rate1 = -100.0F;
    paramDRData.curveparams[this.mCurrentSWstate].rate2 = 100.0F;
    paramDRData.curveparams[this.mCurrentSWstate].expo1 = 0.0F;
    paramDRData.curveparams[this.mCurrentSWstate].expo2 = 0.0F;
    paramDRData.curveparams[this.mCurrentSWstate].offset = 0.0F;
  }
  
  private static ArrayList<Integer> getCurvePoints(Context paramContext, DRData[] paramArrayOfDRData, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0;; i++)
    {
      if (i >= 3) {
        return localArrayList;
      }
      localArrayList.addAll(Arrays.asList(Utilities.get17PointsValueForFightControl(150.0F, -150.0F, Expo2LineView.getAllPointsChValue(paramContext, 150.0F, -150.0F, paramArrayOfDRData[paramInt].curveparams[i].rate1, paramArrayOfDRData[paramInt].curveparams[i].rate2, paramArrayOfDRData[paramInt].curveparams[i].expo1, paramArrayOfDRData[paramInt].curveparams[i].expo2, paramArrayOfDRData[paramInt].curveparams[i].offset))));
    }
  }
  
  public static MixedData[] getDRDatas(Context paramContext, long paramLong, DRData[] paramArrayOfDRData)
  {
    ChannelMap[] arrayOfChannelMap = DataProviderHelper.readChannelMapFromDatabase(paramContext, paramLong);
    ArrayList localArrayList = new ArrayList();
    int m = Utilities.getFmodeState();
    int k = 0;
    int i = 0;
    if (i >= arrayOfChannelMap.length) {
      paramContext = new MixedData[localArrayList.size()];
    }
    for (i = 0;; i++)
    {
      if (i >= paramContext.length)
      {
        return paramContext;
        MixedData localMixedData = new MixedData();
        if (arrayOfChannelMap[i].alias.contains(FUNCS[0]))
        {
          localMixedData.mDr_switch = Utilities.getHardwareIndexT(paramArrayOfDRData[0].sw);
          localMixedData.mCurvePoint = getCurvePoints(paramContext, paramArrayOfDRData, 0);
        }
        for (;;)
        {
          label109:
          localMixedData.mFmode = m;
          localMixedData.mChannel = arrayOfChannelMap[i].channel;
          localMixedData.mhardware = Utilities.getHardwareIndexT(arrayOfChannelMap[i].hardware);
          localMixedData.mHardwareType = Utilities.getHardwareType(arrayOfChannelMap[i].hardware);
          localMixedData.mPriority = 1;
          ServoData localServoData = ServoSetupFragment.getServoSetup(DataProviderHelper.readServoDataFromDatabase(paramContext, paramLong, m), arrayOfChannelMap[i].function);
          if (localServoData != null)
          {
            localMixedData.mSpeed = localServoData.speed;
            localMixedData.mReverse = localServoData.reverse;
          }
          localArrayList.add(k, localMixedData);
          int j = k + 1;
          do
          {
            i++;
            k = j;
            break;
            if (arrayOfChannelMap[i].alias.contains(FUNCS[1]))
            {
              localMixedData.mDr_switch = Utilities.getHardwareIndexT(paramArrayOfDRData[1].sw);
              localMixedData.mCurvePoint = getCurvePoints(paramContext, paramArrayOfDRData, 1);
              break label109;
            }
            j = k;
          } while (!arrayOfChannelMap[i].alias.contains(FUNCS[2]));
          localMixedData.mDr_switch = Utilities.getHardwareIndexT(paramArrayOfDRData[2].sw);
          localMixedData.mCurvePoint = getCurvePoints(paramContext, paramArrayOfDRData, 2);
        }
      }
      paramContext[i] = ((MixedData)localArrayList.get(i));
    }
  }
  
  private void initDRData()
  {
    this.mModel_id = getActivity().getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    this.mDRData = DataProviderHelper.readDRDataFromDatabase(getActivity(), this.mModel_id, Utilities.getFmodeState());
  }
  
  private void initiation(View paramView)
  {
    this.mFuncGrp = ((RadioGroup)paramView.findViewById(2131689554));
    this.mFuncGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        switch (paramAnonymousInt)
        {
        default: 
          DR_Fragment.this.mFuncIndex = 0;
        }
        for (;;)
        {
          DR_Fragment.this.refreshScreen(DR_Fragment.this.mFuncIndex);
          return;
          DR_Fragment.this.mFuncIndex = 0;
          continue;
          DR_Fragment.this.mFuncIndex = 1;
          continue;
          DR_Fragment.this.mFuncIndex = 2;
        }
      }
    });
    this.mElv = ((Expo2LineView)paramView.findViewById(2131689552));
    this.mElv.setOnCurveTouchListener(new CurveView.OnCurveTouchListener()
    {
      public void onCurveValueChanged(int paramAnonymousInt) {}
      
      public void onCurveValueChanged(int paramAnonymousInt, float paramAnonymousFloat)
      {
        switch (paramAnonymousInt)
        {
        }
        for (;;)
        {
          return;
          paramAnonymousFloat = DR_Fragment.this.mElv.convertCurveValueToUserValueL(paramAnonymousFloat);
          DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].rate1 = paramAnonymousFloat;
          DR_Fragment.this.mDR_pg.setValue1(paramAnonymousFloat);
          continue;
          paramAnonymousFloat = DR_Fragment.this.mElv.convertCurveValueToUserValueR(paramAnonymousFloat);
          DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].rate2 = paramAnonymousFloat;
          DR_Fragment.this.mDR_pg.setValue2(paramAnonymousFloat);
          continue;
          paramAnonymousFloat = DR_Fragment.this.mElv.getLeftExpoValue(paramAnonymousFloat);
          DRData.CurveParams localCurveParams = DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate];
          DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].expo2 = paramAnonymousFloat;
          localCurveParams.expo1 = paramAnonymousFloat;
          DR_Fragment.this.mExpo_pg.setValue1(paramAnonymousFloat);
          DR_Fragment.this.mExpo_pg.setValue2(paramAnonymousFloat);
        }
      }
    });
    this.mExpo_pg = ((DualPickerGroup)paramView.findViewById(2131689558));
    this.mExpo_pg.initation(100.0F, -100.0F, 0.0F, 0.0F, 0.1F);
    this.mExpo_pg.setOnDualPickerListener(new DualPickerGroup.OnDualPickerListener()
    {
      public void onClicked(float paramAnonymousFloat1, float paramAnonymousFloat2)
      {
        DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].expo1 = paramAnonymousFloat1;
        DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].expo2 = paramAnonymousFloat2;
        DR_Fragment.this.setCurve(DR_Fragment.this.mFuncIndex);
      }
    });
    this.mDR_pg = ((DualPickerGroup)paramView.findViewById(2131689559));
    this.mDR_pg.initation(150.0F, -150.0F, 0.0F, 0.0F, 0.1F);
    this.mDR_pg.setOnDualPickerListener(new DualPickerGroup.OnDualPickerListener()
    {
      public void onClicked(float paramAnonymousFloat1, float paramAnonymousFloat2)
      {
        DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].rate1 = paramAnonymousFloat1;
        DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].curveparams[DR_Fragment.this.mCurrentSWstate].rate2 = paramAnonymousFloat2;
        DR_Fragment.this.setCurve(DR_Fragment.this.mFuncIndex);
      }
    });
    this.mSwitch_bp = ((ButtonPicker)paramView.findViewById(2131689563));
    this.mSwitch_bp.initiation(new String[] { "INH", "S2", "S3", "S4" }, "INH");
    this.mSwitch_bp.setOnPickerListener(new ButtonPicker.OnPickerListener()
    {
      public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
      {
        DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex].sw = paramAnonymousString;
      }
    });
    paramView.findViewById(2131689566).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        DR_Fragment.this.resetData();
      }
    });
    paramView.findViewById(2131689565).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        DR_Fragment.this.saveData();
        DR_Fragment.this.sendData();
      }
    });
  }
  
  private void refreshScreen(int paramInt)
  {
    setCurve(paramInt);
    this.mExpo_pg.setValue1(this.mDRData[paramInt].curveparams[this.mCurrentSWstate].expo1);
    this.mExpo_pg.setValue2(this.mDRData[paramInt].curveparams[this.mCurrentSWstate].expo2);
    this.mDR_pg.setValue1(this.mDRData[paramInt].curveparams[this.mCurrentSWstate].rate1);
    this.mDR_pg.setValue2(this.mDRData[paramInt].curveparams[this.mCurrentSWstate].rate2);
    this.mSwitch_bp.setStringValue(this.mDRData[paramInt].sw);
  }
  
  private void resetData()
  {
    final TwoButtonPopDialog localTwoButtonPopDialog = new TwoButtonPopDialog(getActivity());
    localTwoButtonPopDialog.setTitle("Reset");
    localTwoButtonPopDialog.adjustHeight(380);
    localTwoButtonPopDialog.setMessage(getResources().getString(2131296461));
    localTwoButtonPopDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        DR_Fragment.this.formatDRData(DR_Fragment.this.mDRData[DR_Fragment.this.mFuncIndex]);
        DR_Fragment.this.refreshScreen(DR_Fragment.this.mFuncIndex);
        localTwoButtonPopDialog.dismiss();
      }
    });
    localTwoButtonPopDialog.setNegativeButton(2131296277, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localTwoButtonPopDialog.cancel();
      }
    });
    localTwoButtonPopDialog.show();
  }
  
  private void saveData()
  {
    DataProviderHelper.writeDRDataToDatabase(getActivity(), this.mDRData);
  }
  
  private void sendData()
  {
    sendDataToTranslator(getDRDatas(getActivity(), this.mModel_id, this.mDRData));
  }
  
  private void sendDataToTranslator(MixedData[] paramArrayOfMixedData)
  {
    this.mProgressDialog = MyProgressDialog.show(getActivity(), null, getResources().getString(2131296305), false, false);
    new SendDataTask(null).execute(paramArrayOfMixedData);
  }
  
  private void setCurve(int paramInt)
  {
    this.mElv.setParams(this.mDRData[paramInt].curveparams[this.mCurrentSWstate].rate1, this.mDRData[paramInt].curveparams[this.mCurrentSWstate].rate2, this.mDRData[paramInt].curveparams[this.mCurrentSWstate].expo1, this.mDRData[paramInt].curveparams[this.mCurrentSWstate].expo2, this.mDRData[paramInt].curveparams[this.mCurrentSWstate].offset);
  }
  
  private void showFailedToSendDialog()
  {
    final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(getActivity());
    localOneButtonPopDialog.setTitle("Failure");
    localOneButtonPopDialog.adjustHeight(380);
    localOneButtonPopDialog.setMessage(getResources().getString(2131296473));
    localOneButtonPopDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localOneButtonPopDialog.dismiss();
      }
    });
    localOneButtonPopDialog.show();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initDRData();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130903055, null);
    initiation(paramLayoutInflater);
    return paramLayoutInflater;
  }
  
  public void onPause()
  {
    super.onPause();
  }
  
  public void onResume()
  {
    super.onResume();
    refreshScreen(this.mFuncIndex);
  }
  
  public void setHardwarePos(UARTInfoMessage.Channel paramChannel, ChannelMap[] paramArrayOfChannelMap)
  {
    paramArrayOfChannelMap = paramArrayOfChannelMap[(this.mFuncIndex + 1)].hardware;
    int i = ChannelSettings.getHardwareIndex(getActivity(), paramArrayOfChannelMap);
    if ((i >= 0) && (i < paramChannel.channels.size()))
    {
      int j = ((Float)paramChannel.channels.get(i)).intValue();
      i = j;
      if (this.mServoData.reverse) {
        i = 4095 - j;
      }
      if ((Math.abs(i - this.mStickLastPosValue) > 50) || ((i > 2037) && (i <= 2057) && ((this.mStickLastPosValue <= 2037) || (this.mStickLastPosValue > 2057))))
      {
        this.mElv.refreshStick(i);
        this.mStickLastPosValue = i;
      }
    }
    for (;;)
    {
      return;
      Log.e("DR_Fragment", "Can't get hardware value");
    }
  }
  
  public void setUserVisibleHint(boolean paramBoolean)
  {
    super.setUserVisibleHint(paramBoolean);
    if (paramBoolean) {
      this.mServoData = ServoSetupFragment.getServoSetup(DataProviderHelper.readServoDataFromDatabase(getActivity(), this.mModel_id, 0), FUNCS[this.mFuncIndex]);
    }
    for (;;)
    {
      return;
      this.mStickLastPosValue = -100;
    }
  }
  
  private class SendDataTask
    extends AsyncTask<MixedData, Void, Boolean>
  {
    private SendDataTask() {}
    
    protected Boolean doInBackground(MixedData... paramVarArgs)
    {
      boolean bool = true;
      UARTController localUARTController = ((ChannelSettings)DR_Fragment.this.getActivity()).getUARTController();
      for (int i = 0;; i++)
      {
        if (i >= paramVarArgs.length) {
          return Boolean.valueOf(bool);
        }
        if (!localUARTController.syncMixingData(true, paramVarArgs[i], 0)) {
          bool = false;
        }
      }
    }
    
    protected void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      DR_Fragment.this.mProgressDialog.dismiss();
      if (!paramBoolean.booleanValue()) {
        DR_Fragment.this.showFailedToSendDialog();
      }
      for (;;)
      {
        return;
        MyToast.makeText(DR_Fragment.this.getActivity(), DR_Fragment.this.getString(2131296462), 0, 1).show();
      }
    }
  }
}


