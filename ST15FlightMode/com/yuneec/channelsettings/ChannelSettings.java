package com.yuneec.channelsettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.ChannelMap;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.widget.MyToast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChannelSettings
  extends FragmentActivity
{
  public static final int CURVE_TYPE_BROKENLINE = 3;
  public static final int CURVE_TYPE_EXPO_1 = 1;
  public static final int CURVE_TYPE_EXPO_2 = 2;
  public static final int CURVE_TYPE_SPLINE = 4;
  public static final int STICK_RATE_0_OR_M100 = 683;
  public static final int STICK_RATE_100_OR_100 = 3412;
  public static final int STICK_RATE_125_OR_150 = 4095;
  public static final int STICK_RATE_50_OR_0 = 2048;
  public static final int STICK_RATE_M25_OR_M150 = 0;
  private static final String TAG = "FlightSettings";
  private ChannelMap[] mChMap;
  private UARTController mController;
  private DR_Fragment mDRFrgmt;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (ChannelSettings.this.mController == null) {
        Log.i("FlightSettings", "UARTController is null");
      }
      for (;;)
      {
        return;
        if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
        {
          paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
          switch (paramAnonymousMessage.what)
          {
          case 4: 
          case 5: 
          case 14: 
          default: 
            break;
          case 2: 
            paramAnonymousMessage = (UARTInfoMessage.Channel)paramAnonymousMessage;
            if ((ChannelSettings.this.mThrFrgmt != null) && (ChannelSettings.this.mThrFrgmt.getUserVisibleHint()))
            {
              int i = ChannelSettings.getHardwareIndex(ChannelSettings.this, ChannelSettings.this.mChMap[0].hardware);
              if ((i >= 0) && (i < paramAnonymousMessage.channels.size()))
              {
                i = ((Float)paramAnonymousMessage.channels.get(i)).intValue();
                ChannelSettings.this.mThrFrgmt.setHardwarePos(i, ChannelSettings.this.mChMap[0].function);
              }
              else
              {
                Log.e("FlightSettings", "Can't get hardware value");
              }
            }
            else if ((ChannelSettings.this.mDRFrgmt != null) && (ChannelSettings.this.mDRFrgmt.getUserVisibleHint()))
            {
              ChannelSettings.this.mDRFrgmt.setHardwarePos(paramAnonymousMessage, ChannelSettings.this.mChMap);
            }
            break;
          }
        }
      }
    }
  };
  private long mModel_id;
  private ServoSetupFragment mSrvFrgmt;
  private ThrottleCurveFragment mThrFrgmt;
  private ViewPager mViewPager;
  
  public static int getHardwareIndex(Context paramContext, String paramString)
  {
    String[] arrayOfString = null;
    if ("ST10".equals("ST15")) {
      arrayOfString = paramContext.getResources().getStringArray(2131361801);
    }
    for (;;)
    {
      return Arrays.asList(arrayOfString).indexOf(paramString);
      if ("ST10".equals("ST10")) {
        arrayOfString = paramContext.getResources().getStringArray(2131361802);
      } else if ("ST10".equals("ST12")) {
        arrayOfString = paramContext.getResources().getStringArray(2131361803);
      }
    }
  }
  
  public DR_Fragment getDRFrgmt()
  {
    return this.mDRFrgmt;
  }
  
  public ServoSetupFragment getSrvFrgmt()
  {
    return this.mSrvFrgmt;
  }
  
  public ThrottleCurveFragment getThrFrgmt()
  {
    return this.mThrFrgmt;
  }
  
  public UARTController getUARTController()
  {
    return this.mController;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(Integer.MIN_VALUE);
    getWindow().addFlags(128);
    setContentView(2130903053);
    this.mViewPager = ((ViewPager)findViewById(2131689541));
    this.mViewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
    this.mModel_id = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    this.mChMap = DataProviderHelper.readChannelMapFromDatabase(this, this.mModel_id);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 3) && (!paramKeyEvent.isCanceled())) {
      Utilities.backToFlightScreen(this);
    }
    for (boolean bool = true;; bool = super.onKeyUp(paramInt, paramKeyEvent)) {
      return bool;
    }
  }
  
  protected void onPause()
  {
    super.onPause();
    if ((!Utilities.isRunningMode()) && (!Utilities.ensureAwaitState(this.mController))) {
      Log.e("FlightSettings", "fail to change to await");
    }
    Utilities.UartControllerStandBy(this.mController);
    this.mController = null;
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    if (this.mController == null) {
      Log.e("FlightSettings", "UARTController is null");
    }
    for (;;)
    {
      return;
      this.mController.registerReaderHandler(this.mHandler);
      this.mController.startReading();
      if ((!Utilities.isRunningMode()) && (!Utilities.ensureSimState(this.mController))) {
        MyToast.makeText(this, getString(2131296306), 0, 0);
      }
    }
  }
  
  class FragmentAdapter
    extends FragmentStatePagerAdapter
  {
    public FragmentAdapter(FragmentManager paramFragmentManager)
    {
      super();
    }
    
    public int getCount()
    {
      return 3;
    }
    
    public Fragment getItem(int paramInt)
    {
      Object localObject;
      switch (paramInt)
      {
      default: 
        localObject = null;
      }
      for (;;)
      {
        return (Fragment)localObject;
        ChannelSettings.this.mThrFrgmt = ((ThrottleCurveFragment)Fragment.instantiate(ChannelSettings.this, "com.yuneec.channelsettings.ThrottleCurveFragment"));
        localObject = ChannelSettings.this.mThrFrgmt;
        continue;
        ChannelSettings.this.mDRFrgmt = ((DR_Fragment)Fragment.instantiate(ChannelSettings.this, "com.yuneec.channelsettings.DR_Fragment"));
        localObject = ChannelSettings.this.mDRFrgmt;
        continue;
        ChannelSettings.this.mSrvFrgmt = ((ServoSetupFragment)Fragment.instantiate(ChannelSettings.this, "com.yuneec.channelsettings.ServoSetupFragment"));
        localObject = ChannelSettings.this.mSrvFrgmt;
      }
    }
    
    public CharSequence getPageTitle(int paramInt)
    {
      Object localObject;
      switch (paramInt)
      {
      default: 
        localObject = null;
      }
      for (;;)
      {
        return (CharSequence)localObject;
        localObject = "Throttle Curve";
        continue;
        localObject = "D/R";
        continue;
        localObject = "Servo Setup";
      }
    }
  }
}


