package com.yuneec.channelsettings;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import com.yuneec.widget.ButtonPicker;
import com.yuneec.widget.ButtonPicker.OnPickerListener;

public class DualPickerGroup
  extends LinearLayout
  implements View.OnClickListener, View.OnLongClickListener
{
  private static final String TAG = "DualButtonPicker";
  private Runnable longClickRunnable = new Runnable()
  {
    public void run()
    {
      DualPickerGroup.this.clickEvent(DualPickerGroup.this.mClickedButton);
      if (DualPickerGroup.this.mClickedButton.isPressed()) {
        DualPickerGroup.this.mHandler.postDelayed(DualPickerGroup.this.longClickRunnable, 5L);
      }
      for (;;)
      {
        return;
        DualPickerGroup.this.mHandler.removeCallbacks(DualPickerGroup.this.longClickRunnable);
        DualPickerGroup.this.mClickedButton = null;
      }
    }
  };
  private ButtonPicker mBp_bottom;
  private ButtonPicker mBp_top;
  private Button mClickedButton;
  private OnDualPickerListener mDualPickerListener;
  private Handler mHandler = new Handler();
  private Button mLeftButton;
  private float mMax;
  private float mMin;
  private Button mRightButton;
  private float mStep;
  private float mValue1;
  private float mValue2;
  
  public DualPickerGroup(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public DualPickerGroup(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public DualPickerGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903059, this);
  }
  
  private void clickEvent(Button paramButton)
  {
    float f;
    if (paramButton.equals(this.mLeftButton))
    {
      f = this.mValue1 + this.mStep;
      if (f >= this.mMin)
      {
        this.mValue1 = f;
        this.mBp_top.setFloatValue(f);
      }
      f = this.mValue2 + this.mStep;
      if (f >= this.mMin)
      {
        this.mValue2 = f;
        this.mBp_bottom.setFloatValue(f);
      }
    }
    for (;;)
    {
      this.mDualPickerListener.onClicked(this.mValue1, this.mValue2);
      return;
      if (paramButton.equals(this.mRightButton))
      {
        f = this.mValue1 - this.mStep;
        if (f <= this.mMax)
        {
          this.mValue1 = f;
          this.mBp_top.setFloatValue(f);
        }
        f = this.mValue2 - this.mStep;
        if (f <= this.mMax)
        {
          this.mValue2 = f;
          this.mBp_bottom.setFloatValue(f);
        }
      }
    }
  }
  
  public float getValue1()
  {
    return this.mValue1;
  }
  
  public float getValue2()
  {
    return this.mValue2;
  }
  
  public void initation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
  {
    this.mMax = paramFloat1;
    this.mMin = paramFloat2;
    this.mValue1 = paramFloat3;
    this.mValue2 = paramFloat4;
    this.mStep = paramFloat5;
    this.mBp_top.initiation(paramFloat1, paramFloat2, paramFloat3, paramFloat5);
    this.mBp_bottom.initiation(paramFloat1, paramFloat2, paramFloat4, paramFloat5);
  }
  
  public void onClick(View paramView)
  {
    clickEvent((Button)paramView);
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mLeftButton = ((Button)findViewById(2131689587));
    this.mLeftButton.setOnClickListener(this);
    this.mLeftButton.setOnLongClickListener(this);
    this.mRightButton = ((Button)findViewById(2131689590));
    this.mRightButton.setOnClickListener(this);
    this.mRightButton.setOnLongClickListener(this);
    this.mBp_top = ((ButtonPicker)findViewById(2131689588));
    this.mBp_top.setOnPickerListener(new ButtonPicker.OnPickerListener()
    {
      public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
      {
        DualPickerGroup.this.mValue1 = DualPickerGroup.this.mBp_top.getFloatValue();
        DualPickerGroup.this.mDualPickerListener.onClicked(DualPickerGroup.this.mValue1, DualPickerGroup.this.mValue2);
      }
    });
    this.mBp_bottom = ((ButtonPicker)findViewById(2131689589));
    this.mBp_bottom.setOnPickerListener(new ButtonPicker.OnPickerListener()
    {
      public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
      {
        DualPickerGroup.this.mValue2 = DualPickerGroup.this.mBp_bottom.getFloatValue();
        DualPickerGroup.this.mDualPickerListener.onClicked(DualPickerGroup.this.mValue1, DualPickerGroup.this.mValue2);
      }
    });
  }
  
  public boolean onLongClick(View paramView)
  {
    this.mClickedButton = ((Button)paramView);
    this.mHandler.post(this.longClickRunnable);
    return true;
  }
  
  public void setOnDualPickerListener(OnDualPickerListener paramOnDualPickerListener)
  {
    this.mDualPickerListener = paramOnDualPickerListener;
  }
  
  public void setValue1(float paramFloat)
  {
    this.mValue1 = paramFloat;
    this.mBp_top.setFloatValue(paramFloat);
  }
  
  public void setValue2(float paramFloat)
  {
    this.mValue2 = paramFloat;
    this.mBp_bottom.setFloatValue(paramFloat);
  }
  
  public static abstract interface OnDualPickerListener
  {
    public abstract void onClicked(float paramFloat1, float paramFloat2);
  }
}


