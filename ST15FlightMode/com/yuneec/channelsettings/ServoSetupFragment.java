package com.yuneec.channelsettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.Toast;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.ChannelMap;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.MixedData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.ButtonPicker;
import com.yuneec.widget.ButtonPicker.OnPickerListener;
import com.yuneec.widget.ButtonSeekBar;
import com.yuneec.widget.ButtonSeekBar.onButtonSeekChangeListener;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.OneButtonPopDialog;
import com.yuneec.widget.TwoButtonPopDialog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServoSetupFragment
  extends Fragment
  implements ButtonPicker.OnPickerListener
{
  private static ServoData SER_DATA_DEFAULT = new ServoData();
  private static final String TAG = "ServoSetupFragment";
  private String[] mAllFunctions;
  private ButtonPicker mFunc_bp;
  private int mIndex;
  private long mModel_id;
  private MyProgressDialog mProgressDialog;
  private Switch mReverse_sw;
  private ServoData[] mServoData;
  private ButtonPicker mSpeed_bp;
  private ButtonSeekBar mSubTrim_bp;
  private ButtonPicker mTravelL_bp;
  private ButtonPicker mTravelR_bp;
  
  static
  {
    SER_DATA_DEFAULT.subTrim = 0;
    SER_DATA_DEFAULT.reverse = false;
    SER_DATA_DEFAULT.speed = 10;
    SER_DATA_DEFAULT.travelL = -100;
    SER_DATA_DEFAULT.travelR = 100;
  }
  
  private void formatServoData(ServoData paramServoData, int paramInt)
  {
    paramServoData.func = this.mAllFunctions[paramInt];
    paramServoData.subTrim = SER_DATA_DEFAULT.subTrim;
    paramServoData.reverse = SER_DATA_DEFAULT.reverse;
    paramServoData.speed = SER_DATA_DEFAULT.speed;
    paramServoData.travelL = SER_DATA_DEFAULT.travelL;
    paramServoData.travelR = SER_DATA_DEFAULT.travelR;
  }
  
  private MixedData[] getAllServoSetup(Context paramContext)
  {
    Object localObject = DataProviderHelper.readChannelMapFromDatabase(paramContext, this.mModel_id);
    int j = Utilities.getFmodeState();
    MixedData[] arrayOfMixedData = new MixedData[this.mServoData.length];
    int i = 0;
    if (i >= arrayOfMixedData.length)
    {
      localObject = DataProviderHelper.readThrDataFromDatabase(paramContext, this.mModel_id, j);
      arrayOfMixedData[0] = ThrottleCurveFragment.getThrottleData(paramContext, this.mModel_id, (ThrottleData)localObject);
      localObject = DataProviderHelper.readDRDataFromDatabase(paramContext, this.mModel_id, j);
      paramContext = DR_Fragment.getDRDatas(paramContext, this.mModel_id, (DRData[])localObject);
    }
    for (i = 0;; i++)
    {
      if (i >= paramContext.length)
      {
        return arrayOfMixedData;
        arrayOfMixedData[i] = new MixedData();
        arrayOfMixedData[i].mFmode = j;
        arrayOfMixedData[i].mChannel = (i + 1);
        arrayOfMixedData[i].mhardware = Utilities.getHardwareIndexT(localObject[i].hardware);
        arrayOfMixedData[i].mHardwareType = Utilities.getHardwareType(localObject[i].hardware);
        arrayOfMixedData[i].mPriority = 1;
        ServoData localServoData = getServoSetup(this.mServoData, localObject[i].function);
        if (localServoData != null)
        {
          arrayOfMixedData[i].mSpeed = localServoData.speed;
          arrayOfMixedData[i].mReverse = localServoData.reverse;
        }
        i++;
        break;
      }
      arrayOfMixedData[(paramContext[i].mChannel - 1)] = paramContext[i];
    }
  }
  
  public static ServoData getServoSetup(ServoData[] paramArrayOfServoData, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i >= paramArrayOfServoData.length)
    {
      i = localArrayList.indexOf(paramString);
      if (i == -1) {
        break label50;
      }
    }
    for (paramArrayOfServoData = paramArrayOfServoData[i];; paramArrayOfServoData = null)
    {
      return paramArrayOfServoData;
      localArrayList.add(paramArrayOfServoData[i].func);
      i++;
      break;
      label50:
      Log.e("ServoSetupFragment", "getServoSetup----Invalid functions");
    }
  }
  
  private void initServoData()
  {
    this.mAllFunctions = getResources().getStringArray(2131361797);
    this.mModel_id = getActivity().getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    this.mServoData = DataProviderHelper.readServoDataFromDatabase(getActivity(), this.mModel_id, Utilities.getFmodeState());
    if (this.mServoData.length != this.mAllFunctions.length) {
      Log.e("ServoSetupFragment", "Exceptional servo data");
    }
  }
  
  private void initiation(View paramView)
  {
    this.mFunc_bp = ((ButtonPicker)paramView.findViewById(2131689567));
    this.mFunc_bp.setOnPickerListener(this);
    this.mFunc_bp.initiation(this.mAllFunctions, "Thr");
    this.mSubTrim_bp = ((ButtonSeekBar)paramView.findViewById(2131689568));
    this.mSubTrim_bp.setOnButtonSeekChangeListener(new ButtonSeekBar.onButtonSeekChangeListener()
    {
      public void onProgressChanged(ButtonSeekBar paramAnonymousButtonSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
      {
        ServoSetupFragment.this.mServoData[ServoSetupFragment.this.mIndex].subTrim = paramAnonymousInt;
      }
    });
    this.mSubTrim_bp.initiation(null, 10, -10);
    this.mReverse_sw = ((Switch)paramView.findViewById(2131689569));
    this.mReverse_sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        ServoSetupFragment.this.mServoData[ServoSetupFragment.this.mIndex].reverse = paramAnonymousBoolean;
      }
    });
    this.mSpeed_bp = ((ButtonPicker)paramView.findViewById(2131689570));
    this.mSpeed_bp.setOnPickerListener(this);
    this.mSpeed_bp.initiation(30, 10, 10, 5);
    this.mTravelL_bp = ((ButtonPicker)paramView.findViewById(2131689571));
    this.mTravelL_bp.setOnPickerListener(this);
    this.mTravelL_bp.initiation(-30, 65386, -100, 1);
    this.mTravelR_bp = ((ButtonPicker)paramView.findViewById(2131689572));
    this.mTravelR_bp.setOnPickerListener(this);
    this.mTravelR_bp.initiation(150, 30, 100, 1);
    paramView.findViewById(2131689566).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ServoSetupFragment.this.resetData();
      }
    });
    paramView.findViewById(2131689565).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ServoSetupFragment.this.saveData();
        ServoSetupFragment.this.sendData();
      }
    });
  }
  
  private void refreshScreen(int paramInt)
  {
    this.mSubTrim_bp.setProgress(this.mServoData[paramInt].subTrim);
    this.mReverse_sw.setChecked(this.mServoData[paramInt].reverse);
    this.mSpeed_bp.setIntegerValue(this.mServoData[paramInt].speed);
    this.mTravelL_bp.setIntegerValue(this.mServoData[paramInt].travelL);
    this.mTravelR_bp.setIntegerValue(this.mServoData[paramInt].travelR);
  }
  
  private void resetData()
  {
    final TwoButtonPopDialog localTwoButtonPopDialog = new TwoButtonPopDialog(getActivity());
    localTwoButtonPopDialog.setTitle("Reset");
    localTwoButtonPopDialog.adjustHeight(380);
    localTwoButtonPopDialog.setMessage(getResources().getString(2131296461));
    localTwoButtonPopDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ServoSetupFragment.this.formatServoData(ServoSetupFragment.this.mServoData[ServoSetupFragment.this.mIndex], ServoSetupFragment.this.mIndex);
        ServoSetupFragment.this.refreshScreen(ServoSetupFragment.this.mIndex);
        localTwoButtonPopDialog.dismiss();
      }
    });
    localTwoButtonPopDialog.setNegativeButton(2131296277, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localTwoButtonPopDialog.cancel();
      }
    });
    localTwoButtonPopDialog.show();
  }
  
  private void saveData()
  {
    DataProviderHelper.writeServoDataToDatabase(getActivity(), this.mServoData);
  }
  
  private void sendData()
  {
    sendDataToTranslator(getAllServoSetup(getActivity()));
  }
  
  private void sendDataToTranslator(MixedData[] paramArrayOfMixedData)
  {
    this.mProgressDialog = MyProgressDialog.show(getActivity(), null, getResources().getString(2131296305), false, false);
    new SendDataTask(null).execute(paramArrayOfMixedData);
  }
  
  private void showFailedToSendDialog()
  {
    final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(getActivity());
    localOneButtonPopDialog.setTitle("Failure");
    localOneButtonPopDialog.adjustHeight(380);
    localOneButtonPopDialog.setMessage(getResources().getString(2131296473));
    localOneButtonPopDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localOneButtonPopDialog.dismiss();
      }
    });
    localOneButtonPopDialog.show();
  }
  
  public void onClicked(ButtonPicker paramButtonPicker, String paramString)
  {
    if (paramButtonPicker.equals(this.mFunc_bp))
    {
      this.mIndex = Arrays.asList(this.mAllFunctions).indexOf(paramString);
      refreshScreen(this.mIndex);
    }
    for (;;)
    {
      return;
      if (paramButtonPicker.equals(this.mSpeed_bp)) {
        this.mServoData[this.mIndex].speed = Integer.valueOf(paramString).intValue();
      } else if (paramButtonPicker.equals(this.mTravelL_bp)) {
        this.mServoData[this.mIndex].travelL = Integer.valueOf(paramString).intValue();
      } else if (paramButtonPicker.equals(this.mTravelR_bp)) {
        this.mServoData[this.mIndex].travelR = Integer.valueOf(paramString).intValue();
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initServoData();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130903056, null);
    initiation(paramLayoutInflater);
    return paramLayoutInflater;
  }
  
  public void onPause()
  {
    super.onPause();
  }
  
  public void onResume()
  {
    super.onResume();
    refreshScreen(this.mIndex);
  }
  
  public void setUserVisibleHint(boolean paramBoolean)
  {
    super.setUserVisibleHint(paramBoolean);
  }
  
  private class SendDataTask
    extends AsyncTask<MixedData, Void, Boolean>
  {
    private SendDataTask() {}
    
    protected Boolean doInBackground(MixedData... paramVarArgs)
    {
      boolean bool = true;
      UARTController localUARTController = ((ChannelSettings)ServoSetupFragment.this.getActivity()).getUARTController();
      for (int i = 0;; i++)
      {
        if (i >= paramVarArgs.length) {
          return Boolean.valueOf(bool);
        }
        if (!localUARTController.syncMixingData(true, paramVarArgs[i], 1))
        {
          Log.e("ServoSetupFragment", "Failed to send Servo data at " + i);
          bool = false;
        }
      }
    }
    
    protected void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      ServoSetupFragment.this.mProgressDialog.dismiss();
      if (!paramBoolean.booleanValue()) {
        ServoSetupFragment.this.showFailedToSendDialog();
      }
      for (;;)
      {
        return;
        MyToast.makeText(ServoSetupFragment.this.getActivity(), ServoSetupFragment.this.getString(2131296462), 0, 1).show();
      }
    }
  }
}


