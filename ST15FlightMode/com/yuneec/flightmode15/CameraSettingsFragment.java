package com.yuneec.flightmode15;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.yuneec.IPCameraManager.Amba2;
import com.yuneec.IPCameraManager.CameraParams;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.BaseDialog;
import com.yuneec.widget.SwitchWidgetPreference;
import com.yuneec.widget.TextWidgetPreference;

public class CameraSettingsFragment
  extends PreferenceFragment
  implements Preference.OnPreferenceClickListener, Preference.OnPreferenceChangeListener
{
  public static final String CAMERA_TYPE_CGO3 = "C-GO3";
  private static final String KEY_AUDIO = "audio";
  private static final String KEY_CALIBRATION = "calibration";
  private static final String KEY_GPS = "gps";
  private static final String KEY_IQ_TYPE = "iq_type";
  private static final String KEY_PHOTO_FORMAT = "photo_format";
  private static final String KEY_RESET = "reset";
  private static final String KEY_RESOLUTION = "resolution";
  private static final String TAG = "SettingsFragment";
  private SwitchWidgetPreference mAudioPref;
  private Preference mCalibrationPref;
  private CameraParams mCameraParams;
  private UARTController mController = null;
  private String mCurrentCamera;
  private SwitchWidgetPreference mGPSPref;
  private Handler mHandler = new Handler();
  private WeakHandler<PreferenceFragment> mHttpHandler = new WeakHandler(this)
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((PreferenceFragment)getOwner() == null) {
        Log.i("SettingsFragment", "fragment destoryed");
      }
      for (;;)
      {
        return;
        switch (paramAnonymousMessage.what)
        {
        default: 
          break;
        case 1: 
          switch (paramAnonymousMessage.arg1)
          {
          default: 
            break;
          case 24: 
          case 43: 
          case 45: 
          case 47: 
          case 51: 
          case 57: 
          case 59: 
            try
            {
              if (!"HTTPCODE OK".equals((String)paramAnonymousMessage.obj)) {
                Log.e("SettingsFragment", "Failed to communicate camera");
              }
              CameraSettingsFragment.this.dismissProgressDialog();
            }
            catch (Exception paramAnonymousMessage)
            {
              Log.e("SettingsFragment", "Exception--Failed to communicate camera");
            }
            break;
          case 58: 
            try
            {
              if (!(paramAnonymousMessage.obj instanceof String)) {
                break label262;
              }
              paramAnonymousMessage = (String)paramAnonymousMessage.obj;
              CameraSettingsFragment.this.mResolutionPref.setValue(paramAnonymousMessage);
              CameraSettingsFragment.this.mResolutionPref.setWidgetText(paramAnonymousMessage);
              CameraSettingsFragment.this.dismissProgressDialog();
            }
            catch (Exception paramAnonymousMessage)
            {
              Log.e("SettingsFragment", "Exception--Failed to get video mode");
            }
            continue;
            Log.e("SettingsFragment", "Failed to get video mode");
            break;
          case 60: 
            for (;;)
            {
              try
              {
                if (!(paramAnonymousMessage.obj instanceof Integer)) {
                  break label336;
                }
                if (((Integer)paramAnonymousMessage.obj).intValue() != 0) {
                  break label331;
                }
                bool = true;
                CameraSettingsFragment.this.mAudioPref.setWidgetStates(bool);
                CameraSettingsFragment.this.dismissProgressDialog();
              }
              catch (Exception paramAnonymousMessage)
              {
                Log.e("SettingsFragment", "Exception--Failed to get audio state");
              }
              break;
              boolean bool = false;
              continue;
              Log.e("SettingsFragment", "Failed to get audio state");
            }
          case 46: 
            for (;;)
            {
              try
              {
                if (!(paramAnonymousMessage.obj instanceof String)) {
                  break label409;
                }
                paramAnonymousMessage = (String)paramAnonymousMessage.obj;
                CameraSettingsFragment.this.mPhotoFormatPref.setValue(paramAnonymousMessage);
                CameraSettingsFragment.this.mPhotoFormatPref.setWidgetText(paramAnonymousMessage);
                CameraSettingsFragment.this.dismissProgressDialog();
              }
              catch (Exception paramAnonymousMessage)
              {
                Log.e("SettingsFragment", "Exception--Failed to get photo format");
              }
              break;
              Log.e("SettingsFragment", "Failed to get photo format");
            }
          case 52: 
            for (;;)
            {
              try
              {
                if (!(paramAnonymousMessage.obj instanceof Integer)) {
                  break label501;
                }
                paramAnonymousMessage = (Integer)paramAnonymousMessage.obj;
                CameraSettingsFragment.this.mIQPref.setValue(String.valueOf(paramAnonymousMessage));
                CameraSettingsFragment.this.mIQPref.setWidgetText(CameraSettingsFragment.this.getResources().getStringArray(2131361813)[paramAnonymousMessage.intValue()]);
                CameraSettingsFragment.this.dismissProgressDialog();
              }
              catch (Exception paramAnonymousMessage)
              {
                Log.e("SettingsFragment", "Exception--Failed to get IQ type");
              }
              break;
              Log.e("SettingsFragment", "Failed to get IQ type");
            }
          case 37: 
            label262:
            label331:
            label336:
            label409:
            label501:
            if ((paramAnonymousMessage.obj instanceof CameraParams))
            {
              CameraSettingsFragment.this.mCameraParams = ((CameraParams)paramAnonymousMessage.obj);
              CameraSettingsFragment.this.initPerferences();
            }
            break;
          }
          break;
        }
      }
    }
  };
  private Messenger mHttpMessenger = new Messenger(this.mHttpHandler);
  private Amba2 mIPCameraManager;
  private TextWidgetPreference mIQPref;
  private TextWidgetPreference mPhotoFormatPref;
  private ProgressDialog mProgressDialog;
  private Runnable mProgressTimeOutRunnable = new Runnable()
  {
    public void run()
    {
      Toast.makeText(CameraSettingsFragment.this.getActivity(), 2131296627, 1).show();
      CameraSettingsFragment.this.dismissProgressDialog();
    }
  };
  private CameraResetDialogPreference mResetPref;
  private TextWidgetPreference mResolutionPref;
  
  private void calibrationAction()
  {
    final BaseDialog localBaseDialog = new BaseDialog(getActivity(), 2131230729);
    localBaseDialog.setContentView(2130903048);
    ((Button)localBaseDialog.findViewById(2131689518)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (CameraSettingsFragment.this.mController != null) {
          CameraSettingsFragment.this.mController.setTTBState(false, Utilities.HW_VB_BASE + 3, false);
        }
        localBaseDialog.dismiss();
      }
    });
    ((Button)localBaseDialog.findViewById(2131689519)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (CameraSettingsFragment.this.mController != null) {
          CameraSettingsFragment.this.mController.setTTBState(false, Utilities.HW_VB_BASE + 4, false);
        }
        localBaseDialog.dismiss();
      }
    });
    localBaseDialog.show();
  }
  
  private void dismissProgressDialog()
  {
    if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing()))
    {
      this.mProgressDialog.dismiss();
      this.mProgressDialog = null;
    }
    removeProgressRunnable();
  }
  
  private void initPerferences()
  {
    this.mResolutionPref.setWidgetText(this.mCameraParams.video_mode);
    Object localObject = this.mAudioPref;
    if (this.mCameraParams.audio_sw == 0) {}
    for (boolean bool = true;; bool = false)
    {
      ((SwitchWidgetPreference)localObject).setWidgetStates(bool);
      this.mPhotoFormatPref.setWidgetText(this.mCameraParams.photo_format);
      localObject = getResources().getStringArray(2131361813);
      if (this.mCameraParams.iq_type >= localObject.length) {
        this.mCameraParams.iq_type = 0;
      }
      this.mIQPref.setWidgetText(localObject[this.mCameraParams.iq_type]);
      this.mGPSPref.setWidgetStates(getActivity().getIntent().getExtras().getBoolean("gps_switch"));
      this.mResolutionPref.setValue(this.mCameraParams.video_mode);
      this.mPhotoFormatPref.setValue(this.mCameraParams.photo_format);
      this.mIQPref.setValue(String.valueOf(this.mCameraParams.iq_type));
      if (this.mCameraParams.status.equals("record"))
      {
        this.mResolutionPref.setEnabled(false);
        this.mAudioPref.setEnabled(false);
        this.mPhotoFormatPref.setEnabled(false);
        this.mIQPref.setEnabled(false);
        this.mResetPref.setEnabled(false);
      }
      if (this.mCameraParams.cam_mode == 2) {
        this.mResolutionPref.setEnabled(false);
      }
      return;
    }
  }
  
  private void removeProgressRunnable()
  {
    if (this.mProgressTimeOutRunnable != null) {
      this.mHandler.removeCallbacks(this.mProgressTimeOutRunnable);
    }
  }
  
  private void showProgressDialog()
  {
    this.mProgressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(2131296620), false, false);
    this.mProgressDialog.setCancelable(true);
    this.mHandler.postDelayed(this.mProgressTimeOutRunnable, 5000L);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity().getIntent();
    int i = paramBundle.getExtras().getInt("fmodeStatus");
    this.mCameraParams = ((CameraParams)paramBundle.getExtras().getParcelable("camera_params"));
    this.mCurrentCamera = paramBundle.getExtras().getString("currentCamera");
    addPreferencesFromResource(2131099648);
    this.mPhotoFormatPref = ((TextWidgetPreference)findPreference("photo_format"));
    this.mPhotoFormatPref.setOnPreferenceChangeListener(this);
    this.mResolutionPref = ((TextWidgetPreference)findPreference("resolution"));
    if (this.mCurrentCamera.equals("C-GO3"))
    {
      this.mResolutionPref.setEntries(2131361810);
      this.mResolutionPref.setEntryValues(2131361810);
      ((PreferenceGroup)findPreference("camera_category")).removePreference(this.mPhotoFormatPref);
      this.mResolutionPref.setOnPreferenceChangeListener(this);
      this.mAudioPref = ((SwitchWidgetPreference)findPreference("audio"));
      this.mAudioPref.setOnPreferenceClickListener(this);
      this.mIQPref = ((TextWidgetPreference)findPreference("iq_type"));
      this.mIQPref.setOnPreferenceChangeListener(this);
      this.mResetPref = ((CameraResetDialogPreference)findPreference("reset"));
      this.mResetPref.setOnPreferenceChangeListener(this);
      this.mGPSPref = ((SwitchWidgetPreference)findPreference("gps"));
      this.mCalibrationPref = findPreference("calibration");
      this.mCalibrationPref.setOnPreferenceClickListener(this);
      if (16 == i) {
        break label301;
      }
      this.mGPSPref.setEnabled(false);
      this.mCalibrationPref.setEnabled(false);
    }
    for (;;)
    {
      this.mController = UARTController.getInstance();
      return;
      this.mResolutionPref.setEntries(2131361811);
      this.mResolutionPref.setEntryValues(2131361811);
      break;
      label301:
      this.mGPSPref.setOnPreferenceClickListener(this);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.mController != null) {
      this.mController = null;
    }
  }
  
  public void onPause()
  {
    super.onPause();
  }
  
  public boolean onPreferenceChange(Preference paramPreference, Object paramObject)
  {
    if (paramPreference.getKey().equals("resolution"))
    {
      paramPreference = (String)paramObject;
      Log.i("SettingsFragment", "set Video mode to " + paramPreference);
      this.mIPCameraManager.setVideoMode(this.mHttpMessenger, paramPreference);
      this.mResolutionPref.setWidgetText(paramPreference);
      showProgressDialog();
    }
    for (;;)
    {
      return true;
      if (paramPreference.getKey().equals("photo_format"))
      {
        paramPreference = (String)paramObject;
        Log.i("SettingsFragment", "set photo format to " + paramPreference);
        this.mIPCameraManager.setPhotoFormat(this.mHttpMessenger, paramPreference);
        this.mPhotoFormatPref.setWidgetText(paramPreference);
        showProgressDialog();
      }
      else if (paramPreference.getKey().equals("iq_type"))
      {
        int i = Integer.parseInt((String)paramObject);
        Log.i("SettingsFragment", "set IQ type to " + i);
        this.mIPCameraManager.setIQtype(this.mHttpMessenger, i);
        this.mIQPref.setWidgetText(getResources().getStringArray(2131361813)[i]);
        showProgressDialog();
      }
      else if (paramPreference.getKey().equals("reset"))
      {
        if (((Boolean)paramObject).booleanValue())
        {
          this.mIPCameraManager.resetDefault(this.mHttpMessenger);
          showProgressDialog();
        }
        Log.d("SettingsFragment", "Camera reset:" + paramObject);
      }
    }
  }
  
  public boolean onPreferenceClick(Preference paramPreference)
  {
    boolean bool;
    int i;
    if (paramPreference.getKey().equals("audio"))
    {
      paramPreference = this.mAudioPref;
      if (this.mAudioPref.getIsOnStates())
      {
        bool = false;
        paramPreference.setWidgetStates(bool);
        if (!this.mAudioPref.getIsOnStates()) {
          break label65;
        }
        i = 0;
        label46:
        this.mIPCameraManager.setAudioState(this.mHttpMessenger, i);
      }
    }
    for (;;)
    {
      return true;
      bool = true;
      break;
      label65:
      i = 1;
      break label46;
      if (paramPreference.getKey().equals("gps"))
      {
        if (this.mController != null)
        {
          paramPreference = this.mGPSPref;
          if (this.mGPSPref.getIsOnStates()) {}
          for (bool = false;; bool = true)
          {
            paramPreference.setWidgetStates(bool);
            if (!Boolean.valueOf(this.mGPSPref.getIsOnStates()).booleanValue()) {
              break label150;
            }
            this.mController.setTTBState(false, Utilities.HW_VB_BASE + 2, false);
            break;
          }
          label150:
          this.mController.setTTBState(false, Utilities.HW_VB_BASE + 1, false);
        }
      }
      else if (paramPreference.getKey().equals("calibration")) {
        calibrationAction();
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
  }
  
  public void onStart()
  {
    super.onStart();
    this.mIPCameraManager = ((Amba2)IPCameraManager.getIPCameraManager(getActivity(), 104));
    this.mIPCameraManager.getWorkStatus(this.mHttpMessenger);
  }
  
  public void onStop()
  {
    super.onStop();
    this.mIPCameraManager.finish();
    this.mIPCameraManager = null;
    this.mHandler.removeCallbacks(this.mProgressTimeOutRunnable);
  }
}


