package com.yuneec.flightmode15;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import com.yuneec.uartcontroller.UARTController;

public class BootCompletedReceiver
  extends BroadcastReceiver
{
  private static final String TAG = "BootCompletedReceiver";
  private UARTController mController;
  
  