package com.yuneec.flightmode15;

public abstract interface WbIsoChangedListener
{
  public abstract void onIsoModeChanged(boolean paramBoolean);
  
  public abstract void onWbModeChanged(int paramInt, boolean paramBoolean);
}


