package com.yuneec.flightmode15;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.util.AttributeSet;
import android.view.View;

public class CameraResetDialogPreference
  extends DialogPreference
{
  private Preference.OnPreferenceChangeListener listener;
  
  public CameraResetDialogPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public CameraResetDialogPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void onBindDialogView(View paramView)
  {
    super.onBindDialogView(paramView);
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    super.onClick(paramDialogInterface, paramInt);
    paramDialogInterface = this.listener;
    if (paramInt == -1) {}
    for (boolean bool = true;; bool = false)
    {
      paramDialogInterface.onPreferenceChange(this, Boolean.valueOf(bool));
      return;
    }
  }
  
  public void setOnPreferenceChangeListener(Preference.OnPreferenceChangeListener paramOnPreferenceChangeListener)
  {
    this.listener = paramOnPreferenceChangeListener;
  }
}


