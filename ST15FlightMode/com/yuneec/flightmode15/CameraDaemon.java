package com.yuneec.flightmode15;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import com.yuneec.IPCameraManager.CameraParams;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.IPCameraManager.IPCameraManager.RecordStatus;
import com.yuneec.IPCameraManager.IPCameraManager.SDCardStatus;
import com.yuneec.IPCameraManager.IPCameraManager.ToneSetting;

public class CameraDaemon
{
  public static final int CONNECTION = 101;
  private static final boolean DEBUG = true;
  public static final int HTTP_DEAD = 104;
  private static final int HTTP_DEAD_COUNT = 20;
  private static final int RECORD_INTERVAL = 10000;
  public static final int SDSTATUS = 100;
  private static final int SDSTATUS_INTERVAL = 10000;
  public static final int STATUS_CHANGE = 102;
  private static final String TAG = "CameraDaemon";
  public static final int TONE_SETTING = 103;
  public static final int VIDEO_RESOLUTION = 105;
  public static final String WIFI_STATE_INA = "invalid";
  public static final String WIFI_STATE_NONE = "disconnectted";
  public static final String WIFI_STATE_OK = "connectted";
  private static final int WORKING_INTERVAL = 10000;
  private Handler mHandler;
  private int mHttpDeadCounter = 0;
  private boolean mHttpDeadSent = false;
  private WeakHandler<CameraDaemon> mHttpHandler = new WeakHandler(this)
  {
    private void checkAndSendHttpDeadMessage() {}
    
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      }
      for (;;)
      {
        return;
        switch (paramAnonymousMessage.arg1)
        {
        default: 
          Log.i("CameraDaemon", "response :" + paramAnonymousMessage.arg1);
          break;
        case 10: 
          if ((paramAnonymousMessage.obj instanceof IPCameraManager.SDCardStatus))
          {
            paramAnonymousMessage = Message.obtain(paramAnonymousMessage);
            paramAnonymousMessage.what = 100;
            CameraDaemon.this.mHandler.sendMessage(paramAnonymousMessage);
            CameraDaemon.this.mHttpDeadCounter = 0;
          }
          else
          {
            Log.i("CameraDaemon", "request sdcard status:" + paramAnonymousMessage.obj.toString());
            checkAndSendHttpDeadMessage();
          }
          break;
        case 32: 
          if ((paramAnonymousMessage.obj instanceof String))
          {
            paramAnonymousMessage = Message.obtain(paramAnonymousMessage);
            paramAnonymousMessage.what = 105;
            CameraDaemon.this.mHandler.sendMessage(paramAnonymousMessage);
            CameraDaemon.this.mHttpDeadCounter = 0;
          }
          break;
        case 15: 
          if ((paramAnonymousMessage.obj instanceof IPCameraManager.ToneSetting))
          {
            paramAnonymousMessage = Message.obtain(paramAnonymousMessage);
            paramAnonymousMessage.what = 103;
            CameraDaemon.this.mHandler.sendMessage(paramAnonymousMessage);
            CameraDaemon.this.mHttpDeadCounter = 0;
          }
          else
          {
            Log.i("CameraDaemon", "request tone setting:" + paramAnonymousMessage.obj.toString());
            checkAndSendHttpDeadMessage();
          }
          break;
        case 21: 
          Log.i("CameraDaemon", "REQUEST_IS_RECORDING:" + paramAnonymousMessage.obj);
          if ((paramAnonymousMessage.obj instanceof IPCameraManager.RecordStatus))
          {
            paramAnonymousMessage = Message.obtain(paramAnonymousMessage);
            paramAnonymousMessage.what = 102;
            CameraDaemon.this.mHandler.sendMessage(paramAnonymousMessage);
            CameraDaemon.this.mHttpDeadCounter = 0;
          }
          else
          {
            Log.i("CameraDaemon", "recording status:" + paramAnonymousMessage.obj.toString());
            checkAndSendHttpDeadMessage();
          }
          break;
        case 37: 
          Log.i("CameraDaemon", "REQUEST_GET_WORK_STATUS:" + paramAnonymousMessage.obj);
          if (((paramAnonymousMessage.obj instanceof IPCameraManager.RecordStatus)) || ((paramAnonymousMessage.obj instanceof CameraParams)))
          {
            paramAnonymousMessage = Message.obtain(paramAnonymousMessage);
            paramAnonymousMessage.what = 102;
            CameraDaemon.this.mHandler.sendMessage(paramAnonymousMessage);
            CameraDaemon.this.mHttpDeadCounter = 0;
          }
          else
          {
            Log.i("CameraDaemon", "recording status:" + paramAnonymousMessage.obj.toString());
            checkAndSendHttpDeadMessage();
          }
          break;
        }
      }
    }
  };
  private Messenger mHttpMessenger = new Messenger(this.mHttpHandler);
  private IPCameraManager mIPCameraManager;
  private Runnable mRecordingDaemon = new Runnable()
  {
    public void run()
    {
      if (CameraDaemon.this.mIPCameraManager != null)
      {
        CameraDaemon.this.mIPCameraManager.isRecording(CameraDaemon.this.mHttpMessenger);
        CameraDaemon.this.mHandler.postDelayed(CameraDaemon.this.mRecordingDaemon, 10000L);
      }
    }
  };
  private Runnable mSDStausDaemon = new Runnable()
  {
    public void run()
    {
      if (CameraDaemon.this.mIPCameraManager != null)
      {
        CameraDaemon.this.mIPCameraManager.getSDCardStatus(CameraDaemon.this.mHttpMessenger);
        CameraDaemon.this.mHandler.postDelayed(CameraDaemon.this.mSDStausDaemon, 10000L);
      }
    }
  };
  private Runnable mWokingDaemon = new Runnable()
  {
    public void run()
    {
      if (CameraDaemon.this.mIPCameraManager != null)
      {
        CameraDaemon.this.mIPCameraManager.getWorkStatus(CameraDaemon.this.mHttpMessenger);
        CameraDaemon.this.mHandler.postDelayed(CameraDaemon.this.mWokingDaemon, 10000L);
      }
    }
  };
  
  public CameraDaemon(Handler paramHandler)
  {
    this.mHandler = paramHandler;
  }
  
  public void start(Context paramContext)
  {
    int i = paramContext.getSharedPreferences("flight_setting_value", 0).getInt("camera_type_flag", paramContext.getResources().getInteger(2131492868));
    if ((i & 0x1) == 1) {
      this.mIPCameraManager = IPCameraManager.getIPCameraManager(paramContext, 101);
    }
    for (;;)
    {
      this.mHttpDeadCounter = 0;
      this.mHttpDeadSent = false;
      return;
      if ((i & 0x4) == 4)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(paramContext, 102);
        this.mSDStausDaemon.run();
        this.mRecordingDaemon.run();
        Log.d("CameraDaemon", "mSDStausDaemon--start");
      }
      else if (((i & 0x8) == 8) || ((i & 0x20) == 32))
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(paramContext, 104);
        this.mWokingDaemon.run();
        Log.d("CameraDaemon", "getworkingdaemon--start");
      }
      else
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(paramContext, 100);
      }
    }
  }
  
  public void stop(Context paramContext)
  {
    this.mHandler.removeCallbacks(this.mSDStausDaemon);
    this.mHandler.removeCallbacks(this.mRecordingDaemon);
    this.mHandler.removeCallbacks(this.mWokingDaemon);
    this.mIPCameraManager.finish();
    this.mIPCameraManager = null;
  }
  
  public void updateSDStatusImediately()
  {
    if (this.mIPCameraManager != null) {
      this.mSDStausDaemon.run();
    }
    for (;;)
    {
      return;
      Log.i("CameraDaemon", "the daemon has not been started yet");
    }
  }
}


