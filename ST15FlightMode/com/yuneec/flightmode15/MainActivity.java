package com.yuneec.flightmode15;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.SoundPool;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Vibrator;
import android.text.format.Formatter;
import android.text.format.Time;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.yuneec.IPCameraManager.Amba2;
import com.yuneec.IPCameraManager.CameraParams;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.IPCameraManager.IPCameraManager.RecordStatus;
import com.yuneec.IPCameraManager.IPCameraManager.RecordTime;
import com.yuneec.IPCameraManager.IPCameraManager.SDCardStatus;
import com.yuneec.IPCameraManager.IPCameraManager.ShutterTimeISO;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.BindWifiManage;
import com.yuneec.flight_settings.FlightSettings;
import com.yuneec.mission.MissionPresentation.MissionState;
import com.yuneec.model_select.ModelSelectMain;
import com.yuneec.rtvplayer.RTVPlayer;
import com.yuneec.rtvplayer.RTVPlayer.VideoEventCallback;
import com.yuneec.uartcontroller.FCSensorData;
import com.yuneec.uartcontroller.FModeData;
import com.yuneec.uartcontroller.GPSUpLinkData;
import com.yuneec.uartcontroller.MixedData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.uartcontroller.UARTInfoMessage.MissionReply;
import com.yuneec.uartcontroller.UARTInfoMessage.SwitchChanged;
import com.yuneec.uartcontroller.UARTInfoMessage.Telemetry;
import com.yuneec.uartcontroller.UARTInfoMessage.Trim;
import com.yuneec.widget.CounterView;
import com.yuneec.widget.CounterView.OnTickListener;
import com.yuneec.widget.HomeCompassView;
import com.yuneec.widget.IndicatorView;
import com.yuneec.widget.LeftTrimView;
import com.yuneec.widget.MissionView;
import com.yuneec.widget.MissionView.MissionViewCallback;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.OneButtonPopDialog;
import com.yuneec.widget.RightTrimView;
import com.yuneec.widget.StatusbarView;
import com.yuneec.widget.StyledNumberPicker;
import com.yuneec.widget.StyledNumberPicker.OnButtonClickedListener;
import com.yuneec.widget.StyledNumberPicker.OnScrollListener;
import com.yuneec.widget.StyledNumberPicker.OnValueChangeListener;
import com.yuneec.widget.SyncToggleButton;
import com.yuneec.widget.SyncToggleButton.OnUpdateChangeListener;
import com.yuneec.widget.ToggleButtonWithColorText;
import com.yuneec.widget.ToggleFrameView;
import com.yuneec.widget.ToggleFrameView.ToggleOnClickListener;
import com.yuneec.widget.TwoButtonPopDialog;
import com.yuneec.widget.VerticalSeekBar;
import com.yuneec.widget.WhiteBalanceScrollView;
import com.yuneec.widget.WhiteBalanceScrollView.OnItemSelectedListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity
  extends Activity
  implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, Animation.AnimationListener, SeekBar.OnSeekBarChangeListener
{
  public static final String CAMERA_TYPE_CGO2 = "C-GO2";
  public static final String CAMERA_TYPE_CGO3 = "C-GO3";
  public static final String CAMERA_TYPE_CGO3_PRO = "C-GO3-Pro";
  private static final boolean DEBUG_CH = false;
  private static final boolean DEBUG_WIFI = false;
  private static final int DEFAULT_COLOR = -16724737;
  private static final int LOW_CAMERA_STORAGE_THRESHOLD = 420;
  private static final int POSTANIM_DELAY_MILLIS = 630;
  private static final int PRIORITY_BATTERY_WARNING1 = 1;
  private static final int PRIORITY_BATTERY_WARNING2 = 4;
  private static final int PRIORITY_GPS_WARNING = 2;
  protected static final int RECONNECT_INTERVAL = 3000;
  public static final String SAVED_LOCAL_VIDEO = "/sdcard/FPV-Video/Local";
  private static final String TAG = "FlightModeMainActivity";
  private static final int TIME_SETTING_REQ = 1003;
  private static final String VIDEO_LOCATION = "rtsp://192.168.73.254:8556/PSIA/Streaming/channels/2?videoCodecType=H.264";
  private static final String VIDEO_LOCATION2 = "rtsp://192.168.73.254:8557/PSIA/Streaming/channels/2?videoCodecType=H.264";
  private static final String VIDEO_LOCATION3 = "rtsp://192.168.42.1/live";
  private static final String VIDEO_LOCATION4 = "rtsp://192.168.110.1/cam1/h264";
  private static int VOL_INDEX_MAX = 15;
  private static int mIndex = 0;
  private static final String mInvalidValueString = "N/A";
  private static float mTmpVOLFloatValue = 0.0F;
  private static float[] mVOLValue = new float[VOL_INDEX_MAX];
  private static final ArrayList<Integer> mWBList;
  private static final float sCatBatteyValue = 10.4F;
  private static final float sLowBatteyValue = 10.6F;
  private float VolValue = 160.0F;
  private AlertDialog bindStatePrompt;
  private RelativeLayout cameraControlCombine;
  private int cameraControlCombineVisibility;
  private int cameraModeCache = 0;
  private int countTime = 0;
  private int currentPriority = 0;
  private FlightLog flightLog;
  private HandlerThread handlerThread;
  private int initLcdHeight;
  private int initLcdTopMargin;
  private int initLcdWidth;
  private boolean isChangeAndStartRecord = false;
  private boolean isCheckedBindState = false;
  private boolean isFSKConneted = false;
  private int isMasterControl;
  private int isMultiControl;
  private boolean isVibrating = false;
  private boolean isWIFIConneted = false;
  private OneButtonPopDialog mAirportWarningDialog;
  private OneButtonPopDialog mAltitudeWarningDialog;
  private Button mAutoBtn;
  private LinearLayout mAutoModeFrame;
  private Animation mBottomFadeIn;
  private Animation mBottomFadeOut;
  private Button mBtnFlightSetting;
  private Button mBtnModelSelect;
  private ToggleButton mBtnRecord;
  private Button mBtnSnapshot;
  private Button mBtnSystemSetting;
  private CameraDaemon mCameraDaemon;
  private Handler mCameraDaemonHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      }
      for (;;)
      {
        return;
        MainActivity.this.onSDcardChanged((IPCameraManager.SDCardStatus)paramAnonymousMessage.obj);
        continue;
        if ((paramAnonymousMessage.obj instanceof IPCameraManager.RecordStatus))
        {
          paramAnonymousMessage = (IPCameraManager.RecordStatus)paramAnonymousMessage.obj;
          Log.i("FlightModeMainActivity", "is recording:" + paramAnonymousMessage.isRecording);
          MainActivity.this.setRecordBtnState(paramAnonymousMessage.isRecording);
        }
        else if ((paramAnonymousMessage.obj instanceof CameraParams))
        {
          MainActivity.this.mCameraParams = ((CameraParams)paramAnonymousMessage.obj);
          if (MainActivity.this.mCameraParams.status.equals("record"))
          {
            if (!MainActivity.this.mHasStoppedRecord) {
              MainActivity.this.setRecordBtnState(true);
            }
            MainActivity.this.mHasStartedRecord = false;
            label187:
            if (MainActivity.this.mCameraParams.cam_mode == 2) {
              break label440;
            }
            MainActivity.this.setRlnText(MainActivity.this.mCameraParams.video_mode);
            label218:
            if (MainActivity.this.cameraModeCache == 0) {
              break label500;
            }
            Log.i("FlightModeMainActivity", "CameraMode--cameraModeFlash != 0");
            if (MainActivity.this.cameraModeCache != MainActivity.this.mCameraParams.cam_mode) {
              break label451;
            }
            Log.i("FlightModeMainActivity", "CameraMode--cameraModeCache == mCameraParams.cam_mode, " + MainActivity.this.mCameraParams.cam_mode);
            if (MainActivity.this.mSRswitchCacheRunnable != null)
            {
              Log.i("FlightModeMainActivity", "CameraMode--remove SRswitch Runnable");
              MainActivity.this.mHandler.removeCallbacks(MainActivity.this.mSRswitchCacheRunnable);
            }
            MainActivity.this.cameraModeCache = 0;
          }
          for (;;)
          {
            MainActivity.this.onSDcardChanged(MainActivity.this.mCameraParams.sdFree);
            break;
            if (MainActivity.this.mCameraParams.status.equals("write err"))
            {
              MainActivity.this.mStatusBarView.setInfoText(MainActivity.this.getResources().getString(2131296356), 0);
              ((Amba2)MainActivity.this.mIPCameraManager).resetStatus(MainActivity.this.mHttpResponseMessenger);
            }
            if (!MainActivity.this.mHasStartedRecord) {
              MainActivity.this.setRecordBtnState(false);
            }
            MainActivity.this.mHasStoppedRecord = false;
            break label187;
            label440:
            MainActivity.this.setRlnText(null);
            break label218;
            label451:
            Log.i("FlightModeMainActivity", "CameraMode--cameraModeCache=" + MainActivity.this.cameraModeCache + " mCameraParams.cam_mode=" + MainActivity.this.mCameraParams.cam_mode);
          }
          label500:
          Log.i("FlightModeMainActivity", "CameraMode--cameraModeFlash == 0");
          paramAnonymousMessage = MainActivity.this.mSRswitch;
          if (MainActivity.this.mCameraParams.cam_mode == 2) {}
          for (boolean bool = false;; bool = true)
          {
            paramAnonymousMessage.syncState(bool);
            break;
          }
          MainActivity.this.setRlnBtnText((String)paramAnonymousMessage.obj);
        }
      }
    }
  };
  private String mCameraInfo;
  private CameraParams mCameraParams = new CameraParams();
  private Button mCameraSettingsBtn;
  private int mCameraShutterSoundId;
  private Runnable mChangeCameraModeRunnable = new Runnable()
  {
    public void run()
    {
      if ((MainActivity.this.mProgressDialog != null) && (MainActivity.this.mProgressDialog.isShowing())) {
        MainActivity.this.mProgressDialog.dismiss();
      }
      Log.d("FlightModeMainActivity", "CameraMode--get camera mode that change camera mode after 5 seconds");
      ((Amba2)MainActivity.this.mIPCameraManager).getCameraMode(MainActivity.this.mHttpResponseMessenger);
    }
  };
  private ProgressDialog mCommunicating;
  private Runnable mCommunicatingRunnable = new Runnable()
  {
    public void run()
    {
      MainActivity.this.dismissCommunicatingDialog();
      Toast.makeText(MainActivity.this, 2131296627, 1).show();
    }
  };
  private CompassUpdater mCompassUpdater;
  private MyProgressDialog mConnectingDialog;
  private UARTController mController;
  private String mCurrentCameraName;
  private int mCurrentExposureIndex = 0;
  private int mCurrentFmode;
  private long mCurrentModelId;
  private long mCurrentModelType;
  private String mCurrentVideoLocation = "rtsp://192.168.73.254:8556/PSIA/Streaming/channels/2?videoCodecType=H.264";
  private List<String> mEVListValue;
  private ImageView mEVdecrement;
  private ImageView mEVincrement;
  private StyledNumberPicker mEVpicker;
  private Runnable mExhibitionRunnable = new Runnable()
  {
    private int i = 0;
    
    public void run()
    {
      if (this.i == 0) {
        this.i += 1;
      }
      for (;;)
      {
        MainActivity.this.mHandler.postDelayed(MainActivity.this.mExhibitionRunnable, 1000L);
        return;
        if (this.i == 1) {
          this.i += 1;
        } else if (this.i == 2) {
          this.i += 1;
        } else if (this.i == 3) {
          this.i += 1;
        } else if (this.i == 4) {
          this.i = 0;
        }
      }
    }
  };
  private Runnable mExhibitionRunnable2 = new Runnable()
  {
    int IntValue = 0;
    int IntValue2 = 0;
    float floatValue = 0.0F;
    float floatValue2 = 0.0F;
    boolean reverse = false;
    boolean reverse2 = false;
    boolean reverse3 = false;
    boolean reverse4;
    
    public void run()
    {
      if (this.reverse)
      {
        this.IntValue -= 1;
        if (!this.reverse2) {
          break label180;
        }
        this.IntValue2 -= 1;
        label34:
        if (!this.reverse3) {
          break label193;
        }
        this.floatValue = ((float)(this.floatValue - 0.1D));
        label55:
        if (!this.reverse4) {
          break label210;
        }
        this.floatValue2 -= 1.0F;
        label72:
        MainActivity.this.mHomeCompassView.setDirection(this.IntValue);
        if (this.IntValue < 360) {
          break label223;
        }
        this.reverse = true;
        label101:
        if (this.IntValue2 < 90) {
          break label238;
        }
        this.reverse2 = true;
        label115:
        if (this.floatValue < 90.0F) {
          break label255;
        }
        this.reverse3 = true;
        label130:
        if (this.floatValue2 < 60.0F) {
          break label272;
        }
        this.reverse4 = true;
      }
      for (;;)
      {
        MainActivity.this.mHandler.postDelayed(MainActivity.this.mExhibitionRunnable2, 100L);
        return;
        this.IntValue += 1;
        break;
        label180:
        this.IntValue2 += 1;
        break label34;
        label193:
        this.floatValue = ((float)(this.floatValue + 0.1D));
        break label55;
        label210:
        this.floatValue2 += 1.0F;
        break label72;
        label223:
        if (this.IntValue > 0) {
          break label101;
        }
        this.reverse = false;
        break label101;
        label238:
        if (this.IntValue2 > -90) {
          break label115;
        }
        this.reverse2 = false;
        break label115;
        label255:
        if (this.floatValue > 0.0F) {
          break label130;
        }
        this.reverse3 = false;
        break label130;
        label272:
        if (this.floatValue2 <= -60.0F) {
          this.reverse4 = false;
        }
      }
    }
  };
  private ListView mFModeList;
  private AlertDialog mFModeOptions;
  private ToggleButtonWithColorText mFlightToggle;
  private CompoundButton.OnCheckedChangeListener mFlightToggleListener = new CompoundButton.OnCheckedChangeListener()
  {
    public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
    {
      if (MainActivity.this.mController == null) {
        return;
      }
      if (paramAnonymousBoolean)
      {
        MainActivity.this.enableButtonBar(false);
        if (MainActivity.this.mCurrentModelType != 406L)
        {
          Utilities.setRunningMode(true);
          Utilities.ensureRunState(MainActivity.this.mController);
          MainActivity.this.mTimerHelper.startFlight();
          MainActivity.this.mGPSUpdater.start(MainActivity.this.mHandler);
          MainActivity.this.mCompassUpdater.start(MainActivity.this.mHandler);
        }
        for (;;)
        {
          MainActivity.this.mIsPlayFPV = true;
          if (!MainActivity.this.isWIFIConneted) {
            break;
          }
          MainActivity.this.mBtnSnapshot.setEnabled(true);
          MainActivity.this.mBtnRecord.setEnabled(true);
          if (MainActivity.this.mIPCameraManager == null) {
            break;
          }
          MainActivity.this.mIPCameraManager.initCamera(MainActivity.this.mHttpResponseMessenger);
          break;
          MainActivity.this.mController.enterSim(true);
          Utilities.ensureSimState(MainActivity.this.mController);
        }
      }
      if (MainActivity.this.mCurrentModelType != 406L)
      {
        Utilities.setRunningMode(false);
        MainActivity.this.mController.exitRun(true);
        MainActivity.this.mGPSUpdater.stop();
        MainActivity.this.mCompassUpdater.stop();
        MainActivity.this.mTimerHelper.endFlight();
      }
      for (;;)
      {
        MainActivity.this.enableButtonBar(true);
        MainActivity.this.mBtnSnapshot.setEnabled(false);
        MainActivity.this.mBtnRecord.setEnabled(false);
        MainActivity.this.mIsPlayFPV = false;
        break;
        MainActivity.this.mController.exitSim(true);
        Utilities.ensureAwaitState(MainActivity.this.mController);
      }
    }
  };
  private View.OnTouchListener mFlightToggleTouchListener = new View.OnTouchListener()
  {
    public boolean onTouch(View paramAnonymousView, final MotionEvent paramAnonymousMotionEvent)
    {
      boolean bool = true;
      ToggleButton localToggleButton = (ToggleButton)paramAnonymousView;
      if (!paramAnonymousView.isEnabled()) {}
      for (;;)
      {
        return bool;
        if (localToggleButton.isChecked())
        {
          bool = false;
        }
        else if (paramAnonymousMotionEvent.getAction() == 0)
        {
          if (MainActivity.this.checkModelStatus() == 0) {
            bool = false;
          } else {
            paramAnonymousView.setPressed(true);
          }
        }
        else if (paramAnonymousMotionEvent.getAction() == 1)
        {
          int i = MainActivity.this.checkModelStatus();
          if (i == 0)
          {
            MainActivity.this.mIsPlayFPV = true;
            bool = false;
          }
          else
          {
            if (i == 2131296344)
            {
              paramAnonymousMotionEvent = new TwoButtonPopDialog(MainActivity.this);
              paramAnonymousMotionEvent.adjustHeight(380);
              paramAnonymousMotionEvent.setTitle(2131296341);
              paramAnonymousMotionEvent.setMessage(2131296342);
              paramAnonymousMotionEvent.setPositiveButton(17039379, new View.OnClickListener()
              {
                public void onClick(View paramAnonymous2View)
                {
                  MainActivity.this.mIsPlayFPV = false;
                  MainActivity.this.mFlightToggle.setChecked(true);
                  paramAnonymousMotionEvent.dismiss();
                }
              });
              paramAnonymousMotionEvent.setNegativeButton(17039369, new View.OnClickListener()
              {
                public void onClick(View paramAnonymous2View)
                {
                  paramAnonymousMotionEvent.cancel();
                }
              });
              paramAnonymousMotionEvent.show();
            }
            for (;;)
            {
              paramAnonymousView.setPressed(false);
              break;
              MainActivity.this.showToastDontStack(i, 0);
            }
          }
        }
        else
        {
          bool = false;
        }
      }
    }
  };
  private boolean mFmodeBtnChecked = false;
  private Button mFmodeButton;
  private TextView mFmodeCH5;
  private TextView mFmodeCH6;
  private OneButtonPopDialog mFmodeFailDialog = null;
  private OneButtonPopDialog mGPSDisabledWarningDialog;
  private GPSUpdater mGPSUpdater;
  private boolean mGPSswtich = true;
  private GestureDetector mGesture;
  private Dialog mGuardDialog;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      }
      for (;;)
      {
        return;
        if ((MainActivity.this.mController != null) && ((paramAnonymousMessage.obj instanceof GPSUpLinkData)))
        {
          paramAnonymousMessage = (GPSUpLinkData)paramAnonymousMessage.obj;
          MainActivity.this.mController.updateGps(false, paramAnonymousMessage);
          try
          {
            paramAnonymousMessage = paramAnonymousMessage.toString().getBytes();
            MainActivity.this.flightLog.saveRemoteGpsFos(paramAnonymousMessage);
          }
          catch (Exception paramAnonymousMessage)
          {
            Log.e("FlightModeMainActivity", "write remote gps data error: " + paramAnonymousMessage);
          }
          continue;
          if (MainActivity.this.mController != null) {
            MainActivity.this.mController.updateCompass(false, paramAnonymousMessage.arg1);
          }
        }
      }
    }
  };
  private boolean mHasStartedRecord = false;
  private boolean mHasStoppedRecord = false;
  private HomeCompassView mHomeCompassView;
  private HttpRequestHandler mHttpHandler = new HttpRequestHandler(this);
  private Messenger mHttpResponseMessenger = new Messenger(this.mHttpHandler);
  private IPCameraManager mIPCameraManager;
  private List<String> mISOListValue;
  private ImageView mISOdecrement;
  private ImageView mISOincrement;
  private StyledNumberPicker mISOpicker;
  private IndicatorView mIndicatorALT;
  private IndicatorView mIndicatorDIS;
  private IndicatorView mIndicatorGPS;
  private IndicatorView mIndicatorGPSStatus;
  private IndicatorView mIndicatorMODE;
  private IndicatorView mIndicatorPOS;
  private IndicatorView mIndicatorTAS;
  private IndicatorView mIndicatorVOL;
  private boolean mInitiailDataTranfered = false;
  private boolean mInitiailized = false;
  private IntentFilter mIntentFilter = new IntentFilter();
  private boolean mIsLocalRecording;
  private boolean mIsPaused = false;
  private boolean mIsPlayFPV = false;
  private boolean mIsRecording = false;
  private boolean mIsThrottleSafe = false;
  private boolean mIsWifiDelayConnecting;
  private SurfaceView mLCDDislpayView;
  private boolean mLastAirportWarning = false;
  private boolean mLastAltitudeWarning = false;
  private boolean mLastGPSDisabledWarning = true;
  private IPCameraManager.SDCardStatus mLastSDcardStatus = null;
  private boolean mLastVoltageLowWarning1 = false;
  private boolean mLastVoltageLowWarning2 = false;
  private Animation mLeftFadeIn;
  private Animation mLeftFadeOut;
  private LeftTrimView mLeftTrimView;
  private TextView mLinkSpeed;
  private RelativeLayout mMainscreenLcdFrame;
  private Button mManualBtn;
  private LinearLayout mManualModeFrame;
  private MissionView mMissionView;
  private View.OnClickListener mModeSwitchListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (paramAnonymousView.equals(MainActivity.this.mAutoBtn))
      {
        MainActivity.this.mAutoModeFrame.setVisibility(8);
        MainActivity.this.mManualModeFrame.setVisibility(0);
        MainActivity.this.mCameraParams.ae_enable = 0;
        ((Amba2)MainActivity.this.mIPCameraManager).setAEenable(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.ae_enable);
        paramAnonymousView = MainActivity.this.wbIsoChangedListener;
        if (MainActivity.this.mCameraParams.ae_enable != 0) {
          break label160;
        }
      }
      label160:
      for (boolean bool = true;; bool = false)
      {
        paramAnonymousView.onIsoModeChanged(bool);
        return;
        if (!paramAnonymousView.equals(MainActivity.this.mManualBtn)) {
          break;
        }
        MainActivity.this.mAutoModeFrame.setVisibility(0);
        MainActivity.this.mManualModeFrame.setVisibility(8);
        MainActivity.this.mCameraParams.ae_enable = 1;
        break;
      }
    }
  };
  private CounterView mOSDTime;
  private WhiteBalanceScrollView.OnItemSelectedListener mOnItemSelectedListener = new WhiteBalanceScrollView.OnItemSelectedListener()
  {
    public void onItemSelected(int paramAnonymousInt)
    {
      MainActivity.this.mWhitBalanceList.setItemSelect(paramAnonymousInt);
      MainActivity.this.wbIsoChangedListener.onWbModeChanged(paramAnonymousInt, true);
      paramAnonymousInt = ((Integer)MainActivity.mWBList.get(paramAnonymousInt)).intValue();
      MainActivity.this.showCommunicatingDialog();
      ((Amba2)MainActivity.this.mIPCameraManager).setWhiteBalance(MainActivity.this.mHttpResponseMessenger, paramAnonymousInt);
    }
  };
  private StyledNumberPicker.OnButtonClickedListener mPickerButtonListener = new StyledNumberPicker.OnButtonClickedListener()
  {
    public void OnButtonClicked(StyledNumberPicker paramAnonymousStyledNumberPicker, int paramAnonymousInt)
    {
      if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mEVpicker))
      {
        paramAnonymousInt = MainActivity.this.mEVpicker.getValue();
        if (paramAnonymousInt == MainActivity.this.mEVpicker.getMaxValue())
        {
          MainActivity.this.mEVdecrement.setVisibility(4);
          MainActivity.this.mCameraParams.exposure_value = ((String)MainActivity.this.mEVListValue.get(paramAnonymousInt));
          Log.d("FlightModeMainActivity", "Set EV to: " + MainActivity.this.mCameraParams.exposure_value);
          ((Amba2)MainActivity.this.mIPCameraManager).setExposure(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.exposure_value);
        }
      }
      do
      {
        return;
        if (paramAnonymousInt == MainActivity.this.mEVpicker.getMinValue())
        {
          MainActivity.this.mEVincrement.setVisibility(4);
          break;
        }
        MainActivity.this.mEVincrement.setVisibility(0);
        MainActivity.this.mEVdecrement.setVisibility(0);
        break;
        if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mISOpicker))
        {
          paramAnonymousInt = MainActivity.this.mISOpicker.getValue();
          if (paramAnonymousInt == MainActivity.this.mISOpicker.getMaxValue()) {
            MainActivity.this.mISOdecrement.setVisibility(4);
          }
          for (;;)
          {
            MainActivity.this.mCameraParams.iso = ((String)MainActivity.this.mISOListValue.get(paramAnonymousInt));
            Log.d("FlightModeMainActivity", "Set ISO to: " + MainActivity.this.mCameraParams.iso);
            ((Amba2)MainActivity.this.mIPCameraManager).setShutterTimeAndISO(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.shutter_time, MainActivity.this.mCameraParams.iso);
            break;
            if (paramAnonymousInt == MainActivity.this.mEVpicker.getMinValue())
            {
              MainActivity.this.mISOincrement.setVisibility(4);
            }
            else
            {
              MainActivity.this.mISOincrement.setVisibility(0);
              MainActivity.this.mISOdecrement.setVisibility(0);
            }
          }
        }
      } while (!paramAnonymousStyledNumberPicker.equals(MainActivity.this.mSTpicker));
      paramAnonymousInt = MainActivity.this.mSTpicker.getValue();
      if (paramAnonymousInt == MainActivity.this.mSTpicker.getMaxValue()) {
        MainActivity.this.mSTdecrement.setVisibility(4);
      }
      for (;;)
      {
        MainActivity.this.mCameraParams.shutter_time = Integer.parseInt((String)MainActivity.this.mSHTimeListValue.get(MainActivity.this.mSTpicker.getValue()));
        Log.d("FlightModeMainActivity", "Set shutter time to: " + MainActivity.this.mCameraParams.shutter_time);
        ((Amba2)MainActivity.this.mIPCameraManager).setShutterTimeAndISO(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.shutter_time, MainActivity.this.mCameraParams.iso);
        break;
        if (paramAnonymousInt == MainActivity.this.mEVpicker.getMinValue())
        {
          MainActivity.this.mSTincrement.setVisibility(4);
        }
        else
        {
          MainActivity.this.mSTincrement.setVisibility(0);
          MainActivity.this.mSTdecrement.setVisibility(0);
        }
      }
    }
  };
  private StyledNumberPicker.OnValueChangeListener mPickerChangedListener = new StyledNumberPicker.OnValueChangeListener()
  {
    public void onValueChange(StyledNumberPicker paramAnonymousStyledNumberPicker, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mEVpicker)) {
        if (paramAnonymousInt2 == MainActivity.this.mEVpicker.getMaxValue()) {
          MainActivity.this.mEVdecrement.setVisibility(4);
        }
      }
      for (;;)
      {
        return;
        if (paramAnonymousInt2 == MainActivity.this.mEVpicker.getMinValue())
        {
          MainActivity.this.mEVincrement.setVisibility(4);
        }
        else
        {
          MainActivity.this.mEVincrement.setVisibility(0);
          MainActivity.this.mEVdecrement.setVisibility(0);
          continue;
          if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mISOpicker))
          {
            if (paramAnonymousInt2 == MainActivity.this.mISOpicker.getMaxValue())
            {
              MainActivity.this.mISOdecrement.setVisibility(4);
            }
            else if (paramAnonymousInt2 == MainActivity.this.mEVpicker.getMinValue())
            {
              MainActivity.this.mISOincrement.setVisibility(4);
            }
            else
            {
              MainActivity.this.mISOincrement.setVisibility(0);
              MainActivity.this.mISOdecrement.setVisibility(0);
            }
          }
          else if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mSTpicker)) {
            if (paramAnonymousInt2 == MainActivity.this.mSTpicker.getMaxValue())
            {
              MainActivity.this.mSTdecrement.setVisibility(4);
            }
            else if (paramAnonymousInt2 == MainActivity.this.mEVpicker.getMinValue())
            {
              MainActivity.this.mSTincrement.setVisibility(4);
            }
            else
            {
              MainActivity.this.mSTincrement.setVisibility(0);
              MainActivity.this.mSTdecrement.setVisibility(0);
            }
          }
        }
      }
    }
  };
  private StyledNumberPicker.OnScrollListener mPickerScrollListener = new StyledNumberPicker.OnScrollListener()
  {
    public void onScrollStateChange(StyledNumberPicker paramAnonymousStyledNumberPicker, int paramAnonymousInt)
    {
      if (paramAnonymousInt == 0)
      {
        if (!paramAnonymousStyledNumberPicker.equals(MainActivity.this.mEVpicker)) {
          break label117;
        }
        paramAnonymousInt = MainActivity.this.mEVpicker.getValue();
        MainActivity.this.mCameraParams.exposure_value = ((String)MainActivity.this.mEVListValue.get(paramAnonymousInt));
        Log.d("FlightModeMainActivity", "Set EV to: " + MainActivity.this.mCameraParams.exposure_value);
        ((Amba2)MainActivity.this.mIPCameraManager).setExposure(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.exposure_value);
      }
      for (;;)
      {
        return;
        label117:
        if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mISOpicker))
        {
          paramAnonymousInt = MainActivity.this.mISOpicker.getValue();
          MainActivity.this.mCameraParams.iso = ((String)MainActivity.this.mISOListValue.get(paramAnonymousInt));
          Log.d("FlightModeMainActivity", "Set ISO to: " + MainActivity.this.mCameraParams.iso);
          ((Amba2)MainActivity.this.mIPCameraManager).setShutterTimeAndISO(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.shutter_time, MainActivity.this.mCameraParams.iso);
        }
        else if (paramAnonymousStyledNumberPicker.equals(MainActivity.this.mSTpicker))
        {
          paramAnonymousInt = MainActivity.this.mSTpicker.getValue();
          MainActivity.this.mCameraParams.shutter_time = Integer.parseInt((String)MainActivity.this.mSHTimeListValue.get(paramAnonymousInt));
          Log.d("FlightModeMainActivity", "Set shutter time to: " + MainActivity.this.mCameraParams.shutter_time);
          ((Amba2)MainActivity.this.mIPCameraManager).setShutterTimeAndISO(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraParams.shutter_time, MainActivity.this.mCameraParams.iso);
        }
      }
    }
  };
  private boolean mPostSendCommand = false;
  private SharedPreferences mPref;
  private MyProgressDialog mProgressDialog;
  private RTVPlayer mRTVPlayer;
  private CounterView.OnTickListener mRecTickListener = new CounterView.OnTickListener()
  {
    public void onAlmostEnd() {}
    
    public void onEnd()
    {
      MainActivity.this.mBtnRecord.setChecked(false);
    }
    
    public void onTick(int paramAnonymousInt) {}
  };
  private CounterView mRecTime;
  private Runnable mReconnectCameraRunnable = new Runnable()
  {
    public void run()
    {
      if (!MainActivity.this.mIsPaused) {
        MainActivity.this.mRTVPlayer.play(MainActivity.this.mCurrentVideoLocation);
      }
      for (;;)
      {
        return;
        Log.d("FlightModeMainActivity", "activity paused,reconnecting ignored");
      }
    }
  };
  private boolean mRecordProcessing = false;
  private CompoundButton.OnCheckedChangeListener mRecordStateListener = new CompoundButton.OnCheckedChangeListener()
  {
    public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
    {
      Log.i("FlightModeMainActivity", "#### Rec mRecordStateListener->isChecked:[" + paramAnonymousBoolean + "], mIsRecording:[" + MainActivity.this.mIsRecording + "] ,mRecTime.isStarted():[" + MainActivity.this.mRecTime.isStarted() + "]");
      if ((paramAnonymousBoolean) && (!MainActivity.this.mIsRecording))
      {
        MainActivity.this.mIPCameraManager.startRecord(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraInfo);
        MainActivity.this.startWaitingAnimation(MainActivity.this.mBtnRecord);
        MainActivity.this.mStatusBarView.setInfoText(MainActivity.this.getResources().getString(2131296353), 0);
      }
      for (;;)
      {
        return;
        if ((!paramAnonymousBoolean) && (MainActivity.this.mIsRecording))
        {
          MainActivity.this.mIPCameraManager.stopRecord(MainActivity.this.mHttpResponseMessenger, MainActivity.this.mCameraInfo);
          MainActivity.this.startWaitingAnimation(MainActivity.this.mBtnRecord);
          MainActivity.this.mStatusBarView.setInfoText(MainActivity.this.getResources().getString(2131296354), 0);
        }
        else if ((!paramAnonymousBoolean) && (!MainActivity.this.mIsRecording))
        {
          if (MainActivity.this.mRecTime.isStarted())
          {
            MainActivity.this.mRecTime.stop();
            MainActivity.this.mRecTime.setVisibility(4);
            if ((MainActivity.this.mCurrentCameraName.equals("C-GO3-Pro")) || (MainActivity.this.mCurrentCameraName.equals("C-GO3"))) {
              MainActivity.this.mSRswitch.setVisibility(0);
            }
          }
        }
        else if ((paramAnonymousBoolean) && (MainActivity.this.mIsRecording) && (!MainActivity.this.mRecTime.isStarted()))
        {
          MainActivity.this.mIPCameraManager.getRecordTime(MainActivity.this.mHttpResponseMessenger);
        }
      }
    }
  };
  private int mRecorderShutterSoundId;
  private TextView mRemoteGPSdata;
  private String mResolution;
  private Animation mRightFadeIn;
  private Animation mRightFadeOut;
  private RightTrimView mRightTrimView;
  private boolean mRlnBtnPressed = false;
  private Button mRlnButton;
  private Runnable mRlnProgressRunnable = new Runnable()
  {
    public void run()
    {
      if ((MainActivity.this.mProgressDialog != null) && (MainActivity.this.mProgressDialog.isShowing())) {
        MainActivity.this.mProgressDialog.dismiss();
      }
      MainActivity.this.mStatusBarView.setInfoText(MainActivity.this.getResources().getString(2131296392), 0);
    }
  };
  private ResolutionSelection mRlnSelection;
  private TextView mRlnText;
  private TextView mRssi;
  private boolean mSDErrDialogShown = false;
  private List<String> mSHTimeListValue;
  private SyncToggleButton mSRswitch;
  private Runnable mSRswitchCacheRunnable = new Runnable()
  {
    public void run()
    {
      SyncToggleButton localSyncToggleButton = MainActivity.this.mSRswitch;
      if (MainActivity.this.mCameraParams.cam_mode == 2) {}
      for (boolean bool = false;; bool = true)
      {
        localSyncToggleButton.syncState(bool);
        MainActivity.this.cameraModeCache = 0;
        Log.i("FlightModeMainActivity", "CameraMode--switch camera mode timeout");
        return;
      }
    }
  };
  private TextView mSSID;
  private ImageView mSTdecrement;
  private ImageView mSTincrement;
  private StyledNumberPicker mSTpicker;
  private boolean mSnapShotProcessing = false;
  private SoundPool mSoundPool;
  private StatusbarView mStatusBarView;
  private TimerHelper mTimerHelper;
  private Toast mToast;
  private Animation mTopFadeIn;
  private Animation mTopFadeOut;
  private Animation mTrimLeftFadeIn;
  private Animation mTrimLeftFadeOut;
  private Animation mTrimRightFadeIn;
  private Animation mTrimRightFadeOut;
  private Handler mUartHandler = new Handler()
  {
    private final int MAX_TIME_GAP_THRESHOLD = 5000;
    long currentFeedbackTimestamp = System.currentTimeMillis();
    private boolean isChangeStatus = true;
    boolean one_flight = true;
    
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MainActivity.this.mController == null) {
        Log.i("FlightModeMainActivity", "the activity was paused");
      }
      for (;;)
      {
        return;
        if ((paramAnonymousMessage.obj instanceof UARTInfoMessage)) {
          paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        }
        switch (paramAnonymousMessage.what)
        {
        default: 
          label100:
          if ((this.isChangeStatus) && (MainActivity.this.mConnectingDialog != null))
          {
            if (!MainActivity.this.isWIFIConneted) {
              break label706;
            }
            MainActivity.this.mConnectingDialog.setMessage(MainActivity.this.getResources().getText(2131296373));
          }
          break;
        }
        for (;;)
        {
          long l = System.currentTimeMillis() - this.currentFeedbackTimestamp;
          if ((l > 5000L) && (MainActivity.this.isFSKConneted))
          {
            Log.i("FlightModeMainActivity", "FSK may disconnected!");
            this.isChangeStatus = true;
            MainActivity.this.isFSKConneted = false;
            this.one_flight = true;
            MainActivity.this.mCameraSettingsBtn.setVisibility(0);
            if (!MainActivity.this.isWIFIConneted) {
              MainActivity.this.mCameraSettingsBtn.setEnabled(false);
            }
            MainActivity.this.initIndicatorValue();
            MainActivity.this.resetWarnings();
            MainActivity.this.flightLog.closedFlightNote(false);
          }
          if ((l > 5000L) || (MainActivity.this.isFSKConneted)) {
            break;
          }
          this.isChangeStatus = false;
          MainActivity.this.isFSKConneted = true;
          MainActivity.this.mCameraSettingsBtn.setEnabled(true);
          if (MainActivity.this.mConnectingDialog == null) {
            break;
          }
          if (!MainActivity.this.isWIFIConneted) {
            break label732;
          }
          MainActivity.this.mConnectingDialog.setMessage(MainActivity.this.getResources().getText(2131296374));
          break;
          paramAnonymousMessage = (UARTInfoMessage.Channel)paramAnonymousMessage;
          MainActivity.this.handleZoomAction(paramAnonymousMessage);
          break label100;
          UARTInfoMessage.Channel localChannel = (UARTInfoMessage.Channel)paramAnonymousMessage;
          MainActivity.this.handleTimerTirgger(localChannel);
          if (localChannel.channels.size() <= 0) {
            break label100;
          }
          paramAnonymousMessage = MainActivity.this;
          if (((Float)localChannel.channels.get(0)).floatValue() > 100.0F) {}
          for (boolean bool = false;; bool = true)
          {
            paramAnonymousMessage.mIsThrottleSafe = bool;
            ChannelDataForward.getInstance().forwardMixChannelData(localChannel);
            try
            {
              paramAnonymousMessage = localChannel.toString().getBytes();
              MainActivity.this.flightLog.saveRemoteFos(paramAnonymousMessage);
            }
            catch (Exception paramAnonymousMessage)
            {
              Log.e("FlightModeMainActivity", "write remote data error: " + paramAnonymousMessage);
            }
            break;
          }
          paramAnonymousMessage = (UARTInfoMessage.Telemetry)paramAnonymousMessage;
          MainActivity.this.handleTelemetryInfoChanged(paramAnonymousMessage);
          if (paramAnonymousMessage != null) {}
          try
          {
            paramAnonymousMessage = paramAnonymousMessage.toString().getBytes();
            if (paramAnonymousMessage.length > 0)
            {
              if (this.one_flight)
              {
                MainActivity.this.flightLog.createFlightNote(MainActivity.this);
                this.one_flight = false;
              }
              MainActivity.this.flightLog.saveTelemetryFos(paramAnonymousMessage);
            }
          }
          catch (Exception paramAnonymousMessage)
          {
            for (;;)
            {
              Log.e("FlightModeMainActivity", "write error: " + paramAnonymousMessage);
            }
          }
          this.currentFeedbackTimestamp = System.currentTimeMillis();
          break label100;
          paramAnonymousMessage = (UARTInfoMessage.SwitchChanged)paramAnonymousMessage;
          MainActivity.this.handleSwitchChanged(paramAnonymousMessage);
          break label100;
          paramAnonymousMessage = (UARTInfoMessage.Trim)paramAnonymousMessage;
          MainActivity.this.mRightTrimView.setValue(MainActivity.this.trimDataConversion(paramAnonymousMessage.t1), MainActivity.this.trimDataConversion(paramAnonymousMessage.t2));
          MainActivity.this.mLeftTrimView.setValue(MainActivity.this.trimDataConversion(paramAnonymousMessage.t3), MainActivity.this.trimDataConversion(paramAnonymousMessage.t4));
          break label100;
          if (!"ST10".equals("ST12")) {
            break label100;
          }
          MainActivity.this.mMissionView.updateFeedback((UARTInfoMessage.MissionReply)paramAnonymousMessage);
          break label100;
          label706:
          MainActivity.this.mConnectingDialog.setMessage(MainActivity.this.getResources().getText(2131296371));
        }
        label732:
        MainActivity.this.mConnectingDialog.setMessage(MainActivity.this.getResources().getText(2131296372));
      }
    }
  };
  private int mValueForZoom = 1;
  private int mVehicleType;
  private Vibrator mVibrator;
  private RTVPlayer.VideoEventCallback mVideoEventCallback = new RTVPlayer.VideoEventCallback()
  {
    public void onPlayerEncoutneredError()
    {
      MainActivity.this.reconnectCamera();
    }
    
    public void onPlayerEndReached()
    {
      MainActivity.this.reconnectCamera();
    }
    
    public void onPlayerPlayerRecordingFinished() {}
    
    public void onPlayerPlaying()
    {
      Log.d("FlightModeMainActivity", "#### Rec RTV playing...");
      if (MainActivity.this.mIsLocalRecording)
      {
        MainActivity.this.mRTVPlayer.startRecord("/sdcard/FPV-Video/Local/" + MainActivity.this.prepareFilename());
        MainActivity.this.mIPCameraManager.getRecordTime(MainActivity.this.mHttpResponseMessenger);
      }
      MainActivity.this.dismissProgressDialog(null);
    }
    
    public void onPlayerRecordableChanged() {}
    
    public void onPlayerSnapshotTaken() {}
    
    public void onPlayerStopped() {}
    
    public void onPlayerSurfaceChanged(SurfaceHolder paramAnonymousSurfaceHolder, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    
    public void onPlayerSurfaceCreated(SurfaceHolder paramAnonymousSurfaceHolder) {}
    
    public void onPlayerSurfaceDestroyed(SurfaceHolder paramAnonymousSurfaceHolder) {}
  };
  private Dialog mVoltageLowWarning1Dialog;
  private Dialog mVoltageLowWarning2Dialog;
  private WhiteBalanceScrollView mWhitBalanceList;
  private Runnable mWifiEnabledRunnable = new Runnable()
  {
    public void run()
    {
      Log.d("FlightModeMainActivity", "mWifiEnabledRunnable run!");
      MainActivity.this.isWIFIConneted = false;
      if (MainActivity.this.isBindWifi(MainActivity.this, MainActivity.this.mCurrentModelId))
      {
        MainActivity.this.showConnectingDialog();
        MainActivity.this.connectWifi();
      }
      MainActivity.this.mIsWifiDelayConnecting = false;
    }
  };
  private TextView mWifiState;
  private WifiThread mWifiThread;
  private Runnable mZoomRunnable = new Runnable()
  {
    public void run()
    {
      if (MainActivity.this.mIPCameraManager != null) {
        MainActivity.this.mIPCameraManager.zoom(MainActivity.this.mValueForZoom, MainActivity.this.mHttpResponseMessenger);
      }
    }
  };
  private VerticalSeekBar mZoombar;
  private View mZoombarContainer;
  private boolean motorStatusErr = false;
  private BroadcastReceiver receiverWifi = new BroadcastReceiver()
  {
    private Handler heartBeatHandler;
    
    private void disableWifiDelay()
    {
      if (MainActivity.this.mWifiEnabledRunnable == null) {}
      for (;;)
      {
        return;
        if (MainActivity.this.mIsWifiDelayConnecting)
        {
          MainActivity.this.mIsWifiDelayConnecting = false;
          MainActivity.this.mHandler.removeCallbacks(MainActivity.this.mWifiEnabledRunnable);
        }
      }
    }
    
    private void enableWifiDelay()
    {
      if (MainActivity.this.mWifiEnabledRunnable == null) {}
      for (;;)
      {
        return;
        if (!MainActivity.this.mIsWifiDelayConnecting)
        {
          MainActivity.this.mIsWifiDelayConnecting = true;
          MainActivity.this.mHandler.removeCallbacks(MainActivity.this.mWifiEnabledRunnable);
          MainActivity.this.mHandler.postDelayed(MainActivity.this.mWifiEnabledRunnable, 300L);
        }
      }
    }
    
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      String str;
      if (MainActivity.this.wifiManager.isWifiEnabled())
      {
        str = paramAnonymousIntent.getAction();
        Log.i("FlightModeMainActivity", str);
        if (!str.equals("android.net.wifi.SCAN_RESULTS")) {
          break label57;
        }
        Log.v("FlightModeMainActivity", " ----- connecting I---" + str);
      }
      for (;;)
      {
        return;
        label57:
        if ((str.equals("android.net.wifi.WIFI_STATE_CHANGED")) || (str.equals("android.net.wifi.supplicant.STATE_CHANGE")))
        {
          Log.v("FlightModeMainActivity", " ----- connecting II---" + str);
          if (paramAnonymousIntent.getIntExtra("wifi_state", 4) == 3) {
            enableWifiDelay();
          }
        }
        else if (str.equals("android.net.wifi.STATE_CHANGE"))
        {
          paramAnonymousIntent = ((NetworkInfo)paramAnonymousIntent.getParcelableExtra("networkInfo")).getState();
          Log.d("FlightModeMainActivity", "NETWORK_STATE_CHANGED_ACTION:" + paramAnonymousIntent.name() + ", mIsWifiDelayConnecting=" + MainActivity.this.mIsWifiDelayConnecting);
          if (NetworkInfo.State.CONNECTED == paramAnonymousIntent)
          {
            MainActivity.this.isWIFIConneted = true;
            MainActivity.this.mCameraSettingsBtn.setEnabled(true);
            if (MainActivity.this.mCurrentModelType == 406L) {
              MainActivity.this.mHandler.postDelayed(new Runnable()
              {
                public void run()
                {
                  ChannelDataForward.getInstance().bind();
                }
              }, 1000L);
            }
            if (MainActivity.this.mConnectingDialog != null)
            {
              if (MainActivity.this.isFSKConneted) {
                MainActivity.this.mConnectingDialog.setMessage(MainActivity.this.getResources().getText(2131296374));
              }
            }
            else
            {
              label281:
              MainActivity.this.dismissConnectingDialog();
              disableWifiDelay();
              if (Utilities.isValidWifi(paramAnonymousContext, MainActivity.this.mCurrentModelId)) {
                break label412;
              }
              enableWifiDelay();
            }
            for (;;)
            {
              if (MainActivity.this.handlerThread == null)
              {
                MainActivity.this.handlerThread = new HandlerThread("Heartbeat");
                MainActivity.this.handlerThread.start();
                this.heartBeatHandler = new HeartbeatEventHandler(MainActivity.this.handlerThread.getLooper());
              }
              if (this.heartBeatHandler == null) {
                break;
              }
              this.heartBeatHandler.sendEmptyMessage(1);
              break;
              MainActivity.this.mConnectingDialog.setMessage(MainActivity.this.getResources().getText(2131296373));
              break label281;
              label412:
              MainActivity.this.mIPCameraManager.initCamera(MainActivity.this.mHttpResponseMessenger);
              Log.i("FlightModeMainActivity", "Valid wifi connected ,init Camera");
            }
          }
          if (NetworkInfo.State.DISCONNECTED == paramAnonymousIntent)
          {
            enableWifiDelay();
            MainActivity.this.onWiFiConntectionLost();
            if (!MainActivity.this.isFSKConneted) {
              MainActivity.this.mCameraSettingsBtn.setEnabled(false);
            }
            if (this.heartBeatHandler != null) {
              this.heartBeatHandler.sendEmptyMessage(3);
            }
            if (MainActivity.this.handlerThread != null)
            {
              MainActivity.this.handlerThread.quit();
              MainActivity.this.handlerThread.interrupt();
              MainActivity.this.handlerThread = null;
            }
          }
          else if (NetworkInfo.State.CONNECTING == paramAnonymousIntent)
          {
            disableWifiDelay();
          }
        }
      }
    }
    
    class HeartbeatEventHandler
      extends Handler
    {
      private static final int PING_ROUTER = 2;
      public static final int START_PING = 1;
      public static final int STOP_PING = 3;
      private int missPingCount;
      private String routerAddress;
      private boolean stopPing = true;
      
      public HeartbeatEventHandler(Looper paramLooper)
      {
        super();
      }
      
      private String executeCmd(String paramString, boolean paramBoolean)
      {
        if (!paramBoolean) {}
        for (;;)
        {
          try
          {
            paramString = Runtime.getRuntime().exec(paramString);
            BufferedReader localBufferedReader = new java/io/BufferedReader;
            localObject = new java/io/InputStreamReader;
            ((InputStreamReader)localObject).<init>(paramString.getInputStream());
            localBufferedReader.<init>((Reader)localObject);
            paramString = "";
            str = localBufferedReader.readLine();
            if (str != null) {
              continue;
            }
          }
          catch (Exception paramString)
          {
            Object localObject;
            String str;
            paramString.printStackTrace();
            paramString = "";
            continue;
          }
          return paramString;
          paramString = Runtime.getRuntime().exec(new String[] { "su", "-c", paramString });
          continue;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>(String.valueOf(paramString));
          paramString = str + "\n";
        }
      }
      
      public void handleMessage(Message paramMessage)
      {
        switch (paramMessage.what)
        {
        }
        for (;;)
        {
          return;
          this.routerAddress = Formatter.formatIpAddress(((WifiManager)MainActivity.this.getSystemService("wifi")).getDhcpInfo().gateway);
          if (this.routerAddress != null)
          {
            Log.d("FlightModeMainActivity", "Get gateway ip: " + this.routerAddress);
            this.stopPing = false;
            this.missPingCount = 0;
            sendEmptyMessageDelayed(2, 1000L);
            continue;
            if (!this.stopPing)
            {
              boolean bool = executeCmd("ping -c 1 -w 1 " + this.routerAddress, false).contains(" 0% packet loss,");
              Log.d("FlightModeMainActivity", "Ping result, isPass: " + bool);
              if (!bool)
              {
                int i = this.missPingCount + 1;
                this.missPingCount = i;
                if (i > 5)
                {
                  executeCmd("ifconfig wlan0 down", false);
                  executeCmd("ifconfig wlan0 up", false);
                  Log.d("FlightModeMainActivity", "restart wlan0");
                }
              }
              for (this.missPingCount = 0;; this.missPingCount = 0)
              {
                sendEmptyMessageDelayed(2, 1000L);
                break;
              }
              this.stopPing = true;
            }
          }
        }
      }
    }
  };
  private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener()
  {
    public void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
    {
      Log.i("FlightModeMainActivity", "****BAR****, CHG:" + paramAnonymousInt);
      paramAnonymousInt++;
      if (MainActivity.this.mValueForZoom != paramAnonymousInt)
      {
        MainActivity.this.mHandler.removeCallbacks(MainActivity.this.mZoomRunnable);
        MainActivity.this.mValueForZoom = paramAnonymousInt;
        MainActivity.this.mHandler.postDelayed(MainActivity.this.mZoomRunnable, 200L);
      }
    }
    
    public void onStartTrackingTouch(SeekBar paramAnonymousSeekBar)
    {
      Log.i("FlightModeMainActivity", "****BAR****, START:" + paramAnonymousSeekBar.getProgress());
    }
    
    public void onStopTrackingTouch(SeekBar paramAnonymousSeekBar)
    {
      Log.i("FlightModeMainActivity", "****BAR****, STOP:" + paramAnonymousSeekBar.getProgress());
    }
  };
  private ToggleButton sonarButton;
  private boolean validVehicle = false;
  private WbIsoChangedListener wbIsoChangedListener;
  private FrameLayout wbIsoFrame;
  private WifiManager wifiManager;
  private ToggleButton yawModeButton;
  
  static
  {
    mTmpVOLFloatValue = 0.0F;
    mIndex = 0;
    mWBList = new ArrayList();
    mWBList.add(0, Integer.valueOf(0));
    mWBList.add(1, Integer.valueOf(99));
    mWBList.add(2, Integer.valueOf(4));
    mWBList.add(3, Integer.valueOf(5));
    mWBList.add(4, Integer.valueOf(7));
    mWBList.add(5, Integer.valueOf(1));
    mWBList.add(6, Integer.valueOf(3));
  }
  
  private void changeSRstateCache(int paramInt)
  {
    if (this.mSRswitchCacheRunnable != null) {
      this.mHandler.removeCallbacks(this.mSRswitchCacheRunnable);
    }
    this.cameraModeCache = paramInt;
    Log.d("FlightModeMainActivity", "CameraMode-- changeSRstateCache=" + paramInt + ",cancel change after 10200 millis");
    this.mHandler.postDelayed(this.mSRswitchCacheRunnable, 10200L);
  }
  
  private boolean checkIsFPVModel()
  {
    boolean bool = true;
    if (Utilities.isFPVModel(this, this.mCurrentModelId) == 1) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  private int checkModelStatus()
  {
    int i = 2131296344;
    if (!checkIsFPVModel()) {
      i = 2131296358;
    }
    for (;;)
    {
      return i;
      WifiManager localWifiManager = (WifiManager)getSystemService("wifi");
      if (!localWifiManager.isWifiEnabled()) {
        Log.i("FlightModeMainActivity", "Wifi is not opened");
      } else if (localWifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.COMPLETED)) {
        i = 0;
      }
    }
  }
  
  private void connectWifi()
  {
    if (this.mCurrentModelId != -2L) {
      Utilities.connectModelWifi(this, this.mCurrentModelId);
    }
  }
  
  private void createFmodeMenu()
  {
    View localView = View.inflate(this, 2130903063, null);
    this.mFModeList = ((ListView)localView.findViewById(2131689599));
    ArrayList localArrayList = new ArrayList();
    for (int i = 0;; i++)
    {
      if (i >= 3)
      {
        localObject = new SimpleAdapter(this, localArrayList, 2130903062, new String[] { "name", "status1", "status2", "status3" }, new int[] { 2131689595, 2131689596, 2131689597, 2131689598 });
        this.mFModeList.setAdapter((ListAdapter)localObject);
        this.mFModeList.setChoiceMode(1);
        this.mFModeList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            MainActivity.this.mPref.edit().putInt("fmode_option", paramAnonymousInt).commit();
            MainActivity.this.sendFmodeData(paramAnonymousInt);
            MainActivity.this.mFModeOptions.dismiss();
          }
        });
        this.mFModeOptions = new AlertDialog.Builder(this).setView(localView).create();
        i = this.mPref.getInt("fmode_option", 1);
        this.mFModeList.setSelection(i);
        return;
      }
      Object localObject = new HashMap();
      ((HashMap)localObject).put("name", getString(new int[] { 2131296360, 2131296361, 2131296362 }[i]));
      ((HashMap)localObject).put("status1", getString(new int[] { 2131296366, 2131296366, 2131296366 }[i]));
      ((HashMap)localObject).put("status2", getString(new int[] { 2131296363, 2131296364, 2131296365 }[i]));
      ((HashMap)localObject).put("status3", getString(new int[] { 2131296367, 2131296367, 2131296367 }[i]));
      localArrayList.add(localObject);
    }
  }
  
  private void dismissAirportWarning()
  {
    if ((this.mAirportWarningDialog != null) && (this.mAirportWarningDialog.isShowing())) {
      this.mAirportWarningDialog.dismiss();
    }
  }
  
  private void dismissAltitudeWarning()
  {
    if ((this.mAltitudeWarningDialog != null) && (this.mAltitudeWarningDialog.isShowing())) {
      this.mAltitudeWarningDialog.dismiss();
    }
  }
  
  private void dismissCommunicatingDialog()
  {
    if ((this.mCommunicating != null) && (this.mCommunicating.isShowing()))
    {
      this.mCommunicating.dismiss();
      this.mHandler.removeCallbacks(this.mCommunicatingRunnable);
      this.mCommunicating = null;
    }
  }
  
  private void dismissConnectingDialog()
  {
    if ((this.mConnectingDialog != null) && (this.mConnectingDialog.isShowing())) {
      this.mConnectingDialog.dismiss();
    }
  }
  
  private void dismissFModeFailDialog()
  {
    if ((this.mFmodeFailDialog != null) && (this.mFmodeFailDialog.isShowing())) {
      this.mFmodeFailDialog.dismiss();
    }
  }
  
  private void dismissGPSDisabledWarning()
  {
    if ((this.mGPSDisabledWarningDialog != null) && (this.mGPSDisabledWarningDialog.isShowing())) {
      this.mGPSDisabledWarningDialog.dismiss();
    }
  }
  
  private void dismissProgressDialog(Runnable paramRunnable)
  {
    if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing())) {
      this.mProgressDialog.dismiss();
    }
    if (paramRunnable != null) {
      this.mHandler.removeCallbacks(paramRunnable);
    }
  }
  
  private void dismissVoltageLowWarning1()
  {
    if ((this.mVoltageLowWarning1Dialog != null) && (this.mVoltageLowWarning1Dialog.isShowing())) {
      this.mVoltageLowWarning1Dialog.dismiss();
    }
  }
  
  private void dismissVoltageLowWarning2()
  {
    if ((this.mVoltageLowWarning2Dialog != null) && (this.mVoltageLowWarning2Dialog.isShowing())) {
      this.mVoltageLowWarning2Dialog.dismiss();
    }
  }
  
  private void enableButtonBar(boolean paramBoolean)
  {
    this.mBtnFlightSetting.setEnabled(true);
    this.mBtnModelSelect.setEnabled(true);
    this.mBtnSystemSetting.setEnabled(true);
  }
  
  private int getVoltageLevel(float paramFloat, int paramInt)
  {
    int i = 0;
    if (paramInt == 5) {
      if (paramFloat >= 15.4F) {
        paramInt = 100;
      }
    }
    for (;;)
    {
      return paramInt;
      if ((paramFloat >= 14.9F) && (paramFloat < 15.4F))
      {
        paramInt = Math.round((paramFloat - 14.9F) * 50.0F / (15.4F - 14.9F) + 50.0F);
      }
      else if ((paramFloat > 14.2F) && (paramFloat < 14.9F))
      {
        paramInt = Math.round((paramFloat - 14.2F) * 25.0F / (14.9F - 14.2F) + 25.0F);
      }
      else if ((paramFloat > 14.0F) && (paramFloat <= 14.2F))
      {
        paramInt = Math.round((paramFloat - 14.0F) * 20.0F / (14.2F - 14.0F) + 5.0F);
      }
      else if ((paramFloat > 13.8F) && (paramFloat <= 14.0F))
      {
        paramInt = Math.round((paramFloat - 13.8F) * 5.0F / (14.2F - 14.0F));
      }
      else
      {
        paramInt = i;
        if (paramFloat <= 13.8F)
        {
          paramInt = 0;
          continue;
          if ((paramInt == 2) || (paramInt == 4))
          {
            if (paramFloat >= 12.6F)
            {
              paramInt = 100;
            }
            else if ((paramFloat >= 10.7F) && (paramFloat < 12.6F))
            {
              paramInt = Math.round((paramFloat - 10.7F) * 75.0F / (12.6F - 10.7F) + 25.0F);
            }
            else if ((paramFloat > 10.5F) && (paramFloat < 10.7F))
            {
              paramInt = Math.round((paramFloat - 10.5F) * 20.0F / (10.7F - 10.5F) + 5.0F);
            }
            else if ((paramFloat > 10.3F) && (paramFloat <= 10.5F))
            {
              paramInt = Math.round((paramFloat - 10.3F) * 5.0F / (10.5F - 10.3F));
            }
            else
            {
              paramInt = i;
              if (paramFloat <= 10.3F) {
                paramInt = 0;
              }
            }
          }
          else if (paramInt == 1)
          {
            if (paramFloat >= 25.2F)
            {
              paramInt = 100;
            }
            else if ((paramFloat >= 23.9F) && (paramFloat < 25.2F))
            {
              paramInt = Math.round((paramFloat - 23.9F) * 5.0F / (25.2F - 23.9F) + 95.0F);
            }
            else if ((paramFloat >= 21.7F) && (paramFloat < 23.9F))
            {
              paramInt = Math.round((paramFloat - 21.7F) * 75.0F / (23.9F - 21.7F) + 20.0F);
            }
            else if ((paramFloat >= 21.3F) && (paramFloat < 21.7F))
            {
              paramInt = Math.round((paramFloat - 21.3F) * 5.0F / (21.7F - 21.3F) + 5.0F);
            }
            else if ((paramFloat >= 21.1F) && (paramFloat < 21.3F))
            {
              paramInt = Math.round((paramFloat - 21.1F) * 5.0F / (21.3F - 21.1F));
            }
            else
            {
              paramInt = i;
              if (paramFloat < 21.1F) {
                paramInt = 0;
              }
            }
          }
          else if (paramFloat >= 15.4F)
          {
            paramInt = 100;
          }
          else if ((paramFloat >= 14.9F) && (paramFloat < 15.4F))
          {
            paramInt = Math.round((paramFloat - 14.9F) * 50.0F / (15.4F - 14.9F) + 50.0F);
          }
          else if ((paramFloat > 14.2F) && (paramFloat < 14.9F))
          {
            paramInt = Math.round((paramFloat - 14.2F) * 25.0F / (14.9F - 14.2F) + 25.0F);
          }
          else if ((paramFloat > 14.0F) && (paramFloat <= 14.2F))
          {
            paramInt = Math.round((paramFloat - 14.0F) * 20.0F / (14.2F - 14.0F) + 5.0F);
          }
          else if ((paramFloat > 13.8F) && (paramFloat <= 14.0F))
          {
            paramInt = Math.round((paramFloat - 13.8F) * 5.0F / (14.2F - 14.0F));
          }
          else
          {
            paramInt = i;
            if (paramFloat <= 13.8F) {
              paramInt = 0;
            }
          }
        }
      }
    }
  }
  
  private void handleSwitchChanged(UARTInfoMessage.SwitchChanged paramSwitchChanged)
  {
    if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing())) {}
    label349:
    label437:
    label475:
    label652:
    for (;;)
    {
      return;
      if (this.mIsPlayFPV) {
        if (paramSwitchChanged.hw_id == Utilities.CAMERA_KEY_INDEX) {
          if ((paramSwitchChanged.old_state == 0) && (paramSwitchChanged.new_state == 1)) {
            Log.d("FlightModeMainActivity", "Camera Button pressed");
          }
        }
      }
      for (;;)
      {
        if (paramSwitchChanged.hw_id != Utilities.FMODE_KEY_INDEX) {
          break label652;
        }
        this.mStatusBarView.setLeftText(1, getString(2131296533, new Object[] { Integer.valueOf(paramSwitchChanged.new_state) }));
        break;
        if ((paramSwitchChanged.old_state == 1) && (paramSwitchChanged.new_state == 0))
        {
          Log.d("FlightModeMainActivity", "Camera Button released");
          if (!this.mSnapShotProcessing) {
            if (!this.mIsRecording)
            {
              this.mSnapShotProcessing = true;
              this.mBtnSnapshot.performClick();
              this.mSoundPool.play(this.mCameraShutterSoundId, 1.0F, 1.0F, 0, 0, 1.0F);
            }
          }
          for (;;)
          {
            this.mRTVPlayer.snapShot(0, "/sdcard/FPV-Video/Local/" + prepareFilename(), 0, 0);
            break;
            this.mStatusBarView.setInfoText(getResources().getString(2131296345), -65536);
            continue;
            Log.d("FlightModeMainActivity", "SnapShot request processing...");
          }
          if (paramSwitchChanged.hw_id == Utilities.VIDEO_KEY_INDEX) {
            if ((paramSwitchChanged.old_state == 0) && (paramSwitchChanged.new_state == 1))
            {
              Log.d("FlightModeMainActivity", "Video Button pressed");
            }
            else if ((paramSwitchChanged.old_state == 1) && (paramSwitchChanged.new_state == 0))
            {
              Log.d("FlightModeMainActivity", "Video Button released");
              if ((this.cameraModeCache == 2) || (this.mCameraParams.cam_mode == 2))
              {
                if (!this.isChangeAndStartRecord)
                {
                  this.mSRswitch.setChecked(true);
                  this.isChangeAndStartRecord = true;
                  showProgressDialog(this.mChangeCameraModeRunnable, 5000L);
                }
                if (!this.mRTVPlayer.canRecord()) {
                  break label475;
                }
                if (!this.mRTVPlayer.isRecording()) {
                  break label437;
                }
                this.mRTVPlayer.stopRecord();
                this.mIsLocalRecording = false;
              }
              for (;;)
              {
                this.mSoundPool.play(this.mRecorderShutterSoundId, 1.0F, 1.0F, 0, 0, 1.0F);
                break;
                if (!this.mRecordProcessing)
                {
                  this.mRecordProcessing = true;
                  this.mBtnRecord.performClick();
                  break label349;
                }
                Log.d("FlightModeMainActivity", "Record request processing...");
                break label349;
                if (this.mRTVPlayer.startRecord("/sdcard/FPV-Video/Local/" + prepareFilename()) >= 0) {
                  this.mIsLocalRecording = true;
                }
              }
              Log.w("FlightModeMainActivity", "RTVPlayer can't record for now, maybe the video is not playing");
              continue;
              if (this.mFlightToggle.isChecked())
              {
                if (paramSwitchChanged.hw_id == Utilities.FMODE_KEY_INDEX)
                {
                  Log.d("FlightModeMainActivity", "F mode Key changed old:" + paramSwitchChanged.old_state + " new:" + paramSwitchChanged.new_state);
                  this.mTimerHelper.handleFmodeTrigger();
                }
              }
              else if (paramSwitchChanged.hw_id == Utilities.BIND_KEY_INDEX)
              {
                if (!this.mInitiailDataTranfered)
                {
                  Log.d("FlightModeMainActivity", "data still initializing...");
                  break;
                }
                if ((paramSwitchChanged.old_state == 0) && (paramSwitchChanged.new_state == 1))
                {
                  Log.d("FlightModeMainActivity", "Bind Button pressed");
                }
                else if ((paramSwitchChanged.old_state == 1) && (paramSwitchChanged.new_state == 0))
                {
                  Log.d("FlightModeMainActivity", "Bind Button released");
                  startActivity(new Intent(this, FlightSettings.class));
                }
              }
            }
          }
        }
      }
    }
  }
  
  private void handleTelemetryInfoChanged(UARTInfoMessage.Telemetry paramTelemetry)
  {
    Log.e("lifei", "--->" + paramTelemetry.toString());
    Log.e("lifei", "--->" + paramTelemetry.gps_status + "-----" + paramTelemetry.gps_used);
    updateFmodeButton(paramTelemetry);
    this.mCurrentFmode = paramTelemetry.f_mode;
    if ("ST10".equals("ST12")) {
      this.mMissionView.updateDroneGps(paramTelemetry.f_mode, paramTelemetry.latitude, paramTelemetry.longitude, paramTelemetry.altitude);
    }
    if (paramTelemetry.gps_status == 1) {
      this.mGPSswtich = true;
    }
    while (!isValidTelemetryData(paramTelemetry))
    {
      return;
      if (paramTelemetry.gps_used) {
        this.mGPSswtich = true;
      } else {
        this.mGPSswtich = false;
      }
    }
    int j;
    int i;
    Object localObject;
    if (this.mIndicatorMODE != null)
    {
      j = 1;
      i = 1;
      if (paramTelemetry.vehicle_type == 1) {
        break label1103;
      }
      localObject = new FCSensorData();
      ((FCSensorData)localObject).setData(paramTelemetry.imu_status, paramTelemetry.press_compass_status);
      if (((FCSensorData)localObject).mError != null)
      {
        this.mIndicatorMODE.setValueText(((FCSensorData)localObject).mError);
        this.mIndicatorMODE.setValueColor(-65536);
        i = 0;
      }
      this.mCameraSettingsBtn.setVisibility(0);
      label241:
      if (i != 0)
      {
        localObject = new FModeData();
        ((FModeData)localObject).setData(paramTelemetry.f_mode, paramTelemetry.vehicle_type);
        this.mIndicatorMODE.setValueText(((FModeData)localObject).fModeString);
        this.mIndicatorMODE.setValueColor(((FModeData)localObject).fModeColor);
      }
    }
    if (this.mIndicatorGPSStatus != null)
    {
      if (paramTelemetry.gps_status != 1) {
        break label1135;
      }
      if (paramTelemetry.gps_used)
      {
        localObject = getResources().getString(2131296381);
        i = -16711936;
        label329:
        this.mIndicatorGPSStatus.setValueText((CharSequence)localObject);
        this.mIndicatorGPSStatus.setValueColor(i);
      }
    }
    else
    {
      if ((paramTelemetry.imu_status & 0x10) == 0) {
        break label1154;
      }
      bool = true;
      label359:
      if ("ST10".equals("ST12")) {
        this.sonarButton.setChecked(bool);
      }
      if (this.mIndicatorGPS != null)
      {
        i = paramTelemetry.satellites_num;
        this.mIndicatorGPS.setValueText(String.valueOf(i));
      }
      if (this.mIndicatorVOL != null)
      {
        if (mIndex >= VOL_INDEX_MAX) {
          mIndex = 0;
        }
        localObject = mVOLValue;
        i = mIndex;
        mIndex = i + 1;
        localObject[i] = paramTelemetry.voltage;
        mTmpVOLFloatValue = 0.0F;
        i = 0;
        label452:
        if (i < VOL_INDEX_MAX) {
          break label1160;
        }
        label459:
        mTmpVOLFloatValue /= i;
        mTmpVOLFloatValue = Math.round(10.0F * mTmpVOLFloatValue) / 10.0F;
        i = getVoltageLevel(mTmpVOLFloatValue, paramTelemetry.vehicle_type);
        this.mIndicatorVOL.setValueText(mTmpVOLFloatValue + " V");
        updateVolDrawable(i);
        if ((paramTelemetry.error_flags1 & 0x2) == 0) {
          break label1188;
        }
        this.mIndicatorVOL.setValueColor(-65536);
      }
      label551:
      if (this.mIndicatorALT != null)
      {
        localObject = Utilities.FormatLengthDisplayString(this, paramTelemetry.altitude);
        this.mIndicatorALT.setValueText((CharSequence)localObject);
      }
      if (this.mIndicatorTAS != null)
      {
        localObject = Utilities.FormatVelocityDisplayString(this, paramTelemetry.tas);
        this.mIndicatorTAS.setValueText((CharSequence)localObject);
      }
      if (this.mIndicatorPOS != null)
      {
        localObject = Utilities.FormatPositionDisplayString(this, paramTelemetry.longitude, paramTelemetry.latitude);
        this.mIndicatorPOS.setValueText((CharSequence)localObject);
      }
      localObject = this.mGPSUpdater.getCurrentLocation();
      if (((paramTelemetry.latitude == 0.0F) && (paramTelemetry.longitude == 0.0F)) || ((((GPSUpLinkData)localObject).lat == 0.0F) && (((GPSUpLinkData)localObject).lon == 0.0F))) {
        break label1304;
      }
      localObject = Utilities.calculateDistanceAndBearing(paramTelemetry.latitude, paramTelemetry.longitude, paramTelemetry.altitude, ((GPSUpLinkData)localObject).lat, ((GPSUpLinkData)localObject).lon, 0.0F);
      if ((this.mIndicatorDIS != null) && (localObject[0] != -1.0F))
      {
        String str = Utilities.FormatHomeDistanceDisplayString(this, localObject[0]);
        this.mIndicatorDIS.setValueText(str);
      }
      if ((this.mHomeCompassView != null) && (localObject[1] != -1.0F))
      {
        if ((paramTelemetry.f_mode != 6) && (paramTelemetry.f_mode != 23) && (paramTelemetry.f_mode != 24) && (paramTelemetry.f_mode != 21) && (paramTelemetry.f_mode != 22) && (paramTelemetry.f_mode != 16)) {
          break label1270;
        }
        this.mHomeCompassView.setHomeEnabled(false);
      }
      label825:
      int k = getMotorCount(paramTelemetry.vehicle_type);
      i = 0;
      label837:
      if (i < k) {
        break label1352;
      }
      if (((paramTelemetry.error_flags1 & 0x1) == 0) || ((paramTelemetry.error_flags1 & 0x2) != 0) || (!FModeData.isMotorWorking(paramTelemetry.f_mode))) {
        break label1413;
      }
      bool = true;
      label874:
      if ((bool ^ this.mLastVoltageLowWarning1))
      {
        if (!bool) {
          break label1419;
        }
        showVoltageLowWarning1();
        startVibrator(1);
      }
      label898:
      this.mLastVoltageLowWarning1 = bool;
      if (((paramTelemetry.error_flags1 & 0x2) == 0) || (!FModeData.isMotorWorking(paramTelemetry.f_mode))) {
        break label1431;
      }
      bool = true;
      label926:
      if ((bool ^ this.mLastVoltageLowWarning2))
      {
        if (!bool) {
          break label1437;
        }
        showVoltageLowWarning2();
        startVibrator(4);
      }
      label950:
      this.mLastVoltageLowWarning2 = bool;
      i = 0;
      if (paramTelemetry.gps_status != 1) {
        break label1449;
      }
      if (!paramTelemetry.gps_used) {
        i = 1;
      }
      label975:
      if (!paramTelemetry.gps_used) {
        break label1461;
      }
      bool = false;
      label985:
      if ((bool ^ this.mLastGPSDisabledWarning))
      {
        if (!bool) {
          break label1467;
        }
        showGPSDisabledWarning(i);
        startVibrator(2);
      }
      label1010:
      this.mLastGPSDisabledWarning = bool;
      if ((paramTelemetry.error_flags1 & 0x80) == 0) {
        break label1479;
      }
      bool = true;
      label1030:
      if (((bool ^ this.mLastAirportWarning)) && (bool)) {
        showAirportWarning();
      }
      this.mLastAirportWarning = bool;
      if (((paramTelemetry.error_flags1 & 0x4) == 0) || (paramTelemetry.vehicle_type != 2)) {
        break label1485;
      }
    }
    label1103:
    label1135:
    label1154:
    label1160:
    label1188:
    label1270:
    label1304:
    label1352:
    label1407:
    label1413:
    label1419:
    label1431:
    label1437:
    label1449:
    label1461:
    label1467:
    label1479:
    label1485:
    for (boolean bool = true;; bool = false)
    {
      if (((bool ^ this.mLastAltitudeWarning)) && (bool)) {
        showAltitudeWarning();
      }
      this.mLastAltitudeWarning = bool;
      break;
      this.mCameraSettingsBtn.setVisibility(4);
      i = j;
      break label241;
      localObject = getResources().getString(2131296382);
      i = 65280;
      break label329;
      localObject = getResources().getString(2131296383);
      i = -65536;
      break label329;
      bool = false;
      break label359;
      if (mVOLValue[i] <= 0.0F) {
        break label459;
      }
      mTmpVOLFloatValue += mVOLValue[i];
      i++;
      break label452;
      if ((paramTelemetry.error_flags1 & 0x1) != 0)
      {
        this.mIndicatorVOL.setValueColor(65280);
        break label551;
      }
      if (i > 25)
      {
        this.mIndicatorVOL.setValueColor(-16724737);
        break label551;
      }
      if ((i > 5) && (i <= 25))
      {
        this.mIndicatorVOL.setValueColor(65280);
        break label551;
      }
      if (i > 5) {
        break label551;
      }
      this.mIndicatorVOL.setValueColor(-65536);
      break label551;
      this.mHomeCompassView.setHomeEnabled(true);
      i = Math.round(Utilities.getRelativeDegree(localObject[1], paramTelemetry.yaw));
      this.mHomeCompassView.setDirection(i);
      break label825;
      localObject = getResources().getString(2131296311, new Object[] { "N/A", Utilities.getDisplayLengthUnit(this) });
      this.mIndicatorDIS.setValueText((CharSequence)localObject);
      this.mHomeCompassView.setHomeEnabled(false);
      break label825;
      if ((paramTelemetry.motor_status >> i & 0x1) == 0) {}
      for (j = 1;; j = 0)
      {
        if ((paramTelemetry.motor_status == 12) || (j == 0) || (this.motorStatusErr)) {
          break label1407;
        }
        this.motorStatusErr = true;
        showFModeFailDialog();
        startVibrator(1);
        break;
      }
      i++;
      break label837;
      bool = false;
      break label874;
      dismissVoltageLowWarning1();
      stopVibrator(1);
      break label898;
      bool = false;
      break label926;
      dismissVoltageLowWarning2();
      stopVibrator(4);
      break label950;
      if (paramTelemetry.gps_used) {
        break label975;
      }
      i = 2;
      break label975;
      bool = true;
      break label985;
      dismissGPSDisabledWarning();
      stopVibrator(2);
      break label1010;
      bool = false;
      break label1030;
    }
  }
  
  private void handleTimerTirgger(UARTInfoMessage.Channel paramChannel)
  {
    if (!this.mFlightToggle.isChecked()) {}
    for (;;)
    {
      return;
      this.mTimerHelper.handleThrottleTrigger(paramChannel);
    }
  }
  
  private void handleZoomAction(UARTInfoMessage.Channel paramChannel)
  {
    if (paramChannel.channels.size() >= Utilities.ZOOM_KEY_INDEX) {
      zoom((int)Utilities.getChannelValue(paramChannel, Utilities.ZOOM_KEY_INDEX));
    }
  }
  
  private void initIndicatorValue()
  {
    if (this.mIndicatorMODE != null)
    {
      this.mIndicatorMODE.setValueText("N/A");
      this.mIndicatorMODE.setValueColor(-16724737);
    }
    if (this.mIndicatorGPSStatus != null)
    {
      this.mIndicatorGPSStatus.setValueText("N/A");
      this.mIndicatorGPSStatus.setValueColor(-16724737);
      setFmodeButtonEnable(false);
    }
    if (this.mIndicatorGPS != null) {
      this.mIndicatorGPS.setValueText("N/A");
    }
    String str;
    if (this.mIndicatorVOL != null)
    {
      str = getResources().getString(2131296311, new Object[] { "N/A", " V" });
      this.mIndicatorVOL.setValueText(str);
      this.mIndicatorVOL.setValueTextSize(10.0F);
      this.mIndicatorVOL.setValueTextDrawable(getResources().getDrawable(2130837745));
      this.mIndicatorVOL.setValueColor(-16724737);
      updateVolDrawable(0);
    }
    if (this.mIndicatorALT != null)
    {
      str = getResources().getString(2131296311, new Object[] { "N/A", Utilities.getDisplayLengthUnit(this) });
      this.mIndicatorALT.setValueText(str);
    }
    if (this.mIndicatorTAS != null)
    {
      str = getResources().getString(2131296311, new Object[] { "N/A", Utilities.getDisplayVelocityUnit(this) });
      this.mIndicatorTAS.setValueText(str);
    }
    if (this.mIndicatorPOS != null)
    {
      str = getResources().getString(2131296311, new Object[] { "N/A", "E" }) + "\n" + getResources().getString(2131296311, new Object[] { "N/A", "N" });
      this.mIndicatorPOS.setValueText(str);
      this.mIndicatorPOS.setValueTextSize(10.0F);
    }
    if (this.mIndicatorDIS != null)
    {
      str = getResources().getString(2131296311, new Object[] { "N/A", Utilities.getDisplayLengthUnit(this) });
      this.mIndicatorDIS.setValueText(str);
    }
  }
  
  private boolean isBindRX(Context paramContext, long paramLong)
  {
    boolean bool = false;
    if (paramLong != -2L)
    {
      Object localObject = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
      paramContext = paramContext.getContentResolver().query((Uri)localObject, new String[] { "_id", "rx" }, null, null, null);
      if (!DataProviderHelper.isCursorValid(paramContext)) {
        Log.e("FlightModeMainActivity", "isBindRX, Cursor is invalid");
      }
      bool = false;
      localObject = paramContext.getString(paramContext.getColumnIndex("rx"));
      paramContext.close();
      if (localObject != null) {
        bool = true;
      }
    }
    return bool;
  }
  
  private boolean isBindWifi(Context paramContext, long paramLong)
  {
    boolean bool = false;
    if (Utilities.getModelWifiInfoToDatabase(paramContext, paramLong) != null) {
      bool = true;
    }
    return bool;
  }
  
  private boolean isValidTelemetryData(UARTInfoMessage.Telemetry paramTelemetry)
  {
    boolean bool = false;
    if ((paramTelemetry.vehicle_type < 0) || (paramTelemetry.vehicle_type > 10)) {
      Log.e("FlightModeMainActivity", "info.vehicle_type:" + paramTelemetry.vehicle_type);
    }
    for (;;)
    {
      return bool;
      if ((paramTelemetry.f_mode < 0) || (paramTelemetry.f_mode > 25)) {
        Log.e("FlightModeMainActivity", "info.f_mode:" + paramTelemetry.f_mode);
      } else if ((paramTelemetry.altitude < -100.0F) && (paramTelemetry.altitude > 200.0F)) {
        Log.e("FlightModeMainActivity", "info.altitude:" + paramTelemetry.altitude);
      } else {
        bool = true;
      }
    }
  }
  
  private void loadAnimation()
  {
    this.mLeftFadeIn = AnimationUtils.loadAnimation(this, 2130968581);
    this.mLeftFadeIn.setAnimationListener(this);
    this.mLeftFadeOut = AnimationUtils.loadAnimation(this, 2130968582);
    this.mLeftFadeOut.setAnimationListener(this);
    this.mRightFadeIn = AnimationUtils.loadAnimation(this, 2130968585);
    this.mRightFadeIn.setAnimationListener(this);
    this.mRightFadeOut = AnimationUtils.loadAnimation(this, 2130968586);
    this.mRightFadeOut.setAnimationListener(this);
    this.mTrimLeftFadeIn = AnimationUtils.loadAnimation(this, 2130968581);
    this.mTrimLeftFadeIn.setAnimationListener(this);
    this.mTrimLeftFadeOut = AnimationUtils.loadAnimation(this, 2130968582);
    this.mTrimLeftFadeOut.setAnimationListener(this);
    this.mTrimRightFadeIn = AnimationUtils.loadAnimation(this, 2130968585);
    this.mTrimRightFadeIn.setAnimationListener(this);
    this.mTrimRightFadeOut = AnimationUtils.loadAnimation(this, 2130968586);
    this.mTrimRightFadeOut.setAnimationListener(this);
    this.mTopFadeIn = AnimationUtils.loadAnimation(this, 2130968588);
    this.mTopFadeIn.setAnimationListener(this);
    this.mTopFadeOut = AnimationUtils.loadAnimation(this, 2130968589);
    this.mTopFadeOut.setAnimationListener(this);
    this.mBottomFadeIn = AnimationUtils.loadAnimation(this, 2130968576);
    this.mBottomFadeIn.setAnimationListener(this);
    this.mBottomFadeOut = AnimationUtils.loadAnimation(this, 2130968577);
    this.mBottomFadeOut.setAnimationListener(this);
  }
  
  private void onSDcardChanged(int paramInt)
  {
    int i = 0;
    String str2 = null;
    String str1 = null;
    if (this.mIsPaused) {}
    label163:
    for (;;)
    {
      return;
      if (paramInt >> 10 <= 30)
      {
        str2 = getResources().getString(2131296337);
        str1 = getResources().getString(2131296338);
        paramInt = 1;
      }
      for (;;)
      {
        if ((paramInt == 0) || (this.mSDErrDialogShown)) {
          break label163;
        }
        final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(this);
        localOneButtonPopDialog.setCancelable(false);
        localOneButtonPopDialog.setTitle(str2);
        localOneButtonPopDialog.setMessage(str1);
        localOneButtonPopDialog.setPositiveButton(17039370, new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            localOneButtonPopDialog.dismiss();
          }
        });
        localOneButtonPopDialog.show();
        this.mSDErrDialogShown = true;
        break;
        if (paramInt < 420)
        {
          str2 = getResources().getString(2131296340);
          str1 = getResources().getString(2131296339);
          paramInt = 1;
        }
        else
        {
          this.mSDErrDialogShown = false;
          paramInt = i;
        }
      }
    }
  }
  
  private void onSDcardChanged(IPCameraManager.SDCardStatus paramSDCardStatus)
  {
    int i = 0;
    String str2 = null;
    String str1 = null;
    if (this.mIsPaused) {}
    for (;;)
    {
      return;
      if (!paramSDCardStatus.equals(this.mLastSDcardStatus)) {
        break;
      }
      this.mLastSDcardStatus = paramSDCardStatus;
    }
    if (!paramSDCardStatus.isInsert)
    {
      str2 = getResources().getString(2131296337);
      str1 = getResources().getString(2131296338);
      i = 1;
    }
    for (;;)
    {
      if ((i != 0) && (!this.mSDErrDialogShown))
      {
        final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(this);
        localOneButtonPopDialog.setCancelable(false);
        localOneButtonPopDialog.setTitle(str2);
        localOneButtonPopDialog.setMessage(str1);
        localOneButtonPopDialog.setPositiveButton(17039370, new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            localOneButtonPopDialog.dismiss();
          }
        });
        localOneButtonPopDialog.show();
        this.mSDErrDialogShown = true;
      }
      this.mLastSDcardStatus = paramSDCardStatus;
      break;
      if (paramSDCardStatus.free_space < 420L)
      {
        str2 = getResources().getString(2131296340);
        str1 = getResources().getString(2131296339);
        i = 1;
      }
      else
      {
        this.mSDErrDialogShown = false;
      }
    }
  }
  
  private void onWiFiConntectionLost()
  {
    Log.i("FlightModeMainActivity", "#### Rec onWiFiConntectionLost mIsRecording=" + this.mIsRecording);
    if (this.mRTVPlayer.isRecording())
    {
      this.mRTVPlayer.stopRecord();
      this.mIsLocalRecording = false;
    }
    this.mRecTime.stop();
    this.mRecTime.setVisibility(4);
    if ((this.mCurrentCameraName.equals("C-GO3-Pro")) || (this.mCurrentCameraName.equals("C-GO3"))) {
      this.mSRswitch.setVisibility(0);
    }
    this.mResolution = null;
    setRlnBtnText(null);
    if (this.mIsRecording)
    {
      this.mIsRecording = false;
      this.mBtnRecord.setChecked(false);
    }
    this.mRecordProcessing = false;
    this.mHasStartedRecord = false;
    this.mHasStoppedRecord = false;
    this.mSDErrDialogShown = false;
    this.isChangeAndStartRecord = false;
    setRlnText(null);
  }
  
  private boolean playFPV()
  {
    boolean bool = false;
    if (this.mRTVPlayer.isPlaying()) {
      Log.i("FlightModeMainActivity", "Player has already been played");
    }
    for (;;)
    {
      return bool;
      if (!((WifiManager)getSystemService("wifi")).isWifiEnabled())
      {
        Log.i("FlightModeMainActivity", "Wifi is not opened");
      }
      else
      {
        this.mLCDDislpayView.setVisibility(0);
        bool = this.mRTVPlayer.play(this.mCurrentVideoLocation);
      }
    }
  }
  
  private String prepareFilename()
  {
    Time localTime = new Time();
    localTime.setToNow();
    return localTime.format("%Y%m%d_%H-%M-%S");
  }
  
  private boolean preparePlayer()
  {
    this.mRTVPlayer = RTVPlayer.getPlayer(2);
    this.mRTVPlayer.init(this, 1, false);
    this.mRTVPlayer.setSurfaceView(this.mLCDDislpayView);
    this.mRTVPlayer.setVideoEventCallback(this.mVideoEventCallback);
    return true;
  }
  
  private void prepareSoundPool()
  {
    this.mSoundPool = new SoundPool(2, 3, 0);
    this.mCameraShutterSoundId = this.mSoundPool.load(this, 2131165185, 1);
    this.mRecorderShutterSoundId = this.mSoundPool.load(this, 2131165188, 1);
  }
  
  private void prepareVideoFolder()
  {
    File localFile = new File("/sdcard/FPV-Video/Local");
    if (!localFile.exists()) {
      localFile.mkdirs();
    }
  }
  
  private void reconnectCamera()
  {
    if (this.mIsPaused) {
      Log.d("FlightModeMainActivity", "the activity is paused, don't retry connect to camera");
    }
    for (;;)
    {
      return;
      if (!this.mFlightToggle.isChecked()) {
        Log.d("FlightModeMainActivity", "the plane has been taken off, don't retry connect to camera");
      }
      onWiFiConntectionLost();
      this.mStatusBarView.setInfoText(getResources().getString(2131296346), -65536);
      this.mHandler.removeCallbacks(this.mReconnectCameraRunnable);
      this.mHandler.postDelayed(this.mReconnectCameraRunnable, 3000L);
    }
  }
  
  private void refreshScreen()
  {
    if (this.mCurrentCameraName.equals("C-GO2"))
    {
      this.mIPCameraManager.getSDCardStatus(this.mHttpResponseMessenger);
      this.mIPCameraManager.restartVF(this.mHttpResponseMessenger);
    }
    boolean bool;
    if ((this.mCurrentCameraName.equals("C-GO3")) || (this.mCurrentCameraName.equals("C-GO3-Pro")))
    {
      updateDrawerLayout();
      if (this.mCurrentCameraName.equals("C-GO3"))
      {
        SyncToggleButton localSyncToggleButton = this.mSRswitch;
        if (this.mCameraParams.cam_mode != 2) {
          break label120;
        }
        bool = false;
        localSyncToggleButton.syncState(bool);
      }
      if (this.mCameraParams.cam_mode == 2) {
        break label125;
      }
      setRlnText(this.mCameraParams.video_mode);
    }
    for (;;)
    {
      return;
      label120:
      bool = true;
      break;
      label125:
      setRlnText(null);
    }
  }
  
  private void resetWarnings()
  {
    this.mLastVoltageLowWarning1 = false;
    this.mLastVoltageLowWarning2 = false;
    this.mLastGPSDisabledWarning = true;
    this.mLastAirportWarning = false;
    this.mLastAltitudeWarning = false;
    dismissVoltageLowWarning1();
    dismissVoltageLowWarning2();
    dismissGPSDisabledWarning();
    dismissAirportWarning();
    dismissAltitudeWarning();
    stopVibrator(7);
  }
  
  private void sendFmodeData(int paramInt)
  {
    Utilities.getFmodeChannelValues(this);
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 5;
    localMixedData.mhardware = 21;
    localMixedData.mHardwareType = 2;
    localMixedData.mPriority = 1;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(Utilities.value_ch5[0]));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(Utilities.value_ch5[1]));
    localMixedData.mSwitchValue.add(2, Integer.valueOf(Utilities.value_ch5[2]));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    this.mController.syncMixingData(true, localMixedData, 1);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 6;
    localMixedData.mhardware = 21;
    localMixedData.mHardwareType = 2;
    localMixedData.mPriority = 1;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(Utilities.value_ch6[0]));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(Utilities.value_ch6[1]));
    localMixedData.mSwitchValue.add(2, Integer.valueOf(Utilities.value_ch6[2]));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    this.mController.syncMixingData(true, localMixedData, 1);
  }
  
  private void setExposure(int paramInt)
  {
    switch (paramInt)
    {
    }
    for (;;)
    {
      return;
      this.mAutoModeFrame.setVisibility(8);
      this.mManualModeFrame.setVisibility(8);
      this.mWhitBalanceList.setVisibility(0);
      continue;
      this.mWhitBalanceList.setVisibility(8);
      if (this.mCameraParams.ae_enable == 0)
      {
        this.mAutoModeFrame.setVisibility(8);
        this.mManualModeFrame.setVisibility(0);
      }
      else
      {
        this.mAutoModeFrame.setVisibility(0);
        this.mManualModeFrame.setVisibility(8);
      }
    }
  }
  
  private void setFmodeButtonChecked(boolean paramBoolean)
  {
    this.mFmodeButton.setEnabled(true);
    if (paramBoolean) {
      this.mFmodeButton.getBackground().setLevel(2);
    }
    for (this.mFmodeBtnChecked = true;; this.mFmodeBtnChecked = false)
    {
      return;
      this.mFmodeButton.getBackground().setLevel(1);
    }
  }
  
  private void setFmodeButtonEnable(boolean paramBoolean)
  {
    this.mFmodeButton.setEnabled(paramBoolean);
    if (!paramBoolean)
    {
      this.mFmodeBtnChecked = false;
      this.mFmodeButton.getBackground().setLevel(0);
    }
    for (;;)
    {
      return;
      if (this.mFmodeBtnChecked) {
        this.mFmodeButton.getBackground().setLevel(2);
      } else {
        this.mFmodeButton.getBackground().setLevel(1);
      }
    }
  }
  
  private void setLeftDrawerDefault()
  {
    this.mCurrentExposureIndex = 0;
    this.mCameraParams.ae_enable = 1;
    this.mCameraParams.white_balance = 0;
    this.mCameraParams.exposure_value = "0.0";
    this.mCameraParams.iso = "ISO_600";
    this.mCameraParams.shutter_time = 800;
    updateDrawerLayout();
  }
  
  private void setRecordBtnState(boolean paramBoolean)
  {
    Log.i("FlightModeMainActivity", "setRecordBtnState:" + paramBoolean);
    if (paramBoolean)
    {
      this.mIsRecording = true;
      this.mBtnRecord.setChecked(true);
      this.mRecTime.setVisibility(0);
      this.mRlnButton.setEnabled(false);
      if ((this.mCurrentCameraName.equals("C-GO3-Pro")) || (this.mCurrentCameraName.equals("C-GO3"))) {
        this.mSRswitch.setVisibility(4);
      }
    }
    for (;;)
    {
      return;
      this.mIsRecording = false;
      this.mBtnRecord.setChecked(false);
      this.mRecTime.setVisibility(4);
      this.mRlnButton.setEnabled(true);
      if ((this.mCurrentCameraName.equals("C-GO3-Pro")) || (this.mCurrentCameraName.equals("C-GO3"))) {
        this.mSRswitch.setVisibility(0);
      }
    }
  }
  
  private void setRlnBtnText(String paramString)
  {
    if (paramString != null)
    {
      if (!this.mRlnButton.isEnabled()) {
        this.mRlnButton.setEnabled(true);
      }
      if (paramString.contains("60P"))
      {
        this.mResolution = "60P";
        this.mRlnButton.setText(2131296387);
        this.mRlnButton.setVisibility(0);
      }
    }
    for (;;)
    {
      return;
      if (paramString.contains("50P"))
      {
        this.mResolution = "50P";
        this.mRlnButton.setText(2131296386);
        this.mRlnButton.setVisibility(0);
      }
      else if (paramString.contains("48P"))
      {
        this.mResolution = "48P";
        this.mRlnButton.setText(2131296385);
        this.mRlnButton.setVisibility(0);
      }
      else
      {
        this.mResolution = null;
        this.mRlnButton.setVisibility(4);
        this.mRlnButton.setEnabled(false);
        continue;
        this.mRlnButton.setVisibility(4);
        this.mRlnButton.setEnabled(false);
      }
    }
  }
  
  private void setRlnText(String paramString)
  {
    if (paramString != null)
    {
      this.mRlnText.setText(paramString);
      this.mRlnText.setVisibility(0);
    }
    for (;;)
    {
      return;
      this.mRlnText.setVisibility(4);
    }
  }
  
  private void setVideoResolution(String paramString)
  {
    if (paramString != null)
    {
      if (!"48P".equals(paramString)) {
        break label27;
      }
      this.mIPCameraManager.setVideoResolution(this.mHttpResponseMessenger, 5);
    }
    for (;;)
    {
      return;
      label27:
      if ("50P".equals(paramString)) {
        this.mIPCameraManager.setVideoResolution(this.mHttpResponseMessenger, 3);
      } else if ("60P".equals(paramString)) {
        this.mIPCameraManager.setVideoResolution(this.mHttpResponseMessenger, 1);
      }
    }
  }
  
  private void setVideoStandard(String paramString)
  {
    if (paramString != null)
    {
      if ((!"48P".equals(paramString)) && (!"60P".equals(paramString))) {
        break label37;
      }
      this.mIPCameraManager.setVideoStandard(this.mHttpResponseMessenger, 1);
    }
    for (;;)
    {
      return;
      label37:
      if ("50P".equals(paramString)) {
        this.mIPCameraManager.setVideoStandard(this.mHttpResponseMessenger, 2);
      }
    }
  }
  
  private void setupDrawerLayout()
  {
    this.mCameraSettingsBtn = ((Button)findViewById(2131689735));
    this.mCameraSettingsBtn.setEnabled(false);
    this.mCameraSettingsBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(final View paramAnonymousView)
      {
        paramAnonymousView = new TwoButtonPopDialog(MainActivity.this);
        paramAnonymousView.adjustHeight(380);
        paramAnonymousView.setTitle(2131296368);
        paramAnonymousView.setMessage(2131296370);
        paramAnonymousView.setPositiveButton(17039379, new View.OnClickListener()
        {
          public void onClick(View paramAnonymous2View)
          {
            paramAnonymousView.dismiss();
            MainActivity.this.mRlnBtnPressed = true;
            MainActivity.this.stopFPV();
            paramAnonymous2View = new Intent(MainActivity.this, CameraSettingsActivity.class);
            paramAnonymous2View.putExtra("currentCamera", MainActivity.this.mCurrentCameraName);
            paramAnonymous2View.putExtra("wifi_connected", MainActivity.this.isWIFIConneted);
            paramAnonymous2View.putExtra("fmodeStatus", MainActivity.this.mCurrentFmode);
            paramAnonymous2View.putExtra("gps_switch", MainActivity.this.mGPSswtich);
            if (MainActivity.this.mCameraParams != null)
            {
              Log.i("FlightModeMainActivity", "mCameraParams != null" + MainActivity.this.mCameraParams);
              paramAnonymous2View.putExtra("camera_params", MainActivity.this.mCameraParams);
            }
            MainActivity.this.startActivity(paramAnonymous2View);
          }
        });
        paramAnonymousView.setNegativeButton(17039369, new View.OnClickListener()
        {
          public void onClick(View paramAnonymous2View)
          {
            paramAnonymousView.cancel();
          }
        });
        paramAnonymousView.show();
      }
    });
    this.wbIsoFrame = ((FrameLayout)findViewById(2131689708));
    this.cameraControlCombine = ((RelativeLayout)findViewById(2131689729));
    Object localObject = (ToggleFrameView)findViewById(2131689727);
    ((ToggleFrameView)localObject).setToggleOnClickListener(new ToggleFrameView.ToggleOnClickListener()
    {
      public void isoItemOnClick(View paramAnonymousView)
      {
        MainActivity.this.mCurrentExposureIndex = 1;
        MainActivity.this.setExposure(MainActivity.this.mCurrentExposureIndex);
      }
      
      public void wbItemOnClick(View paramAnonymousView)
      {
        MainActivity.this.mCurrentExposureIndex = 0;
        MainActivity.this.setExposure(MainActivity.this.mCurrentExposureIndex);
      }
    });
    this.wbIsoChangedListener = ((WbIsoChangedListener)localObject);
    this.mWhitBalanceList = ((WhiteBalanceScrollView)findViewById(2131689709));
    this.mWhitBalanceList.setVerticalScrollBarEnabled(false);
    this.mWhitBalanceList.setOnItemSelectedListener(this.mOnItemSelectedListener);
    this.mManualModeFrame = ((LinearLayout)findViewById(2131689710));
    this.mAutoModeFrame = ((LinearLayout)findViewById(2131689720));
    this.mEVListValue = Arrays.asList(getResources().getStringArray(2131361816));
    this.mISOListValue = Arrays.asList(getResources().getStringArray(2131361818));
    this.mSHTimeListValue = Arrays.asList(getResources().getStringArray(2131361820));
    localObject = getResources().getStringArray(2131361815);
    this.mEVpicker = ((StyledNumberPicker)findViewById(2131689725));
    this.mEVpicker.setOnValueChangedListener(this.mPickerChangedListener);
    this.mEVpicker.setOnScrollListener(this.mPickerScrollListener);
    this.mEVpicker.setOnButtonClickedListener(this.mPickerButtonListener);
    this.mEVpicker.setDisplayedValues((String[])localObject);
    this.mEVpicker.setMinValue(0);
    this.mEVpicker.setMaxValue(localObject.length - 1);
    this.mEVincrement = ((ImageView)findViewById(2131689724));
    this.mEVdecrement = ((ImageView)findViewById(2131689726));
    this.mAutoBtn = ((Button)findViewById(2131689721));
    this.mAutoBtn.setOnClickListener(this.mModeSwitchListener);
    this.mManualBtn = ((Button)findViewById(2131689711));
    this.mManualBtn.setOnClickListener(this.mModeSwitchListener);
    localObject = getResources().getStringArray(2131361817);
    this.mISOpicker = ((StyledNumberPicker)findViewById(2131689714));
    this.mISOpicker.setOnValueChangedListener(this.mPickerChangedListener);
    this.mISOpicker.setOnScrollListener(this.mPickerScrollListener);
    this.mISOpicker.setOnButtonClickedListener(this.mPickerButtonListener);
    this.mISOpicker.setDisplayedValues((String[])localObject);
    this.mISOpicker.setMinValue(0);
    this.mISOpicker.setMaxValue(localObject.length - 1);
    this.mISOincrement = ((ImageView)findViewById(2131689713));
    this.mISOdecrement = ((ImageView)findViewById(2131689715));
    localObject = getResources().getStringArray(2131361819);
    this.mSTpicker = ((StyledNumberPicker)findViewById(2131689718));
    this.mSTpicker.setOnValueChangedListener(this.mPickerChangedListener);
    this.mSTpicker.setOnScrollListener(this.mPickerScrollListener);
    this.mSTpicker.setOnButtonClickedListener(this.mPickerButtonListener);
    this.mSTpicker.setDisplayedValues((String[])localObject);
    this.mSTpicker.setMinValue(0);
    this.mSTpicker.setMaxValue(localObject.length - 1);
    this.mSTincrement = ((ImageView)findViewById(2131689717));
    this.mSTdecrement = ((ImageView)findViewById(2131689719));
    ((FrameLayout)findViewById(2131689706)).setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
      {
        return MainActivity.this.mGesture.onTouchEvent(paramAnonymousMotionEvent);
      }
    });
    this.mSRswitch = ((SyncToggleButton)findViewById(2131689733));
    this.mSRswitch.setOnUpdateChangeListener(new SyncToggleButton.OnUpdateChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean1, boolean paramAnonymousBoolean2)
      {
        if (paramAnonymousBoolean2)
        {
          if (!paramAnonymousBoolean1) {
            break label80;
          }
          paramAnonymousCompoundButton = "video";
          MainActivity.this.changeSRstateCache(1);
        }
        for (;;)
        {
          Log.d("FlightModeMainActivity", "CameraMode--set camera mode:" + paramAnonymousCompoundButton);
          ((Amba2)MainActivity.this.mIPCameraManager).setCameraMode(MainActivity.this.mHttpResponseMessenger, paramAnonymousCompoundButton);
          MainActivity.this.showProgressDialog(MainActivity.this.mChangeCameraModeRunnable, 5000L);
          return;
          label80:
          paramAnonymousCompoundButton = "photo";
          MainActivity.this.changeSRstateCache(2);
        }
      }
    });
    setLeftDrawerDefault();
  }
  
  private void setupViews()
  {
    this.mMainscreenLcdFrame = ((RelativeLayout)findViewById(2131689705));
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.mMainscreenLcdFrame.getLayoutParams();
    this.initLcdWidth = localMarginLayoutParams.width;
    this.initLcdHeight = localMarginLayoutParams.height;
    this.initLcdTopMargin = localMarginLayoutParams.topMargin;
    this.mBtnSystemSetting = ((Button)findViewById(2131689701));
    this.mBtnSystemSetting.setOnClickListener(this);
    this.mBtnFlightSetting = ((Button)findViewById(2131689702));
    this.mBtnFlightSetting.setOnClickListener(this);
    this.mBtnModelSelect = ((Button)findViewById(2131689703));
    this.mBtnModelSelect.setOnClickListener(this);
    this.mLCDDislpayView = ((SurfaceView)findViewById(2131689707));
    this.mZoombarContainer = findViewById(2131689743);
    this.mZoombar = ((VerticalSeekBar)findViewById(2131689745));
    this.mZoombar.setOnSeekBarChangeListener(this.seekBarListener);
    this.mZoombar.setEnabled(true);
    this.mZoombar.setMax(17);
    this.mBtnSnapshot = ((Button)findViewById(2131689740));
    this.mBtnSnapshot.setOnClickListener(this);
    this.mBtnRecord = ((ToggleButton)findViewById(2131689741));
    this.mBtnRecord.setOnCheckedChangeListener(this.mRecordStateListener);
    this.mRecTime = ((CounterView)findViewById(2131689742));
    this.mRecTime.setDuration(3600);
    this.mRecTime.setStyle(1);
    this.mRecTime.setTickListener(this.mRecTickListener);
    this.mBtnSnapshot.setEnabled(false);
    this.mBtnRecord.setEnabled(false);
    this.mFlightToggle = ((ToggleButtonWithColorText)findViewById(2131689704));
    this.mFlightToggle.setOnCheckedChangeListener(this.mFlightToggleListener);
    this.mFlightToggle.setOnTouchListener(this.mFlightToggleTouchListener);
    this.mOSDTime = ((CounterView)findViewById(2131689746));
    this.mTimerHelper = new TimerHelper(this.mOSDTime);
    this.mTimerHelper.setupDefault();
    this.mOSDTime.setVisibility(8);
    this.mHomeCompassView = ((HomeCompassView)findViewById(2131689737));
    this.mLeftTrimView = ((LeftTrimView)findViewById(2131689739));
    this.mRightTrimView = ((RightTrimView)findViewById(2131689738));
    this.mLeftTrimView.setVisibility(8);
    this.mRightTrimView.setVisibility(8);
    this.mIndicatorMODE = ((IndicatorView)findViewById(2131689693));
    this.mIndicatorGPSStatus = ((IndicatorView)findViewById(2131689694));
    this.mIndicatorGPS = ((IndicatorView)findViewById(2131689695));
    this.mIndicatorVOL = ((IndicatorView)findViewById(2131689697));
    this.mIndicatorALT = ((IndicatorView)findViewById(2131689698));
    this.mIndicatorTAS = ((IndicatorView)findViewById(2131689699));
    this.mIndicatorPOS = ((IndicatorView)findViewById(2131689696));
    this.mIndicatorDIS = ((IndicatorView)findViewById(2131689700));
    this.mStatusBarView = ((StatusbarView)findViewById(2131689540));
    this.mSSID = ((TextView)findViewById(2131689749));
    this.mRssi = ((TextView)findViewById(2131689750));
    this.mLinkSpeed = ((TextView)findViewById(2131689751));
    this.mWifiState = ((TextView)findViewById(2131689752));
    this.mFmodeCH5 = ((TextView)findViewById(2131689753));
    this.mFmodeCH6 = ((TextView)findViewById(2131689754));
    this.mRlnButton = ((Button)findViewById(2131689728));
    this.mRlnButton.setOnClickListener(this);
    this.mRlnButton.setEnabled(false);
    this.mRlnText = ((TextView)findViewById(2131689734));
    this.mFmodeButton = ((Button)findViewById(2131689730));
    this.mFmodeButton.setOnClickListener(this);
    this.mFmodeButton.setVisibility(0);
    setFmodeButtonEnable(false);
    if ("ST10".equals("ST12"))
    {
      this.sonarButton = ((ToggleButton)findViewById(2131689732));
      this.yawModeButton = ((ToggleButton)findViewById(2131689731));
      this.yawModeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
      {
        public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
        {
          int i;
          if (MainActivity.this.mController != null)
          {
            paramAnonymousCompoundButton = MainActivity.this.mController;
            i = Utilities.HW_VS_BASE;
            if (!paramAnonymousBoolean) {
              break label39;
            }
          }
          label39:
          for (paramAnonymousBoolean = false;; paramAnonymousBoolean = true)
          {
            paramAnonymousCompoundButton.setTTBState(false, i + 1, paramAnonymousBoolean);
            return;
          }
        }
      });
      this.sonarButton = ((ToggleButton)findViewById(2131689732));
      this.sonarButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          UARTController.getInstance().sonarSwitch(MainActivity.this.sonarButton.isChecked());
        }
      });
      this.mMissionView = ((MissionView)findViewById(2131689755));
      this.mMissionView.setMissionViewCallback(new MissionView.MissionViewCallback()
      {
        public GPSUpLinkData getControllerGps()
        {
          if (MainActivity.this.mGPSUpdater != null) {}
          for (GPSUpLinkData localGPSUpLinkData = MainActivity.this.mGPSUpdater.getCurrentLocation();; localGPSUpLinkData = null) {
            return localGPSUpLinkData;
          }
        }
        
        public void updateError(int paramAnonymousInt)
        {
          MainActivity.this.mStatusBarView.setInfoText(MainActivity.this.getResources().getString(paramAnonymousInt), -65536);
        }
        
        public void updateStep(MissionPresentation.MissionState paramAnonymousMissionState) {}
      });
    }
    for (;;)
    {
      return;
      this.sonarButton = ((ToggleButton)findViewById(2131689732));
      this.sonarButton.setVisibility(4);
      this.yawModeButton = ((ToggleButton)findViewById(2131689731));
      this.yawModeButton.setVisibility(4);
    }
  }
  
  private void showAirportWarning()
  {
    if (this.mAirportWarningDialog == null)
    {
      this.mAirportWarningDialog = new OneButtonPopDialog(this);
      this.mAirportWarningDialog.adjustHeight(300);
      this.mAirportWarningDialog.setCancelable(true);
      this.mAirportWarningDialog.setTitle(2131296368);
      this.mAirportWarningDialog.setTitleCompoundDrawable(getResources().getDrawable(17301642));
      this.mAirportWarningDialog.setMessage(2131296379);
      this.mAirportWarningDialog.setButtonVisble(false);
    }
    if (!this.mAirportWarningDialog.isShowing())
    {
      this.mAirportWarningDialog.show();
      this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          MainActivity.this.dismissAirportWarning();
        }
      }, 11000L);
    }
  }
  
  private void showAltitudeWarning()
  {
    if (this.mAltitudeWarningDialog == null)
    {
      this.mAltitudeWarningDialog = new OneButtonPopDialog(this);
      this.mAltitudeWarningDialog.adjustHeight(300);
      this.mAltitudeWarningDialog.setCancelable(true);
      this.mAltitudeWarningDialog.setTitle(2131296368);
      this.mAltitudeWarningDialog.setTitleCompoundDrawable(getResources().getDrawable(17301642));
      this.mAltitudeWarningDialog.setMessage(2131296380);
      this.mAltitudeWarningDialog.setButtonVisble(false);
    }
    if (!this.mAltitudeWarningDialog.isShowing())
    {
      this.mAltitudeWarningDialog.show();
      this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          MainActivity.this.dismissAltitudeWarning();
        }
      }, 11000L);
    }
  }
  
  private void showCommunicatingDialog()
  {
    if (this.mCommunicating == null)
    {
      this.mCommunicating = ProgressDialog.show(this, null, getResources().getString(2131296620), false, false);
      this.mCommunicating.setCanceledOnTouchOutside(true);
    }
    for (;;)
    {
      this.mHandler.postDelayed(this.mCommunicatingRunnable, 5000L);
      return;
      if (!this.mCommunicating.isShowing()) {
        this.mCommunicating.show();
      }
    }
  }
  
  private void showConnectingDialog()
  {
    if (this.mCurrentModelId != -2L)
    {
      if (this.mConnectingDialog != null) {
        break label86;
      }
      this.mConnectingDialog = MyProgressDialog.show(this, null, getResources().getText(2131296371), false, false);
      this.mConnectingDialog.setCanceledOnTouchOutside(true);
    }
    label86:
    label106:
    label196:
    for (;;)
    {
      if (this.mConnectingDialog != null)
      {
        if ((!this.isFSKConneted) || (this.isWIFIConneted)) {
          break label106;
        }
        this.mConnectingDialog.setMessage(getResources().getText(2131296372));
      }
      for (;;)
      {
        return;
        if (this.mConnectingDialog.isShowing()) {
          break label196;
        }
        this.mConnectingDialog.show();
        break;
        if ((!this.isFSKConneted) && (this.isWIFIConneted))
        {
          this.mConnectingDialog.setMessage(getResources().getText(2131296373));
        }
        else if ((this.isFSKConneted) && (this.isWIFIConneted))
        {
          this.mConnectingDialog.setMessage(getResources().getText(2131296374));
          dismissConnectingDialog();
        }
        else
        {
          this.mConnectingDialog.setMessage(getResources().getText(2131296371));
        }
      }
    }
  }
  
  private void showFModeFailDialog()
  {
    this.mFmodeFailDialog = new OneButtonPopDialog(this);
    this.mFmodeFailDialog.setTitle(2131296368);
    this.mFmodeFailDialog.adjustHeight(380);
    this.mFmodeFailDialog.setMessage(getResources().getString(2131296594));
    this.mFmodeFailDialog.setPositiveButton(2131296276, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MainActivity.this.mFmodeFailDialog.dismiss();
        MainActivity.this.stopVibrator(1);
      }
    });
    if (!this.mFmodeFailDialog.isShowing())
    {
      this.mFmodeFailDialog.show();
      this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          MainActivity.this.dismissFModeFailDialog();
          MainActivity.this.stopVibrator(1);
        }
      }, 10000L);
    }
  }
  
  private void showGPSDisabledWarning(int paramInt)
  {
    if (this.mGPSDisabledWarningDialog == null)
    {
      this.mGPSDisabledWarningDialog = new OneButtonPopDialog(this);
      this.mGPSDisabledWarningDialog.adjustHeight(300);
      this.mGPSDisabledWarningDialog.setCancelable(true);
      this.mGPSDisabledWarningDialog.setTitle(2131296368);
      this.mGPSDisabledWarningDialog.setTitleCompoundDrawable(getResources().getDrawable(17301642));
      if (paramInt == 1)
      {
        this.mGPSDisabledWarningDialog.setMessage(2131296378);
        this.mGPSDisabledWarningDialog.setButtonVisble(false);
      }
    }
    else if (!this.mGPSDisabledWarningDialog.isShowing())
    {
      if (paramInt != 1) {
        break label152;
      }
      this.mGPSDisabledWarningDialog.setMessage(2131296378);
    }
    for (;;)
    {
      this.mGPSDisabledWarningDialog.show();
      this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          MainActivity.this.dismissGPSDisabledWarning();
          MainActivity.this.stopVibrator(2);
        }
      }, 11000L);
      return;
      this.mGPSDisabledWarningDialog.setMessage(2131296377);
      break;
      label152:
      this.mGPSDisabledWarningDialog.setMessage(2131296377);
    }
  }
  
  private void showProgressDialog(Runnable paramRunnable, long paramLong)
  {
    if (this.mProgressDialog == null)
    {
      this.mProgressDialog = MyProgressDialog.show(this, null, getResources().getText(2131296273), false, false);
      this.mProgressDialog.setCanceledOnTouchOutside(true);
      this.mProgressDialog.setCancelable(false);
      this.mHandler.postDelayed(paramRunnable, paramLong);
    }
    for (;;)
    {
      return;
      if (!this.mProgressDialog.isShowing())
      {
        this.mProgressDialog.show();
        this.mHandler.postDelayed(paramRunnable, paramLong);
      }
    }
  }
  
  private void showSpeciallyChannelSender()
  {
    final SpeciallyChannelSender localSpeciallyChannelSender = new SpeciallyChannelSender(this);
    localSpeciallyChannelSender.setOnButtonClicked(new SpeciallyChannelSender.onButtonClickListener()
    {
      public void onButtonClick(int paramAnonymousInt)
      {
        if (MainActivity.this.mController != null) {
          MainActivity.this.mController.setTTBState(false, Utilities.HW_VB_BASE + paramAnonymousInt + 1, false);
        }
        localSpeciallyChannelSender.dismiss();
      }
    });
    localSpeciallyChannelSender.show();
  }
  
  private void showToastDontStack(int paramInt1, int paramInt2)
  {
    if (this.mToast != null) {
      this.mToast.cancel();
    }
    this.mToast = MyToast.makeText(this, paramInt1, 0, paramInt2);
    this.mToast.show();
  }
  
  private void showVoltageLowWarning1()
  {
    this.mVoltageLowWarning1Dialog = new Dialog(this, 2131230767);
    this.mVoltageLowWarning1Dialog.setContentView(2130903118);
    this.mVoltageLowWarning1Dialog.show();
    this.mHandler.postDelayed(new Runnable()
    {
      public void run()
      {
        MainActivity.this.dismissVoltageLowWarning1();
      }
    }, 11000L);
  }
  
  private void showVoltageLowWarning2()
  {
    this.mVoltageLowWarning2Dialog = new Dialog(this, 2131230767);
    this.mVoltageLowWarning2Dialog.setContentView(2130903119);
    this.mVoltageLowWarning2Dialog.show();
    this.mHandler.postDelayed(new Runnable()
    {
      public void run()
      {
        MainActivity.this.dismissVoltageLowWarning2();
      }
    }, 11000L);
  }
  
  private void startVibrator(int paramInt)
  {
    if (paramInt == 4)
    {
      if ((this.currentPriority & 0x4) != 4)
      {
        this.mVibrator.cancel();
        this.mVibrator.vibrate(new long[] { 1000L, 1000L, 1000L, 1000L }, 0);
      }
      this.currentPriority |= 0x4;
    }
    for (;;)
    {
      return;
      if (paramInt == 2)
      {
        if (((this.currentPriority & 0x4) != 4) && ((this.currentPriority & 0x2) != 2))
        {
          this.mVibrator.cancel();
          this.mVibrator.vibrate(new long[] { 1000L, 1000L, 1000L, 1000L }, 0);
        }
        this.currentPriority |= 0x2;
      }
      else
      {
        if (((this.currentPriority & 0x4) != 4) && ((this.currentPriority & 0x2) != 2) && ((this.currentPriority & 0x1) != 1))
        {
          this.mVibrator.cancel();
          this.mVibrator.vibrate(new long[] { 1000L, 1000L, 5000L, 1000L }, 0);
        }
        this.currentPriority |= 0x1;
      }
    }
  }
  
  private void startWaitingAnimation(View paramView)
  {
    if (paramView.equals(this.mBtnSnapshot))
    {
      this.mBtnSnapshot.setEnabled(false);
      this.mBtnSnapshot.setBackgroundResource(2130837599);
      ((AnimationDrawable)this.mBtnSnapshot.getBackground()).start();
    }
    for (;;)
    {
      return;
      if (paramView.equals(this.mBtnRecord))
      {
        this.mBtnRecord.setEnabled(false);
        this.mBtnRecord.setBackgroundResource(2130837608);
        ((AnimationDrawable)this.mBtnRecord.getBackground()).start();
      }
      else
      {
        Log.w("FlightModeMainActivity", "Unknown Button to perform waiting animation:" + paramView.toString());
      }
    }
  }
  
  private void stopFPV()
  {
    if (this.mRTVPlayer.isPlaying()) {
      this.mRTVPlayer.stop();
    }
    for (;;)
    {
      ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.mMainscreenLcdFrame.getLayoutParams();
      int i = localMarginLayoutParams.width;
      int j = localMarginLayoutParams.height;
      if ((i != this.initLcdWidth) || (j != this.initLcdHeight))
      {
        this.mStatusBarView.setVisibility(0);
        localMarginLayoutParams.width = this.initLcdWidth;
        localMarginLayoutParams.height = this.initLcdHeight;
        localMarginLayoutParams.setMargins(0, this.initLcdTopMargin, 0, 0);
        this.mMainscreenLcdFrame.setLayoutParams(localMarginLayoutParams);
      }
      this.mLCDDislpayView.setVisibility(4);
      return;
      Log.i("FlightModeMainActivity", "Player has already been stopped");
    }
  }
  
  private void stopVibrator(int paramInt)
  {
    if ((this.currentPriority & paramInt) == 0) {}
    for (;;)
    {
      return;
      this.mVibrator.cancel();
      this.currentPriority &= (paramInt ^ 0xFFFFFFFF);
      if ((this.currentPriority & 0x4) == 4) {
        this.mVibrator.vibrate(new long[] { 1000L, 1000L, 1000L, 1000L }, 0);
      } else if ((this.currentPriority & 0x2) == 2) {
        this.mVibrator.vibrate(new long[] { 1000L, 1000L, 1000L, 1000L }, 0);
      } else if ((this.currentPriority & 0x1) == 1) {
        this.mVibrator.vibrate(new long[] { 1000L, 1000L, 5000L, 1000L }, 0);
      }
    }
  }
  
  private void stopWaitingAnimation(View paramView)
  {
    if (paramView.equals(this.mBtnSnapshot)) {
      if (this.mFlightToggle.isChecked())
      {
        this.mBtnSnapshot.setEnabled(true);
        if ((this.mBtnSnapshot.getBackground() instanceof AnimationDrawable)) {
          ((AnimationDrawable)this.mBtnSnapshot.getBackground()).stop();
        }
        this.mBtnSnapshot.setBackgroundResource(2130837593);
      }
    }
    for (;;)
    {
      return;
      this.mBtnSnapshot.setEnabled(false);
      break;
      if (paramView.equals(this.mBtnRecord))
      {
        if (this.mFlightToggle.isChecked()) {
          this.mBtnRecord.setEnabled(true);
        }
        for (;;)
        {
          if ((this.mBtnRecord.getBackground() instanceof AnimationDrawable)) {
            ((AnimationDrawable)this.mBtnRecord.getBackground()).stop();
          }
          this.mBtnRecord.setBackgroundResource(2130837598);
          if (!this.mIsRecording) {
            break;
          }
          this.mBtnRecord.setEnabled(true);
          break;
          this.mBtnRecord.setEnabled(false);
        }
      }
      Log.w("FlightModeMainActivity", "Unknown Button to stop waiting animation:" + paramView.toString());
    }
  }
  
  private int trimDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 20.0F) {
      f = 20.0F;
    }
    paramFloat = f;
    if (f < -20.0F) {
      paramFloat = -20.0F;
    }
    return (int)paramFloat;
  }
  
  private void updateDrawerLayout()
  {
    boolean bool2 = true;
    WbIsoChangedListener localWbIsoChangedListener = this.wbIsoChangedListener;
    int i = mWBList.indexOf(Integer.valueOf(this.mCameraParams.white_balance));
    boolean bool1;
    if (this.mCurrentExposureIndex == 0)
    {
      bool1 = true;
      localWbIsoChangedListener.onWbModeChanged(i, bool1);
      localWbIsoChangedListener = this.wbIsoChangedListener;
      if (this.mCameraParams.ae_enable != 0) {
        break label241;
      }
      bool1 = bool2;
      label61:
      localWbIsoChangedListener.onIsoModeChanged(bool1);
      this.mWhitBalanceList.setItemSelect(mWBList.indexOf(Integer.valueOf(this.mCameraParams.white_balance)));
      i = this.mEVListValue.indexOf(this.mCameraParams.exposure_value);
      this.mEVpicker.setValue(i);
      if (i != this.mEVpicker.getMaxValue()) {
        break label246;
      }
      this.mEVdecrement.setVisibility(4);
      label136:
      i = this.mISOListValue.indexOf(this.mCameraParams.iso);
      this.mISOpicker.setValue(i);
      if (i != this.mISOpicker.getMaxValue()) {
        break label287;
      }
      this.mISOdecrement.setVisibility(4);
      label180:
      i = this.mSHTimeListValue.indexOf(Integer.toString(this.mCameraParams.shutter_time));
      this.mSTpicker.setValue(i);
      if (i != this.mSTpicker.getMaxValue()) {
        break label328;
      }
      this.mSTdecrement.setVisibility(4);
    }
    for (;;)
    {
      setExposure(this.mCurrentExposureIndex);
      return;
      bool1 = false;
      break;
      label241:
      bool1 = false;
      break label61;
      label246:
      if (i == this.mEVpicker.getMinValue())
      {
        this.mEVincrement.setVisibility(4);
        break label136;
      }
      this.mEVincrement.setVisibility(0);
      this.mEVdecrement.setVisibility(0);
      break label136;
      label287:
      if (i == this.mEVpicker.getMinValue())
      {
        this.mISOincrement.setVisibility(4);
        break label180;
      }
      this.mISOincrement.setVisibility(0);
      this.mISOdecrement.setVisibility(0);
      break label180;
      label328:
      if (i == this.mSTpicker.getMinValue())
      {
        this.mSTincrement.setVisibility(4);
      }
      else
      {
        this.mSTincrement.setVisibility(0);
        this.mSTdecrement.setVisibility(0);
      }
    }
  }
  
  private void updateFmodeButton(UARTInfoMessage.Telemetry paramTelemetry)
  {
    Log.d("FlightModeMainActivity", "Vehicle type is:" + paramTelemetry.vehicle_type + ", fmode is:" + paramTelemetry.f_mode + "info.gps_status:" + paramTelemetry.gps_status);
    if (!this.validVehicle) {
      switch (paramTelemetry.vehicle_type)
      {
      default: 
        this.validVehicle = false;
        setFmodeButtonEnable(false);
      }
    }
    for (;;)
    {
      return;
      this.mVehicleType = paramTelemetry.vehicle_type;
      this.validVehicle = true;
      if (paramTelemetry.vehicle_type == 4)
      {
        if (paramTelemetry.gps_status != 1) {
          setFmodeButtonEnable(false);
        } else {
          setFmodeButtonEnable(true);
        }
      }
      else if ((paramTelemetry.vehicle_type != 2) && (paramTelemetry.vehicle_type != 5))
      {
        setFmodeButtonEnable(false);
      }
      else if ((paramTelemetry.gps_status != 1) || ((paramTelemetry.cgps_status & 0x2) != 2))
      {
        setFmodeButtonEnable(false);
      }
      else
      {
        if (!this.mFmodeButton.isEnabled()) {
          setFmodeButtonEnable(true);
        }
        switch (paramTelemetry.f_mode)
        {
        default: 
          break;
        case 21: 
        case 22: 
          setFmodeButtonChecked(false);
          break;
        case 23: 
        case 24: 
          setFmodeButtonChecked(true);
        }
      }
    }
  }
  
  private void updateVolDrawable(int paramInt)
  {
    if (paramInt <= 0) {
      paramInt = 0;
    }
    for (;;)
    {
      this.mIndicatorVOL.setValueTextDrawableLevel(paramInt);
      return;
      if (paramInt <= 5) {
        paramInt = 10;
      } else if (paramInt <= 25) {
        paramInt = 25;
      } else if (paramInt <= 50) {
        paramInt = 50;
      } else if (paramInt <= 75) {
        paramInt = 75;
      } else {
        paramInt = 100;
      }
    }
  }
  
  private void zoom(int paramInt)
  {
    paramInt = paramInt * 18 / 4096;
    this.mZoombar.setProgress(paramInt);
  }
  
  public int getMotorCount(int paramInt)
  {
    int i = 6;
    if ((paramInt == 1) || (paramInt == 5)) {
      i = 6;
    }
    for (;;)
    {
      return i;
      if (paramInt == 3) {
        i = 4;
      } else if (paramInt == 2) {
        i = 4;
      } else if (paramInt == 4) {
        i = 4;
      } else if (paramInt == 255) {
        i = 8;
      }
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {}
  
  public void onAnimationEnd(Animation paramAnimation)
  {
    if (paramAnimation.equals(this.mBottomFadeIn)) {
      this.mHomeCompassView.setVisibility(0);
    }
    for (;;)
    {
      return;
      if (paramAnimation.equals(this.mBottomFadeOut)) {
        this.mHomeCompassView.setVisibility(4);
      } else if (paramAnimation.equals(this.mTrimLeftFadeIn)) {
        this.mLeftTrimView.setVisibility(0);
      } else if (paramAnimation.equals(this.mTrimLeftFadeOut)) {
        this.mLeftTrimView.setVisibility(4);
      } else if (paramAnimation.equals(this.mTrimRightFadeIn)) {
        this.mRightTrimView.setVisibility(0);
      } else if (paramAnimation.equals(this.mTrimRightFadeOut)) {
        this.mRightTrimView.setVisibility(4);
      }
    }
  }
  
  public void onAnimationRepeat(Animation paramAnimation) {}
  
  public void onAnimationStart(Animation paramAnimation) {}
  
  public void onBackPressed() {}
  
  public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean) {}
  
  public void onClick(View paramView)
  {
    final TwoButtonPopDialog localTwoButtonPopDialog = new TwoButtonPopDialog(this);
    if (paramView.equals(this.mBtnModelSelect)) {
      if (((isBindRX(this, this.mCurrentModelId)) || (isBindWifi(this, this.mCurrentModelId))) && (this.mFlightToggle.isChecked()))
      {
        localTwoButtonPopDialog.adjustHeight(380);
        localTwoButtonPopDialog.setTitle(2131296368);
        localTwoButtonPopDialog.setMessage(2131296369);
        localTwoButtonPopDialog.setPositiveButton(17039379, new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            localTwoButtonPopDialog.dismiss();
            MainActivity.this.mFlightToggle.setChecked(false);
            MainActivity.this.startActivity(new Intent(MainActivity.this, ModelSelectMain.class));
          }
        });
        localTwoButtonPopDialog.setNegativeButton(17039369, new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            localTwoButtonPopDialog.cancel();
          }
        });
        localTwoButtonPopDialog.show();
      }
    }
    do
    {
      for (;;)
      {
        return;
        this.mFlightToggle.setChecked(false);
        startActivity(new Intent(this, ModelSelectMain.class));
        continue;
        if (paramView.equals(this.mBtnFlightSetting))
        {
          if (((isBindRX(this, this.mCurrentModelId)) || (isBindWifi(this, this.mCurrentModelId))) && (this.mFlightToggle.isChecked()))
          {
            localTwoButtonPopDialog.adjustHeight(380);
            localTwoButtonPopDialog.setTitle(2131296368);
            localTwoButtonPopDialog.setMessage(2131296369);
            localTwoButtonPopDialog.setPositiveButton(17039379, new View.OnClickListener()
            {
              public void onClick(View paramAnonymousView)
              {
                localTwoButtonPopDialog.dismiss();
                MainActivity.this.mFlightToggle.setChecked(false);
                MainActivity.this.startActivity(new Intent(MainActivity.this, FlightSettings.class));
              }
            });
            localTwoButtonPopDialog.setNegativeButton(17039369, new View.OnClickListener()
            {
              public void onClick(View paramAnonymousView)
              {
                localTwoButtonPopDialog.cancel();
              }
            });
            localTwoButtonPopDialog.show();
          }
          else
          {
            this.mFlightToggle.setChecked(false);
            startActivity(new Intent(this, FlightSettings.class));
          }
        }
        else if (paramView.equals(this.mBtnSystemSetting))
        {
          if (((isBindRX(this, this.mCurrentModelId)) || (isBindWifi(this, this.mCurrentModelId))) && (this.mFlightToggle.isChecked()))
          {
            localTwoButtonPopDialog.adjustHeight(380);
            localTwoButtonPopDialog.setTitle(2131296368);
            localTwoButtonPopDialog.setMessage(2131296369);
            localTwoButtonPopDialog.setPositiveButton(17039379, new View.OnClickListener()
            {
              public void onClick(View paramAnonymousView)
              {
                localTwoButtonPopDialog.dismiss();
                MainActivity.this.mFlightToggle.setChecked(false);
                MainActivity.this.startActivity(new Intent("android.settings.SETTINGS"));
              }
            });
            localTwoButtonPopDialog.setNegativeButton(17039369, new View.OnClickListener()
            {
              public void onClick(View paramAnonymousView)
              {
                localTwoButtonPopDialog.cancel();
              }
            });
            localTwoButtonPopDialog.show();
          }
          else
          {
            this.mFlightToggle.setChecked(false);
            startActivity(new Intent("android.settings.SETTINGS"));
          }
        }
        else if (paramView.equals(this.mBtnSnapshot))
        {
          startWaitingAnimation(paramView);
          this.mIPCameraManager.snapShot(prepareFilename(), this.mHttpResponseMessenger, this.mCameraInfo);
          this.mStatusBarView.setInfoText(getResources().getString(2131296351), 0);
        }
        else if (paramView.equals(this.mFmodeButton))
        {
          if (this.mVehicleType != 4) {
            break;
          }
          showSpeciallyChannelSender();
        }
      }
    } while (((this.mVehicleType != 2) && (this.mVehicleType != 5)) || (this.mController == null));
    if (!this.mFmodeBtnChecked) {
      this.mController.setTTBState(false, Utilities.HW_VB_BASE + 9, false);
    }
    for (this.mFmodeBtnChecked = true;; this.mFmodeBtnChecked = false)
    {
      setFmodeButtonChecked(this.mFmodeBtnChecked);
      break;
      this.mController.setTTBState(false, Utilities.HW_VB_BASE + 8, false);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Utilities.killBackgroundApps(this);
    getWindow().addFlags(Integer.MIN_VALUE);
    getWindow().addFlags(128);
    Utilities.setCurrentMode(this, 1);
    setContentView(2130903075);
    mIndex = 0;
    int i = 0;
    AlertDialog.Builder localBuilder;
    if (i >= VOL_INDEX_MAX)
    {
      this.mIntentFilter.addAction("android.net.wifi.SCAN_RESULTS");
      this.mIntentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
      this.mIntentFilter.addAction("android.net.wifi.supplicant.STATE_CHANGE");
      this.mIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
      this.wifiManager = ((WifiManager)getSystemService("wifi"));
      setupViews();
      loadAnimation();
      Utilities.checkFirstStart(this);
      preparePlayer();
      prepareVideoFolder();
      prepareSoundPool();
      setupDrawerLayout();
      setVolumeControlStream(3);
      this.mPref = getSharedPreferences("flight_setting_value", 0);
      this.mCurrentModelId = this.mPref.getLong("current_model_id", -2L);
      this.isMasterControl = this.mPref.getInt("master_unit", 2);
      this.mController = UARTController.getInstance();
      if (this.mController != null) {
        break label460;
      }
      paramBundle = UARTController.getPackagenameByProcess(this);
      localBuilder = new AlertDialog.Builder(this);
      if (!"ST10".equals("ST10")) {
        break label310;
      }
      this.mGuardDialog = localBuilder.setTitle(2131296601).setMessage(getResources().getString(2131296602, new Object[] { paramBundle })).setNeutralButton(17039370, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.dismiss();
          MainActivity.this.finish();
        }
      }).setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          MainActivity.this.finish();
        }
      }).create();
      label290:
      this.mGuardDialog.show();
    }
    for (;;)
    {
      return;
      mVOLValue[i] = 0.0F;
      i++;
      break;
      label310:
      if ("ST10".equals("ST12"))
      {
        this.mGuardDialog = localBuilder.setTitle(2131296603).setMessage(getResources().getString(2131296604, new Object[] { paramBundle })).setNeutralButton(17039370, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            paramAnonymousDialogInterface.dismiss();
            MainActivity.this.finish();
          }
        }).setOnCancelListener(new DialogInterface.OnCancelListener()
        {
          public void onCancel(DialogInterface paramAnonymousDialogInterface)
          {
            MainActivity.this.finish();
          }
        }).create();
        break label290;
      }
      if (!"ST10".equals("ST15")) {
        break label290;
      }
      this.mGuardDialog = localBuilder.setTitle(2131296599).setMessage(getResources().getString(2131296600, new Object[] { paramBundle })).setNeutralButton(17039370, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.dismiss();
          MainActivity.this.finish();
        }
      }).setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          MainActivity.this.finish();
        }
      }).create();
      break label290;
      label460:
      if (this.mController != null)
      {
        this.mController.registerReaderHandler(this.mUartHandler);
        this.mController.startReading();
        this.mController.setBindKeyFunction(true, 1, 3);
      }
      this.mGPSUpdater = new GPSUpdater(this);
      this.mCompassUpdater = new CompassUpdater(this);
      this.mVibrator = ((Vibrator)getSystemService("vibrator"));
      this.mGesture = new GestureDetector(this, new GestureListener());
      this.mInitiailized = true;
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add("menu");
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.mRTVPlayer.deinit();
    if (this.mController != null)
    {
      this.mController.startReading();
      this.mController.setBindKeyFunction(true, 0, 3);
      this.mController.clearRadioInfo();
      if (!Utilities.ensureAwaitState(this.mController)) {
        Log.e("FlightModeMainActivity", "fail to change to await");
      }
      this.mController.destory();
      this.mController = null;
    }
    this.mTimerHelper.releaseResource();
    this.mSoundPool.release();
    this.mSoundPool = null;
    this.mInitiailDataTranfered = false;
    this.mInitiailized = false;
    this.mConnectingDialog = null;
    this.mGPSDisabledWarningDialog = null;
    this.mVoltageLowWarning1Dialog = null;
    this.mVoltageLowWarning2Dialog = null;
    this.mAirportWarningDialog = null;
    this.mAltitudeWarningDialog = null;
    this.mRlnBtnPressed = false;
    this.flightLog.closedFlightNote(true);
    ChannelDataForward.getInstance().exit();
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 3) && (!paramKeyEvent.isCanceled())) {
      onBackPressed();
    }
    for (boolean bool = true;; bool = super.onKeyUp(paramInt, paramKeyEvent)) {
      return bool;
    }
  }
  
  public boolean onMenuOpened(int paramInt, Menu paramMenu)
  {
    return false;
  }
  
  protected void onPause()
  {
    super.onPause();
    Log.i("FlightModeMainActivity", "Main Screen onPause");
    this.mHandler.removeCallbacks(this.mWifiEnabledRunnable);
    this.isFSKConneted = false;
    this.isWIFIConneted = false;
    this.mCameraSettingsBtn.setEnabled(false);
    Utilities.setWIFIConnectFlag(false);
    if (this.mController != null)
    {
      this.mPostSendCommand = false;
      Utilities.UartControllerStandBy(this.mController);
    }
    if (this.mWifiThread != null)
    {
      this.mWifiThread.interrupt();
      this.mWifiThread = null;
    }
    dismissConnectingDialog();
    resetWarnings();
    unregisterReceiver(this.receiverWifi);
    this.mCameraDaemon.stop(this);
    this.mCameraDaemon = null;
    this.mRecordProcessing = false;
    this.mHasStartedRecord = false;
    this.mHasStoppedRecord = false;
    this.mLastSDcardStatus = null;
    this.mSDErrDialogShown = false;
    this.mIsPaused = true;
    this.validVehicle = false;
    this.isChangeAndStartRecord = false;
    if (this.handlerThread != null)
    {
      this.handlerThread.quit();
      this.handlerThread.interrupt();
      this.handlerThread = null;
    }
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    
    if (this.mValueForZoom != paramInt)
    {
      this.mHandler.removeCallbacks(this.mZoomRunnable);
      this.mValueForZoom = paramInt;
      this.mHandler.postDelayed(this.mZoomRunnable, 200L);
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    this.flightLog = FlightLog.getInstance();
    Utilities.setStatusBarLeftText(this, this.mStatusBarView);
    initIndicatorValue();
    BindWifiManage localBindWifiManage = new BindWifiManage(this.wifiManager);
    this.mLCDDislpayView.setVisibility(4);
    if (Utilities.isWIFIConnected)
    {
      this.isWIFIConneted = true;
      this.mCameraSettingsBtn.setEnabled(true);
      this.mLCDDislpayView.setVisibility(0);
    }
    this.mCurrentModelId = this.mPref.getLong("current_model_id", -2L);
    this.mCurrentCameraName = this.mPref.getString("camera_current_selected", getResources().getString(2131296262));
    this.isMasterControl = this.mPref.getInt("master_unit", 2);
    if ((this.mCurrentCameraName.equals("C-GO3-Pro")) || (this.mCurrentCameraName.equals("C-GO3")))
    {
      this.mSRswitch.setVisibility(0);
      if (this.mCurrentCameraName.equals("C-GO3-Pro"))
      {
        this.wbIsoFrame.setVisibility(0);
        this.cameraControlCombine.setVisibility(0);
        this.cameraControlCombineVisibility = 0;
        if ("ST10".equals("ST12"))
        {
          if (this.cameraControlCombineVisibility != 0) {
            break label567;
          }
          this.mMissionView.setVisibility(8);
        }
        label225:
        if (this.mCurrentModelId != -2L)
        {
          if (!isBindWifi(this, this.mCurrentModelId)) {
            this.wifiManager.disableNetwork(localBindWifiManage.getCurrentNetId());
          }
          if ((isBindWifi(this, this.mCurrentModelId)) || (isBindRX(this, this.mCurrentModelId)))
          {
            showConnectingDialog();
            connectWifi();
            Utilities.setRunningMode(true);
            this.mIsPlayFPV = true;
          }
        }
        if (this.mController != null)
        {
          this.mController.registerReaderHandler(this.mUartHandler);
          this.mController.startReading();
          if (!this.mInitiailDataTranfered) {
            break label578;
          }
          this.mPostSendCommand = false;
          label339:
          if (!checkIsFPVModel()) {
            break label586;
          }
          enableButtonBar(false);
          this.mBtnFlightSetting.setEnabled(false);
          this.mFlightToggle.setEnabled(false);
          this.mStatusBarView.setInfoText(getResources().getString(2131296355), 0);
          new SyncModelDataTask(this, this.mController, new SyncModelDataTask.SyncModelDataCompletedAction()
          {
            public void SyncModelDataCompleted()
            {
              MainActivity.this.enableButtonBar(true);
              MainActivity.this.mBtnFlightSetting.setEnabled(true);
              MainActivity.this.mFlightToggle.setEnabled(true);
              if ((MainActivity.this.mCurrentModelId != -2L) && (Utilities.getCurrentRxAddrFromDB(MainActivity.this, MainActivity.this.mCurrentModelId) != null))
              {
                MainActivity.this.mIsPlayFPV = true;
                MainActivity.this.mFlightToggle.setChecked(true);
                MainActivity.this.enableButtonBar(false);
              }
              if (MainActivity.this.mCurrentModelType == 406L)
              {
                MainActivity.this.mController.enterSim(true);
                Utilities.ensureSimState(MainActivity.this.mController);
              }
              MainActivity.this.mInitiailDataTranfered = true;
              Log.i("FlightModeMainActivity", "Data Sync Completed");
              MainActivity.this.mController.receiveBothChannel(true, 3);
            }
          }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Long[] { Long.valueOf(this.mCurrentModelId) });
        }
      }
    }
    for (;;)
    {
      Utilities.showFmodeState(this, this.mCurrentModelId, this.mController, this.mStatusBarView);
      this.mSSID.setVisibility(4);
      this.mRssi.setVisibility(4);
      this.mLinkSpeed.setVisibility(4);
      this.mWifiState.setVisibility(4);
      this.mFmodeCH5.setVisibility(8);
      this.mFmodeCH6.setVisibility(8);
      registerReceiver(this.receiverWifi, this.mIntentFilter);
      this.mCameraDaemon = new CameraDaemon(this.mCameraDaemonHandler);
      this.mCameraDaemon.start(this);
      this.mIsPaused = false;
      return;
      this.wbIsoFrame.setVisibility(8);
      break;
      this.wbIsoFrame.setVisibility(8);
      this.mSRswitch.setVisibility(8);
      break;
      label567:
      this.mMissionView.setVisibility(0);
      break label225;
      label578:
      this.mPostSendCommand = true;
      break label339;
      label586:
      this.mInitiailDataTranfered = true;
    }
  }
  
  protected void onStart()
  {
    super.onStart();
    int i = this.mPref.getInt("camera_type_flag", getResources().getInteger(2131492868));
    this.mCurrentModelType = this.mPref.getLong("current_model_type", getResources().getInteger(2131492869));
    if ((i & 0x1) == 1)
    {
      this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 101);
      this.mCurrentVideoLocation = "rtsp://192.168.73.254:8557/PSIA/Streaming/channels/2?videoCodecType=H.264";
      this.isCheckedBindState = false;
      ChannelDataForward.getInstance().setCallback(new ChannelDataForward.ForwardCallback()
      {
        public void onBindResult(boolean paramAnonymousBoolean)
        {
          if (!MainActivity.this.isCheckedBindState) {
            if (!paramAnonymousBoolean) {
              break label131;
            }
          }
          label131:
          for (int i = 2131296640;; i = 2131296641)
          {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(MainActivity.this).setTitle(i).setPositiveButton(2131296276, new DialogInterface.OnClickListener()
            {
              public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
              {
                paramAnonymous2DialogInterface.dismiss();
              }
            });
            if ((MainActivity.this.bindStatePrompt != null) && (MainActivity.this.bindStatePrompt.isShowing())) {
              MainActivity.this.bindStatePrompt.dismiss();
            }
            MainActivity.this.bindStatePrompt = localBuilder.create();
            MainActivity.this.bindStatePrompt.show();
            MainActivity.this.mHandler.postDelayed(new Runnable()
            {
              public void run()
              {
                if ((MainActivity.this.bindStatePrompt != null) && (MainActivity.this.bindStatePrompt.isShowing())) {
                  MainActivity.this.bindStatePrompt.dismiss();
                }
              }
            }, 10000L);
            MainActivity.this.isCheckedBindState = true;
            return;
          }
        }
      });
      if (!(this.mIPCameraManager instanceof Amba2)) {
        break label253;
      }
      ChannelDataForward.getInstance().setBindCamera((Amba2)this.mIPCameraManager);
      label119:
      if ((i & 0x2) != 2) {
        break label262;
      }
      this.mZoombarContainer.setVisibility(0);
    }
    for (;;)
    {
      return;
      if ((i & 0x4) == 4)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 102);
        this.mCurrentVideoLocation = "rtsp://192.168.42.1/live";
        break;
      }
      if (((i & 0x8) == 8) || ((i & 0x20) == 32))
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 104);
        this.mCurrentVideoLocation = "rtsp://192.168.42.1/live";
        break;
      }
      if ((i & 0x10) == 16)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 105);
        this.mCurrentVideoLocation = "rtsp://192.168.110.1/cam1/h264";
        break;
      }
      this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 100);
      this.mCurrentVideoLocation = "rtsp://192.168.73.254:8556/PSIA/Streaming/channels/2?videoCodecType=H.264";
      this.mRlnButton.setVisibility(4);
      break;
      label253:
      ChannelDataForward.getInstance().exit();
      break label119;
      label262:
      this.mZoombarContainer.setVisibility(4);
    }
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  protected void onStop()
  {
    super.onStop();
    this.mIPCameraManager.finish();
    this.mIPCameraManager = null;
    if (this.mGuardDialog != null) {
      this.mGuardDialog.dismiss();
    }
  }
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {}
  
  class GestureListener
    extends GestureDetector.SimpleOnGestureListener
  {
    GestureListener() {}
    
    public boolean onDoubleTap(MotionEvent paramMotionEvent)
    {
      if ((MainActivity.this.mMainscreenLcdFrame != null) && (MainActivity.this.mLCDDislpayView.getVisibility() == 0))
      {
        paramMotionEvent = (ViewGroup.MarginLayoutParams)MainActivity.this.mMainscreenLcdFrame.getLayoutParams();
        int j = paramMotionEvent.width;
        int i = paramMotionEvent.height;
        if ((j == MainActivity.this.initLcdWidth) && (i == MainActivity.this.initLcdHeight))
        {
          if (MainActivity.this.mStatusBarView != null) {
            MainActivity.this.mStatusBarView.setVisibility(8);
          }
          if (MainActivity.this.mCurrentCameraName.equals("C-GO3-Pro")) {
            MainActivity.this.wbIsoFrame.setVisibility(8);
          }
          MainActivity.this.cameraControlCombine.setVisibility(8);
          if ("ST10".equals("ST12")) {
            MainActivity.this.mMissionView.setVisibility(8);
          }
          paramMotionEvent.width = -1;
          paramMotionEvent.height = -1;
          paramMotionEvent.setMargins(0, 0, 0, 0);
          MainActivity.this.mMainscreenLcdFrame.setLayoutParams(paramMotionEvent);
        }
      }
      else
      {
        return true;
      }
      if (MainActivity.this.mStatusBarView != null) {
        MainActivity.this.mStatusBarView.setVisibility(0);
      }
      if ((MainActivity.this.mCurrentCameraName.equals("C-GO3-Pro")) || (MainActivity.this.mCurrentCameraName.equals("C-GO3")))
      {
        MainActivity.this.mSRswitch.setVisibility(0);
        if (MainActivity.this.mCurrentCameraName.equals("C-GO3-Pro"))
        {
          MainActivity.this.wbIsoFrame.setVisibility(MainActivity.this.cameraControlCombineVisibility);
          label277:
          MainActivity.this.cameraControlCombine.setVisibility(MainActivity.this.cameraControlCombineVisibility);
          if ("ST10".equals("ST12"))
          {
            if (MainActivity.this.cameraControlCombineVisibility != 0) {
              break label418;
            }
            MainActivity.this.mMissionView.setVisibility(8);
          }
        }
      }
      for (;;)
      {
        paramMotionEvent.width = MainActivity.this.initLcdWidth;
        paramMotionEvent.height = MainActivity.this.initLcdHeight;
        paramMotionEvent.setMargins(0, MainActivity.this.initLcdTopMargin, 0, 0);
        MainActivity.this.mMainscreenLcdFrame.setLayoutParams(paramMotionEvent);
        break;
        MainActivity.this.wbIsoFrame.setVisibility(8);
        break label277;
        MainActivity.this.mSRswitch.setVisibility(8);
        MainActivity.this.wbIsoFrame.setVisibility(8);
        break label277;
        label418:
        MainActivity.this.mMissionView.setVisibility(0);
      }
    }
    
    public boolean onDown(MotionEvent paramMotionEvent)
    {
      return true;
    }
    
    public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
    {
      boolean bool = false;
      if (MainActivity.this.mStatusBarView.getVisibility() == 8) {
        return bool;
      }
      if (MainActivity.this.mCurrentCameraName.equals("C-GO3-Pro"))
      {
        if (MainActivity.this.wbIsoFrame.getVisibility() == 0) {
          MainActivity.this.wbIsoFrame.setVisibility(8);
        }
      }
      else
      {
        label59:
        if (MainActivity.this.cameraControlCombine.getVisibility() != 0) {
          break label148;
        }
        MainActivity.this.cameraControlCombine.setVisibility(8);
        MainActivity.this.cameraControlCombineVisibility = 8;
        if ("ST10".equals("ST12")) {
          MainActivity.this.mMissionView.setVisibility(0);
        }
      }
      for (;;)
      {
        bool = true;
        break;
        if (MainActivity.this.wbIsoFrame.getVisibility() != 8) {
          break label59;
        }
        MainActivity.this.wbIsoFrame.setVisibility(0);
        break label59;
        label148:
        if (MainActivity.this.cameraControlCombine.getVisibility() == 8)
        {
          MainActivity.this.cameraControlCombine.setVisibility(0);
          MainActivity.this.cameraControlCombineVisibility = 0;
          if ("ST10".equals("ST12")) {
            MainActivity.this.mMissionView.setVisibility(8);
          }
        }
      }
    }
  }
  
  private class HttpRequestHandler
    extends WeakHandler<MainActivity>
  {
    public HttpRequestHandler(MainActivity paramMainActivity)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      MainActivity localMainActivity = (MainActivity)getOwner();
      if (localMainActivity == null) {}
      for (;;)
      {
        return;
        if (localMainActivity.mIsPaused) {
          Log.d("FlightModeMainActivity", "the activity is paused");
        } else {
          switch (paramMessage.what)
          {
          default: 
            break;
          case 1: 
            switch (paramMessage.arg1)
            {
            case 0: 
            case 1: 
            default: 
              break;
            case 2: 
              Log.i("FlightModeMainActivity", "#### Rec Start=" + paramMessage.obj);
              if (!"HTTPCODE OK".equals(paramMessage.obj))
              {
                localMainActivity.mIsRecording = false;
                localMainActivity.mBtnRecord.setChecked(false);
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296349), -65536);
                localMainActivity.mHasStoppedRecord = false;
              }
            case 24: 
            case 25: 
              for (;;)
              {
                localMainActivity.mRecordProcessing = false;
                localMainActivity.stopWaitingAnimation(localMainActivity.mBtnRecord);
                break;
                if ((paramMessage.obj instanceof String))
                {
                  if ("HTTPCODE OK".equals(String.valueOf(paramMessage.obj)))
                  {
                    Log.i("FlightModeMainActivity", "Init camera complete");
                    localMainActivity.mIPCameraManager.syncTime(localMainActivity.mHttpResponseMessenger);
                    localMainActivity.refreshScreen();
                    if (!localMainActivity.mIsPlayFPV) {
                      break;
                    }
                    localMainActivity.playFPV();
                    break;
                  }
                  localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296343), 0);
                  break;
                }
                if (!(paramMessage.obj instanceof CameraParams)) {
                  break;
                }
                paramMessage = (CameraParams)paramMessage.obj;
                if ("HTTPCODE OK".equals(paramMessage.response))
                {
                  Log.i("FlightModeMainActivity", "Init camera complete");
                  localMainActivity.mCameraParams = paramMessage;
                  localMainActivity.mIPCameraManager.syncTime(localMainActivity.mHttpResponseMessenger);
                  localMainActivity.refreshScreen();
                  if (!localMainActivity.mIsPlayFPV) {
                    break;
                  }
                  localMainActivity.playFPV();
                  break;
                }
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296343), 0);
                break;
                Log.i("FlightModeMainActivity", "#### Rec REQUEST_GET_REC_TIME=" + paramMessage.obj);
                if (!(paramMessage.obj instanceof IPCameraManager.RecordTime)) {
                  break;
                }
                paramMessage = (IPCameraManager.RecordTime)paramMessage.obj;
                Log.d("FlightModeMainActivity", "#### Rec get record time: " + paramMessage.recTime);
                localMainActivity.mRecTime.setVisibility(0);
                localMainActivity.mRecTime.setStartTime(paramMessage.recTime * 1000);
                localMainActivity.mRecTime.start();
                if ((!localMainActivity.mCurrentCameraName.equals("C-GO3-Pro")) && (!localMainActivity.mCurrentCameraName.equals("C-GO3"))) {
                  break;
                }
                localMainActivity.mSRswitch.setVisibility(4);
                break;
                localMainActivity.mHasStartedRecord = true;
                localMainActivity.mIsRecording = true;
                localMainActivity.mBtnRecord.setChecked(true);
                localMainActivity.mRecTime.setVisibility(0);
                localMainActivity.mRecTime.setStartTime(0);
                localMainActivity.mRecTime.start();
                if ((localMainActivity.mCurrentCameraName.equals("C-GO3-Pro")) || (localMainActivity.mCurrentCameraName.equals("C-GO3"))) {
                  localMainActivity.mSRswitch.setVisibility(4);
                }
              }
            case 3: 
              Log.i("FlightModeMainActivity", "#### Rec Stop=" + paramMessage.obj);
              if (!"HTTPCODE OK".equals(paramMessage.obj))
              {
                localMainActivity.mIsRecording = true;
                localMainActivity.mBtnRecord.setChecked(true);
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296350), -65536);
                localMainActivity.mHasStartedRecord = false;
              }
              for (;;)
              {
                localMainActivity.mRecordProcessing = false;
                localMainActivity.stopWaitingAnimation(localMainActivity.mBtnRecord);
                break;
                localMainActivity.mHasStoppedRecord = true;
                localMainActivity.mIsRecording = false;
                localMainActivity.mBtnRecord.setChecked(false);
                localMainActivity.mRecTime.setVisibility(4);
                localMainActivity.mRecTime.stop();
                if ((localMainActivity.mCurrentCameraName.equals("C-GO3-Pro")) || (localMainActivity.mCurrentCameraName.equals("C-GO3"))) {
                  localMainActivity.mSRswitch.setVisibility(0);
                }
              }
            case 4: 
              if ("HTTPCODE OK".equals(paramMessage.obj)) {
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296352), 0);
              }
              for (;;)
              {
                localMainActivity.mSnapShotProcessing = false;
                localMainActivity.stopWaitingAnimation(localMainActivity.mBtnSnapshot);
                break;
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296348), -65536);
              }
            case 10: 
              if ((paramMessage.obj instanceof IPCameraManager.SDCardStatus))
              {
                paramMessage = (IPCameraManager.SDCardStatus)paramMessage.obj;
                localMainActivity.dismissCommunicatingDialog();
                localMainActivity.onSDcardChanged(paramMessage);
              }
              else if ("HTTPCODE Internal Error".equals(paramMessage.obj))
              {
                Log.i("FlightModeMainActivity", "get sdcard status error, try again");
                if (localMainActivity.mIPCameraManager != null) {
                  localMainActivity.mIPCameraManager.getSDCardStatus(localMainActivity.mHttpResponseMessenger);
                }
              }
              else
              {
                Log.i("FlightModeMainActivity", "request sdcard status error:" + paramMessage.obj);
              }
              break;
            case 32: 
              if ((paramMessage.obj instanceof String)) {
                localMainActivity.setRlnBtnText((String)paramMessage.obj);
              }
              break;
            case 31: 
              if ("HTTPCODE OK".equals(String.valueOf(paramMessage.obj))) {
                localMainActivity.setRlnBtnText(localMainActivity.mResolution);
              }
              for (;;)
              {
                localMainActivity.refreshScreen();
                break;
                Log.e("FlightModeMainActivity", "Failed to set resolution");
                localMainActivity.dismissProgressDialog(localMainActivity.mRlnProgressRunnable);
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296391), -65536);
              }
            case 33: 
              if ("HTTPCODE OK".equals(String.valueOf(paramMessage.obj)))
              {
                localMainActivity.setVideoResolution(localMainActivity.mResolution);
              }
              else
              {
                Log.e("FlightModeMainActivity", "Failed to set standard");
                localMainActivity.dismissProgressDialog(localMainActivity.mRlnProgressRunnable);
                localMainActivity.mStatusBarView.setInfoText(localMainActivity.getResources().getString(2131296391), -65536);
              }
              break;
            case 26: 
              if ("HTTPCODE OK".equals(String.valueOf(paramMessage.obj))) {
                localMainActivity.playFPV();
              }
              localMainActivity.dismissProgressDialog(null);
              break;
            case 27: 
              if ("HTTPCODE OK".equals(String.valueOf(paramMessage.obj))) {
                if (localMainActivity.mRlnBtnPressed) {
                  localMainActivity.mRlnBtnPressed = false;
                }
              }
              for (;;)
              {
                localMainActivity.dismissProgressDialog(null);
                break;
                localMainActivity.refreshScreen();
              }
            case 49: 
            case 53: 
            case 55: 
              if (!"HTTPCODE OK".equals(String.valueOf(paramMessage.obj))) {
                Log.e("FlightModeMainActivity", "Failed to communicate camera");
              } else {
                localMainActivity.dismissCommunicatingDialog();
              }
              break;
            case 48: 
              if ((paramMessage.obj instanceof Integer)) {
                localMainActivity.mCameraParams.ae_enable = ((Integer)paramMessage.obj).intValue();
              }
              break;
            case 54: 
              if ((paramMessage.obj instanceof Integer)) {
                localMainActivity.mCameraParams.white_balance = ((Integer)paramMessage.obj).intValue();
              }
              break;
            case 56: 
              if ((paramMessage.obj instanceof String)) {
                localMainActivity.mCameraParams.exposure_value = ((String)paramMessage.obj);
              }
              break;
            case 50: 
              if ((paramMessage.obj instanceof IPCameraManager.ShutterTimeISO))
              {
                paramMessage = (IPCameraManager.ShutterTimeISO)paramMessage.obj;
                localMainActivity.mCameraParams.iso = paramMessage.iso;
                localMainActivity.mCameraParams.shutter_time = paramMessage.time;
              }
              break;
            case 23: 
              if ((paramMessage.obj instanceof String))
              {
                paramMessage = (String)paramMessage.obj;
                Log.i("FlightModeMainActivity", "camera version is " + paramMessage);
              }
              break;
            case 62: 
              if ((paramMessage.obj instanceof Integer))
              {
                int i = ((Integer)paramMessage.obj).intValue();
                if ((i == 1) && (localMainActivity.isChangeAndStartRecord))
                {
                  localMainActivity.mBtnRecord.performClick();
                  localMainActivity.isChangeAndStartRecord = false;
                }
                localMainActivity.mCameraParams.cam_mode = i;
                Log.d("FlightModeMainActivity", "CameraMode--get camera mode: " + i);
              }
              break;
            }
            break;
          }
        }
      }
    }
  }
  
  private class WifiThread
    extends Thread
  {
    private WifiManager mWifi;
    
    public WifiThread(WifiManager paramWifiManager)
    {
      this.mWifi = paramWifiManager;
      setName("Wifi State");
    }
    
    public void run()
    {
      for (;;)
      {
        try
        {
          localObject = this.mWifi.getConnectionInfo();
          if (localObject != null)
          {
            str4 = ((WifiInfo)localObject).getSSID();
            String str1 = String.valueOf(((WifiInfo)localObject).getRssi());
            str3 = String.valueOf(((WifiInfo)localObject).getLinkSpeed());
            localObject = ((WifiInfo)localObject).getSupplicantState().name();
            Handler localHandler = MainActivity.this.mHandler;
            Runnable local1 = new com/yuneec/flightmode15/MainActivity$WifiThread$1;
            local1.<init>(this, str4, str1, str3, (String)localObject);
            localHandler.post(local1);
            Thread.sleep(1000L);
            continue;
          }
          String str4 = "null";
        }
        catch (InterruptedException localInterruptedException)
        {
          MainActivity.this.mHandler.post(new Runnable()
          {
            public void run()
            {
              Toast.makeText(MainActivity.this, "WifiThread stopped", 0).show();
            }
          });
          return;
        }
        String str2 = "null";
        String str3 = " null";
        Object localObject = "Unknown";
      }
    }
  }
}


