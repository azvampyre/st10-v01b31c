package com.yuneec.flightmode15;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import java.util.Timer;
import java.util.TimerTask;

public class CompassUpdater
  implements SensorEventListener
{
  public static final int EVENT_ACCURACY_CHANGED = 201;
  public static final int EVENT_COMPASS_UPDATED = 200;
  private static final int UPDATE_INTERVAL = 200;
  private int mDegree;
  private Handler mHandler;
  private SensorManager mSensorManager;
  private Timer mUpdateTimer;
  private UpdateTimerTask mUpdateTimerTask;
  
  public CompassUpdater(Context paramContext)
  {
    this.mSensorManager = ((SensorManager)paramContext.getSystemService("sensor"));
  }
  
  private void registerHandler(Handler paramHandler)
  {
    this.mHandler = paramHandler;
  }
  
  private void unregisterHandler()
  {
    this.mHandler = null;
  }
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
    if (this.mHandler == null) {}
    for (;;)
    {
      return;
      paramSensor = Message.obtain();
      paramSensor.what = 201;
      paramSensor.arg1 = paramInt;
      this.mHandler.sendMessage(paramSensor);
    }
  }
  
  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    if (this.mHandler == null) {}
    for (;;)
    {
      return;
      this.mDegree = ((int)Utilities.normalizeDegree(paramSensorEvent.values[0] * -1.0F));
      this.mDegree = ((int)Utilities.normalizeDegree(this.mDegree * -1.0F));
      this.mDegree = ((this.mDegree + 90) % 360);
    }
  }
  
  public void start(Handler paramHandler)
  {
    registerHandler(paramHandler);
    this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(3), 2);
    this.mUpdateTimer = new Timer("CompassUpdater");
    this.mUpdateTimerTask = new UpdateTimerTask(null);
    this.mUpdateTimer.scheduleAtFixedRate(this.mUpdateTimerTask, 200L, 200L);
  }
  
  public void stop()
  {
    unregisterHandler();
    this.mSensorManager.unregisterListener(this, this.mSensorManager.getDefaultSensor(3));
    if (this.mUpdateTimer != null)
    {
      this.mUpdateTimer.cancel();
      if (this.mUpdateTimerTask != null) {
        this.mUpdateTimerTask.cancel();
      }
      this.mUpdateTimer = null;
    }
  }
  
  private class UpdateTimerTask
    extends TimerTask
  {
    private UpdateTimerTask() {}
    
    public void run()
    {
      if (CompassUpdater.this.mHandler != null)
      {
        Message localMessage = Message.obtain();
        localMessage.what = 200;
        localMessage.arg1 = CompassUpdater.this.mDegree;
        CompassUpdater.this.mHandler.sendMessage(localMessage);
      }
    }
  }
}


