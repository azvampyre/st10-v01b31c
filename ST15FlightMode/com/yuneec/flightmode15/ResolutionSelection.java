package com.yuneec.flightmode15;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class ResolutionSelection
  extends Dialog
{
  private RadioGroup mRadioGroup;
  
  public ResolutionSelection(Context paramContext)
  {
    super(paramContext, 2131230729);
    setContentView(2130903103);
    this.mRadioGroup = ((RadioGroup)findViewById(2131689812));
  }
  
  public void adjustHeight(int paramInt)
  {
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.height = paramInt;
    getWindow().setAttributes(localLayoutParams);
  }
  
  public String getValue()
  {
    String str = null;
    int i = this.mRadioGroup.getCheckedRadioButtonId();
    if (i == 2131689905) {
      str = "48P";
    }
    for (;;)
    {
      return str;
      if (i == 2131689906) {
        str = "50P";
      } else if (i == 2131689907) {
        str = "60P";
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
  
  public void setMessageGravity(int paramInt)
  {
    this.mRadioGroup.setGravity(paramInt);
  }
  
  public void setSelectionListener(RadioGroup.OnCheckedChangeListener paramOnCheckedChangeListener)
  {
    this.mRadioGroup.setOnCheckedChangeListener(paramOnCheckedChangeListener);
  }
  
  public void setValue(String paramString)
  {
    if (paramString != null)
    {
      if (!"48P".equals(paramString)) {
        break label23;
      }
      this.mRadioGroup.check(2131689905);
    }
    for (;;)
    {
      return;
      label23:
      if ("50P".equals(paramString)) {
        this.mRadioGroup.check(2131689906);
      } else if ("60P".equals(paramString)) {
        this.mRadioGroup.check(2131689907);
      }
    }
  }
}


