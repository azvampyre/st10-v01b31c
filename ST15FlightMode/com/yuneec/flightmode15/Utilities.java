package com.yuneec.flightmode15;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.provider.MediaStore.Images.Thumbnails;
import android.util.Log;
import android.util.SparseIntArray;
import android.widget.ImageView;
import com.yuneec.channelsettings.DRData;
import com.yuneec.channelsettings.DR_Fragment;
import com.yuneec.channelsettings.ServoData;
import com.yuneec.channelsettings.ServoSetupFragment;
import com.yuneec.channelsettings.ThrottleCurveFragment;
import com.yuneec.channelsettings.ThrottleData;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.BindWifiManage;
import com.yuneec.flight_settings.ChannelMap;
import com.yuneec.flight_settings.WifiConnect;
import com.yuneec.uartcontroller.MixedData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.StatusbarView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Utilities
{
  public static int BIND_KEY_INDEX = 0;
  public static final float B_EXPO_1_MAX = 1.0F;
  public static final float B_EXPO_1_MID = 0.17F;
  public static final float B_EXPO_1_MIN = 0.0F;
  public static final float B_EXPO_2_MAX = 0.5F;
  public static final float B_EXPO_2_MID = 0.0F;
  public static final float B_EXPO_2_MIN = -0.5F;
  public static final float B_SWITCH_MAX = 1.0F;
  public static final float B_SWITCH_MID = 0.0F;
  public static final float B_SWITCH_MIN = -1.0F;
  public static int CAMERA_KEY_INDEX = 0;
  public static float CONVERSION_MI_TO_FT = 0.0F;
  public static float CONVERSION_MPS_TO_KPH = 0.0F;
  public static float CONVERSION_MPS_TO_MPH = 0.0F;
  public static float CONVERSION_MTR_TO_FT = 0.0F;
  public static final int DEFAULT_ANALOG_NUM = 10;
  public static final int DEFAULT_CHANNEL_NUM = 12;
  public static final int DEFAULT_FMODE_NUM = 3;
  public static final int DEFAULT_SWITCH_NUM = 2;
  public static final int EXPO_MAX = 100;
  public static final int EXPO_MID = 0;
  public static final int EXPO_MIN = -100;
  private static final String FIRST_START_PREFERENCE = "first_start_preference";
  public static final int FLAG_HOMEKEY_DISPATCHED = Integer.MIN_VALUE;
  public static int FMODE_KEY_INDEX = 0;
  public static final boolean FMODE_OPTION_OPEN = false;
  public static final int FPV_MODE = 1;
  public static final long GIMBAL_CONTROL_MODEL = 406L;
  public static final ArrayList<String> HARDWARE_ARRAYLIST;
  public static final int HARDWARE_TYPE_ANALOG = 0;
  public static final int HARDWARE_TYPE_SWITCH = 1;
  public static final int HARDWARE_TYPE_TOUCHSCREEN = 2;
  public static int HW_B_BASE = 0;
  public static SparseIntArray HW_CHANNEL_INDEX = new SparseIntArray();
  public static int HW_J_BASE = 0;
  public static int HW_K_BASE = 10;
  public static int HW_S_BASE = 0;
  public static int HW_T_BASE = 20;
  public static int HW_VB_BASE = 0;
  public static int HW_VS_BASE = 0;
  public static final float K_MAX = 2.0F;
  public static final float K_MID = 0.67F;
  public static final float K_MIN = 0.5F;
  public static final float LEFT_K_MAX = 1.0F;
  public static final float LEFT_K_MID = 0.17F;
  public static final float LEFT_K_MIN = 0.0F;
  public static final int NO_MODEL_SELECTED = -2;
  public static final float N_MAX = 4.0F;
  public static final float N_MID = 1.0F;
  public static final float N_MIN = 0.3F;
  public static final int OFFSET_MAX = 100;
  public static final int OFFSET_MID = 0;
  public static final int OFFSET_MIN = -100;
  public static final int OFFSET_SWITCH_MAX_1 = 125;
  public static final int OFFSET_SWITCH_MAX_2 = 150;
  public static final int OFFSET_SWITCH_MID_1 = 0;
  public static final int OFFSET_SWITCH_MID_2 = 0;
  public static final int OFFSET_SWITCH_MIN_1 = -25;
  public static final int OFFSET_SWITCH_MIN_2 = -150;
  public static final boolean OPTIMIZE_PICKER_SOUND_POOL = true;
  public static final String PROJECT_TAG = "ST10";
  public static final int RATE_MAX = 100;
  public static final int RATE_MID = 0;
  public static final int RATE_MIN = -100;
  public static final int RC_MODE = 0;
  public static final float RIGHT_K_MAX = 1.5F;
  public static final float RIGHT_K_MID = 0.84F;
  public static final float RIGHT_K_MIN = -0.3F;
  public static final String SDCARD2_PATH = "/storage/sdcard1";
  private static final String TAG = "Utilities";
  public static final int THROTTLE_CHANNEL = 1;
  public static final boolean TRIM_ENABLE = false;
  public static String UNIT_FEET;
  public static String UNIT_FEET_PER_SECOND;
  public static String UNIT_KELOMETER;
  public static String UNIT_KELOMETER_PER_HOUR;
  public static String UNIT_METER;
  public static String UNIT_METER_PER_SECOND;
  public static String UNIT_MILE;
  public static String UNIT_MILE_PER_HOUR;
  public static int VIDEO_KEY_INDEX;
  public static int ZOOM_KEY_INDEX;
  public static boolean isWIFIConnected = false;
  private static SharedPreferences mPrefsUtil;
  public static boolean mSendDataCompleted;
  private static int sCurrentMode;
  private static boolean sIsRunningMode;
  private static String[] sKillAppsBlacklist;
  private static String[] sKillAppsWhitelist;
  private static MyProgressDialog sProgressDialog;
  public static final int[] value_ch5;
  public static final int[] value_ch6;
  
  static
  {
    HW_S_BASE = 30;
    HW_B_BASE = 50;
    HW_VS_BASE = 70;
    HW_VB_BASE = 90;
    sCurrentMode = -1;
    sIsRunningMode = false;
    UNIT_METER = "m";
    UNIT_KELOMETER = "km";
    UNIT_FEET = "ft";
    UNIT_MILE = "mi";
    UNIT_METER_PER_SECOND = "mps";
    UNIT_KELOMETER_PER_HOUR = "kph";
    UNIT_FEET_PER_SECOND = "fps";
    UNIT_MILE_PER_HOUR = "mph";
    CONVERSION_MTR_TO_FT = 3.2808F;
    CONVERSION_MI_TO_FT = 5280.0F;
    CONVERSION_MPS_TO_KPH = 3.6F;
    CONVERSION_MPS_TO_MPH = 2.2369F;
    mSendDataCompleted = false;
    HARDWARE_ARRAYLIST = new ArrayList();
    HARDWARE_ARRAYLIST.add("INH");
    HARDWARE_ARRAYLIST.add("J1");
    HARDWARE_ARRAYLIST.add("J2");
    HARDWARE_ARRAYLIST.add("J3");
    HARDWARE_ARRAYLIST.add("J4");
    HARDWARE_ARRAYLIST.add("J5");
    HARDWARE_ARRAYLIST.add("J6");
    HARDWARE_ARRAYLIST.add("J7");
    HARDWARE_ARRAYLIST.add("J8");
    HARDWARE_ARRAYLIST.add("J9");
    HARDWARE_ARRAYLIST.add("J10");
    HARDWARE_ARRAYLIST.add("K1");
    HARDWARE_ARRAYLIST.add("K2");
    HARDWARE_ARRAYLIST.add("K3");
    HARDWARE_ARRAYLIST.add("K4");
    HARDWARE_ARRAYLIST.add("K5");
    HARDWARE_ARRAYLIST.add("K6");
    HARDWARE_ARRAYLIST.add("K7");
    HARDWARE_ARRAYLIST.add("K8");
    HARDWARE_ARRAYLIST.add("K9");
    HARDWARE_ARRAYLIST.add("K10");
    HARDWARE_ARRAYLIST.add("S1");
    HARDWARE_ARRAYLIST.add("S2");
    HARDWARE_ARRAYLIST.add("S3");
    HARDWARE_ARRAYLIST.add("S4");
    HARDWARE_ARRAYLIST.add("S5");
    HARDWARE_ARRAYLIST.add("S6");
    HARDWARE_ARRAYLIST.add("S7");
    HARDWARE_ARRAYLIST.add("S8");
    HARDWARE_ARRAYLIST.add("S9");
    HARDWARE_ARRAYLIST.add("S10");
    HARDWARE_ARRAYLIST.add("B1");
    HARDWARE_ARRAYLIST.add("B2");
    HARDWARE_ARRAYLIST.add("B3");
    HARDWARE_ARRAYLIST.add("B4");
    HARDWARE_ARRAYLIST.add("B5");
    HARDWARE_ARRAYLIST.add("B6");
    HARDWARE_ARRAYLIST.add("B7");
    HARDWARE_ARRAYLIST.add("B8");
    HARDWARE_ARRAYLIST.add("B9");
    HARDWARE_ARRAYLIST.add("B10");
    sKillAppsBlacklist = new String[] { "com.android.quicksearchbox", "com.android.musicfx", "com.android.defcontainer", "com.android.settings", "com.android.gallery3d", "com.mediatek.weather", "com.mediatek.appwidget.weather", "com.android.contacts", "com.android.providers.calendar", "com.android.deskclock", "com.android.email", "com.android.exchange", "com.android.mms", "com.android.calendar" };
    sKillAppsWhitelist = new String[] { "com.android", "android", "system", "com.mediatek", "com.yuneec" };
    value_ch5 = new int[3];
    value_ch6 = new int[3];
  }
  
  public static String FormatHomeDistanceDisplayString(Context paramContext, float paramFloat)
  {
    String str2 = paramContext.getResources().getString(2131296311, new Object[] { "N/A", getDisplayLengthUnit(paramContext) });
    String str1 = str2;
    float f;
    if (paramFloat >= 0.0F)
    {
      str1 = str2;
      if (paramFloat <= 1.0E7F)
      {
        if (1 != getUnit(paramContext)) {
          break label111;
        }
        f = paramFloat;
        str1 = UNIT_METER;
        paramFloat = f;
        if (f >= 10000.0F)
        {
          paramFloat = f / 1000.0F;
          str1 = UNIT_KELOMETER;
        }
      }
    }
    for (;;)
    {
      str1 = paramContext.getResources().getString(2131296312, new Object[] { Float.valueOf(paramFloat), str1 });
      return str1;
      label111:
      f = paramFloat * CONVERSION_MTR_TO_FT;
      str1 = UNIT_FEET;
      paramFloat = f;
      if (f >= 10000.0F)
      {
        paramFloat = f / CONVERSION_MI_TO_FT;
        str1 = UNIT_MILE;
      }
    }
  }
  
  public static String FormatLengthDisplayString(Context paramContext, float paramFloat)
  {
    String str = getDisplayLengthUnit(paramContext);
    if (str.equals(UNIT_METER)) {}
    for (;;)
    {
      return paramContext.getResources().getString(2131296312, new Object[] { Float.valueOf(paramFloat), str });
      paramFloat *= CONVERSION_MTR_TO_FT;
    }
  }
  
  public static boolean FormatPositionDisplayStatic(float paramFloat1, float paramFloat2)
  {
    if ((-180.0F < paramFloat1) && (paramFloat1 < 180.0F) && (-180.0F < paramFloat2) && (paramFloat2 < 180.0F)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static String FormatPositionDisplayString(Context paramContext, float paramFloat1, float paramFloat2)
  {
    String str1;
    if (paramFloat1 >= 0.0F)
    {
      str1 = "E";
      if (paramFloat2 < 0.0F) {
        break label76;
      }
    }
    label76:
    for (String str2 = "N";; str2 = "S")
    {
      paramFloat1 = Math.abs(paramFloat1);
      paramFloat2 = Math.abs(paramFloat2);
      return paramContext.getResources().getString(2131296315, new Object[] { Float.valueOf(paramFloat1), str1, Float.valueOf(paramFloat2), str2 });
      str1 = "W";
      break;
    }
  }
  
  public static String FormatVelocityDisplayString(Context paramContext, float paramFloat)
  {
    String str = getDisplayVelocityUnit(paramContext);
    if (str.equals(UNIT_KELOMETER_PER_HOUR)) {}
    for (paramFloat *= CONVERSION_MPS_TO_KPH;; paramFloat *= CONVERSION_MPS_TO_MPH) {
      return paramContext.getResources().getString(2131296312, new Object[] { Float.valueOf(paramFloat), str });
    }
  }
  
  static ArrayList<MixedData> GB603CameraMixMode()
  {
    ArrayList localArrayList = new ArrayList();
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 1;
    localMixedData.mhardware = (HW_J_BASE + 3);
    localMixedData.mHardwareType = 1;
    localMixedData.mPriority = 1;
    localMixedData.mCurvePoint = initDRCurve(0);
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 2;
    localMixedData.mhardware = (HW_J_BASE + 4);
    localMixedData.mHardwareType = 1;
    localMixedData.mPriority = 1;
    localMixedData.mCurvePoint = initDRCurve(0);
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 5;
    localMixedData.mhardware = (HW_S_BASE + 1);
    localMixedData.mHardwareType = 2;
    localMixedData.mPriority = 1;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(100));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 6;
    localMixedData.mhardware = (HW_S_BASE + 2);
    localMixedData.mHardwareType = 2;
    localMixedData.mPriority = 1;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(-100));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
    localMixedData.mSwitchValue.add(2, Integer.valueOf(100));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 7;
    localMixedData.mhardware = (HW_K_BASE + 1);
    localMixedData.mHardwareType = 1;
    localMixedData.mPriority = 1;
    localMixedData.mCurvePoint = initDRCurve(0);
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    return localArrayList;
  }
  
  static ArrayList<MixedData> GimbalMixMode(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 6;
    localMixedData.mhardware = (HW_K_BASE + 1);
    localMixedData.mHardwareType = 1;
    localMixedData.mPriority = 1;
    localMixedData.mCurvePoint = initDRCurve(0);
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 7;
    switch (paramInt)
    {
    default: 
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = true;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 8;
      switch (paramInt)
      {
      }
      break;
    }
    for (;;)
    {
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 9;
      localMixedData.mhardware = (HW_S_BASE + 1);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(99));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(11));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(11));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 10;
      localMixedData.mhardware = (HW_S_BASE + 2);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(-106));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(-47));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(99));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      return localArrayList;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break;
      localMixedData.mhardware = (HW_J_BASE + 4);
      continue;
      localMixedData.mhardware = (HW_J_BASE + 2);
    }
  }
  
  static ArrayList<MixedData> H920MixMode(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 1;
    switch (paramInt)
    {
    default: 
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initThrCurve();
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 1;
      localMixedData.mhardware = (HW_B_BASE + 3);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 2;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(-50));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(-50));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 2;
      switch (paramInt)
      {
      default: 
        label272:
        localMixedData.mHardwareType = 1;
        localMixedData.mPriority = 1;
        localMixedData.mCurvePoint = initDRCurve(0);
        localMixedData.mSpeed = 10;
        localMixedData.mReverse = false;
        localArrayList.add(localMixedData);
        localMixedData = new MixedData();
        localMixedData.mFmode = 0;
        localMixedData.mChannel = 3;
        switch (paramInt)
        {
        default: 
          label356:
          localMixedData.mHardwareType = 1;
          localMixedData.mPriority = 1;
          localMixedData.mCurvePoint = initDRCurve(0);
          localMixedData.mSpeed = 10;
          localMixedData.mReverse = false;
          localArrayList.add(localMixedData);
          localMixedData = new MixedData();
          localMixedData.mFmode = 0;
          localMixedData.mChannel = 4;
          switch (paramInt)
          {
          }
          break;
        }
        break;
      }
      break;
    }
    for (;;)
    {
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(30);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 5;
      localMixedData.mhardware = (HW_S_BASE + 2);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(false));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(100));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(0));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 6;
      localMixedData.mhardware = (HW_S_BASE + 2);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 2;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(false));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(150));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 6;
      localMixedData.mhardware = (HW_K_BASE + 3);
      localMixedData.mHardwareType = 1;
      localMixedData.mCurvePoint = initDRCurve(1);
      localMixedData.mPriority = 3;
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 7;
      localMixedData.mhardware = HW_J_BASE;
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 8;
      localMixedData.mhardware = HW_J_BASE;
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 9;
      localMixedData.mhardware = HW_J_BASE;
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 10;
      localMixedData.mhardware = HW_J_BASE;
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 11;
      localMixedData.mhardware = (HW_S_BASE + 1);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(-100));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(100));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      return localArrayList;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break;
      localMixedData.mhardware = (HW_J_BASE + 4);
      break label272;
      localMixedData.mhardware = (HW_J_BASE + 2);
      break label272;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break label356;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break label356;
      localMixedData.mhardware = (HW_J_BASE + 2);
      continue;
      localMixedData.mhardware = (HW_J_BASE + 4);
    }
  }
  
  public static void UartControllerStandBy(UARTController paramUARTController)
  {
    paramUARTController.stopReading();
    paramUARTController.registerReaderHandler(null);
  }
  
  static ArrayList<MixedData> V18CameraMixMode()
  {
    ArrayList localArrayList = new ArrayList();
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 1;
    localMixedData.mhardware = (HW_J_BASE + 3);
    localMixedData.mHardwareType = 1;
    localMixedData.mPriority = 1;
    localMixedData.mCurvePoint = initDRCurve(0);
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 2;
    localMixedData.mhardware = (HW_J_BASE + 4);
    localMixedData.mHardwareType = 1;
    localMixedData.mPriority = 1;
    localMixedData.mCurvePoint = initDRCurve(0);
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 5;
    localMixedData.mhardware = (HW_S_BASE + 1);
    localMixedData.mHardwareType = 2;
    localMixedData.mPriority = 1;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(100));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(100));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 6;
    localMixedData.mhardware = (HW_S_BASE + 2);
    localMixedData.mHardwareType = 2;
    localMixedData.mPriority = 1;
    localMixedData.mMixedType = 3;
    localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
    localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
    localMixedData.mSwitchValue.add(0, Integer.valueOf(-100));
    localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
    localMixedData.mSwitchValue.add(2, Integer.valueOf(100));
    localMixedData.mSpeed = 10;
    localMixedData.mReverse = false;
    localArrayList.add(localMixedData);
    return localArrayList;
  }
  
  public static void backToFlightScreen(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, MainActivity.class));
  }
  
  public static float[] calculateDistanceAndBearing(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    float[] arrayOfFloat = new float[2];
    if (((paramFloat1 == 0.0F) && (paramFloat2 == 0.0F)) || ((paramFloat3 == 0.0F) && (paramFloat4 == 0.0F)))
    {
      Log.d("Utilities", "can't get plane or controller's location:" + paramFloat1 + "," + paramFloat2 + "..." + paramFloat3 + "," + paramFloat4);
      arrayOfFloat[0] = -1.0F;
      arrayOfFloat[1] = -1.0F;
    }
    paramFloat1 = formatFloat(paramFloat1, null);
    paramFloat2 = formatFloat(paramFloat2, null);
    paramFloat3 = formatFloat(paramFloat3, null);
    paramFloat4 = formatFloat(paramFloat4, null);
    Location.distanceBetween(paramFloat1, paramFloat2, paramFloat3, paramFloat4, arrayOfFloat);
    if (arrayOfFloat[1] < 0.0F) {
      arrayOfFloat[1] += 360.0F;
    }
    return arrayOfFloat;
  }
  
  public static float[] calculateDistanceAndBearing(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
  {
    float[] arrayOfFloat = new float[2];
    if (((paramFloat1 == 0.0F) && (paramFloat2 == 0.0F)) || ((paramFloat4 == 0.0F) && (paramFloat5 == 0.0F)))
    {
      Log.d("Utilities", "can't get plane or controller's location:" + paramFloat1 + "," + paramFloat2 + "..." + paramFloat4 + "," + paramFloat5);
      arrayOfFloat[0] = -1.0F;
      arrayOfFloat[1] = -1.0F;
    }
    paramFloat1 = formatFloat(paramFloat1, null);
    paramFloat2 = formatFloat(paramFloat2, null);
    paramFloat3 = formatFloat(paramFloat3, null);
    paramFloat4 = formatFloat(paramFloat4, null);
    paramFloat5 = formatFloat(paramFloat5, null);
    paramFloat6 = formatFloat(paramFloat6, null);
    Location.distanceBetween(paramFloat1, paramFloat2, paramFloat4, paramFloat5, arrayOfFloat);
    arrayOfFloat[0] = ((float)Math.sqrt(Math.pow(arrayOfFloat[0], 2.0D) + Math.pow(paramFloat6 - paramFloat3, 2.0D)));
    if (arrayOfFloat[1] < 0.0F) {
      arrayOfFloat[1] += 360.0F;
    }
    return arrayOfFloat;
  }
  
  public static boolean checkDefaultMixingDataExisted(Context paramContext, int paramInt)
  {
    try
    {
      Object localObject1 = paramContext.getAssets();
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>(String.valueOf(String.valueOf(paramInt)));
      ((AssetManager)localObject1).open("/manifest.csv").close();
      localObject2 = paramContext.getAssets();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>(String.valueOf(String.valueOf(paramInt)));
      localObject1 = ((AssetManager)localObject2).open("/curves.csv");
      ((InputStream)localObject1).close();
      localObject2 = paramContext.getAssets();
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>(String.valueOf(String.valueOf(paramInt)));
      ((AssetManager)localObject2).open("/switches.csv");
      ((InputStream)localObject1).close();
      bool = true;
    }
    catch (IOException paramContext)
    {
      for (;;)
      {
        Log.i("Utilities", paramContext.getMessage());
        boolean bool = false;
      }
    }
    return bool;
  }
  
  public static void checkFirstStart(Context paramContext)
  {
    if (isFirstStart(paramContext))
    {
      readSDcard(paramContext);
      showProgressDialog(paramContext, null, paramContext.getResources().getText(2131296359), false, false);
      new ImportPreloadedModelsTask(null).execute(new Context[] { paramContext });
    }
  }
  
  public static void connectModelWifi(Context paramContext, long paramLong)
  {
    WifiManager localWifiManager = (WifiManager)paramContext.getSystemService("wifi");
    BindWifiManage localBindWifiManage = new BindWifiManage(localWifiManager);
    WifiConnect localWifiConnect = new WifiConnect(localWifiManager);
    if (!localBindWifiManage.getWifiStatus()) {
      localBindWifiManage.openWifi();
    }
    paramContext = getModelWifiInfoToDatabase(paramContext, paramLong);
    if (paramContext != null)
    {
      paramContext = paramContext.split(",");
      if (paramContext.length == 3)
      {
        if (paramContext[0].equals(localBindWifiManage.getSSID())) {
          break label112;
        }
        localWifiManager.disableNetwork(localBindWifiManage.getCurrentNetId());
        localWifiConnect.Connect(paramContext[0], paramContext[1], Integer.parseInt(paramContext[2]));
      }
    }
    for (;;)
    {
      return;
      label112:
      if (paramContext[0].equals(localBindWifiManage.getSSID())) {
        isWIFIConnected = true;
      }
    }
  }
  
  public static float converRateLtoCoefficient_expo1(float paramFloat)
  {
    float f2 = 0.0F;
    float f1;
    if ((paramFloat <= 100.0F) && (paramFloat >= 0.0F)) {
      f1 = 0.17F + (paramFloat - 0.0F) * 0.83F / 100.0F;
    }
    for (;;)
    {
      return f1;
      f1 = f2;
      if (paramFloat >= -100.0F)
      {
        f1 = f2;
        if (paramFloat < 0.0F) {
          f1 = 0.17F - (0.0F - paramFloat) * 0.17F / 100.0F;
        }
      }
    }
  }
  
  public static float converRateRtoCoefficient_expo1(float paramFloat)
  {
    float f2 = 0.0F;
    float f1;
    if ((paramFloat <= 100.0F) && (paramFloat >= 0.0F)) {
      f1 = 0.84F + (paramFloat - 0.0F) * 0.66F / 100.0F;
    }
    for (;;)
    {
      return f1;
      f1 = f2;
      if (paramFloat >= -100.0F)
      {
        f1 = f2;
        if (paramFloat < 0.0F) {
          f1 = 0.84F - (0.0F - paramFloat) * 1.14F / 100.0F;
        }
      }
    }
  }
  
  public static int convertCoefficientBtoOffset(double paramDouble, int paramInt)
  {
    int i = 0;
    if (paramInt == 2) {
      i = (int)((paramDouble - 0.0D) * 200.0D / 1.0D + 0.0D);
    }
    return i;
  }
  
  public static int convertCoefficientKtoRate(double paramDouble)
  {
    int j = 0;
    int i;
    if ((paramDouble >= 0.6700000166893005D) && (paramDouble <= 2.0D)) {
      i = (int)((paramDouble - 0.6700000166893005D) * 100.0D / 1.3299999237060547D + 0.0D);
    }
    for (;;)
    {
      return i;
      i = j;
      if (paramDouble >= 0.5D)
      {
        i = j;
        if (paramDouble < 0.6700000166893005D) {
          i = (int)(0.0D - (0.6700000166893005D - paramDouble) * 100.0D / 0.17000001668930054D);
        }
      }
    }
  }
  
  public static float convertCoefficientNtoExpo(float paramFloat)
  {
    float f2 = 0.0F;
    float f1;
    if ((paramFloat >= 1.0F) && (paramFloat <= 4.0F)) {
      f1 = 0.0F + (paramFloat - 1.0F) * 100.0F / 3.0F;
    }
    for (;;)
    {
      return f1;
      f1 = f2;
      if (paramFloat >= 0.3F)
      {
        f1 = f2;
        if (paramFloat < 1.0F) {
          f1 = 0.0F - (1.0F - paramFloat) * 100.0F / 0.7F;
        }
      }
    }
  }
  
  public static int convertCoefficienttoRateLeft_expo1(double paramDouble)
  {
    int j = 0;
    int i;
    if ((paramDouble >= 0.17000000178813934D) && (paramDouble <= 1.0D)) {
      i = (int)((paramDouble - 0.17000000178813934D) * 100.0D / 0.8299999833106995D + 0.0D);
    }
    for (;;)
    {
      return i;
      i = j;
      if (paramDouble >= 0.0D)
      {
        i = j;
        if (paramDouble < 0.17000000178813934D) {
          i = (int)(0.0D - (0.17000000178813934D - paramDouble) * 100.0D / 0.17000000178813934D);
        }
      }
    }
  }
  
  public static int convertCoefficienttoRateRight_expo1(double paramDouble)
  {
    int j = 0;
    int i;
    if ((paramDouble >= 0.8399999737739563D) && (paramDouble <= 1.5D)) {
      i = (int)((paramDouble - 0.8399999737739563D) * 100.0D / 0.6600000262260437D + 0.0D);
    }
    for (;;)
    {
      return i;
      i = j;
      if (paramDouble >= -0.30000001192092896D)
      {
        i = j;
        if (paramDouble < 0.8399999737739563D) {
          i = (int)(0.0D - (0.8399999737739563D - paramDouble) * 100.0D / 1.1399999856948853D);
        }
      }
    }
  }
  
  public static float convertExpoToCoefficientN(float paramFloat)
  {
    float f2 = 0.0F;
    float f1;
    if ((paramFloat <= 100.0F) && (paramFloat >= 0.0F)) {
      f1 = 1.0F + (paramFloat - 0.0F) * 3.0F / 100.0F;
    }
    for (;;)
    {
      return f1;
      f1 = f2;
      if (paramFloat >= -100.0F)
      {
        f1 = f2;
        if (paramFloat < 0.0F) {
          f1 = 1.0F - (0.0F - paramFloat) * 0.7F / 100.0F;
        }
      }
    }
  }
  
  public static float[] convertPositionToValue(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
  {
    float[] arrayOfFloat = new float[paramArrayOfInt.length];
    for (int i = 0;; i++)
    {
      if (i >= paramArrayOfInt.length) {
        return arrayOfFloat;
      }
      arrayOfFloat[i] = (paramInt2 + (paramArrayOfInt[i] - paramInt4) * (paramInt1 - paramInt2) / (paramInt3 - paramInt4));
    }
  }
  
  public static double convertRateToCoefficientK(float paramFloat1, float paramFloat2)
  {
    return 0.0D;
  }
  
  public static float convertRateToCoefficientK(float paramFloat)
  {
    float f2 = 0.0F;
    float f1;
    if ((paramFloat <= 100.0F) && (paramFloat >= 0.0F)) {
      f1 = 0.67F + (paramFloat - 0.0F) * 1.3299999F / 100.0F;
    }
    for (;;)
    {
      return f1;
      f1 = f2;
      if (paramFloat >= -100.0F)
      {
        f1 = f2;
        if (paramFloat < 0.0F) {
          f1 = 0.67F - (0.0F - paramFloat) * 0.17000002F / 100.0F;
        }
      }
    }
  }
  
  public static Point[] countAllPointOfExpo1(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    Point[] arrayOfPoint = new Point[paramInt3 + 1];
    int i = 0;
    if (i >= arrayOfPoint.length) {
      return arrayOfPoint;
    }
    float f1 = i * 1 / paramInt3;
    arrayOfPoint[i] = new Point();
    arrayOfPoint[i].x = (paramInt1 + i);
    float f2 = (float)(paramFloat1 * Math.pow(f1, paramFloat2) + paramFloat3);
    if (f2 < 0.0F) {
      f1 = 0.0F;
    }
    for (;;)
    {
      arrayOfPoint[i].y = (paramInt2 + paramInt4 - Math.round(paramInt4 * f1));
      i++;
      break;
      f1 = f2;
      if (f2 > 1.0F) {
        f1 = 1.0F;
      }
    }
  }
  
  public static Point[] countAllPointOfExpo2(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
  {
    Point[] arrayOfPoint = new Point[paramInt3 + 1];
    int i;
    int j;
    if (arrayOfPoint.length % 2 != 0)
    {
      i = paramInt3 / 2;
      j = 0;
    }
    for (;;)
    {
      if (j > paramInt3 / 2)
      {
        return arrayOfPoint;
        i = paramInt3 / 2 + 1;
        break;
      }
      float f1 = j * 2.0F / paramInt3;
      float f2 = j * 2.0F / paramInt3;
      arrayOfPoint[j] = new Point();
      arrayOfPoint[i] = new Point();
      arrayOfPoint[j].x = (paramInt1 + j);
      arrayOfPoint[i].x = (paramInt1 + i);
      f1 = (float)(paramFloat1 * Math.pow(1.0F - f1, paramFloat3) - paramFloat5);
      f2 = (float)(paramFloat2 * Math.pow(f2, paramFloat4) + paramFloat5);
      arrayOfPoint[i].y = (paramInt4 / 2 + paramInt2 - Math.round(paramInt4 * f2 / 2.0F));
      arrayOfPoint[j].y = (paramInt4 / 2 + paramInt2 + Math.round(paramInt4 * f1 / 2.0F));
      if (arrayOfPoint[j].y > paramInt2 + paramInt4) {
        arrayOfPoint[j].y = (paramInt2 + paramInt4);
      }
      if (arrayOfPoint[i].y < paramInt2) {
        arrayOfPoint[i].y = paramInt2;
      }
      j++;
      i++;
    }
  }
  
  public static void dismissProgressDialog()
  {
    if (sProgressDialog != null)
    {
      sProgressDialog.cancel();
      sProgressDialog = null;
    }
  }
  
  public static boolean ensureAwaitState(UARTController paramUARTController)
  {
    return paramUARTController.correctTxState(1, 3);
  }
  
  public static boolean ensureBindState(UARTController paramUARTController)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (!paramUARTController.enterBind(true))
    {
      bool1 = bool2;
      if (!paramUARTController.correctTxState(2, 3)) {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  public static boolean ensureCalibrationState(UARTController paramUARTController)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (!paramUARTController.startCalibration(true))
    {
      bool1 = bool2;
      if (!paramUARTController.correctTxState(3, 3)) {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  public static boolean ensureRunState(UARTController paramUARTController)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (!paramUARTController.enterRun(true))
    {
      bool1 = bool2;
      if (!paramUARTController.correctTxState(5, 3)) {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  public static boolean ensureSimState(UARTController paramUARTController)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (!paramUARTController.enterSim(true))
    {
      bool1 = bool2;
      if (!paramUARTController.correctTxState(6, 3)) {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  public static float formatFloat(float paramFloat, String paramString)
  {
    String str;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.length() != 0) {}
    }
    else
    {
      str = "0.00000";
    }
    try
    {
      paramString = new java/text/DecimalFormat;
      paramString.<init>(str);
      float f = Float.valueOf(paramString.format(paramFloat)).floatValue();
      paramFloat = f;
    }
    catch (NumberFormatException paramString)
    {
      for (;;) {}
    }
    return paramFloat;
  }
  
  public static Integer[] get17PointsValueForFightControl(float paramFloat1, float paramFloat2, int[] paramArrayOfInt)
  {
    Integer[] arrayOfInteger = new Integer[17];
    int j = 0;
    int i = 0;
    arrayOfInteger[0] = Integer.valueOf((int)((paramArrayOfInt[0] + 0) * (paramFloat1 - paramFloat2) / 4095.0F) * 10);
    for (;;)
    {
      if ((j >= paramArrayOfInt.length) || (i >= 16)) {
        return arrayOfInteger;
      }
      j += paramArrayOfInt.length / 16;
      i++;
      arrayOfInteger[i] = Integer.valueOf((int)((paramArrayOfInt[j] + 0) * (paramFloat1 - paramFloat2) / 4095.0F * 10.0F));
    }
  }
  
  public static Integer[] get17PointsValueForFightControl(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
  {
    Integer[] arrayOfInteger = new Integer[17];
    for (paramInt1 = 0;; paramInt1++)
    {
      if (paramInt1 >= paramArrayOfFloat.length - 1)
      {
        arrayOfInteger[16] = Integer.valueOf((int)(paramArrayOfFloat[8] * 10.0F - paramInt2 * 10));
        return arrayOfInteger;
      }
      arrayOfInteger[(paramInt1 * 2)] = Integer.valueOf((int)(paramArrayOfFloat[paramInt1] * 10.0F) - paramInt2 * 10);
      arrayOfInteger[(paramInt1 * 2 + 1)] = Integer.valueOf((int)((paramArrayOfFloat[(paramInt1 + 1)] + paramArrayOfFloat[paramInt1]) / 2.0F * 10.0F) - paramInt2 * 10);
    }
  }
  
  public static float getChannelValue(UARTInfoMessage.Channel paramChannel, int paramInt)
  {
    float f2 = 0.0F;
    float f1 = f2;
    if (paramInt != HW_J_BASE)
    {
      f1 = f2;
      if (paramInt != HW_K_BASE)
      {
        f1 = f2;
        if (paramInt != HW_T_BASE)
        {
          f1 = f2;
          if (paramInt != HW_S_BASE)
          {
            f1 = f2;
            if (paramInt != HW_B_BASE)
            {
              f1 = f2;
              if (paramInt != HW_VS_BASE) {
                if (paramInt != HW_VB_BASE) {
                  break label67;
                }
              }
            }
          }
        }
      }
    }
    label67:
    for (f1 = f2;; f1 = ((Float)paramChannel.channels.get(HW_CHANNEL_INDEX.get(paramInt))).floatValue()) {
      return f1;
    }
  }
  
  public static int getCurrentMode()
  {
    return sCurrentMode;
  }
  
  public static String getCurrentRxAddrFromDB(Context paramContext, long paramLong)
  {
    Object localObject = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
    localObject = paramContext.getContentResolver().query((Uri)localObject, new String[] { "_id", "rx" }, null, null, null);
    if (DataProviderHelper.isCursorValid((Cursor)localObject))
    {
      paramContext = ((Cursor)localObject).getString(((Cursor)localObject).getColumnIndex("rx"));
      ((Cursor)localObject).close();
    }
    for (;;)
    {
      return paramContext;
      paramContext = null;
    }
  }
  
  public static int getCurrentRxTypeFromDB(Context paramContext, long paramLong)
  {
    Uri localUri = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
    paramContext = paramContext.getContentResolver().query(localUri, new String[] { "_id", "rx_type" }, null, null, null);
    int i;
    if (DataProviderHelper.isCursorValid(paramContext))
    {
      i = paramContext.getInt(paramContext.getColumnIndex("rx_type"));
      paramContext.close();
    }
    for (;;)
    {
      return i;
      i = -1;
    }
  }
  
  public static String getDisplayLengthUnit(Context paramContext)
  {
    if (getUnit(paramContext) == 1) {}
    for (paramContext = UNIT_METER;; paramContext = UNIT_FEET) {
      return paramContext;
    }
  }
  
  public static String getDisplayVelocityUnit(Context paramContext)
  {
    if (getUnit(paramContext) == 1) {}
    for (paramContext = UNIT_KELOMETER_PER_HOUR;; paramContext = UNIT_MILE_PER_HOUR) {
      return paramContext;
    }
  }
  
  public static String getExterPath()
  {
    return "/storage/sdcard1";
  }
  
  public static boolean getFmodeChannelValues(Context paramContext)
  {
    boolean bool = false;
    int i = paramContext.getSharedPreferences("flight_setting_value", 0).getInt("fmode_option", 1);
    if (i == 0)
    {
      paramContext = paramContext.getResources().getStringArray(2131361804);
      if (paramContext.length == 3) {
        break label100;
      }
      Log.e("Utilities", "Invalid fmode option! length is:" + paramContext.length);
    }
    for (;;)
    {
      return bool;
      if (i == 1)
      {
        paramContext = paramContext.getResources().getStringArray(2131361805);
        break;
      }
      paramContext = paramContext.getResources().getStringArray(2131361806);
      break;
      label100:
      for (i = 0;; i++)
      {
        if (i >= paramContext.length)
        {
          bool = true;
          break;
        }
        String[] arrayOfString = paramContext[i].split(",");
        Log.d("Utilities", "getFmodeChannelValues---fmode_values:" + paramContext[i]);
        Log.d("Utilities", "getFmodeChannelValues---temp[0]:" + arrayOfString[0] + ";temp[1]:" + arrayOfString[1]);
        if (arrayOfString.length != 2) {
          break label219;
        }
        value_ch5[i] = Integer.parseInt(arrayOfString[0]);
        value_ch6[i] = Integer.parseInt(arrayOfString[1]);
      }
      label219:
      Log.e("Utilities", "fmode values format error");
    }
  }
  
  public static int getFmodeState()
  {
    return 0;
  }
  
  public static int getHardwareIndexT(String paramString)
  {
    int i = -1;
    if (paramString != null) {
      i = HARDWARE_ARRAYLIST.indexOf(paramString);
    }
    for (;;)
    {
      return i;
      Log.e("Utilities", "Invalid hardware:" + paramString);
    }
  }
  
  public static int getHardwareType(String paramString)
  {
    int j = 0;
    int k = getHardwareIndexT(paramString);
    int i;
    if ((k > 0) && (k <= 20)) {
      i = 1;
    }
    for (;;)
    {
      return i;
      i = j;
      if (k > 20)
      {
        i = j;
        if (k <= 40) {
          i = 2;
        }
      }
    }
  }
  
  public static String getHardwareValue(int paramInt)
  {
    String str = null;
    if (paramInt < HARDWARE_ARRAYLIST.size()) {
      str = (String)HARDWARE_ARRAYLIST.get(paramInt);
    }
    return str;
  }
  
  public static String getModelWifiInfoToDatabase(Context paramContext, long paramLong)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramLong != -2L)
    {
      localObject1 = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
      paramContext = paramContext.getContentResolver().query((Uri)localObject1, new String[] { "_id", "last_connected_da58_wifi" }, null, null, null);
      if (DataProviderHelper.isCursorValid(paramContext)) {
        break label71;
      }
      Log.e("Utilities", "Read curve params of analog, Cursor is invalid");
      localObject1 = localObject2;
    }
    for (;;)
    {
      return (String)localObject1;
      label71:
      localObject1 = paramContext.getString(paramContext.getColumnIndex("last_connected_da58_wifi"));
      paramContext.close();
    }
  }
  
  public static ReceiverInfomation getReceiverInfo(Context paramContext, long paramLong)
  {
    ReceiverInfomation localReceiverInfomation = new ReceiverInfomation();
    paramContext = paramContext.getContentResolver().query(ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong), new String[] { "_id", "rx", "rx_analog_num", "rx_analog_bit", "rx_switch_num", "rx_switch_bit", "analog_min", "switch_min" }, null, null, null);
    if (!DataProviderHelper.isCursorValid(paramContext)) {
      Log.e("Utilities", "Read switch curve params, Cursor is invalid");
    }
    for (paramContext = null;; paramContext = localReceiverInfomation)
    {
      return paramContext;
      localReceiverInfomation.receiver = paramContext.getInt(paramContext.getColumnIndex("rx"));
      localReceiverInfomation.analogChNumber = paramContext.getInt(paramContext.getColumnIndex("rx_analog_num"));
      localReceiverInfomation.analogChBit = paramContext.getInt(paramContext.getColumnIndex("rx_analog_bit"));
      localReceiverInfomation.switchChNumber = paramContext.getInt(paramContext.getColumnIndex("rx_switch_num"));
      localReceiverInfomation.switchChBit = paramContext.getInt(paramContext.getColumnIndex("rx_switch_bit"));
      localReceiverInfomation.channelNumber = (localReceiverInfomation.analogChNumber + localReceiverInfomation.switchChNumber);
      localReceiverInfomation.analogChNumber_min = paramContext.getInt(paramContext.getColumnIndex("analog_min"));
      localReceiverInfomation.switchChNumber_min = paramContext.getInt(paramContext.getColumnIndex("switch_min"));
      paramContext.close();
    }
  }
  
  public static float getRelativeDegree(float paramFloat1, float paramFloat2)
  {
    return normalizeDegree(paramFloat1 - paramFloat2);
  }
  
  public static int getUnit(Context paramContext)
  {
    return paramContext.getSharedPreferences("flight_setting_value", 0).getInt("velocity_unit", 2);
  }
  
  public static boolean hasSDCard(Context paramContext)
  {
    return paramContext.getSharedPreferences("flight_setting_value", 0).getBoolean("has_sdcard_value", false);
  }
  
  private static int importModelFromAssets(Context paramContext, String paramString)
  {
    return new ModelImporter(paramContext).importModelFromAssets(paramString);
  }
  
  private static int importPerloadedModels(Context paramContext)
  {
    i = -1;
    localObject = null;
    try
    {
      String[] arrayOfString = paramContext.getAssets().list("models");
      localObject = arrayOfString;
      Log.d("Utilities", "getLocals");
      localObject = arrayOfString;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        int j;
        localIOException.printStackTrace();
        continue;
        i = importModelFromAssets(paramContext, "models" + "/" + localObject[j]);
        j++;
      }
    }
    j = i;
    if (localObject != null)
    {
      j = 0;
      if (j >= localObject.length) {
        j = i;
      }
    }
    else
    {
      return j;
    }
  }
  
  private static ArrayList<Integer> initDRCurve(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramInt == 30)
    {
      localArrayList.add(0, Integer.valueOf(500));
      localArrayList.add(1, Integer.valueOf(721));
      localArrayList.add(2, Integer.valueOf(923));
      localArrayList.add(3, Integer.valueOf(1086));
      localArrayList.add(4, Integer.valueOf(1230));
      localArrayList.add(5, Integer.valueOf(1346));
      localArrayList.add(6, Integer.valueOf(1432));
      localArrayList.add(7, Integer.valueOf(1480));
      localArrayList.add(8, Integer.valueOf(1500));
      localArrayList.add(9, Integer.valueOf(1519));
      localArrayList.add(10, Integer.valueOf(1567));
      localArrayList.add(11, Integer.valueOf(1653));
      localArrayList.add(12, Integer.valueOf(1769));
      localArrayList.add(13, Integer.valueOf(1913));
      localArrayList.add(14, Integer.valueOf(2076));
      localArrayList.add(15, Integer.valueOf(2278));
      localArrayList.add(16, Integer.valueOf(2500));
    }
    for (;;)
    {
      return localArrayList;
      if (paramInt == 1)
      {
        localArrayList.add(0, Integer.valueOf(2500));
        localArrayList.add(1, Integer.valueOf(2500));
        localArrayList.add(2, Integer.valueOf(2500));
        localArrayList.add(3, Integer.valueOf(2500));
        localArrayList.add(4, Integer.valueOf(2500));
        localArrayList.add(5, Integer.valueOf(2500));
        localArrayList.add(6, Integer.valueOf(2500));
        localArrayList.add(7, Integer.valueOf(2500));
        localArrayList.add(8, Integer.valueOf(1500));
        localArrayList.add(9, Integer.valueOf(500));
        localArrayList.add(10, Integer.valueOf(500));
        localArrayList.add(11, Integer.valueOf(500));
        localArrayList.add(12, Integer.valueOf(500));
        localArrayList.add(13, Integer.valueOf(500));
        localArrayList.add(14, Integer.valueOf(500));
        localArrayList.add(15, Integer.valueOf(500));
        localArrayList.add(16, Integer.valueOf(500));
      }
      else
      {
        localArrayList.add(0, Integer.valueOf(500));
        localArrayList.add(1, Integer.valueOf(625));
        localArrayList.add(2, Integer.valueOf(750));
        localArrayList.add(3, Integer.valueOf(875));
        localArrayList.add(4, Integer.valueOf(1000));
        localArrayList.add(5, Integer.valueOf(1125));
        localArrayList.add(6, Integer.valueOf(1250));
        localArrayList.add(7, Integer.valueOf(1375));
        localArrayList.add(8, Integer.valueOf(1500));
        localArrayList.add(9, Integer.valueOf(1625));
        localArrayList.add(10, Integer.valueOf(1750));
        localArrayList.add(11, Integer.valueOf(1875));
        localArrayList.add(12, Integer.valueOf(2000));
        localArrayList.add(13, Integer.valueOf(2125));
        localArrayList.add(14, Integer.valueOf(2250));
        localArrayList.add(15, Integer.valueOf(2375));
        localArrayList.add(16, Integer.valueOf(2500));
      }
    }
  }
  
  private static void initHardwareChannelIndex()
  {
    HW_CHANNEL_INDEX.clear();
    HW_CHANNEL_INDEX.put(HW_J_BASE + 1, 0);
    HW_CHANNEL_INDEX.put(HW_J_BASE + 2, 1);
    HW_CHANNEL_INDEX.put(HW_J_BASE + 3, 2);
    HW_CHANNEL_INDEX.put(HW_J_BASE + 4, 3);
    HW_CHANNEL_INDEX.put(HW_K_BASE + 1, 4);
    HW_CHANNEL_INDEX.put(HW_K_BASE + 2, 5);
    if ("ST10".equals("ST12"))
    {
      HW_CHANNEL_INDEX.put(HW_K_BASE + 3, 6);
      HW_CHANNEL_INDEX.put(HW_S_BASE + 1, 7);
      HW_CHANNEL_INDEX.put(HW_S_BASE + 2, 8);
      HW_CHANNEL_INDEX.put(HW_B_BASE + 1, 9);
      HW_CHANNEL_INDEX.put(HW_B_BASE + 2, 10);
      HW_CHANNEL_INDEX.put(HW_B_BASE + 3, 11);
    }
    for (;;)
    {
      return;
      if ("ST10".equals("ST10"))
      {
        HW_CHANNEL_INDEX.put(HW_S_BASE + 1, 6);
        HW_CHANNEL_INDEX.put(HW_B_BASE + 1, 7);
        HW_CHANNEL_INDEX.put(HW_B_BASE + 2, 8);
        HW_CHANNEL_INDEX.put(HW_B_BASE + 3, 9);
      }
    }
  }
  
  private static ArrayList<Integer> initThrCurve()
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(0, Integer.valueOf(250));
    localArrayList.add(1, Integer.valueOf(313));
    localArrayList.add(2, Integer.valueOf(375));
    localArrayList.add(3, Integer.valueOf(438));
    localArrayList.add(4, Integer.valueOf(500));
    localArrayList.add(5, Integer.valueOf(563));
    localArrayList.add(6, Integer.valueOf(625));
    localArrayList.add(7, Integer.valueOf(688));
    localArrayList.add(8, Integer.valueOf(750));
    localArrayList.add(9, Integer.valueOf(913));
    localArrayList.add(10, Integer.valueOf(875));
    localArrayList.add(11, Integer.valueOf(938));
    localArrayList.add(12, Integer.valueOf(1000));
    localArrayList.add(13, Integer.valueOf(1063));
    localArrayList.add(14, Integer.valueOf(1125));
    localArrayList.add(15, Integer.valueOf(1187));
    localArrayList.add(16, Integer.valueOf(1250));
    return localArrayList;
  }
  
  public static int isFPVModel(Context paramContext, long paramLong)
  {
    if (paramLong != -2L) {}
    for (int i = 1;; i = -1) {
      return i;
    }
  }
  
  private static boolean isFirstStart(Context paramContext)
  {
    paramContext = paramContext.getSharedPreferences("first_start_preference", 0);
    boolean bool = paramContext.getBoolean("is_first", true);
    if (bool) {
      paramContext.edit().putBoolean("is_first", false).commit();
    }
    return bool;
  }
  
  public static boolean isRunningMode()
  {
    return sIsRunningMode;
  }
  
  public static boolean isValidWifi(Context paramContext, long paramLong)
  {
    boolean bool2 = false;
    BindWifiManage localBindWifiManage = new BindWifiManage((WifiManager)paramContext.getSystemService("wifi"));
    paramContext = getModelWifiInfoToDatabase(paramContext, paramLong);
    boolean bool1 = bool2;
    if (paramContext != null)
    {
      paramContext = paramContext.split(",");
      bool1 = bool2;
      if (paramContext.length == 3) {
        bool1 = paramContext[0].equals(localBindWifiManage.getSSID());
      }
    }
    return bool1;
  }
  
  public static void killBackgroundApps(Context paramContext)
  {
    ActivityManager localActivityManager = (ActivityManager)paramContext.getSystemService("activity");
    paramContext = localActivityManager.getRunningAppProcesses();
    int i = 0;
    for (;;)
    {
      if (i >= paramContext.size()) {
        return;
      }
      String str = ((ActivityManager.RunningAppProcessInfo)paramContext.get(i)).processName;
      int j = 0;
      label48:
      if (j >= sKillAppsBlacklist.length)
      {
        int k = 0;
        j = 0;
        if (j < sKillAppsWhitelist.length) {
          break label178;
        }
        j = k;
        label70:
        if (j == 0)
        {
          localActivityManager.killBackgroundProcesses(str);
          Log.d("Utilities", "killed :" + str);
          if (!"com.FrozenPepper.RcPlane2".equals(str)) {}
        }
      }
      try
      {
        Thread.sleep(300L);
        i++;
        continue;
        if (str.equals(sKillAppsBlacklist[j]))
        {
          localActivityManager.killBackgroundProcesses(str);
          Log.d("Utilities", "killed :" + str);
        }
        j++;
        break label48;
        label178:
        if (str.startsWith(sKillAppsWhitelist[j]))
        {
          j = 1;
          break label70;
        }
        j++;
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
  }
  
  static ArrayList<MixedData> mControlMixMode_ST10(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 1;
    switch (paramInt)
    {
    default: 
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initThrCurve();
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 1;
      localMixedData.mhardware = (HW_B_BASE + 3);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 2;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(-50));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(-50));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 2;
      switch (paramInt)
      {
      default: 
        label272:
        localMixedData.mHardwareType = 1;
        localMixedData.mPriority = 1;
        localMixedData.mCurvePoint = initDRCurve(0);
        localMixedData.mSpeed = 10;
        localMixedData.mReverse = false;
        localArrayList.add(localMixedData);
        localMixedData = new MixedData();
        localMixedData.mFmode = 0;
        localMixedData.mChannel = 3;
        switch (paramInt)
        {
        default: 
          label356:
          localMixedData.mHardwareType = 1;
          localMixedData.mPriority = 1;
          localMixedData.mCurvePoint = initDRCurve(0);
          localMixedData.mSpeed = 10;
          localMixedData.mReverse = false;
          localArrayList.add(localMixedData);
          localMixedData = new MixedData();
          localMixedData.mFmode = 0;
          localMixedData.mChannel = 4;
          switch (paramInt)
          {
          }
          break;
        }
        break;
      }
      break;
    }
    for (;;)
    {
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(30);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 5;
      localMixedData.mhardware = (HW_S_BASE + 1);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(value_ch5[0]));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(value_ch5[1]));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(value_ch5[2]));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 6;
      localMixedData.mhardware = (HW_S_BASE + 1);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(value_ch6[0]));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(value_ch6[1]));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(value_ch6[2]));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 7;
      localMixedData.mhardware = (HW_K_BASE + 1);
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 8;
      localMixedData.mhardware = (HW_K_BASE + 2);
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 9;
      localMixedData.mhardware = (HW_J_BASE + 0);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 10;
      localMixedData.mhardware = (HW_J_BASE + 0);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      return localArrayList;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break;
      localMixedData.mhardware = (HW_J_BASE + 4);
      break label272;
      localMixedData.mhardware = (HW_J_BASE + 2);
      break label272;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break label356;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break label356;
      localMixedData.mhardware = (HW_J_BASE + 2);
      continue;
      localMixedData.mhardware = (HW_J_BASE + 4);
    }
  }
  
  static ArrayList<MixedData> mControlMixMode_ST12(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    MixedData localMixedData = new MixedData();
    localMixedData.mFmode = 0;
    localMixedData.mChannel = 1;
    switch (paramInt)
    {
    default: 
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initThrCurve();
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 1;
      localMixedData.mhardware = (HW_B_BASE + 3);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 2;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(-50));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(-50));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 2;
      switch (paramInt)
      {
      default: 
        label272:
        localMixedData.mHardwareType = 1;
        localMixedData.mPriority = 1;
        localMixedData.mCurvePoint = initDRCurve(0);
        localMixedData.mSpeed = 10;
        localMixedData.mReverse = false;
        localArrayList.add(localMixedData);
        localMixedData = new MixedData();
        localMixedData.mFmode = 0;
        localMixedData.mChannel = 3;
        switch (paramInt)
        {
        default: 
          label356:
          localMixedData.mHardwareType = 1;
          localMixedData.mPriority = 1;
          localMixedData.mCurvePoint = initDRCurve(0);
          localMixedData.mSpeed = 10;
          localMixedData.mReverse = false;
          localArrayList.add(localMixedData);
          localMixedData = new MixedData();
          localMixedData.mFmode = 0;
          localMixedData.mChannel = 4;
          switch (paramInt)
          {
          }
          break;
        }
        break;
      }
      break;
    }
    for (;;)
    {
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(30);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 5;
      localMixedData.mhardware = (HW_S_BASE + 2);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(value_ch5[0]));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(value_ch5[1]));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(value_ch5[2]));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 6;
      localMixedData.mhardware = (HW_S_BASE + 2);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(2, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(value_ch6[0]));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(value_ch6[1]));
      localMixedData.mSwitchValue.add(2, Integer.valueOf(value_ch6[2]));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 7;
      localMixedData.mhardware = (HW_K_BASE + 1);
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 8;
      localMixedData.mhardware = (HW_K_BASE + 3);
      localMixedData.mHardwareType = 1;
      localMixedData.mPriority = 1;
      localMixedData.mCurvePoint = initDRCurve(0);
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = true;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 9;
      localMixedData.mhardware = (HW_VB_BASE + 9);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(10));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(10));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 10;
      localMixedData.mhardware = HW_J_BASE;
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(false));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(0));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(0));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      localMixedData = new MixedData();
      localMixedData.mFmode = 0;
      localMixedData.mChannel = 11;
      localMixedData.mhardware = (HW_S_BASE + 1);
      localMixedData.mHardwareType = 2;
      localMixedData.mPriority = 1;
      localMixedData.mMixedType = 3;
      localMixedData.mSwitchStatus.add(0, Boolean.valueOf(true));
      localMixedData.mSwitchStatus.add(1, Boolean.valueOf(true));
      localMixedData.mSwitchValue.add(0, Integer.valueOf(-100));
      localMixedData.mSwitchValue.add(1, Integer.valueOf(100));
      localMixedData.mSpeed = 10;
      localMixedData.mReverse = false;
      localArrayList.add(localMixedData);
      return localArrayList;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break;
      localMixedData.mhardware = (HW_J_BASE + 4);
      break label272;
      localMixedData.mhardware = (HW_J_BASE + 2);
      break label272;
      localMixedData.mhardware = (HW_J_BASE + 1);
      break label356;
      localMixedData.mhardware = (HW_J_BASE + 3);
      break label356;
      localMixedData.mhardware = (HW_J_BASE + 2);
      continue;
      localMixedData.mhardware = (HW_J_BASE + 4);
    }
  }
  
  public static float normalizeDegree(float paramFloat)
  {
    return (720.0F + paramFloat) % 360.0F;
  }
  
  public static void readSDcard(Context paramContext)
  {
    paramContext = paramContext.getSharedPreferences("flight_setting_value", 0);
    if (new File("/storage/sdcard1").canWrite()) {}
    for (boolean bool = true;; bool = false)
    {
      Log.v("Utilities", "-----has SDcard----" + bool);
      paramContext.edit().putBoolean("has_sdcard_value", bool).commit();
      return;
    }
  }
  
  public static void resolvePointsArray(String[] paramArrayOfString, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    if (paramArrayOfString != null)
    {
      if (paramArrayOfString.length <= 9) {
        break label21;
      }
      Log.e("Utilities", "Error format of points");
    }
    for (;;)
    {
      return;
      label21:
      int i = 0;
      while (i < paramArrayOfString.length)
      {
        String[] arrayOfString;
        if (paramArrayOfString[i] != null)
        {
          arrayOfString = paramArrayOfString[i].split("&");
          if (arrayOfString.length != 2)
          {
            Log.e("Utilities", "pointArray's format is wrong");
            break;
          }
          if (paramArrayOfFloat1 == null) {}
        }
        try
        {
          paramArrayOfFloat1[i] = Float.parseFloat(arrayOfString[0]);
          if (paramArrayOfFloat2 != null) {
            paramArrayOfFloat2[i] = Float.parseFloat(arrayOfString[1]);
          }
          i++;
        }
        catch (NumberFormatException paramArrayOfString)
        {
          Log.e("Utilities", "NumberFormatException:" + paramArrayOfString.getMessage());
        }
      }
    }
  }
  
  public static void saveRxResInfoToDatabase(Context paramContext, UARTController paramUARTController, long paramLong, int paramInt)
  {
    byte[] arrayOfByte = paramUARTController.getRxResInfo(paramInt);
    if ((arrayOfByte != null) && (paramLong != -2L))
    {
      paramUARTController = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("rx_res_info_blob", arrayOfByte);
      paramContext.getContentResolver().update(paramUARTController, localContentValues, null, null);
    }
  }
  
  public static void saveWifiInfoToDatabase(Context paramContext, long paramLong, String paramString)
  {
    if ((paramString != null) && (paramLong != -2L))
    {
      Uri localUri = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("last_connected_da58_wifi", paramString);
      paramContext.getContentResolver().update(localUri, localContentValues, null, null);
    }
  }
  
  public static boolean sendAllDataToFlightControl(Context paramContext, long paramLong, UARTController paramUARTController)
  {
    boolean bool = false;
    if (paramUARTController == null) {
      Log.e("Utilities", "UartController is null");
    }
    for (;;)
    {
      return bool;
      ReceiverInfomation localReceiverInfomation = getReceiverInfo(paramContext, paramLong);
      if (localReceiverInfomation.channelNumber != 0) {
        paramUARTController.setChannelConfig(false, localReceiverInfomation.analogChNumber, localReceiverInfomation.switchChNumber);
      }
      try
      {
        Thread.sleep(5L);
        syncMixingDataDeleteAll(paramUARTController);
        if (("ST10".equals("ST10")) || ("ST10".equals("ST12")))
        {
          setIndexNumFunc();
          sendDataEachChannelToFlightControl(paramContext, paramUARTController);
          bool = sendFmodeKey(paramContext, paramLong, paramUARTController);
          if (!bool) {
            Log.e("Utilities", "Failed to send F-Mode key to transmitter");
          }
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        try
        {
          for (;;)
          {
            Thread.sleep(50L);
            mSendDataCompleted = true;
            break;
            sendDataEachChannelToFlightControl(paramContext, paramLong, paramUARTController, 0);
          }
          localInterruptedException = localInterruptedException;
        }
        catch (InterruptedException paramContext)
        {
          for (;;) {}
        }
      }
    }
  }
  
  public static boolean sendDataEachChannelToFlightControl(Context paramContext, long paramLong, UARTController paramUARTController, int paramInt)
  {
    boolean bool = true;
    ChannelMap[] arrayOfChannelMap = DataProviderHelper.readChannelMapFromDatabase(paramContext, paramLong);
    ThrottleData localThrottleData = DataProviderHelper.readThrDataFromDatabase(paramContext, paramLong, paramInt);
    DRData[] arrayOfDRData = DataProviderHelper.readDRDataFromDatabase(paramContext, paramLong, paramInt);
    ServoData[] arrayOfServoData = DataProviderHelper.readServoDataFromDatabase(paramContext, paramLong, paramInt);
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i >= 12)
    {
      localArrayList.remove(0);
      localArrayList.add(0, ThrottleCurveFragment.getThrottleData(paramContext, paramLong, localThrottleData));
      if (!localThrottleData.cut_sw.equals("INH")) {
        localArrayList.add(ThrottleCurveFragment.getThrCutData(localThrottleData));
      }
      paramContext = DR_Fragment.getDRDatas(paramContext, paramLong, arrayOfDRData);
      paramInt = 0;
      label111:
      if (paramInt < paramContext.length) {
        break label252;
      }
    }
    for (paramInt = 0;; paramInt++)
    {
      if (paramInt >= localArrayList.size())
      {
        return bool;
        MixedData localMixedData = new MixedData();
        localMixedData.mFmode = paramInt;
        localMixedData.mChannel = (i + 1);
        localMixedData.mhardware = getHardwareIndexT(arrayOfChannelMap[i].hardware);
        localMixedData.mHardwareType = getHardwareType(arrayOfChannelMap[i].hardware);
        localMixedData.mPriority = 1;
        ServoData localServoData = ServoSetupFragment.getServoSetup(arrayOfServoData, arrayOfChannelMap[i].function);
        if (localServoData != null)
        {
          localMixedData.mSpeed = localServoData.speed;
          localMixedData.mReverse = localServoData.reverse;
        }
        localArrayList.add(i, localMixedData);
        i++;
        break;
        label252:
        localArrayList.remove(paramContext[paramInt].mChannel - 1);
        localArrayList.add(paramContext[paramInt].mChannel - 1, paramContext[paramInt]);
        paramInt++;
        break label111;
      }
      if (!paramUARTController.syncMixingData(true, (MixedData)localArrayList.get(paramInt), 0)) {
        bool = false;
      }
    }
  }
  
  public static boolean sendDataEachChannelToFlightControl(Context paramContext, UARTController paramUARTController)
  {
    boolean bool = true;
    ArrayList localArrayList = new ArrayList();
    mPrefsUtil = paramContext.getSharedPreferences("flight_setting_value", 0);
    int i = mPrefsUtil.getInt("mode_select_value", 2);
    long l = mPrefsUtil.getLong("current_model_type", -2L);
    Log.e("Utilities", "####model_type = " + l + "####");
    getFmodeChannelValues(paramContext);
    if (l == 404L) {
      paramContext = H920MixMode(i);
    }
    for (i = 0;; i++)
    {
      if (i >= paramContext.size())
      {
        return bool;
        if (l == 406L)
        {
          paramContext = GimbalMixMode(i);
          break;
        }
        if ("ST10".equals("ST12"))
        {
          paramContext = mControlMixMode_ST12(i);
          break;
        }
        paramContext = localArrayList;
        if (!"ST10".equals("ST10")) {
          break;
        }
        paramContext = mControlMixMode_ST10(i);
        break;
      }
      if (!paramUARTController.syncMixingData(true, (MixedData)paramContext.get(i), 0)) {
        bool = false;
      }
    }
  }
  
  public static boolean sendDataToFlightControl(Context paramContext, UARTController paramUARTController, int paramInt1, int paramInt2, String paramString, long paramLong, int paramInt3)
  {
    return false;
  }
  
  public static boolean sendFmodeKey(Context paramContext, long paramLong, UARTController paramUARTController)
  {
    boolean bool = paramUARTController.setFmodeKey(true, DataProviderHelper.getFmodeKeyFromDatabase(paramContext, paramLong), 3);
    if (!bool) {
      Log.e("Utilities", "Failed to send F-Mode key to FCS");
    }
    return bool;
  }
  
  public static boolean sendRxResInfoFromDatabase(Context paramContext, UARTController paramUARTController, long paramLong)
  {
    boolean bool = false;
    Object localObject1 = null;
    Object localObject2 = paramContext.getContentResolver();
    paramContext = (Context)localObject1;
    if (paramLong != -2L)
    {
      localObject2 = ((ContentResolver)localObject2).query(ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong), new String[] { "_id", "rx_res_info_blob" }, null, null, null);
      paramContext = (Context)localObject1;
      if (DataProviderHelper.isCursorValid((Cursor)localObject2))
      {
        paramContext = ((Cursor)localObject2).getBlob(((Cursor)localObject2).getColumnIndex("rx_res_info_blob"));
        ((Cursor)localObject2).close();
      }
    }
    if (paramContext != null) {
      bool = paramUARTController.sendRxResInfo(paramContext);
    }
    return bool;
  }
  
  public static void setCurrentMode(Activity paramActivity, int paramInt)
  {
    sCurrentMode = 1;
  }
  
  public static void setImageThumbnail(Context paramContext, ImageView paramImageView, Uri paramUri)
  {
    long l2;
    if ("content".equals(paramUri.getScheme())) {
      l2 = -1L;
    }
    for (;;)
    {
      try
      {
        l1 = Long.valueOf(paramUri.getLastPathSegment()).longValue();
        if (l1 != -1L)
        {
          paramContext = MediaStore.Images.Thumbnails.getThumbnail(paramContext.getContentResolver(), l1, 3, null);
          if (paramContext != null)
          {
            paramImageView.setImageBitmap(paramContext);
            return;
          }
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        Log.e("Utilities", "Fail to parse content uri:" + paramUri);
        long l1 = l2;
        continue;
        Log.e("Utilities", "Fail to get Thumbnail");
        continue;
      }
      if ("file".equals(paramUri.getScheme()))
      {
        Log.w("Utilities", "encounter file scheme,should not happen! uri:" + paramUri);
        setImageUri(paramContext, paramImageView, paramUri);
      }
      else
      {
        setImageUri(paramContext, paramImageView, paramUri);
      }
    }
  }
  
  