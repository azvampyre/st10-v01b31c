package com.yuneec.flightmode15;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.GpsStatus.NmeaListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.yuneec.uartcontroller.GPSUpLinkData;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class GPSUpdater
  implements LocationListener, GpsStatus.Listener, GpsStatus.NmeaListener
{
  public static final int EVENT_GPS_AVAILABLE = 104;
  public static final int EVENT_GPS_DISABLED = 103;
  public static final int EVENT_GPS_ENABLED = 102;
  public static final int EVENT_GPS_OUT_OF_SERVICE = 105;
  public static final int EVENT_GPS_TEMPORARILY_UNAVAILABLE = 106;
  public static final int EVENT_LOCATION_UPDATED = 100;
  public static final int EVENT_STAELLITES_NUM_UPDATED = 101;
  private static final int UPDATE_INTERVAL = 200;
  private GPSUpLinkData mGPSData = new GPSUpLinkData();
  private Handler mHandler;
  private LocationManager mLM;
  private Timer mUpdateTimer;
  private UpdateTimerTask mUpdateTimerTask;
  
  public GPSUpdater(Context paramContext)
  {
    this.mLM = ((LocationManager)paramContext.getSystemService("location"));
  }
  
  private boolean checkValidGpsData()
  {
    if ((this.mGPSData != null) && (!this.mGPSData.reset) && (this.mGPSData.no_satelites > 0) && (this.mGPSData.accuracy > 0.0F) && ((this.mGPSData.lon != 0.0F) || (this.mGPSData.lat != 0.0F))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private boolean checksumRMC(String paramString)
  {
    return true;
  }
  
  private void registerHandler(Handler paramHandler)
  {
    this.mHandler = paramHandler;
  }
  
  private void unregisterHandler()
  {
    this.mHandler = null;
  }
  
  public GPSUpLinkData getCurrentLocation()
  {
    return this.mGPSData;
  }
  
  public void onGpsStatusChanged(int paramInt)
  {
    switch (paramInt)
    {
    default: 
    case 1: 
    case 2: 
    case 3: 
      for (;;)
      {
        return;
        if (this.mGPSData != null)
        {
          this.mGPSData.reset();
          continue;
          if (this.mGPSData != null)
          {
            this.mGPSData.reset();
            continue;
            if (this.mGPSData != null) {
              this.mGPSData.reset = false;
            }
          }
        }
      }
    }
    Object localObject = this.mLM.getGpsStatus(null);
    paramInt = -1;
    if (localObject != null)
    {
      paramInt = 0;
      localObject = ((GpsStatus)localObject).getSatellites().iterator();
    }
    for (;;)
    {
      if (!((Iterator)localObject).hasNext())
      {
        if (this.mHandler != null)
        {
          localObject = Message.obtain();
          ((Message)localObject).what = 101;
          ((Message)localObject).arg1 = paramInt;
          this.mHandler.sendMessage((Message)localObject);
        }
        if ((paramInt < 0) || (this.mGPSData == null)) {
          break;
        }
        this.mGPSData.no_satelites = paramInt;
        break;
      }
      if (((GpsSatellite)((Iterator)localObject).next()).usedInFix()) {
        paramInt++;
      }
    }
  }
  
  public void onLocationChanged(Location paramLocation)
  {
    if (paramLocation == null) {
      this.mGPSData.reset();
    }
    for (;;)
    {
      return;
      this.mGPSData.setData(paramLocation);
    }
  }
  
  public void onNmeaReceived(long paramLong, String paramString)
  {
    if ((paramString != null) && (paramString.startsWith("$GPRMC")) && (checksumRMC(paramString)))
    {
      paramString = paramString.split(",");
      if (paramString.length < 12) {}
    }
    for (;;)
    {
      try
      {
        f = Utilities.normalizeDegree(Float.parseFloat(paramString[8]));
        if ((f >= 0.0F) && (f < 180.0F))
        {
          this.mGPSData.angle = f;
          return;
        }
      }
      catch (NumberFormatException paramString)
      {
        float f;
        this.mGPSData.angle = 0.0F;
        continue;
      }
      this.mGPSData.angle = (f - 360.0F);
    }
  }
  
  public void onProviderDisabled(String paramString)
  {
    if (this.mHandler != null) {
      this.mHandler.sendEmptyMessage(103);
    }
  }
  
  public void onProviderEnabled(String paramString)
  {
    if (this.mHandler != null) {
      this.mHandler.sendEmptyMessage(102);
    }
  }
  
  public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
  {
    if (this.mHandler == null) {}
    for (;;)
    {
      return;
      switch (paramInt)
      {
      default: 
        break;
      case 0: 
        this.mHandler.sendEmptyMessage(105);
        break;
      case 2: 
        this.mHandler.sendEmptyMessage(104);
        break;
      case 1: 
        this.mHandler.sendEmptyMessage(104);
      }
    }
  }
  
  public void start(Handler paramHandler)
  {
    this.mGPSData.reset();
    registerHandler(paramHandler);
    this.mLM.requestLocationUpdates("gps", 200L, 0.0F, this);
    this.mLM.addGpsStatusListener(this);
    this.mLM.addNmeaListener(this);
    this.mUpdateTimer = new Timer("GPSUpdater");
    this.mUpdateTimerTask = new UpdateTimerTask(null);
    this.mUpdateTimer.scheduleAtFixedRate(this.mUpdateTimerTask, 200L, 200L);
  }
  
  public void stop()
  {
    unregisterHandler();
    this.mLM.removeGpsStatusListener(this);
    this.mLM.removeUpdates(this);
    if (this.mUpdateTimer != null)
    {
      this.mUpdateTimer.cancel();
      if (this.mUpdateTimerTask != null) {
        this.mUpdateTimerTask.cancel();
      }
      this.mUpdateTimer = null;
    }
  }
  
  private class UpdateTimerTask
    extends TimerTask
  {
    private UpdateTimerTask() {}
    
    public void run()
    {
      if ((GPSUpdater.this.mHandler != null) && (GPSUpdater.this.checkValidGpsData())) {
        Message.obtain(GPSUpdater.this.mHandler, 100, GPSUpdater.this.mGPSData).sendToTarget();
      }
    }
  }
}


