package com.yuneec.flightmode15;

import android.content.Context;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Vibrator;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.widget.CounterView;
import com.yuneec.widget.CounterView.OnTickListener;
import java.util.ArrayList;

public class TimerHelper
  implements CounterView.OnTickListener
{
  public static final int F_MODE_INDEX = 0;
  public static final String KEY_DURATION = "duration";
  public static final String KEY_STYLE = "style";
  public static final String KEY_TITTLE = "title";
  public static final String KEY_TRIGGER = "trigger";
  public static final String KEY_TRIGGER_POPUP = "trigger_popup";
  public static final String KEY_WARNING = "warning";
  private static final String TAG = "TimerHelper";
  public static final int THROTTLE_25 = 0;
  public static final int THROTTLE_50 = 1;
  public static final int THROTTLE_75 = 2;
  public static final int THROTTLE_INDEX = 1;
  public static final int WARNING_BOTH = 2;
  public static final int WARNING_RING = 0;
  public static final int WARNING_VIBRATE = 1;
  private CounterView mCounter;
  private int mDuration;
  private boolean mIsTimerStarted;
  private SoundPool mSoundPool;
  private int mStyle;
  private int mThrottleTimerStartThreshold;
  private int mTimerTrigger = -1;
  private Vibrator mVibrator;
  private int mWarningSoundId;
  private int mWarningStreamId;
  private int mWarningType = 0;
  
  public TimerHelper(CounterView paramCounterView)
  {
    if (paramCounterView == null) {
      throw new IllegalArgumentException("you must assgin a CounterView to monitor the Timer");
    }
    this.mCounter = paramCounterView;
    this.mCounter.setTickListener(this);
    this.mSoundPool = new SoundPool(2, 3, 0);
    this.mVibrator = ((Vibrator)this.mCounter.getContext().getSystemService("vibrator"));
    this.mWarningSoundId = this.mSoundPool.load(this.mCounter.getContext(), 2131165189, 1);
  }
  
  private void startWarningCounter()
  {
    this.mCounter.setStyle(1);
    this.mCounter.setDuration(36000);
    this.mCounter.start();
  }
  
  public void endFlight()
  {
    this.mIsTimerStarted = false;
    this.mCounter.stop();
    this.mSoundPool.stop(this.mWarningStreamId);
    this.mVibrator.cancel();
  }
  
  public void fillIntentForTimerSetting(Intent paramIntent)
  {
    paramIntent.putExtra("style", this.mStyle);
    paramIntent.putExtra("duration", this.mDuration);
    paramIntent.putExtra("warning", this.mWarningType);
    paramIntent.putExtra("trigger", this.mTimerTrigger);
    paramIntent.putExtra("trigger_popup", this.mThrottleTimerStartThreshold);
  }
  
  public void handleFmodeTrigger()
  {
    if ((this.mTimerTrigger == 0) && (!this.mIsTimerStarted))
    {
      this.mCounter.start();
      this.mIsTimerStarted = true;
    }
  }
  
  public void handleThrottleTrigger(UARTInfoMessage.Channel paramChannel)
  {
    if ((this.mDuration <= 0) || (this.mIsTimerStarted)) {}
    for (;;)
    {
      return;
      int i = paramChannel.channels.size();
      if ((this.mTimerTrigger == 1) && (this.mThrottleTimerStartThreshold != -1) && (i >= 1) && ((int)(((Float)paramChannel.channels.get(0)).floatValue() / 4096.0F * 100.0F) > this.mThrottleTimerStartThreshold))
      {
        this.mCounter.start();
        this.mIsTimerStarted = true;
      }
    }
  }
  
  public void onAlmostEnd()
  {
    if (this.mWarningType == 0) {
      this.mWarningStreamId = this.mSoundPool.play(this.mWarningSoundId, 1.0F, 1.0F, 0, 2, 2.0F);
    }
    for (;;)
    {
      return;
      if (this.mWarningType == 1)
      {
        this.mVibrator.vibrate(new long[] { 200L, 800L, 200L, 800L }, -1);
      }
      else if (this.mWarningType == 2)
      {
        this.mWarningStreamId = this.mSoundPool.play(this.mWarningSoundId, 1.0F, 1.0F, 0, 2, 2.0F);
        this.mVibrator.vibrate(new long[] { 200L, 800L, 200L, 800L }, -1);
      }
    }
  }
  
  public void onEnd()
  {
    if (this.mWarningType == 0) {
      this.mWarningStreamId = this.mSoundPool.play(this.mWarningSoundId, 1.0F, 1.0F, 0, -1, 1.0F);
    }
    for (;;)
    {
      startWarningCounter();
      return;
      if (this.mWarningType == 1)
      {
        this.mVibrator.vibrate(new long[] { 1000L, 2000L, 1000L }, 0);
      }
      else if (this.mWarningType == 2)
      {
        this.mWarningStreamId = this.mSoundPool.play(this.mWarningSoundId, 1.0F, 1.0F, 0, -1, 1.0F);
        this.mVibrator.vibrate(new long[] { 1000L, 2000L, 1000L }, 0);
      }
    }
  }
  
  public void onTick(int paramInt) {}
  
  public void releaseResource()
  {
    this.mSoundPool.release();
    this.mSoundPool = null;
  }
  
  public void setupDefault()
  {
    this.mDuration = 36000;
    this.mWarningType = 0;
    this.mStyle = 1;
    this.mTimerTrigger = 0;
    this.mThrottleTimerStartThreshold = -1;
    this.mCounter.resetCounterColor();
    this.mCounter.setDuration(this.mDuration);
    this.mCounter.setStyle(this.mStyle);
  }
  
  public void setupUsingTimerSettingResult(Intent paramIntent)
  {
    this.mDuration = paramIntent.getIntExtra("duration", 0);
    this.mStyle = paramIntent.getIntExtra("style", 0);
    this.mTimerTrigger = paramIntent.getIntExtra("trigger", 0);
    this.mThrottleTimerStartThreshold = paramIntent.getIntExtra("trigger_popup", -1);
    this.mWarningType = paramIntent.getIntExtra("warning", 0);
    this.mCounter.resetCounterColor();
    this.mCounter.setDuration(this.mDuration);
    this.mCounter.setStyle(this.mStyle);
  }
  
  public void startFlight()
  {
    this.mCounter.reset();
    this.mCounter.setDuration(this.mDuration);
    this.mCounter.setStyle(this.mStyle);
  }
}


