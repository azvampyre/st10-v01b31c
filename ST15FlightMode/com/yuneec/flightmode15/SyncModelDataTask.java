package com.yuneec.flightmode15;

import android.content.Context;
import android.os.AsyncTask;
import com.yuneec.uartcontroller.UARTController;

public class SyncModelDataTask
  extends AsyncTask<Long, Void, Boolean>
{
  private Context mContext;
  private UARTController mController;
  private SyncModelDataCompletedAction onCompletedAction;
  
  public SyncModelDataTask(Context paramContext, UARTController paramUARTController, SyncModelDataCompletedAction paramSyncModelDataCompletedAction)
  {
    this.mContext = paramContext;
    this.onCompletedAction = paramSyncModelDataCompletedAction;
    this.mController = paramUARTController;
  }
  
  protected Boolean doInBackground(Long... paramVarArgs)
  {
    if ((this.mContext == null) || (this.mController == null)) {}
    for (paramVarArgs = Boolean.valueOf(false);; paramVarArgs = Boolean.valueOf(true))
    {
      return paramVarArgs;
      long l = paramVarArgs[0].longValue();
      if (l != -2L)
      {
        Utilities.sendAllDataToFlightControl(this.mContext, l, this.mController);
        Utilities.sendRxResInfoFromDatabase(this.mContext, this.mController, l);
      }
    }
  }
  
  protected void onPostExecute(Boolean paramBoolean)
  {
    super.onPostExecute(paramBoolean);
    if (this.onCompletedAction != null) {
      this.onCompletedAction.SyncModelDataCompleted();
    }
  }
  
  public static abstract interface SyncModelDataCompletedAction
  {
    public abstract void SyncModelDataCompleted();
  }
}


