package com.yuneec.flightmode15;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;

public class SpeciallyChannelSender
  extends Dialog
  implements View.OnClickListener
{
  private int btnNumber;
  private Button mFollowBtn;
  private Button mNormalBtn;
  private onButtonClickListener mOnButtonClickListener;
  private Button mTrackBtn;
  
  public SpeciallyChannelSender(Context paramContext)
  {
    super(paramContext, 2131230730);
    setContentView(2130903105);
  }
  
  public void adjustHeight(int paramInt)
  {
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.height = paramInt;
    getWindow().setAttributes(localLayoutParams);
  }
  
  public void onClick(View paramView)
  {
    if (paramView.equals(this.mNormalBtn)) {
      this.btnNumber = 6;
    }
    for (;;)
    {
      this.mOnButtonClickListener.onButtonClick(this.btnNumber);
      return;
      if (paramView.equals(this.mFollowBtn)) {
        this.btnNumber = 7;
      } else if (paramView.equals(this.mTrackBtn)) {
        this.btnNumber = 8;
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mNormalBtn = ((Button)findViewById(2131689931));
    this.mNormalBtn.setOnClickListener(this);
    this.mFollowBtn = ((Button)findViewById(2131689932));
    this.mFollowBtn.setOnClickListener(this);
    this.mTrackBtn = ((Button)findViewById(2131689933));
    this.mTrackBtn.setOnClickListener(this);
  }
  
  public void setOnButtonClicked(onButtonClickListener paramonButtonClickListener)
  {
    this.mOnButtonClickListener = paramonButtonClickListener;
  }
  
  public static abstract interface onButtonClickListener
  {
    public abstract void onButtonClick(int paramInt);
  }
}


