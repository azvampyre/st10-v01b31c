package com.yuneec.flightmode15;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.BaseDialog;
import com.yuneec.widget.SwitchWidgetPreference;

public class NormalSettingsFragment
  extends PreferenceFragment
  implements Preference.OnPreferenceClickListener
{
  private static final String KEY_CALIBRATION = "calibration";
  private static final String KEY_GPS = "gps";
  private static final String TAG = "NormalSettingsFragment";
  private Preference mCalibrationPref;
  private UARTController mController = null;
  private SwitchWidgetPreference mGPSPref;
  private Handler mHandler = new Handler();
  private ProgressDialog mProgressDialog;
  private Runnable mProgressTimeOutRunnable = new Runnable()
  {
    public void run()
    {
      Toast.makeText(NormalSettingsFragment.this.getActivity(), 2131296627, 1).show();
      NormalSettingsFragment.this.dismissProgressDialog();
    }
  };
  
  private void calibrationAction()
  {
    final BaseDialog localBaseDialog = new BaseDialog(getActivity(), 2131230729);
    localBaseDialog.setContentView(2130903048);
    ((Button)localBaseDialog.findViewById(2131689518)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (NormalSettingsFragment.this.mController != null) {
          NormalSettingsFragment.this.mController.setTTBState(false, Utilities.HW_VB_BASE + 3, false);
        }
        localBaseDialog.dismiss();
      }
    });
    ((Button)localBaseDialog.findViewById(2131689519)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (NormalSettingsFragment.this.mController != null) {
          NormalSettingsFragment.this.mController.setTTBState(false, Utilities.HW_VB_BASE + 4, false);
        }
        localBaseDialog.dismiss();
      }
    });
    localBaseDialog.show();
  }
  
  private void dismissProgressDialog()
  {
    if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing()))
    {
      this.mProgressDialog.dismiss();
      this.mProgressDialog = null;
    }
    removeProgressRunnable();
  }
  
  private void removeProgressRunnable()
  {
    if (this.mProgressTimeOutRunnable != null) {
      this.mHandler.removeCallbacks(this.mProgressTimeOutRunnable);
    }
  }
  
  private void showProgressDialog()
  {
    this.mProgressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(2131296620), false, false);
    this.mProgressDialog.setCancelable(true);
    this.mHandler.postDelayed(this.mProgressTimeOutRunnable, 5000L);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    addPreferencesFromResource(2131099650);
    paramBundle = getActivity().getIntent();
    int i = paramBundle.getExtras().getInt("fmodeStatus");
    boolean bool = paramBundle.getExtras().getBoolean("gps_switch");
    this.mGPSPref = ((SwitchWidgetPreference)findPreference("gps"));
    this.mGPSPref.setWidgetStates(bool);
    this.mCalibrationPref = findPreference("calibration");
    this.mCalibrationPref.setOnPreferenceClickListener(this);
    if (16 != i)
    {
      this.mGPSPref.setEnabled(false);
      this.mCalibrationPref.setEnabled(false);
    }
    for (;;)
    {
      this.mController = UARTController.getInstance();
      return;
      this.mGPSPref.setOnPreferenceClickListener(this);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.mController != null) {
      this.mController = null;
    }
  }
  
  public void onPause()
  {
    super.onPause();
  }
  
  public boolean onPreferenceClick(Preference paramPreference)
  {
    if (paramPreference.getKey().equals("calibration")) {
      calibrationAction();
    }
    for (;;)
    {
      return true;
      if ((paramPreference.getKey().equals("gps")) && (this.mController != null))
      {
        paramPreference = this.mGPSPref;
        if (this.mGPSPref.getIsOnStates()) {}
        for (boolean bool = false;; bool = true)
        {
          paramPreference.setWidgetStates(bool);
          if (!Boolean.valueOf(this.mGPSPref.getIsOnStates()).booleanValue()) {
            break label98;
          }
          this.mController.setTTBState(false, Utilities.HW_VB_BASE + 2, false);
          break;
        }
        label98:
        this.mController.setTTBState(false, Utilities.HW_VB_BASE + 1, false);
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
  }
}


