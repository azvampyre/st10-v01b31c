package com.yuneec.flightmode15;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import com.yuneec.IPCameraManager.Amba2;
import com.yuneec.IPCameraManager.BindResponse;
import com.yuneec.IPCameraManager.BindStateResponse;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Locale;

public class ChannelDataForward
{
  private static final String MAC_ADDRESS;
  private static final int REMOTE_PORT = 49156;
  private static final int[] TABLE;
  private static int count;
  private static ChannelDataForward instance = null;
  private final String TAG = ChannelDataForward.class.getSimpleName();
  private Amba2 bindCamera;
  private ForwardCallback callback;
  private boolean isBindedCamera = false;
  private HttpRequestHandler mHandler = new HttpRequestHandler(null);
  private Messenger mHttpResponseMessenger = new Messenger(this.mHandler);
  private Handler sendHandler;
  private HandlerThread sendThread;
  private InetAddress serverAddress;
  private DatagramSocket socket;
  
  static
  {
    MAC_ADDRESS = getMacAddress();
    count = 0;
    int[] arrayOfInt = new int['Ā'];
    arrayOfInt[1] = 4489;
    arrayOfInt[2] = 8978;
    arrayOfInt[3] = 12955;
    arrayOfInt[4] = 17956;
    arrayOfInt[5] = 22445;
    arrayOfInt[6] = 25910;
    arrayOfInt[7] = 29887;
    arrayOfInt[8] = 35912;
    arrayOfInt[9] = 40385;
    arrayOfInt[10] = 44890;
    arrayOfInt[11] = 48851;
    arrayOfInt[12] = 51820;
    arrayOfInt[13] = 56293;
    arrayOfInt[14] = 59774;
    arrayOfInt[15] = 63735;
    arrayOfInt[16] = 4225;
    arrayOfInt[17] = 264;
    arrayOfInt[18] = 13203;
    arrayOfInt[19] = 8730;
    arrayOfInt[20] = 22181;
    arrayOfInt[21] = 18220;
    arrayOfInt[22] = 30135;
    arrayOfInt[23] = 25662;
    arrayOfInt[24] = 40137;
    arrayOfInt[25] = 36160;
    arrayOfInt[26] = 49115;
    arrayOfInt[27] = 44626;
    arrayOfInt[28] = 56045;
    arrayOfInt[29] = 52068;
    arrayOfInt[30] = 63999;
    arrayOfInt[31] = 59510;
    arrayOfInt[32] = 8450;
    arrayOfInt[33] = 12427;
    arrayOfInt[34] = 528;
    arrayOfInt[35] = 5017;
    arrayOfInt[36] = 26406;
    arrayOfInt[37] = 30383;
    arrayOfInt[38] = 17460;
    arrayOfInt[39] = 21949;
    arrayOfInt[40] = 44362;
    arrayOfInt[41] = 48323;
    arrayOfInt[42] = 36440;
    arrayOfInt[43] = 40913;
    arrayOfInt[44] = 60270;
    arrayOfInt[45] = 64231;
    arrayOfInt[46] = 51324;
    arrayOfInt[47] = 55797;
    arrayOfInt[48] = 12675;
    arrayOfInt[49] = 8202;
    arrayOfInt[50] = 4753;
    arrayOfInt[51] = 792;
    arrayOfInt[52] = 30631;
    arrayOfInt[53] = 26158;
    arrayOfInt[54] = 21685;
    arrayOfInt[55] = 17724;
    arrayOfInt[56] = 48587;
    arrayOfInt[57] = 44098;
    arrayOfInt[58] = 40665;
    arrayOfInt[59] = 36688;
    arrayOfInt[60] = 64495;
    arrayOfInt[61] = 60006;
    arrayOfInt[62] = 55549;
    arrayOfInt[63] = 51572;
    arrayOfInt[64] = 16900;
    arrayOfInt[65] = 21389;
    arrayOfInt[66] = 24854;
    arrayOfInt[67] = 28831;
    arrayOfInt[68] = 1056;
    arrayOfInt[69] = 5545;
    arrayOfInt[70] = 10034;
    arrayOfInt[71] = 14011;
    arrayOfInt[72] = 52812;
    arrayOfInt[73] = 57285;
    arrayOfInt[74] = 60766;
    arrayOfInt[75] = 64727;
    arrayOfInt[76] = 34920;
    arrayOfInt[77] = 39393;
    arrayOfInt[78] = 43898;
    arrayOfInt[79] = 47859;
    arrayOfInt[80] = 21125;
    arrayOfInt[81] = 17164;
    arrayOfInt[82] = 29079;
    arrayOfInt[83] = 24606;
    arrayOfInt[84] = 5281;
    arrayOfInt[85] = 1320;
    arrayOfInt[86] = 14259;
    arrayOfInt[87] = 9786;
    arrayOfInt[88] = 57037;
    arrayOfInt[89] = 53060;
    arrayOfInt[90] = 64991;
    arrayOfInt[91] = 60502;
    arrayOfInt[92] = 39145;
    arrayOfInt[93] = 35168;
    arrayOfInt[94] = 48123;
    arrayOfInt[95] = 43634;
    arrayOfInt[96] = 25350;
    arrayOfInt[97] = 29327;
    arrayOfInt[98] = 16404;
    arrayOfInt[99] = 20893;
    arrayOfInt[100] = 9506;
    arrayOfInt[101] = 13483;
    arrayOfInt[102] = 1584;
    arrayOfInt[103] = 6073;
    arrayOfInt[104] = 61262;
    arrayOfInt[105] = 65223;
    arrayOfInt[106] = 52316;
    arrayOfInt[107] = 56789;
    arrayOfInt[108] = 43370;
    arrayOfInt[109] = 47331;
    arrayOfInt[110] = 35448;
    arrayOfInt[111] = 39921;
    arrayOfInt[112] = 29575;
    arrayOfInt[113] = 25102;
    arrayOfInt[114] = 20629;
    arrayOfInt[115] = 16668;
    arrayOfInt[116] = 13731;
    arrayOfInt[117] = 9258;
    arrayOfInt[118] = 5809;
    arrayOfInt[119] = 1848;
    arrayOfInt[120] = 65487;
    arrayOfInt[121] = 60998;
    arrayOfInt[122] = 56541;
    arrayOfInt[123] = 52564;
    arrayOfInt[124] = 47595;
    arrayOfInt[125] = 43106;
    arrayOfInt[126] = 39673;
    arrayOfInt[127] = 35696;
    arrayOfInt[''] = 33800;
    arrayOfInt[''] = 38273;
    arrayOfInt[''] = 42778;
    arrayOfInt[''] = 46739;
    arrayOfInt[''] = 49708;
    arrayOfInt['
'] = 54181;
    arrayOfInt[''] = 57662;
    arrayOfInt[''] = 61623;
    arrayOfInt[''] = 2112;
    arrayOfInt[''] = 6601;
    arrayOfInt[''] = 11090;
    arrayOfInt[''] = 15067;
    arrayOfInt[''] = 20068;
    arrayOfInt[''] = 24557;
    arrayOfInt[''] = 28022;
    arrayOfInt[''] = 31999;
    arrayOfInt[''] = 38025;
    arrayOfInt[''] = 34048;
    arrayOfInt[''] = 47003;
    arrayOfInt[''] = 42514;
    arrayOfInt[''] = 53933;
    arrayOfInt[''] = 49956;
    arrayOfInt[''] = 61887;
    arrayOfInt[''] = 57398;
    arrayOfInt[''] = 6337;
    arrayOfInt[''] = 2376;
    arrayOfInt[''] = 15315;
    arrayOfInt[''] = 10842;
    arrayOfInt[''] = 24293;
    arrayOfInt[''] = 20332;
    arrayOfInt[''] = 32247;
    arrayOfInt[''] = 27774;
    arrayOfInt[' '] = 42250;
    arrayOfInt['¡'] = 46211;
    arrayOfInt['¢'] = 34328;
    arrayOfInt['£'] = 38801;
    arrayOfInt['¤'] = 58158;
    arrayOfInt['¥'] = 62119;
    arrayOfInt['¦'] = 49212;
    arrayOfInt['§'] = 53685;
    arrayOfInt['¨'] = 10562;
    arrayOfInt['©'] = 14539;
    arrayOfInt['ª'] = 2640;
    arrayOfInt['«'] = 7129;
    arrayOfInt['¬'] = 28518;
    arrayOfInt['­'] = 32495;
    arrayOfInt['®'] = 19572;
    arrayOfInt['¯'] = 24061;
    arrayOfInt['°'] = 46475;
    arrayOfInt['±'] = 41986;
    arrayOfInt['²'] = 38553;
    arrayOfInt['³'] = 34576;
    arrayOfInt['´'] = 62383;
    arrayOfInt['µ'] = 57894;
    arrayOfInt['¶'] = 53437;
    arrayOfInt['·'] = 49460;
    arrayOfInt['¸'] = 14787;
    arrayOfInt['¹'] = 10314;
    arrayOfInt['º'] = 6865;
    arrayOfInt['»'] = 2904;
    arrayOfInt['¼'] = 32743;
    arrayOfInt['½'] = 28270;
    arrayOfInt['¾'] = 23797;
    arrayOfInt['¿'] = 19836;
    arrayOfInt['À'] = 50700;
    arrayOfInt['Á'] = 55173;
    arrayOfInt['Â'] = 58654;
    arrayOfInt['Ã'] = 62615;
    arrayOfInt['Ä'] = 32808;
    arrayOfInt['Å'] = 37281;
    arrayOfInt['Æ'] = 41786;
    arrayOfInt['Ç'] = 45747;
    arrayOfInt['È'] = 19012;
    arrayOfInt['É'] = 23501;
    arrayOfInt['Ê'] = 26966;
    arrayOfInt['Ë'] = 30943;
    arrayOfInt['Ì'] = 3168;
    arrayOfInt['Í'] = 7657;
    arrayOfInt['Î'] = 12146;
    arrayOfInt['Ï'] = 16123;
    arrayOfInt['Ð'] = 54925;
    arrayOfInt['Ñ'] = 50948;
    arrayOfInt['Ò'] = 62879;
    arrayOfInt['Ó'] = 58390;
    arrayOfInt['Ô'] = 37033;
    arrayOfInt['Õ'] = 33056;
    arrayOfInt['Ö'] = 46011;
    arrayOfInt['×'] = 41522;
    arrayOfInt['Ø'] = 23237;
    arrayOfInt['Ù'] = 19276;
    arrayOfInt['Ú'] = 31191;
    arrayOfInt['Û'] = 26718;
    arrayOfInt['Ü'] = 7393;
    arrayOfInt['Ý'] = 3432;
    arrayOfInt['Þ'] = 16371;
    arrayOfInt['ß'] = 11898;
    arrayOfInt['à'] = 59150;
    arrayOfInt['á'] = 63111;
    arrayOfInt['â'] = 50204;
    arrayOfInt['ã'] = 54677;
    arrayOfInt['ä'] = 41258;
    arrayOfInt['å'] = 45219;
    arrayOfInt['æ'] = 33336;
    arrayOfInt['ç'] = 37809;
    arrayOfInt['è'] = 27462;
    arrayOfInt['é'] = 31439;
    arrayOfInt['ê'] = 18516;
    arrayOfInt['ë'] = 23005;
    arrayOfInt['ì'] = 11618;
    arrayOfInt['í'] = 15595;
    arrayOfInt['î'] = 3696;
    arrayOfInt['ï'] = 8185;
    arrayOfInt['ð'] = 63375;
    arrayOfInt['ñ'] = 58886;
    arrayOfInt['ò'] = 54429;
    arrayOfInt['ó'] = 50452;
    arrayOfInt['ô'] = 45483;
    arrayOfInt['õ'] = 40994;
    arrayOfInt['ö'] = 37561;
    arrayOfInt['÷'] = 33584;
    arrayOfInt['ø'] = 31687;
    arrayOfInt['ù'] = 27214;
    arrayOfInt['ú'] = 22741;
    arrayOfInt['û'] = 18780;
    arrayOfInt['ü'] = 15843;
    arrayOfInt['ý'] = 11370;
    arrayOfInt['þ'] = 7921;
    arrayOfInt['ÿ'] = 3960;
    TABLE = arrayOfInt;
  }
  
  public static String byteArrayToString(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = paramArrayOfByte.length;
    for (int i = 0;; i++)
    {
      if (i >= j) {
        return localStringBuilder.toString();
      }
      localStringBuilder.append(String.format(" %02x", new Object[] { Byte.valueOf(paramArrayOfByte[i]) }));
    }
  }
  
  private static int crc16(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = 65535;
    for (;;)
    {
      if (paramInt1 >= paramInt2) {
        return i;
      }
      i = i >> 8 ^ TABLE[((paramArrayOfByte[paramInt1] & 0xFF ^ i) & 0xFF)];
      paramInt1++;
    }
  }
  
  public static ChannelDataForward getInstance()
  {
    if (instance == null) {
      instance = new ChannelDataForward();
    }
    return instance;
  }
  
  private static String getMacAddress()
  {
    try
    {
      String str = loadFileAsString("/sys/class/net/wlan0/address").toLowerCase(Locale.ENGLISH).substring(0, 17);
      return str;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        Object localObject = null;
      }
    }
  }
  
  private static String loadFileAsString(String paramString)
    throws IOException
  {
    StringBuffer localStringBuffer = new StringBuffer(1000);
    BufferedReader localBufferedReader = new BufferedReader(new FileReader(paramString));
    paramString = new char['Ѐ'];
    for (;;)
    {
      int i = localBufferedReader.read(paramString);
      if (i == -1)
      {
        localBufferedReader.close();
        return localStringBuffer.toString();
      }
      localStringBuffer.append(String.valueOf(paramString, 0, i));
    }
  }
  
  private byte[] packToYMavlink(UARTInfoMessage.Channel paramChannel)
  {
    byte[] arrayOfByte = new byte[37];
    arrayOfByte[0] = -2;
    arrayOfByte[1] = 27;
    int i = count;
    count = i + 1;
    arrayOfByte[2] = ((byte)i);
    arrayOfByte[3] = 4;
    arrayOfByte[4] = 0;
    arrayOfByte[5] = 2;
    arrayOfByte[6] = 0;
    arrayOfByte[7] = 6;
    int j = 0;
    i = 0;
    if (i >= 12)
    {
      arrayOfByte[35] = 0;
      i = crc16(arrayOfByte, 1, 36);
      arrayOfByte[35] = ((byte)(i & 0xFF));
      arrayOfByte[36] = ((byte)(i >> 8 & 0xFF));
      return arrayOfByte;
    }
    float f = ((Float)paramChannel.channels.get(i)).floatValue();
    if (i % 2 == 0)
    {
      arrayOfByte[(i + 8 + i / 2)] = ((byte)((int)f >> 4 & 0xFF));
      j = (byte)((int)f & 0xF);
    }
    for (;;)
    {
      i++;
      break;
      arrayOfByte[(i + 8 + i / 2)] = ((byte)(j << 4 & 0xF0 | (int)f >> 8 & 0xF));
      arrayOfByte[(i + 8 + i / 2 + 1)] = ((byte)((int)f & 0xFF));
    }
  }
  
  private void sendToRemote(final byte[] paramArrayOfByte)
  {
    if (this.socket != null) {
      this.sendHandler.post(new Runnable()
      {
        public void run()
        {
          try
          {
            DatagramPacket localDatagramPacket = new java/net/DatagramPacket;
            localDatagramPacket.<init>(paramArrayOfByte, paramArrayOfByte.length, ChannelDataForward.this.serverAddress, 49156);
            ChannelDataForward.this.socket.send(localDatagramPacket);
            return;
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              localIOException.printStackTrace();
            }
          }
        }
      });
    }
  }
  
  private void setupSocket()
    throws SocketException, UnknownHostException
  {
    this.socket = new DatagramSocket(23602);
    this.serverAddress = InetAddress.getByName("192.168.42.1");
  }
  
  public void bind()
  {
    if (this.bindCamera != null) {
      this.bindCamera.getBindState(this.mHttpResponseMessenger);
    }
    for (;;)
    {
      return;
      if (this.callback != null) {
        this.callback.onBindResult(false);
      }
    }
  }
  
  public void exit()
  {
    unbind();
    instance = null;
  }
  
  public void forwardMixChannelData(UARTInfoMessage.Channel paramChannel)
  {
    if (this.isBindedCamera) {
      sendToRemote(packToYMavlink(paramChannel));
    }
  }
  
  public boolean isBindedCamera()
  {
    return this.isBindedCamera;
  }
  
  public void setBindCamera(Amba2 paramAmba2)
  {
    this.bindCamera = paramAmba2;
  }
  
  public void setCallback(ForwardCallback paramForwardCallback)
  {
    this.callback = paramForwardCallback;
  }
  
  public void unbind()
  {
    this.isBindedCamera = false;
    if (this.socket != null)
    {
      this.socket.close();
      this.socket = null;
      this.sendHandler.removeCallbacksAndMessages(null);
      this.sendHandler.getLooper().quit();
    }
  }
  
  public static abstract interface ForwardCallback
  {
    public abstract void onBindResult(boolean paramBoolean);
  }
  
  private class HttpRequestHandler
    extends Handler
  {
    private String serverMacAddress;
    
    private HttpRequestHandler() {}
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      }
      for (;;)
      {
        return;
        switch (paramMessage.arg1)
        {
        default: 
          break;
        case 1001: 
          paramMessage = (BindResponse)paramMessage.obj;
          if (paramMessage.bindedResult)
          {
            this.serverMacAddress = paramMessage.serverMacAddress;
            if (ChannelDataForward.this.bindCamera != null) {
              ChannelDataForward.this.bindCamera.getBindState(ChannelDataForward.this.mHttpResponseMessenger);
            }
          }
          else
          {
            ChannelDataForward.this.isBindedCamera = false;
            if (ChannelDataForward.this.callback != null) {
              ChannelDataForward.this.callback.onBindResult(false);
            }
          }
          break;
        case 1002: 
          paramMessage = (BindStateResponse)paramMessage.obj;
          if (paramMessage.isOk)
          {
            if (paramMessage.isBinded)
            {
              if (ChannelDataForward.MAC_ADDRESS.equals(paramMessage.bindedClientAddress))
              {
                try
                {
                  if (!ChannelDataForward.this.isBindedCamera)
                  {
                    ChannelDataForward.this.setupSocket();
                    Object localObject = ChannelDataForward.this;
                    paramMessage = new android/os/HandlerThread;
                    paramMessage.<init>("forward thread");
                    ((ChannelDataForward)localObject).sendThread = paramMessage;
                    ChannelDataForward.this.sendThread.start();
                    paramMessage = ChannelDataForward.this;
                    localObject = new android/os/Handler;
                    ((Handler)localObject).<init>(ChannelDataForward.this.sendThread.getLooper());
                    paramMessage.sendHandler = ((Handler)localObject);
                  }
                  ChannelDataForward.count = 0;
                  ChannelDataForward.this.isBindedCamera = true;
                  if (ChannelDataForward.this.callback == null) {
                    continue;
                  }
                  ChannelDataForward.this.callback.onBindResult(true);
                }
                catch (SocketException paramMessage)
                {
                  ChannelDataForward.this.unbind();
                  if (ChannelDataForward.this.callback != null) {
                    ChannelDataForward.this.callback.onBindResult(false);
                  }
                  paramMessage.printStackTrace();
                }
                catch (UnknownHostException paramMessage)
                {
                  ChannelDataForward.this.unbind();
                  if (ChannelDataForward.this.callback != null) {
                    ChannelDataForward.this.callback.onBindResult(false);
                  }
                  paramMessage.printStackTrace();
                }
              }
              else
              {
                ChannelDataForward.this.unbind();
                if (ChannelDataForward.this.callback != null) {
                  ChannelDataForward.this.callback.onBindResult(false);
                }
              }
            }
            else if (ChannelDataForward.this.bindCamera != null) {
              ChannelDataForward.this.bindCamera.requestBind(ChannelDataForward.this.mHttpResponseMessenger, ChannelDataForward.MAC_ADDRESS);
            }
          }
          else
          {
            ChannelDataForward.this.unbind();
            if (ChannelDataForward.this.callback != null) {
              ChannelDataForward.this.callback.onBindResult(false);
            }
          }
          break;
        }
      }
    }
  }
}


