package com.yuneec.flightmode15;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.storage.StorageManager;
import com.yuneec.IPCameraManager.DES;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;

public class FlightLog
{
  private static FlightLog instance = null;
  private static int maxNum;
  private static int reserveNum = 100;
  private String apppath = "sdcard";
  private Runnable closeFlightModeRunnable = new Runnable()
  {
    public void run()
    {
      if (!FlightLog.this.isCreatedNode) {
        FlightLog.this.flightNoteClose();
      }
    }
  };
  private Context context;
  private boolean isCreatedNode = false;
  private Handler mHandler = new Handler();
  private FileOutputStream mRemoteFos = null;
  private FileOutputStream mRemoteGpsFos = null;
  private FileOutputStream mTelemetryFos = null;
  private String remFilePath;
  private String remGPSFilePath;
  private String telFilePath;
  
  static
  {
    maxNum = 10000;
  }
  
  private void delLogFile(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt1 - paramInt2;
    if (paramInt1 < paramInt2) {
      i = paramInt1 - paramInt2 + paramInt3;
    }
    Object localObject3 = this.apppath + File.separator + "FlightLog" + File.separator + "Telemetry" + File.separator + "Telemetry_" + String.format("%05d", new Object[] { Integer.valueOf(i) }) + ".csv";
    Object localObject2 = this.apppath + File.separator + "FlightLog" + File.separator + "Remote" + File.separator + "Remote_" + String.format("%05d", new Object[] { Integer.valueOf(i) }) + ".csv";
    Object localObject1 = this.apppath + File.separator + "FlightLog" + File.separator + "RemoteGPS" + File.separator + "RemoteGPS_" + String.format("%05d", new Object[] { Integer.valueOf(i) }) + ".csv";
    localObject3 = new File((String)localObject3);
    if (((File)localObject3).exists()) {
      ((File)localObject3).delete();
    }
    localObject2 = new File((String)localObject2);
    if (((File)localObject2).exists()) {
      ((File)localObject2).delete();
    }
    localObject1 = new File((String)localObject1);
    if (((File)localObject1).exists()) {
      ((File)localObject1).delete();
    }
  }
  
  private void delOtherFile(String paramString, File paramFile)
  {
    paramFile = paramFile.list();
    for (int i = 0;; i++)
    {
      if (i >= paramFile.length) {
        return;
      }
      String str = paramFile[i];
      Object localObject = str.substring(10, 15);
      if ((!str.contains("Telemetry_")) || (!((String)localObject).matches("[0-9]+")))
      {
        localObject = new File(paramString + "/FlightLog/Telemetry/" + str);
        if (((File)localObject).exists()) {
          ((File)localObject).delete();
        }
      }
    }
  }
  
  private void encryptLog()
  {
    DES.logEncrypt(this.telFilePath);
    this.telFilePath = null;
    DES.logEncrypt(this.remFilePath);
    this.remFilePath = null;
    DES.logEncrypt(this.remGPSFilePath);
    this.remGPSFilePath = null;
  }
  
  private void flightNoteClose()
  {
    if (this.mTelemetryFos != null) {}
    try
    {
      this.mTelemetryFos.close();
      if (this.mRemoteFos == null) {}
    }
    catch (Exception localException2)
    {
      try
      {
        this.mRemoteFos.close();
        if (this.mRemoteGpsFos == null) {}
      }
      catch (Exception localException2)
      {
        try
        {
          for (;;)
          {
            this.mRemoteGpsFos.close();
            if ((this.mRemoteGpsFos != null) || (this.mRemoteFos != null) || (this.mTelemetryFos != null)) {
              encryptLog();
            }
            this.mRemoteGpsFos = null;
            this.mRemoteFos = null;
            this.mTelemetryFos = null;
            return;
            localException1 = localException1;
            localException1.printStackTrace();
          }
          localException2 = localException2;
          localException2.printStackTrace();
        }
        catch (Exception localException3)
        {
          for (;;)
          {
            localException3.printStackTrace();
          }
        }
      }
    }
  }
  
  public static FlightLog getInstance()
  {
    if (instance == null) {
      instance = new FlightLog();
    }
    return instance;
  }
  
  private String[] sdCardPath()
  {
    Object localObject1 = null;
    Object localObject2 = (StorageManager)this.context.getSystemService("storage");
    try
    {
      localObject2 = (String[])localObject2.getClass().getMethod("getVolumePaths", null).invoke(localObject2, null);
      localObject1 = localObject2;
      int i = localObject2.length;
      localObject1 = localObject2;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return (String[])localObject1;
  }
  
  public void closedFlightNote(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (;;)
    {
      try
      {
        flightNoteClose();
        this.isCreatedNode = false;
        this.mHandler.removeCallbacksAndMessages(null);
        return;
      }
      finally {}
      if (this.isCreatedNode)
      {
        this.isCreatedNode = false;
        this.mHandler.removeCallbacks(this.closeFlightModeRunnable);
        this.mHandler.postDelayed(this.closeFlightModeRunnable, 30000L);
      }
    }
  }
  
  