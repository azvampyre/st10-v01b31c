package com.yuneec.flightmode15;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.BaseDialog;
import com.yuneec.widget.SwitchWidgetPreference;
import com.yuneec.widget.TextWidgetPreference;

public class Cgo2SettingsFragment
  extends PreferenceFragment
  implements Preference.OnPreferenceClickListener, Preference.OnPreferenceChangeListener
{
  private static final String KEY_AUDIO = "audio";
  private static final String KEY_CALIBRATION = "calibration";
  private static final String KEY_GPS = "gps";
  private static final String KEY_RESOLUTION = "resolution";
  private static final String TAG = "Cgo2SettingsFragment";
  private SwitchWidgetPreference mAudioPref;
  private Preference mCalibrationPref;
  private UARTController mController = null;
  private SwitchWidgetPreference mGPSPref;
  private Handler mHandler = new Handler();
  private WeakHandler<PreferenceFragment> mHttpHandler = new WeakHandler(this)
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((PreferenceFragment)getOwner() == null) {
        Log.i("Cgo2SettingsFragment", "fragment destoryed");
      }
      for (;;)
      {
        return;
        switch (paramAnonymousMessage.what)
        {
        default: 
          break;
        case 1: 
          switch (paramAnonymousMessage.arg1)
          {
          default: 
            break;
          case 24: 
          case 45: 
          case 47: 
          case 51: 
          case 57: 
          case 59: 
            try
            {
              if (!"HTTPCODE OK".equals((String)paramAnonymousMessage.obj)) {
                Log.e("Cgo2SettingsFragment", "Failed to communicate camera");
              }
              Cgo2SettingsFragment.this.dismissProgressDialog();
            }
            catch (Exception paramAnonymousMessage)
            {
              Log.e("Cgo2SettingsFragment", "Exception--Failed to communicate camera");
            }
            break;
          case 58: 
            try
            {
              if (!(paramAnonymousMessage.obj instanceof String)) {
                break label254;
              }
              paramAnonymousMessage = (String)paramAnonymousMessage.obj;
              Cgo2SettingsFragment.this.mResolutionPref.setValue(paramAnonymousMessage);
              Cgo2SettingsFragment.this.mResolutionPref.setWidgetText(paramAnonymousMessage);
              Cgo2SettingsFragment.this.dismissProgressDialog();
            }
            catch (Exception paramAnonymousMessage)
            {
              Log.e("Cgo2SettingsFragment", "Exception--Failed to get video mode");
            }
            continue;
            Log.e("Cgo2SettingsFragment", "Failed to get video mode");
            break;
          case 60: 
            for (;;)
            {
              try
              {
                if (!(paramAnonymousMessage.obj instanceof Integer)) {
                  break label328;
                }
                if (((Integer)paramAnonymousMessage.obj).intValue() != 0) {
                  break label323;
                }
                bool = true;
                Cgo2SettingsFragment.this.mAudioPref.setWidgetStates(bool);
                Cgo2SettingsFragment.this.dismissProgressDialog();
              }
              catch (Exception paramAnonymousMessage)
              {
                Log.e("Cgo2SettingsFragment", "Exception--Failed to get audio state");
              }
              break;
              boolean bool = false;
              continue;
              Log.e("Cgo2SettingsFragment", "Failed to get audio state");
            }
          case 32: 
            if ((paramAnonymousMessage.obj instanceof String))
            {
              paramAnonymousMessage = (String)paramAnonymousMessage.obj;
              Cgo2SettingsFragment.this.setResolutionPreference(paramAnonymousMessage);
              Cgo2SettingsFragment.this.dismissProgressDialog();
            }
            break;
          case 31: 
            if (!"HTTPCODE OK".equals(String.valueOf(paramAnonymousMessage.obj)))
            {
              Log.e("Cgo2SettingsFragment", "Failed to set resolution");
              Cgo2SettingsFragment.this.dismissProgressDialog();
            }
            Cgo2SettingsFragment.this.dismissProgressDialog();
            break;
          case 33: 
            label254:
            label323:
            label328:
            if ("HTTPCODE OK".equals(String.valueOf(paramAnonymousMessage.obj)))
            {
              Cgo2SettingsFragment.this.setVideoResolution(Cgo2SettingsFragment.this.mResolution);
            }
            else
            {
              Log.e("Cgo2SettingsFragment", "Failed to set standard");
              Cgo2SettingsFragment.this.dismissProgressDialog();
            }
            break;
          }
          break;
        }
      }
    }
  };
  private Messenger mHttpMessenger = new Messenger(this.mHttpHandler);
  private IPCameraManager mIPCameraManager;
  private ProgressDialog mProgressDialog;
  private Runnable mProgressTimeOutRunnable = new Runnable()
  {
    public void run()
    {
      Toast.makeText(Cgo2SettingsFragment.this.getActivity(), 2131296627, 1).show();
      Cgo2SettingsFragment.this.dismissProgressDialog();
    }
  };
  private String mResolution;
  private TextWidgetPreference mResolutionPref;
  
  private void calibrationAction()
  {
    final BaseDialog localBaseDialog = new BaseDialog(getActivity(), 2131230729);
    localBaseDialog.setContentView(2130903048);
    ((Button)localBaseDialog.findViewById(2131689518)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (Cgo2SettingsFragment.this.mController != null) {
          Cgo2SettingsFragment.this.mController.setTTBState(false, Utilities.HW_VB_BASE + 3, false);
        }
        localBaseDialog.dismiss();
      }
    });
    ((Button)localBaseDialog.findViewById(2131689519)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (Cgo2SettingsFragment.this.mController != null) {
          Cgo2SettingsFragment.this.mController.setTTBState(false, Utilities.HW_VB_BASE + 4, false);
        }
        localBaseDialog.dismiss();
      }
    });
    localBaseDialog.show();
  }
  
  private void dismissProgressDialog()
  {
    if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing()))
    {
      this.mProgressDialog.dismiss();
      this.mProgressDialog = null;
    }
    removeProgressRunnable();
  }
  
  private void removeProgressRunnable()
  {
    if (this.mProgressTimeOutRunnable != null) {
      this.mHandler.removeCallbacks(this.mProgressTimeOutRunnable);
    }
  }
  
  private void setResolutionPreference(String paramString)
  {
    this.mResolutionPref.setEnabled(true);
    if (paramString.contains("60P"))
    {
      this.mResolutionPref.setValue("60P");
      this.mResolutionPref.setWidgetText(paramString);
    }
    for (;;)
    {
      return;
      if (paramString.contains("50P"))
      {
        this.mResolutionPref.setValue("50P");
        this.mResolutionPref.setWidgetText(paramString);
      }
      else if (paramString.contains("48P"))
      {
        this.mResolutionPref.setValue("48P");
        this.mResolutionPref.setWidgetText(paramString);
      }
      else
      {
        this.mResolutionPref.setEnabled(false);
      }
    }
  }
  
  private void setVideoResolution(String paramString)
  {
    if (paramString != null)
    {
      if (!"48P".equals(paramString)) {
        break label26;
      }
      this.mIPCameraManager.setVideoResolution(this.mHttpMessenger, 5);
    }
    for (;;)
    {
      return;
      label26:
      if ("50P".equals(paramString)) {
        this.mIPCameraManager.setVideoResolution(this.mHttpMessenger, 3);
      } else if ("60P".equals(paramString)) {
        this.mIPCameraManager.setVideoResolution(this.mHttpMessenger, 1);
      }
    }
  }
  
  private void setVideoStandard(String paramString)
  {
    if (paramString != null)
    {
      if ((!"48P".equals(paramString)) && (!"60P".equals(paramString))) {
        break label35;
      }
      this.mIPCameraManager.setVideoStandard(this.mHttpMessenger, 1);
    }
    for (;;)
    {
      return;
      label35:
      if ("50P".equals(paramString)) {
        this.mIPCameraManager.setVideoStandard(this.mHttpMessenger, 2);
      }
    }
  }
  
  private void showProgressDialog()
  {
    this.mProgressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(2131296620), false, false);
    this.mProgressDialog.setCancelable(true);
    this.mHandler.postDelayed(this.mProgressTimeOutRunnable, 5000L);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    addPreferencesFromResource(2131099649);
    paramBundle = getActivity().getIntent();
    int i = paramBundle.getExtras().getInt("fmodeStatus");
    boolean bool = paramBundle.getExtras().getBoolean("gps_switch");
    this.mResolutionPref = ((TextWidgetPreference)findPreference("resolution"));
    this.mResolutionPref.setOnPreferenceChangeListener(this);
    this.mAudioPref = ((SwitchWidgetPreference)findPreference("audio"));
    this.mAudioPref.setOnPreferenceClickListener(this);
    this.mGPSPref = ((SwitchWidgetPreference)findPreference("gps"));
    this.mCalibrationPref = findPreference("calibration");
    this.mCalibrationPref.setOnPreferenceClickListener(this);
    this.mGPSPref.setWidgetStates(bool);
    if (16 != i)
    {
      this.mGPSPref.setEnabled(false);
      this.mCalibrationPref.setEnabled(false);
    }
    for (;;)
    {
      this.mController = UARTController.getInstance();
      return;
      this.mGPSPref.setOnPreferenceClickListener(this);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.mController != null) {
      this.mController = null;
    }
  }
  
  public void onPause()
  {
    super.onPause();
    this.mIPCameraManager.finish();
    this.mIPCameraManager = null;
    this.mHandler.removeCallbacks(this.mProgressTimeOutRunnable);
  }
  
  public boolean onPreferenceChange(Preference paramPreference, Object paramObject)
  {
    if (paramPreference.getKey().equals("resolution"))
    {
      this.mResolution = ((String)paramObject);
      setVideoStandard(this.mResolution);
      showProgressDialog();
    }
    return true;
  }
  
  public boolean onPreferenceClick(Preference paramPreference)
  {
    boolean bool;
    int i;
    if (paramPreference.getKey().equals("audio"))
    {
      paramPreference = this.mAudioPref;
      if (this.mAudioPref.getIsOnStates())
      {
        bool = false;
        paramPreference.setWidgetStates(bool);
        if (!this.mAudioPref.getIsOnStates()) {
          break label65;
        }
        i = 0;
        label46:
        this.mIPCameraManager.setAudioState(this.mHttpMessenger, i);
      }
    }
    for (;;)
    {
      return true;
      bool = true;
      break;
      label65:
      i = 1;
      break label46;
      if (paramPreference.getKey().equals("gps"))
      {
        if (this.mController != null)
        {
          paramPreference = this.mGPSPref;
          if (this.mGPSPref.getIsOnStates()) {}
          for (bool = false;; bool = true)
          {
            paramPreference.setWidgetStates(bool);
            if (!Boolean.valueOf(this.mGPSPref.getIsOnStates()).booleanValue()) {
              break label150;
            }
            this.mController.setTTBState(false, Utilities.HW_VB_BASE + 2, false);
            break;
          }
          label150:
          this.mController.setTTBState(false, Utilities.HW_VB_BASE + 1, false);
        }
      }
      else if (paramPreference.getKey().equals("calibration")) {
        calibrationAction();
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
    this.mIPCameraManager = IPCameraManager.getIPCameraManager(getActivity(), 102);
    this.mIPCameraManager.getVideoResolution(this.mHttpMessenger);
    this.mIPCameraManager.getAudioState(this.mHttpMessenger);
    showProgressDialog();
  }
}


