package com.yuneec.flightmode15;

import android.os.Handler;
import java.lang.ref.WeakReference;

public abstract class WeakHandler<T>
  extends Handler
{
  private WeakReference<T> mOwner;
  
  public WeakHandler(T paramT)
  {
    this.mOwner = new WeakReference(paramT);
  }
  
  public T getOwner()
  {
    return (T)this.mOwner.get();
  }
}


