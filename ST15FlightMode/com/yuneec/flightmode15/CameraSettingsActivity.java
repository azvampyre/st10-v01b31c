package com.yuneec.flightmode15;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

public class CameraSettingsActivity
  extends PreferenceActivity
{
  private String mCurrentCamera;
  private FragmentTransaction transaction;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mCurrentCamera = getIntent().getExtras().getString("currentCamera");
    boolean bool = getIntent().getExtras().getBoolean("wifi_connected");
    this.transaction = getFragmentManager().beginTransaction();
    if (((this.mCurrentCamera.equals("C-GO3")) || (this.mCurrentCamera.equals("C-GO3-Pro"))) && (bool))
    {
      this.transaction.replace(16908290, new CameraSettingsFragment());
      this.transaction.commit();
    }
    for (;;)
    {
      return;
      if ((this.mCurrentCamera.equals("C-GO2")) && (bool))
      {
        this.transaction.replace(16908290, new Cgo2SettingsFragment());
        this.transaction.commit();
      }
      else
      {
        this.transaction.replace(16908290, new NormalSettingsFragment());
        this.transaction.commit();
      }
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332) {
      finish();
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  protected void onResume()
  {
    super.onResume();
  }
}


