package com.yuneec.flightmode15;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import com.yuneec.channelsettings.DRData;
import com.yuneec.channelsettings.ServoData;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flight_settings.ChannelMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ModelImporter
  implements MediaScannerConnection.OnScanCompletedListener
{
  private static final String TAG = "ModelImporter";
  private boolean isMediaScanning;
  private Context mContext;
  private Uri mMediaUri;
  
  public ModelImporter(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  private static void buildContentValue(ContentValues paramContentValues, String paramString1, String paramString2)
  {
    if (!paramString2.equals("null")) {
      paramContentValues.put(paramString1, paramString2);
    }
  }
  
  private File checkVaild(String paramString, int paramInt, File paramFile)
  {
    File localFile = paramFile;
    if (paramFile.exists()) {
      localFile = checkVaild(paramString, paramInt + 1, new File(paramString + "-" + paramInt + ".jpg"));
    }
    return localFile;
  }
  
  private static boolean importChannelMap(Context paramContext, InputStream paramInputStream, long paramLong)
  {
    int i = 0;
    if (paramInputStream == null) {
      i = 1;
    }
    if (i == 0) {}
    for (;;)
    {
      try
      {
        BufferedReader localBufferedReader = new java/io/BufferedReader;
        localObject = new java/io/InputStreamReader;
        ((InputStreamReader)localObject).<init>(paramInputStream);
        localBufferedReader.<init>((Reader)localObject);
        try
        {
          localObject = DataProviderHelper.readChannelMapFromDatabase(paramContext, paramLong);
          i = 0;
          str = localBufferedReader.readLine();
          if (str == null)
          {
            DataProviderHelper.writeChannelMapFromDatabase(paramContext, (ChannelMap[])localObject);
            bool = true;
            return bool;
          }
          paramInputStream = str.split(",");
          if (paramInputStream.length == 3) {
            continue;
          }
          StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
          localStringBuilder1.<init>("fields corrputted len:");
          Log.w("ModelImporter", paramInputStream.length + " content:" + str);
          continue;
          Log.e("ModelImporter", "Parsing Mixing Params Error :" + paramContext.getMessage());
        }
        catch (Exception paramContext) {}
      }
      catch (Exception paramContext)
      {
        Object localObject;
        String str;
        boolean bool;
        continue;
      }
      bool = false;
      continue;
      try
      {
        localObject[i].channel = Integer.parseInt(paramInputStream[0]);
        if (i >= localObject.length) {
          continue;
        }
        localObject[i].function = paramInputStream[1];
        localObject[i].hardware = paramInputStream[2];
        localObject[i].alias = paramInputStream[3];
        i++;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>("fields corrputted len:");
        Log.w("ModelImporter", paramInputStream.length + " content:" + str);
      }
      continue;
      Log.e("ModelImporter", "index >= cm_olde.length");
    }
  }
  
  private static boolean importDRDatas(Context paramContext, InputStream paramInputStream, long paramLong)
  {
    int i = 0;
    if (paramInputStream == null) {
      i = 1;
    }
    if (i == 0) {}
    for (;;)
    {
      try
      {
        BufferedReader localBufferedReader = new java/io/BufferedReader;
        localObject1 = new java/io/InputStreamReader;
        ((InputStreamReader)localObject1).<init>(paramInputStream);
        localBufferedReader.<init>((Reader)localObject1);
        try
        {
          paramInputStream = new java/util/ArrayList;
          paramInputStream.<init>();
          localObject1 = new DRData[3];
          i = 0;
          if (i >= 3)
          {
            str1 = localBufferedReader.readLine();
            if (str1 == null)
            {
              DataProviderHelper.writeDRDataToDatabase(paramContext, (DRData[])paramInputStream.get(0));
              DataProviderHelper.writeDRDataToDatabase(paramContext, (DRData[])paramInputStream.get(1));
              DataProviderHelper.writeDRDataToDatabase(paramContext, (DRData[])paramInputStream.get(2));
              bool = true;
              return bool;
            }
          }
          else
          {
            paramInputStream.add(i, DataProviderHelper.readDRDataFromDatabase(paramContext, paramLong, i));
            i++;
            continue;
          }
          localObject1 = str1.split(",");
          if (localObject1.length == 9) {
            continue;
          }
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("fields corrputted len:");
          Log.w("ModelImporter", localObject1.length + " content:" + str1);
          continue;
          Log.e("ModelImporter", "Parsing Mixing Params Error :" + paramContext.getMessage());
        }
        catch (Exception paramContext) {}
      }
      catch (Exception paramContext)
      {
        Object localObject1;
        String str1;
        boolean bool;
        Object localObject2;
        continue;
      }
      bool = false;
      continue;
      try
      {
        i = Integer.parseInt(localObject1[0]);
        localObject2 = (DRData[])paramInputStream.get(i);
        str2 = localObject1[1];
        i = -1;
        if (!str2.equals("ail")) {
          continue;
        }
        i = 0;
        localObject2[i].func = str2;
        localObject2[i].sw = localObject1[2];
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        try
        {
          int j = Integer.parseInt(localObject1[3]);
          localObject2[i].curveparams[j].sw_state = j;
          try
          {
            localObject2[i].curveparams[j].rate1 = Float.parseFloat(localObject1[4]);
            localObject2[i].curveparams[j].rate2 = Float.parseFloat(localObject1[5]);
            localObject2[i].curveparams[j].expo1 = Float.parseFloat(localObject1[6]);
            localObject2[i].curveparams[j].expo2 = Float.parseFloat(localObject1[7]);
            localObject2[i].curveparams[j].offset = Float.parseFloat(localObject1[8]);
          }
          catch (Exception localException)
          {
            StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
            localStringBuilder1.<init>("fields corrputted len:");
            Log.w("ModelImporter", localObject1.length + " content:" + str1);
          }
        }
        catch (NumberFormatException localNumberFormatException2)
        {
          String str2;
          StringBuilder localStringBuilder2;
          StringBuilder localStringBuilder3 = new java/lang/StringBuilder;
          localStringBuilder3.<init>("fields corrputted len:");
          Log.w("ModelImporter", localObject1.length + " content:" + str1);
        }
        localNumberFormatException1 = localNumberFormatException1;
        localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>("fields corrputted len:");
        Log.w("ModelImporter", localObject1.length + " content:" + str1);
      }
      continue;
      if (str2.equals("ele")) {
        i = 1;
      } else if (str2.equals("rud")) {
        i = 2;
      } else {
        Log.e("ModelImporter", "Invalid function");
      }
    }
  }
  
  public static boolean importMixingChannels(Context paramContext, InputStream paramInputStream1, InputStream paramInputStream2, InputStream paramInputStream3, InputStream paramInputStream4, long paramLong)
  {
    if ((importChannelMap(paramContext, paramInputStream1, paramLong)) && (importThrDatas(paramContext, paramInputStream2, paramLong)) && (importDRDatas(paramContext, paramInputStream3, paramLong)) && (importServos(paramContext, paramInputStream4, paramLong))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean importMixingChannels(Context paramContext, String paramString1, String paramString2, long paramLong)
  {
    Object localObject2 = new File(new File(paramString1, paramString2), "channelmap.csv");
    try
    {
      localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>((File)localObject2);
      localObject4 = new File(new File(paramString1, paramString2), "thr_datas.csv");
    }
    catch (FileNotFoundException localFileNotFoundException3)
    {
      try
      {
        localObject2 = new java/io/FileInputStream;
        ((FileInputStream)localObject2).<init>((File)localObject4);
        localFile = new File(new File(paramString1, paramString2), "dr_datas.csv");
      }
      catch (FileNotFoundException localFileNotFoundException3)
      {
        try
        {
          localObject4 = new java/io/FileInputStream;
          ((FileInputStream)localObject4).<init>(localFile);
          paramString2 = new File(new File(paramString1, paramString2), "servos.csv");
        }
        catch (FileNotFoundException localFileNotFoundException3)
        {
          try
          {
            File localFile;
            for (;;)
            {
              FileInputStream localFileInputStream;
              Object localObject4;
              paramString1 = new java/io/FileInputStream;
              paramString1.<init>(paramString2);
              importMixingChannels(paramContext, localFileInputStream, (InputStream)localObject2, (InputStream)localObject4, paramString1, paramLong);
              if (localFileInputStream != null) {}
              try
              {
                localFileInputStream.close();
                if (localObject2 != null) {
                  ((FileInputStream)localObject2).close();
                }
                if (localObject4 != null) {
                  ((FileInputStream)localObject4).close();
                }
                if (paramString1 != null) {
                  paramString1.close();
                }
              }
              catch (IOException paramContext)
              {
                Object localObject1;
                Object localObject3;
                Object localObject5;
                for (;;) {}
              }
              return true;
              localFileNotFoundException1 = localFileNotFoundException1;
              Log.w("ModelImporter", ((File)localObject2).getAbsolutePath() + " is not exist!");
              localObject1 = null;
              continue;
              localFileNotFoundException2 = localFileNotFoundException2;
              Log.w("ModelImporter", ((File)localObject4).getAbsolutePath() + " is not exist!");
              localObject3 = null;
            }
            localFileNotFoundException3 = localFileNotFoundException3;
            Log.w("ModelImporter", localFile.getAbsolutePath() + " is not exist!");
            localObject5 = null;
          }
          catch (FileNotFoundException paramString1)
          {
            for (;;)
            {
              Log.w("ModelImporter", paramString2.getAbsolutePath() + " is not exist!");
              paramString1 = null;
            }
          }
        }
      }
    }
  }
  
  private static boolean importServos(Context paramContext, InputStream paramInputStream, long paramLong)
  {
    int i = 0;
    if (paramInputStream == null) {
      i = 1;
    }
    if (i == 0) {}
    for (;;)
    {
      try
      {
        BufferedReader localBufferedReader = new java/io/BufferedReader;
        localObject1 = new java/io/InputStreamReader;
        ((InputStreamReader)localObject1).<init>(paramInputStream);
        localBufferedReader.<init>((Reader)localObject1);
        try
        {
          paramInputStream = paramContext.getResources().getStringArray(2131361797);
          localObject1 = new ServoData[paramInputStream.length];
          localArrayList = new java/util/ArrayList;
          localArrayList.<init>();
          i = 0;
          if (i >= 3)
          {
            localObject1 = localBufferedReader.readLine();
            if (localObject1 == null)
            {
              DataProviderHelper.writeServoDataToDatabase(paramContext, (ServoData[])localArrayList.get(0));
              DataProviderHelper.writeServoDataToDatabase(paramContext, (ServoData[])localArrayList.get(1));
              DataProviderHelper.writeServoDataToDatabase(paramContext, (ServoData[])localArrayList.get(2));
              bool = true;
              return bool;
            }
          }
          else
          {
            localArrayList.add(i, DataProviderHelper.readServoDataFromDatabase(paramContext, paramLong, i));
            i++;
            continue;
          }
          arrayOfString = ((String)localObject1).split(",");
          if (arrayOfString.length == 7) {
            continue;
          }
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("fields corrputted len:");
          Log.w("ModelImporter", arrayOfString.length + " content:" + (String)localObject1);
          continue;
          Log.e("ModelImporter", "Parsing Mixing Params Error :" + paramContext.getMessage());
        }
        catch (Exception paramContext) {}
      }
      catch (Exception paramContext)
      {
        Object localObject1;
        ArrayList localArrayList;
        boolean bool;
        String[] arrayOfString;
        Object localObject2;
        continue;
      }
      bool = false;
    }
    for (;;)
    {
      try
      {
        i = Integer.parseInt(arrayOfString[0]);
        localObject2 = (ServoData[])localArrayList.get(i);
        String str = arrayOfString[1];
        i = Arrays.asList(paramInputStream).indexOf(str);
        localObject2[i].func = str;
        str = localObject2[i];
        if (!arrayOfString[3].equals("true")) {
          break label475;
        }
        bool = true;
        str.reverse = bool;
        try
        {
          localObject2[i].subTrim = Integer.parseInt(arrayOfString[2]);
          localObject2[i].speed = Integer.parseInt(arrayOfString[4]);
          localObject2[i].travelL = Integer.parseInt(arrayOfString[5]);
          localObject2[i].travelR = Integer.parseInt(arrayOfString[6]);
        }
        catch (NumberFormatException localNumberFormatException1)
        {
          StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
          localStringBuilder1.<init>("fields corrputted len:");
          Log.w("ModelImporter", arrayOfString.length + " content:" + (String)localObject1);
        }
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>("fields corrputted len:");
        Log.w("ModelImporter", arrayOfString.length + " content:" + (String)localObject1);
      }
      break;
      label475:
      bool = false;
    }
  }
  
  