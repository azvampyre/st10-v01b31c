package com.yuneec.mission;

import android.os.Environment;
import com.yuneec.uartcontroller.WaypointData;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SaveMissionData
{
  private static final String CCC_MISSIONS_KEY = "ccc_missions";
  private static final String WAY_POINT_KEY = "way_points";
  private static final String WAY_POINT_NAME_KEY = "name";
  private static final String WP_ALTITUDE_KEY = "altitude";
  private static final String WP_GIMBAL_PITCH_KEY = "gimbalPitch";
  private static final String WP_GIMBAL_YAM_KEY = "gimbalYam";
  private static final String WP_INDEX_KEY = "pointerIndex";
  private static final String WP_LATITUDE_KEY = "latitude";
  private static final String WP_LONGITUDE_KEY = "longitude";
  private static final String WP_PITCH_KEY = "pitch";
  private static final String WP_ROLL_KEY = "roll";
  private static final String WP_YAW_KEY = "yaw";
  
  private static File getMissionDirectiory()
  {
    File localFile = new File(Environment.getExternalStorageDirectory().toString() + "/flightmode");
    localFile.mkdirs();
    return new File(localFile, "mission_data.json");
  }
  
  public static void loadCruveCableCam(String paramString, List<WaypointData> paramList)
  {
    Object localObject = loadMissionFromFile();
    if (localObject == null) {}
    for (;;)
    {
      return;
      try
      {
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>((String)localObject);
        localObject = localJSONObject.getJSONArray("ccc_missions");
        i = 0;
        if (i < ((JSONArray)localObject).length())
        {
          localJSONObject = ((JSONArray)localObject).getJSONObject(i);
          if (paramString.equals(localJSONObject.getString("name"))) {
            readWaypointArray(localJSONObject.getJSONArray("way_points"), paramList);
          }
        }
      }
      catch (JSONException paramString)
      {
        for (;;)
        {
          int i;
          paramString.printStackTrace();
          break;
          i++;
        }
      }
      catch (IOException paramString)
      {
        paramString.printStackTrace();
      }
    }
  }
  
  private static String loadMissionFromFile()
  {
    try
    {
      Object localObject1 = new java/io/BufferedInputStream;
      Object localObject3 = new java/io/FileInputStream;
      ((FileInputStream)localObject3).<init>(getMissionDirectiory());
      ((BufferedInputStream)localObject1).<init>((InputStream)localObject3);
      localObject3 = new byte[((InputStream)localObject1).available()];
      ((InputStream)localObject1).read((byte[])localObject3);
      ((InputStream)localObject1).close();
      localObject1 = new java/lang/String;
      ((String)localObject1).<init>((byte[])localObject3, "UTF-8");
      return (String)localObject1;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
        Object localObject2 = null;
      }
    }
  }
  
  private static WaypointData readWaypoint(JSONObject paramJSONObject)
    throws IOException, JSONException
  {
    WaypointData localWaypointData = new WaypointData();
    localWaypointData.pointerIndex = paramJSONObject.getInt("pointerIndex");
    localWaypointData.latitude = ((float)paramJSONObject.getDouble("latitude"));
    localWaypointData.longitude = ((float)paramJSONObject.getDouble("longitude"));
    localWaypointData.altitude = ((float)paramJSONObject.getDouble("altitude"));
    localWaypointData.roll = ((float)paramJSONObject.getDouble("roll"));
    localWaypointData.pitch = ((float)paramJSONObject.getDouble("pitch"));
    localWaypointData.yaw = ((float)paramJSONObject.getDouble("yaw"));
    localWaypointData.gimbalPitch = ((float)paramJSONObject.getDouble("gimbalPitch"));
    localWaypointData.gimbalYam = ((float)paramJSONObject.getDouble("gimbalYam"));
    return localWaypointData;
  }
  
  private static void readWaypointArray(JSONArray paramJSONArray, List<WaypointData> paramList)
    throws IOException, JSONException
  {
    for (int i = 0;; i++)
    {
      if (i >= paramJSONArray.length()) {
        return;
      }
      paramList.add(readWaypoint((JSONObject)paramJSONArray.get(i)));
    }
  }
  
  public static void saveCruveCableCam(String paramString, List<WaypointData> paramList)
  {
    for (;;)
    {
      try
      {
        localObject = loadMissionFromFile();
        localJSONArray = new org/json/JSONArray;
        localJSONArray.<init>();
        if (localObject == null) {
          continue;
        }
        localJSONObject1 = new org/json/JSONObject;
        localJSONObject1.<init>((String)localObject);
        localObject = localJSONObject1.getJSONArray("ccc_missions");
        i = 0;
        if (i >= ((JSONArray)localObject).length())
        {
          localJSONObject1.remove("ccc_missions");
          localObject = new org/json/JSONArray;
          ((JSONArray)localObject).<init>();
          writeWaypointsArray((JSONArray)localObject, paramList);
          paramList = new org/json/JSONObject;
          paramList.<init>();
          paramList.put("name", paramString);
          paramList.put("way_points", localObject);
          localJSONArray.put(paramList);
          localJSONObject1.put("ccc_missions", localJSONArray);
          saveMissionToFile(localJSONObject1.toString(4));
          return;
        }
      }
      catch (JSONException paramString)
      {
        Object localObject;
        JSONArray localJSONArray;
        JSONObject localJSONObject1;
        int i;
        JSONObject localJSONObject2;
        paramString.printStackTrace();
        continue;
      }
      catch (IOException paramString)
      {
        paramString.printStackTrace();
        continue;
      }
      localJSONObject2 = ((JSONArray)localObject).getJSONObject(i);
      if (!paramString.equals(localJSONObject2.getString("name"))) {
        localJSONArray.put(localJSONObject2);
      }
      i++;
      continue;
      localJSONObject1 = new JSONObject();
    }
  }
  
  private static void saveMissionToFile(String paramString)
    throws IOException
  {
    BufferedOutputStream localBufferedOutputStream = new BufferedOutputStream(new FileOutputStream(getMissionDirectiory()));
    localBufferedOutputStream.write(paramString.getBytes());
    localBufferedOutputStream.close();
  }
  
  private static JSONObject writeWaypoint(WaypointData paramWaypointData)
    throws IOException, JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("pointerIndex", paramWaypointData.pointerIndex);
    localJSONObject.put("latitude", paramWaypointData.latitude);
    localJSONObject.put("longitude", paramWaypointData.longitude);
    localJSONObject.put("altitude", paramWaypointData.altitude);
    localJSONObject.put("roll", paramWaypointData.roll);
    localJSONObject.put("pitch", paramWaypointData.pitch);
    localJSONObject.put("yaw", paramWaypointData.yaw);
    localJSONObject.put("gimbalPitch", paramWaypointData.gimbalPitch);
    localJSONObject.put("gimbalYam", paramWaypointData.gimbalYam);
    return localJSONObject;
  }
  
  private static void writeWaypointsArray(JSONArray paramJSONArray, List<WaypointData> paramList)
    throws IOException, JSONException
  {
    paramList = paramList.iterator();
    for (;;)
    {
      if (!paramList.hasNext()) {
        return;
      }
      paramJSONArray.put(writeWaypoint((WaypointData)paramList.next()));
    }
  }
}


