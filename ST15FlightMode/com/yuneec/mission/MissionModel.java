package com.yuneec.mission;

import com.yuneec.uartcontroller.RoiData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.WaypointData;
import java.util.ArrayList;
import java.util.Iterator;

public class MissionModel
{
  public static MissionModelInterface misssionModelCreate(MissionInterface.MissionType paramMissionType)
  {
    Object localObject = null;
    switch (paramMissionType)
    {
    default: 
      paramMissionType = (MissionInterface.MissionType)localObject;
    }
    for (;;)
    {
      return paramMissionType;
      paramMissionType = new ModelCcc();
      continue;
      paramMissionType = new ModelJourney();
      continue;
      paramMissionType = new ModelCycle();
      continue;
      paramMissionType = new ModelRoi();
    }
  }
  
  public static abstract class BaseMode
    implements MissionModel.MissionModelInterface
  {
    public int cancel()
    {
      int i = 1;
      if (UARTController.getInstance().sendMissionRequest(4, getModeTypeCommand(), getWaypointSize())) {
        i = 0;
      }
      return i;
    }
    
    protected abstract int getModeTypeCommand();
    
    protected int getWaypointSize()
    {
      return 0;
    }
    
    public int pause()
    {
      int i = 1;
      if (UARTController.getInstance().sendMissionRequest(2, getModeTypeCommand(), getWaypointSize())) {
        i = 0;
      }
      return i;
    }
    
    public Object query()
    {
      return null;
    }
    
    public int resume()
    {
      int i = 1;
      if (UARTController.getInstance().sendMissionRequest(3, getModeTypeCommand(), getWaypointSize())) {
        i = 0;
      }
      return i;
    }
    
    public int terminate()
    {
      int i = 1;
      if (UARTController.getInstance().sendMissionRequest(4, getModeTypeCommand(), getWaypointSize())) {
        i = 0;
      }
      return i;
    }
  }
  
  public static abstract interface MissionModelInterface
  {
    public abstract int cancel();
    
    public abstract int confirm();
    
    public abstract MissionInterface.MissionType getMissionType();
    
    public abstract int pause();
    
    public abstract int prepare(Object paramObject)
      throws IllegalArgumentException;
    
    public abstract Object query();
    
    public abstract int resume();
    
    public abstract int terminate();
  }
  
  public static class ModelCcc
    extends MissionModel.BaseMode
  {
    private ArrayList<WaypointData> recordWaypoint;
    
    public int confirm()
    {
      int i = 1;
      if ((this.recordWaypoint == null) || (this.recordWaypoint.size() == 0)) {
        i = 83;
      }
      for (;;)
      {
        return i;
        if (!UARTController.getInstance().sendMissionRequest(6, getModeTypeCommand(), getWaypointSize()))
        {
          i = 1;
        }
        else
        {
          Iterator localIterator = this.recordWaypoint.iterator();
          WaypointData localWaypointData;
          do
          {
            if (!localIterator.hasNext())
            {
              if (UARTController.getInstance().sendMissionRequest(0, getModeTypeCommand(), getWaypointSize())) {
                i = 0;
              }
              break;
            }
            localWaypointData = (WaypointData)localIterator.next();
          } while (UARTController.getInstance().sendMissionSettingCCC(localWaypointData));
          i = 1;
        }
      }
    }
    
    public MissionInterface.MissionType getMissionType()
    {
      return MissionInterface.MissionType.CRUVE_CABLE_CAM;
    }
    
    protected int getModeTypeCommand()
    {
      return 3;
    }
    
    protected int getWaypointSize()
    {
      if (this.recordWaypoint == null) {}
      for (int i = 0;; i = this.recordWaypoint.size()) {
        return i;
      }
    }
    
    public int prepare(Object paramObject)
      throws IllegalArgumentException
    {
      if (!(paramObject instanceof ArrayList)) {
        throw new IllegalArgumentException();
      }
      this.recordWaypoint = ((ArrayList)paramObject);
      return 0;
    }
    
    public Object query()
    {
      WaypointData localWaypointData = new WaypointData();
      if (UARTController.getInstance().sendMissionRequestGetWaypoint(localWaypointData)) {}
      for (;;)
      {
        return localWaypointData;
        localWaypointData = null;
      }
    }
  }
  
  public static class ModelCycle
    extends MissionModel.BaseMode
  {
    public int confirm()
    {
      int i = 0;
      if (UARTController.getInstance().sendMissionRequest(0, getModeTypeCommand(), 0)) {}
      for (;;)
      {
        return i;
        i = 1;
      }
    }
    
    public MissionInterface.MissionType getMissionType()
    {
      return MissionInterface.MissionType.CYCLE;
    }
    
    protected int getModeTypeCommand()
    {
      return 4;
    }
    
    public int prepare(Object paramObject)
      throws IllegalArgumentException
    {
      return 0;
    }
  }
  
  public static class ModelJourney
    extends MissionModel.BaseMode
  {
    private Integer journeyDistance;
    
    public int confirm()
    {
      int i = 1;
      int j = 0;
      if (this.journeyDistance == null) {
        return i;
      }
      if (UARTController.getInstance().sendMissionRequest(0, getModeTypeCommand(), this.journeyDistance.intValue())) {}
      for (i = j;; i = 1) {
        break;
      }
    }
    
    public MissionInterface.MissionType getMissionType()
    {
      return MissionInterface.MissionType.JOURNEY;
    }
    
    protected int getModeTypeCommand()
    {
      return 1;
    }
    
    public int prepare(Object paramObject)
      throws IllegalArgumentException
    {
      if (!(paramObject instanceof Integer)) {
        throw new IllegalArgumentException();
      }
      this.journeyDistance = ((Integer)paramObject);
      return 0;
    }
  }
  
  public static class ModelRoi
    extends MissionModel.BaseMode
  {
    private RoiData roiData;
    
    public int confirm()
    {
      int i = 1;
      if (this.roiData == null) {
        i = 83;
      }
      for (;;)
      {
        return i;
        if (!UARTController.getInstance().sendMissionRequest(6, getModeTypeCommand(), getWaypointSize())) {
          i = 1;
        } else if (!UARTController.getInstance().sendMissionSettingRoiCenter(this.roiData)) {
          i = 1;
        } else if (UARTController.getInstance().sendMissionRequest(0, getModeTypeCommand(), getWaypointSize())) {
          i = 0;
        }
      }
    }
    
    public MissionInterface.MissionType getMissionType()
    {
      return MissionInterface.MissionType.ROI;
    }
    
    protected int getModeTypeCommand()
    {
      return 2;
    }
    
    public int prepare(Object paramObject)
      throws IllegalArgumentException
    {
      if (!(paramObject instanceof RoiData)) {
        throw new IllegalArgumentException();
      }
      this.roiData = ((RoiData)paramObject);
      return 0;
    }
  }
}


