package com.yuneec.uartcontroller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UARTInfoMessage
{
  public static final int BIND_STATE = 12;
  public static final int CALIBRATION_RAW_INFO = 20;
  public static final int CALIBRATION_STATE_INFO = 18;
  public static final int CHANNEL_INFO = 2;
  public static final int FEEDBACK_TELEMETRY_COORDINATES_INFO = 141;
  public static final int FEEDBACK_TELEMETRY_INFORMATION_INFO = 101;
  public static final int MISSION_REPLY = 23;
  public static final int MIXED_CHANNEL_INFO = 5;
  public static final int RADIO_VERSION = 17;
  public static final int RXBIND_INFO = 11;
  public static final int SIGNAL_VALUE = 22;
  public static final int STICK_INFO = 3;
  public static final int SWITCH_CHANGED = 14;
  public static final int SWITCH_STATE = 15;
  public static final int TELEMETRY_INFO = 13;
  public static final int TRANSMITTER_INFO = 1;
  public static final int TRANSMITTER_VERSION = 16;
  public static final int TRIM_INFO = 4;
  public static final int TXBL_VERSION = 21;
  public static final int TX_IN_UPDATE_MODE = 19;
  public int what;
  
  public static class AWaitReply
    extends UARTInfoMessage.UARTRelyMessage
  {
    public AWaitReply()
    {
      this.what = 1061;
    }
  }
  
  public static class Bind
    extends UARTInfoMessage.UARTRelyMessage
  {
    public Bind()
    {
      this.what = 1011;
    }
  }
  
  public static class BindState
    extends UARTInfoMessage
  {
    public static final int BOUND = 1;
    public static final int NOT_BOUND = 0;
    public int state = 0;
  }
  
  public static class CalibrationRawData
    extends UARTInfoMessage
  {
    public ArrayList<Integer> rawData = new ArrayList();
  }
  
  public static class CalibrationState
    extends UARTInfoMessage
  {
    public static final int STATE_MAX = 3;
    public static final int STATE_MID = 2;
    public static final int STATE_MIN = 1;
    public static final int STATE_NONE = 0;
    public static final int STATE_RAG = 4;
    public ArrayList<Integer> hardware_state = new ArrayList();
  }
  
  public static class Channel
    extends UARTInfoMessage
  {
    public ArrayList<Float> channels = new ArrayList();
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      String str = new SimpleDateFormat("yyyyMMdd HH:mm:ss:SSS").format(new Date());
      for (int i = 0;; i++)
      {
        if (i >= this.channels.size()) {
          return str + localStringBuilder.toString() + "\n";
        }
        localStringBuilder.append("," + this.channels.get(i));
      }
    }
  }
  
  public static class ChannelReplyOfCalibration
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ChannelReplyOfCalibration()
    {
      this.what = 1021;
    }
  }
  
  public static class ChannelReplyOfCurve
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ChannelReplyOfCurve()
    {
      this.what = 1024;
    }
  }
  
  public static class ChannelReplyOfMixing
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ChannelReplyOfMixing()
    {
      this.what = 1023;
    }
  }
  
  public static class ChannelReplyOfReverse
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ChannelReplyOfReverse()
    {
      this.what = 1022;
    }
  }
  
  public static class ChannelReplyOfTravel
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ChannelReplyOfTravel()
    {
      this.what = 1025;
    }
  }
  
  public static class ChannelReplyOfTrim
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ChannelReplyOfTrim()
    {
      this.what = 1026;
    }
  }
  
  public static class ExecuteReply
    extends UARTInfoMessage.UARTRelyMessage
  {
    public ExecuteReply()
    {
      this.what = 1051;
    }
  }
  
  public static class JoinReply
    extends UARTInfoMessage.UARTRelyMessage
  {
    public JoinReply()
    {
      this.what = 1041;
    }
  }
  
  public static class MissionReply
    extends UARTInfoMessage
  {
    public byte[] replyInfo;
    
    public MissionReply()
    {
      this.what = 23;
    }
  }
  
  public static class RxBindInfo
    extends UARTInfoMessage
  {
    public int a_bit;
    public int a_num;
    public int mode;
    public int node_id;
    public int pan_id;
    public int sw_bit;
    public int sw_num;
    public int tx_addr;
  }
  
  public static class SaveReplyOfAll
    extends UARTInfoMessage.UARTRelyMessage
  {
    public SaveReplyOfAll()
    {
      this.what = 1081;
    }
  }
  
  public static class SaveReplyOfMixing
    extends UARTInfoMessage.UARTRelyMessage
  {
    public SaveReplyOfMixing()
    {
      this.what = 1083;
    }
  }
  
  public static class SaveReplyOfTravelTrim
    extends UARTInfoMessage.UARTRelyMessage
  {
    public SaveReplyOfTravelTrim()
    {
      this.what = 1082;
    }
  }
  
  public static class Signal
    extends UARTInfoMessage
  {
    public int rf_signal;
    public int tx_signal;
  }
  
  public static class SimulatorReply
    extends UARTInfoMessage.UARTRelyMessage
  {
    public SimulatorReply()
    {
      this.what = 1071;
    }
  }
  
  public static class Stick
    extends UARTInfoMessage
  {
    public float ch1;
    public float ch2;
    public float ch3;
    public float ch4;
  }
  
  public static class SwitchChanged
    extends UARTInfoMessage
  {
    public int hw_id;
    public int new_state;
    public int old_state;
  }
  
  public static class SwitchState
    extends UARTInfoMessage
  {
    public int hw_id;
    public int state;
  }
  
  public static class Telemetry
    extends UARTInfoMessage
  {
    public static final int ERROR_FLAG_AIRPORT_WARNING = 128;
    public static final int ERROR_FLAG_COMPASS_CALIBRATION_WARNING = 32;
    public static final int ERROR_FLAG_COMPLETE_MOTOR_ESC_FAILURE = 8;
    public static final int ERROR_FLAG_FLYAWAY_CHECKER_WARNING = 64;
    public static final int ERROR_FLAG_HIGH_TEMPERATURE_WARNING = 16;
    public static final int ERROR_FLAG_MOTOR_FAILSAFE_MODE = 4;
    public static final int ERROR_FLAG_VOLTAGE_WARNING1 = 1;
    public static final int ERROR_FLAG_VOLTAGE_WARNING2 = 2;
    public float altitude;
    public int cgps_status;
    public float current;
    public int error_flags1;
    public int f_mode;
    public int fix_type;
    public int fsk_rssi;
    public float gps_accH;
    public boolean gps_pos_used;
    public int gps_status;
    public boolean gps_used;
    public int imu_status;
    public float latitude;
    public float longitude;
    public int motor_status;
    public float pitch;
    public int press_compass_status;
    public float roll;
    public int satellites_num;
    public float tas;
    public int vehicle_type;
    public float voltage;
    public float yaw;
    
    public static String getParamsName()
    {
      return ",fsk_rssi,voltage,current,altitude,latitude,longitude,tas,gps_used,fix_type,satellites_num,roll,yaw,pitch,motor_status,imu_status,gps_status,cgps_used,press_compass_status,f_mode,gps_pos_used,vehicle_type,error_flags1,gps_accH\n";
    }
    
    public String toString()
    {
      return new SimpleDateFormat("yyyyMMdd HH:mm:ss:SSS").format(new Date()) + "," + this.fsk_rssi + "," + this.voltage + "," + this.current + "," + this.altitude + "," + this.latitude + "," + this.longitude + "," + this.tas + "," + this.gps_used + "," + this.fix_type + "," + this.satellites_num + "," + this.roll + "," + this.yaw + "," + this.pitch + "," + this.motor_status + "," + this.imu_status + "," + this.gps_status + "," + this.cgps_status + "," + this.press_compass_status + "," + this.f_mode + "," + this.gps_pos_used + "," + this.vehicle_type + "," + this.error_flags1 + "," + this.gps_accH + "\n";
    }
  }
  
  public static class TrainerReplyOfQuit
    extends UARTInfoMessage.UARTRelyMessage
  {
    public TrainerReplyOfQuit()
    {
      this.what = 1033;
    }
  }
  
  public static class TrainerReplyOfStudent
    extends UARTInfoMessage.UARTRelyMessage
  {
    public TrainerReplyOfStudent()
    {
      this.what = 1032;
    }
  }
  
  public static class TrainerReplyOfTeacher
    extends UARTInfoMessage.UARTRelyMessage
  {
    public TrainerReplyOfTeacher()
    {
      this.what = 1031;
    }
  }
  
  public static class TransmitterState
    extends UARTInfoMessage
  {
    public int status;
  }
  
  public static class Trim
    extends UARTInfoMessage
  {
    public float t1;
    public float t2;
    public float t3;
    public float t4;
  }
  
  public static class TxNeedUpdate
    extends UARTInfoMessage
  {
    public TxNeedUpdate()
    {
      this.what = 19;
    }
  }
  
  public static class UARTRelyMessage
    extends UARTInfoMessage
  {
    public static final int REPLY_AWAIT_INFO = 1061;
    public static final int REPLY_BIND_INFO = 1011;
    public static final int REPLY_CHANNEL_CALIBRATION_INFO = 1021;
    public static final int REPLY_CHANNEL_CURVE_INFO = 1024;
    public static final int REPLY_CHANNEL_MIXING_INFO = 1023;
    public static final int REPLY_CHANNEL_REVERSE_INFO = 1022;
    public static final int REPLY_CHANNEL_TRAVEL_INFO = 1025;
    public static final int REPLY_CHANNEL_TRIM_INFO = 1026;
    public static final int REPLY_EXECUTE_INFO = 1051;
    public static final int REPLY_JOIN_INFO = 1041;
    public static final int REPLY_SAVE_ALL_INFO = 1081;
    public static final int REPLY_SAVE_MIXING_INFO = 1083;
    public static final int REPLY_SAVE_TRAVEL_TRIM_INFO = 1082;
    public static final int REPLY_SIMULATOR_INFO = 1071;
    public static final int REPLY_TRAINER_QUIT_INFO = 1033;
    public static final int REPLY_TRAINER_STUDENT_INFO = 1032;
    public static final int REPLY_TRAINER_TEACHER_INFO = 1031;
    public int id;
    public boolean isRight;
  }
  
  public static class Version
    extends UARTInfoMessage
  {
    public String version;
  }
}


