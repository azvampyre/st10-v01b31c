package com.yuneec.uartcontroller;

public class UartInterface
{
  static
  {
    System.loadLibrary("flycontroljni");
  }
  
  static native int Recv(byte[] paramArrayOfByte, int paramInt);
  
  public static native UARTInfoMessage RecvMsg();
  
  static native int Send(byte[] paramArrayOfByte, int paramInt);
  
  static native void UpdateRfVersionCompleted();
  
  static native void UpdateTxVersionCompleted();
  
  static native boolean acquireTxResourceInfo();
  
  static native boolean bind(int paramInt);
  
  static native void clearRecvBuf();
  
  static native boolean closeDevice();
  
  static native boolean enterBind();
  
  static native boolean enterFactoryCalibration();
  
  static native boolean enterRun();
  
  static native boolean enterSim();
  
  static native boolean enterTestRF(int paramInt1, int paramInt2);
  
  static native boolean enterTransmitTest();
  
  static native boolean exitBind();
  
  static native boolean exitFactoryCalibration();
  
  static native boolean exitRun();
  
  static native boolean exitSim();
  
  static native boolean exitTransmitTest();
  
  static native boolean finishBind();
  
  static native boolean finishCalibration();
  
  static native UARTInfoMessage.CalibrationRawData getCalibrationRawData(boolean paramBoolean);
  
  static native boolean getRfVersion();
  
  static native byte[] getRxResInfo(int paramInt);
  
  static native boolean getSignal(int paramInt);
  
  static native boolean getSubTrim(int[] paramArrayOfInt, boolean paramBoolean);
  
  static native int getTrimStep(boolean paramBoolean);
  
  static native boolean getTxBLVersion();
  
  static native boolean getTxVersion();
  
  static native boolean isOpenDevice();
  
  static native void nativeDestory();
  
  static native void nativeInit();
  
  static native String openDevice();
  
  static native UARTInfoMessage parseMsg(byte[] paramArrayOfByte, int paramInt);
  
  static native boolean queryBindState();
  
  static native boolean querySwitchState(int paramInt);
  
  static native int readTransmitRate();
  
  static native void readyForUpdateRfVersion();
  
  static native void readyForUpdateTxVersion();
  
  static native boolean receiveBothChannel();
  
  static native boolean receiveMixedChannelOnly();
  
  static native boolean receiveRawChannelOnly();
  
  static native boolean sendMissionRequest(int paramInt1, int paramInt2, int paramInt3);
  
  static native boolean sendMissionResponse(int paramInt1, int paramInt2, int paramInt3);
  
  static native boolean sendMissionSettingCccWaypoint(WaypointData paramWaypointData);
  
  static native boolean sendMissionSettingRoiCenter(RoiData paramRoiData);
  
  static native boolean sendRxResInfo(byte[] paramArrayOfByte);
  
  static native boolean setBindKeyFunction(int paramInt);
  
  static native boolean setChannelConfig(int paramInt1, int paramInt2);
  
  static native boolean setFmodeKey(int paramInt);
  
  static native boolean setSubTrim(int[] paramArrayOfInt);
  
  static native boolean setTTBstate(int paramInt, boolean paramBoolean);
  
  static native boolean setTrimStep(int paramInt);
  
  static native boolean shutDown();
  
  static native boolean sonarSwitch(boolean paramBoolean);
  
  static native boolean startBind();
  
  static native boolean startCalibration();
  
  static native boolean syncMixingData(MixedData paramMixedData, int paramInt);
  
  static native boolean syncMixingDataDeleteAll();
  
  public static native boolean testBit();
  
  static native boolean unbind();
  
  static native boolean updateCompass(float paramFloat);
  
  static native boolean updateGPS(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, int paramInt);
  
  static native String updateRfVersion(String paramString);
  
  static native String updateTxVersion(String paramString);
  
  static native boolean writeTransmitRate(int paramInt);
}


