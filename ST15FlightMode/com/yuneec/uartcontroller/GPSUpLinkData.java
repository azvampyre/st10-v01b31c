package com.yuneec.uartcontroller;

import android.location.Location;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GPSUpLinkData
{
  public float accuracy;
  public float alt;
  public float angle;
  public float lat;
  public float lon;
  public int no_satelites;
  public boolean reset = true;
  public float speed;
  
  public static String getParamsName()
  {
    return ",lon,lat,alt,accuracy,speed,angle\n";
  }
  
  public void reset()
  {
    this.lon = 0.0F;
    this.lat = 0.0F;
    this.alt = 0.0F;
    this.accuracy = 0.0F;
    this.speed = 0.0F;
    this.no_satelites = 0;
    this.reset = true;
  }
  
  public void setData(Location paramLocation)
  {
    this.lon = ((float)paramLocation.getLongitude());
    this.lat = ((float)paramLocation.getLatitude());
    this.alt = ((float)paramLocation.getAltitude());
    this.accuracy = Math.abs(paramLocation.getAccuracy());
    this.speed = Math.abs(paramLocation.getSpeed());
    this.reset = false;
  }
  
  public String toString()
  {
    return new SimpleDateFormat("yyyyMMdd HH:mm:ss:SSS").format(new Date()) + "," + this.lon + "," + this.lat + "," + this.alt + "," + this.accuracy * 10.0F + "," + this.speed * 10.0F + "," + this.angle + "\n";
  }
}


