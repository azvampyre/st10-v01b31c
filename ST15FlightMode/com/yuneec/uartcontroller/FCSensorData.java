package com.yuneec.uartcontroller;

public class FCSensorData
{
  public static final int ACCELEROMETER_IN_IMU_INIT_STATUS = 4;
  public static final int ACCELEROMETER_IN_IMU_WARNING_STATUS = 32;
  public static final int COMPASS1_IN_FC0_STATUS = 4;
  public static final int COMPASS2_IN_IMU_STATUS = 8;
  public static final int FC0_AND_IMU_DATA_MISMATCH_WARNING_STATUS = 64;
  public static final int GPS1_IN_FC0_STATUS = 16;
  public static final int GPS2_IN_IMU_STATUS = 32;
  public static final int MPU6050_IN_FC0_INIT_STATUS = 1;
  public static final int MPU6050_IN_FC0_WARNING_STATUS = 8;
  public static final int MPU6050_IN_IMU_INIT_STATUS = 2;
  public static final int MPU6050_IN_IMU_WARNING_STATUS = 16;
  public static final int PRESSURE_IN_FC0_STATUS = 1;
  public static final int PRESSURE_IN_IMU_STATUS = 2;
  public String mError;
  public int mImuStatus;
  public int mPressCompassGpsStatus;
  
  public static String getError(int paramInt1, int paramInt2)
  {
    String str2 = getImuStatusError(paramInt1);
    String str1 = str2;
    if (str2 == null) {
      str1 = getPressCompassGpsStatusError(paramInt2);
    }
    return str1;
  }
  
  public static String getImuStatusError(int paramInt)
  {
    String str = null;
    if ((paramInt & 0x1) == 0) {
      str = "IMU Fail";
    }
    return str;
  }
  
  public static String getPressCompassGpsStatusError(int paramInt)
  {
    String str = null;
    if ((paramInt & 0x4) == 0) {
      str = "Mag Fail";
    }
    for (;;)
    {
      return str;
      if ((paramInt & 0x1) == 0) {
        str = "Pre Fail";
      } else if ((paramInt & 0x10) == 0) {
        str = "GPS Fail";
      }
    }
  }
  
  public void setData(int paramInt1, int paramInt2)
  {
    this.mImuStatus = paramInt1;
    this.mPressCompassGpsStatus = paramInt2;
    this.mError = getError(paramInt1, paramInt2);
  }
}


