package com.yuneec.uartcontroller;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.SparseArray;
import java.util.Iterator;
import java.util.LinkedList;

public class UARTController
{
  public static final int BIND_KEY_FUNCTION_BIND = 1;
  public static final int BIND_KEY_FUNCTION_PWR = 0;
  public static final int STATE_AWAIT = 1;
  public static final int STATE_BIND = 2;
  public static final int STATE_CALIBRATION = 3;
  public static final int STATE_FACTORY_CALI = 7;
  public static final int STATE_RUN = 5;
  public static final int STATE_SIM = 6;
  public static final int SYNC_MIXING_DATA_ADD = 0;
  public static final int SYNC_MIXING_DATA_DELETE = 2;
  public static final int SYNC_MIXING_DATA_DELETE_ALL = 3;
  public static final int SYNC_MIXING_DATA_UPDATE = 1;
  private static final String TAG = "UARTController";
  private static final int TIMEOUT = 300;
  public static final int TTS_START_INDEX = 19;
  private static final int TX_STATE_UPDATE_INTERVAL = 500;
  private static final int TX_STATE_UPDATE_TIMEOUT = 5000;
  private static UARTController sInstance;
  private static String sOccupiedprocess;
  private ChangeState AwaitState = new ChangeState()
  {
    public boolean enterState(boolean paramAnonymousBoolean)
    {
      return true;
    }
    
    public boolean exitState(boolean paramAnonymousBoolean)
    {
      return true;
    }
  };
  private ChangeState BindState = new ChangeState()
  {
    public boolean enterState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.enterBind(paramAnonymousBoolean);
    }
    
    public boolean exitState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.exitBind(paramAnonymousBoolean);
    }
  };
  private ChangeState CalibrationState = new ChangeState()
  {
    public boolean enterState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.startCalibration(paramAnonymousBoolean);
    }
    
    public boolean exitState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.finishCalibration(paramAnonymousBoolean);
    }
  };
  private ChangeState FactoryCalibrationState = new ChangeState()
  {
    public boolean enterState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.enterFactoryCalibration(paramAnonymousBoolean);
    }
    
    public boolean exitState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.exitFactoryCalibration(paramAnonymousBoolean);
    }
  };
  private ChangeState RunState = new ChangeState()
  {
    public boolean enterState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.enterRun(paramAnonymousBoolean);
    }
    
    public boolean exitState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.exitRun(paramAnonymousBoolean);
    }
  };
  private ChangeState SimState = new ChangeState()
  {
    public boolean enterState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.enterSim(paramAnonymousBoolean);
    }
    
    public boolean exitState(boolean paramAnonymousBoolean)
    {
      return UARTController.this.exitSim(paramAnonymousBoolean);
    }
  };
  private UartCommand acquireTxResourceInfoCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.acquireTxResourceInfo();
    }
  };
  private UartCommand bindCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.bind(((Integer)paramAnonymousVarArgs[0]).intValue());
    }
  };
  private UartCommand enterBindCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.enterBind();
    }
  };
  private UartCommand enterFactoryCalibrationCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.enterFactoryCalibration();
    }
  };
  private UartCommand enterRunCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.enterRun();
    }
  };
  private UartCommand enterSimCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.enterSim();
    }
  };
  private UartCommand enterTransmitTestCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.enterTransmitTest();
    }
  };
  private UartCommand exitBindCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.exitBind();
    }
  };
  private UartCommand exitFactoryCalibrationCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.exitFactoryCalibration();
    }
  };
  private UartCommand exitRunCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.exitRun();
    }
  };
  private UartCommand exitSimCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.exitSim();
    }
  };
  private UartCommand exitTransmitTestCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.exitTransmitTest();
    }
  };
  private UartCommand finishBindCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.finishBind();
    }
  };
  private UartCommand finishCalibrationCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.finishCalibration();
    }
  };
  private UartCommand getSignalCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.getSignal(((Integer)paramAnonymousVarArgs[0]).intValue());
    }
  };
  private ReaderThread mReader;
  private Handler mReaderHandler;
  private final SparseArray<ChangeState> mStateChangeAction = new SparseArray();
  private StateMachine mStateMachine = new StateMachine(null);
  private Object mStateMachineLock = new Object();
  private UartCommand missionRequestCommand = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object[] arg2)
    {
      int j = 1;
      int k = ((Integer)???[0]).intValue();
      int m = ((Integer)???[1]).intValue();
      int n = ((Integer)???[2]).intValue();
      for (;;)
      {
        int i;
        synchronized (UARTController.this.responseMissionQueue)
        {
          UARTController.this.responseMissionQueue.clear();
          i = 0;
          i++;
          if (i <= 15) {}
        }
        try
        {
          for (;;)
          {
            synchronized (UARTController.this.responseMissionQueue)
            {
              UARTController.this.responseMissionQueue.wait(100L);
              Iterator localIterator = UARTController.this.responseMissionQueue.iterator();
              if (!localIterator.hasNext())
              {
                i = j;
                UARTController.this.responseMissionQueue.clear();
                if (i != 0) {
                  break label254;
                }
                paramAnonymousBoolean = true;
                return paramAnonymousBoolean;
                ??? = finally;
                throw ???;
                UartInterface.sendMissionRequest(k, m, n);
                try
                {
                  Thread.sleep(5L);
                }
                catch (InterruptedException ???)
                {
                  ???.printStackTrace();
                }
                break;
              }
              ??? = (byte[])localIterator.next();
              if (???.length <= 3) {
                continue;
              }
              StringBuilder localStringBuilder = new java/lang/StringBuilder;
              localStringBuilder.<init>("missionRequest, receive: ");
              Log.i("UARTController", UARTController.byteArrayToString((byte[])???));
              if ((???[0] & 0xFF) != 1) {
                continue;
              }
              i = ???[1] & 0xFF;
            }
            label254:
            paramAnonymousBoolean = false;
          }
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;) {}
        }
      }
    }
  };
  private UartCommand missionRequestGetWaypoint = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... arg2)
    {
      WaypointData localWaypointData = (WaypointData)???[0];
      int i;
      synchronized (UARTController.this.responseMissionQueue)
      {
        UARTController.this.responseMissionQueue.clear();
        i = 0;
        i++;
        if (i > 15) {
          ??? = null;
        }
      }
      for (;;)
      {
        Iterator localIterator;
        synchronized (UARTController.this.responseMissionQueue)
        {
          try
          {
            UARTController.this.responseMissionQueue.wait(100L);
            localIterator = UARTController.this.responseMissionQueue.iterator();
            if (!localIterator.hasNext())
            {
              UARTController.this.responseMissionQueue.clear();
              if (??? != null) {
                break label197;
              }
              paramAnonymousBoolean = false;
              return paramAnonymousBoolean;
              localObject = finally;
              throw ((Throwable)localObject);
              UartInterface.sendMissionRequest(5, 3, 0);
              try
              {
                Thread.sleep(5L);
              }
              catch (InterruptedException ???)
              {
                ???.printStackTrace();
              }
            }
          }
          catch (InterruptedException localInterruptedException)
          {
            localInterruptedException.printStackTrace();
            continue;
          }
        }
        byte[] arrayOfByte = (byte[])localIterator.next();
        if (arrayOfByte.length > 24)
        {
          i = arrayOfByte[1];
          if ((i & 0xFF) == 0)
          {
            ??? = arrayOfByte;
            continue;
            label197:
            localWaypointData.pointerIndex = (???[2] & 0xFF);
            localWaypointData.latitude = ((float)LittleEndianUtil.getUInt(???, 3) * 1.0E-7F);
            localWaypointData.longitude = ((float)LittleEndianUtil.getUInt(???, 7) * 1.0E-7F);
            localWaypointData.altitude = ((float)LittleEndianUtil.getUInt(???, 11) * 0.01F);
            localWaypointData.roll = (LittleEndianUtil.getUShort(???, 15) * 0.01F);
            localWaypointData.pitch = (LittleEndianUtil.getUShort(???, 17) * 0.01F);
            localWaypointData.yaw = (LittleEndianUtil.getUShort(???, 19) * 0.01F);
            localWaypointData.gimbalPitch = (LittleEndianUtil.getUShort(???, 21) * 0.01F);
            localWaypointData.gimbalYam = (LittleEndianUtil.getUShort(???, 23) * 0.01F);
            paramAnonymousBoolean = true;
          }
        }
      }
    }
  };
  private UartCommand missionSettingCccCommand = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object[] arg2)
    {
      paramAnonymousBoolean = true;
      int j = 1;
      ??? = (WaypointData)???[0];
      int i;
      synchronized (UARTController.this.responseMissionQueue)
      {
        UARTController.this.responseMissionQueue.clear();
        i = 0;
        i++;
        if (i <= 15) {}
      }
      for (;;)
      {
        Iterator localIterator;
        synchronized (UARTController.this.responseMissionQueue)
        {
          try
          {
            UARTController.this.responseMissionQueue.wait(100L);
            localIterator = UARTController.this.responseMissionQueue.iterator();
            if (!localIterator.hasNext())
            {
              i = j;
              UARTController.this.responseMissionQueue.clear();
              if (i != 0) {
                break label199;
              }
              return paramAnonymousBoolean;
              ??? = finally;
              throw ???;
              UartInterface.sendMissionSettingCccWaypoint(???);
              try
              {
                Thread.sleep(5L);
              }
              catch (InterruptedException localInterruptedException1)
              {
                localInterruptedException1.printStackTrace();
              }
            }
          }
          catch (InterruptedException localInterruptedException2)
          {
            localInterruptedException2.printStackTrace();
            continue;
          }
        }
        byte[] arrayOfByte = (byte[])localIterator.next();
        if ((arrayOfByte != null) && (arrayOfByte.length > 3))
        {
          i = arrayOfByte[1];
          i &= 0xFF;
          continue;
          label199:
          paramAnonymousBoolean = false;
        }
      }
    }
  };
  private UartCommand missionSettingRoiCenterCommand = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object[] arg2)
    {
      paramAnonymousBoolean = true;
      int j = 1;
      ??? = (RoiData)???[0];
      int i;
      synchronized (UARTController.this.responseMissionQueue)
      {
        UARTController.this.responseMissionQueue.clear();
        i = 0;
        i++;
        if (i <= 15) {}
      }
      for (;;)
      {
        synchronized (UARTController.this.responseMissionQueue)
        {
          try
          {
            UARTController.this.responseMissionQueue.wait(100L);
            ??? = UARTController.this.responseMissionQueue.iterator();
            if (!((Iterator)???).hasNext())
            {
              i = j;
              UARTController.this.responseMissionQueue.clear();
              if (i != 0) {
                break label199;
              }
              return paramAnonymousBoolean;
              ??? = finally;
              throw ???;
              UartInterface.sendMissionSettingRoiCenter(???);
              try
              {
                Thread.sleep(5L);
              }
              catch (InterruptedException localInterruptedException1)
              {
                localInterruptedException1.printStackTrace();
              }
            }
          }
          catch (InterruptedException localInterruptedException2)
          {
            localInterruptedException2.printStackTrace();
            continue;
          }
        }
        byte[] arrayOfByte = (byte[])((Iterator)localObject2).next();
        if ((arrayOfByte != null) && (arrayOfByte.length > 3))
        {
          i = arrayOfByte[1];
          i &= 0xFF;
          continue;
          label199:
          paramAnonymousBoolean = false;
        }
      }
    }
  };
  private UartCommand receiveBothChannelCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.receiveBothChannel();
    }
  };
  private UartCommand receiveMixedChannelOnlyCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.receiveMixedChannelOnly();
    }
  };
  private UartCommand receiveRawChannelOnlyCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.receiveRawChannelOnly();
    }
  };
  private volatile LinkedList<byte[]> responseMissionQueue = new LinkedList();
  private volatile int responseOfBindState = 0;
  private volatile String responseOfRadioVersion = null;
  private volatile UARTInfoMessage.SwitchState responseOfSwitchState = null;
  private volatile String responseOfTxBLVersion = null;
  private volatile String responseOfTxVersion = null;
  private volatile boolean responseReached = false;
  private UartCommand setBindKeyFunctionCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.setBindKeyFunction(((Integer)paramAnonymousVarArgs[0]).intValue());
    }
  };
  private UartCommand setChannelConfigCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.setChannelConfig(((Integer)paramAnonymousVarArgs[0]).intValue(), ((Integer)paramAnonymousVarArgs[1]).intValue());
    }
  };
  private UartCommand setFmodeKeyCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.setFmodeKey(((Integer)paramAnonymousVarArgs[0]).intValue());
    }
  };
  private UartCommand setSubTrimCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.setSubTrim((int[])paramAnonymousVarArgs[0]);
    }
  };
  private UartCommand setTTBStateCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.setTTBstate(((Integer)paramAnonymousVarArgs[0]).intValue(), ((Boolean)paramAnonymousVarArgs[1]).booleanValue());
    }
  };
  private UartCommand setTrimStepCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.setTrimStep(((Integer)paramAnonymousVarArgs[0]).intValue());
    }
  };
  private UartCommand shutDownCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.shutDown();
    }
  };
  private UartCommand sonarSwitchCommand = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object[] paramAnonymousArrayOfObject)
    {
      paramAnonymousBoolean = ((Boolean)paramAnonymousArrayOfObject[0]).booleanValue();
      int i = 0;
      for (;;)
      {
        i++;
        if (i > 15) {
          return false;
        }
        UartInterface.sonarSwitch(paramAnonymousBoolean);
        try
        {
          Thread.sleep(5L);
        }
        catch (InterruptedException paramAnonymousArrayOfObject)
        {
          paramAnonymousArrayOfObject.printStackTrace();
        }
      }
    }
  };
  private UartCommand startBindCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.startBind();
    }
  };
  private UartCommand startCalibrationCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.startCalibration();
    }
  };
  private UartCommand syncMixingDataCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.syncMixingData((MixedData)paramAnonymousVarArgs[0], ((Integer)paramAnonymousVarArgs[1]).intValue());
    }
  };
  private UartCommand syncMixingDataDeleteAllCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.syncMixingDataDeleteAll();
    }
  };
  private UartCommand unBindCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.unbind();
    }
  };
  private UartCommand updateCompassCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.updateCompass(((Float)paramAnonymousVarArgs[0]).floatValue());
    }
  };
  private UartCommand updateGpsCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.updateGPS(((Float)paramAnonymousVarArgs[0]).floatValue(), ((Float)paramAnonymousVarArgs[1]).floatValue(), ((Float)paramAnonymousVarArgs[2]).floatValue(), ((Float)paramAnonymousVarArgs[3]).floatValue(), ((Float)paramAnonymousVarArgs[4]).floatValue(), ((Float)paramAnonymousVarArgs[5]).floatValue(), ((Integer)paramAnonymousVarArgs[6]).intValue());
    }
  };
  private UartCommand writeTransmitRateCmd = new UartCommand()
  {
    public boolean execute(boolean paramAnonymousBoolean, Object... paramAnonymousVarArgs)
    {
      return UartInterface.writeTransmitRate(((Integer)paramAnonymousVarArgs[0]).intValue());
    }
  };
  
  private UARTController()
  {
    initStateChangeActionMaps();
    UartInterface.nativeInit();
  }
  
  public static String byteArrayToString(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = paramArrayOfByte.length;
    for (int i = 0;; i++)
    {
      if (i >= j) {
        return localStringBuilder.toString();
      }
      localStringBuilder.append(String.format(" %02x", new Object[] { Byte.valueOf(paramArrayOfByte[i]) }));
    }
  }
  
  private static boolean ensureUartInterface()
  {
    String str = UartInterface.openDevice();
    if ("OK".equals(str)) {}
    for (boolean bool = true;; bool = false)
    {
      return bool;
      if (!"FAIL".equals(str))
      {
        Log.i("UARTController", "occupied process " + str);
        sOccupiedprocess = str;
      }
    }
  }
  
  