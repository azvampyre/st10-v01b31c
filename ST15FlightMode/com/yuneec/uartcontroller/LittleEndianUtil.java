package com.yuneec.uartcontroller;

import java.io.IOException;
import java.io.InputStream;

public class LittleEndianUtil
{
  public static final int INT_SIZE = 4;
  public static final int LONG_SIZE = 8;
  public static final int SHORT_SIZE = 2;
  
  public static byte[] getByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = new byte[paramInt2];
    System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
  
  public static double getDouble(byte[] paramArrayOfByte, int paramInt)
  {
    return Double.longBitsToDouble(getLong(paramArrayOfByte, paramInt));
  }
  
  public static int getInt(byte[] paramArrayOfByte)
  {
    return getInt(paramArrayOfByte, 0);
  }
  
  public static int getInt(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramInt + 1;
    paramInt = paramArrayOfByte[paramInt];
    int j = i + 1;
    int k = paramArrayOfByte[i];
    i = j + 1;
    j = paramArrayOfByte[j];
    return ((paramArrayOfByte[i] & 0xFF) << 24) + ((j & 0xFF) << 16) + ((k & 0xFF) << 8) + ((paramInt & 0xFF) << 0);
  }
  
  public static long getLong(byte[] paramArrayOfByte, int paramInt)
  {
    long l = 0L;
    for (int i = paramInt + 8 - 1;; i--)
    {
      if (i < paramInt) {
        return l;
      }
      l = l << 8 | paramArrayOfByte[i] & 0xFF;
    }
  }
  
  public static short getShort(byte[] paramArrayOfByte)
  {
    return getShort(paramArrayOfByte, 0);
  }
  
  public static short getShort(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramArrayOfByte[paramInt];
    return (short)(((paramArrayOfByte[(paramInt + 1)] & 0xFF) << 8) + ((i & 0xFF) << 0));
  }
  
  public static long getUInt(byte[] paramArrayOfByte)
  {
    return getUInt(paramArrayOfByte, 0);
  }
  
  public static long getUInt(byte[] paramArrayOfByte, int paramInt)
  {
    return 0xFFFFFFFFFFFFFFFF & getInt(paramArrayOfByte, paramInt);
  }
  
  public static int getUShort(byte[] paramArrayOfByte)
  {
    return getUShort(paramArrayOfByte, 0);
  }
  
  public static int getUShort(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramArrayOfByte[paramInt];
    return ((paramArrayOfByte[(paramInt + 1)] & 0xFF) << 8) + ((i & 0xFF) << 0);
  }
  
  public static int getUnsignedByte(byte[] paramArrayOfByte, int paramInt)
  {
    return paramArrayOfByte[paramInt] & 0xFF;
  }
  
  public static void putByte(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[paramInt1] = ((byte)paramInt2);
  }
  
  public static void putDouble(byte[] paramArrayOfByte, int paramInt, double paramDouble)
  {
    putLong(paramArrayOfByte, paramInt, Double.doubleToLongBits(paramDouble));
  }
  
  public static void putInt(byte[] paramArrayOfByte, int paramInt)
  {
    putInt(paramArrayOfByte, 0, paramInt);
  }
  
  public static void putInt(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1 + 1;
    paramArrayOfByte[paramInt1] = ((byte)(paramInt2 >>> 0 & 0xFF));
    paramInt1 = i + 1;
    paramArrayOfByte[i] = ((byte)(paramInt2 >>> 8 & 0xFF));
    i = paramInt1 + 1;
    paramArrayOfByte[paramInt1] = ((byte)(paramInt2 >>> 16 & 0xFF));
    paramArrayOfByte[i] = ((byte)(paramInt2 >>> 24 & 0xFF));
  }
  
  public static void putLong(byte[] paramArrayOfByte, int paramInt, long paramLong)
  {
    for (int i = paramInt;; i++)
    {
      if (i >= paramInt + 8) {
        return;
      }
      paramArrayOfByte[i] = ((byte)(int)(0xFF & paramLong));
      paramLong >>= 8;
    }
  }
  
  public static void putShort(byte[] paramArrayOfByte, int paramInt, short paramShort)
  {
    int i = paramInt + 1;
    paramArrayOfByte[paramInt] = ((byte)(paramShort >>> 0 & 0xFF));
    paramArrayOfByte[i] = ((byte)(paramShort >>> 8 & 0xFF));
  }
  
  public static void putShort(byte[] paramArrayOfByte, short paramShort)
  {
    putShort(paramArrayOfByte, 0, paramShort);
  }
  
  public static void putUShort(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1 + 1;
    paramArrayOfByte[paramInt1] = ((byte)(paramInt2 >>> 0 & 0xFF));
    paramArrayOfByte[i] = ((byte)(paramInt2 >>> 8 & 0xFF));
  }
  
  public static int readInt(InputStream paramInputStream)
    throws IOException, LittleEndianUtil.BufferUnderrunException
  {
    int k = paramInputStream.read();
    int j = paramInputStream.read();
    int i = paramInputStream.read();
    int m = paramInputStream.read();
    if ((k | j | i | m) < 0) {
      throw new BufferUnderrunException();
    }
    return (m << 24) + (i << 16) + (j << 8) + (k << 0);
  }
  
  public static long readLong(InputStream paramInputStream)
    throws IOException, LittleEndianUtil.BufferUnderrunException
  {
    int i3 = paramInputStream.read();
    int k = paramInputStream.read();
    int m = paramInputStream.read();
    int j = paramInputStream.read();
    int i2 = paramInputStream.read();
    int n = paramInputStream.read();
    int i = paramInputStream.read();
    int i1 = paramInputStream.read();
    if ((i3 | k | m | j | i2 | n | i | i1) < 0) {
      throw new BufferUnderrunException();
    }
    return (i1 << 56) + (i << 48) + (n << 40) + (i2 << 32) + (j << 24) + (m << 16) + (k << 8) + (i3 << 0);
  }
  
  public static short readShort(InputStream paramInputStream)
    throws IOException, LittleEndianUtil.BufferUnderrunException
  {
    return (short)readUShort(paramInputStream);
  }
  
  public static int readUShort(InputStream paramInputStream)
    throws IOException, LittleEndianUtil.BufferUnderrunException
  {
    int j = paramInputStream.read();
    int i = paramInputStream.read();
    if ((j | i) < 0) {
      throw new BufferUnderrunException();
    }
    return (i << 8) + (j << 0);
  }
  
  public static int ubyteToInt(byte paramByte)
  {
    return paramByte & 0xFF;
  }
  
  public static final class BufferUnderrunException
    extends IOException
  {
    BufferUnderrunException()
    {
      super();
    }
  }
}


