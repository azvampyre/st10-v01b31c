package com.yuneec.uartcontroller.test;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.yuneec.uartcontroller.GPSUpLinkData;
import com.yuneec.uartcontroller.MixedData;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.CalibrationState;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.uartcontroller.UARTInfoMessage.RxBindInfo;
import com.yuneec.uartcontroller.UARTInfoMessage.SwitchChanged;
import com.yuneec.uartcontroller.UARTInfoMessage.TransmitterState;
import com.yuneec.uartcontroller.UARTInfoMessage.Trim;
import com.yuneec.uartcontroller15.R.id;
import com.yuneec.uartcontroller15.R.layout;
import java.util.ArrayList;

public class TestActivity
  extends Activity
  implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener
{
  protected static final String TAG = "TestActivity";
  private ArrayAdapter<String> mAdapter;
  private Button mBtnCh1;
  private Button mBtnCh2;
  private Button mBtnCh3;
  private Button mBtnCh4;
  private ToggleButton mCalibration;
  private SeekBar mCh1;
  private SeekBar mCh2;
  private SeekBar mCh3;
  private SeekBar mCh4;
  private SeekBar mCh5;
  private SeekBar mCh6;
  private SeekBar mCh7;
  private SeekBar mCh8;
  private UARTController mController;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
      {
        paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        switch (paramAnonymousMessage.what)
        {
        }
      }
      for (;;)
      {
        return;
        Log.d("TestActivity", "UARTInfoMessage.CHANNEL_INFO");
        paramAnonymousMessage = (UARTInfoMessage.Channel)paramAnonymousMessage;
        if (paramAnonymousMessage.channels.size() >= 4)
        {
          float f = ((Float)paramAnonymousMessage.channels.get(0)).floatValue();
          TestActivity.this.mCh1.setProgress((int)f);
          TestActivity.this.mBtnCh1.setText(String.valueOf(f));
          f = ((Float)paramAnonymousMessage.channels.get(1)).floatValue();
          TestActivity.this.mBtnCh2.setText(String.valueOf(f));
          TestActivity.this.mCh2.setProgress((int)f);
          f = ((Float)paramAnonymousMessage.channels.get(2)).floatValue();
          TestActivity.this.mBtnCh3.setText(String.valueOf(f));
          TestActivity.this.mCh3.setProgress((int)f);
          f = ((Float)paramAnonymousMessage.channels.get(3)).floatValue();
          TestActivity.this.mBtnCh4.setText(String.valueOf(f));
          TestActivity.this.mCh4.setProgress((int)f);
          continue;
          paramAnonymousMessage = (UARTInfoMessage.RxBindInfo)paramAnonymousMessage;
          Log.i("TestActivity", "rx info:" + paramAnonymousMessage.node_id);
          TestActivity.this.mAdapter.add(String.valueOf(paramAnonymousMessage.node_id));
          TestActivity.this.refreshList();
          continue;
          paramAnonymousMessage = (UARTInfoMessage.Trim)paramAnonymousMessage;
          Log.v("TestActivity", "TRIM:" + paramAnonymousMessage.t1 + " " + paramAnonymousMessage.t2 + " " + paramAnonymousMessage.t3 + " " + paramAnonymousMessage.t4);
          continue;
          Toast.makeText(TestActivity.this, "Bind success", 0).show();
          continue;
          paramAnonymousMessage = (UARTInfoMessage.TransmitterState)paramAnonymousMessage;
          Log.d("TestActivity", "TRANSMITTER_INFO:" + paramAnonymousMessage.status);
          continue;
          paramAnonymousMessage = (UARTInfoMessage.SwitchChanged)paramAnonymousMessage;
          Log.d("TestActivity", "switch changed :" + paramAnonymousMessage.hw_id + " " + paramAnonymousMessage.old_state + " " + paramAnonymousMessage.new_state);
          continue;
          paramAnonymousMessage = (UARTInfoMessage.CalibrationState)paramAnonymousMessage;
          Log.d("TestActivity", "csi :" + paramAnonymousMessage.hardware_state.size() + ",");
          continue;
          Log.d("TestActivity", "tx need update");
        }
      }
    }
  };
  private ListView mListView;
  private ToggleButton mRun;
  private ArrayList<String> mRxList = new ArrayList();
  private int test_addr = 1001;
  private boolean toggle = false;
  private UARTInfoMessage umsg;
  
  private void refreshList()
  {
    this.mListView.invalidateViews();
  }
  
  public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    if (paramCompoundButton.equals(this.mCalibration)) {
      if (paramBoolean) {
        this.mController.startCalibration(true);
      }
    }
    for (;;)
    {
      return;
      this.mController.finishCalibration(true);
      continue;
      if (paramCompoundButton.equals(this.mRun)) {
        if (paramBoolean) {
          this.mController.enterRun(true);
        } else {
          this.mController.exitRun(true);
        }
      }
    }
  }
  
  public void onClick(View paramView)
  {
    if (paramView.equals(findViewById(R.id.bind)))
    {
      this.mController.enterBind(false);
      this.mController.startBind(false);
    }
    for (;;)
    {
      return;
      if (paramView.equals(findViewById(R.id.read)))
      {
        this.mController.startReading();
        this.mController.registerReaderHandler(this.mHandler);
        paramView.setEnabled(false);
      }
      else if (!paramView.equals(findViewById(R.id.bindAModel)))
      {
        if (paramView.equals(findViewById(R.id.finishBind)))
        {
          this.mController.finishBind(false);
          this.mController.exitBind(false);
        }
        else if (paramView.equals(findViewById(R.id.testBit)))
        {
          this.mController.updateRfVersion(Environment.getExternalStorageDirectory() + "/firmware/testaes.bin");
        }
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_main);
    findViewById(R.id.bind).setOnClickListener(this);
    findViewById(R.id.read).setOnClickListener(this);
    findViewById(R.id.bindAModel).setOnClickListener(this);
    findViewById(R.id.finishBind).setOnClickListener(this);
    findViewById(R.id.testBit).setOnClickListener(this);
    this.mCh1 = ((SeekBar)findViewById(R.id.seekBar1));
    this.mCh2 = ((SeekBar)findViewById(R.id.seekBar2));
    this.mCh3 = ((SeekBar)findViewById(R.id.seekBar3));
    this.mCh4 = ((SeekBar)findViewById(R.id.seekBar4));
    this.mCh5 = ((SeekBar)findViewById(R.id.seekBar5));
    this.mCh6 = ((SeekBar)findViewById(R.id.seekBar6));
    this.mCh7 = ((SeekBar)findViewById(R.id.seekBar7));
    this.mCh8 = ((SeekBar)findViewById(R.id.seekBar8));
    this.mBtnCh1 = ((Button)findViewById(R.id.button1));
    this.mBtnCh2 = ((Button)findViewById(R.id.button2));
    this.mBtnCh3 = ((Button)findViewById(R.id.button3));
    this.mBtnCh4 = ((Button)findViewById(R.id.button4));
    this.mCh1.setMax(4096);
    this.mCh2.setMax(4096);
    this.mCh3.setMax(4096);
    this.mCh4.setMax(4096);
    this.mCh5.setMax(4096);
    this.mCh6.setMax(4096);
    this.mCh7.setMax(4096);
    this.mCh8.setMax(4096);
    this.mCalibration = ((ToggleButton)findViewById(R.id.buttonStartCali));
    this.mCalibration.setOnCheckedChangeListener(this);
    this.mRun = ((ToggleButton)findViewById(R.id.tog_run));
    this.mRun.setOnCheckedChangeListener(this);
    paramBundle = new TextView(this);
    paramBundle.setTextSize(20.0F);
    paramBundle.setText("No Rx Founds");
    this.mListView = ((ListView)findViewById(R.id.listView1));
    this.mListView.setEmptyView(paramBundle);
    this.mAdapter = new ArrayAdapter(this, 17367062, this.mRxList);
    this.mListView.setAdapter(this.mAdapter);
    this.mListView.setOnItemClickListener(this);
    this.mController = UARTController.getInstance();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.mController.stopReading();
    this.mController.destory();
    this.mController = null;
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    paramAdapterView = (String)paramAdapterView.getItemAtPosition(paramInt);
    try
    {
      paramInt = Integer.parseInt(paramAdapterView);
      this.mController.bind(false, paramInt);
      return;
    }
    catch (NumberFormatException paramAdapterView)
    {
      for (;;) {}
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 7) {
      this.mController.enterBind(true);
    }
    for (;;)
    {
      return super.onKeyDown(paramInt, paramKeyEvent);
      if (paramInt == 8)
      {
        this.mController.exitBind(true);
        continue;
      }
      if (paramInt == 9)
      {
        this.mController.enterRun(true);
        continue;
      }
      if (paramInt == 10)
      {
        this.mController.exitRun(true);
        continue;
      }
      if (paramInt == 11)
      {
        this.mController.enterSim(true);
        continue;
      }
      if (paramInt == 12)
      {
        this.mController.exitSim(true);
        continue;
      }
      Object localObject;
      if (paramInt == 13)
      {
        this.mController.syncMixingDataDeleteAll(false);
        localObject = new MixedData();
        ((MixedData)localObject).mChannel = 1;
        ((MixedData)localObject).mFmode = 0;
        ((MixedData)localObject).mhardware = 1;
        ((MixedData)localObject).mHardwareType = 2;
        ((MixedData)localObject).mMixedType = 3;
        ((MixedData)localObject).mPriority = 4;
        ((MixedData)localObject).mReverse = true;
        ((MixedData)localObject).mSpeed = 5;
        ((MixedData)localObject).mCurvePoint.add(Integer.valueOf(1));
        ((MixedData)localObject).mCurvePoint.add(Integer.valueOf(2));
        ((MixedData)localObject).mCurvePoint.add(Integer.valueOf(3));
        ((MixedData)localObject).mCurvePoint.add(Integer.valueOf(4));
        ((MixedData)localObject).mSwitchStatus.add(Boolean.valueOf(true));
        ((MixedData)localObject).mSwitchStatus.add(Boolean.valueOf(false));
        ((MixedData)localObject).mSwitchStatus.add(Boolean.valueOf(true));
        ((MixedData)localObject).mSwitchValue.add(Integer.valueOf(10));
        ((MixedData)localObject).mSwitchValue.add(Integer.valueOf(11));
        ((MixedData)localObject).mSwitchValue.add(Integer.valueOf(-12));
        this.mController.syncMixingData(false, (MixedData)localObject, 0);
        continue;
      }
      if (paramInt == 14)
      {
        this.mController.unbind(false);
        continue;
      }
      int i;
      if (paramInt == 15)
      {
        i = this.mController.queryBindState();
        Log.e("TestActivity", "rx state " + i);
        continue;
      }
      if (paramInt == 16)
      {
        localObject = (String)this.mListView.getSelectedItem();
        continue;
      }
      if (paramInt == 27)
      {
        Log.i("TestActivity", "camera state:" + paramKeyEvent.getAction());
        continue;
      }
      if (paramInt == 80)
      {
        Log.i("TestActivity", "video state:" + paramKeyEvent.getAction());
        continue;
      }
      if (paramInt == 29)
      {
        this.mController.startReading();
        this.mController.registerReaderHandler(this.mHandler);
        continue;
      }
      if (paramInt == 30)
      {
        this.mController.correctTxState(1, 3);
        continue;
      }
      if (paramInt == 31)
      {
        this.mController.correctTxState(2, 3);
        continue;
      }
      if (paramInt == 32)
      {
        this.mController.correctTxState(5, 3);
        continue;
      }
      if (paramInt == 33)
      {
        this.mController.correctTxState(6, 3);
        continue;
      }
      if (paramInt == 34)
      {
        this.mController.receiveRawChannelOnly(false);
        continue;
      }
      if (paramInt == 35)
      {
        this.mController.receiveMixedChannelOnly(true);
        continue;
      }
      if (paramInt == 36)
      {
        i = this.mController.querySwitchState(17);
        Log.d("TestActivity", "switch state:" + i);
        continue;
      }
      if (paramInt == 37)
      {
        Log.i("TestActivity", this.mController.getTransmitterVersion());
        Log.i("TestActivity", this.mController.getRadioVersion());
        continue;
      }
      if (paramInt == 38)
      {
        if (this.toggle)
        {
          this.mController.setTTBState(false, 0, true);
          this.mController.setTTBState(false, 1, true);
          this.mController.setTTBState(false, 2, true);
          this.mController.setTTBState(false, 3, true);
          this.mController.setTTBState(false, 4, true);
          this.mController.setTTBState(false, 5, true);
          label778:
          if (!this.toggle) {
            break label866;
          }
        }
        label866:
        for (boolean bool = false;; bool = true)
        {
          this.toggle = bool;
          break;
          this.mController.setTTBState(false, 0, false);
          this.mController.setTTBState(false, 1, false);
          this.mController.setTTBState(false, 2, false);
          this.mController.setTTBState(false, 3, false);
          this.mController.setTTBState(false, 4, false);
          this.mController.setTTBState(false, 5, false);
          break label778;
        }
      }
      if (paramInt == 39)
      {
        this.mController.setChannelConfig(false, 4, 4);
        continue;
      }
      if (paramInt == 40)
      {
        localObject = new GPSUpLinkData();
        ((GPSUpLinkData)localObject).accuracy = 10.434F;
        ((GPSUpLinkData)localObject).alt = 45.556F;
        ((GPSUpLinkData)localObject).lat = 31.43213F;
        ((GPSUpLinkData)localObject).lon = 131.14342F;
        ((GPSUpLinkData)localObject).angle = 82.4F;
        ((GPSUpLinkData)localObject).no_satelites = 9;
        ((GPSUpLinkData)localObject).speed = 10.3F;
        this.mController.updateGps(false, (GPSUpLinkData)localObject);
      }
      try
      {
        Thread.sleep(30L);
        this.mController.updateCompass(false, 45.87F);
        continue;
        if (paramInt == 41)
        {
          this.mController.updateTxVersion(Environment.getExternalStorageDirectory() + "/firmware/st24_mcu_iap.bin");
          continue;
        }
        if (paramInt == 42)
        {
          this.mController.getTrimStep();
          continue;
        }
        if (paramInt == 43)
        {
          localObject = new int[10];
          Object tmp1064_1062 = localObject;
          tmp1064_1062[0] = -2;
          Object tmp1069_1064 = tmp1064_1062;
          tmp1069_1064[1] = -2;
          Object tmp1074_1069 = tmp1069_1064;
          tmp1074_1069[2] = -3;
          Object tmp1079_1074 = tmp1074_1069;
          tmp1079_1074[3] = -4;
          Object tmp1084_1079 = tmp1079_1074;
          tmp1084_1079[4] = -5;
          Object tmp1089_1084 = tmp1084_1079;
          tmp1089_1084[5] = -6;
          Object tmp1094_1089 = tmp1089_1084;
          tmp1094_1089[6] = -7;
          Object tmp1100_1094 = tmp1094_1089;
          tmp1100_1094[7] = -8;
          Object tmp1106_1100 = tmp1100_1094;
          tmp1106_1100[8] = -8;
          Object tmp1112_1106 = tmp1106_1100;
          tmp1112_1106[9] = -10;
          tmp1112_1106;
          this.mController.setSubTrim(false, (int[])localObject);
          this.mController.getSubTrim((int[])localObject);
          continue;
        }
        if (paramInt == 44)
        {
          this.mController.enterFactoryCalibration(true);
          continue;
        }
        if (paramInt == 45)
        {
          this.mController.exitFactoryCalibration(true);
          continue;
        }
        if (paramInt == 46)
        {
          this.mController.updateTxVersion(Environment.getExternalStorageDirectory() + "/firmware/st24_mcu_iap.bin");
          continue;
        }
        if (paramInt == 47)
        {
          this.mController.updateRfVersion(Environment.getExternalStorageDirectory() + "/firmware/testaes.bin");
          continue;
        }
        if (paramInt == 48)
        {
          localObject = this.mController.getCalibrationRawData(true);
          if (localObject == null) {
            continue;
          }
          Log.i("TestActivity", "info:" + localObject.toString());
          continue;
        }
        if (paramInt != 49) {
          continue;
        }
        this.mController = UARTController.getInstance();
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
  }
  
  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27) {
      Log.i("TestActivity", "camera state: long press");
    }
    for (;;)
    {
      return super.onKeyLongPress(paramInt, paramKeyEvent);
      if (paramInt == 80) {
        Log.i("TestActivity", "video state: long press");
      }
    }
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27) {
      Log.i("TestActivity", "camera state:" + paramKeyEvent.getAction());
    }
    for (;;)
    {
      return super.onKeyUp(paramInt, paramKeyEvent);
      if (paramInt == 80) {
        Log.i("TestActivity", "video state:" + paramKeyEvent.getAction());
      }
    }
  }
}


