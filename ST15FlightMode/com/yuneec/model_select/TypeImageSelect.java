package com.yuneec.model_select;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.StatusbarView;
import java.util.ArrayList;

public class TypeImageSelect
  extends Activity
{
  private static final int APPS_PER_PAGE = 8;
  private static final String TAG = "TypeImageSelect";
  private int PrimaryType = 4;
  private int SubType = -1;
  private UARTController mController;
  private AdapterView.OnItemClickListener mElementClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousView = new Intent();
      paramAnonymousAdapterView = (SubTypeInfo)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
      if ((TypeImageSelect.this.mModelId == -1L) || (paramAnonymousAdapterView.type == TypeImageSelect.this.SubType))
      {
        paramAnonymousView.putExtra("sub_type", paramAnonymousAdapterView.type);
        paramAnonymousView.putExtra("type_name", paramAnonymousAdapterView.name);
        TypeImageSelect.this.setResult(-1, paramAnonymousView);
        TypeImageSelect.this.finish();
        TypeImageSelect.this.overridePendingTransition(2130968590, 2130968591);
      }
      for (;;)
      {
        return;
        TypeImageSelect.this.mController = UARTController.getInstance();
        if (TypeImageSelect.this.mController != null)
        {
          Utilities.showProgressDialog(TypeImageSelect.this, null, TypeImageSelect.this.getResources().getString(2131296273), false, false);
          TypeImageSelect.this.mController.startReading();
          new TypeImageSelect.ResetModelTypeTask(TypeImageSelect.this, paramAnonymousAdapterView.name, paramAnonymousAdapterView.type).execute(new Long[] { Long.valueOf(TypeImageSelect.this.mModelId) });
        }
        else
        {
          Log.e("TypeImageSelect", "Should never be here,controller == null");
        }
      }
    }
  };
  private ArrayList<SubTypeInfo> mGridViewList;
  private PageIndicator mIndicator;
  private long mModelId = -2L;
  private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener()
  {
    public void onPageScrollStateChanged(int paramAnonymousInt) {}
    
    public void onPageScrolled(int paramAnonymousInt1, float paramAnonymousFloat, int paramAnonymousInt2) {}
    
    public void onPageSelected(int paramAnonymousInt)
    {
      TypeImageSelect.this.mIndicator.setCurrentPage(paramAnonymousInt);
    }
  };
  private ViewPager mViewPager;
  private TextView model_select_model_type;
  
  private void loadPage(int paramInt1, int paramInt2)
  {
    paramInt2 = TypeImageResource.getTypeCount(paramInt1);
    this.mGridViewList = new ArrayList(paramInt2);
    int i = paramInt1 * 100;
    for (paramInt1 = 0;; paramInt1++)
    {
      if (paramInt1 >= paramInt2) {
        return;
      }
      SubTypeInfo localSubTypeInfo = new SubTypeInfo(i + paramInt1 + 1, TypeImageResource.getTypeName(this, i + paramInt1 + 1));
      this.mGridViewList.add(localSubTypeInfo);
    }
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(2130968590, 2130968591);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903087);
    Utilities.setStatusBarLeftText(this, (StatusbarView)findViewById(2131689807));
    paramBundle = getIntent();
    this.mModelId = paramBundle.getLongExtra("model_id", -2L);
    if (this.mModelId == -2L)
    {
      Log.e("TypeImageSelect", "Can't get the model id!");
      finish();
      overridePendingTransition(2130968590, 2130968591);
    }
    for (;;)
    {
      return;
      this.PrimaryType = paramBundle.getIntExtra("primary_type", 0);
      this.SubType = paramBundle.getIntExtra("sub_type", -1);
      if ((this.SubType == -1) || (this.PrimaryType == this.SubType / 100)) {
        break;
      }
      Log.e("TypeImageSelect", "primary type is not matched with sub type");
      finish();
      overridePendingTransition(2130968590, 2130968591);
    }
    this.model_select_model_type = ((TextView)findViewById(2131689809));
    switch (this.PrimaryType)
    {
    }
    for (paramBundle = getResources().getString(2131296500);; paramBundle = getResources().getString(2131296500) + " --> " + getResources().getString(2131296493))
    {
      this.model_select_model_type.setText(paramBundle);
      loadPage(this.PrimaryType, this.SubType);
      Log.v("TypeImageSelect", "++++++++++++ PrimaryType is" + this.PrimaryType);
      this.mViewPager = ((ViewPager)findViewById(2131689805));
      this.mIndicator = ((PageIndicator)findViewById(2131689810));
      this.mViewPager.setAdapter(new AircraftPagerAdapter(this.SubType));
      this.mViewPager.setOnPageChangeListener(this.mPageChangeListener);
      break;
    }
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 3) && (!paramKeyEvent.isCanceled())) {
      Utilities.backToFlightScreen(this);
    }
    for (boolean bool = true;; bool = super.onKeyUp(paramInt, paramKeyEvent)) {
      return bool;
    }
  }
  
  private class AircraftPagerAdapter
    extends PagerAdapter
  {
    private int highlight_position;
    
    public AircraftPagerAdapter(int paramInt)
    {
      paramInt %= 100;
      if (paramInt == 0) {
        this.highlight_position = -1;
      }
      this.highlight_position = paramInt;
    }
    
    public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
    {
      paramViewGroup.removeView((View)paramObject);
    }
    
    public int getCount()
    {
      int i = (int)Math.ceil(TypeImageSelect.this.mGridViewList.size() / 8.0D);
      TypeImageSelect.this.mIndicator.setPageCount(i);
      return i;
    }
    
    public Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
    {
      int i = paramInt * 8;
      int k = (paramInt + 1) * 8;
      int j = -1;
      paramInt = j;
      if (this.highlight_position > i)
      {
        paramInt = j;
        if (this.highlight_position <= k)
        {
          j = this.highlight_position % 8;
          paramInt = j;
          if (j == 0) {
            paramInt = 8;
          }
        }
      }
      ArrayList localArrayList = new ArrayList(8);
      for (;;)
      {
        if ((i >= k) || (i >= TypeImageSelect.this.mGridViewList.size()))
        {
          View localView = TypeImageSelect.this.getLayoutInflater().inflate(2130903086, null);
          GridView localGridView = (GridView)localView;
          localGridView.setAdapter(new TypeImageAdapter(TypeImageSelect.this.getApplicationContext(), localArrayList, paramInt));
          localGridView.setOnItemClickListener(TypeImageSelect.this.mElementClickListener);
          paramViewGroup.addView(localView);
          return localView;
        }
        localArrayList.add((SubTypeInfo)TypeImageSelect.this.mGridViewList.get(i));
        i++;
      }
    }
    
    public boolean isViewFromObject(View paramView, Object paramObject)
    {
      return paramView.equals(paramObject);
    }
  }
  
  private class ResetModelTypeTask
    extends AsyncTask<Long, Void, Void>
  {
    private long mModel;
    private int mType;
    private String mTypeName;
    
    public ResetModelTypeTask(String paramString, int paramInt)
    {
      this.mTypeName = paramString;
      this.mType = paramInt;
    }
    
    protected Void doInBackground(Long... paramVarArgs)
    {
      paramVarArgs = paramVarArgs[0];
      this.mModel = paramVarArgs.longValue();
      if (Utilities.checkDefaultMixingDataExisted(TypeImageSelect.this, this.mType)) {
        DataProviderHelper.resetMixingChannel(TypeImageSelect.this, paramVarArgs.longValue());
      }
      for (;;)
      {
        Utilities.sendAllDataToFlightControl(TypeImageSelect.this.getApplication(), paramVarArgs.longValue(), TypeImageSelect.this.mController);
        return null;
        Log.w("TypeImageSelect", "Type:" + TypeImageSelect.this.SubType + " Default Data not Found");
      }
    }
    
    protected void onPostExecute(Void paramVoid)
    {
      super.onPostExecute(paramVoid);
      Utilities.dismissProgressDialog();
      TypeImageSelect.this.mController.stopReading();
      TypeImageSelect.this.mController = null;
      paramVoid = ContentUris.withAppendedId(DataProvider.MODEL_URI, this.mModel);
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("type", Integer.valueOf(this.mType));
      TypeImageSelect.this.getContentResolver().update(paramVoid, localContentValues, null, null);
      paramVoid = new Intent();
      paramVoid.putExtra("sub_type", this.mType);
      paramVoid.putExtra("type_name", this.mTypeName);
      TypeImageSelect.this.setResult(-1, paramVoid);
      TypeImageSelect.this.finish();
    }
  }
}


