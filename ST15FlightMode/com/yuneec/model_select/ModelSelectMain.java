package com.yuneec.model_select;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore.Images.Media;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.yuneec.channelsettings.DRData;
import com.yuneec.channelsettings.DRData.CurveParams;
import com.yuneec.channelsettings.ServoData;
import com.yuneec.channelsettings.ThrottleData;
import com.yuneec.channelsettings.ThrottleData.ThrCurve;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.database.ModelBrief;
import com.yuneec.flight_settings.ChannelMap;
import com.yuneec.flightmode15.SyncModelDataTask;
import com.yuneec.flightmode15.SyncModelDataTask.SyncModelDataCompletedAction;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.flightmode15.Utilities.ReceiverInfomation;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.BaseDialog;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.StatusbarView;
import com.yuneec.widget.TwoButtonPopDialog;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelSelectMain
  extends Activity
{
  private static final int APPS_PER_PAGE = 8;
  public static final String IMAGE_SUFFIX = ".jpg";
  private static final String TAG = "ModelSelectMain";
  private int mAircraftType;
  private UARTController mController;
  private long mCurrModelId;
  private AdapterView.OnItemClickListener mElementClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(final AdapterView<?> paramAnonymousAdapterView, final View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousView = (ModelBrief)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
      if (paramAnonymousView._id == -1L) {
        ModelSelectMain.this.AddImportAction(paramAnonymousView);
      }
      for (;;)
      {
        return;
        int i = ModelSelectMain.this.getCurrentModelReceiverAddress(paramAnonymousView._id);
        paramAnonymousInt = ModelSelectMain.this.mController.queryBindState();
        if (paramAnonymousInt > 0)
        {
          if (paramAnonymousInt != i)
          {
            final TwoButtonPopDialog localTwoButtonPopDialog = new TwoButtonPopDialog(ModelSelectMain.this);
            localTwoButtonPopDialog.adjustHeight(380);
            localTwoButtonPopDialog.setTitle(2131296511);
            localTwoButtonPopDialog.setMessage(2131296512);
            localTwoButtonPopDialog.setPositiveButton(17039379, new View.OnClickListener()
            {
              public void onClick(View paramAnonymous2View)
              {
                ModelSelectMain.this.mController.unbind(true, 3);
                ModelSelectMain.this.switchModel(paramAnonymousAdapterView, paramAnonymousView);
                localTwoButtonPopDialog.dismiss();
              }
            });
            localTwoButtonPopDialog.setNegativeButton(17039369, new View.OnClickListener()
            {
              public void onClick(View paramAnonymous2View)
              {
                localTwoButtonPopDialog.cancel();
              }
            });
            localTwoButtonPopDialog.show();
          }
          else
          {
            ModelSelectMain.this.switchModel(paramAnonymousAdapterView, paramAnonymousView);
          }
        }
        else {
          ModelSelectMain.this.switchModel(paramAnonymousAdapterView, paramAnonymousView);
        }
      }
    }
  };
  private AdapterView.OnItemLongClickListener mElementLongClickListener = new AdapterView.OnItemLongClickListener()
  {
    public boolean onItemLongClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousAdapterView = (ModelBrief)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
      if (paramAnonymousAdapterView._id == -1L) {
        ModelSelectMain.this.AddImportAction(paramAnonymousAdapterView);
      }
      for (;;)
      {
        return false;
        ModelSelectMain.this.ModelAction(paramAnonymousAdapterView);
      }
    }
  };
  private ArrayList<GridView> mGridList = new ArrayList(3);
  private ArrayList<ModelBrief> mGridViewList;
  private Handler mHandler = new Handler();
  private PageIndicator mIndicator;
  private Runnable mInvalidateOtherPages = new Runnable()
  {
    public void run()
    {
      int j = ModelSelectMain.this.mGridList.size();
      for (int i = 0;; i++)
      {
        if (i >= j) {
          return;
        }
        ((GridView)ModelSelectMain.this.mGridList.get(i)).invalidateViews();
      }
    }
  };
  private ModelBrief mModelToAsyncOperate;
  private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener()
  {
    public void onPageScrollStateChanged(int paramAnonymousInt) {}
    
    public void onPageScrolled(int paramAnonymousInt1, float paramAnonymousFloat, int paramAnonymousInt2) {}
    
    public void onPageSelected(int paramAnonymousInt)
    {
      ModelSelectMain.this.mIndicator.setCurrentPage(paramAnonymousInt);
    }
  };
  private SharedPreferences mPrefs;
  private MyProgressDialog mProgressDialog;
  private StatusbarView mStatus;
  private ViewPager mViewPager;
  
  private void AddImportAction(final ModelBrief paramModelBrief)
  {
    final BaseDialog localBaseDialog = new BaseDialog(this, 2131230729);
    localBaseDialog.setContentView(2130903080);
    Button localButton1 = (Button)localBaseDialog.findViewById(2131689783);
    Button localButton2 = (Button)localBaseDialog.findViewById(2131689784);
    if (("ST10".equals("ST10")) || ("ST10".equals("ST12")))
    {
      localButton2.setVisibility(4);
      addModel(paramModelBrief);
    }
    for (;;)
    {
      return;
      if ("ST10".equals("ST15"))
      {
        localButton1.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            localBaseDialog.dismiss();
            ModelSelectMain.this.addModel(paramModelBrief);
          }
        });
        localButton2.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            localBaseDialog.dismiss();
            paramAnonymousView = new Intent(ModelSelectMain.this, ModelImport.class);
            ModelSelectMain.this.startActivity(paramAnonymousView);
          }
        });
        localBaseDialog.show();
        localBaseDialog.setCanceledOnTouchOutside(true);
      }
    }
  }
  
  private void CopyIconResource(File paramFile)
    throws IOException
  {}
  
  private void ModelAction(final ModelBrief paramModelBrief)
  {
    final BaseDialog localBaseDialog = new BaseDialog(this, 2131230729);
    localBaseDialog.setContentView(2130903081);
    Button localButton2 = (Button)localBaseDialog.findViewById(2131689785);
    Button localButton3 = (Button)localBaseDialog.findViewById(2131689787);
    Button localButton1 = (Button)localBaseDialog.findViewById(2131689786);
    Button localButton4 = (Button)localBaseDialog.findViewById(2131689788);
    if (("ST10".equals("ST10")) || ("ST10".equals("ST12")))
    {
      localButton1.setVisibility(8);
      localButton4.setVisibility(8);
    }
    localButton2.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localBaseDialog.dismiss();
        ModelSelectMain.this.editModel(paramModelBrief);
      }
    });
    localButton3.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(final View paramAnonymousView)
      {
        localBaseDialog.dismiss();
        paramAnonymousView = new TwoButtonPopDialog(ModelSelectMain.this);
        paramAnonymousView.setTitle(ModelSelectMain.this.getResources().getString(2131296270));
        paramAnonymousView.adjustHeight(380);
        paramAnonymousView.setMessage(ModelSelectMain.this.getResources().getString(2131296510));
        paramAnonymousView.setPositiveButton(2131296276, new View.OnClickListener()
        {
          public void onClick(View paramAnonymous2View)
          {
            paramAnonymousView.dismiss();
            ModelSelectMain.this.deleteModel(this.val$element);
          }
        });
        paramAnonymousView.setNegativeButton(2131296277, new View.OnClickListener()
        {
          public void onClick(View paramAnonymous2View)
          {
            paramAnonymousView.dismiss();
          }
        });
        paramAnonymousView.show();
        paramAnonymousView.setCanceledOnTouchOutside(true);
      }
    });
    if ("ST10".equals("ST15"))
    {
      localButton1.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          localBaseDialog.dismiss();
          ModelSelectMain.this.mModelToAsyncOperate = paramModelBrief;
          ModelSelectMain.this.showProgressDialog("copy", false);
        }
      });
      localButton4.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          localBaseDialog.dismiss();
          ModelSelectMain.this.mModelToAsyncOperate = paramModelBrief;
          ModelSelectMain.this.showProgressDialog("export", false);
        }
      });
    }
    localBaseDialog.show();
    localBaseDialog.setCanceledOnTouchOutside(true);
  }
  
  private void addModel(ModelBrief paramModelBrief)
  {
    editModel(paramModelBrief);
  }
  
  private void buildModelManifest(FileWriter paramFileWriter)
    throws IOException
  {
    paramFileWriter.write("fpv");
    paramFileWriter.write(44);
    paramFileWriter.write(String.valueOf(this.mModelToAsyncOperate.fpv));
    paramFileWriter.write(10);
    paramFileWriter.write("version");
    paramFileWriter.write(44);
    paramFileWriter.write("1");
    paramFileWriter.write(10);
    paramFileWriter.write("name");
    paramFileWriter.write(44);
    paramFileWriter.write(this.mModelToAsyncOperate.name);
    paramFileWriter.write(10);
    paramFileWriter.write("type");
    paramFileWriter.write(44);
    paramFileWriter.write(String.valueOf(this.mModelToAsyncOperate.type));
    paramFileWriter.write(10);
    paramFileWriter.write("f_mode_key");
    paramFileWriter.write(44);
    paramFileWriter.write(String.valueOf(this.mModelToAsyncOperate.f_mode_key));
    paramFileWriter.write(10);
    paramFileWriter.write("analog_min");
    paramFileWriter.write(44);
    paramFileWriter.write(String.valueOf(this.mModelToAsyncOperate.analog_min));
    paramFileWriter.write(10);
    paramFileWriter.write("switch_min");
    paramFileWriter.write(44);
    paramFileWriter.write(String.valueOf(this.mModelToAsyncOperate.switch_min));
    paramFileWriter.write(10);
    Utilities.ReceiverInfomation localReceiverInfomation = Utilities.getReceiverInfo(this, this.mModelToAsyncOperate._id);
    if (localReceiverInfomation != null)
    {
      paramFileWriter.write("rx_analog_num");
      paramFileWriter.write(44);
      paramFileWriter.write(String.valueOf(localReceiverInfomation.analogChNumber));
      paramFileWriter.write(10);
      paramFileWriter.write("rx_switch_num");
      paramFileWriter.write(44);
      paramFileWriter.write(String.valueOf(localReceiverInfomation.switchChNumber));
      paramFileWriter.write(10);
    }
    for (;;)
    {
      paramFileWriter.flush();
      return;
      Log.i("ModelSelectMain", "cann't get rx info,skip export");
    }
  }
  
  private boolean copyChannelMap(long paramLong1, long paramLong2)
  {
    ChannelMap[] arrayOfChannelMap1 = DataProviderHelper.readChannelMapFromDatabase(this, paramLong1);
    ChannelMap[] arrayOfChannelMap2 = DataProviderHelper.readChannelMapFromDatabase(this, paramLong2);
    int i;
    if (arrayOfChannelMap1.length == arrayOfChannelMap2.length)
    {
      i = 0;
      if (i >= arrayOfChannelMap2.length) {
        DataProviderHelper.writeChannelMapFromDatabase(this, arrayOfChannelMap1);
      }
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      arrayOfChannelMap1[i].id = arrayOfChannelMap2[i].id;
      i++;
      break;
      Log.e("ModelSelectMain", "copyChannelMap----Data is unified");
    }
  }
  
  private boolean copyDRData(long paramLong1, long paramLong2)
  {
    boolean bool = false;
    int i = 0;
    if (i >= 3) {
      return bool;
    }
    DRData[] arrayOfDRData2 = DataProviderHelper.readDRDataFromDatabase(this, paramLong1, i);
    DRData[] arrayOfDRData1 = DataProviderHelper.readDRDataFromDatabase(this, paramLong2, i);
    int j;
    if (arrayOfDRData2.length == arrayOfDRData1.length)
    {
      j = 0;
      label45:
      if (j >= 3) {
        DataProviderHelper.writeDRDataToDatabase(this, arrayOfDRData2);
      }
    }
    for (bool = true;; bool = false)
    {
      i++;
      break;
      arrayOfDRData2[j].id = arrayOfDRData1[j].id;
      arrayOfDRData2[j].curveparams[0].id = arrayOfDRData1[j].curveparams[0].id;
      arrayOfDRData2[j].curveparams[1].id = arrayOfDRData1[j].curveparams[1].id;
      arrayOfDRData2[j].curveparams[2].id = arrayOfDRData1[j].curveparams[2].id;
      j++;
      break label45;
      Log.e("ModelSelectMain", "copyDRData----Data is unified");
    }
  }
  
  private String copyModel()
  {
    Object localObject3 = null;
    Object localObject4 = new ContentValues();
    ContentResolver localContentResolver = getContentResolver();
    ((ContentValues)localObject4).put("name", this.mModelToAsyncOperate.name);
    ((ContentValues)localObject4).put("icon", Integer.valueOf(this.mModelToAsyncOperate.iconResourceId));
    ((ContentValues)localObject4).put("type", Integer.valueOf(this.mModelToAsyncOperate.type));
    ((ContentValues)localObject4).put("fpv", Integer.valueOf(this.mModelToAsyncOperate.fpv));
    ((ContentValues)localObject4).put("f_mode_key", Integer.valueOf(this.mModelToAsyncOperate.f_mode_key));
    Object localObject1 = Utilities.getReceiverInfo(this, this.mModelToAsyncOperate._id);
    if (localObject1 != null) {
      if ((((Utilities.ReceiverInfomation)localObject1).analogChNumber == 0) && (((Utilities.ReceiverInfomation)localObject1).switchChNumber == 0))
      {
        Log.i("ModelSelectMain", "can't get receiver info,skip copy");
        ((ContentValues)localObject4).put("analog_min", Integer.valueOf(this.mModelToAsyncOperate.analog_min));
        ((ContentValues)localObject4).put("switch_min", Integer.valueOf(this.mModelToAsyncOperate.switch_min));
        localObject4 = localContentResolver.insert(DataProvider.MODEL_URI, (ContentValues)localObject4);
        if (localObject4 != null) {
          break label255;
        }
        localObject1 = localObject3;
      }
    }
    for (;;)
    {
      return (String)localObject1;
      ((ContentValues)localObject4).put("rx_analog_num", Integer.valueOf(((Utilities.ReceiverInfomation)localObject1).analogChNumber));
      ((ContentValues)localObject4).put("rx_switch_num", Integer.valueOf(((Utilities.ReceiverInfomation)localObject1).switchChNumber));
      break;
      Log.i("ModelSelectMain", "can't get receiver info,skip copy");
      break;
      label255:
      long l1;
      try
      {
        l1 = ContentUris.parseId((Uri)localObject4);
        if (l1 != -1L) {
          break label333;
        }
        Log.e("ModelSelectMain", "Bad Uri :" + localObject4);
        localObject1 = localObject3;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        Log.e("ModelSelectMain", "Bad Uri :" + localObject4);
        localObject2 = localObject3;
      }
      continue;
      label333:
      long l2 = this.mModelToAsyncOperate._id;
      Object localObject2 = localObject3;
      if (copyChannelMap(l2, l1))
      {
        localObject2 = localObject3;
        if (copyThrData(l2, l1))
        {
          localObject2 = localObject3;
          if (copyDRData(l2, l1))
          {
            localObject2 = localObject3;
            if (copyServoData(l2, l1)) {
              localObject2 = "OK";
            }
          }
        }
      }
    }
  }
  
  private boolean copyServoData(long paramLong1, long paramLong2)
  {
    boolean bool = false;
    int i = 0;
    if (i >= 3) {
      return bool;
    }
    ServoData[] arrayOfServoData2 = DataProviderHelper.readServoDataFromDatabase(this, paramLong1, i);
    ServoData[] arrayOfServoData1 = DataProviderHelper.readServoDataFromDatabase(this, paramLong2, i);
    int j;
    if (arrayOfServoData2.length == arrayOfServoData1.length)
    {
      j = 0;
      label45:
      if (j >= arrayOfServoData1.length) {
        DataProviderHelper.writeServoDataToDatabase(this, arrayOfServoData2);
      }
    }
    for (bool = true;; bool = false)
    {
      i++;
      break;
      arrayOfServoData2[j].id = arrayOfServoData1[j].id;
      j++;
      break label45;
      Log.e("ModelSelectMain", "copyServoData----Data is unified");
    }
  }
  
  private boolean copyThrData(long paramLong1, long paramLong2)
  {
    for (int i = 0;; i++)
    {
      if (i >= 3) {
        return true;
      }
      ThrottleData localThrottleData2 = DataProviderHelper.readThrDataFromDatabase(this, paramLong1, i);
      ThrottleData localThrottleData1 = DataProviderHelper.readThrDataFromDatabase(this, paramLong2, i);
      localThrottleData2.id = localThrottleData1.id;
      localThrottleData2.thrCurve[0].id = localThrottleData1.thrCurve[0].id;
      localThrottleData2.thrCurve[1].id = localThrottleData1.thrCurve[1].id;
      localThrottleData2.thrCurve[2].id = localThrottleData1.thrCurve[2].id;
      DataProviderHelper.writeThrDataToDatabase(this, localThrottleData2);
    }
  }
  
  private void deleteModel(ModelBrief paramModelBrief)
  {
    Uri localUri = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramModelBrief._id);
    getContentResolver().delete(localUri, null, null);
    if (paramModelBrief._id == this.mCurrModelId)
    {
      this.mCurrModelId = -2L;
      this.mPrefs.edit().putLong("current_model_id", this.mCurrModelId).commit();
      Utilities.syncMixingDataDeleteAll(this.mController);
      Utilities.setStatusBarLeftText(this, this.mStatus);
    }
    refresh();
  }
  
  private void editModel(ModelBrief paramModelBrief)
  {
    Intent localIntent = new Intent(this, EditModel.class);
    localIntent.putExtra("type", paramModelBrief.type);
    localIntent.putExtra("_id", paramModelBrief._id);
    startActivity(localIntent);
  }
  
  private void exporServoData(Context paramContext, FileWriter paramFileWriter, long paramLong)
    throws IOException
  {
    int i = 0;
    if (i >= 3) {
      return;
    }
    ServoData[] arrayOfServoData = DataProviderHelper.readServoDataFromDatabase(paramContext, paramLong, i);
    for (int j = 0;; j++)
    {
      if (j >= arrayOfServoData.length)
      {
        i++;
        break;
      }
      paramFileWriter.write(i + ",");
      paramFileWriter.write(arrayOfServoData[j].func + ",");
      paramFileWriter.write(arrayOfServoData[j].subTrim + ",");
      paramFileWriter.write(arrayOfServoData[j].reverse + ",");
      paramFileWriter.write(arrayOfServoData[j].speed + ",");
      paramFileWriter.write(arrayOfServoData[j].travelL + ",");
      paramFileWriter.write(arrayOfServoData[j].travelR + "\n");
    }
  }
  
  private void exportChannelMap(Context paramContext, FileWriter paramFileWriter, long paramLong)
    throws IOException
  {
    paramContext = DataProviderHelper.readChannelMapFromDatabase(paramContext, paramLong);
    for (int i = 0;; i++)
    {
      if (i >= paramContext.length) {
        return;
      }
      paramFileWriter.write(paramContext[i].channel + ",");
      paramFileWriter.write(paramContext[i].function + ",");
      paramFileWriter.write(paramContext[i].hardware + ",");
      paramFileWriter.write(paramContext[i].alias + "\n");
    }
  }
  
  private void exportDRData(Context paramContext, FileWriter paramFileWriter, long paramLong)
    throws IOException
  {
    DRData[] arrayOfDRData;
    int j;
    for (int i = 0;; i++)
    {
      if (i >= 3) {
        return;
      }
      arrayOfDRData = DataProviderHelper.readDRDataFromDatabase(paramContext, paramLong, i);
      j = 0;
      if (j < 3) {
        break;
      }
    }
    for (int k = 0;; k++)
    {
      if (k >= 3)
      {
        j++;
        break;
      }
      paramFileWriter.write(i + ",");
      paramFileWriter.write(arrayOfDRData[j].func + ",");
      paramFileWriter.write(arrayOfDRData[j].sw + ",");
      paramFileWriter.write(arrayOfDRData[j].curveparams[k].sw_state + ",");
      paramFileWriter.write(arrayOfDRData[j].curveparams[k].rate1 + ",");
      paramFileWriter.write(arrayOfDRData[j].curveparams[k].rate2 + ",");
      paramFileWriter.write(arrayOfDRData[j].curveparams[k].expo1 + ",");
      paramFileWriter.write(arrayOfDRData[j].curveparams[k].expo2 + ",");
      paramFileWriter.write(arrayOfDRData[j].curveparams[k].offset + "\n");
    }
  }
  
  