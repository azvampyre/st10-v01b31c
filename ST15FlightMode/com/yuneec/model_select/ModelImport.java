package com.yuneec.model_select;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.yuneec.flightmode15.ModelImporter;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.widget.MyProgressDialog;
import java.io.File;
import java.util.ArrayList;

public class ModelImport
  extends Activity
  implements AdapterView.OnItemClickListener, View.OnClickListener
{
  private static final String EXTEN_PATH = "/storage/sdcard1/Exported-Models";
  private static final String LOCATION = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "Exported-Models";
  private static final int SHOW_DIALOG_NUM = 5;
  private static final String TAG = "ModelImportFragment";
  private boolean isFirstShowDetail = true;
  private Button mBtnDelete;
  private Button mBtnImport;
  private String mCurrentImport;
  private Handler mHandler = new Handler();
  private ViewGroup mImportContent;
  private ViewGroup mImportDetail;
  private ArrayList<String> mImportNames = new ArrayList();
  private ListView mListView;
  private ProgressBar mLoadingProgressBar;
  private MyAsyncTask mLoadingTask;
  private ArrayAdapter<String> mModelAdapter;
  private TextView mNameLabel;
  private TextView mNoDetailLabel;
  private TextView mNoModelLabel;
  private ImageView mPicture;
  private ViewGroup mPictureFrame;
  private MyProgressDialog mProgressDialog;
  private ViewGroup mTypeFrame;
  private ImageView mTypeImage;
  private TextView mTypeName;
  
  private void deleteImport(String paramString)
  {
    File[] arrayOfFile;
    if (Utilities.hasSDCard(this))
    {
      paramString = new File("/storage/sdcard1/Exported-Models", paramString);
      arrayOfFile = paramString.listFiles();
      if (arrayOfFile == null) {}
    }
    for (int i = 0;; i++)
    {
      if (i >= arrayOfFile.length)
      {
        paramString.delete();
        return;
        paramString = new File(LOCATION, paramString);
        break;
      }
      arrayOfFile[i].delete();
    }
  }
  
  