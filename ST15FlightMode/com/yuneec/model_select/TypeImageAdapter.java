package com.yuneec.model_select;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class TypeImageAdapter
  extends ArrayAdapter<SubTypeInfo>
{
  private ImageView image_item;
  private int mHightlightPos;
  private ArrayList<SubTypeInfo> mTypeImageInfo;
  private RelativeLayout rl;
  private TextView text_item;
  
  public TypeImageAdapter(Context paramContext, ArrayList<SubTypeInfo> paramArrayList, int paramInt)
  {
    super(paramContext, 0, paramArrayList);
    this.mTypeImageInfo = paramArrayList;
    this.mHightlightPos = paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    SubTypeInfo localSubTypeInfo = (SubTypeInfo)this.mTypeImageInfo.get(paramInt);
    View localView = paramView;
    if (paramView == null) {
      localView = ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(2130903083, paramViewGroup, false);
    }
    this.image_item = ((ImageView)localView.findViewById(2131689801));
    this.text_item = ((TextView)localView.findViewById(2131689802));
    this.rl = ((RelativeLayout)localView.findViewById(2131689800));
    if (paramInt + 1 == this.mHightlightPos) {
      this.rl.setBackgroundResource(2130837741);
    }
    this.image_item.setImageResource(TypeImageResource.typeTransformToImageId(getContext(), localSubTypeInfo.type));
    this.text_item.setText(localSubTypeInfo.name);
    return localView;
  }
}


