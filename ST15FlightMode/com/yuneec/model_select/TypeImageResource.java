package com.yuneec.model_select;

import android.content.Context;
import android.content.res.Resources;

public class TypeImageResource
{
  private static int[] multicopter_image_ST10 = { 2130837722, 2130837723, 2130837724 };
  private static int[] multicopter_image_ST12 = { 2130837722, 2130837723, 2130837724, 2130837725, 2130837726, 2130837727 };
  private static int[] multicopter_image_name_ST10 = { 2131296524, 2131296525, 2131296526 };
  private static int[] multicopter_image_name_ST12 = { 2131296524, 2131296525, 2131296526, 2131296527, 2131296528, 2131296529 };
  
  public static int getTypeCount(int paramInt)
  {
    int[] arrayOfInt = null;
    if ("ST10".equals("ST12")) {
      arrayOfInt = multicopter_image_ST12;
    }
    for (;;)
    {
      return arrayOfInt.length;
      if ("ST10".equals("ST10")) {
        arrayOfInt = multicopter_image_ST10;
      }
    }
  }
  
  public static String getTypeName(Context paramContext, int paramInt)
  {
    int k = 0;
    int i;
    if ("ST10".equals("ST12"))
    {
      i = 0;
      if (i >= multicopter_image_ST12.length)
      {
        i = k;
        label26:
        if (i == 0) {
          break label119;
        }
      }
    }
    label119:
    for (paramContext = paramContext.getResources().getString(i);; paramContext = null)
    {
      return paramContext;
      if (paramInt == i + 400 + 1)
      {
        i = multicopter_image_name_ST12[i];
        break label26;
      }
      i++;
      break;
      i = k;
      if (!"ST10".equals("ST10")) {
        break label26;
      }
      for (int j = 0;; j++)
      {
        i = k;
        if (j >= multicopter_image_ST10.length) {
          break;
        }
        if (paramInt == j + 400 + 1)
        {
          i = multicopter_image_name_ST10[j];
          break;
        }
      }
    }
  }
  
  public static int typeTransformToImageId(Context paramContext, int paramInt)
  {
    int k = 0;
    int i;
    if ("ST10".equals("ST12"))
    {
      i = 0;
      if (i >= multicopter_image_ST12.length) {
        i = k;
      }
    }
    do
    {
      for (;;)
      {
        return i;
        if (paramInt != i + 400 + 1) {
          break;
        }
        i = multicopter_image_ST12[i];
      }
      i++;
      break;
      i = k;
    } while (!"ST10".equals("ST10"));
    for (int j = 0;; j++)
    {
      i = k;
      if (j >= multicopter_image_ST10.length) {
        break;
      }
      if (paramInt == j + 400 + 1)
      {
        i = multicopter_image_ST10[j];
        break;
      }
    }
  }
}


