package com.yuneec.model_select;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class PageIndicator
  extends View
{
  private static final int INDICATOR_PADDING = 22;
  private static final String TAG = "PageIndicator";
  private int mCurrentPage;
  private Drawable mIndicator;
  private int mIndicatorHeight;
  private Drawable mIndicatorPressed;
  private int mIndicatorWidth;
  private int mPages;
  private boolean mShowIfNeeded = true;
  
  public PageIndicator(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public PageIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public PageIndicator(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.mIndicator = paramContext.getResources().getDrawable(2130837742);
    this.mIndicatorPressed = paramContext.getResources().getDrawable(2130837743);
    this.mIndicatorWidth = this.mIndicator.getIntrinsicWidth();
    this.mIndicatorHeight = this.mIndicator.getIntrinsicHeight();
    this.mIndicator.setBounds(0, 0, this.mIndicatorWidth, this.mIndicatorHeight);
    this.mIndicatorPressed.setBounds(0, 0, this.mIndicatorWidth, this.mIndicatorHeight);
  }
  
  public int getCurrentPage()
  {
    return this.mCurrentPage;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (this.mPages < 0) {
      throw new IllegalArgumentException("PageIndicator illegal page count :" + this.mPages);
    }
    if (this.mPages == 0) {
      Log.w("PageIndicator", "No pages,Nothing to Draw");
    }
    while ((this.mPages == 1) && (this.mShowIfNeeded)) {
      return;
    }
    int j = this.mPages;
    int i = 0;
    label72:
    if (i < j)
    {
      paramCanvas.save();
      paramCanvas.translate((this.mIndicatorWidth + 22) * i, 0.0F);
      if (i != this.mCurrentPage) {
        break label123;
      }
      this.mIndicatorPressed.draw(paramCanvas);
    }
    for (;;)
    {
      paramCanvas.restore();
      i++;
      break label72;
      break;
      label123:
      this.mIndicator.draw(paramCanvas);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (this.mPages < 0) {
      throw new IllegalArgumentException("PageIndicator illegal page count :" + this.mPages);
    }
    if (this.mPages == 0)
    {
      Log.w("PageIndicator", "onMeasure mPages == 0,width&height set to 0 mPages:" + this.mPages);
      paramInt2 = 0;
      paramInt1 = 0;
    }
    for (;;)
    {
      setMeasuredDimension(paramInt1, paramInt2);
      return;
      paramInt1 = this.mIndicatorWidth * this.mPages + (this.mPages - 1) * 22;
      paramInt2 = this.mIndicatorHeight;
    }
  }
  
  public void setCurrentPage(int paramInt)
  {
    this.mCurrentPage = paramInt;
    invalidate();
  }
  
  public void setPageCount(int paramInt)
  {
    this.mPages = paramInt;
    invalidate();
  }
  
  public void setShowPolicy(boolean paramBoolean)
  {
    this.mShowIfNeeded = paramBoolean;
    invalidate();
  }
}


