package com.yuneec.model_select;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.text.Editable;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.database.ModelBrief;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.StatusbarView;
import java.io.File;

public class EditModel
  extends Activity
  implements View.OnClickListener, MediaScannerConnection.OnScanCompletedListener
{
  private static final int CROP_ERR = -1;
  private static final int CROP_NEEDED = 1;
  private static final int CROP_NONEED = 0;
  private static final int CROP_REQ = 1002;
  static final String IMAGE_SUFFIX = ".jpg";
  private static final int MODEL_TYPE = 1;
  private static final int PHOTO_HEIGHT = 289;
  private static final int PHOTO_WIDTH = 515;
  private static final int PICKICON_REQ = 1000;
  private static final String TAG = "EditModel";
  private static final int TAKEPICTURE_REQ = 1001;
  private boolean isMediaScanning = false;
  private ModelBrief mBrief;
  private UARTController mController;
  private int mIconResourceId = 2130837731;
  private long mModelId;
  private int mModelSubType = -1;
  private int mModelType;
  private Uri mOutput;
  private SharedPreferences mPrefs;
  private MyProgressDialog mProgressDialog;
  private RelativeLayout model_image_frame;
  private EditText model_name_title;
  private ImageView model_photo;
  private Button model_reset;
  private Button model_save;
  private ImageView model_type;
  private int originType;
  private TextView type_name;
  
  private File checkVaild(String paramString, int paramInt, File paramFile)
  {
    File localFile = paramFile;
    if (paramFile.exists()) {
      localFile = checkVaild(paramString, paramInt + 1, new File(paramString + "-" + paramInt + ".jpg"));
    }
    return localFile;
  }
  
  private int cropIfNeeded(Intent paramIntent)
  {
    int i = -1;
    if (paramIntent == null) {
      Log.w("EditModel", "cropIfNeeded returned uri is null");
    }
    for (;;)
    {
      return i;
      paramIntent = paramIntent.getData();
      if (paramIntent == null)
      {
        Log.w("EditModel", "cropIfNeeded returned uri is null");
      }
      else
      {
        String str = getIconLocation(paramIntent);
        if (str.startsWith(new File(Environment.getExternalStorageDirectory(), "ModelsIcon").getAbsolutePath()))
        {
          Log.i("EditModel", "picked icon " + str + " was in ModelIcons Folder,no need to crop");
          i = 0;
        }
        else
        {
          startCrop(paramIntent, true);
          i = 1;
        }
      }
    }
  }
  
  private String getIconLocation(Uri paramUri)
  {
    if (paramUri.getScheme().startsWith("file")) {
      paramUri = Uri.decode(paramUri.getPath());
    }
    for (;;)
    {
      return paramUri;
      if (paramUri.getScheme().startsWith("content"))
      {
        Cursor localCursor = MediaStore.Images.Media.query(getContentResolver(), paramUri, new String[] { "_data" });
        localCursor.moveToFirst();
        paramUri = localCursor.getString(localCursor.getColumnIndex("_data"));
        localCursor.close();
      }
      else
      {
        Log.w("EditModel", "getIconLocation Unkown Uri: " + paramUri);
        paramUri = null;
      }
    }
  }
  
  private void getIntentValue()
  {
    Intent localIntent = getIntent();
    this.mModelId = localIntent.getLongExtra("_id", -2L);
    this.originType = localIntent.getIntExtra("type", 0);
  }
  
  private ModelBrief loadModel(long paramLong)
  {
    Object localObject = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
    Cursor localCursor = getContentResolver().query((Uri)localObject, new String[] { "_id", "name", "icon", "type" }, null, null, null);
    if ((localCursor == null) || (!localCursor.moveToFirst()))
    {
      Log.e("EditModel", "Cursor open failed,Cannot get Model Brief");
      localObject = null;
    }
    for (;;)
    {
      return (ModelBrief)localObject;
      localObject = new ModelBrief();
      ((ModelBrief)localObject).name = localCursor.getString(localCursor.getColumnIndex("name"));
      ((ModelBrief)localObject).iconResourceId = localCursor.getInt(localCursor.getColumnIndex("icon"));
      ((ModelBrief)localObject).type = localCursor.getInt(localCursor.getColumnIndex("type"));
      localCursor.close();
    }
  }
  
  private Uri prepare()
  {
    Object localObject1 = new File(Environment.getExternalStorageDirectory(), "ModelsIcon");
    if ((!((File)localObject1).exists()) && (!((File)localObject1).mkdir())) {
      Log.e("EditModel", "mkdir error dir:" + ((File)localObject1).getAbsolutePath());
    }
    Object localObject2 = new Time();
    ((Time)localObject2).setToNow();
    localObject2 = ((Time)localObject2).format("%Y-%m-%d %H.%M.%S");
    localObject1 = localObject1 + "/" + "Icon-" + (String)localObject2;
    return Uri.fromFile(checkVaild((String)localObject1, 1, new File(localObject1 + ".jpg")));
  }
  
  private void setInitValue()
  {
    if (this.mModelId == -2L)
    {
      Log.e("EditModel", "Cannnot get Model Id!!");
      finish();
      return;
    }
    if (this.mModelId != -1L)
    {
      this.type_name.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(2130837739), null, null, null);
      this.mModelSubType = this.originType;
      this.mModelType = (this.mModelSubType / 100);
    }
    for (;;)
    {
      this.model_name_title.setSelection(this.model_name_title.getText().length());
      if (this.mModelId != -1L)
      {
        this.mBrief = loadModel(this.mModelId);
        this.mIconResourceId = this.mBrief.iconResourceId;
        setupView();
      }
      Log.v("EditModel", "mModelType is : " + this.mModelType);
      break;
      this.type_name.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(2130837732), null, null, null);
      this.mModelType = this.originType;
      this.mModelSubType = -1;
    }
  }
  
  private void setTypeImage()
  {
    this.model_type.setImageResource(TypeImageResource.typeTransformToImageId(this, this.mBrief.type));
    this.type_name.setText(TypeImageResource.getTypeName(this, this.mBrief.type));
  }
  
  private void setupView()
  {
    this.model_name_title.setText(this.mBrief.name);
    if (this.mBrief.iconResourceId == 0) {
      this.model_photo.setImageResource(2130837731);
    }
    for (;;)
    {
      setTypeImage();
      return;
      this.model_photo.setImageResource(this.mBrief.iconResourceId);
    }
  }
  
  private void showSelectPictureAcitivity()
  {
    Intent localIntent = new Intent();
    localIntent.setFlags(1073741824);
    localIntent.setAction("android.intent.action.GET_CONTENT");
    localIntent.addCategory("android.intent.category.OPENABLE");
    localIntent.setType("image