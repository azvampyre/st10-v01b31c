package com.yuneec.model_select;

class SubTypeInfo
{
  String name;
  int type;
  
  public SubTypeInfo() {}
  
  public SubTypeInfo(int paramInt, String paramString)
  {
    this.type = paramInt;
    this.name = paramString;
  }
}


