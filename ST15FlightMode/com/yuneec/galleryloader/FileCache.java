package com.yuneec.galleryloader;

import android.content.Context;

public class FileCache
  extends AbstractFileCache
{
  public FileCache(Context paramContext)
  {
    super(paramContext);
  }
  
  public String getCacheDir()
  {
    return FileManager.getCacheFilePath();
  }
  
  public String getSavePath(String paramString)
  {
    int i = paramString.hashCode();
    return getCacheDir() + String.valueOf(i);
  }
}


