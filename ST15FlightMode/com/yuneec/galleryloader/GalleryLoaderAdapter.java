package com.yuneec.galleryloader;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GalleryLoaderAdapter
  extends BaseAdapter
{
  private static final String TAG = "GalleryLoaderAdapter";
  private boolean mBusy = false;
  private Context mContext;
  private int mCount;
  private ImageLoader mImageLoader;
  private boolean mSelectionMode = false;
  private String[] urlArrays;
  
  public GalleryLoaderAdapter(Context paramContext, String[] paramArrayOfString)
  {
    this.mCount = paramArrayOfString.length;
    this.mContext = paramContext;
    this.urlArrays = paramArrayOfString;
    this.mImageLoader = new ImageLoader(paramContext);
  }
  
  public int getCount()
  {
    return this.mCount;
  }
  
  public ImageLoader getImageLoader()
  {
    return this.mImageLoader;
  }
  
  public Object getItem(int paramInt)
  {
    return this.urlArrays[paramInt];
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    String str;
    if (paramView == null)
    {
      paramView = new GalleryItem(this.mContext);
      paramView.setLayoutParams(new AbsListView.LayoutParams(-2, -2));
      paramViewGroup = new ViewHolder();
      paramViewGroup.mTextView = ((TextView)paramView.findViewById(2131689605));
      paramViewGroup.mImageView = ((ImageView)paramView.findViewById(2131689603));
      paramView.setTag(paramViewGroup);
      paramViewGroup.mImageView.setTag(Integer.valueOf(paramInt));
      if (paramInt <= this.urlArrays.length) {
        break label185;
      }
      Log.w("GalleryLoaderAdapter", "UI position > url arrays");
      str = this.urlArrays[0];
      label106:
      if (!this.mSelectionMode) {
        paramView.findViewById(2131689604).setVisibility(4);
      }
      if (this.mBusy) {
        break label196;
      }
      this.mImageLoader.DisplayImage(Gallery.mCurrentFlieGetUrl + str, paramViewGroup.mImageView, false);
      paramViewGroup.mTextView.setText(str);
    }
    for (;;)
    {
      return paramView;
      paramViewGroup = (ViewHolder)paramView.getTag();
      break;
      label185:
      str = this.urlArrays[paramInt];
      break label106;
      label196:
      this.mImageLoader.DisplayImage(Gallery.mCurrentFlieGetUrl + str, paramViewGroup.mImageView, true);
      paramViewGroup.mTextView.setText(str);
    }
  }
  
  public void setFlagBusy(boolean paramBoolean)
  {
    this.mBusy = paramBoolean;
  }
  
  public void setSelectionMode(boolean paramBoolean)
  {
    this.mSelectionMode = paramBoolean;
  }
  
  static class ViewHolder
  {
    ImageView mImageView;
    TextView mTextView;
  }
}


