package com.yuneec.galleryloader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.IPCameraManager.IPCameraManager.GetMediaFileCallback;
import java.io.File;
import java.util.ArrayList;

public class Gallery
  extends Activity
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  public static final String FILE_DELURL_PREFIX_1 = "http://192.168.73.254/sddel.htm?FILE=";
  public static final String FILE_DELURL_PREFIX_2 = "http://192.168.42.1/DCIM/100MEDIA/";
  public static final String FILE_GETURL_PREFIX_1 = "http://192.168.73.254/sdget.htm?FILE=";
  public static final String FILE_GETURL_PREFIX_2 = "http://192.168.42.1/DCIM/100MEDIA/";
  private static final String TAG = "Gallery";
  public static String mCurrentFlieDelUrl;
  public static String mCurrentFlieGetUrl;
  private GridView mGridView;
  private Handler mHandler = new Handler();
  private IPCameraManager mIPCameraManager;
  private int mLongpressPosition = -1;
  private ActionMode.Callback mSelectActionMode = new ActionMode.Callback()
  {
    TextView mActionText;
    
    private ArrayList<String> collectSelection()
    {
      SparseBooleanArray localSparseBooleanArray = Gallery.this.mGridView.getCheckedItemPositions();
      ArrayList localArrayList = new ArrayList();
      for (int i = 0;; i++)
      {
        if (i >= localSparseBooleanArray.size()) {
          return localArrayList;
        }
        int j = localSparseBooleanArray.keyAt(i);
        if (localSparseBooleanArray.valueAt(i)) {
          localArrayList.add((String)Gallery.this.mGridView.getItemAtPosition(j));
        }
      }
    }
    
    public boolean onActionItemClicked(ActionMode paramAnonymousActionMode, MenuItem paramAnonymousMenuItem)
    {
      boolean bool2 = true;
      boolean bool1 = false;
      if (Gallery.this.mGridView.getVisibility() != 0) {
        bool1 = false;
      }
      for (;;)
      {
        return bool1;
        switch (paramAnonymousMenuItem.getItemId())
        {
        default: 
          bool1 = bool2;
          break;
        case 2131690031: 
          paramAnonymousActionMode = collectSelection();
          if (paramAnonymousActionMode.size() == 0)
          {
            Toast.makeText(Gallery.this.getApplicationContext(), 2131296486, 0).show();
            bool1 = bool2;
          }
          else
          {
            new Gallery.SelectionActionTask(Gallery.this, paramAnonymousActionMode).execute(new String[] { "delete" });
            bool1 = bool2;
          }
          break;
        }
      }
      if (Gallery.this.mToggleSelectAll)
      {
        Gallery.this.toggleSelectAll(false);
        paramAnonymousMenuItem.setTitle(2131296483);
        label150:
        paramAnonymousActionMode = Gallery.this;
        if (!Gallery.this.mToggleSelectAll) {
          break label196;
        }
      }
      for (;;)
      {
        paramAnonymousActionMode.mToggleSelectAll = bool1;
        bool1 = bool2;
        break;
        Gallery.this.toggleSelectAll(true);
        paramAnonymousMenuItem.setTitle(2131296484);
        break label150;
        label196:
        bool1 = true;
      }
    }
    
    public boolean onCreateActionMode(ActionMode paramAnonymousActionMode, Menu paramAnonymousMenu)
    {
      View localView = LayoutInflater.from(Gallery.this).inflate(2130903067, null);
      this.mActionText = ((TextView)localView.findViewById(2131689606));
      this.mActionText.setVisibility(8);
      paramAnonymousActionMode.setCustomView(localView);
      Gallery.this.getMenuInflater().inflate(2131623937, paramAnonymousMenu);
      paramAnonymousActionMode = (GalleryLoaderAdapter)Gallery.this.mGridView.getAdapter();
      if (paramAnonymousActionMode != null) {
        paramAnonymousActionMode.setSelectionMode(true);
      }
      Gallery.this.mGridView.setChoiceMode(2);
      Gallery.this.mGridView.setOnItemClickListener(null);
      Gallery.this.mGridView.setOnItemLongClickListener(null);
      if (Gallery.this.mLongpressPosition != -1) {
        Gallery.this.mGridView.setItemChecked(Gallery.this.mLongpressPosition, true);
      }
      return true;
    }
    
    public void onDestroyActionMode(ActionMode paramAnonymousActionMode)
    {
      Gallery.this.toggleSelectAll(false);
      paramAnonymousActionMode = (GalleryLoaderAdapter)Gallery.this.mGridView.getAdapter();
      if (paramAnonymousActionMode != null) {
        paramAnonymousActionMode.setSelectionMode(false);
      }
      Gallery.this.mGridView.setChoiceMode(0);
      Gallery.this.mGridView.setOnItemClickListener(Gallery.this);
      Gallery.this.mGridView.setOnItemLongClickListener(Gallery.this);
      Gallery.this.mLongpressPosition = -1;
    }
    
    public boolean onPrepareActionMode(ActionMode paramAnonymousActionMode, Menu paramAnonymousMenu)
    {
      return true;
    }
  };
  private boolean mToggleSelectAll = false;
  
  private void clearLoaderMemoryCache()
  {
    GalleryLoaderAdapter localGalleryLoaderAdapter = (GalleryLoaderAdapter)this.mGridView.getAdapter();
    if (localGalleryLoaderAdapter != null) {
      localGalleryLoaderAdapter.getImageLoader().clearMemoryCache();
    }
  }
  
  private void loadMedia()
  {
    findViewById(2131689830).setVisibility(4);
    findViewById(2131689803).setVisibility(4);
    findViewById(2131689831).setVisibility(0);
    this.mIPCameraManager.getMediaFile(new IPCameraManager.GetMediaFileCallback()
    {
      public void MediaFileGot(String[] paramAnonymousArrayOfString)
      {
        if (paramAnonymousArrayOfString == null)
        {
          Gallery.this.findViewById(2131689830).setVisibility(0);
          Gallery.this.findViewById(2131689831).setVisibility(4);
          return;
        }
        ArrayList localArrayList = new ArrayList();
        int i = 0;
        label41:
        if (i >= paramAnonymousArrayOfString.length)
        {
          paramAnonymousArrayOfString = new String[localArrayList.size()];
          localArrayList.toArray(paramAnonymousArrayOfString);
        }
        for (i = 0;; i++)
        {
          if (i >= paramAnonymousArrayOfString.length)
          {
            paramAnonymousArrayOfString = new GalleryLoaderAdapter(Gallery.this, paramAnonymousArrayOfString);
            Gallery.this.mGridView.setAdapter(paramAnonymousArrayOfString);
            Gallery.this.findViewById(2131689803).setVisibility(0);
            Gallery.this.findViewById(2131689831).setVisibility(4);
            break;
            if (!paramAnonymousArrayOfString[i].endsWith(".avi")) {
              localArrayList.add(paramAnonymousArrayOfString[i]);
            }
            i++;
            break label41;
          }
          Log.i("Gallery", "media:" + paramAnonymousArrayOfString[i]);
        }
      }
    });
  }
  
  private void reloadMedia()
  {
    clearLoaderMemoryCache();
    this.mGridView.setAdapter(null);
    loadMedia();
  }
  
  private void toggleSelectAll(boolean paramBoolean)
  {
    ListAdapter localListAdapter = this.mGridView.getAdapter();
    if (localListAdapter == null) {}
    for (;;)
    {
      return;
      int j = localListAdapter.getCount();
      for (int i = 0; i < j; i++) {
        this.mGridView.setItemChecked(i, paramBoolean);
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Gallery", "onCreate");
    super.onCreate(paramBundle);
    setContentView(2130903095);
    this.mGridView = ((GridView)findViewById(2131689803));
    this.mGridView.setOnItemClickListener(this);
    this.mGridView.setOnItemLongClickListener(this);
    this.mGridView.setChoiceMode(0);
    int i = getSharedPreferences("flight_setting_value", 0).getInt("camera_type_flag", 0);
    if ((i & 0x1) == 1)
    {
      this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 101);
      mCurrentFlieGetUrl = "http://192.168.73.254/sdget.htm?FILE=";
      mCurrentFlieDelUrl = "http://192.168.73.254/sddel.htm?FILE=";
    }
    for (;;)
    {
      loadMedia();
      return;
      if ((i & 0x4) == 4)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 102);
        mCurrentFlieGetUrl = "http://192.168.42.1/DCIM/100MEDIA/";
        mCurrentFlieDelUrl = "http://192.168.42.1/DCIM/100MEDIA/";
      }
      else if ((i & 0x8) == 8)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 104);
        mCurrentFlieGetUrl = "http://192.168.42.1/DCIM/100MEDIA/";
        mCurrentFlieDelUrl = "http://192.168.42.1/DCIM/100MEDIA/";
      }
      else
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 100);
        mCurrentFlieGetUrl = "http://192.168.73.254/sdget.htm?FILE=";
        mCurrentFlieDelUrl = "http://192.168.73.254/sddel.htm?FILE=";
      }
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131623936, paramMenu);
    return true;
  }
  
  protected void onDestroy()
  {
    Log.d("Gallery", "onDestroy");
    super.onDestroy();
    this.mIPCameraManager.finish();
    this.mIPCameraManager = null;
    clearLoaderMemoryCache();
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    paramView = (String)paramAdapterView.getItemAtPosition(paramInt);
    if (paramView.endsWith(".jpg"))
    {
      paramAdapterView = ((GalleryLoaderAdapter)paramAdapterView.getAdapter()).getImageLoader().getFileCache(mCurrentFlieGetUrl + paramView);
      if (paramAdapterView != null)
      {
        paramView = new Intent(this, ImageViewActivity.class);
        paramView.putExtra("url", paramAdapterView.getAbsolutePath());
        startActivity(paramView);
      }
    }
  }
  
  public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.mLongpressPosition = paramInt;
    startActionMode(this.mSelectActionMode);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 2131690030) {
      startActionMode(this.mSelectActionMode);
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  private class SelectionActionTask
    extends AsyncTask<String, Object, String>
  {
    private ProgressDialog dialog;
    private String mAction;
    private ArrayList<String> mFilelist;
    
    public SelectionActionTask()
    {
      ArrayList localArrayList;
      this.mFilelist = localArrayList;
    }
    
    protected String doInBackground(String... paramVarArgs)
    {
      int j;
      int i;
      if ("delete".equals(paramVarArgs[0]))
      {
        this.mAction = "delete";
        this.dialog.setProgressNumberFormat("%1d/%2d");
        this.dialog.setMessage(Gallery.this.getResources().getString(2131296485));
        j = this.mFilelist.size();
        paramVarArgs = new Object[3];
        paramVarArgs[1] = Integer.valueOf(j);
        paramVarArgs[2] = Integer.valueOf(0);
        publishProgress(paramVarArgs);
        i = 0;
      }
      for (;;)
      {
        if (i >= this.mFilelist.size()) {
          paramVarArgs = null;
        }
        String str;
        do
        {
          for (;;)
          {
            return paramVarArgs;
            if (!isCancelled()) {
              break;
            }
            paramVarArgs = "HTTPCODE User Cancelled";
          }
          paramVarArgs = (String)this.mFilelist.get(i);
          publishProgress(new Object[] { paramVarArgs + " " + (i + 1) + "/" + j, Integer.valueOf(0), Integer.valueOf(i + 1) });
          str = Gallery.this.mIPCameraManager.rawRequestBlock(Gallery.mCurrentFlieDelUrl + paramVarArgs, false, null);
          Log.d("Gallery", "delete " + paramVarArgs + " result:" + str);
          paramVarArgs = ((GalleryLoaderAdapter)Gallery.this.mGridView.getAdapter()).getImageLoader().getFileCache(Gallery.mCurrentFlieGetUrl + paramVarArgs);
          if (paramVarArgs != null) {
            paramVarArgs.delete();
          }
          paramVarArgs = str;
        } while ("HTTPCODE User Cancelled".equals(str));
        try
        {
          Thread.sleep(100L);
          i++;
        }
        catch (InterruptedException paramVarArgs)
        {
          for (;;) {}
        }
      }
    }
    
    protected void onCancelled(String paramString)
    {
      super.onCancelled(paramString);
      if ("HTTPCODE User Cancelled".equals(paramString)) {
        Toast.makeText(Gallery.this.getApplicationContext(), 2131296488, 0).show();
      }
      for (;;)
      {
        return;
        Log.i("Gallery", "onCancelled result:" + paramString);
      }
    }
    
    protected void onPostExecute(String paramString)
    {
      super.onPostExecute(paramString);
      this.dialog.setProgress(this.dialog.getMax());
      if ("delete".equals(this.mAction)) {
        Gallery.this.reloadMedia();
      }
      Gallery.this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          Gallery.SelectionActionTask.this.dialog.dismiss();
        }
      }, 200L);
    }
    
    protected void onPreExecute()
    {
      super.onPreExecute();
      this.dialog = new ProgressDialog(Gallery.this);
      this.dialog.setCanceledOnTouchOutside(false);
      this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          Gallery.SelectionActionTask.this.cancel(true);
        }
      });
      this.dialog.setProgressStyle(1);
      this.dialog.setMessage(Gallery.this.getResources().getString(2131296485));
      this.dialog.setMax(0);
      this.dialog.show();
    }
    
    protected void onProgressUpdate(Object... paramVarArgs)
    {
      super.onProgressUpdate(paramVarArgs);
      String str = (String)paramVarArgs[0];
      if (str != null) {
        this.dialog.setMessage(str);
      }
      int i = ((Integer)paramVarArgs[1]).intValue();
      if (i != 0) {
        this.dialog.setMax(i);
      }
      i = ((Integer)paramVarArgs[2]).intValue();
      this.dialog.setProgress(i);
    }
  }
}


