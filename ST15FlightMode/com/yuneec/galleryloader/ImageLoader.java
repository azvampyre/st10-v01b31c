package com.yuneec.galleryloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.Log;
import android.widget.ImageView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader
{
  private ExecutorService executorService;
  private AbstractFileCache fileCache;
  private Map<Integer, String> imageViews = Collections.synchronizedMap(new WeakHashMap());
  private MemoryCache memoryCache = new MemoryCache();
  
  public ImageLoader(Context paramContext)
  {
    this.fileCache = new FileCache(paramContext);
    this.executorService = Executors.newFixedThreadPool(5);
  }
  
  public static void CopyStream(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    for (;;)
    {
      try
      {
        arrayOfByte = new byte[51200];
        i = paramInputStream.read(arrayOfByte, 0, 51200);
        if (i == -1) {
          return;
        }
      }
      catch (Exception paramInputStream)
      {
        byte[] arrayOfByte;
        int i;
        Log.e("", "CopyStream catch Exception...");
        continue;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  private Bitmap decodeFile(File paramFile)
  {
    localObject1 = null;
    for (;;)
    {
      try
      {
        Object localObject2 = new android/graphics/BitmapFactory$Options;
        ((BitmapFactory.Options)localObject2).<init>();
        ((BitmapFactory.Options)localObject2).inJustDecodeBounds = true;
        Object localObject3 = new java/io/FileInputStream;
        ((FileInputStream)localObject3).<init>(paramFile);
        BitmapFactory.decodeStream((InputStream)localObject3, null, (BitmapFactory.Options)localObject2);
        j = ((BitmapFactory.Options)localObject2).outWidth;
        k = ((BitmapFactory.Options)localObject2).outHeight;
        i = 1;
        if ((j / 2 >= 100) && (k / 2 >= 100)) {
          continue;
        }
        localObject3 = new android/graphics/BitmapFactory$Options;
        ((BitmapFactory.Options)localObject3).<init>();
        ((BitmapFactory.Options)localObject3).inSampleSize = i;
        localObject2 = new java/io/FileInputStream;
        ((FileInputStream)localObject2).<init>(paramFile);
        paramFile = BitmapFactory.decodeStream((InputStream)localObject2, null, (BitmapFactory.Options)localObject3);
      }
      catch (FileNotFoundException paramFile)
      {
        int j;
        int k;
        int i;
        paramFile = (File)localObject1;
        continue;
      }
      return paramFile;
      j /= 2;
      k /= 2;
      i *= 2;
    }
  }
  
  private Bitmap getBitmap(String paramString)
  {
    File localFile = this.fileCache.getFile(paramString);
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (localFile != null)
    {
      localObject1 = localObject2;
      if (localFile.exists()) {
        localObject1 = decodeFile(localFile);
      }
    }
    if (localObject1 != null) {}
    for (;;)
    {
      return (Bitmap)localObject1;
      try
      {
        localObject1 = new java/net/URL;
        ((URL)localObject1).<init>(paramString);
        paramString = (HttpURLConnection)((URL)localObject1).openConnection();
        paramString.setConnectTimeout(30000);
        paramString.setReadTimeout(30000);
        paramString.setInstanceFollowRedirects(true);
        paramString = paramString.getInputStream();
        localObject1 = new java/io/FileOutputStream;
        ((FileOutputStream)localObject1).<init>(localFile);
        CopyStream(paramString, (OutputStream)localObject1);
        ((OutputStream)localObject1).close();
        localObject1 = decodeFile(localFile);
      }
      catch (Exception paramString)
      {
        Log.e("", "getBitmap catch Exception...\nmessage = " + paramString.getMessage());
        localObject1 = null;
      }
    }
  }
  
  private void queuePhoto(String paramString, ImageView paramImageView)
  {
    paramString = new PhotoToLoad(paramString, paramImageView);
    this.executorService.submit(new PhotosLoader(paramString));
  }
  
  public void DisplayImage(String paramString, ImageView paramImageView, boolean paramBoolean)
  {
    int i = ((Integer)paramImageView.getTag()).intValue();
    this.imageViews.put(Integer.valueOf(i), paramString);
    Bitmap localBitmap = this.memoryCache.get(paramString);
    if (localBitmap != null) {
      paramImageView.setImageBitmap(localBitmap);
    }
    for (;;)
    {
      return;
      if (!paramBoolean)
      {
        paramImageView.setImageResource(2130837621);
        queuePhoto(paramString, paramImageView);
      }
    }
  }
  
  public void clearCache()
  {
    this.memoryCache.clear();
    this.fileCache.clear();
  }
  
  public void clearFileCache()
  {
    this.fileCache.clear();
  }
  
  public void clearMemoryCache()
  {
    this.memoryCache.clear();
  }
  
  public File getFileCache(String paramString)
  {
    paramString = this.fileCache.getFile(paramString);
    if ((paramString != null) && (paramString.exists())) {}
    for (;;)
    {
      return paramString;
      paramString = null;
    }
  }
  
  boolean imageViewReused(PhotoToLoad paramPhotoToLoad)
  {
    int i = ((Integer)paramPhotoToLoad.imageView.getTag()).intValue();
    String str = (String)this.imageViews.get(Integer.valueOf(i));
    if ((str == null) || (!str.equals(paramPhotoToLoad.url))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  class BitmapDisplayer
    implements Runnable
  {
    Bitmap bitmap;
    ImageLoader.PhotoToLoad photoToLoad;
    
    public BitmapDisplayer(Bitmap paramBitmap, ImageLoader.PhotoToLoad paramPhotoToLoad)
    {
      this.bitmap = paramBitmap;
      this.photoToLoad = paramPhotoToLoad;
    }
    
    public void run()
    {
      if (ImageLoader.this.imageViewReused(this.photoToLoad)) {}
      for (;;)
      {
        return;
        if (this.bitmap != null)
        {
          ImageView localImageView = this.photoToLoad.imageView;
          Object localObject = new BitmapDrawable(localImageView.getResources(), this.bitmap);
          localObject = new TransitionDrawable(new Drawable[] { new ColorDrawable(17170445), localObject });
          localImageView.setImageDrawable((Drawable)localObject);
          ((TransitionDrawable)localObject).startTransition(1000);
        }
      }
    }
  }
  
  private class PhotoToLoad
  {
    public ImageView imageView;
    public String url;
    
    public PhotoToLoad(String paramString, ImageView paramImageView)
    {
      this.url = paramString;
      this.imageView = paramImageView;
    }
  }
  
  class PhotosLoader
    implements Runnable
  {
    ImageLoader.PhotoToLoad photoToLoad;
    
    PhotosLoader(ImageLoader.PhotoToLoad paramPhotoToLoad)
    {
      this.photoToLoad = paramPhotoToLoad;
    }
    
    public void run()
    {
      if (ImageLoader.this.imageViewReused(this.photoToLoad)) {}
      for (;;)
      {
        return;
        Object localObject = ImageLoader.this.getBitmap(this.photoToLoad.url);
        ImageLoader.this.memoryCache.put(this.photoToLoad.url, (Bitmap)localObject);
        if (!ImageLoader.this.imageViewReused(this.photoToLoad))
        {
          localObject = new ImageLoader.BitmapDisplayer(ImageLoader.this, (Bitmap)localObject, this.photoToLoad);
          ((Activity)this.photoToLoad.imageView.getContext()).runOnUiThread((Runnable)localObject);
        }
      }
    }
  }
}


