package com.yuneec.galleryloader;

import android.content.Context;
import android.util.Log;
import java.io.File;

public abstract class AbstractFileCache
{
  private String dirString = getCacheDir();
  
  public AbstractFileCache(Context paramContext)
  {
    boolean bool = FileHelper.createDirectory(this.dirString);
    Log.e("", "FileHelper.createDirectory:" + this.dirString + ", ret = " + bool);
  }
  
  public void clear()
  {
    FileHelper.deleteDirectory(this.dirString);
  }
  
  public abstract String getCacheDir();
  
  public File getFile(String paramString)
  {
    return new File(getSavePath(paramString));
  }
  
  public abstract String getSavePath(String paramString);
}


