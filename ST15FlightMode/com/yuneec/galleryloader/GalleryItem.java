package com.yuneec.galleryloader;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class GalleryItem
  extends RelativeLayout
  implements Checkable
{
  private ImageView mCheckImg;
  private boolean mChecked;
  
  public GalleryItem(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public GalleryItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public GalleryItem(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    LayoutInflater.from(paramContext).inflate(2130903066, this);
    this.mCheckImg = ((ImageView)findViewById(2131689604));
    this.mChecked = false;
  }
  
  public boolean isChecked()
  {
    return this.mChecked;
  }
  
  public void setChecked(boolean paramBoolean)
  {
    this.mChecked = paramBoolean;
    if (paramBoolean) {
      this.mCheckImg.setVisibility(0);
    }
    for (;;)
    {
      return;
      this.mCheckImg.setVisibility(4);
    }
  }
  
  public void toggle()
  {
    if (this.mChecked) {}
    for (boolean bool = false;; bool = true)
    {
      setChecked(bool);
      return;
    }
  }
}


