package com.yuneec.galleryloader;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ImageViewActivity
  extends Activity
{
  private static final long AUTO_HIDE_ACTIONBAR_DURATION = 2000L;
  private BitmapDrawable mBitmapDrawable;
  private Handler mHandler = new Handler();
  private Runnable mHideActionBarRunnable = new Runnable()
  {
    public void run()
    {
      ImageViewActivity.this.getActionBar().hide();
    }
  };
  private ImageView mImageView;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903065);
    paramBundle = getIntent().getStringExtra("url");
    if (paramBundle == null) {
      finish();
    }
    for (;;)
    {
      return;
      this.mBitmapDrawable = new BitmapDrawable(getResources(), paramBundle);
      this.mImageView = ((ImageView)findViewById(2131689601));
      this.mImageView.setImageDrawable(this.mBitmapDrawable);
      this.mImageView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (ImageViewActivity.this.getActionBar().isShowing())
          {
            ImageViewActivity.this.mHandler.removeCallbacks(ImageViewActivity.this.mHideActionBarRunnable);
            ImageViewActivity.this.mHideActionBarRunnable.run();
          }
          for (;;)
          {
            return;
            ImageViewActivity.this.getActionBar().show();
            ImageViewActivity.this.mHandler.postDelayed(ImageViewActivity.this.mHideActionBarRunnable, 2000L);
          }
        }
      });
      this.mHandler.postDelayed(this.mHideActionBarRunnable, 2000L);
      getActionBar().setTitle("");
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.mImageView.setImageDrawable(null);
    if (this.mBitmapDrawable.getBitmap() != null) {
      this.mBitmapDrawable.getBitmap().recycle();
    }
    this.mBitmapDrawable = null;
  }
}


