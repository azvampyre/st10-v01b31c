package com.yuneec.flight_settings;

public class WifiPassword
{
  private String password;
  private String ssid;
  
  public WifiPassword() {}
  
  public WifiPassword(String paramString1, String paramString2)
  {
    this.ssid = paramString1;
    this.password = paramString2;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public String getSsid()
  {
    return this.ssid;
  }
  
  public void setPassword(String paramString)
  {
    this.password = paramString;
  }
  
  public void setSsid(String paramString)
  {
    this.ssid = paramString;
  }
  
  public String toString()
  {
    return "WifiPassword [ssid=" + this.ssid + ", password=" + this.password + "]";
  }
}


