package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.widget.ButtonPicker;
import com.yuneec.widget.ButtonPicker.OnPickerListener;
import java.util.ArrayList;

public class SwitchSelect
  extends Activity
{
  private static final String[] AUX_LIST = { "INH", "S1", "S2", "S3", "S4", "B1", "B2" };
  private static final String[] GO_HOME_LIST;
  private static final String TAG = "SwitchSelect";
  private static ArrayList<String> auxArray;
  private static ArrayList<String> gohomeArray = new ArrayList();
  private ButtonPicker mAUX10;
  private ButtonPicker mAUX11;
  private ButtonPicker mAUX2;
  private ButtonPicker mAUX3;
  private ButtonPicker mAUX4;
  private ButtonPicker mAUX5;
  private ButtonPicker mAUX6;
  private ButtonPicker mAUX7;
  private ButtonPicker mAUX8;
  private ButtonPicker mAUX9;
  private ButtonPicker.OnPickerListener mAUXPickListener10 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[9] = paramAnonymousString;
      SwitchSelect.this.setArrayList(9);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener11 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[10] = paramAnonymousString;
      SwitchSelect.this.setArrayList(10);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener2 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[1] = paramAnonymousString;
      SwitchSelect.this.setArrayList(1);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener3 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[2] = paramAnonymousString;
      SwitchSelect.this.setArrayList(2);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener4 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[3] = paramAnonymousString;
      SwitchSelect.this.setArrayList(3);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener5 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[4] = paramAnonymousString;
      SwitchSelect.this.setArrayList(4);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener6 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[5] = paramAnonymousString;
      SwitchSelect.this.setArrayList(5);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener7 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[6] = paramAnonymousString;
      SwitchSelect.this.setArrayList(6);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener8 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[7] = paramAnonymousString;
      SwitchSelect.this.setArrayList(7);
    }
  };
  private ButtonPicker.OnPickerListener mAUXPickListener9 = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[8] = paramAnonymousString;
      SwitchSelect.this.setArrayList(8);
    }
  };
  private ChannelMap[] mChannelMaps;
  private UARTController mController;
  private long mCurrentModelId;
  private ButtonPicker mGoHome;
  private ButtonPicker.OnPickerListener mGoHomeListener = new ButtonPicker.OnPickerListener()
  {
    public void onClicked(ButtonPicker paramAnonymousButtonPicker, String paramAnonymousString)
    {
      SwitchSelect.this.str_select[0] = paramAnonymousString;
      SwitchSelect.this.setArrayList(0);
    }
  };
  private String[] str_select = new String[11];
  
  static
  {
    auxArray = new ArrayList();
    GO_HOME_LIST = new String[] { "INH", "S2", "S3", "B1", "B2" };
  }
  
  private void initAUXArrayList()
  {
    auxArray.clear();
    for (int i = 0;; i++)
    {
      if (i >= AUX_LIST.length) {
        return;
      }
      auxArray.add(AUX_LIST[i]);
    }
  }
  
  private void initGoHomeArrayList()
  {
    gohomeArray.clear();
    for (int i = 0;; i++)
    {
      if (i >= GO_HOME_LIST.length) {
        return;
      }
      gohomeArray.add(GO_HOME_LIST[i]);
    }
  }
  
  private void initValue()
  {
    this.mGoHome = ((ButtonPicker)findViewById(2131689949));
    this.mAUX2 = ((ButtonPicker)findViewById(2131689952));
    this.mAUX3 = ((ButtonPicker)findViewById(2131689955));
    this.mAUX4 = ((ButtonPicker)findViewById(2131689958));
    this.mAUX5 = ((ButtonPicker)findViewById(2131689961));
    this.mAUX6 = ((ButtonPicker)findViewById(2131689964));
    this.mAUX7 = ((ButtonPicker)findViewById(2131689967));
    this.mAUX8 = ((ButtonPicker)findViewById(2131689970));
    this.mAUX9 = ((ButtonPicker)findViewById(2131689973));
    this.mAUX10 = ((ButtonPicker)findViewById(2131689976));
    this.mAUX11 = ((ButtonPicker)findViewById(2131689979));
    readDataToDB();
    setGoHomeArrayList();
    setAUXArrayList();
    this.mGoHome.initiationArray(gohomeArray, this.str_select[0]);
    this.mAUX2.initiationArray(auxArray, this.str_select[1]);
    this.mAUX3.initiationArray(auxArray, this.str_select[2]);
    this.mAUX4.initiationArray(auxArray, this.str_select[3]);
    this.mAUX5.initiationArray(auxArray, this.str_select[4]);
    this.mAUX6.initiationArray(auxArray, this.str_select[5]);
    this.mAUX7.initiationArray(auxArray, this.str_select[6]);
    this.mAUX8.initiationArray(auxArray, this.str_select[7]);
    this.mAUX9.initiationArray(auxArray, this.str_select[8]);
    this.mAUX10.initiationArray(auxArray, this.str_select[9]);
    this.mAUX11.initiationArray(auxArray, this.str_select[10]);
  }
  
  private void readDataToDB()
  {
    this.mChannelMaps = DataProviderHelper.readChannelMapFromDatabase(this, this.mCurrentModelId);
    int i;
    if (this.mChannelMaps != null)
    {
      i = 4;
      if (i < this.mChannelMaps.length) {}
    }
    for (;;)
    {
      return;
      this.str_select[(i - 4)] = this.mChannelMaps[i].hardware;
      i++;
      break;
      for (i = 0; i < this.str_select.length; i++) {
        this.str_select[i] = "INH";
      }
    }
  }
  
  private void setAUXArrayList()
  {
    initAUXArrayList();
    for (int i = 0;; i++)
    {
      if (i >= auxArray.size()) {
        return;
      }
      if ((this.str_select[0].equals(auxArray.get(i))) && (!this.str_select[0].equals("INH"))) {
        auxArray.remove(i);
      }
    }
  }
  
  private void setArrayList(int paramInt)
  {
    switch (paramInt)
    {
    }
    for (;;)
    {
      wirteDataToDB();
      return;
      setGoHomeArrayList();
      continue;
      setAUXArrayList();
    }
  }
  
  private void setGoHomeArrayList()
  {
    initGoHomeArrayList();
    int i = 1;
    if (i >= this.str_select.length) {
      return;
    }
    if (!"INH".equals(this.str_select[i])) {}
    for (int j = 0;; j++)
    {
      if (j >= gohomeArray.size())
      {
        i++;
        break;
      }
      if (this.str_select[i].equals(gohomeArray.get(j))) {
        gohomeArray.remove(j);
      }
    }
  }
  
  private void wirteDataToDB()
  {
    for (int i = 0;; i++)
    {
      if (i >= this.str_select.length)
      {
        DataProviderHelper.writeChannelMapFromDatabase(this, this.mChannelMaps);
        return;
      }
      this.mChannelMaps[(i + 4)].hardware = this.str_select[i];
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903109);
    this.mCurrentModelId = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    initValue();
    this.mGoHome.setOnPickerListener(this.mGoHomeListener);
    this.mAUX2.setOnPickerListener(this.mAUXPickListener2);
    this.mAUX3.setOnPickerListener(this.mAUXPickListener3);
    this.mAUX4.setOnPickerListener(this.mAUXPickListener4);
    this.mAUX5.setOnPickerListener(this.mAUXPickListener5);
    this.mAUX6.setOnPickerListener(this.mAUXPickListener6);
    this.mAUX7.setOnPickerListener(this.mAUXPickListener7);
    this.mAUX8.setOnPickerListener(this.mAUXPickListener8);
    this.mAUX9.setOnPickerListener(this.mAUXPickListener9);
    this.mAUX10.setOnPickerListener(this.mAUXPickListener10);
    this.mAUX11.setOnPickerListener(this.mAUXPickListener11);
  }
  
  protected void onPause()
  {
    super.onPause();
    Utilities.sendAllDataToFlightControl(this, this.mCurrentModelId, this.mController);
    if (!Utilities.ensureAwaitState(this.mController)) {
      Log.e("SwitchSelect", "fails to enter await state");
    }
    Utilities.UartControllerStandBy(this.mController);
    this.mController = null;
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    this.mController.startReading();
    if (!Utilities.ensureSimState(this.mController)) {
      Log.e("SwitchSelect", "fails to enter sim state");
    }
  }
}


