package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.yuneec.flightmode15.Utilities;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class QuickReview
  extends Activity
  implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  private static final int SHOW_DIALOG_FILE_SELECTED_NUM = 5;
  private static final String TAG = "QuickReview";
  private Button mBtnCancel;
  private Button mBtnDelete;
  private boolean mInSelection = false;
  private ListView mListView;
  private TextView mNoVideoLabel;
  private String[] mShortnameVideoList = null;
  private ToggleButton mTogSelectAll;
  private FilenameFilter mVideoFilter = new FilenameFilter()
  {
    public boolean accept(File paramAnonymousFile, String paramAnonymousString)
    {
      boolean bool2 = false;
      boolean bool1 = bool2;
      if (paramAnonymousString != null)
      {
        bool1 = bool2;
        if (paramAnonymousString.lastIndexOf(".avc") != -1) {
          bool1 = true;
        }
      }
      return bool1;
    }
  };
  
  private void deleteVideo()
  {
    SparseBooleanArray localSparseBooleanArray = this.mListView.getCheckedItemPositions();
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i >= localSparseBooleanArray.size())
    {
      if (localArrayList.size() >= 5) {
        break label84;
      }
      deleteVideoInternal(localArrayList);
      refreshList();
    }
    for (;;)
    {
      return;
      int j = localSparseBooleanArray.keyAt(i);
      if (localSparseBooleanArray.valueAt(i)) {
        localArrayList.add((String)this.mListView.getItemAtPosition(j));
      }
      i++;
      break;
      label84:
      Utilities.showProgressDialog(this, null, getResources().getString(2131296273), false, false);
      new DeleteTask(null).execute(new ArrayList[] { localArrayList });
    }
  }
  
  private void deleteVideoInternal(ArrayList<String> paramArrayList)
  {
    int i = 0;
    if (i >= paramArrayList.size()) {
      return;
    }
    File localFile = new File("/sdcard/FPV-Video/Local", (String)paramArrayList.get(i));
    Log.d("QuickReview", "To delete :" + localFile.getAbsolutePath());
    if (localFile.exists()) {
      if (!localFile.delete()) {
        Log.w("QuickReview", "delete file fail:" + localFile.getAbsolutePath());
      }
    }
    for (;;)
    {
      i++;
      break;
      Log.w("QuickReview", "file not existed:" + localFile.getAbsolutePath());
    }
  }
  
  private void enterSelectionMode()
  {
    this.mListView.setOnItemClickListener(null);
    this.mListView.setOnItemLongClickListener(null);
    this.mListView.setChoiceMode(2);
    this.mListView.setAdapter(null);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903117, 16908308, this.mShortnameVideoList);
    this.mListView.setAdapter(localArrayAdapter);
    this.mBtnCancel.setVisibility(0);
    this.mBtnDelete.setVisibility(0);
    this.mTogSelectAll.setVisibility(0);
    this.mInSelection = true;
  }
  
  private void exitSelectionMode()
  {
    this.mListView.setChoiceMode(0);
    this.mListView.setAdapter(null);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903116, 16908308, this.mShortnameVideoList);
    this.mListView.setAdapter(localArrayAdapter);
    this.mListView.setOnItemClickListener(this);
    this.mListView.setOnItemLongClickListener(this);
    this.mBtnCancel.setVisibility(4);
    this.mBtnDelete.setVisibility(4);
    this.mTogSelectAll.setVisibility(4);
    this.mInSelection = false;
  }
  
  private String[] loadList()
  {
    Object localObject = new File("/sdcard/FPV-Video/Local");
    if (!((File)localObject).exists()) {
      Log.i("QuickReview", "dir is not existed :/sdcard/FPV-Video/Local");
    }
    for (localObject = null;; localObject = ((File)localObject).list(this.mVideoFilter)) {
      return (String[])localObject;
    }
  }
  
  private void refreshList()
  {
    this.mShortnameVideoList = loadList();
    if ((this.mShortnameVideoList != null) && (this.mShortnameVideoList.length > 0)) {
      exitSelectionMode();
    }
    for (;;)
    {
      return;
      this.mNoVideoLabel.setVisibility(0);
      this.mListView.setVisibility(4);
      this.mBtnCancel.setVisibility(4);
      this.mBtnDelete.setVisibility(4);
      this.mTogSelectAll.setVisibility(4);
    }
  }
  
  private void toggleSelectAll(boolean paramBoolean)
  {
    int j = this.mListView.getAdapter().getCount();
    for (int i = 0;; i++)
    {
      if (i >= j) {
        return;
      }
      this.mListView.setItemChecked(i, paramBoolean);
    }
  }
  
  public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    if (paramCompoundButton.equals(this.mTogSelectAll)) {
      toggleSelectAll(paramBoolean);
    }
  }
  
  public void onClick(View paramView)
  {
    if (paramView.equals(this.mBtnCancel)) {
      exitSelectionMode();
    }
    for (;;)
    {
      return;
      if (paramView.equals(this.mBtnDelete)) {
        deleteVideo();
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903098);
    this.mListView = ((ListView)findViewById(2131689841));
    this.mBtnCancel = ((Button)findViewById(2131689837));
    this.mBtnDelete = ((Button)findViewById(2131689838));
    this.mTogSelectAll = ((ToggleButton)findViewById(2131689839));
    this.mNoVideoLabel = ((TextView)findViewById(2131689840));
    this.mBtnCancel.setOnClickListener(this);
    this.mBtnDelete.setOnClickListener(this);
    this.mTogSelectAll.setOnCheckedChangeListener(this);
    this.mShortnameVideoList = loadList();
    if ((this.mShortnameVideoList != null) && (this.mShortnameVideoList.length > 0))
    {
      this.mNoVideoLabel.setVisibility(4);
      this.mListView.setVisibility(0);
      paramBundle = new ArrayAdapter(this, 2130903116, 16908308, this.mShortnameVideoList);
      this.mListView.setAdapter(paramBundle);
      this.mListView.setOnItemClickListener(this);
      this.mListView.setOnItemLongClickListener(this);
    }
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    paramAdapterView = (String)paramAdapterView.getItemAtPosition(paramInt);
    paramView = new Intent(this, QuickReviewPlayActivity.class);
    paramView.putExtra("short_file_name", paramAdapterView);
    startActivity(paramView);
  }
  
  public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    enterSelectionMode();
    return true;
  }
  
  private class DeleteTask
    extends AsyncTask<ArrayList<String>, Void, Void>
  {
    private DeleteTask() {}
    
    protected Void doInBackground(ArrayList<String>... paramVarArgs)
    {
      paramVarArgs = paramVarArgs[0];
      QuickReview.this.deleteVideoInternal(paramVarArgs);
      return null;
    }
    
    protected void onPostExecute(Void paramVoid)
    {
      super.onPostExecute(paramVoid);
      QuickReview.this.refreshList();
      Utilities.dismissProgressDialog();
    }
  }
}


