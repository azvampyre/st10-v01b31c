package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.yuneec.IPCameraManager.Cameras;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.flightmode15.WeakHandler;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.OneButtonPopDialog;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CameraSelect
  extends Activity
{
  private static final String TAG = "CameraSelect";
  private AdapterView.OnItemClickListener adapterOnItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      CameraSelect.this.current_positon = paramAnonymousInt;
      paramAnonymousAdapterView = (Cameras)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
      CameraSelect.this.camera_name.setText(paramAnonymousAdapterView.getName());
      CameraSelect.this.camera_type.setText(paramAnonymousAdapterView.getType());
      CameraSelect.this.current_type = CameraSelect.parseType(paramAnonymousAdapterView.getType());
      if (CameraSelect.this.mIPCameraManager != null)
      {
        CameraSelect.this.mIPCameraManager.finish();
        CameraSelect.this.mIPCameraManager = null;
      }
      if ((CameraSelect.this.current_type & 0x1) == 1) {
        CameraSelect.this.mIPCameraManager = IPCameraManager.getIPCameraManager(CameraSelect.this, 101);
      }
      for (;;)
      {
        return;
        if ((CameraSelect.this.current_type & 0x4) == 4) {
          CameraSelect.this.mIPCameraManager = IPCameraManager.getIPCameraManager(CameraSelect.this, 102);
        } else if (((CameraSelect.this.current_type & 0x8) == 8) || ((CameraSelect.this.current_type & 0x20) == 32)) {
          CameraSelect.this.mIPCameraManager = IPCameraManager.getIPCameraManager(CameraSelect.this, 104);
        } else if ((CameraSelect.this.current_type & 0x10) == 16) {
          CameraSelect.this.mIPCameraManager = IPCameraManager.getIPCameraManager(CameraSelect.this, 105);
        } else {
          CameraSelect.this.mIPCameraManager = IPCameraManager.getIPCameraManager(CameraSelect.this, 100);
        }
      }
    }
  };
  private TextView camera_name;
  private TextView camera_type;
  private List<Cameras> cameras;
  private ListView choose_camera_list;
  private Button choose_camera_selected;
  private int current_positon;
  private int current_type;
  private View.OnClickListener mButtonOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      paramAnonymousView = new BindWifiManage((WifiManager)CameraSelect.this.getSystemService("wifi"));
      Log.v("CameraSelect", " wifi -- " + paramAnonymousView.getWifiStatus());
      if (!paramAnonymousView.getWifiStatus()) {
        CameraSelect.this.showOneButtonDialog(300, 2131296467, 2131296468);
      }
      for (;;)
      {
        return;
        CameraSelect.this.mCurrentCamera = ((Cameras)CameraSelect.this.mCamerasList.get(CameraSelect.this.current_positon));
        CameraSelect.this.mPrefs.edit().putString("camera_selected_info", CameraSelect.this.getCurrentmCurrentCameraTypeString(CameraSelect.this.mCurrentCamera)).commit();
        CameraSelect.this.mPrefs.edit().putString("camera_current_selected", CameraSelect.this.mCurrentCamera.getName()).commit();
        CameraSelect.this.current_type = CameraSelect.parseType(CameraSelect.this.mCurrentCamera.getType());
        CameraSelect.this.mIPCameraManager.setCC4In1Config(CameraSelect.this.mHttpResponseMessenger, CameraSelect.this.mCurrentCamera);
        Utilities.showProgressDialog(CameraSelect.this, null, CameraSelect.this.getResources().getText(2131296472), false, false);
      }
    }
  };
  private CameraArrayAdapter mCameraArrayAdapter;
  private ArrayList<Cameras> mCamerasList = new ArrayList();
  private Cameras mCurrentCamera = null;
  private HttpRequestHandler mHttpHandler = new HttpRequestHandler(this);
  private Messenger mHttpResponseMessenger = new Messenger(this.mHttpHandler);
  private IPCameraManager mIPCameraManager;
  private InputStream mInputStream;
  private SharedPreferences mPrefs;
  private CameraParser parser;
  
  private String getCurrentmCurrentCameraTypeString(Cameras paramCameras)
  {
    return "name:" + paramCameras.getName() + "@" + "dr" + ":" + paramCameras.getDr() + "@" + "f" + ":" + paramCameras.getF() + "@" + "n" + ":" + paramCameras.getN() + "@" + "t1" + ":" + paramCameras.getT1() + "@" + "t2" + ":" + paramCameras.getT2() + "@" + "intervalp" + ":" + paramCameras.getIntervalp() + "@" + "codep" + ":" + paramCameras.getCodep() + "@" + "intervalv" + ":" + paramCameras.getIntervalv() + "@" + "codev" + ":" + paramCameras.getCodev();
  }
  
  private void loadXMLData()
  {
    for (;;)
    {
      try
      {
        if (!"ST10".equals("ST12")) {
          continue;
        }
        this.mInputStream = getAssets().open("camera_command_ST12.xml");
        PullCameraParser localPullCameraParser = new com/yuneec/flight_settings/PullCameraParser;
        localPullCameraParser.<init>();
        this.parser = localPullCameraParser;
        this.cameras = this.parser.parse(this.mInputStream);
        localIterator = this.cameras.iterator();
      }
      catch (Exception localException)
      {
        Iterator localIterator;
        Log.e("CameraSelect", localException.getMessage());
        continue;
        Cameras localCameras = (Cameras)localIterator.next();
        this.mCamerasList.add(localCameras);
        continue;
      }
      if (localIterator.hasNext()) {
        continue;
      }
      return;
      if ("ST10".equals("ST10")) {
        this.mInputStream = getAssets().open("camera_command_ST10.xml");
      }
    }
  }
  
  public static int parseType(String paramString)
  {
    int k = 0;
    paramString = paramString.split(",");
    int j = 0;
    if (j >= paramString.length) {
      return k;
    }
    int i;
    if ((paramString[j].equals("cc4in1")) || (paramString[j].equals("lk58"))) {
      i = k | 0x1;
    }
    for (;;)
    {
      j++;
      k = i;
      break;
      if (paramString[j].equals("remotezoom"))
      {
        i = k | 0x2;
      }
      else if (paramString[j].equals("amba_cgo2"))
      {
        i = k | 0x4;
      }
      else if (paramString[j].equals("amba_cgo3"))
      {
        i = k | 0x8;
      }
      else if (paramString[j].equals("minilk58"))
      {
        i = k | 0x10;
      }
      else
      {
        i = k;
        if (paramString[j].equals("amba_cgo3_pro")) {
          i = k | 0x20;
        }
      }
    }
  }
  
  private void showOneButtonDialog(int paramInt1, int paramInt2, int paramInt3)
  {
    final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(this);
    localOneButtonPopDialog.adjustHeight(paramInt1);
    localOneButtonPopDialog.setMessageGravity(17);
    localOneButtonPopDialog.setTitle(paramInt2);
    localOneButtonPopDialog.setMessage(paramInt3);
    localOneButtonPopDialog.setPositiveButton(17039370, new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CameraSelect.this.mIPCameraManager.initCamera(CameraSelect.this.mHttpResponseMessenger);
        localOneButtonPopDialog.dismiss();
      }
    });
    localOneButtonPopDialog.show();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903052);
    this.mPrefs = getSharedPreferences("flight_setting_value", 0);
    this.current_type = this.mPrefs.getInt("camera_type_flag", getResources().getInteger(2131492868));
    this.current_positon = this.mPrefs.getInt("camera_selected_position_value", getResources().getInteger(2131492870));
    this.camera_name = ((TextView)findViewById(2131689536));
    this.camera_type = ((TextView)findViewById(2131689538));
    this.choose_camera_selected = ((Button)findViewById(2131689539));
    this.choose_camera_list = ((ListView)findViewById(2131689534));
    loadXMLData();
    this.mCameraArrayAdapter = new CameraArrayAdapter(this, 17367062, this.mCamerasList);
    this.choose_camera_list.setAdapter(this.mCameraArrayAdapter);
    this.choose_camera_list.setOnItemClickListener(this.adapterOnItemClickListener);
    this.choose_camera_selected.setOnClickListener(this.mButtonOnClickListener);
  }
  
  public void onPause()
  {
    super.onPause();
    if (this.mIPCameraManager != null) {
      this.mIPCameraManager.finish();
    }
    this.mIPCameraManager = null;
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.mCamerasList.size() > 0)
    {
      this.mCurrentCamera = ((Cameras)this.mCamerasList.get(this.current_positon));
      this.choose_camera_list.setItemChecked(this.current_positon, true);
      this.camera_name.setText(this.mCurrentCamera.getName());
      this.camera_type.setText(this.mCurrentCamera.getType());
      this.mPrefs.edit().putString("camera_current_selected", this.mCurrentCamera.getName()).commit();
      if ((this.current_type & 0x1) != 1) {
        break label132;
      }
      this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 101);
      Log.v("CameraSelect", " -- IPCameraManager  current type is cc4in1");
    }
    for (;;)
    {
      return;
      label132:
      if ((this.current_type & 0x4) == 4)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 102);
        Log.v("CameraSelect", " -- IPCameraManager  current type is amba");
      }
      else if (((this.current_type & 0x8) == 8) || ((this.current_type & 0x20) == 32))
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 104);
      }
      else if ((this.current_type & 0x10) == 16)
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 105);
      }
      else
      {
        this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 100);
        Log.v("CameraSelect", " -- IPCameraManager  current type is dm368");
      }
    }
  }
  
  private class CameraArrayAdapter
    extends ArrayAdapter<Cameras>
  {
    private LayoutInflater mInflater;
    private int mResource;
    
    public CameraArrayAdapter(int paramInt, List<Cameras> paramList)
    {
      super(paramList, localList);
      this.mResource = paramList;
      this.mInflater = ((LayoutInflater)paramInt.getSystemService("layout_inflater"));
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null) {
        paramView = this.mInflater.inflate(this.mResource, paramViewGroup, false);
      }
      for (;;)
      {
        try
        {
          paramViewGroup = (TextView)paramView;
          paramViewGroup.setText(((Cameras)getItem(paramInt)).getName());
          return paramView;
        }
        catch (ClassCastException paramView)
        {
          Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
          throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", paramView);
        }
      }
    }
  }
  
  private class HttpRequestHandler
    extends WeakHandler<CameraSelect>
  {
    public HttpRequestHandler(CameraSelect paramCameraSelect)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.arg1)
      {
      default: 
        Utilities.dismissProgressDialog();
      }
      for (;;)
      {
        return;
        if (!"HTTPCODE OK".equals(paramMessage.obj))
        {
          MyToast.makeText(CameraSelect.this, CameraSelect.this.getString(2131296343), 0, 1);
        }
        else
        {
          Log.i("CameraSelect", "Init camera complete");
          continue;
          Log.v("CameraSelect", " ------ resp   " + paramMessage.obj);
          if ("HTTPCODE OK".equals(paramMessage.obj))
          {
            Utilities.dismissProgressDialog();
            CameraSelect.this.showOneButtonDialog(380, 2131296469, 2131296471);
            CameraSelect.this.mPrefs.edit().putInt("camera_type_flag", CameraSelect.this.current_type).commit();
            CameraSelect.this.mPrefs.edit().putInt("camera_selected_position_value", CameraSelect.this.current_positon).commit();
          }
          else
          {
            Utilities.dismissProgressDialog();
            CameraSelect.this.showOneButtonDialog(380, 2131296469, 2131296470);
          }
        }
      }
    }
  }
}


