package com.yuneec.flight_settings;

import android.util.Xml;
import com.yuneec.IPCameraManager.Cameras;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class PullCameraParser
  implements CameraParser
{
  public List<Cameras> parse(InputStream paramInputStream)
    throws Exception
  {
    Object localObject1 = null;
    InputStream localInputStream = null;
    XmlPullParser localXmlPullParser = Xml.newPullParser();
    localXmlPullParser.setInput(paramInputStream, "UTF-8");
    int i = localXmlPullParser.getEventType();
    if (i == 1) {
      return (List<Cameras>)localObject1;
    }
    paramInputStream = localInputStream;
    Object localObject2 = localObject1;
    switch (i)
    {
    default: 
      localObject2 = localObject1;
      paramInputStream = localInputStream;
    }
    for (;;)
    {
      i = localXmlPullParser.next();
      localInputStream = paramInputStream;
      localObject1 = localObject2;
      break;
      localObject2 = new ArrayList();
      paramInputStream = localInputStream;
      continue;
      if (localXmlPullParser.getName().equals("camera"))
      {
        paramInputStream = new Cameras();
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("id"))
      {
        localXmlPullParser.next();
        localInputStream.setId(Integer.parseInt(localXmlPullParser.getText()));
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("name"))
      {
        localXmlPullParser.next();
        localInputStream.setName(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("type"))
      {
        localXmlPullParser.next();
        localInputStream.setType(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("dr"))
      {
        localXmlPullParser.next();
        localInputStream.setDr(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("f"))
      {
        localXmlPullParser.next();
        localInputStream.setF(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("n"))
      {
        localXmlPullParser.next();
        localInputStream.setN(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("t1"))
      {
        localXmlPullParser.next();
        localInputStream.setT1(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("t2"))
      {
        localXmlPullParser.next();
        localInputStream.setT2(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("intervalp"))
      {
        localXmlPullParser.next();
        localInputStream.setIntervalp(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("codep"))
      {
        localXmlPullParser.next();
        localInputStream.setCodep(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("intervalv"))
      {
        localXmlPullParser.next();
        localInputStream.setIntervalv(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else
      {
        paramInputStream = localInputStream;
        localObject2 = localObject1;
        if (localXmlPullParser.getName().equals("codev"))
        {
          localXmlPullParser.next();
          localInputStream.setCodev(localXmlPullParser.getText());
          paramInputStream = localInputStream;
          localObject2 = localObject1;
          continue;
          paramInputStream = localInputStream;
          localObject2 = localObject1;
          if (localXmlPullParser.getName().equals("camera"))
          {
            ((List)localObject1).add(localInputStream);
            paramInputStream = null;
            localObject2 = localObject1;
          }
        }
      }
    }
  }
  
  public String serialize(List<Cameras> paramList)
    throws Exception
  {
    XmlSerializer localXmlSerializer = Xml.newSerializer();
    StringWriter localStringWriter = new StringWriter();
    localXmlSerializer.setOutput(localStringWriter);
    localXmlSerializer.startDocument("UTF-8", Boolean.valueOf(true));
    localXmlSerializer.startTag("", "cameras");
    paramList = paramList.iterator();
    for (;;)
    {
      if (!paramList.hasNext())
      {
        localXmlSerializer.endTag("", "cameras");
        localXmlSerializer.endDocument();
        return localStringWriter.toString();
      }
      Cameras localCameras = (Cameras)paramList.next();
      localXmlSerializer.startTag("", "camera");
      localXmlSerializer.attribute("", "id", localCameras.getId());
      localXmlSerializer.startTag("", "name");
      localXmlSerializer.text(localCameras.getName());
      localXmlSerializer.endTag("", "name");
      localXmlSerializer.startTag("", "type");
      localXmlSerializer.text(localCameras.getType());
      localXmlSerializer.endTag("", "type");
      localXmlSerializer.startTag("", "dr");
      localXmlSerializer.text(localCameras.getDr());
      localXmlSerializer.endTag("", "dr");
      localXmlSerializer.startTag("", "f");
      localXmlSerializer.text(localCameras.getF());
      localXmlSerializer.endTag("", "f");
      localXmlSerializer.startTag("", "n");
      localXmlSerializer.text(localCameras.getN());
      localXmlSerializer.endTag("", "n");
      localXmlSerializer.startTag("", "t1");
      localXmlSerializer.text(localCameras.getT1());
      localXmlSerializer.endTag("", "t1");
      localXmlSerializer.startTag("", "t2");
      localXmlSerializer.text(localCameras.getT2());
      localXmlSerializer.endTag("", "t2");
      localXmlSerializer.startTag("", "intervalp");
      localXmlSerializer.text(localCameras.getIntervalp());
      localXmlSerializer.endTag("", "intervalp");
      localXmlSerializer.startTag("", "codep");
      localXmlSerializer.text(localCameras.getCodep());
      localXmlSerializer.endTag("", "codep");
      localXmlSerializer.startTag("", "intervalv");
      localXmlSerializer.text(localCameras.getIntervalv());
      localXmlSerializer.endTag("", "intervalv");
      localXmlSerializer.startTag("", "codev");
      localXmlSerializer.text(localCameras.getCodev());
      localXmlSerializer.endTag("", "codev");
      localXmlSerializer.endTag("", "camera");
    }
  }
}


