package com.yuneec.flight_settings;

import com.yuneec.IPCameraManager.Cameras;
import java.io.InputStream;
import java.util.List;

public abstract interface CameraParser
{
  public abstract List<Cameras> parse(InputStream paramInputStream)
    throws Exception;
  
  public abstract String serialize(List<Cameras> paramList)
    throws Exception;
}


