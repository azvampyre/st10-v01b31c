package com.yuneec.flight_settings;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.widget.SlideVernierView;
import com.yuneec.widget.ThreeSpeedSwitchView;
import com.yuneec.widget.TwoSpeedSwitchView;
import com.yuneec.widget.VernierView;
import java.util.ArrayList;

public class HardwareMonitorST10
  extends Activity
{
  private static final String TAG = "HardwareMonitor10";
  private TwoSpeedSwitchView hm_view_b1;
  private TwoSpeedSwitchView hm_view_b2;
  private TwoSpeedSwitchView hm_view_b3;
  private VernierView hm_view_j1;
  private VernierView hm_view_j2;
  private VernierView hm_view_j3;
  private VernierView hm_view_j4;
  private SlideVernierView hm_view_k1;
  private SlideVernierView hm_view_k2;
  private ThreeSpeedSwitchView hm_view_s1;
  private Button hw_clear;
  private TextView j1_value;
  private TextView j2_value;
  private TextView j3_value;
  private TextView j4_value;
  private TextView k1_value;
  private TextView k2_value;
  private UARTController mController;
  private Handler mUartHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
      {
        paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        switch (paramAnonymousMessage.what)
        {
        }
      }
      for (;;)
      {
        return;
        paramAnonymousMessage = (UARTInfoMessage.Channel)paramAnonymousMessage;
        int i = paramAnonymousMessage.channels.size();
        HardwareMonitorST10.this.setValue(i, paramAnonymousMessage);
      }
    }
  };
  
  private void setValue(int paramInt, UARTInfoMessage.Channel paramChannel)
  {
    this.hm_view_j1.setValue(vernieDataConversion(((Float)paramChannel.channels.get(0)).floatValue()));
    if (vernieDataConversion(((Float)paramChannel.channels.get(0)).floatValue()) != 0) {
      this.j1_value.setTextColor(-65536);
    }
    this.j1_value.setText(String.valueOf(vernieDataConversion(((Float)paramChannel.channels.get(0)).floatValue())));
    this.hm_view_j2.setValue(vernieDataConversion(((Float)paramChannel.channels.get(1)).floatValue()));
    if (vernieDataConversion(((Float)paramChannel.channels.get(1)).floatValue()) != 0) {
      this.j2_value.setTextColor(-65536);
    }
    this.j2_value.setText(String.valueOf(vernieDataConversion(((Float)paramChannel.channels.get(1)).floatValue())));
    this.hm_view_j3.setValue(vernieDataConversion(((Float)paramChannel.channels.get(2)).floatValue()));
    if (vernieDataConversion(((Float)paramChannel.channels.get(2)).floatValue()) != 0) {
      this.j3_value.setTextColor(-65536);
    }
    this.j3_value.setText(String.valueOf(vernieDataConversion(((Float)paramChannel.channels.get(2)).floatValue())));
    this.hm_view_j4.setValue(vernieDataConversion(((Float)paramChannel.channels.get(3)).floatValue()));
    if (vernieDataConversion(((Float)paramChannel.channels.get(3)).floatValue()) != 0) {
      this.j4_value.setTextColor(-65536);
    }
    this.j4_value.setText(String.valueOf(vernieDataConversion(((Float)paramChannel.channels.get(3)).floatValue())));
    this.hm_view_k1.setValue(slideDataConversion(((Float)paramChannel.channels.get(4)).floatValue()));
    this.k1_value.setText(String.valueOf(slideDataConversion(((Float)paramChannel.channels.get(4)).floatValue())));
    this.hm_view_k2.setValue(slideDataConversion(((Float)paramChannel.channels.get(5)).floatValue()));
    this.k2_value.setText(String.valueOf(slideDataConversion(((Float)paramChannel.channels.get(5)).floatValue())));
    this.hm_view_s1.setValue(threeSwitchDataConversion(((Float)paramChannel.channels.get(6)).floatValue()));
    this.hm_view_b1.setValue(twoSwitchDataConversion(((Float)paramChannel.channels.get(7)).floatValue()));
    this.hm_view_b2.setValue(twoSwitchDataConversion(((Float)paramChannel.channels.get(8)).floatValue()));
    this.hm_view_b3.setValue(twoSwitchDataConversion(((Float)paramChannel.channels.get(9)).floatValue()));
  }
  
  private int slideDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 4095.0F) {
      f = 4095.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)(paramFloat / 20.48F);
  }
  
  private int threeSwitchDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 2.0F) {
      f = 2.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)paramFloat;
  }
  
  private int twoSwitchDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 1.0F) {
      f = 1.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)paramFloat;
  }
  
  private int vernieDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 4095.0F) {
      f = 4095.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)(paramFloat / 20.48F - 100.0F);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903070);
    this.hm_view_k1 = ((SlideVernierView)findViewById(2131689617));
    this.hm_view_k2 = ((SlideVernierView)findViewById(2131689614));
    this.hm_view_b1 = ((TwoSpeedSwitchView)findViewById(2131689626));
    this.hm_view_b2 = ((TwoSpeedSwitchView)findViewById(2131689641));
    this.hm_view_b3 = ((TwoSpeedSwitchView)findViewById(2131689672));
    this.hm_view_s1 = ((ThreeSpeedSwitchView)findViewById(2131689623));
    this.hm_view_j1 = ((VernierView)findViewById(2131689657));
    this.hm_view_j2 = ((VernierView)findViewById(2131689654));
    this.hm_view_j3 = ((VernierView)findViewById(2131689666));
    this.hm_view_j4 = ((VernierView)findViewById(2131689669));
    this.k1_value = ((TextView)findViewById(2131689677));
    this.k2_value = ((TextView)findViewById(2131689684));
    this.j1_value = ((TextView)findViewById(2131689678));
    this.j2_value = ((TextView)findViewById(2131689679));
    this.j3_value = ((TextView)findViewById(2131689685));
    this.j4_value = ((TextView)findViewById(2131689686));
    this.hw_clear = ((Button)findViewById(2131689687));
    this.hw_clear.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        HardwareMonitorST10.this.j1_value.setTextColor(-4144960);
        HardwareMonitorST10.this.j2_value.setTextColor(-4144960);
        HardwareMonitorST10.this.j3_value.setTextColor(-4144960);
        HardwareMonitorST10.this.j4_value.setTextColor(-4144960);
      }
    });
  }
  
  protected void onPause()
  {
    super.onPause();
    if (!Utilities.ensureAwaitState(this.mController)) {
      Log.e("HardwareMonitor10", "fails to enter await state");
    }
    Utilities.UartControllerStandBy(this.mController);
    this.mController = null;
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    this.mController.registerReaderHandler(this.mUartHandler);
    this.mController.startReading();
    if (!Utilities.ensureSimState(this.mController)) {
      Log.e("HardwareMonitor10", "fails to enter sim state");
    }
  }
}


