package com.yuneec.flight_settings;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.Channel;
import com.yuneec.uartcontroller.UARTInfoMessage.Trim;
import com.yuneec.widget.KnobsView;
import com.yuneec.widget.SlideVernierView;
import com.yuneec.widget.ThreeSpeedSwitchView;
import com.yuneec.widget.TrimVernierView;
import com.yuneec.widget.TwoSpeedButtonView;
import com.yuneec.widget.VernierView;
import java.util.ArrayList;

public class HardwareMonitor
  extends Activity
{
  private static final String TAG = "HardwareMonitor";
  private TwoSpeedButtonView hm_view_b1;
  private TwoSpeedButtonView hm_view_b2;
  private VernierView hm_view_j1;
  private VernierView hm_view_j2;
  private VernierView hm_view_j3;
  private VernierView hm_view_j4;
  private KnobsView hm_view_k1;
  private KnobsView hm_view_k2;
  private KnobsView hm_view_k3;
  private SlideVernierView hm_view_k4;
  private SlideVernierView hm_view_k5;
  private ThreeSpeedSwitchView hm_view_s1;
  private TwoSpeedButtonView hm_view_s2;
  private TwoSpeedButtonView hm_view_s3;
  private ThreeSpeedSwitchView hm_view_s4;
  private TrimVernierView hm_view_t1;
  private TrimVernierView hm_view_t2;
  private TrimVernierView hm_view_t3;
  private TrimVernierView hm_view_t4;
  private UARTController mController;
  private Handler mUartHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
      {
        paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        switch (paramAnonymousMessage.what)
        {
        }
      }
      for (;;)
      {
        return;
        paramAnonymousMessage = (UARTInfoMessage.Channel)paramAnonymousMessage;
        HardwareMonitor.this.hm_view_b2.setValue(HardwareMonitor.this.twoSwitchDataConversion(((Float)paramAnonymousMessage.channels.get(14)).floatValue()));
        HardwareMonitor.this.hm_view_b1.setValue(HardwareMonitor.this.twoSwitchDataConversion(((Float)paramAnonymousMessage.channels.get(13)).floatValue()));
        HardwareMonitor.this.hm_view_s4.setValue(HardwareMonitor.this.threeSwitchDataConversion(((Float)paramAnonymousMessage.channels.get(12)).floatValue()));
        HardwareMonitor.this.hm_view_s3.setValue(HardwareMonitor.this.twoSwitchDataConversion(((Float)paramAnonymousMessage.channels.get(11)).floatValue()));
        HardwareMonitor.this.hm_view_s2.setValue(HardwareMonitor.this.twoSwitchDataConversion(((Float)paramAnonymousMessage.channels.get(10)).floatValue()));
        HardwareMonitor.this.hm_view_s1.setValue(HardwareMonitor.this.threeSwitchDataConversion(((Float)paramAnonymousMessage.channels.get(9)).floatValue()));
        HardwareMonitor.this.hm_view_k5.setValue(HardwareMonitor.this.slideDataConversion(((Float)paramAnonymousMessage.channels.get(8)).floatValue()));
        HardwareMonitor.this.hm_view_k4.setValue(HardwareMonitor.this.slideDataConversion(((Float)paramAnonymousMessage.channels.get(7)).floatValue()));
        HardwareMonitor.this.hm_view_k3.setValue(HardwareMonitor.this.knobsDataConversion(((Float)paramAnonymousMessage.channels.get(6)).floatValue()));
        HardwareMonitor.this.hm_view_k2.setValue(HardwareMonitor.this.knobsDataConversion(((Float)paramAnonymousMessage.channels.get(5)).floatValue()));
        HardwareMonitor.this.hm_view_k1.setValue(HardwareMonitor.this.knobsDataConversion(((Float)paramAnonymousMessage.channels.get(4)).floatValue()));
        HardwareMonitor.this.hm_view_j4.setValue(HardwareMonitor.this.vernieDataConversion(((Float)paramAnonymousMessage.channels.get(3)).floatValue()));
        HardwareMonitor.this.hm_view_j3.setValue(HardwareMonitor.this.vernieDataConversion(((Float)paramAnonymousMessage.channels.get(2)).floatValue()));
        HardwareMonitor.this.hm_view_j2.setValue(HardwareMonitor.this.vernieDataConversion(((Float)paramAnonymousMessage.channels.get(1)).floatValue()));
        HardwareMonitor.this.hm_view_j1.setValue(HardwareMonitor.this.vernieDataConversion(((Float)paramAnonymousMessage.channels.get(0)).floatValue()));
        continue;
        paramAnonymousMessage = (UARTInfoMessage.Trim)paramAnonymousMessage;
        HardwareMonitor.this.hm_view_t1.setValue(HardwareMonitor.this.vernieTrimDataConversion(paramAnonymousMessage.t1));
        HardwareMonitor.this.hm_view_t2.setValue(HardwareMonitor.this.vernieTrimDataConversion(paramAnonymousMessage.t2));
        HardwareMonitor.this.hm_view_t3.setValue(HardwareMonitor.this.vernieTrimDataConversion(paramAnonymousMessage.t3));
        HardwareMonitor.this.hm_view_t4.setValue(HardwareMonitor.this.vernieTrimDataConversion(paramAnonymousMessage.t4));
      }
    }
  };
  
  private int knobsDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 4095.0F) {
      f = 4095.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return ((int)(paramFloat / 15.17037F) + 225) % 360;
  }
  
  private int slideDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 4095.0F) {
      f = 4095.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)(paramFloat / 20.48F);
  }
  
  private int threeSwitchDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 2.0F) {
      f = 2.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)paramFloat;
  }
  
  private int twoSwitchDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 1.0F) {
      f = 1.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)paramFloat;
  }
  
  private int vernieDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 4095.0F) {
      f = 4095.0F;
    }
    paramFloat = f;
    if (f < 0.0F) {
      paramFloat = 0.0F;
    }
    return (int)(paramFloat / 20.48F - 100.0F);
  }
  
  private int vernieTrimDataConversion(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 20.0F) {
      f = 20.0F;
    }
    paramFloat = f;
    if (f < -20.0F) {
      paramFloat = -20.0F;
    }
    return (int)paramFloat;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903069);
    this.hm_view_k1 = ((KnobsView)findViewById(2131689617));
    this.hm_view_k2 = ((KnobsView)findViewById(2131689614));
    this.hm_view_k3 = ((KnobsView)findViewById(2131689632));
    this.hm_view_k4 = ((SlideVernierView)findViewById(2131689629));
    this.hm_view_k5 = ((SlideVernierView)findViewById(2131689644));
    this.hm_view_b1 = ((TwoSpeedButtonView)findViewById(2131689626));
    this.hm_view_b2 = ((TwoSpeedButtonView)findViewById(2131689641));
    this.hm_view_s2 = ((TwoSpeedButtonView)findViewById(2131689620));
    this.hm_view_s3 = ((TwoSpeedButtonView)findViewById(2131689635));
    this.hm_view_s1 = ((ThreeSpeedSwitchView)findViewById(2131689623));
    this.hm_view_s4 = ((ThreeSpeedSwitchView)findViewById(2131689638));
    this.hm_view_j1 = ((VernierView)findViewById(2131689657));
    this.hm_view_j2 = ((VernierView)findViewById(2131689654));
    this.hm_view_j3 = ((VernierView)findViewById(2131689666));
    this.hm_view_j4 = ((VernierView)findViewById(2131689669));
    this.hm_view_t1 = ((TrimVernierView)findViewById(2131689651));
    this.hm_view_t2 = ((TrimVernierView)findViewById(2131689648));
    this.hm_view_t3 = ((TrimVernierView)findViewById(2131689660));
    this.hm_view_t4 = ((TrimVernierView)findViewById(2131689663));
  }
  
  protected void onPause()
  {
    super.onPause();
    if (!Utilities.ensureAwaitState(this.mController)) {
      Log.e("HardwareMonitor", "fails to enter await state");
    }
    Utilities.UartControllerStandBy(this.mController);
    this.mController = null;
  }
  
  protected void onResume()
  {
    super.onResume();
    this.mController = UARTController.getInstance();
    this.mController.registerReaderHandler(this.mUartHandler);
    this.mController.startReading();
    if (!Utilities.ensureSimState(this.mController)) {
      Log.e("HardwareMonitor", "fails to enter sim state");
    }
  }
}


