package com.yuneec.flight_settings;

import android.util.Xml;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class WifiPasswordService
{
  public static List<WifiPassword> getWifiPasswords(InputStream paramInputStream)
    throws Exception
  {
    Object localObject1 = null;
    InputStream localInputStream = null;
    XmlPullParser localXmlPullParser = Xml.newPullParser();
    localXmlPullParser.setInput(paramInputStream, "UTF-8");
    int i = localXmlPullParser.getEventType();
    if (i == 1) {
      return (List<WifiPassword>)localObject1;
    }
    paramInputStream = localInputStream;
    Object localObject2 = localObject1;
    switch (i)
    {
    default: 
      localObject2 = localObject1;
      paramInputStream = localInputStream;
    }
    for (;;)
    {
      i = localXmlPullParser.next();
      localInputStream = paramInputStream;
      localObject1 = localObject2;
      break;
      localObject2 = new ArrayList();
      paramInputStream = localInputStream;
      continue;
      if (localXmlPullParser.getName().equals("wifipassword"))
      {
        paramInputStream = new WifiPassword();
        localObject2 = localObject1;
      }
      else if (localXmlPullParser.getName().equals("ssid"))
      {
        localXmlPullParser.next();
        localInputStream.setSsid(localXmlPullParser.getText());
        paramInputStream = localInputStream;
        localObject2 = localObject1;
      }
      else
      {
        paramInputStream = localInputStream;
        localObject2 = localObject1;
        if (localXmlPullParser.getName().equals("password"))
        {
          localXmlPullParser.next();
          localInputStream.setPassword(localXmlPullParser.getText());
          paramInputStream = localInputStream;
          localObject2 = localObject1;
          continue;
          paramInputStream = localInputStream;
          localObject2 = localObject1;
          if (localXmlPullParser.getName().equals("wifipassword"))
          {
            ((List)localObject1).add(localInputStream);
            paramInputStream = null;
            localObject2 = localObject1;
          }
        }
      }
    }
  }
  
  public static void save(List<WifiPassword> paramList, OutputStream paramOutputStream)
    throws Exception
  {
    XmlSerializer localXmlSerializer = Xml.newSerializer();
    localXmlSerializer.setOutput(paramOutputStream, "UTF-8");
    localXmlSerializer.startDocument("UTF-8", Boolean.valueOf(true));
    localXmlSerializer.startTag(null, "wifipasswords");
    paramList = paramList.iterator();
    for (;;)
    {
      if (!paramList.hasNext())
      {
        localXmlSerializer.endTag(null, "wifipasswords");
        localXmlSerializer.endDocument();
        paramOutputStream.flush();
        paramOutputStream.close();
        return;
      }
      WifiPassword localWifiPassword = (WifiPassword)paramList.next();
      localXmlSerializer.startTag(null, "wifipassword");
      localXmlSerializer.startTag(null, "ssid");
      localXmlSerializer.text(localWifiPassword.getSsid());
      localXmlSerializer.endTag(null, "ssid");
      localXmlSerializer.startTag(null, "password");
      localXmlSerializer.text(localWifiPassword.getPassword());
      localXmlSerializer.endTag(null, "password");
      localXmlSerializer.endTag(null, "wifipassword");
    }
  }
}


