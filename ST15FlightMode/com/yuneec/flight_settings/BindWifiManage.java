package com.yuneec.flight_settings;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BindWifiManage
{
  private List<ScanResult> scanResultList;
  private List<WifiConfiguration> wifiConfigList;
  private WifiInfo wifiInfo;
  private WifiManager.WifiLock wifiLock;
  private WifiManager wifiManager;
  
  public BindWifiManage(WifiManager paramWifiManager)
  {
    this.wifiManager = paramWifiManager;
    this.wifiInfo = paramWifiManager.getConnectionInfo();
  }
  
  public boolean addNetWordLink(WifiConfiguration paramWifiConfiguration)
  {
    int i = this.wifiManager.addNetwork(paramWifiConfiguration);
    return this.wifiManager.enableNetwork(i, true);
  }
  
  public boolean closeWifi()
  {
    if (!this.wifiManager.isWifiEnabled()) {}
    for (boolean bool = true;; bool = this.wifiManager.setWifiEnabled(false)) {
      return bool;
    }
  }
  
  public void connectConfiguration(int paramInt)
  {
    if (paramInt > this.wifiConfigList.size()) {}
    for (;;)
    {
      return;
      this.wifiManager.enableNetwork(((WifiConfiguration)this.wifiConfigList.get(paramInt)).networkId, true);
    }
  }
  
  public void createWifiLock()
  {
    this.wifiLock = this.wifiManager.createWifiLock("fly");
  }
  
  public boolean disableNetWordLick(int paramInt)
  {
    this.wifiManager.disableNetwork(paramInt);
    return this.wifiManager.disconnect();
  }
  
  public void displaySSID(int paramInt)
  {
    ((WifiConfiguration)this.wifiConfigList.get(paramInt)).hiddenSSID = false;
  }
  
  public String getBSSID()
  {
    if (this.wifiInfo == null) {}
    for (String str = null;; str = this.wifiInfo.getBSSID()) {
      return str;
    }
  }
  
  public int getCurrentNetId()
  {
    if (this.wifiInfo == null) {}
    for (Integer localInteger = null;; localInteger = Integer.valueOf(this.wifiInfo.getNetworkId())) {
      return localInteger.intValue();
    }
  }
  
  public int getIP()
  {
    if (this.wifiInfo == null) {}
    for (Integer localInteger = null;; localInteger = Integer.valueOf(this.wifiInfo.getIpAddress())) {
      return localInteger.intValue();
    }
  }
  
  public int getLevel(int paramInt)
  {
    return ((ScanResult)this.scanResultList.get(paramInt)).level;
  }
  
  public String getMac()
  {
    if (this.wifiInfo == null) {}
    for (String str = "";; str = this.wifiInfo.getMacAddress()) {
      return str;
    }
  }
  
  public String getSSID()
  {
    if (this.wifiInfo == null) {}
    for (String str = null;; str = this.wifiInfo.getSSID()) {
      return str;
    }
  }
  
  public ArrayList<String> getSSIDList()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator;
    if (this.scanResultList != null) {
      localIterator = this.scanResultList.iterator();
    }
    for (;;)
    {
      if (!localIterator.hasNext()) {
        return localArrayList;
      }
      localArrayList.add(((ScanResult)localIterator.next()).SSID);
    }
  }
  
  public List<WifiConfiguration> getWifiConfigList()
  {
    return this.wifiConfigList;
  }
  
  public List<ScanResult> getWifiList()
  {
    return this.scanResultList;
  }
  
  public boolean getWifiStatus()
  {
    return this.wifiManager.isWifiEnabled();
  }
  
  public String getwifiInfo()
  {
    if (this.wifiInfo == null) {}
    for (String str = null;; str = this.wifiInfo.toString()) {
      return str;
    }
  }
  
  public void hiddenSSID(int paramInt)
  {
    ((WifiConfiguration)this.wifiConfigList.get(paramInt)).hiddenSSID = true;
  }
  
  public void lockWifi()
  {
    this.wifiLock.acquire();
  }
  
  public StringBuilder lookUpscan()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0;; i++)
    {
      if (i >= this.scanResultList.size()) {
        return localStringBuilder;
      }
      localStringBuilder.append("编号：" + (i + 1));
      localStringBuilder.append(((ScanResult)this.scanResultList.get(i)).toString());
      localStringBuilder.append("\n");
    }
  }
  
  public boolean openWifi()
  {
    if (!this.wifiManager.isWifiEnabled()) {}
    for (boolean bool = this.wifiManager.setWifiEnabled(true);; bool = false) {
      return bool;
    }
  }
  
  public boolean removeNetworkLink(int paramInt)
  {
    return this.wifiManager.removeNetwork(paramInt);
  }
  
  public void startScan()
  {
    this.wifiManager.startScan();
    this.scanResultList = this.wifiManager.getScanResults();
    this.wifiConfigList = this.wifiManager.getConfiguredNetworks();
  }
  
  public void unLockWifi()
  {
    if (!this.wifiLock.isHeld()) {
      this.wifiLock.release();
    }
  }
}


