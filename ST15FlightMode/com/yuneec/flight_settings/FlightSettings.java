package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.yuneec.channelsettings.ChannelSettings;
import com.yuneec.galleryloader.Gallery;
import com.yuneec.widget.MyToast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlightSettings
  extends Activity
{
  public static final int AMBA_2_FLAG = 8;
  public static final int AMBA_2_FLAG_PRO = 32;
  public static final int AMBA_FLAG = 4;
  public static final String CAMERA_CURRENT_SELECTED = "camera_current_selected";
  public static final String CAMERA_SELECTED_INFO = "camera_selected_info";
  public static final String CAMERA_SELECTED_POSITION_VALUE = "camera_selected_position_value";
  public static final String CAMERA_TYPE_VALUE = "camera_type_flag";
  public static final int CC4IN1_FLAG = 1;
  public static final String FLIGHT_SETTINGS_FILE = "flight_setting_value";
  public static final String FLIGHT_SETTINGS_MODE = "mode_select_value";
  public static final int GOPRO_FLAG = 16;
  public static final String MASTER_UNIT = "master_unit";
  public static final int MODE_SELECT_1 = 1;
  public static final int MODE_SELECT_2 = 2;
  public static final int MODE_SELECT_3 = 3;
  public static final int MODE_SELECT_4 = 4;
  public static final int REMOTE_ZOOM_FLAG = 2;
  private static final int ST10_BIND = 0;
  private static final int ST10_CAMERA_SELECT = 1;
  private static final int ST10_HARDWARE_MONITOR = 3;
  private static final int ST10_MODE_SELECT = 2;
  private static final int ST10_OTHER_SETTINGS = 4;
  private static final int ST12_BIND = 0;
  private static final int ST12_CAMERA_SELECT = 1;
  private static final int ST12_HARDWARE_MONITOR = 3;
  private static final int ST12_MODE_SELECT = 2;
  private static final int ST12_OTHER_SETTINGS = 4;
  private static final int ST15_BIND = 0;
  private static final int ST15_CAMERA_SELECT = 3;
  private static final int ST15_CHANNEL_SETTINGS = 1;
  private static final int ST15_HARDWARE_MONITOR = 6;
  private static final int ST15_MODE_SELECT = 5;
  private static final int ST15_ONLINE_PICTURE = 7;
  private static final int ST15_QUICK_REVIEW = 4;
  private static final int ST15_SWITCH_SELECT = 2;
  public static final String TAG = "FlightSettings";
  public static final int UNIT_BSW = 2;
  public static final int UNIT_MASTER = 2;
  public static final int UNIT_METRIC = 1;
  public static final int UNIT_SERVANT = 1;
  public static final String VELOCITY_UNIT = "velocity_unit";
  private List<Map<String, Object>> data = new ArrayList();
  private ListView mListView;
  private AdapterView.OnItemClickListener mST10OnClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      switch (paramAnonymousInt)
      {
      }
      for (;;)
      {
        return;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, Bind.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, CameraSelect.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, ModeSelect.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, HardwareMonitorST10.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, OtherSettings.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
      }
    }
  };
  private AdapterView.OnItemClickListener mST12OnClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      switch (paramAnonymousInt)
      {
      }
      for (;;)
      {
        return;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, Bind.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, CameraSelect.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, ModeSelect.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, HardwareMonitorST12.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, OtherSettings.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
      }
    }
  };
  private AdapterView.OnItemClickListener mST15OnClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      switch (paramAnonymousInt)
      {
      }
      for (;;)
      {
        return;
        paramAnonymousAdapterView = new Intent(FlightSettings.this, Bind.class);
        FlightSettings.this.startActivity(paramAnonymousAdapterView);
        continue;
        if (FlightSettings.this.modelId > 0L)
        {
          paramAnonymousAdapterView = new Intent(FlightSettings.this, ChannelSettings.class);
          FlightSettings.this.startActivity(paramAnonymousAdapterView);
        }
        else
        {
          MyToast.makeText(FlightSettings.this, 2131296358, 0, 0).show();
          continue;
          if (FlightSettings.this.modelId > 0L)
          {
            paramAnonymousAdapterView = new Intent(FlightSettings.this, SwitchSelect.class);
            FlightSettings.this.startActivity(paramAnonymousAdapterView);
          }
          else
          {
            MyToast.makeText(FlightSettings.this, 2131296358, 0, 0).show();
            continue;
            paramAnonymousAdapterView = new Intent(FlightSettings.this, CameraSelect.class);
            FlightSettings.this.startActivity(paramAnonymousAdapterView);
            continue;
            paramAnonymousAdapterView = new Intent(FlightSettings.this, QuickReview.class);
            FlightSettings.this.startActivity(paramAnonymousAdapterView);
            continue;
            paramAnonymousAdapterView = new Intent(FlightSettings.this, ModeSelect.class);
            FlightSettings.this.startActivity(paramAnonymousAdapterView);
            continue;
            paramAnonymousAdapterView = new Intent(FlightSettings.this, HardwareMonitor.class);
            FlightSettings.this.startActivity(paramAnonymousAdapterView);
            continue;
            paramAnonymousAdapterView = new Intent(FlightSettings.this, Gallery.class);
            FlightSettings.this.startActivity(paramAnonymousAdapterView);
          }
        }
      }
    }
  };
  private long modelId;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903060);
    this.modelId = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    HashMap localHashMap3;
    HashMap localHashMap1;
    HashMap localHashMap4;
    HashMap localHashMap2;
    if (("ST10".equals("ST10")) || ("ST10".equals("ST12")))
    {
      localHashMap3 = new HashMap();
      localHashMap1 = new HashMap();
      localHashMap4 = new HashMap();
      localHashMap2 = new HashMap();
      paramBundle = new HashMap();
      new HashMap();
      localHashMap3.put("title_string", getResources().getString(2131296404));
      localHashMap3.put("drawable_id", Integer.valueOf(2130837686));
      localHashMap1.put("title_string", getResources().getString(2131296407));
      localHashMap1.put("drawable_id", Integer.valueOf(2130837689));
      localHashMap4.put("title_string", getResources().getString(2131296410));
      localHashMap4.put("drawable_id", Integer.valueOf(2130837698));
      localHashMap2.put("title_string", getResources().getString(2131296411));
      localHashMap2.put("drawable_id", Integer.valueOf(2130837695));
      paramBundle.put("title_string", getResources().getString(2131296412));
      paramBundle.put("drawable_id", Integer.valueOf(2130837701));
      this.data.add(localHashMap3);
      this.data.add(localHashMap1);
      this.data.add(localHashMap4);
      this.data.add(localHashMap2);
      this.data.add(paramBundle);
    }
    label875:
    for (;;)
    {
      this.mListView = ((ListView)findViewById(2131689592));
      this.mListView.setAdapter(new MyAdapter(this, this.data));
      if ("ST10".equals("ST10")) {
        this.mListView.setOnItemClickListener(this.mST10OnClickListener);
      }
      for (;;)
      {
        return;
        if (!"ST10".equals("ST15")) {
          break label875;
        }
        localHashMap2 = new HashMap();
        localHashMap1 = new HashMap();
        HashMap localHashMap6 = new HashMap();
        HashMap localHashMap5 = new HashMap();
        localHashMap3 = new HashMap();
        paramBundle = new HashMap();
        localHashMap4 = new HashMap();
        HashMap localHashMap7 = new HashMap();
        localHashMap2.put("title_string", getResources().getString(2131296404));
        localHashMap2.put("drawable_id", Integer.valueOf(2130837686));
        localHashMap1.put("title_string", getResources().getString(2131296405));
        localHashMap1.put("drawable_id", Integer.valueOf(2130837692));
        localHashMap6.put("title_string", getResources().getString(2131296406));
        localHashMap6.put("drawable_id", Integer.valueOf(2130837707));
        localHashMap5.put("title_string", getResources().getString(2131296407));
        localHashMap5.put("drawable_id", Integer.valueOf(2130837689));
        localHashMap3.put("title_string", getResources().getString(2131296408));
        localHashMap3.put("drawable_id", Integer.valueOf(2130837704));
        paramBundle.put("title_string", getResources().getString(2131296410));
        paramBundle.put("drawable_id", Integer.valueOf(2130837698));
        localHashMap4.put("title_string", getResources().getString(2131296411));
        localHashMap4.put("drawable_id", Integer.valueOf(2130837695));
        localHashMap7.put("title_string", getResources().getString(2131296409));
        localHashMap7.put("drawable_id", Integer.valueOf(2130837704));
        this.data.add(localHashMap2);
        this.data.add(localHashMap1);
        this.data.add(localHashMap6);
        this.data.add(localHashMap5);
        this.data.add(localHashMap3);
        this.data.add(paramBundle);
        this.data.add(localHashMap4);
        this.data.add(localHashMap7);
        break;
        if ("ST10".equals("ST15")) {
          this.mListView.setOnItemClickListener(this.mST15OnClickListener);
        } else if ("ST10".equals("ST12")) {
          this.mListView.setOnItemClickListener(this.mST12OnClickListener);
        }
      }
    }
  }
  
  class MyAdapter
    extends BaseAdapter
  {
    private List<Map<String, Object>> data;
    private List<View> holder;
    private ImageView imageView;
    private LayoutInflater inflater;
    private TextView titleText;
    
    public MyAdapter(List<Map<String, Object>> paramList)
    {
      this.inflater = LayoutInflater.from(paramList);
      List localList;
      this.data = localList;
    }
    
    public int getCount()
    {
      return this.data.size();
    }
    
    public Object getItem(int paramInt)
    {
      return this.data.get(paramInt);
    }
    
    public long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
      {
        paramView = this.inflater.inflate(2130903058, null);
        this.imageView = ((ImageView)paramView.findViewById(2131689586));
        this.titleText = ((TextView)paramView.findViewById(2131689514));
        this.holder = new ArrayList();
        this.holder.add(this.imageView);
        this.holder.add(this.titleText);
        paramView.setTag(this.holder);
      }
      for (;;)
      {
        ((ImageView)this.holder.get(0)).setImageResource(((Integer)((Map)this.data.get(paramInt)).get("drawable_id")).intValue());
        ((TextView)this.holder.get(1)).setText((String)((Map)this.data.get(paramInt)).get("title_string"));
        return paramView;
        this.holder = ((ArrayList)paramView.getTag());
      }
    }
  }
}


