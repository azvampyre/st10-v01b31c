package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.Toast;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.rtvplayer.RTVPlayer;
import com.yuneec.rtvplayer.RTVPlayer.VideoEventCallback;
import com.yuneec.widget.MyToast;

public class QuickReviewPlayActivity
  extends Activity
{
  private static final String TAG = "QuickReviewActivity";
  private String mFullPathName;
  private Handler mHandler = new Handler();
  private RTVPlayer mPlayer;
  private String mShortName;
  private boolean mSurfaceCreated = false;
  private RTVPlayer.VideoEventCallback mVideoEventCallback = new RTVPlayer.VideoEventCallback()
  {
    public void onPlayerEncoutneredError()
    {
      MyToast.makeText(QuickReviewPlayActivity.this, 2131296479, 0, 1).show();
    }
    
    public void onPlayerEndReached() {}
    
    public void onPlayerPlayerRecordingFinished() {}
    
    public void onPlayerPlaying() {}
    
    public void onPlayerRecordableChanged() {}
    
    public void onPlayerSnapshotTaken() {}
    
    public void onPlayerStopped()
    {
      MyToast.makeText(QuickReviewPlayActivity.this, 2131296480, 0, 1).show();
    }
    
    public void onPlayerSurfaceChanged(SurfaceHolder paramAnonymousSurfaceHolder, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    
    public void onPlayerSurfaceCreated(SurfaceHolder paramAnonymousSurfaceHolder)
    {
      QuickReviewPlayActivity.this.mSurfaceCreated = true;
    }
    
    public void onPlayerSurfaceDestroyed(SurfaceHolder paramAnonymousSurfaceHolder)
    {
      QuickReviewPlayActivity.this.mSurfaceCreated = false;
    }
  };
  private Runnable playReviewRunnable = new Runnable()
  {
    public void run()
    {
      if (QuickReviewPlayActivity.this.mSurfaceCreated) {
        QuickReviewPlayActivity.this.mPlayer.play(QuickReviewPlayActivity.this.mFullPathName);
      }
      for (;;)
      {
        return;
        QuickReviewPlayActivity.this.mHandler.postDelayed(QuickReviewPlayActivity.this.playReviewRunnable, 500L);
      }
    }
  };
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903099);
    this.mShortName = getIntent().getStringExtra("short_file_name");
    if (this.mShortName == null)
    {
      Log.w("QuickReviewActivity", "No File specified, activity about to finish");
      finish();
    }
    for (;;)
    {
      return;
      paramBundle = (SurfaceView)findViewById(2131689842);
      this.mPlayer = RTVPlayer.getPlayer(2);
      this.mPlayer.init(this, 1, false);
      this.mPlayer.setSurfaceView(paramBundle);
      this.mPlayer.setVideoEventCallback(this.mVideoEventCallback);
      if (this.mShortName != null)
      {
        this.mFullPathName = ("file:///sdcard/FPV-Video/Local/" + this.mShortName);
        this.mHandler.postDelayed(this.playReviewRunnable, 500L);
      }
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.mPlayer.deinit();
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 3) && (!paramKeyEvent.isCanceled())) {
      Utilities.backToFlightScreen(this);
    }
    for (boolean bool = true;; bool = super.onKeyUp(paramInt, paramKeyEvent)) {
      return bool;
    }
  }
  
  protected void onPause()
  {
    super.onPause();
    this.mPlayer.stop();
    this.mHandler.removeCallbacks(this.playReviewRunnable);
  }
}


