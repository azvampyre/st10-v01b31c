package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.widget.TwoButtonPopDialog;

public class ModeSelect
  extends Activity
  implements RadioGroup.OnCheckedChangeListener
{
  private static final String TAG = "ModeSelect";
  private int current_mode;
  private TwoButtonPopDialog mConfirmDialog;
  private int mLastMode;
  private SharedPreferences mPrefs;
  private RadioGroup mode_select;
  private boolean shouldConfirm = true;
  
  private void initSelectedMode()
  {
    if (this.current_mode == 1) {
      this.mode_select.check(2131689762);
    }
    for (;;)
    {
      return;
      if (this.current_mode == 2) {
        this.mode_select.check(2131689763);
      } else if (this.current_mode == 3) {
        this.mode_select.check(2131689764);
      } else if (this.current_mode == 4) {
        this.mode_select.check(2131689765);
      }
    }
  }
  
  private void showConfirmDialog()
  {
    if (this.mConfirmDialog == null)
    {
      this.mConfirmDialog = new TwoButtonPopDialog(this);
      this.mConfirmDialog.adjustHeight(380);
      this.mConfirmDialog.setTitle(2131296393);
      this.mConfirmDialog.setMessage(2131296394);
      this.mConfirmDialog.setPositiveButton(17039379, new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          ModeSelect.this.mConfirmDialog.dismiss();
        }
      });
      this.mConfirmDialog.setNegativeButton(17039369, new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          ModeSelect.this.mConfirmDialog.dismiss();
          ModeSelect.this.shouldConfirm = false;
          ModeSelect.this.current_mode = ModeSelect.this.mLastMode;
          if (ModeSelect.this.current_mode == 1) {
            ((RadioButton)ModeSelect.this.findViewById(2131689762)).setChecked(true);
          }
          for (;;)
          {
            return;
            if (ModeSelect.this.current_mode == 2) {
              ((RadioButton)ModeSelect.this.findViewById(2131689763)).setChecked(true);
            } else if (ModeSelect.this.current_mode == 3) {
              ((RadioButton)ModeSelect.this.findViewById(2131689764)).setChecked(true);
            } else if (ModeSelect.this.current_mode == 4) {
              ((RadioButton)ModeSelect.this.findViewById(2131689765)).setChecked(true);
            }
          }
        }
      });
    }
    this.mConfirmDialog.show();
  }
  
  public void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    this.mLastMode = this.current_mode;
    if (paramInt == 2131689762)
    {
      this.current_mode = 1;
      this.mPrefs.edit().putInt("mode_select_value", this.current_mode).commit();
      DataProviderHelper.changeMode(getApplicationContext(), this.current_mode);
      if (!this.shouldConfirm) {
        break label121;
      }
      showConfirmDialog();
    }
    for (;;)
    {
      return;
      if (paramInt == 2131689763)
      {
        this.current_mode = 2;
        break;
      }
      if (paramInt == 2131689764)
      {
        this.current_mode = 3;
        break;
      }
      if (paramInt == 2131689765)
      {
        this.current_mode = 4;
        break;
      }
      Log.e("ModeSelect", "no mode selected !");
      continue;
      label121:
      this.shouldConfirm = true;
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903078);
    this.mode_select = ((RadioGroup)findViewById(2131689761));
    this.mPrefs = getSharedPreferences("flight_setting_value", 0);
    this.current_mode = this.mPrefs.getInt("mode_select_value", 2);
    this.mLastMode = this.current_mode;
    initSelectedMode();
    this.mode_select.setOnCheckedChangeListener(this);
  }
}


