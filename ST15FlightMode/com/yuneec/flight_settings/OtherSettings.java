package com.yuneec.flight_settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

public class OtherSettings
  extends Activity
{
  private int isMetricOrImperial;
  private SharedPreferences mPrefs;
  private Switch unit_switch;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(128);
    setContentView(2130903093);
    this.mPrefs = getSharedPreferences("flight_setting_value", 0);
    this.isMetricOrImperial = this.mPrefs.getInt("velocity_unit", 2);
    this.unit_switch = ((Switch)findViewById(2131689822));
    this.unit_switch.setOnCheckedChangeListener(new GenderOnCheckedChangeListener(null));
    if (this.isMetricOrImperial == 2) {
      this.unit_switch.setChecked(true);
    }
    for (;;)
    {
      return;
      this.unit_switch.setChecked(false);
    }
  }
  
  private class GenderOnCheckedChangeListener
    implements CompoundButton.OnCheckedChangeListener
  {
    private GenderOnCheckedChangeListener() {}
    
    public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
    {
      if (paramCompoundButton.equals(OtherSettings.this.unit_switch))
      {
        if (!paramBoolean) {
          break label45;
        }
        OtherSettings.this.mPrefs.edit().putInt("velocity_unit", 2).commit();
      }
      for (;;)
      {
        return;
        label45:
        OtherSettings.this.mPrefs.edit().putInt("velocity_unit", 1).commit();
      }
    }
  }
}


