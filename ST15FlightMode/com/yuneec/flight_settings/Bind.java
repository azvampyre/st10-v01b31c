package com.yuneec.flight_settings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.yuneec.IPCameraManager.IPCameraManager;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.flightmode15.Utilities.ReceiverInfomation;
import com.yuneec.model_select.ModelSelectMain;
import com.yuneec.uartcontroller.UARTController;
import com.yuneec.uartcontroller.UARTInfoMessage;
import com.yuneec.uartcontroller.UARTInfoMessage.RxBindInfo;
import com.yuneec.widget.BaseDialog;
import com.yuneec.widget.MyProgressDialog;
import com.yuneec.widget.MyToast;
import com.yuneec.widget.OneButtonPopDialog;
import com.yuneec.widget.TwoButtonPopDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Bind
  extends Activity
{
  private static final int BIND_MODEL_FIRST_TIMEOUT = 800;
  private static final int BIND_MODEL_TIMEOUT = 500;
  private static final int MODEL_SELECT_REQ = 1001;
  private static final int REFRESH_MSG = 291;
  private static final String TAG = "Bind";
  private WifiArrayAdapter FPVWifiListArrayAdapter = null;
  private AdapterView.OnItemClickListener adapterOnItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      switch (paramAnonymousAdapterView.getId())
      {
      }
      for (;;)
      {
        return;
        paramAnonymousAdapterView = (UARTInfoMessage.RxBindInfo)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
        if (Bind.this.checkRxCompatibility(paramAnonymousAdapterView))
        {
          Bind.this.mCurrentSelectedModel = paramAnonymousAdapterView;
          Bind.this.bindRC();
          continue;
          Bind.this.mCurrentFPVItem = ((ScanResult)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt));
          if (Bind.this.mCurrentFPVItem != null) {
            Bind.this.bindFPV();
          }
        }
      }
    }
  };
  private ListView bind_camera_list;
  private ListView bind_model_list;
  private BindWifiManage bwm;
  private String fpv_ssid_password = new String();
  private boolean isAuthenticationError;
  private boolean isModelBound = false;
  private boolean isWifiConnect;
  private TextView mAicraftCameraHeader;
  private TextView mAicraftModelHeader;
  private int mBindModelRetryTimes = 0;
  private Runnable mBindModelTimeoutRunnable = new Runnable()
  {
    private static final int RETRY_TIMES = 3;
    
    public void run()
    {
      if (Bind.this.isModelBound) {
        Log.i("Bind", "model has been bound,timeout ignored");
      }
      for (;;)
      {
        return;
        if (Bind.this.mController == null)
        {
          Log.i("Bind", "user quits,don't care the timeout of the bind");
          if (Bind.this.mProgressDialog != null) {
            Bind.this.dismissProgressDialog();
          }
        }
        else
        {
          int i = Bind.this.mController.queryBindState();
          Log.d("Bind", "M4 bind state: " + i);
          if ((i > 0) && (i == Bind.this.getRxAddr()))
          {
            Bind.this.afterBindModel();
          }
          else if (Bind.this.mBindModelRetryTimes >= 3)
          {
            Log.w("Bind", "too many attempts,bind failed");
            Bind.this.dismissProgressDialog();
            Bind.this.showResult("Bind Model Failed");
          }
          else
          {
            i = Bind.this.getRxAddr();
            if (i == -1)
            {
              Log.e("Bind", "mBindModelTimeoutRunnable shoud never be here");
              Bind.this.dismissProgressDialog();
              Bind.this.showResult("Bind Model Failed");
            }
            else
            {
              Bind.this.mController.unbind(true, 3);
              Bind.this.mController.bind(false, i);
              Bind localBind = Bind.this;
              localBind.mBindModelRetryTimes += 1;
              Bind.this.mHandler.postDelayed(Bind.this.mBindModelTimeoutRunnable, 500L);
            }
          }
        }
      }
    }
  };
  private UARTController mController;
  private ScanResult mCurrentFPVItem = null;
  private int mCurrentRXType = -1;
  private UARTInfoMessage.RxBindInfo mCurrentSelectedModel = null;
  private final View.OnClickListener mGotoModelSelectListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Bind.this.startActivityForResult(new Intent(Bind.this, ModelSelectMain.class), 1001);
    }
  };
  private Handler mHandler = new Handler();
  private IPCameraManager mIPCameraManager;
  private InputStream mInputStream;
  private IntentFilter mIntentFilter = new IntentFilter();
  private SharedPreferences mPrefs;
  private MyProgressDialog mProgressDialog;
  private RxBindArrayAdapter mRxAdapter = null;
  private ArrayList<UARTInfoMessage.RxBindInfo> mRxList = new ArrayList();
  private ArrayList<ScanResult> mScanFPVWifiResultList = new ArrayList();
  private Handler mUartHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      int i;
      if ((paramAnonymousMessage.obj instanceof UARTInfoMessage))
      {
        paramAnonymousMessage = (UARTInfoMessage)paramAnonymousMessage.obj;
        if (paramAnonymousMessage.what != 11) {
          break label161;
        }
        paramAnonymousMessage = (UARTInfoMessage.RxBindInfo)paramAnonymousMessage;
        Log.d("Bind", "bind info, rxAddr: " + paramAnonymousMessage.node_id + ", txAddr: " + paramAnonymousMessage.tx_addr);
        i = 0;
        if (i < Bind.this.mRxAdapter.getCount()) {
          break label107;
        }
        Bind.this.mRxAdapter.add(paramAnonymousMessage);
        Bind.this.bind_model_list.invalidateViews();
      }
      for (;;)
      {
        return;
        label107:
        if (((UARTInfoMessage.RxBindInfo)Bind.this.mRxAdapter.getItem(i)).node_id == paramAnonymousMessage.node_id) {
          Bind.this.mRxAdapter.remove((UARTInfoMessage.RxBindInfo)Bind.this.mRxAdapter.getItem(i));
        }
        i++;
        break;
        label161:
        if (paramAnonymousMessage.what == 1011)
        {
          Log.d("Bind", "receiver bind rsp");
          if (!Bind.this.isModelBound)
          {
            Bind.this.mHandler.removeCallbacks(Bind.this.mBindModelTimeoutRunnable);
            if (Bind.this.getRxAddr() > 0)
            {
              Bind.this.afterBindModel();
            }
            else
            {
              Log.e("Bind", "Receiver bind rsp ERROR: No RxAddr!");
              Bind.this.dismissProgressDialog();
              Bind.this.showResult("Bind Model Failed");
            }
          }
        }
      }
    }
  };
  private Runnable mWifiEnabledRunnable = new Runnable()
  {
    public void run()
    {
      Bind.this.refreshWiFiList();
    }
  };
  private Runnable mWifiTimoutRunnable = new Runnable()
  {
    int i = 0;
    
    public void run()
    {
      SupplicantState localSupplicantState = Bind.this.wifiManager.getConnectionInfo().getSupplicantState();
      if (Bind.this.isAuthenticationError)
      {
        this.i = 0;
        Bind.this.dismissProgressDialog();
        Bind.this.mHandler.removeCallbacks(Bind.this.mWifiTimoutRunnable);
        Bind.this.showResult("Unable to connect to Camera");
      }
      for (;;)
      {
        return;
        if (localSupplicantState.equals(SupplicantState.COMPLETED))
        {
          this.i = 0;
          Bind.this.isWifiConnect = true;
          Bind.this.dismissProgressDialog();
          Bind.this.mHandler.removeCallbacks(Bind.this.mWifiTimoutRunnable);
          if (406L == Bind.this.modeType)
          {
            Bind.this.mPrefs.edit().putInt("camera_type_flag", CameraSelect.parseType("amba_cgo3_pro")).commit();
            Bind.this.mPrefs.edit().putInt("camera_selected_position_value", 4).commit();
          }
          Bind.this.showResult("success");
        }
        else
        {
          if ((Bind.this.mProgressDialog != null) && (Bind.this.mProgressDialog.isShowing())) {
            Bind.this.mProgressDialog.setMessage(Bind.this.getResources().getText(2131296436) + "\n");
          }
          if (this.i < 300)
          {
            this.i += 1;
            Bind.this.mHandler.postDelayed(Bind.this.mWifiTimoutRunnable, 100L);
          }
          else
          {
            this.i = 0;
            Bind.this.dismissProgressDialog();
            Bind.this.mHandler.removeCallbacks(Bind.this.mWifiTimoutRunnable);
            Bind.this.showResult("Unable to connect to Camera");
          }
        }
      }
    }
  };
  private long modeType;
  private boolean noModel;
  private FileOutputStream outStream;
  private BroadcastReceiver receiverWifi = new BroadcastReceiver()
  {
    private SupplicantState mPreviousState = null;
    
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      if (Bind.this.wifiManager.isWifiEnabled())
      {
        paramAnonymousContext = paramAnonymousIntent.getAction();
        Log.i("Bind", paramAnonymousContext);
        if (!paramAnonymousContext.equals("android.net.wifi.SCAN_RESULTS")) {
          break label54;
        }
        Bind.this.mHandler.post(new Runnable()
        {
          public void run()
          {
            Bind.this.initFPVWifiSSIDList();
          }
        });
      }
      for (;;)
      {
        return;
        label54:
        if (paramAnonymousContext.equals("android.net.wifi.supplicant.STATE_CHANGE"))
        {
          paramAnonymousContext = (SupplicantState)paramAnonymousIntent.getParcelableExtra("newState");
          int i = paramAnonymousIntent.getIntExtra("supplicantError", -1);
          Log.i("Bind", "--SUPPLICANT_STATE_CHANGED_ACTION:" + paramAnonymousContext.name());
          Log.i("Bind", " -- " + String.valueOf(i));
          if (i == 1)
          {
            Bind.this.isAuthenticationError = true;
            this.mPreviousState = null;
          }
          else
          {
            if (this.mPreviousState != null) {
              if ((this.mPreviousState.equals(SupplicantState.FOUR_WAY_HANDSHAKE)) && (paramAnonymousContext.equals(SupplicantState.DISCONNECTED))) {
                Bind.this.isAuthenticationError = true;
              }
            }
            for (;;)
            {
              if (!this.mPreviousState.equals(SupplicantState.COMPLETED)) {
                break label217;
              }
              this.mPreviousState = null;
              break;
              this.mPreviousState = paramAnonymousContext;
            }
          }
        }
        else
        {
          label217:
          if ((paramAnonymousContext.equals("android.net.wifi.WIFI_STATE_CHANGED")) && (paramAnonymousIntent.getIntExtra("wifi_state", 4) == 3)) {
            Bind.this.mHandler.postDelayed(Bind.this.mWifiEnabledRunnable, 5000L);
          }
        }
      }
    }
  };
  private Runnable refreshRunable = new Runnable()
  {
    public void run()
    {
      Message localMessage = new Message();
      localMessage.what = 291;
      Bind.this.refreshThreadHandler.sendMessage(localMessage);
      Bind.this.refreshThreadHandler.postDelayed(Bind.this.refreshRunable, 300L);
    }
  };
  private Handler refreshThreadHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((paramAnonymousMessage.what == 291) && (!Bind.this.noModel)) {
        Bind.this.refreshAllList();
      }
    }
  };
  WifiConnect wc = null;
  WifiCipherType wct = null;
  private WifiManager wifiManager;
  private List<WifiPassword> wifiPasswords = new ArrayList();
  
  private void addXMLData(String paramString1, String paramString2)
    throws Exception
  {
    Iterator localIterator = this.wifiPasswords.iterator();
    for (;;)
    {
      if (!localIterator.hasNext())
      {
        this.wifiPasswords.add(new WifiPassword(paramString1, paramString2));
        writeXMLData();
        return;
      }
      WifiPassword localWifiPassword = (WifiPassword)localIterator.next();
      if (localWifiPassword.getSsid().equals(paramString1)) {
        this.wifiPasswords.remove(localWifiPassword);
      }
    }
  }
  
  private void afterBindModel()
  {
    this.isModelBound = true;
    long l = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    saveRxInfo(l, this.mCurrentSelectedModel);
    saveRxResInfo(l);
    syncModelData();
  }
  
  private boolean checkRxCompatibility(UARTInfoMessage.RxBindInfo paramRxBindInfo)
  {
    long l = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    Utilities.ReceiverInfomation localReceiverInfomation = Utilities.getReceiverInfo(this, l);
    Object localObject = ContentUris.withAppendedId(DataProvider.MODEL_URI, l);
    localObject = getContentResolver().query((Uri)localObject, new String[] { "_id", "name" }, null, null, null);
    boolean bool;
    if (!DataProviderHelper.isCursorValid((Cursor)localObject))
    {
      Log.e("Bind", "Cann't get model name");
      bool = false;
    }
    for (;;)
    {
      return bool;
      String str = "\"" + ((Cursor)localObject).getString(((Cursor)localObject).getColumnIndex("name")) + "\"";
      ((Cursor)localObject).close();
      if (localReceiverInfomation.receiver == 0)
      {
        if ((localReceiverInfomation.analogChNumber == paramRxBindInfo.a_num) && (localReceiverInfomation.switchChNumber == paramRxBindInfo.sw_num))
        {
          Log.i("Bind", "User pre-defined rx info check OK");
          bool = true;
        }
        else if ((localReceiverInfomation.analogChNumber == 0) && (localReceiverInfomation.switchChNumber == 0))
        {
          Log.i("Bind", "An empty model...save rx info");
          if ((paramRxBindInfo.a_num < localReceiverInfomation.analogChNumber_min) || (paramRxBindInfo.sw_num < localReceiverInfomation.switchChNumber_min))
          {
            Log.i("Bind", "rx incompatibility minimum:" + paramRxBindInfo.a_num + "," + paramRxBindInfo.sw_num + ",require:" + localReceiverInfomation.analogChNumber_min + "," + localReceiverInfomation.switchChNumber_min);
            showNoMatchDialog(getResources().getString(2131296430, new Object[] { Integer.valueOf(localReceiverInfomation.analogChNumber_min), Integer.valueOf(localReceiverInfomation.switchChNumber_min), Integer.valueOf(paramRxBindInfo.a_num), Integer.valueOf(paramRxBindInfo.sw_num), str }));
            bool = false;
          }
          else if (paramRxBindInfo.a_num + paramRxBindInfo.sw_num > 12)
          {
            Log.i("Bind", "rx incompatibility:" + paramRxBindInfo.a_num + "," + paramRxBindInfo.sw_num);
            showIncompatibilityDialog(l, paramRxBindInfo, getResources().getString(2131296427, new Object[] { Integer.valueOf(10), Integer.valueOf(2), Integer.valueOf(paramRxBindInfo.a_num), Integer.valueOf(paramRxBindInfo.sw_num), Integer.valueOf(12) }));
            bool = false;
          }
          else
          {
            bool = true;
          }
        }
        else
        {
          bool = true;
        }
      }
      else if (localReceiverInfomation.receiver == paramRxBindInfo.node_id)
      {
        int i;
        label547:
        int j;
        if (paramRxBindInfo.a_num + paramRxBindInfo.sw_num > 12) {
          if (paramRxBindInfo.a_num > 10)
          {
            i = 10;
            if (paramRxBindInfo.sw_num <= 2) {
              break label598;
            }
            j = 2;
          }
        }
        for (;;)
        {
          if ((localReceiverInfomation.analogChNumber != i) || (localReceiverInfomation.switchChNumber != j)) {
            break label619;
          }
          Log.i("Bind", "rx params checked OK");
          bool = true;
          break;
          i = paramRxBindInfo.a_num;
          break label547;
          label598:
          j = paramRxBindInfo.sw_num;
          continue;
          i = paramRxBindInfo.a_num;
          j = paramRxBindInfo.sw_num;
        }
        label619:
        bool = true;
      }
      else
      {
        Log.i("Bind", "rx address " + localReceiverInfomation.receiver + " not matched with saved " + paramRxBindInfo.node_id);
        showNoMatchDialog(getResources().getString(2131296429, new Object[] { Integer.valueOf(localReceiverInfomation.receiver), Integer.valueOf(paramRxBindInfo.node_id), str }));
        bool = false;
      }
    }
  }
  
  private void connectType()
  {
    this.wc = new WifiConnect(this.wifiManager);
    this.wct = new WifiCipherType(this.mCurrentFPVItem.capabilities);
    this.wc.Connect(this.mCurrentFPVItem.SSID, this.fpv_ssid_password, this.wct.getType());
  }
  
  private void connectWifi()
  {
    WifiInfo localWifiInfo = this.wifiManager.getConnectionInfo();
    this.wifiManager.disableNetwork(localWifiInfo.getNetworkId());
    connectType();
    this.mHandler.post(this.mWifiTimoutRunnable);
  }
  
  private void dismissProgressDialog()
  {
    if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing())) {
      this.mProgressDialog.dismiss();
    }
  }
  
  private String getCurrentRXNodeId(long paramLong)
  {
    return Utilities.getCurrentRxAddrFromDB(this, paramLong);
  }
  
  private int getCurrentRXType(long paramLong)
  {
    return Utilities.getCurrentRxTypeFromDB(this, paramLong);
  }
  
  private int getRxAddr()
  {
    int i = -1;
    if (this.mCurrentSelectedModel != null) {
      i = this.mCurrentSelectedModel.node_id;
    }
    return i;
  }
  
  private void initFPVWifiSSIDList()
  {
    List localList = this.bwm.getWifiList();
    if (localList == null) {
      return;
    }
    String[] arrayOfString = getResources().getStringArray(2131361800);
    this.mScanFPVWifiResultList.clear();
    for (int i = 0;; i++)
    {
      if (i >= localList.size())
      {
        refreshListHeaderView();
        this.FPVWifiListArrayAdapter.notifyDataSetChanged();
        this.bind_camera_list.invalidateViews();
        break;
      }
      if ((isItemContainsString(((ScanResult)localList.get(i)).SSID, arrayOfString)) && (!this.mScanFPVWifiResultList.contains(localList.get(i)))) {
        this.mScanFPVWifiResultList.add((ScanResult)localList.get(i));
      }
    }
  }
  
  private void initListAdapter()
  {
    this.mRxAdapter = new RxBindArrayAdapter(this, 17367062, this.mRxList);
    this.bind_model_list.setAdapter(this.mRxAdapter);
    this.FPVWifiListArrayAdapter = new WifiArrayAdapter(this, 17367062, this.mScanFPVWifiResultList);
    this.bind_camera_list.setAdapter(this.FPVWifiListArrayAdapter);
  }
  
  private void initWifi()
  {
    this.mIntentFilter.addAction("android.net.wifi.SCAN_RESULTS");
    this.mIntentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
    this.mIntentFilter.addAction("android.net.wifi.supplicant.STATE_CHANGE");
    this.wifiManager = ((WifiManager)getSystemService("wifi"));
    this.bwm = new BindWifiManage(this.wifiManager);
  }
  
  private boolean isItemContainsString(String paramString, String[] paramArrayOfString)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramString == null) {
      bool1 = bool2;
    }
    do
    {
      return bool1;
      bool1 = bool2;
    } while (paramArrayOfString.length <= 0);
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i >= paramArrayOfString.length) {
        break;
      }
      if (paramString.startsWith(paramArrayOfString[i]))
      {
        bool1 = true;
        break;
      }
    }
  }
  
  private void loadXMLData()
    throws Exception
  {
    this.mInputStream = new FileInputStream(new File(getDir("assets", 0).toString() + "/wifi_ssid_password.xml"));
    this.wifiPasswords = WifiPasswordService.getWifiPasswords(this.mInputStream);
  }
  
  private void refreshAllList()
  {
    if (this.mController == null)
    {
      this.mController = UARTController.getInstance();
      this.mController.registerReaderHandler(this.mUartHandler);
      this.mController.startReading();
      if (!Utilities.ensureBindState(this.mController)) {
        Log.e("Bind", "fails to enter bind state");
      }
    }
    this.mController.startBind(false);
    refreshWiFiList();
    refreshModelList();
  }
  
  private void refreshListHeaderView()
  {
    Object localObject2 = this.wifiManager.getConnectionInfo();
    Object localObject1 = ((WifiInfo)localObject2).getSSID();
    if (this.isWifiConnect) {}
    for (localObject1 = ((WifiInfo)localObject2).getSSID();; localObject1 = (String)getText(2131296421)) {
      do
      {
        localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = (String)getText(2131296421);
        }
        localObject1 = localObject2;
        if (TextUtils.isEmpty((CharSequence)localObject2)) {
          localObject1 = (String)getText(2131296421);
        }
        this.mAicraftCameraHeader.setText((CharSequence)localObject1);
        return;
      } while ((((WifiInfo)localObject2).getNetworkId() != -1) && (((WifiInfo)localObject2).getIpAddress() != 0));
    }
  }
  
  private void refreshModelHeaderView()
  {
    if (this.mController == null) {}
    for (;;)
    {
      return;
      long l = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
      this.mCurrentRXType = getCurrentRXType(l);
      String str2 = getCurrentRXNodeId(l);
      String str1 = str2;
      if (str2 != null)
      {
        str1 = str2;
        if ("0".equals(str2)) {
          str1 = (String)getText(2131296421);
        }
      }
      if (this.mCurrentRXType == -1)
      {
        str1 = (String)getText(2131296421);
        this.mAicraftModelHeader.setText(str1);
      }
      else if (this.mCurrentRXType == 0)
      {
        if (str1.contains(getText(2131296421))) {
          this.mAicraftModelHeader.setText(str1);
        } else {
          this.mAicraftModelHeader.setText("SR12S_" + str1);
        }
      }
      else if (this.mCurrentRXType == 1)
      {
        if (str1.contains(getText(2131296421))) {
          this.mAicraftModelHeader.setText(str1);
        } else {
          this.mAicraftModelHeader.setText("SR12E_" + str1);
        }
      }
      else if (this.mCurrentRXType == 2)
      {
        if (str1.contains(getText(2131296421))) {
          this.mAicraftModelHeader.setText(str1);
        } else {
          this.mAicraftModelHeader.setText("SR24S_" + str1 + "v1.03");
        }
      }
      else if (this.mCurrentRXType == 3)
      {
        if (str1.contains(getText(2131296421))) {
          this.mAicraftModelHeader.setText(str1);
        } else {
          this.mAicraftModelHeader.setText("RX24_" + str1);
        }
      }
      else if (this.mCurrentRXType >= 105)
      {
        if (str1.contains(getText(2131296421))) {
          this.mAicraftModelHeader.setText(str1);
        } else {
          this.mAicraftModelHeader.setText("SR24S_" + str1 + String.format("v%.2f", new Object[] { Float.valueOf(this.mCurrentRXType / 100.0F) }));
        }
      }
      else
      {
        this.mAicraftModelHeader.setText(str1);
      }
    }
  }
  
  private void refreshModelList()
  {
    for (int i = 0;; i++)
    {
      if (i >= this.mRxAdapter.getCount())
      {
        refreshListHeaderView();
        this.mRxAdapter.notifyDataSetChanged();
        this.bind_model_list.invalidateViews();
        return;
      }
      if ((this.mCurrentSelectedModel != null) && (((UARTInfoMessage.RxBindInfo)this.mRxAdapter.getItem(i)).node_id == this.mCurrentSelectedModel.node_id)) {
        this.mRxAdapter.remove((UARTInfoMessage.RxBindInfo)this.mRxAdapter.getItem(i));
      }
    }
  }
  
  private void removeXMLData(String paramString)
    throws Exception
  {
    Iterator localIterator = this.wifiPasswords.iterator();
    for (;;)
    {
      if (!localIterator.hasNext())
      {
        writeXMLData();
        return;
      }
      WifiPassword localWifiPassword = (WifiPassword)localIterator.next();
      if (localWifiPassword.getSsid().equals(paramString)) {
        this.wifiPasswords.remove(localWifiPassword);
      }
    }
  }
  
  private void saveCameraInfo(String paramString1, String paramString2, String paramString3)
  {
    if (this.mCurrentFPVItem != null) {}
    try
    {
      addXMLData(paramString1, paramString2);
      paramString3 = new WifiCipherType(paramString3);
      paramString1 = paramString1 + "," + paramString2 + "," + paramString3.getType();
      Utilities.saveWifiInfoToDatabase(this, getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L), paramString1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  private void saveRxInfo(long paramLong, UARTInfoMessage.RxBindInfo paramRxBindInfo)
  {
    Uri localUri = ContentUris.withAppendedId(DataProvider.MODEL_URI, paramLong);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("rx", Integer.valueOf(paramRxBindInfo.node_id));
    localContentValues.put("rx_analog_num", Integer.valueOf(paramRxBindInfo.a_num));
    localContentValues.put("rx_analog_bit", Integer.valueOf(paramRxBindInfo.a_bit));
    localContentValues.put("rx_switch_num", Integer.valueOf(paramRxBindInfo.sw_num));
    localContentValues.put("rx_switch_bit", Integer.valueOf(paramRxBindInfo.sw_bit));
    localContentValues.put("rx_type", Integer.valueOf(paramRxBindInfo.mode));
    localContentValues.put("pan_id", Integer.valueOf(paramRxBindInfo.pan_id));
    localContentValues.put("tx_addr", Integer.valueOf(paramRxBindInfo.tx_addr));
    getContentResolver().update(localUri, localContentValues, null, null);
  }
  
  private void saveRxResInfo(long paramLong)
  {
    int i = getRxAddr();
    Utilities.saveRxResInfoToDatabase(this, this.mController, paramLong, i);
  }
  
  @SuppressLint({"ResourceAsColor"})
  private void setListHeaderView()
  {
    this.mAicraftModelHeader = ((TextView)LayoutInflater.from(this).inflate(2130903042, null).findViewById(2131689494));
    this.mAicraftModelHeader.setText(2131296421);
    if (406L == this.modeType) {
      this.mAicraftModelHeader.setTextColor(-13417421);
    }
    this.bind_model_list.addHeaderView(this.mAicraftModelHeader, null, false);
    this.mAicraftCameraHeader = ((TextView)LayoutInflater.from(this).inflate(2130903042, null).findViewById(2131689494));
    this.mAicraftCameraHeader.setText(2131296421);
    this.bind_camera_list.addHeaderView(this.mAicraftCameraHeader, null, false);
  }
  
  private void showIncompatibilityDialog(long paramLong, UARTInfoMessage.RxBindInfo paramRxBindInfo, String paramString)
  {
    int i = 10;
    int j = 2;
    paramString = new UARTInfoMessage.RxBindInfo();
    paramString.mode = paramRxBindInfo.mode;
    paramString.pan_id = paramRxBindInfo.pan_id;
    paramString.node_id = paramRxBindInfo.node_id;
    paramString.a_bit = paramRxBindInfo.a_bit;
    paramString.sw_bit = paramRxBindInfo.sw_bit;
    if (paramRxBindInfo.a_num > 10)
    {
      paramString.a_num = i;
      if (paramRxBindInfo.sw_num <= 2) {
        break label119;
      }
    }
    label119:
    for (i = j;; i = paramRxBindInfo.sw_num)
    {
      paramString.sw_num = i;
      saveRxInfo(paramLong, paramString);
      this.mCurrentSelectedModel = paramString;
      return;
      i = paramRxBindInfo.a_num;
      break;
    }
  }
  
  private void showNoMatchDialog(String paramString)
  {
    showNoMatchDialog(paramString, 0, 0);
  }
  
  private void showNoMatchDialog(String paramString, int paramInt1, int paramInt2)
  {
    final TwoButtonPopDialog localTwoButtonPopDialog = new TwoButtonPopDialog(this);
    if (paramInt1 == 0)
    {
      paramInt1 = 2131296424;
      if (paramInt2 != 0) {
        break label81;
      }
      paramInt2 = 17039360;
    }
    label81:
    for (;;)
    {
      localTwoButtonPopDialog.setTitle(2131296425);
      localTwoButtonPopDialog.setMessage(paramString);
      localTwoButtonPopDialog.setPositiveButton(paramInt1, new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          localTwoButtonPopDialog.dismiss();
          Bind.this.bind_model_list.setItemChecked(-1, true);
          Bind.this.startActivity(new Intent(Bind.this, ModelSelectMain.class));
        }
      });
      localTwoButtonPopDialog.setNegativeButton(paramInt2, new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          localTwoButtonPopDialog.dismiss();
          Bind.this.bind_model_list.setItemChecked(-1, true);
        }
      });
      localTwoButtonPopDialog.show();
      return;
      break;
    }
  }
  
  private void showPasswordDailog()
  {
    final BaseDialog localBaseDialog = new BaseDialog(this, 2131230729);
    localBaseDialog.setContentView(2130903043);
    CheckBox localCheckBox = (CheckBox)localBaseDialog.findViewById(2131689501);
    TextView localTextView = (TextView)localBaseDialog.findViewById(2131689498);
    final EditText localEditText = (EditText)localBaseDialog.findViewById(2131689500);
    Button localButton1 = (Button)localBaseDialog.findViewById(2131689503);
    Button localButton2 = (Button)localBaseDialog.findViewById(2131689502);
    localTextView.setText(this.mCurrentFPVItem.SSID);
    localCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean) {
          localEditText.setInputType(145);
        }
        for (;;)
        {
          localEditText.setSelection(localEditText.getText().length());
          return;
          localEditText.setInputType(129);
        }
      }
    });
    localButton1.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Bind.this.fpv_ssid_password = localEditText.getEditableText().toString();
        paramAnonymousView = (InputMethodManager)Bind.this.getSystemService("input_method");
        if (paramAnonymousView.isActive()) {
          paramAnonymousView.toggleSoftInput(1, 2);
        }
        localBaseDialog.dismiss();
        Bind.this.bindWifi();
      }
    });
    localButton2.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localBaseDialog.dismiss();
        Bind.this.dismissKeyBoard();
      }
    });
    localBaseDialog.show();
  }
  
  private void showProgressDialog()
  {
    this.mProgressDialog = MyProgressDialog.show(this, getResources().getText(2131296434), getResources().getText(2131296436), false, false);
  }
  
  private void showProgressDialog(int paramInt1, int paramInt2)
  {
    this.mProgressDialog = MyProgressDialog.show(this, getResources().getText(paramInt1), getResources().getText(paramInt2), false, false);
  }
  
  private void showResult(String paramString)
  {
    int i = 0;
    if ("success".equals(paramString))
    {
      paramString = getResources().getString(2131296437, new Object[] { " " });
      i = 1;
      this.isWifiConnect = true;
      refreshModelHeaderView();
    }
    for (;;)
    {
      if ((this.isWifiConnect) && (this.fpv_ssid_password != null)) {}
      try
      {
        saveCameraInfo(this.mCurrentFPVItem.SSID, this.fpv_ssid_password, this.mCurrentFPVItem.capabilities);
        refreshListHeaderView();
        if (this.isModelBound)
        {
          this.mRxList.remove(this.mCurrentSelectedModel);
          this.bind_model_list.invalidateViews();
        }
        if (paramString != null)
        {
          final OneButtonPopDialog localOneButtonPopDialog = new OneButtonPopDialog(this);
          if (i != 0)
          {
            localOneButtonPopDialog.adjustHeight(380);
            localOneButtonPopDialog.setMessageGravity(17);
            localOneButtonPopDialog.setTitle(2131296439);
            localOneButtonPopDialog.setMessage(paramString);
            localOneButtonPopDialog.setCancelable(false);
            localOneButtonPopDialog.setPositiveButton(17039370, new View.OnClickListener()
            {
              public void onClick(View paramAnonymousView)
              {
                Bind.this.mIPCameraManager.initCamera(null);
                localOneButtonPopDialog.dismiss();
              }
            });
            localOneButtonPopDialog.show();
          }
        }
        else
        {
          return;
          paramString = getResources().getString(2131296438, new Object[] { paramString });
          this.mController.finishBind(false);
          this.mCurrentSelectedModel = null;
        }
      }
      catch (Exception localException1)
      {
        for (;;)
        {
          localException1.printStackTrace();
          continue;
          try
          {
            if (this.mCurrentFPVItem != null) {
              removeXMLData(this.mCurrentFPVItem.SSID);
            }
          }
          catch (Exception localException2)
          {
            localException2.printStackTrace();
          }
        }
      }
    }
  }
  
  private void syncModelData()
  {
    if ((this.mProgressDialog == null) || (!this.mProgressDialog.isShowing())) {
      showProgressDialog();
    }
    for (;;)
    {
      long l = this.mPrefs.getLong("current_model_id", -2L);
      new SyncModelDataTask(null).execute(new Long[] { Long.valueOf(l) });
      return;
      this.mProgressDialog.setMessage(getResources().getString(2131296440));
    }
  }
  
  private void writeXMLData()
    throws Exception
  {
    this.outStream = new FileOutputStream(new File(getDir("assets", 0).toString() + "/wifi_ssid_password.xml"));
    WifiPasswordService.save(this.wifiPasswords, this.outStream);
  }
  
  public void bindFPV()
  {
    Object localObject2 = this.wifiManager.getConnectionInfo().getSSID();
    int i = 0;
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = "";
    }
    localObject2 = this.wifiPasswords.iterator();
    if (!((Iterator)localObject2).hasNext())
    {
      this.wct = new WifiCipherType(this.mCurrentFPVItem.capabilities);
      this.wct = new WifiCipherType(this.mCurrentFPVItem.capabilities);
      if ((i == 0) || (!((String)localObject1).equals(this.mCurrentFPVItem.SSID))) {
        break label152;
      }
      this.isWifiConnect = true;
      showResult("success");
    }
    for (;;)
    {
      return;
      WifiPassword localWifiPassword = (WifiPassword)((Iterator)localObject2).next();
      if (!localWifiPassword.getSsid().equals(this.mCurrentFPVItem.SSID)) {
        break;
      }
      i = 1;
      this.fpv_ssid_password = localWifiPassword.getPassword();
      break;
      label152:
      if ((i != 0) || (2 == this.wct.getType())) {
        bindWifi();
      } else {
        showPasswordDailog();
      }
    }
  }
  
  public boolean bindRC()
  {
    boolean bool = false;
    int i = getRxAddr();
    if (i == -1) {
      MyToast.makeText(this, 2131296442, 0, 0).show();
    }
    for (;;)
    {
      return bool;
      if ((this.mProgressDialog == null) || (!this.mProgressDialog.isShowing())) {
        showProgressDialog(2131296434, 2131296435);
      }
      this.mController.unbind(true, 3);
      this.isModelBound = false;
      this.mController.bind(false, i);
      this.mBindModelRetryTimes = 0;
      this.mHandler.removeCallbacks(this.mBindModelTimeoutRunnable);
      this.mHandler.postDelayed(this.mBindModelTimeoutRunnable, 800L);
      bool = true;
    }
  }
  
  public void bindWifi()
  {
    showProgressDialog();
    WifiInfo localWifiInfo = this.wifiManager.getConnectionInfo();
    this.wifiManager.disableNetwork(localWifiInfo.getNetworkId());
    connectType();
    this.mHandler.post(this.mWifiTimoutRunnable);
  }
  
  public void dismissKeyBoard()
  {
    ((InputMethodManager)getSystemService("input_method")).toggleSoftInput(1, 2);
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 1001) {
      recreate();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(Integer.MIN_VALUE);
    getWindow().addFlags(128);
    this.mPrefs = getSharedPreferences("flight_setting_value", 0);
    long l = getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    this.modeType = this.mPrefs.getLong("current_model_type", 0L);
    this.mCurrentRXType = getCurrentRXType(l);
    if (l == -2L)
    {
      setContentView(2130903045);
      findViewById(2131689512).setOnClickListener(this.mGotoModelSelectListener);
      this.noModel = true;
    }
    for (;;)
    {
      return;
      setContentView(2130903044);
      this.bind_model_list = ((ListView)findViewById(2131689508));
      this.bind_camera_list = ((ListView)findViewById(2131689511));
      setListHeaderView();
      initListAdapter();
      initWifi();
      try
      {
        loadXMLData();
        this.bind_camera_list.setOnItemClickListener(this.adapterOnItemClickListener);
        if (406L == this.modeType) {
          this.bind_model_list.setEnabled(false);
        }
      }
      catch (Exception paramBundle)
      {
        for (;;)
        {
          paramBundle.printStackTrace();
        }
        this.bind_model_list.setOnItemClickListener(this.adapterOnItemClickListener);
      }
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.refreshThreadHandler = null;
  }
  
  public void onPause()
  {
    super.onPause();
    this.refreshThreadHandler.removeCallbacks(this.refreshRunable);
    if (!this.noModel)
    {
      this.mController.finishBind(false);
      if (!Utilities.ensureAwaitState(this.mController)) {
        Log.e("Bind", "fails to enter await state");
      }
      Utilities.UartControllerStandBy(this.mController);
      this.mController = null;
    }
  }
  
  public void onResume()
  {
    super.onResume();
    this.refreshThreadHandler.post(this.refreshRunable);
    if (!this.noModel)
    {
      this.mController = UARTController.getInstance();
      this.mController.registerReaderHandler(this.mUartHandler);
      this.mController.startReading();
      if (!Utilities.ensureBindState(this.mController)) {
        Log.e("Bind", "fails to enter bind state");
      }
      this.mController.startBind(false);
      refreshWiFiList();
      refreshModelHeaderView();
    }
    try
    {
      loadXMLData();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void onStart()
  {
    super.onStart();
    if (!this.noModel)
    {
      this.mIPCameraManager = IPCameraManager.getIPCameraManager(this, 102);
      this.mIPCameraManager.initCamera(null);
      registerReceiver(this.receiverWifi, this.mIntentFilter);
    }
  }
  
  public void onStop()
  {
    super.onStop();
    if (!this.noModel)
    {
      this.mHandler.removeCallbacks(this.mWifiTimoutRunnable);
      this.mHandler.removeCallbacks(this.mWifiEnabledRunnable);
      this.mHandler.removeCallbacks(this.mBindModelTimeoutRunnable);
      this.mIPCameraManager.finish();
      this.mIPCameraManager = null;
      unregisterReceiver(this.receiverWifi);
    }
  }
  
  public void refreshWiFiList()
  {
    if (!this.bwm.getWifiStatus()) {
      this.bwm.openWifi();
    }
    this.bwm.startScan();
    initFPVWifiSSIDList();
  }
  
  private class RxBindArrayAdapter
    extends ArrayAdapter<UARTInfoMessage.RxBindInfo>
  {
    private LayoutInflater mInflater;
    private int mResource;
    
    public RxBindArrayAdapter(int paramInt, List<UARTInfoMessage.RxBindInfo> paramList)
    {
      super(paramList, localList);
      this.mResource = paramList;
      this.mInflater = ((LayoutInflater)paramInt.getSystemService("layout_inflater"));
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      boolean bool = true;
      if (paramView == null) {
        paramView = this.mInflater.inflate(this.mResource, paramViewGroup, false);
      }
      for (;;)
      {
        UARTInfoMessage.RxBindInfo localRxBindInfo;
        try
        {
          paramViewGroup = (TextView)paramView;
          localRxBindInfo = (UARTInfoMessage.RxBindInfo)getItem(paramInt);
          if (localRxBindInfo.mode != 0) {
            break label116;
          }
          paramViewGroup.setText("SR12S_" + String.valueOf(localRxBindInfo.node_id));
          if (406L == Bind.this.modeType) {
            break label320;
          }
          paramView.setEnabled(bool);
          return paramView;
        }
        catch (ClassCastException paramView)
        {
          Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
          throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", paramView);
        }
        continue;
        label116:
        if (localRxBindInfo.mode == 1)
        {
          paramViewGroup.setText("SR12E_" + String.valueOf(localRxBindInfo.node_id));
        }
        else if (localRxBindInfo.mode == 2)
        {
          paramViewGroup.setText("SR24S_" + String.valueOf(localRxBindInfo.node_id) + "v1.03");
        }
        else if (localRxBindInfo.mode == 3)
        {
          paramViewGroup.setText("RX24_" + String.valueOf(localRxBindInfo.node_id));
        }
        else if (localRxBindInfo.mode >= 105)
        {
          paramViewGroup.setText("SR24S_" + String.format("%dv%.2f", new Object[] { Integer.valueOf(localRxBindInfo.node_id), Float.valueOf(localRxBindInfo.mode / 100.0F) }));
        }
        else
        {
          paramViewGroup.setText(String.valueOf(localRxBindInfo.node_id));
          continue;
          label320:
          bool = false;
        }
      }
    }
  }
  
  private class SyncModelDataTask
    extends AsyncTask<Long, Void, Void>
  {
    private SyncModelDataTask() {}
    
    protected Void doInBackground(Long... paramVarArgs)
    {
      long l = paramVarArgs[0].longValue();
      Utilities.sendAllDataToFlightControl(Bind.this, l, Bind.this.mController);
      Utilities.sendRxResInfoFromDatabase(Bind.this, Bind.this.mController, l);
      return null;
    }
    
    protected void onPostExecute(Void paramVoid)
    {
      super.onPostExecute(paramVoid);
      Bind.this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          Bind.this.dismissProgressDialog();
          Bind.this.showResult("success");
        }
      }, 500L);
    }
  }
  
  private class WifiArrayAdapter
    extends ArrayAdapter<ScanResult>
  {
    private LayoutInflater mInflater;
    private int mResource;
    
    public WifiArrayAdapter(int paramInt, List<ScanResult> paramList)
    {
      super(paramList, localList);
      this.mResource = paramList;
      this.mInflater = ((LayoutInflater)paramInt.getSystemService("layout_inflater"));
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null) {
        paramView = this.mInflater.inflate(this.mResource, paramViewGroup, false);
      }
      for (;;)
      {
        try
        {
          paramViewGroup = (TextView)paramView;
          paramViewGroup.setText(String.valueOf(((ScanResult)getItem(paramInt)).SSID));
          return paramView;
        }
        catch (ClassCastException paramView)
        {
          Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
          throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", paramView);
        }
      }
    }
  }
}


