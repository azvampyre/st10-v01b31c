package com.yuneec.flight_settings;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;

public class WifiConnect
{
  WifiManager wifiManager;
  
  public WifiConnect(WifiManager paramWifiManager)
  {
    this.wifiManager = paramWifiManager;
  }
  
  private WifiConfiguration IsExsits(String paramString)
  {
    Iterator localIterator = this.wifiManager.getConfiguredNetworks().iterator();
    if (!localIterator.hasNext()) {}
    WifiConfiguration localWifiConfiguration;
    for (paramString = null;; paramString = localWifiConfiguration)
    {
      return paramString;
      localWifiConfiguration = (WifiConfiguration)localIterator.next();
      if (!localWifiConfiguration.SSID.equals("\"" + paramString + "\"")) {
        break;
      }
    }
  }
  
  public boolean Connect(String paramString1, String paramString2, int paramInt)
  {
    boolean bool = false;
    if (!this.wifiManager.isWifiEnabled()) {}
    for (;;)
    {
      return bool;
      paramString2 = CreateWifiInfo(paramString1, paramString2, paramInt);
      Log.v("SIFI", " -- " + paramString2);
      if (paramString2 != null)
      {
        paramString1 = IsExsits(paramString1);
        if (paramString1 != null) {
          this.wifiManager.removeNetwork(paramString1.networkId);
        }
        paramInt = this.wifiManager.addNetwork(paramString2);
        bool = this.wifiManager.enableNetwork(paramInt, false);
      }
    }
  }
  
  public WifiConfiguration CreateWifiInfo(String paramString1, String paramString2, int paramInt)
  {
    WifiConfiguration localWifiConfiguration = new WifiConfiguration();
    localWifiConfiguration.allowedAuthAlgorithms.clear();
    localWifiConfiguration.allowedGroupCiphers.clear();
    localWifiConfiguration.allowedKeyManagement.clear();
    localWifiConfiguration.allowedPairwiseCiphers.clear();
    localWifiConfiguration.allowedProtocols.clear();
    localWifiConfiguration.SSID = ("\"" + paramString1 + "\"");
    if (paramInt == 2)
    {
      localWifiConfiguration.wepKeys[0] = "";
      localWifiConfiguration.allowedKeyManagement.set(0);
      localWifiConfiguration.wepTxKeyIndex = 0;
    }
    if (paramInt == 0)
    {
      localWifiConfiguration.preSharedKey = ("\"" + paramString2 + "\"");
      localWifiConfiguration.hiddenSSID = true;
      localWifiConfiguration.allowedAuthAlgorithms.set(1);
      localWifiConfiguration.allowedGroupCiphers.set(3);
      localWifiConfiguration.allowedGroupCiphers.set(2);
      localWifiConfiguration.allowedGroupCiphers.set(0);
      localWifiConfiguration.allowedGroupCiphers.set(1);
      localWifiConfiguration.allowedKeyManagement.set(0);
      localWifiConfiguration.wepTxKeyIndex = 0;
    }
    if (paramInt == 1)
    {
      localWifiConfiguration.preSharedKey = ("\"" + paramString2 + "\"");
      localWifiConfiguration.hiddenSSID = true;
      localWifiConfiguration.allowedAuthAlgorithms.set(0);
      localWifiConfiguration.allowedGroupCiphers.set(3);
      localWifiConfiguration.allowedGroupCiphers.set(2);
      localWifiConfiguration.allowedGroupCiphers.set(0);
      localWifiConfiguration.allowedGroupCiphers.set(1);
      localWifiConfiguration.allowedKeyManagement.set(1);
      localWifiConfiguration.allowedKeyManagement.set(2);
      localWifiConfiguration.allowedPairwiseCiphers.set(2);
      localWifiConfiguration.allowedPairwiseCiphers.set(1);
      localWifiConfiguration.allowedProtocols.set(1);
      localWifiConfiguration.allowedProtocols.set(0);
      localWifiConfiguration.status = 2;
    }
    return localWifiConfiguration;
  }
}


