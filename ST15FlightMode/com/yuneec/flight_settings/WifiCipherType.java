package com.yuneec.flight_settings;

import java.io.PrintStream;

public class WifiCipherType
{
  public static final int PROTO_ESS = 2;
  public static final int PROTO_WPS = 2;
  public static final int WIFICIPHER_INVALID = 3;
  public static final int WIFICIPHER_NOPASS = 2;
  public static final int WIFICIPHER_WEP = 0;
  public static final int WIFICIPHER_WPA = 1;
  private String capabilities;
  private int connectType;
  private int groupCipher;
  private int keyMgmt;
  private int pairwiseCiphers;
  private int protocol;
  
  public WifiCipherType(String paramString)
  {
    this.capabilities = paramString;
    resolve();
  }
  
  public int getGroupCipher()
  {
    return this.groupCipher;
  }
  
  public int getKeyMgmt()
  {
    return this.keyMgmt;
  }
  
  public int getPairwiseCiphers()
  {
    return this.pairwiseCiphers;
  }
  
  public int getProtocol()
  {
    return this.protocol;
  }
  
  public int getType()
  {
    return this.connectType;
  }
  
  public void resolve()
  {
    if (this.capabilities.equals("")) {}
    label44:
    label53:
    label81:
    label103:
    label162:
    label177:
    label263:
    label309:
    label344:
    for (;;)
    {
      return;
      Object localObject = this.capabilities.split("]")[0];
      int i;
      if (((String)localObject).contains("["))
      {
        localObject = ((String)localObject).substring(1, ((String)localObject).length());
        localObject = ((String)localObject).split("-");
        i = 0;
        if (i < localObject.length) {
          break label162;
        }
        if (localObject.length >= 3)
        {
          if (!localObject[2].contains("WEP40")) {
            break label177;
          }
          this.groupCipher = 0;
        }
        if (localObject.length >= 2)
        {
          if (!localObject[1].contains("PSK")) {
            break label263;
          }
          this.keyMgmt = 1;
        }
        if (localObject.length >= 1)
        {
          if (!localObject[0].contains("RSN")) {
            break label309;
          }
          this.keyMgmt = 1;
          this.connectType = 1;
        }
      }
      for (;;)
      {
        if ((localObject.length < 0) || (!localObject[0].contains("ESS"))) {
          break label344;
        }
        this.keyMgmt = 2;
        this.connectType = 2;
        break;
        break label44;
        System.out.println(localObject[i]);
        i++;
        break label53;
        if (localObject[2].contains("WEP104"))
        {
          this.groupCipher = 1;
          break label81;
        }
        if (localObject[2].contains("TKIP"))
        {
          this.groupCipher = 2;
          this.keyMgmt = 1;
          break label81;
        }
        if (localObject[2].contains("CCMP"))
        {
          this.groupCipher = 3;
          this.keyMgmt = 2;
          break label81;
        }
        if (!localObject[2].contains("NONE")) {
          break label81;
        }
        this.keyMgmt = 0;
        break label81;
        if (localObject[1].contains("EAP"))
        {
          this.keyMgmt = 2;
          break label103;
        }
        if (localObject[1].contains("IEEE8021X"))
        {
          this.keyMgmt = 3;
          break label103;
        }
        this.keyMgmt = 0;
        break label103;
        if (localObject[0].contains("WPS"))
        {
          this.keyMgmt = 2;
          this.connectType = 2;
        }
        else
        {
          this.keyMgmt = 0;
          this.connectType = 1;
        }
      }
    }
  }
}


