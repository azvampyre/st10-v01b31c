package com.yuneec.IPCameraManager;

import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.format.Time;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class Amba
  extends DM368
{
  public static final int RESULT_APP_NOT_READY = -22;
  public static final int RESULT_CARD_PROTECTED = -18;
  public static final int RESULT_HDMI_INSERTED = -16;
  public static final int RESULT_INVALID_OPERATION = -14;
  public static final int RESULT_INVALID_OPTION_VALUE = -13;
  public static final int RESULT_JSON_PACKAGE_ERROR = -7;
  public static final int RESULT_JSON_PACKAGE_TIMEOUT = -8;
  public static final int RESULT_JSON_SYNTAX_ERROR = -9;
  public static final int RESULT_NO_MORE_SPACE = -17;
  public static final int RESULT_OK = 0;
  public static final int RESULT_OPERATION_MISMATCH = -15;
  public static final int RESULT_PIV_NOT_ALLOWED = -20;
  public static final int RESULT_REACH_MAX_CLNT = -5;
  public static final int RESULT_SESSION_START_FAIL = -3;
  public static final int RESULT_SYSTEM_BUSY = -21;
  public static final int RESULT_UNKNOWN_ERROR = -1;
  private static final String TAG = "Amba";
  
  public void formatSDCard(Messenger paramMessenger)
  {
    Log.i("Amba", "formatSDCard");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=FORMAT_CARD", paramMessenger, 14, 302);
  }
  
  public void getAEenable(Messenger paramMessenger)
  {
    Log.i("Amba", "getAEenable");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_AE_ENABLE", paramMessenger, 48, 301);
  }
  
  public void getAudioState(Messenger paramMessenger)
  {
    Log.i("Amba", "get audio state");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_AUDIO_SW", paramMessenger, 60, 301);
  }
  
  public void getBattery(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_BATTERY_LEVEL", paramMessenger, 30, 301);
  }
  
  public void getCameraMode(Messenger paramMessenger)
  {
    Log.i("Amba", "getCameraMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_CAM_MODE", paramMessenger, 62, 301);
  }
  
  public void getDeviceStatus(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=DETECT_CARD", paramMessenger, 44, 301);
  }
  
  public void getExposure(Messenger paramMessenger)
  {
    Log.i("Amba", "getExposure");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_EXPOSURE_VALUE", paramMessenger, 56, 301);
  }
  
  public void getFieldOfView(Messenger paramMessenger)
  {
    Log.i("Amba", "getFieldOfView");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_FOV", paramMessenger, 36, 301);
  }
  
  public void getIQtype(Messenger paramMessenger)
  {
    Log.i("Amba", "getIQtype");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_IQ_TYPE", paramMessenger, 52, 301);
  }
  
  public void getPhotoFormat(Messenger paramMessenger)
  {
    Log.i("Amba", "getPhotoFormat");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_PHOTO_FORMAT", paramMessenger, 46, 301);
  }
  
  public void getPhotoMode(Messenger paramMessenger)
  {
    Log.i("Amba", "getPhotoMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_PHOTO_MODE", paramMessenger, 42, 301);
  }
  
  public void getPhotoSize(Messenger paramMessenger)
  {
    Log.i("Amba", "getPhotoSize");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_PHOTO_SIZE", paramMessenger, 29, 301);
  }
  
  public void getRecordTime(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_REC_TIME", paramMessenger, 25, 301);
  }
  
  public void getSDCardFormat(Messenger paramMessenger)
  {
    Log.i("Amba", "getSDCardFormat");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_CARD_FORMAT", paramMessenger, 40, 301);
  }
  
  public void getSDCardFreeSpace(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SPACE_FREE", paramMessenger, 39, 301);
  }
  
  public void getSDCardSpace(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SPACE", paramMessenger, 38, 301);
  }
  
  public void getSDCardStatus(Messenger paramMessenger)
  {
    Log.i("Amba", "getSDCardStatus");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SPACE_FREE", paramMessenger, 10, 301);
  }
  
  public void getShutterTimeAndISO(Messenger paramMessenger)
  {
    Log.i("Amba", "getShutterTimeAndISO");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SH_TM_ISO", paramMessenger, 50, 301);
  }
  
  public void getVersion(Messenger paramMessenger)
  {
    Log.i("Amba", "getVersion");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_FW_VERSION", paramMessenger, 23, 301);
  }
  
  public void getVideoMode(Messenger paramMessenger)
  {
    Log.i("Amba", "getVideoMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_VIDEO_MODE", paramMessenger, 58, 301);
  }
  
  public void getVideoResolution(Messenger paramMessenger)
  {
    Log.i("Amba", "getVideoResolution");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SETTING", paramMessenger, 32, 301);
  }
  
  public void getVideoStandard(Messenger paramMessenger)
  {
    Log.i("Amba", "getVideoStandard");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_VIDEO_STANDARD", paramMessenger, 34, 301);
  }
  
  public void getWhiteBalance(Messenger paramMessenger)
  {
    Log.i("Amba", "getWhiteBalance");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_WHITEBLANCE_MODE", paramMessenger, 54, 301);
  }
  
  public void getWorkStatus(Messenger paramMessenger)
  {
    Log.i("Amba", "getWorkStatus");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_STATUS", paramMessenger, 37, 301);
  }
  
  public void init()
  {
    this.SERVER_URL = "http://192.168.42.1/";
    this.FILE_PATH = "DCIM/100MEDIA/";
    this.REGEX_FORMAT_1 = "YUNC[\\w]*\\.THM";
    this.REGEX_FORMAT_2 = "YUNC[\\w]*\\.mp4";
    this.REGEX_FORMAT_3 = null;
    this.REGEX_FORMAT_4 = null;
    this.HTTP_CONNECTION_TIMEOUT = 10000;
    this.HTTP_SOCKET_TIMEOUT = 10000;
  }
  
  public void initCamera(Messenger paramMessenger)
  {
    Log.i("Amba", "init camera");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=INDEX_PAGE", paramMessenger, 24, 301);
  }
  
  public void isRecording(Messenger paramMessenger)
  {
    Log.i("Amba", "isRecording");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_STATUS", paramMessenger, 21, 301);
  }
  
  protected Object onHandleSpecialRequest(Message paramMessage, Object paramObject, IPCameraManager.RequestResult paramRequestResult)
  {
    Object localObject2 = paramObject;
    Object localObject1 = localObject2;
    switch (paramMessage.arg1)
    {
    default: 
      localObject1 = localObject2;
    }
    for (;;)
    {
      paramRequestResult.result = "special_response_handled";
      return localObject1;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      for (;;)
      {
        try
        {
          paramMessage = new org/json/JSONObject;
          paramMessage.<init>((String)paramObject);
          if (paramMessage.getInt("rval") != 0) {
            continue;
          }
          paramMessage = "HTTPCODE OK";
        }
        catch (JSONException paramMessage)
        {
          paramMessage = "HTTPCODE Internal Error";
          continue;
        }
        localObject1 = paramMessage;
        break;
        paramMessage = "HTTPCODE Internal Error";
      }
      localObject1 = paramObject;
      continue;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      for (;;)
      {
        try
        {
          paramMessage = new org/json/JSONObject;
          paramMessage.<init>((String)paramObject);
          if (paramMessage.getInt("rval") != 0) {
            continue;
          }
          paramMessage = "HTTPCODE OK";
        }
        catch (JSONException paramMessage)
        {
          paramMessage = "HTTPCODE Internal Error";
          continue;
        }
        localObject1 = paramMessage;
        break;
        paramMessage = "HTTPCODE Internal Error";
      }
      if ("HTTPCODE OK".equals(paramRequestResult.result)) {
        try
        {
          paramMessage = new org/json/JSONObject;
          paramMessage.<init>((String)paramObject);
          paramMessage = paramMessage.getString("YUNEEC_ver");
          localObject1 = paramMessage;
        }
        catch (JSONException paramMessage)
        {
          for (;;)
          {
            paramMessage = "HTTPCODE Internal Error";
          }
        }
      }
      localObject1 = "Unknown";
      continue;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramObject = paramMessage.getString("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$RecordStatus;
        paramMessage.<init>(false);
        if ("record".equals(paramObject)) {}
        for (paramMessage.isRecording = true;; paramMessage.isRecording = false)
        {
          localObject1 = paramMessage;
          break;
        }
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      int i;
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        if (paramMessage.getInt("rval") == 0)
        {
          i = paramMessage.getInt("param");
          paramObject = new com/yuneec/IPCameraManager/IPCameraManager$RecordTime;
          ((IPCameraManager.RecordTime)paramObject).<init>(i);
        }
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramObject = "HTTPCODE Internal Error";
        }
      }
      localObject1 = paramObject;
      continue;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$Bettery;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardTotalSpace;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardFreeSpace;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param") >> 10;
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardStatus;
        paramMessage.<init>(true, i, 0L);
        if (i <= 18) {
          paramMessage.isInsert = false;
        }
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = paramMessage;
      continue;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardFormat;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$PhotoMode;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        int k = paramMessage.getInt("free");
        i = paramMessage.getInt("total");
        int j = paramMessage.getInt("status");
        paramObject = paramMessage.getString("video_status");
        paramMessage = new java/lang/StringBuilder;
        paramMessage.<init>("Work status is: ");
        Log.i("Amba", (String)paramObject);
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$DevicesStatus;
        paramMessage.<init>(k, i, j, (String)paramObject);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("photo_format");
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("Photo format is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = Integer.valueOf(paramMessage.getInt("ae_enable"));
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("AE enable is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("shutter_time");
        paramObject = paramMessage.getString("iso_value");
        paramMessage = new java/lang/StringBuilder;
        paramMessage.<init>("Shutter time is: ");
        Log.i("Amba", i + "ISO is: " + (String)paramObject);
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$ShutterTimeISO;
        paramMessage.<init>();
        paramMessage.iso = ((String)paramObject);
        paramMessage.time = i;
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = Integer.valueOf(paramMessage.getInt("IQ_type"));
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("IQ type is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = Integer.valueOf(paramMessage.getInt("wb_mode"));
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("White balance is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("exposure_value");
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("Exposure value is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("video_mode");
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("Video mode is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = Integer.valueOf(paramMessage.getInt("audio_sw"));
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("Audio state is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = Integer.valueOf(paramMessage.getInt("cam_mode"));
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("camera mode is: ");
        Log.i("Amba", paramMessage);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
    }
  }
  
  public void resetDefault(Messenger paramMessenger)
  {
    Log.i("Amba", "resetDefault");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=RESET_DEFAULT", paramMessenger, 43, 302);
  }
  
  public void restartVF(Messenger paramMessenger)
  {
    Log.i("Amba", "restartVF");
    stopVF(paramMessenger);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=REST_VF", paramMessenger, 26, 301);
  }
  
  public void setAEenable(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setAEenable: " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_AE_ENABLE&mode=" + paramInt, paramMessenger, 47, 301);
  }
  
  public void setAudioState(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "set audio state: state=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_AUDIO_SW&mode=" + paramInt, paramMessenger, 59, 301);
  }
  
  public void setCC4In1Config(Messenger paramMessenger, Cameras paramCameras)
  {
    paramCameras = Message.obtain();
    paramCameras.what = 1;
    paramCameras.arg1 = 12;
    paramCameras.obj = "HTTPCODE OK";
    try
    {
      paramMessenger.send(paramCameras);
      return;
    }
    catch (RemoteException paramMessenger)
    {
      for (;;)
      {
        paramMessenger.printStackTrace();
      }
    }
  }
  
  public void setCameraMode(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba", "setCameraMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_CAM_MODE&mode=" + paramString, paramMessenger, 61, 301);
  }
  
  public void setExposure(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba", "setExposure:" + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_EXPOSURE_VALUE&mode=" + paramString, paramMessenger, 55, 301);
  }
  
  public void setFieldOfView(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setFieldOfView: param = " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_FOV&PARAM=" + paramInt, paramMessenger, 35, 302);
  }
  
  public void setIQtype(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setIQtype: " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_IQ_TYPE&mode=" + paramInt, paramMessenger, 51, 301);
  }
  
  public void setPhotoFormat(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba", "setPhotoFormat: " + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_PHOTO_FORMAT&value=" + paramString, paramMessenger, 45, 301);
  }
  
  public void setPhotoMode(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setPhotoMode: mode=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_PHOTO_MODE&MODE=" + paramInt, paramMessenger, 41, 302);
  }
  
  public void setPhotoSize(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setPhotoSize: mode=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_PHOTO_SIZE&MODE=" + paramInt, paramMessenger, 28, 302);
  }
  
  public void setShutterTimeAndISO(Messenger paramMessenger, int paramInt, String paramString)
  {
    Log.i("Amba", "setShutterTimeAndISO: time=" + paramInt + ",iso=" + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_SH_TM_ISO&time=" + paramInt + "&value=" + paramString, paramMessenger, 49, 301);
  }
  
  public void setVideoMode(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba", "setVideoMode锛� " + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_VIDEO_MODE&video_mode=" + paramString, paramMessenger, 57, 301);
  }
  
  public void setVideoResolution(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setVideoResolution: value=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_SETTING&resolution=" + paramInt, paramMessenger, 31, 301);
  }
  
  public void setVideoStandard(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setVideoStandard: param= " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_VIDEO_STANDARD&PARAM=" + paramInt, paramMessenger, 33, 301);
  }
  
  public void setWhiteBalance(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba", "setWhiteBalance: " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_WHITEBLANCE_MODE&mode=" + paramInt, paramMessenger, 53, 301);
  }
  
  public void snapShot(String paramString1, Messenger paramMessenger, String paramString2)
  {
    Log.i("Amba", "snapShot");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=TAKE_PHOTO", paramMessenger, 4, 301);
  }
  
  public void startRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba", "startRecord");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=START_RECORD", paramMessenger, 2, 301);
  }
  
  public void stopRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba", "stopRecord");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=STOP_RECORD", paramMessenger, 3, 302);
  }
  
  public void stopVF(Messenger paramMessenger)
  {
    Log.i("Amba", "stopVF");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=STOP_VF", paramMessenger, 27, 301);
  }
  
  public void syncTime(Messenger paramMessenger)
  {
    Log.i("Amba", "syncTime");
    Object localObject = new Time();
    ((Time)localObject).setToNow();
    localObject = ((Time)localObject).format("%Y-%m-%d_%H:%M:%S");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_TIME&time=" + (String)localObject, paramMessenger, 1, 301);
  }
}


