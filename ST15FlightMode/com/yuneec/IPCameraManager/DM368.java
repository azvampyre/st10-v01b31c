package com.yuneec.IPCameraManager;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.format.Time;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

@TargetApi(11)
public class DM368
  extends IPCameraManager
{
  protected static final String Authorization_Header = "Authorization";
  protected static final String Authorization_Values = "Basic YWRtaW46OTk5OQ==";
  private static final ArrayList<String> INTERESTED_CAMERA_CONFIG = new ArrayList();
  private static final String REQUEST_DELSCHEDULE = "delschedule=";
  private static final String REQUEST_SCHEDULE = "schedule=";
  protected static final String REQUEST_SET = "vb.htm";
  private static final String REQUEST_SETDATE = "newdate=";
  private static final String REQUEST_SETTIME = "newtime=";
  private static final String REQUEST_SNAPSHOT = "clicksnapfilename=";
  private static final String TAG = "DM368";
  private Runnable mAfterDelSchedule;
  private int mDesiredRtspResolution;
  private Handler mInternalHandler = new Handler()
  {
    private String getRtspLocation(String paramAnonymousString1, String paramAnonymousString2)
    {
      Object localObject2 = null;
      Object localObject1 = localObject2;
      if (paramAnonymousString1.contains(paramAnonymousString2))
      {
        paramAnonymousString2 = paramAnonymousString1.split("@");
        if (paramAnonymousString2.length < 2) {
          break label33;
        }
      }
      for (localObject1 = paramAnonymousString2[1];; localObject1 = localObject2)
      {
        return (String)localObject1;
        label33:
        Log.w("DM368", "a corrupted stream name :" + paramAnonymousString1);
      }
    }
    
    private void handleRtspLocation(Message paramAnonymousMessage)
    {
      String str1;
      if ((paramAnonymousMessage.obj instanceof HashMap))
      {
        Object localObject3 = (HashMap)paramAnonymousMessage.obj;
        Object localObject2 = null;
        int j = 0;
        if (DM368.this.mDesiredRtspResolution == 200)
        {
          str1 = "720x480";
          Object localObject1 = (String)((HashMap)localObject3).get("streamname1");
          int i = j;
          if (localObject1 != null)
          {
            localObject1 = getRtspLocation((String)localObject1, str1);
            i = j;
            localObject2 = localObject1;
            if (localObject1 != null)
            {
              i = 1;
              localObject2 = localObject1;
            }
          }
          j = i;
          localObject1 = localObject2;
          if (i == 0)
          {
            String str2 = (String)((HashMap)localObject3).get("streamname2");
            j = i;
            localObject1 = localObject2;
            if (str2 != null)
            {
              localObject2 = getRtspLocation(str2, str1);
              j = i;
              localObject1 = localObject2;
              if (localObject2 != null)
              {
                j = 1;
                localObject1 = localObject2;
              }
            }
          }
          localObject2 = localObject1;
          if (j == 0)
          {
            localObject3 = (String)((HashMap)localObject3).get("streamname3");
            localObject2 = localObject1;
            if (localObject3 != null)
            {
              localObject1 = getRtspLocation((String)localObject3, str1);
              localObject2 = localObject1;
              if (localObject1 == null) {}
            }
          }
          paramAnonymousMessage.obj = localObject2;
          notifySender(paramAnonymousMessage, DM368.this.mRtspStreamMessenger, 11);
        }
      }
      for (;;)
      {
        return;
        if (DM368.this.mDesiredRtspResolution == 201)
        {
          str1 = "1920*1080";
          break;
        }
        Log.w("DM368", "Unknown Resolution : " + DM368.this.mDesiredRtspResolution);
        paramAnonymousMessage.obj = "HTTPCODE Internal Error";
        notifySender(paramAnonymousMessage, DM368.this.mRtspStreamMessenger, 11);
        continue;
        notifySender(paramAnonymousMessage, DM368.this.mRtspStreamMessenger, 11);
      }
    }
    
    private void handleSDCardStatus(Message paramAnonymousMessage)
    {
      String str;
      if ((paramAnonymousMessage.obj instanceof String))
      {
        str = (String)paramAnonymousMessage.obj;
        if (!str.startsWith("OK")) {}
      }
      for (;;)
      {
        try
        {
          int j = str.indexOf("sdleft=");
          int i = str.indexOf('U', j);
          long l1 = Long.parseLong(str.substring("sdleft=".length() + j, i));
          j = str.indexOf("sdused=", i);
          i = str.indexOf('U', j);
          long l2 = Long.parseLong(str.substring("sdused=".length() + j, i));
          i = str.indexOf("sdinsert=");
          i = Integer.parseInt(str.substring("sdinsert=".length() + i));
          if (i == 3)
          {
            bool = true;
            paramAnonymousMessage.obj = new IPCameraManager.SDCardStatus(bool, l1 >> 10, l2 >> 10);
            notifySender(paramAnonymousMessage, DM368.this.mSDCardStatusMessenger, 10);
            return;
          }
        }
        catch (Exception localException)
        {
          boolean bool;
          Log.e("DM368", localException.getMessage());
          paramAnonymousMessage.obj = "HTTPCODE Internal Error";
          notifySender(paramAnonymousMessage, DM368.this.mSDCardStatusMessenger, 10);
          continue;
        }
        bool = false;
        continue;
        notifySender(paramAnonymousMessage, DM368.this.mSDCardStatusMessenger, 10);
        continue;
        notifySender(paramAnonymousMessage, DM368.this.mSDCardStatusMessenger, 10);
      }
    }
    
    private void notifySender(Message paramAnonymousMessage, Messenger paramAnonymousMessenger, int paramAnonymousInt)
    {
      Message localMessage;
      if (paramAnonymousMessenger != null)
      {
        localMessage = Message.obtain();
        localMessage.obj = paramAnonymousMessage.obj;
        localMessage.arg1 = paramAnonymousInt;
        localMessage.what = 1;
      }
      for (;;)
      {
        try
        {
          paramAnonymousMessenger.send(localMessage);
          return;
        }
        catch (RemoteException paramAnonymousMessage)
        {
          Log.e("DM368", "RemoteException Messenger was killed");
          continue;
        }
        Log.e("DM368", "No Messenager was Found,notify abort");
      }
    }
    
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      }
      for (;;)
      {
        return;
        switch (paramAnonymousMessage.arg1)
        {
        case 1: 
        default: 
          Log.i("DM368", "response :" + (String)paramAnonymousMessage.obj);
          break;
        case 3: 
          Log.i("DM368", "stop record result:" + paramAnonymousMessage.obj);
          if (((String)paramAnonymousMessage.obj).equals("HTTPCODE OK")) {
            if (DM368.this.mAfterDelSchedule != null)
            {
              DM368.this.mAfterDelSchedule.run();
              DM368.this.mAfterDelSchedule = null;
            }
          }
          for (;;)
          {
            DM368.this.mStopRecordMessenger = null;
            break;
            notifySender(paramAnonymousMessage, DM368.this.mStopRecordMessenger, 3);
          }
        case 10: 
          handleSDCardStatus(paramAnonymousMessage);
          DM368.this.mSDCardStatusMessenger = null;
          break;
        case 9: 
          switch (paramAnonymousMessage.arg2)
          {
          default: 
            break;
          case 11: 
            handleRtspLocation(paramAnonymousMessage);
            DM368.this.mRtspStreamMessenger = null;
            DM368.this.mDesiredRtspResolution = 0;
          }
          break;
        }
      }
    }
  };
  private Messenger mInternalMessenger = new Messenger(this.mInternalHandler);
  private Messenger mRtspStreamMessenger;
  private Messenger mSDCardStatusMessenger;
  private Messenger mStopRecordMessenger;
  private Object mSyncLock = new Object();
  private WorkerThread myWorker = new WorkerThread(null);
  
  static
  {
    INTERESTED_CAMERA_CONFIG.add("sdinsert");
    INTERESTED_CAMERA_CONFIG.add("sdleft");
    INTERESTED_CAMERA_CONFIG.add("sdused");
    INTERESTED_CAMERA_CONFIG.add("streamname1");
    INTERESTED_CAMERA_CONFIG.add("streamname2");
    INTERESTED_CAMERA_CONFIG.add("streamname3");
  }
  
  protected DM368()
  {
    this.myWorker.start();
  }
  
  private void getCameraConfigInternal(Messenger paramMessenger, int paramInt)
  {
    postRequest("ini.htm", paramMessenger, 9, paramInt, 301);
  }
  
  private void postRequest(String paramString, Messenger paramMessenger, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.myWorker.mHandler == null)
    {
      Log.i("DM368", "Worker Thread Handler is null!!");
      synchronized (this.mSyncLock)
      {
        Handler localHandler = this.myWorker.mHandler;
        if (localHandler != null) {}
      }
    }
    try
    {
      this.mSyncLock.wait();
      ??? = this.myWorker.mHandler.obtainMessage();
      ((Message)???).obj = paramString;
      ((Message)???).arg1 = paramInt1;
      ((Message)???).arg2 = paramInt2;
      if (paramMessenger != null) {
        ((Message)???).replyTo = paramMessenger;
      }
      ((Message)???).what = paramInt3;
      this.myWorker.mHandler.sendMessage((Message)???);
      return;
      paramString = finally;
      throw paramString;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  private void putRequestResult(IPCameraManager.RequestResult paramRequestResult, String paramString)
  {
    if (paramRequestResult != null) {
      paramRequestResult.result = paramString;
    }
  }
  
  public void finish()
  {
    this.myWorker.stopThread();
  }
  
  public void formatSDCard(Messenger paramMessenger)
  {
    postRequest("vb.htm?sdformat=1", paramMessenger, 14, 302);
  }
  
  public void getAudioState(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 59);
    Log.w("DM368", "Invalid command----resetDefault");
  }
  
  public void getBattery(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 30);
    Log.w("DM368", "Invalid command----getBattery");
  }
  
  public void getCameraConfig(Messenger paramMessenger)
  {
    getCameraConfigInternal(paramMessenger, 0);
  }
  
  public void getCameraToneSetting(Messenger paramMessenger)
  {
    postRequest("vb.htm?paratest=brightness&paratest=contrast&paratest=sharpness&paratest=saturation", paramMessenger, 15, 301);
  }
  
  public void getDeviceStatus(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 44);
    Log.w("DM368", "Invalid command----resetDefault");
  }
  
  public void getFieldOfView(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 36);
    Log.w("DM368", "Invalid command----getFieldOfView");
  }
  
  public void getMediaFile(IPCameraManager.GetMediaFileCallback paramGetMediaFileCallback)
  {
    new GetMediaFileTask(paramGetMediaFileCallback).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { this.SERVER_URL + this.FILE_PATH });
  }
  
  public void getPhotoMode(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 42);
    Log.w("DM368", "Invalid command----getPhotoMode");
  }
  
  public void getPhotoSize(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 29);
    Log.w("DM368", "Invalid command----getPhotoSize");
  }
  
  public void getRecordTime(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 25);
    Log.w("DM368", "Invalid command----getRecordTime");
  }
  
  public void getRtspStreamLocation(int paramInt, Messenger paramMessenger)
  {
    if (this.mRtspStreamMessenger != null) {
      Log.w("DM368", "getRtspStreamLocation the previous one was still process, request abort");
    }
    for (;;)
    {
      return;
      this.mRtspStreamMessenger = paramMessenger;
      this.mDesiredRtspResolution = paramInt;
      getCameraConfigInternal(this.mInternalMessenger, 11);
    }
  }
  
  public void getSDCardFormat(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 40);
    Log.w("DM368", "Invalid command----getSDCardFormat");
  }
  
  public void getSDCardFreeSpace(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 39);
    Log.w("DM368", "Invalid command----getSDCardFreeSpace");
  }
  
  public void getSDCardSpace(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 38);
    Log.w("DM368", "Invalid command----getSDCardSpace");
  }
  
  public void getSDCardStatus(Messenger paramMessenger)
  {
    if (this.mSDCardStatusMessenger != null) {
      Log.w("DM368", "getSDCardStatus the previous one was still process, request abort");
    }
    for (;;)
    {
      return;
      this.mSDCardStatusMessenger = paramMessenger;
      postRequest("vb.htm?paratest=sdleft&&paratest=sdused&&paratest=sdinsert", this.mInternalMessenger, 10, 301);
    }
  }
  
  public void getVersion(Messenger paramMessenger)
  {
    postRequest("version.txt", paramMessenger, 23, 301);
  }
  
  public void getVideoResolution(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 32);
    Log.w("DM368", "Invalid command----getResolution");
  }
  
  public void getVideoStandard(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 34);
    Log.w("DM368", "Invalid command----getVideoStandard");
  }
  
  public void getWorkStatus(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 37);
    Log.w("DM368", "Invalid command----getWorkStatus");
  }
  
  public void init()
  {
    this.SERVER_URL = "http://192.168.73.254/";
    this.FILE_PATH = "sdget.htm";
    this.REGEX_FORMAT_1 = "IMG_[\\w]*\\.jpg";
    this.REGEX_FORMAT_2 = "MOV_[\\w]*\\.avi";
    this.REGEX_FORMAT_3 = "\\d{14}\\.jpg";
    this.REGEX_FORMAT_4 = "\\d{14}\\.avi";
  }
  
  public void initCamera(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 24);
    Log.w("DM368", "Invalid command----initCamera");
  }
  
  public void isRecording(Messenger paramMessenger)
  {
    postRequest("vb.htm?getalarmstatus", paramMessenger, 21, 301);
  }
  
  protected Object onHandleSpecialRequest(Message paramMessage, Object paramObject, IPCameraManager.RequestResult paramRequestResult)
  {
    return null;
  }
  
  protected void postNullRequest(Messenger paramMessenger, int paramInt)
  {
    Message localMessage;
    if (paramMessenger != null)
    {
      localMessage = Message.obtain();
      localMessage.what = 1;
      localMessage.arg1 = paramInt;
      localMessage.obj = "HTTPCODE OK";
    }
    try
    {
      paramMessenger.send(localMessage);
      return;
    }
    catch (RemoteException paramMessenger)
    {
      for (;;)
      {
        paramMessenger.printStackTrace();
      }
    }
  }
  
  protected void postRequest(String paramString, Messenger paramMessenger, int paramInt1, int paramInt2)
  {
    postRequest(paramString, paramMessenger, paramInt1, 0, paramInt2);
  }
  
  public String rawRequestBlock(String paramString, boolean paramBoolean, IPCameraManager.RequestResult paramRequestResult)
  {
    return rawRequestBlock(paramString, paramBoolean, paramRequestResult, this.HTTP_CONNECTION_TIMEOUT, this.HTTP_SOCKET_TIMEOUT);
  }
  
  public String rawRequestBlock(String paramString, boolean paramBoolean, IPCameraManager.RequestResult paramRequestResult, int paramInt1, int paramInt2)
  {
    if (paramString.startsWith(this.SERVER_URL)) {}
    for (Object localObject1 = paramString;; localObject1 = this.SERVER_URL + paramString)
    {
      if (paramString.startsWith("http://192.168.73.254/")) {
        localObject1 = paramString;
      }
      for (;;)
      {
        Object localObject2;
        Object localObject3;
        try
        {
          paramString = new org/apache/http/client/methods/HttpGet;
          paramString.<init>((String)localObject1);
          paramString.addHeader("Authorization", "Basic YWRtaW46OTk5OQ==");
          localObject1 = new DefaultHttpClient();
          HttpConnectionParams.setConnectionTimeout(((HttpClient)localObject1).getParams(), paramInt1);
          HttpConnectionParams.setSoTimeout(((HttpClient)localObject1).getParams(), paramInt2);
        }
        catch (IllegalArgumentException paramString)
        {
          StringBuilder localStringBuilder;
          Log.e("DM368", "IllegalArgumentException :" + paramString.getMessage());
          putRequestResult(paramRequestResult, "HTTPCODE Bad Request");
          paramString = "HTTPCODE Bad Request";
          continue;
          localStringBuilder.append((String)localObject3);
          continue;
        }
        try
        {
          localObject2 = ((HttpClient)localObject1).execute(paramString);
          if (((HttpResponse)localObject2).getStatusLine().getStatusCode() != 200) {
            break label383;
          }
          if (!paramBoolean) {
            break label326;
          }
          paramString = ((HttpResponse)localObject2).getEntity().getContent();
          localObject2 = new java/io/BufferedReader;
          localObject3 = new java/io/InputStreamReader;
          ((InputStreamReader)localObject3).<init>(paramString);
          ((BufferedReader)localObject2).<init>((Reader)localObject3);
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localObject3 = ((BufferedReader)localObject2).readLine();
          if (localObject3 != null) {
            continue;
          }
          ((BufferedReader)localObject2).close();
          paramString.close();
          paramString = localStringBuilder.toString();
          putRequestResult(paramRequestResult, "HTTPCODE OK");
        }
        catch (ClientProtocolException paramString)
        {
          Log.e("DM368", "ClientProtocolException :" + paramString.getMessage());
          paramString = "HTTPCODE Internal Error";
          putRequestResult(paramRequestResult, "HTTPCODE Internal Error");
          continue;
          paramString = "HTTPCODE OK";
          putRequestResult(paramRequestResult, "HTTPCODE OK");
        }
        catch (ConnectTimeoutException paramString)
        {
          Log.e("DM368", "ConnectTimeoutException :" + paramString.getMessage());
          paramString = "HTTPCODE Connect Timeout";
          putRequestResult(paramRequestResult, "HTTPCODE Connect Timeout");
          continue;
          paramString = ((HttpResponse)localObject2).getStatusLine().getReasonPhrase();
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("rtn_response: ");
          Log.e("DM368", paramString + ", response: " + localObject2.toString());
          putRequestResult(paramRequestResult, paramString);
        }
        catch (SocketTimeoutException paramString)
        {
          Log.e("DM368", "SocketTimeoutException :" + paramString.getMessage());
          paramString = "HTTPCODE Response Timeout";
          putRequestResult(paramRequestResult, "HTTPCODE Response Timeout");
        }
        catch (IOException paramString)
        {
          Log.e("DM368", "IOException :" + paramString.getMessage());
          paramString = "HTTPCODE IOException";
          putRequestResult(paramRequestResult, "HTTPCODE IOException");
        }
      }
      ((HttpClient)localObject1).getConnectionManager().shutdown();
      return paramString;
    }
  }
  
  public String rawRequestBlockJDK(String paramString, boolean paramBoolean, IPCameraManager.RequestResult paramRequestResult)
  {
    return rawRequestBlockJDK(paramString, paramBoolean, paramRequestResult, this.HTTP_CONNECTION_TIMEOUT, this.HTTP_SOCKET_TIMEOUT);
  }
  
  public String rawRequestBlockJDK(String paramString, boolean paramBoolean, IPCameraManager.RequestResult paramRequestResult, int paramInt1, int paramInt2)
  {
    BufferedReader localBufferedReader = null;
    StringBuffer localStringBuffer = null;
    localObject4 = null;
    Object localObject3;
    if (paramString.startsWith(this.SERVER_URL))
    {
      localObject3 = paramString;
      localObject2 = localObject4;
      localObject1 = localBufferedReader;
      paramString = localStringBuffer;
    }
    for (;;)
    {
      try
      {
        Object localObject5 = new java/net/URL;
        localObject2 = localObject4;
        localObject1 = localBufferedReader;
        paramString = localStringBuffer;
        ((URL)localObject5).<init>((String)localObject3);
        localObject2 = localObject4;
        localObject1 = localBufferedReader;
        paramString = localStringBuffer;
        localObject3 = (HttpURLConnection)((URL)localObject5).openConnection();
        if (!paramBoolean) {
          continue;
        }
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localObject4 = ((HttpURLConnection)localObject3).getInputStream();
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localObject5 = new java/io/InputStreamReader;
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        ((InputStreamReader)localObject5).<init>((InputStream)localObject4);
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localBufferedReader = new java/io/BufferedReader;
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localBufferedReader.<init>((Reader)localObject5);
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localStringBuffer = new java/lang/StringBuffer;
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localStringBuffer.<init>();
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        str = localBufferedReader.readLine();
        if (str != null) {
          continue;
        }
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localBufferedReader.close();
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        ((InputStreamReader)localObject5).close();
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        ((InputStream)localObject4).close();
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        localObject4 = localStringBuffer.toString();
        localObject2 = localObject3;
        localObject1 = localObject3;
        paramString = (String)localObject3;
        putRequestResult(paramRequestResult, "HTTPCODE OK");
        paramRequestResult = (IPCameraManager.RequestResult)localObject4;
      }
      catch (MalformedURLException localMalformedURLException)
      {
        String str;
        paramString = (String)localObject2;
        localObject1 = new java/lang/StringBuilder;
        paramString = (String)localObject2;
        ((StringBuilder)localObject1).<init>("MalformedURLException :");
        paramString = (String)localObject2;
        Log.e("DM368", localMalformedURLException.getMessage());
        localObject1 = "HTTPCODE Bad Request";
        paramString = (String)localObject2;
        putRequestResult(paramRequestResult, "HTTPCODE Bad Request");
        paramString = (String)localObject1;
        if (localObject2 == null) {
          continue;
        }
        ((HttpURLConnection)localObject2).disconnect();
        paramString = (String)localObject1;
        continue;
        localObject4 = "HTTPCODE OK";
        localObject2 = localMalformedURLException;
        localObject1 = localMalformedURLException;
        paramString = localMalformedURLException;
        putRequestResult(paramRequestResult, "HTTPCODE OK");
        paramRequestResult = (IPCameraManager.RequestResult)localObject4;
        continue;
      }
      catch (IOException localIOException)
      {
        paramString = (String)localObject1;
        localObject2 = new java/lang/StringBuilder;
        paramString = (String)localObject1;
        ((StringBuilder)localObject2).<init>("IOException :");
        paramString = (String)localObject1;
        Log.e("DM368", localIOException.getMessage());
        localObject2 = "HTTPCODE IOException";
        paramString = (String)localObject1;
        putRequestResult(paramRequestResult, "HTTPCODE IOException");
        paramString = (String)localObject2;
        if (localObject1 == null) {
          continue;
        }
        ((HttpURLConnection)localObject1).disconnect();
        paramString = (String)localObject2;
        continue;
      }
      finally
      {
        if (paramString == null) {
          continue;
        }
        paramString.disconnect();
      }
      paramString = paramRequestResult;
      if (localObject3 != null)
      {
        ((HttpURLConnection)localObject3).disconnect();
        paramString = paramRequestResult;
      }
      return paramString;
      localObject3 = this.SERVER_URL + paramString;
      break;
      localObject2 = localObject3;
      localObject1 = localObject3;
      paramString = (String)localObject3;
      localStringBuffer.append(str);
    }
  }
  
  public void resetDefault(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 43);
    Log.w("DM368", "Invalid command----resetDefault");
  }
  
  public void resetToneSetting(Messenger paramMessenger)
  {
    postRequest("vb.htm?brightness=98&contrast=128&sharpness=128&saturation=128", paramMessenger, 20, 302);
  }
  
  public void restartVF(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 26);
    Log.w("DM368", "Invalid command----restVF");
  }
  
  public void sendAPInfo(String paramString1, String paramString2, Messenger paramMessenger)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("vb.htm").append("?").append("wificonfigsta=").append(paramString1).append(':').append(paramString2).append(':').append("1");
    postRequest(localStringBuilder.toString(), paramMessenger, 6, 301);
  }
  
  public void sendCustomCommand(String paramString, Messenger paramMessenger, int paramInt1, int paramInt2)
  {
    postRequest(paramString, paramMessenger, paramInt1, paramInt2);
  }
  
  public void setAudioState(Messenger paramMessenger, int paramInt)
  {
    postNullRequest(paramMessenger, 59);
    Log.w("DM368", "Invalid command----resetDefault");
  }
  
  public void setBrightness(Messenger paramMessenger, int paramInt)
  {
    postRequest("vb.htm?cameractrl=" + Uri.encode(new StringBuilder("-d /dev/ttyS1 -c BrightnessPos  -p ").append(paramInt).toString()), paramMessenger, 16, 302);
  }
  
  public void setCC4In1Config(Messenger paramMessenger, Cameras paramCameras)
  {
    postRequest("vb.htm?cc4in1select=name:" + paramCameras.getName() + "@" + "dr" + ":" + paramCameras.getDr() + "@" + "f" + ":" + paramCameras.getF() + "@" + "n" + ":" + paramCameras.getN() + "@" + "t1" + ":" + paramCameras.getT1() + "@" + "t2" + ":" + paramCameras.getT2() + "@" + "intervalp" + ":" + paramCameras.getIntervalp() + "@" + "codep" + ":" + paramCameras.getCodep() + "@" + "intervalv" + ":" + paramCameras.getIntervalv() + "@" + "codev" + ":" + paramCameras.getCodev(), paramMessenger, 12, 302);
  }
  
  public void setContrast(Messenger paramMessenger, int paramInt)
  {
    postRequest("vb.htm?cameractrl=" + Uri.encode(new StringBuilder("-d /dev/ttyS1 -c AperturePos  -p ").append(paramInt).toString()), paramMessenger, 17, 302);
  }
  
  public void setFieldOfView(Messenger paramMessenger, int paramInt)
  {
    postNullRequest(paramMessenger, 35);
    Log.w("DM368", "Invalid command----setFieldOfView");
  }
  
  public void setPhotoMode(Messenger paramMessenger, int paramInt)
  {
    postNullRequest(paramMessenger, 41);
    Log.w("DM368", "Invalid command----setPhotoMode");
  }
  
  public void setPhotoSize(Messenger paramMessenger, int paramInt)
  {
    postNullRequest(paramMessenger, 28);
    Log.w("DM368", "Invalid command----setPhotoSize");
  }
  
  public void setSaturation(Messenger paramMessenger, int paramInt)
  {
    postRequest("vb.htm?saturation=" + paramInt, paramMessenger, 19, 302);
  }
  
  public void setSharpness(Messenger paramMessenger, int paramInt)
  {
    postRequest("vb.htm?sharpness=" + paramInt, paramMessenger, 18, 302);
  }
  
  public void setVideoResolution(Messenger paramMessenger, int paramInt)
  {
    postNullRequest(paramMessenger, 31);
    Log.w("DM368", "Invalid command----setResolution");
  }
  
  public void setVideoStandard(Messenger paramMessenger, int paramInt)
  {
    postNullRequest(paramMessenger, 33);
    Log.w("DM368", "Invalid command----setVideoStandard");
  }
  
  public void snapShot(String paramString1, Messenger paramMessenger, String paramString2)
  {
    paramString1 = Uri.encode(paramString1);
    postRequest("vb.htm?clicksnapfilename=" + paramString1 + "&" + "clicksnapstorage=0", paramMessenger, 4, 302);
  }
  
  public void startRecord(Messenger paramMessenger, String paramString)
  {
    Time localTime = new Time();
    localTime.setToNow();
    paramString = new StringBuilder();
    paramString.append("vb.htm").append("?").append("sdrenable=1&schedule=" + String.format("001%02d%02d%02d", new Object[] { Integer.valueOf(8), Integer.valueOf(localTime.hour), Integer.valueOf(localTime.minute) }) + "00240000");
    Log.d("DM368", "schedule:" + paramString.toString());
    postRequest(paramString.toString(), paramMessenger, 2, 302);
  }
  
  public void stopRecord(Messenger paramMessenger, String paramString)
  {
    postRequest("vb.htm?delschedule=1", paramMessenger, 3, 302);
  }
  
  public void stopVF(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 27);
    Log.w("DM368", "Invalid command----stopVF");
  }
  
  public void switchAPStation(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("vb.htm").append("?").append("wifitosta=").append(paramString);
    postRequest(localStringBuilder.toString(), null, 7, 302);
  }
  
  public void switchMode(int paramInt, Messenger paramMessenger)
  {
    postRequest("vb.htm?setcameramode=" + paramInt, paramMessenger, 22, 302);
  }
  
  public void syncTime(Messenger paramMessenger)
  {
    Time localTime = new Time();
    localTime.setToNow();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("vb.htm").append("?").append("newdate=").append(localTime.format("%Y/%m/%d")).append("&").append("newtime=").append(localTime.format("%H:%M:%S"));
    Log.d("DM368", "Date Time:" + localStringBuilder.toString());
    postRequest(localStringBuilder.toString(), paramMessenger, 1, 301);
  }
  
  public void zoom(int paramInt, Messenger paramMessenger)
  {
    int i;
    if (paramInt < 1) {
      i = 1;
    }
    for (;;)
    {
      postRequest("vb.htm?cameractrl=" + Uri.encode(new StringBuilder("-d /dev/ttyS1 -c ZoomPos -p ").append(i).toString()), paramMessenger, 8, 302);
      return;
      i = paramInt;
      if (paramInt > 18) {
        i = 18;
      }
    }
  }
  
  private class GetMediaFileTask
    extends AsyncTask<String, Void, String[]>
  {
    private IPCameraManager.GetMediaFileCallback callback;
    
    public GetMediaFileTask(IPCameraManager.GetMediaFileCallback paramGetMediaFileCallback)
    {
      this.callback = paramGetMediaFileCallback;
    }
    
    private String[] getStreamAndParse(String paramString)
    {
      IPCameraManager.RequestResult localRequestResult = new IPCameraManager.RequestResult();
      paramString = DM368.this.rawRequestBlock(paramString, true, localRequestResult);
      if ("HTTPCODE OK".equals(localRequestResult.result)) {}
      for (paramString = parseHttpContent(paramString);; paramString = null) {
        return paramString;
      }
    }
    
    private void parse(String paramString1, String paramString2, ArrayList<String> paramArrayList)
    {
      if (paramString2 != null) {
        paramString1 = Pattern.compile(paramString2).matcher(paramString1);
      }
      for (;;)
      {
        if (!paramString1.find()) {
          return;
        }
        paramArrayList.add(paramString1.group());
      }
    }
    
    private String[] parseHttpContent(String paramString)
    {
      ArrayList localArrayList = new ArrayList();
      parse(paramString, DM368.this.REGEX_FORMAT_1, localArrayList);
      parse(paramString, DM368.this.REGEX_FORMAT_2, localArrayList);
      parse(paramString, DM368.this.REGEX_FORMAT_3, localArrayList);
      parse(paramString, DM368.this.REGEX_FORMAT_4, localArrayList);
      paramString = new String[localArrayList.size()];
      localArrayList.toArray(paramString);
      return paramString;
    }
    
    protected String[] doInBackground(String... paramVarArgs)
    {
      return getStreamAndParse(paramVarArgs[0]);
    }
    
    protected void onPostExecute(String[] paramArrayOfString)
    {
      super.onPostExecute(paramArrayOfString);
      this.callback.MediaFileGot(paramArrayOfString);
    }
  }
  
  private class WorkerThread
    extends Thread
  {
    private Handler mHandler;
    private Looper myLooper;
    
    private WorkerThread() {}
    
    private Object handleSpecialResponse(Message paramMessage, Object paramObject, IPCameraManager.RequestResult paramRequestResult)
    {
      Object localObject = DM368.this.onHandleSpecialRequest(paramMessage, paramObject, paramRequestResult);
      if ("special_response_handled".equals(paramRequestResult.result))
      {
        paramMessage = (Message)localObject;
        return paramMessage;
      }
      localObject = paramObject;
      switch (paramMessage.arg1)
      {
      default: 
        paramMessage = (Message)localObject;
      }
      for (;;)
      {
        break;
        paramMessage = (Message)localObject;
        if ("HTTPCODE OK".equals(paramRequestResult.result))
        {
          paramMessage = parseParatest((String)paramObject);
          int i = 0;
          paramObject = null;
          try
          {
            int j = Integer.parseInt((String)paramMessage.get("brightness"));
            int n = Integer.parseInt((String)paramMessage.get("contrast"));
            int k = Integer.parseInt((String)paramMessage.get("sharpness"));
            int m = Integer.parseInt((String)paramMessage.get("saturation"));
            paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$ToneSetting;
            paramMessage.<init>(j, n, k, m);
            i = 1;
            paramObject = paramMessage;
          }
          catch (Exception paramMessage)
          {
            for (;;)
            {
              Log.e("DM368", "encounter error while parsing paratest:" + paramMessage.getMessage());
            }
          }
          paramMessage = (Message)localObject;
          if (i != 0)
          {
            paramMessage = (Message)paramObject;
            continue;
            paramMessage = (Message)localObject;
            if ("HTTPCODE OK".equals(paramRequestResult.result))
            {
              paramMessage = parseParatest((String)paramObject);
              i = 0;
              paramObject = null;
              try
              {
                paramMessage = Integer.valueOf((String)paramMessage.get("getalarmstatus"), 16);
                paramObject = paramMessage;
                i = 1;
              }
              catch (Exception paramMessage)
              {
                for (;;)
                {
                  Log.e("DM368", "encounter error while parsing paratest:" + paramMessage.getMessage());
                }
              }
              paramMessage = (Message)localObject;
              if (i != 0)
              {
                paramMessage = (Message)paramObject;
                continue;
                paramMessage = parseCameraConfig((String)paramObject);
                continue;
                if ("HTTPCODE OK".equals(paramRequestResult.result))
                {
                  paramMessage = (Message)paramObject;
                }
                else
                {
                  Log.i("DM368", "HTTP CODE:" + paramRequestResult.result);
                  paramMessage = "Unknown";
                  continue;
                  if ("HTTPCODE OK".equals(paramRequestResult.result))
                  {
                    paramMessage = "HTTPCODE OK";
                    try
                    {
                      Thread.sleep(1000L);
                    }
                    catch (InterruptedException paramObject) {}
                  }
                  else
                  {
                    paramMessage = paramRequestResult.result;
                  }
                }
              }
            }
          }
        }
      }
    }
    
    private void notifySender(Message paramMessage, Object paramObject)
    {
      if (paramMessage.replyTo == null) {
        Log.i("DM368", "No Messenager was Found,notify abort");
      }
      for (;;)
      {
        return;
        Message localMessage = Message.obtain();
        localMessage.obj = paramObject;
        localMessage.arg1 = paramMessage.arg1;
        localMessage.arg2 = paramMessage.arg2;
        localMessage.what = 1;
        try
        {
          paramMessage.replyTo.send(localMessage);
        }
        catch (RemoteException paramMessage)
        {
          Log.e("DM368", "RemoteException messenger was killed");
          stopThread();
        }
      }
    }
    
    private Object parseCameraConfig(String paramString)
    {
      String[] arrayOfString1 = paramString.split("<br>");
      if (arrayOfString1.length == 1) {
        return paramString;
      }
      ArrayList localArrayList = new ArrayList(DM368.INTERESTED_CAMERA_CONFIG);
      paramString = new HashMap();
      int i = 0;
      label39:
      if (i < arrayOfString1.length) {}
      label60:
      label204:
      for (int j = 0;; j++)
      {
        if (j >= localArrayList.size()) {}
        String[] arrayOfString2;
        StringBuilder localStringBuilder;
        for (;;)
        {
          if (localArrayList.size() != 0)
          {
            i++;
            break label39;
            break;
            if (!arrayOfString1[i].startsWith((String)localArrayList.get(j))) {
              break label204;
            }
            arrayOfString2 = arrayOfString1[i].split("=");
            if (arrayOfString2.length < 2) {
              Log.w("DM368", "corrupted camera config");
            } else {
              localStringBuilder = new StringBuilder();
            }
          }
        }
        for (int k = 1;; k++)
        {
          if (k >= arrayOfString2.length)
          {
            localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
            paramString.put(arrayOfString2[0], localStringBuilder.toString());
            localArrayList.remove(j);
            break label60;
            break;
          }
          localStringBuilder.append(arrayOfString2[k]);
          localStringBuilder.append("=");
        }
      }
    }
    
    private HashMap<String, String> parseParatest(String paramString)
    {
      HashMap localHashMap = new HashMap();
      String[] arrayOfString = paramString.replace("\n", "").split("OK ");
      for (int i = 0;; i++)
      {
        if (i >= arrayOfString.length) {
          return localHashMap;
        }
        paramString = arrayOfString[i].split("=");
        if (paramString.length >= 2) {
          localHashMap.put(paramString[0].trim(), paramString[1].trim());
        }
      }
    }
    
    public void run()
    {
      setName("HttpRequest");
      Log.d("DM368", "Http worker is about to run");
      Looper.prepare();
      Log.d("DM368", "Http worker prepared");
      this.myLooper = Looper.myLooper();
      this.mHandler = new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          Object localObject = new IPCameraManager.RequestResult();
          switch (paramAnonymousMessage.what)
          {
          default: 
            Log.i("DM368", "Unknown Message:" + paramAnonymousMessage.what);
          }
          for (;;)
          {
            return;
            String str = DM368.this.rawRequestBlock((String)paramAnonymousMessage.obj, true, (IPCameraManager.RequestResult)localObject);
            localObject = DM368.WorkerThread.this.handleSpecialResponse(paramAnonymousMessage, str, (IPCameraManager.RequestResult)localObject);
            DM368.WorkerThread.this.notifySender(paramAnonymousMessage, localObject);
            continue;
            str = DM368.this.rawRequestBlock((String)paramAnonymousMessage.obj, false, (IPCameraManager.RequestResult)localObject);
            localObject = DM368.WorkerThread.this.handleSpecialResponse(paramAnonymousMessage, str, (IPCameraManager.RequestResult)localObject);
            DM368.WorkerThread.this.notifySender(paramAnonymousMessage, localObject);
            continue;
            str = DM368.this.rawRequestBlockJDK((String)paramAnonymousMessage.obj, true, (IPCameraManager.RequestResult)localObject);
            localObject = DM368.WorkerThread.this.handleSpecialResponse(paramAnonymousMessage, str, (IPCameraManager.RequestResult)localObject);
            DM368.WorkerThread.this.notifySender(paramAnonymousMessage, localObject);
          }
        }
      };
      Log.d("DM368", "Http worker syncing with UI thread");
      synchronized (DM368.this.mSyncLock)
      {
        DM368.this.mSyncLock.notifyAll();
        Log.d("DM368", "Http worker ready,start working");
        Looper.loop();
        return;
      }
    }
    
    public void stopThread()
    {
      if (this.myLooper == null) {
        Log.i("DM368", "Looper not running,namely,thread is not running");
      }
      for (;;)
      {
        return;
        this.myLooper.quit();
      }
    }
  }
}


