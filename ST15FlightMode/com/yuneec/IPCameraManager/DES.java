package com.yuneec.IPCameraManager;

import java.io.File;
import java.io.PrintStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class DES
{
  private static final String key = "ksYuN2eC";
  
  public static byte[] convertHexString(String paramString)
  {
    byte[] arrayOfByte = new byte[paramString.length() / 2];
    for (int i = 0;; i++)
    {
      if (i >= arrayOfByte.length) {
        return arrayOfByte;
      }
      arrayOfByte[i] = ((byte)Integer.parseInt(paramString.substring(i * 2, i * 2 + 2), 16));
    }
  }
  
  public static String creatEncryptPath(String paramString)
  {
    Object localObject3 = paramString.split("/");
    Object localObject1 = paramString.substring(0, paramString.indexOf("/FlightLog"));
    Object localObject2 = localObject3[1];
    localObject2 = localObject3[3];
    paramString = paramString.replace("FlightLog", "FlightEncryptLog");
    localObject3 = localObject1 + "/FlightEncryptLog/Telemetry";
    localObject2 = localObject1 + "/FlightEncryptLog/Remote";
    localObject1 = localObject1 + "/FlightEncryptLog/RemoteGPS";
    localObject3 = new File((String)localObject3);
    if (!((File)localObject3).exists()) {
      ((File)localObject3).mkdirs();
    }
    localObject2 = new File((String)localObject2);
    if (!((File)localObject2).exists()) {
      ((File)localObject2).mkdirs();
    }
    localObject1 = new File((String)localObject1);
    if (!((File)localObject1).exists()) {
      ((File)localObject1).mkdirs();
    }
    return paramString;
  }
  
  public static String decrypt(String paramString)
    throws Exception
  {
    byte[] arrayOfByte = convertHexString(paramString);
    paramString = Cipher.getInstance("DES/CBC/PKCS5Padding");
    DESKeySpec localDESKeySpec = new DESKeySpec("ksYuN2eC".getBytes("UTF-8"));
    paramString.init(2, SecretKeyFactory.getInstance("DES").generateSecret(localDESKeySpec), new IvParameterSpec("ksYuN2eC".getBytes("UTF-8")));
    return new String(paramString.doFinal(arrayOfByte));
  }
  
  