package com.yuneec.IPCameraManager;

import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.format.Time;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Nuvoton
  extends DM368
{
  public static final int RESULT_APP_NOT_READY = -22;
  public static final int RESULT_CARD_PROTECTED = -18;
  public static final int RESULT_HDMI_INSERTED = -16;
  public static final int RESULT_INVALID_OPERATION = -14;
  public static final int RESULT_INVALID_OPTION_VALUE = -13;
  public static final int RESULT_JSON_PACKAGE_ERROR = -7;
  public static final int RESULT_JSON_PACKAGE_TIMEOUT = -8;
  public static final int RESULT_JSON_SYNTAX_ERROR = -9;
  public static final int RESULT_NO_MORE_SPACE = -17;
  public static final int RESULT_OK = 0;
  public static final int RESULT_OPERATION_MISMATCH = -15;
  public static final int RESULT_PIV_NOT_ALLOWED = -20;
  public static final int RESULT_REACH_MAX_CLNT = -5;
  public static final int RESULT_SESSION_START_FAIL = -3;
  public static final int RESULT_SYSTEM_BUSY = -21;
  public static final int RESULT_UNKNOWN_ERROR = -1;
  private static final String TAG = "Nuvoton";
  
  public void formatSDCard(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "formatSDCard");
    postNullRequest(paramMessenger, 14);
  }
  
  public void getBattery(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 30);
  }
  
  public void getDeviceStatus(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getDeviceStatus");
    postRequest(this.SERVER_URL + "SkyEye/server.command?command=check_storage", paramMessenger, 44, 301);
  }
  
  public void getFieldOfView(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getFieldOfView");
    postNullRequest(paramMessenger, 36);
  }
  
  public void getPhotoMode(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getPhotoMode");
    postNullRequest(paramMessenger, 42);
  }
  
  public void getPhotoSize(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getPhotoSize");
    postNullRequest(paramMessenger, 29);
  }
  
  public void getRecordTime(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 25);
  }
  
  public void getSDCardFormat(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getSDCardFormat");
    postNullRequest(paramMessenger, 40);
  }
  
  public void getSDCardFreeSpace(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 39);
  }
  
  public void getSDCardSpace(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 38);
  }
  
  public void getSDCardStatus(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getSDCardStatus");
    postRequest(this.SERVER_URL + "SkyEye/GetStorageCapacity.ncgi", paramMessenger, 10, 303);
  }
  
  public void getVersion(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getVersion");
    postNullRequest(paramMessenger, 23);
  }
  
  public void getVideoResolution(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getVideoResolution");
    postNullRequest(paramMessenger, 32);
  }
  
  public void getVideoStandard(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "getVideoStandard");
    postNullRequest(paramMessenger, 34);
  }
  
  public void getWorkStatus(Messenger paramMessenger)
  {
    postNullRequest(paramMessenger, 37);
  }
  
  public void init()
  {
    this.SERVER_URL = "http://192.168.100.1/";
    this.FILE_PATH = "DCIM/100MEDIA/";
    this.REGEX_FORMAT_1 = "YUNC[\\w]*\\.THM";
    this.REGEX_FORMAT_2 = "YUNC[\\w]*\\.mp4";
    this.REGEX_FORMAT_3 = null;
    this.REGEX_FORMAT_4 = null;
    this.HTTP_CONNECTION_TIMEOUT = 10000;
    this.HTTP_SOCKET_TIMEOUT = 10000;
  }
  
  public void initCamera(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "init camera");
    postNullRequest(paramMessenger, 24);
  }
  
  public void isRecording(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "isRecording");
    postRequest(this.SERVER_URL + "SkyEye/server.command?command=is_recording", paramMessenger, 21, 301);
  }
  
  protected Object onHandleSpecialRequest(Message paramMessage, Object paramObject, IPCameraManager.RequestResult paramRequestResult)
  {
    Object localObject2 = paramObject;
    Object localObject1 = localObject2;
    switch (paramMessage.arg1)
    {
    default: 
      localObject1 = localObject2;
    }
    for (;;)
    {
      paramRequestResult.result = "special_response_handled";
      return localObject1;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      for (;;)
      {
        try
        {
          paramMessage = new org/json/JSONObject;
          paramMessage.<init>((String)paramObject);
          i = paramMessage.getInt("value");
          if (i != 0) {
            continue;
          }
          paramMessage = "HTTPCODE OK";
          paramObject = new java/lang/StringBuilder;
          ((StringBuilder)paramObject).<init>("request resoult: ");
          Log.i("Nuvoton", paramMessage + ",value=" + i);
        }
        catch (JSONException paramMessage)
        {
          paramMessage = "HTTPCODE Internal Error";
          Log.e("Nuvoton", "JSON error");
          continue;
        }
        localObject1 = paramMessage;
        break;
        paramMessage = "HTTPCODE Internal Error";
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("YUNEEC_ver");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      for (;;)
      {
        try
        {
          paramMessage = new org/json/JSONObject;
          paramMessage.<init>((String)paramObject);
          i = paramMessage.getInt("value");
          paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$RecordStatus;
          paramMessage.<init>(false);
          if (i != 1) {
            continue;
          }
          paramMessage.isRecording = true;
        }
        catch (JSONException paramMessage)
        {
          paramMessage = "HTTPCODE Internal Error";
          continue;
        }
        localObject1 = paramMessage;
        break;
        paramMessage.isRecording = false;
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$RecordTime;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$Bettery;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        paramMessage = paramMessage.getString("param");
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardTotalSpace;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardFreeSpace;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("value");
        paramMessage = Integer.valueOf(i);
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = paramMessage;
      continue;
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      paramObject = ((String)paramObject).toCharArray();
      paramMessage = new StringBuilder();
      int i = 0;
      if (i >= paramObject.length) {
        paramObject = "[" + paramMessage.toString() + "]";
      }
      try
      {
        paramMessage = new org/json/JSONArray;
        paramMessage.<init>((String)paramObject);
        paramObject = paramMessage.getJSONObject(0).getString("storage");
        i = paramMessage.getJSONObject(1).getInt("total");
        int j = paramMessage.getJSONObject(2).getInt("used");
        int k = paramMessage.getJSONObject(3).getInt("available");
        localObject1 = paramMessage.getJSONObject(4).getString("unit");
        paramMessage = new com/yuneec/IPCameraManager/Nuvoton$SDcardInfo;
        paramMessage.<init>((String)paramObject, i, j, k, (String)localObject1);
        localObject1 = paramMessage;
        continue;
        paramMessage.append(paramObject[i]);
        if ((paramObject[i] == '}') && (i != paramObject.length - 1)) {
          paramMessage.append(",");
        }
        i++;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardFormat;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
      localObject1 = localObject2;
      if (!"HTTPCODE OK".equals(paramRequestResult.result)) {
        continue;
      }
      try
      {
        paramMessage = new org/json/JSONObject;
        paramMessage.<init>((String)paramObject);
        i = paramMessage.getInt("param");
        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$PhotoMode;
        paramMessage.<init>(i);
        localObject1 = paramMessage;
      }
      catch (JSONException paramMessage)
      {
        for (;;)
        {
          paramMessage = "HTTPCODE Internal Error";
        }
      }
    }
  }
  
  public void resetDefault(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "resetDefault");
    postNullRequest(paramMessenger, 43);
  }
  
  public void restartVF(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "restartVF");
    postNullRequest(paramMessenger, 26);
  }
  
  public void setCC4In1Config(Messenger paramMessenger, Cameras paramCameras)
  {
    paramCameras = Message.obtain();
    paramCameras.what = 1;
    paramCameras.arg1 = 12;
    paramCameras.obj = "HTTPCODE OK";
    try
    {
      paramMessenger.send(paramCameras);
      return;
    }
    catch (RemoteException paramMessenger)
    {
      for (;;)
      {
        paramMessenger.printStackTrace();
      }
    }
  }
  
  public void setFieldOfView(Messenger paramMessenger, int paramInt)
  {
    Log.i("Nuvoton", "setFieldOfView: param = " + paramInt);
    postNullRequest(paramMessenger, 35);
  }
  
  public void setPhotoMode(Messenger paramMessenger, int paramInt)
  {
    Log.i("Nuvoton", "setPhotoMode: mode=" + paramInt);
    postNullRequest(paramMessenger, 41);
  }
  
  public void setPhotoSize(Messenger paramMessenger, int paramInt)
  {
    Log.i("Nuvoton", "setPhotoSize: mode=" + paramInt);
    postNullRequest(paramMessenger, 28);
  }
  
  public void setVideoResolution(Messenger paramMessenger, int paramInt)
  {
    Log.i("Nuvoton", "setVideoResolution: value=" + paramInt);
    postNullRequest(paramMessenger, 31);
  }
  
  public void setVideoStandard(Messenger paramMessenger, int paramInt)
  {
    Log.i("Nuvoton", "setVideoStandard: param= " + paramInt);
    postNullRequest(paramMessenger, 33);
  }
  
  public void snapShot(String paramString1, Messenger paramMessenger, String paramString2)
  {
    Log.i("Nuvoton", "snapShot");
    postRequest(this.SERVER_URL + "SkyEye/server.command?command=snapshot&pipe=0", paramMessenger, 4, 301);
  }
  
  public void startRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("Nuvoton", "startRecord");
    postRequest(this.SERVER_URL + "SkyEye/server.command?command=start_record_pipe&type=h264&pipe=0", paramMessenger, 2, 301);
  }
  
  public void stopRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("Nuvoton", "stopRecord");
    postRequest(this.SERVER_URL + "SkyEye/server.command?command=stop_record&type=h264&pipe=0", paramMessenger, 3, 302);
  }
  
  public void stopVF(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "stopVF");
    postNullRequest(paramMessenger, 27);
  }
  
  public void syncTime(Messenger paramMessenger)
  {
    Log.i("Nuvoton", "syncTime");
    Object localObject = new Time();
    ((Time)localObject).setToNow();
    localObject = ((Time)localObject).format("%Y-%m-%d_%H:%M:%S");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_TIME&time=" + (String)localObject, paramMessenger, 1, 301);
  }
  
  public static class SDcardInfo
  {
    public int available;
    public String storage;
    public int total;
    public String unit;
    public int used;
    
    public SDcardInfo(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2)
    {
      this.storage = paramString1;
      this.total = paramInt1;
      this.used = paramInt2;
      this.available = paramInt3;
      this.unit = paramString2;
    }
  }
}