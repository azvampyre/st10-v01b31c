package com.yuneec.IPCameraManager;

import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class CC4in1
  extends DM368
{
  private static final String TAG = "CC4in1";
  
  public void getSDCardStatus(Messenger paramMessenger)
  {
    Message localMessage;
    if (paramMessenger != null)
    {
      IPCameraManager.SDCardStatus localSDCardStatus = new IPCameraManager.SDCardStatus();
      localSDCardStatus.isInsert = true;
      localSDCardStatus.free_space = 512000L;
      localMessage = Message.obtain();
      localMessage.what = 1;
      localMessage.arg1 = 10;
      localMessage.obj = localSDCardStatus;
    }
    try
    {
      paramMessenger.send(localMessage);
      return;
    }
    catch (RemoteException paramMessenger)
    {
      for (;;)
      {
        paramMessenger.printStackTrace();
      }
    }
  }
  
  public void snapShot(String paramString1, Messenger paramMessenger, String paramString2)
  {
    Log.i("CC4in1", "snapshot");
    postRequest(this.SERVER_URL + "vb.htm" + "?" + "cc4in1capture=1" + "@" + paramString2, paramMessenger, 4, 302);
  }
  
  public void startRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("CC4in1", "start record");
    postRequest(this.SERVER_URL + "vb.htm" + "?" + "cc4in1record=1" + "@" + paramString, paramMessenger, 2, 302);
  }
  
  public void stopRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("CC4in1", "stop record");
    postRequest(this.SERVER_URL + "vb.htm" + "?" + "cc4in1record=0" + "@" + paramString, paramMessenger, 3, 302);
  }
}


