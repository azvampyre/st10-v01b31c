package com.yuneec.IPCameraManager;

import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.format.Time;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class Amba2
  extends DM368
{
  public static final String REMOTE_ADDRESS = "192.168.42.1";
  public static final int RESULT_APP_NOT_READY = -22;
  public static final int RESULT_CARD_PROTECTED = -18;
  public static final int RESULT_HDMI_INSERTED = -16;
  public static final int RESULT_INVALID_OPERATION = -14;
  public static final int RESULT_INVALID_OPTION_VALUE = -13;
  public static final int RESULT_JSON_PACKAGE_ERROR = -7;
  public static final int RESULT_JSON_PACKAGE_TIMEOUT = -8;
  public static final int RESULT_JSON_SYNTAX_ERROR = -9;
  public static final int RESULT_NO_MORE_SPACE = -17;
  public static final int RESULT_OK = 0;
  public static final int RESULT_OPERATION_MISMATCH = -15;
  public static final int RESULT_PIV_NOT_ALLOWED = -20;
  public static final int RESULT_REACH_MAX_CLNT = -5;
  public static final int RESULT_SESSION_START_FAIL = -3;
  public static final int RESULT_SYSTEM_BUSY = -21;
  public static final int RESULT_UNKNOWN_ERROR = -1;
  private static final String TAG = "Amba2";
  
  public void formatSDCard(Messenger paramMessenger)
  {
    Log.i("Amba2", "formatSDCard");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=FORMAT_CARD", paramMessenger, 14, 301);
  }
  
  public void getAEenable(Messenger paramMessenger)
  {
    Log.i("Amba2", "getAEenable");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_AE_ENABLE", paramMessenger, 48, 301);
  }
  
  public void getAudioState(Messenger paramMessenger)
  {
    Log.i("Amba2", "get audio state");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_AUDIO_SW", paramMessenger, 60, 301);
  }
  
  public void getBattery(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_BATTERY_LEVEL", paramMessenger, 30, 301);
  }
  
  public void getBindState(Messenger paramMessenger)
  {
    Log.i("Amba2", "reset Status");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=get_bind_state", paramMessenger, 1002, 301);
  }
  
  public void getCameraMode(Messenger paramMessenger)
  {
    Log.i("Amba2", "getCameraMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_CAM_MODE", paramMessenger, 62, 301);
  }
  
  public void getDeviceStatus(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=DETECT_CARD", paramMessenger, 44, 301);
  }
  
  public void getExposure(Messenger paramMessenger)
  {
    Log.i("Amba2", "getExposure");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_EXPOSURE_VALUE", paramMessenger, 56, 301);
  }
  
  public void getFieldOfView(Messenger paramMessenger)
  {
    Log.i("Amba2", "getFieldOfView");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_FOV", paramMessenger, 36, 301);
  }
  
  public void getIQtype(Messenger paramMessenger)
  {
    Log.i("Amba2", "getIQtype");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_IQ_TYPE", paramMessenger, 52, 301);
  }
  
  public void getPhotoFormat(Messenger paramMessenger)
  {
    Log.i("Amba2", "getPhotoFormat");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_PHOTO_FORMAT", paramMessenger, 46, 301);
  }
  
  public void getPhotoMode(Messenger paramMessenger)
  {
    Log.i("Amba2", "getPhotoMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_PHOTO_MODE", paramMessenger, 42, 301);
  }
  
  public void getPhotoSize(Messenger paramMessenger)
  {
    Log.i("Amba2", "getPhotoSize");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_PHOTO_SIZE", paramMessenger, 29, 301);
  }
  
  public void getRecordTime(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_REC_TIME", paramMessenger, 25, 301);
  }
  
  public void getSDCardFormat(Messenger paramMessenger)
  {
    Log.i("Amba2", "getSDCardFormat");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_CARD_FORMAT", paramMessenger, 40, 301);
  }
  
  public void getSDCardFreeSpace(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SPACE_FREE", paramMessenger, 39, 301);
  }
  
  public void getSDCardSpace(Messenger paramMessenger)
  {
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SPACE", paramMessenger, 38, 301);
  }
  
  public void getSDCardStatus(Messenger paramMessenger)
  {
    Log.i("Amba2", "getSDCardStatus");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SPACE_FREE", paramMessenger, 10, 301);
  }
  
  public void getShutterTimeAndISO(Messenger paramMessenger)
  {
    Log.i("Amba2", "getShutterTimeAndISO");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SH_TM_ISO", paramMessenger, 50, 301);
  }
  
  public void getVersion(Messenger paramMessenger)
  {
    Log.i("Amba2", "getVersion");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_FW_VERSION", paramMessenger, 23, 301);
  }
  
  public void getVideoMode(Messenger paramMessenger)
  {
    Log.i("Amba2", "getVideoMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_VIDEO_MODE", paramMessenger, 58, 301);
  }
  
  public void getVideoResolution(Messenger paramMessenger)
  {
    Log.i("Amba2", "getVideoResolution");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_SETTING", paramMessenger, 32, 301);
  }
  
  public void getVideoStandard(Messenger paramMessenger)
  {
    Log.i("Amba2", "getVideoStandard");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_VIDEO_STANDARD", paramMessenger, 34, 301);
  }
  
  public void getWhiteBalance(Messenger paramMessenger)
  {
    Log.i("Amba2", "getWhiteBalance");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_WHITEBLANCE_MODE", paramMessenger, 54, 301);
  }
  
  public void getWorkStatus(Messenger paramMessenger)
  {
    Log.i("Amba2", "getWorkStatus");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_STATUS", paramMessenger, 37, 301);
  }
  
  public void init()
  {
    this.SERVER_URL = "http://192.168.42.1/";
    this.FILE_PATH = "DCIM/100MEDIA/";
    this.REGEX_FORMAT_1 = "YUNC[\\w]*\\.THM";
    this.REGEX_FORMAT_2 = "YUNC[\\w]*\\.mp4";
    this.REGEX_FORMAT_3 = null;
    this.REGEX_FORMAT_4 = null;
    this.HTTP_CONNECTION_TIMEOUT = 10000;
    this.HTTP_SOCKET_TIMEOUT = 10000;
  }
  
  public void initCamera(Messenger paramMessenger)
  {
    Log.i("Amba2", "init camera");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=INDEX_PAGE", paramMessenger, 24, 301);
  }
  
  public void isRecording(Messenger paramMessenger)
  {
    Log.i("Amba2", "isRecording");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=GET_STATUS", paramMessenger, 21, 301);
  }
  
  protected Object onHandleSpecialRequest(Message paramMessage, Object paramObject, IPCameraManager.RequestResult paramRequestResult)
  {
    Object localObject = paramObject;
    Log.i("Amba2", "****msg.arg1****" + paramMessage.arg1);
    switch (paramMessage.arg1)
    {
    default: 
      paramMessage = (Message)localObject;
    }
    for (;;)
    {
      paramRequestResult.result = "special_response_handled";
      return paramMessage;
      paramMessage = (Message)localObject;
      if ("HTTPCODE OK".equals(paramRequestResult.result))
      {
        try
        {
          localObject = new org/json/JSONObject;
          ((JSONObject)localObject).<init>((String)paramObject);
          if (((JSONObject)localObject).getInt("rval") == 0)
          {
            paramMessage = new com/yuneec/IPCameraManager/CameraParams;
            paramMessage.<init>();
            paramMessage.response = "HTTPCODE OK";
            paramMessage.fw_ver = ((JSONObject)localObject).getString("fw_ver");
            paramMessage.cam_mode = ((JSONObject)localObject).getInt("cam_mode");
            paramMessage.status = ((JSONObject)localObject).getString("status");
            paramMessage.iq_type = ((JSONObject)localObject).getInt("iq_type");
            paramMessage.white_balance = ((JSONObject)localObject).getInt("white_balance");
            paramMessage.sdFree = ((JSONObject)localObject).getInt("sdfree");
            paramMessage.sdTotal = ((JSONObject)localObject).getInt("sdtotal");
            paramMessage.exposure_value = ((JSONObject)localObject).getString("exposure_value");
            paramMessage.video_mode = ((JSONObject)localObject).getString("video_mode");
            paramMessage.record_time = ((JSONObject)localObject).getInt("record_time");
            paramMessage.ae_enable = ((JSONObject)localObject).getInt("ae_enable");
            paramMessage.audio_sw = ((JSONObject)localObject).getInt("audio_sw");
            paramMessage.iso = ((JSONObject)localObject).getString("iso_value");
            paramMessage.shutter_time = ((JSONObject)localObject).getInt("shutter_time");
            paramMessage.photo_format = ((JSONObject)localObject).getString("photo_format");
            if (((JSONObject)localObject).has("audio_enable")) {}
            for (paramMessage.audio_enable = ((JSONObject)localObject).getInt("audio_enable");; paramMessage.audio_enable = 1) {
              break;
            }
          }
        }
        catch (JSONException paramMessage)
        {
          for (;;)
          {
            paramMessage = "HTTPCODE Internal Error";
            continue;
            paramMessage = "HTTPCODE Internal Error";
          }
        }
        paramMessage = (Message)localObject;
        if ("HTTPCODE OK".equals(paramRequestResult.result))
        {
          for (;;)
          {
            try
            {
              paramMessage = new org/json/JSONObject;
              paramMessage.<init>((String)paramObject);
              if (paramMessage.getInt("rval") == 0) {
                paramMessage = "HTTPCODE OK";
              }
            }
            catch (JSONException paramMessage)
            {
              paramMessage = "HTTPCODE Internal Error";
              continue;
            }
            paramMessage = "HTTPCODE Internal Error";
          }
          if ("HTTPCODE OK".equals(paramRequestResult.result))
          {
            try
            {
              paramMessage = new org/json/JSONObject;
              paramMessage.<init>((String)paramObject);
              paramMessage = paramMessage.getString("YUNEEC_ver");
            }
            catch (JSONException paramMessage)
            {
              for (;;)
              {
                paramMessage = "HTTPCODE Internal Error";
              }
            }
          }
          else
          {
            paramMessage = "Unknown";
            continue;
            paramMessage = (Message)localObject;
            if ("HTTPCODE OK".equals(paramRequestResult.result))
            {
              try
              {
                paramMessage = new org/json/JSONObject;
                paramMessage.<init>((String)paramObject);
                paramObject = paramMessage.getString("param");
                paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$RecordStatus;
                paramMessage.<init>(false);
                if ("record".equals(paramObject)) {}
                for (paramMessage.isRecording = true;; paramMessage.isRecording = false) {
                  break;
                }
              }
              catch (JSONException paramMessage)
              {
                for (;;)
                {
                  paramMessage = "HTTPCODE Internal Error";
                }
              }
              paramMessage = (Message)localObject;
              if ("HTTPCODE OK".equals(paramRequestResult.result))
              {
                int i;
                try
                {
                  paramMessage = new org/json/JSONObject;
                  paramMessage.<init>((String)paramObject);
                  if (paramMessage.getInt("rval") == 0)
                  {
                    i = paramMessage.getInt("param");
                    paramObject = new com/yuneec/IPCameraManager/IPCameraManager$RecordTime;
                    ((IPCameraManager.RecordTime)paramObject).<init>(i);
                  }
                }
                catch (JSONException paramMessage)
                {
                  for (;;)
                  {
                    paramObject = "HTTPCODE Internal Error";
                  }
                }
                paramMessage = (Message)paramObject;
                continue;
                paramMessage = (Message)localObject;
                if ("HTTPCODE OK".equals(paramRequestResult.result))
                {
                  try
                  {
                    paramMessage = new org/json/JSONObject;
                    paramMessage.<init>((String)paramObject);
                    paramMessage = paramMessage.getString("param");
                  }
                  catch (JSONException paramMessage)
                  {
                    for (;;)
                    {
                      paramMessage = "HTTPCODE Internal Error";
                    }
                  }
                  paramMessage = (Message)localObject;
                  if ("HTTPCODE OK".equals(paramRequestResult.result))
                  {
                    try
                    {
                      paramMessage = new org/json/JSONObject;
                      paramMessage.<init>((String)paramObject);
                      i = paramMessage.getInt("param");
                      paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$Bettery;
                      paramMessage.<init>(i);
                    }
                    catch (JSONException paramMessage)
                    {
                      for (;;)
                      {
                        paramMessage = "HTTPCODE Internal Error";
                      }
                    }
                    paramMessage = (Message)localObject;
                    if ("HTTPCODE OK".equals(paramRequestResult.result))
                    {
                      try
                      {
                        paramMessage = new org/json/JSONObject;
                        paramMessage.<init>((String)paramObject);
                        paramMessage = paramMessage.getString("param");
                      }
                      catch (JSONException paramMessage)
                      {
                        for (;;)
                        {
                          paramMessage = "HTTPCODE Internal Error";
                        }
                      }
                      paramMessage = (Message)localObject;
                      if ("HTTPCODE OK".equals(paramRequestResult.result))
                      {
                        try
                        {
                          paramMessage = new org/json/JSONObject;
                          paramMessage.<init>((String)paramObject);
                          paramMessage = paramMessage.getString("param");
                        }
                        catch (JSONException paramMessage)
                        {
                          for (;;)
                          {
                            paramMessage = "HTTPCODE Internal Error";
                          }
                        }
                        paramMessage = (Message)localObject;
                        if ("HTTPCODE OK".equals(paramRequestResult.result))
                        {
                          try
                          {
                            paramMessage = new org/json/JSONObject;
                            paramMessage.<init>((String)paramObject);
                            paramMessage = paramMessage.getString("param");
                          }
                          catch (JSONException paramMessage)
                          {
                            for (;;)
                            {
                              paramMessage = "HTTPCODE Internal Error";
                            }
                          }
                          paramMessage = (Message)localObject;
                          if ("HTTPCODE OK".equals(paramRequestResult.result))
                          {
                            try
                            {
                              localObject = new org/json/JSONObject;
                              ((JSONObject)localObject).<init>((String)paramObject);
                              if (((JSONObject)localObject).getInt("rval") == 0)
                              {
                                paramMessage = new com/yuneec/IPCameraManager/CameraParams;
                                paramMessage.<init>();
                                paramMessage.response = "HTTPCODE OK";
                                paramMessage.cam_mode = ((JSONObject)localObject).getInt("cam_mode");
                                paramMessage.status = ((JSONObject)localObject).getString("status");
                                paramMessage.iq_type = ((JSONObject)localObject).getInt("iq_type");
                                paramMessage.white_balance = ((JSONObject)localObject).getInt("white_balance");
                                paramMessage.sdFree = ((JSONObject)localObject).getInt("sdfree");
                                paramMessage.sdTotal = ((JSONObject)localObject).getInt("sdtotal");
                                paramMessage.exposure_value = ((JSONObject)localObject).getString("exposure_value");
                                paramMessage.video_mode = ((JSONObject)localObject).getString("video_mode");
                                paramMessage.record_time = ((JSONObject)localObject).getInt("record_time");
                                paramMessage.ae_enable = ((JSONObject)localObject).getInt("ae_enable");
                                paramMessage.audio_sw = ((JSONObject)localObject).getInt("audio_sw");
                                paramMessage.iso = ((JSONObject)localObject).getString("iso_value");
                                paramMessage.shutter_time = ((JSONObject)localObject).getInt("shutter_time");
                                paramMessage.photo_format = ((JSONObject)localObject).getString("photo_format");
                              }
                              for (;;)
                              {
                                break;
                                paramMessage = "HTTPCODE Internal Error";
                              }
                            }
                            catch (JSONException paramMessage)
                            {
                              for (;;)
                              {
                                paramMessage = "HTTPCODE Internal Error";
                              }
                            }
                            paramMessage = (Message)localObject;
                            if ("HTTPCODE OK".equals(paramRequestResult.result))
                            {
                              try
                              {
                                paramMessage = new org/json/JSONObject;
                                paramMessage.<init>((String)paramObject);
                                i = paramMessage.getInt("param");
                                paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardTotalSpace;
                                paramMessage.<init>(i);
                              }
                              catch (JSONException paramMessage)
                              {
                                for (;;)
                                {
                                  paramMessage = "HTTPCODE Internal Error";
                                }
                              }
                              paramMessage = (Message)localObject;
                              if ("HTTPCODE OK".equals(paramRequestResult.result))
                              {
                                try
                                {
                                  paramMessage = new org/json/JSONObject;
                                  paramMessage.<init>((String)paramObject);
                                  i = paramMessage.getInt("param");
                                  paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardFreeSpace;
                                  paramMessage.<init>(i);
                                }
                                catch (JSONException paramMessage)
                                {
                                  for (;;)
                                  {
                                    paramMessage = "HTTPCODE Internal Error";
                                  }
                                }
                                paramMessage = (Message)localObject;
                                if ("HTTPCODE OK".equals(paramRequestResult.result))
                                {
                                  try
                                  {
                                    paramMessage = new org/json/JSONObject;
                                    paramMessage.<init>((String)paramObject);
                                    i = paramMessage.getInt("param") >> 10;
                                    paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardStatus;
                                    paramMessage.<init>(true, i, 0L);
                                    if (i <= 18) {
                                      paramMessage.isInsert = false;
                                    }
                                  }
                                  catch (JSONException paramMessage)
                                  {
                                    for (;;)
                                    {
                                      paramMessage = "HTTPCODE Internal Error";
                                    }
                                  }
                                  paramMessage = (Message)localObject;
                                  if ("HTTPCODE OK".equals(paramRequestResult.result))
                                  {
                                    try
                                    {
                                      paramMessage = new org/json/JSONObject;
                                      paramMessage.<init>((String)paramObject);
                                      i = paramMessage.getInt("param");
                                      paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$SDCardFormat;
                                      paramMessage.<init>(i);
                                    }
                                    catch (JSONException paramMessage)
                                    {
                                      for (;;)
                                      {
                                        paramMessage = "HTTPCODE Internal Error";
                                      }
                                    }
                                    paramMessage = (Message)localObject;
                                    if ("HTTPCODE OK".equals(paramRequestResult.result))
                                    {
                                      try
                                      {
                                        paramMessage = new org/json/JSONObject;
                                        paramMessage.<init>((String)paramObject);
                                        i = paramMessage.getInt("param");
                                        paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$PhotoMode;
                                        paramMessage.<init>(i);
                                      }
                                      catch (JSONException paramMessage)
                                      {
                                        for (;;)
                                        {
                                          paramMessage = "HTTPCODE Internal Error";
                                        }
                                      }
                                      paramMessage = (Message)localObject;
                                      if ("HTTPCODE OK".equals(paramRequestResult.result))
                                      {
                                        try
                                        {
                                          paramMessage = new org/json/JSONObject;
                                          paramMessage.<init>((String)paramObject);
                                          int k = paramMessage.getInt("free");
                                          i = paramMessage.getInt("total");
                                          int j = paramMessage.getInt("status");
                                          paramObject = paramMessage.getString("video_status");
                                          paramMessage = new java/lang/StringBuilder;
                                          paramMessage.<init>("Work status is: ");
                                          Log.i("Amba2", (String)paramObject);
                                          paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$DevicesStatus;
                                          paramMessage.<init>(k, i, j, (String)paramObject);
                                        }
                                        catch (JSONException paramMessage)
                                        {
                                          for (;;)
                                          {
                                            paramMessage = "HTTPCODE Internal Error";
                                          }
                                        }
                                        paramMessage = (Message)localObject;
                                        if ("HTTPCODE OK".equals(paramRequestResult.result))
                                        {
                                          try
                                          {
                                            paramMessage = new org/json/JSONObject;
                                            paramMessage.<init>((String)paramObject);
                                            paramMessage = paramMessage.getString("photo_format");
                                            paramObject = new java/lang/StringBuilder;
                                            ((StringBuilder)paramObject).<init>("Photo format is: ");
                                            Log.i("Amba2", paramMessage);
                                          }
                                          catch (JSONException paramMessage)
                                          {
                                            for (;;)
                                            {
                                              paramMessage = "HTTPCODE Internal Error";
                                            }
                                          }
                                          paramMessage = (Message)localObject;
                                          if ("HTTPCODE OK".equals(paramRequestResult.result))
                                          {
                                            try
                                            {
                                              paramMessage = new org/json/JSONObject;
                                              paramMessage.<init>((String)paramObject);
                                              paramMessage = Integer.valueOf(paramMessage.getInt("ae_enable"));
                                              paramObject = new java/lang/StringBuilder;
                                              ((StringBuilder)paramObject).<init>("AE enable is: ");
                                              Log.i("Amba2", paramMessage);
                                            }
                                            catch (JSONException paramMessage)
                                            {
                                              for (;;)
                                              {
                                                paramMessage = "HTTPCODE Internal Error";
                                              }
                                            }
                                            paramMessage = (Message)localObject;
                                            if ("HTTPCODE OK".equals(paramRequestResult.result))
                                            {
                                              try
                                              {
                                                paramMessage = new org/json/JSONObject;
                                                paramMessage.<init>((String)paramObject);
                                                i = paramMessage.getInt("shutter_time");
                                                paramObject = paramMessage.getString("iso_value");
                                                paramMessage = new java/lang/StringBuilder;
                                                paramMessage.<init>("Shutter time is: ");
                                                Log.i("Amba2", i + "ISO is: " + (String)paramObject);
                                                paramMessage = new com/yuneec/IPCameraManager/IPCameraManager$ShutterTimeISO;
                                                paramMessage.<init>();
                                                paramMessage.iso = ((String)paramObject);
                                                paramMessage.time = i;
                                              }
                                              catch (JSONException paramMessage)
                                              {
                                                for (;;)
                                                {
                                                  paramMessage = "HTTPCODE Internal Error";
                                                }
                                              }
                                              paramMessage = (Message)localObject;
                                              if ("HTTPCODE OK".equals(paramRequestResult.result))
                                              {
                                                try
                                                {
                                                  paramMessage = new org/json/JSONObject;
                                                  paramMessage.<init>((String)paramObject);
                                                  paramMessage = Integer.valueOf(paramMessage.getInt("IQ_type"));
                                                  paramObject = new java/lang/StringBuilder;
                                                  ((StringBuilder)paramObject).<init>("IQ type is: ");
                                                  Log.i("Amba2", paramMessage);
                                                }
                                                catch (JSONException paramMessage)
                                                {
                                                  for (;;)
                                                  {
                                                    paramMessage = "HTTPCODE Internal Error";
                                                  }
                                                }
                                                paramMessage = (Message)localObject;
                                                if ("HTTPCODE OK".equals(paramRequestResult.result))
                                                {
                                                  try
                                                  {
                                                    paramMessage = new org/json/JSONObject;
                                                    paramMessage.<init>((String)paramObject);
                                                    paramMessage = Integer.valueOf(paramMessage.getInt("wb_mode"));
                                                    paramObject = new java/lang/StringBuilder;
                                                    ((StringBuilder)paramObject).<init>("White balance is: ");
                                                    Log.i("Amba2", paramMessage);
                                                  }
                                                  catch (JSONException paramMessage)
                                                  {
                                                    for (;;)
                                                    {
                                                      paramMessage = "HTTPCODE Internal Error";
                                                    }
                                                  }
                                                  paramMessage = (Message)localObject;
                                                  if ("HTTPCODE OK".equals(paramRequestResult.result))
                                                  {
                                                    try
                                                    {
                                                      paramMessage = new org/json/JSONObject;
                                                      paramMessage.<init>((String)paramObject);
                                                      paramMessage = paramMessage.getString("exposure_value");
                                                      paramObject = new java/lang/StringBuilder;
                                                      ((StringBuilder)paramObject).<init>("Exposure value is: ");
                                                      Log.i("Amba2", paramMessage);
                                                    }
                                                    catch (JSONException paramMessage)
                                                    {
                                                      for (;;)
                                                      {
                                                        paramMessage = "HTTPCODE Internal Error";
                                                      }
                                                    }
                                                    paramMessage = (Message)localObject;
                                                    if ("HTTPCODE OK".equals(paramRequestResult.result))
                                                    {
                                                      try
                                                      {
                                                        paramMessage = new org/json/JSONObject;
                                                        paramMessage.<init>((String)paramObject);
                                                        paramMessage = paramMessage.getString("video_mode");
                                                        paramObject = new java/lang/StringBuilder;
                                                        ((StringBuilder)paramObject).<init>("Video mode is: ");
                                                        Log.i("Amba2", paramMessage);
                                                      }
                                                      catch (JSONException paramMessage)
                                                      {
                                                        for (;;)
                                                        {
                                                          paramMessage = "HTTPCODE Internal Error";
                                                        }
                                                      }
                                                      paramMessage = (Message)localObject;
                                                      if ("HTTPCODE OK".equals(paramRequestResult.result))
                                                      {
                                                        try
                                                        {
                                                          paramMessage = new org/json/JSONObject;
                                                          paramMessage.<init>((String)paramObject);
                                                          paramMessage = Integer.valueOf(paramMessage.getInt("audio_sw"));
                                                          paramObject = new java/lang/StringBuilder;
                                                          ((StringBuilder)paramObject).<init>("Audio state is: ");
                                                          Log.i("Amba2", paramMessage);
                                                        }
                                                        catch (JSONException paramMessage)
                                                        {
                                                          for (;;)
                                                          {
                                                            paramMessage = "HTTPCODE Internal Error";
                                                          }
                                                        }
                                                        paramMessage = (Message)localObject;
                                                        if ("HTTPCODE OK".equals(paramRequestResult.result))
                                                        {
                                                          try
                                                          {
                                                            paramMessage = new org/json/JSONObject;
                                                            paramMessage.<init>((String)paramObject);
                                                            paramMessage = Integer.valueOf(paramMessage.getInt("cam_mode"));
                                                            paramObject = new java/lang/StringBuilder;
                                                            ((StringBuilder)paramObject).<init>("camera mode is: ");
                                                            Log.i("Amba2", paramMessage);
                                                          }
                                                          catch (JSONException paramMessage)
                                                          {
                                                            for (;;)
                                                            {
                                                              paramMessage = "HTTPCODE Internal Error";
                                                            }
                                                          }
                                                          if ("HTTPCODE OK".equals(paramRequestResult.result))
                                                          {
                                                            try
                                                            {
                                                              localObject = new org/json/JSONObject;
                                                              ((JSONObject)localObject).<init>((String)paramObject);
                                                              paramMessage = new com/yuneec/IPCameraManager/BindResponse;
                                                              paramMessage.<init>(true, "ok".equals(((JSONObject)localObject).getString("result")), ((JSONObject)localObject).getString("server_mac_address"));
                                                            }
                                                            catch (JSONException paramMessage)
                                                            {
                                                              for (;;)
                                                              {
                                                                paramMessage = new BindResponse(true, false, null);
                                                              }
                                                            }
                                                          }
                                                          else
                                                          {
                                                            paramMessage = new BindResponse(false, false, null);
                                                            continue;
                                                            if ("HTTPCODE OK".equals(paramRequestResult.result)) {
                                                              try
                                                              {
                                                                localObject = new org/json/JSONObject;
                                                                ((JSONObject)localObject).<init>((String)paramObject);
                                                                paramMessage = new com/yuneec/IPCameraManager/BindStateResponse;
                                                                paramMessage.<init>(true, "yes".equals(((JSONObject)localObject).getString("isbinded")), ((JSONObject)localObject).getString("binded_client_address"));
                                                              }
                                                              catch (JSONException paramMessage)
                                                              {
                                                                for (;;)
                                                                {
                                                                  paramMessage = new BindStateResponse(false, true, "ff:ff:ff:ff:ff:ff");
                                                                }
                                                              }
                                                            } else {
                                                              paramMessage = new BindStateResponse(false, true, "ff:ff:ff:ff:ff:ff");
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  public void requestBind(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "reset Status");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=request_bind&client_mac_address=" + paramString, paramMessenger, 1001, 301);
  }
  
  public void resetDefault(Messenger paramMessenger)
  {
    Log.i("Amba2", "resetDefault");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=RESET_DEFAULT", paramMessenger, 43, 302);
  }
  
  public void resetStatus(Messenger paramMessenger)
  {
    Log.i("Amba2", "reset Status");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=RESET_STATUS", paramMessenger, 63, 301);
  }
  
  public void restartVF(Messenger paramMessenger)
  {
    Log.i("Amba2", "restartVF");
    stopVF(paramMessenger);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=REST_VF", paramMessenger, 26, 301);
  }
  
  public void setAEenable(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setAEenable: " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_AE_ENABLE&mode=" + paramInt, paramMessenger, 47, 301);
  }
  
  public void setAudioState(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "set audio state: state=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_AUDIO_SW&mode=" + paramInt, paramMessenger, 59, 301);
  }
  
  public void setCC4In1Config(Messenger paramMessenger, Cameras paramCameras)
  {
    paramCameras = Message.obtain();
    paramCameras.what = 1;
    paramCameras.arg1 = 12;
    paramCameras.obj = "HTTPCODE OK";
    try
    {
      paramMessenger.send(paramCameras);
      return;
    }
    catch (RemoteException paramMessenger)
    {
      for (;;)
      {
        paramMessenger.printStackTrace();
      }
    }
  }
  
  public void setCameraMode(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "setCameraMode");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_CAM_MODE&mode=" + paramString, paramMessenger, 61, 301);
  }
  
  public void setExposure(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "setExposure:" + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_EXPOSURE_VALUE&mode=" + paramString, paramMessenger, 55, 301);
  }
  
  public void setFieldOfView(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setFieldOfView: param = " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_FOV&PARAM=" + paramInt, paramMessenger, 35, 302);
  }
  
  public void setIQtype(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setIQtype: " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_IQ_TYPE&mode=" + paramInt, paramMessenger, 51, 301);
  }
  
  public void setPhotoFormat(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "setPhotoFormat: " + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_PHOTO_FORMAT&value=" + paramString, paramMessenger, 45, 301);
  }
  
  public void setPhotoMode(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setPhotoMode: mode=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_PHOTO_MODE&MODE=" + paramInt, paramMessenger, 41, 302);
  }
  
  public void setPhotoSize(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setPhotoSize: mode=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_PHOTO_SIZE&MODE=" + paramInt, paramMessenger, 28, 302);
  }
  
  public void setRtspResolution(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "reset Status");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_RTSP_VID&Reslution=" + paramString, paramMessenger, 1002, 301);
  }
  
  public void setShutterTimeAndISO(Messenger paramMessenger, int paramInt, String paramString)
  {
    Log.i("Amba2", "setShutterTimeAndISO: time=" + paramInt + ",iso=" + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_SH_TM_ISO&time=" + paramInt + "&value=" + paramString, paramMessenger, 49, 301);
  }
  
  public void setVideoMode(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "setVideoMode锛� " + paramString);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_VIDEO_MODE&video_mode=" + paramString, paramMessenger, 57, 301);
  }
  
  public void setVideoResolution(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setVideoResolution: value=" + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_SETTING&resolution=" + paramInt, paramMessenger, 31, 301);
  }
  
  public void setVideoStandard(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setVideoStandard: param= " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_VIDEO_STANDARD&PARAM=" + paramInt, paramMessenger, 33, 301);
  }
  
  public void setWhiteBalance(Messenger paramMessenger, int paramInt)
  {
    Log.i("Amba2", "setWhiteBalance: " + paramInt);
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_WHITEBLANCE_MODE&mode=" + paramInt, paramMessenger, 53, 301);
  }
  
  public void snapShot(String paramString1, Messenger paramMessenger, String paramString2)
  {
    Log.i("Amba2", "snapShot");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=TAKE_PHOTO", paramMessenger, 4, 301);
  }
  
  public void startRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "startRecord");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=START_RECORD", paramMessenger, 2, 301);
  }
  
  public void stopRecord(Messenger paramMessenger, String paramString)
  {
    Log.i("Amba2", "stopRecord");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=STOP_RECORD", paramMessenger, 3, 302);
  }
  
  public void stopVF(Messenger paramMessenger)
  {
    Log.i("Amba2", "stopVF");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=STOP_VF", paramMessenger, 27, 301);
  }
  
  public void syncTime(Messenger paramMessenger)
  {
    Log.i("Amba2", "syncTime");
    Object localObject = new Time();
    ((Time)localObject).setToNow();
    localObject = ((Time)localObject).format("%Y-%m-%d_%H:%M:%S");
    postRequest(this.SERVER_URL + "cgi-bin/cgi?CMD=SET_TIME&time=" + (String)localObject, paramMessenger, 1, 301);
  }
}


