package com.yuneec.IPCameraManager;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CameraParams
  implements Parcelable
{
  public static final Parcelable.Creator<CameraParams> CREATOR = new Parcelable.Creator()
  {
    public CameraParams createFromParcel(Parcel paramAnonymousParcel)
    {
      return new CameraParams(paramAnonymousParcel);
    }
    
    public CameraParams[] newArray(int paramAnonymousInt)
    {
      return new CameraParams[paramAnonymousInt];
    }
  };
  public int ae_enable;
  public int audio_enable;
  public int audio_sw;
  public int cam_mode;
  public String exposure_value;
  public String fw_ver;
  public int iq_type;
  public String iso;
  public String photo_format;
  public int record_time;
  public String response;
  public int sdFree;
  public int sdTotal;
  public int shutter_time;
  public String status;
  public String video_mode;
  public int white_balance;
  
  public CameraParams() {}
  
  public CameraParams(Parcel paramParcel)
  {
    this.response = paramParcel.readString();
    this.fw_ver = paramParcel.readString();
    this.cam_mode = paramParcel.readInt();
    this.status = paramParcel.readString();
    this.iq_type = paramParcel.readInt();
    this.white_balance = paramParcel.readInt();
    this.sdFree = paramParcel.readInt();
    this.sdTotal = paramParcel.readInt();
    this.exposure_value = paramParcel.readString();
    this.video_mode = paramParcel.readString();
    this.record_time = paramParcel.readInt();
    this.ae_enable = paramParcel.readInt();
    this.audio_sw = paramParcel.readInt();
    this.shutter_time = paramParcel.readInt();
    this.iso = paramParcel.readString();
    this.photo_format = paramParcel.readString();
    this.audio_enable = paramParcel.readInt();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.response);
    paramParcel.writeString(this.fw_ver);
    paramParcel.writeInt(this.cam_mode);
    paramParcel.writeString(this.status);
    paramParcel.writeInt(this.iq_type);
    paramParcel.writeInt(this.white_balance);
    paramParcel.writeInt(this.sdFree);
    paramParcel.writeInt(this.sdTotal);
    paramParcel.writeString(this.exposure_value);
    paramParcel.writeString(this.video_mode);
    paramParcel.writeInt(this.record_time);
    paramParcel.writeInt(this.ae_enable);
    paramParcel.writeInt(this.audio_sw);
    paramParcel.writeInt(this.shutter_time);
    paramParcel.writeString(this.iso);
    paramParcel.writeString(this.photo_format);
    paramParcel.writeInt(this.audio_enable);
  }
}


