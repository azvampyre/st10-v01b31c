package com.yuneec.IPCameraManager;

import com.yuneec.IPCameraManager.cgo4.ResponseResult;

public class BindResponse
  extends ResponseResult
{
  public final boolean bindedResult;
  public final String serverMacAddress;
  
  public BindResponse(boolean paramBoolean1, boolean paramBoolean2, String paramString)
  {
    super(paramBoolean1);
    this.bindedResult = paramBoolean2;
    this.serverMacAddress = paramString;
  }
}


