package com.yuneec.IPCameraManager;

import com.yuneec.IPCameraManager.cgo4.ResponseResult;

public class BindStateResponse
  extends ResponseResult
{
  public final String bindedClientAddress;
  public final boolean isBinded;
  
  public BindStateResponse(boolean paramBoolean1, boolean paramBoolean2, String paramString)
  {
    super(paramBoolean1);
    this.isBinded = paramBoolean2;
    this.bindedClientAddress = paramString;
  }
}


