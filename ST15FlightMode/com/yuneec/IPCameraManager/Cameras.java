package com.yuneec.IPCameraManager;

public class Cameras
{
  private int id;
  private String p_codep;
  private String p_codev;
  private String p_dr;
  private String p_f;
  private String p_intervalp;
  private String p_intervalv;
  private String p_n;
  private String p_name;
  private String p_t1;
  private String p_t2;
  private String p_type;
  
  public String getCodep()
  {
    return this.p_codep;
  }
  
  public String getCodev()
  {
    return this.p_codev;
  }
  
  public String getDr()
  {
    return this.p_dr;
  }
  
  public String getF()
  {
    return this.p_f;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public String getIntervalp()
  {
    return this.p_intervalp;
  }
  
  public String getIntervalv()
  {
    return this.p_intervalv;
  }
  
  public String getN()
  {
    return this.p_n;
  }
  
  public String getName()
  {
    return this.p_name;
  }
  
  public String getT1()
  {
    return this.p_t1;
  }
  
  public String getT2()
  {
    return this.p_t2;
  }
  
  public String getType()
  {
    return this.p_type;
  }
  
  public void setCodep(String paramString)
  {
    this.p_codep = paramString;
  }
  
  public void setCodev(String paramString)
  {
    this.p_codev = paramString;
  }
  
  public void setDr(String paramString)
  {
    this.p_dr = paramString;
  }
  
  public void setF(String paramString)
  {
    this.p_f = paramString;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setIntervalp(String paramString)
  {
    this.p_intervalp = paramString;
  }
  
  public void setIntervalv(String paramString)
  {
    this.p_intervalv = paramString;
  }
  
  public void setN(String paramString)
  {
    this.p_n = paramString;
  }
  
  public void setName(String paramString)
  {
    this.p_name = paramString;
  }
  
  public void setT1(String paramString)
  {
    this.p_t1 = paramString;
  }
  
  public void setT2(String paramString)
  {
    this.p_t2 = paramString;
  }
  
  public void setType(String paramString)
  {
    this.p_type = paramString;
  }
}


