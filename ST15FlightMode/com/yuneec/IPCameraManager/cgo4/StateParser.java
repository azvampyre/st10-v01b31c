package com.yuneec.IPCameraManager.cgo4;

import android.util.Xml;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class StateParser
{
  static final String RESULT_NODE = "result";
  static final String RESULT_OK = "ok";
  static final String STATE_NODE = "state";
  private static final String TAG = StateParser.class.getSimpleName();
  private static final String ns = null;
  
  private static double fractionToDouble(String paramString)
  {
    if (paramString.contains("/")) {
      paramString = paramString.split("/");
    }
    for (double d = Double.parseDouble(paramString[0]) / Double.parseDouble(paramString[1]);; d = Double.parseDouble(paramString)) {
      return d;
    }
  }
  
  public static StateResponse parse(String paramString)
  {
    ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes());
    for (;;)
    {
      try
      {
        paramString = Xml.newPullParser();
        paramString.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        paramString.setInput(localByteArrayInputStream, null);
        paramString.nextTag();
        paramString = readResponseState(paramString);
      }
      catch (Exception paramString)
      {
        paramString = paramString;
        paramString = new StateResponse(false, false, 0.0D, false);
        try
        {
          localIOException1.close();
        }
        catch (IOException localIOException2)
        {
          localIOException2.printStackTrace();
        }
        continue;
      }
      finally {}
      try
      {
        localByteArrayInputStream.close();
        return paramString;
      }
      catch (IOException localIOException1)
      {
        localIOException1.printStackTrace();
      }
    }
    try
    {
      localIOException2.close();
      throw paramString;
    }
    catch (IOException localIOException3)
    {
      for (;;)
      {
        localIOException3.printStackTrace();
      }
    }
  }
  
  private static StateResponse readInfo(XmlPullParser paramXmlPullParser, boolean paramBoolean)
    throws XmlPullParserException, IOException
  {
    String str6 = "0";
    String str2 = "unset";
    String str4 = "off";
    String str3 = "0";
    String str5 = "-1";
    String str1 = "-1";
    paramXmlPullParser.require(2, ns, "state");
    for (;;)
    {
      if (paramXmlPullParser.next() == 3)
      {
        paramXmlPullParser.require(3, ns, "state");
        paramXmlPullParser = new StateResponse(paramBoolean, "set".equals(str2), fractionToDouble(str6), "on".equals(str4));
        paramXmlPullParser.recordedTime = Integer.parseInt(str3);
        paramXmlPullParser.phtotoCapacity = Integer.parseInt(str5);
        paramXmlPullParser.videoCapacity = Integer.parseInt(str1);
        return paramXmlPullParser;
      }
      if (paramXmlPullParser.getEventType() == 2)
      {
        String str7 = paramXmlPullParser.getName();
        if (str7.equals("batt")) {
          str6 = readTextNode(paramXmlPullParser, str7);
        } else if (str7.equals("sd_memory")) {
          str2 = readTextNode(paramXmlPullParser, str7);
        } else if (str7.equals("rec")) {
          str4 = readTextNode(paramXmlPullParser, str7);
        } else if (str7.equals("progress_time")) {
          str3 = readTextNode(paramXmlPullParser, str7);
        } else if (str7.equals("remaincapacity")) {
          str5 = readTextNode(paramXmlPullParser, str7);
        } else if (str7.equals("video_remaincapacity")) {
          str1 = readTextNode(paramXmlPullParser, str7);
        } else {
          skip(paramXmlPullParser);
        }
      }
    }
  }
  
  private static StateResponse readResponseState(XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    paramXmlPullParser.require(2, ns, "camrply");
    boolean bool = false;
    for (;;)
    {
      if (paramXmlPullParser.next() == 3) {}
      for (paramXmlPullParser = new StateResponse(false, false, 0.0D, false);; paramXmlPullParser = readInfo(paramXmlPullParser, bool))
      {
        return paramXmlPullParser;
        if (paramXmlPullParser.getEventType() != 2) {
          break;
        }
        String str = paramXmlPullParser.getName();
        if (str.equals("result"))
        {
          bool = "ok".equals(readTextNode(paramXmlPullParser, str));
          break;
        }
        if (!str.equals("state")) {
          break label96;
        }
      }
      label96:
      skip(paramXmlPullParser);
    }
  }
  
  private static String readText(XmlPullParser paramXmlPullParser)
    throws IOException, XmlPullParserException
  {
    String str = "";
    if (paramXmlPullParser.next() == 4)
    {
      str = paramXmlPullParser.getText();
      paramXmlPullParser.nextTag();
    }
    return str;
  }
  
  private static String readTextNode(XmlPullParser paramXmlPullParser, String paramString)
    throws XmlPullParserException, IOException
  {
    paramXmlPullParser.require(2, ns, paramString);
    String str = readText(paramXmlPullParser);
    paramXmlPullParser.require(3, ns, paramString);
    return str;
  }
  
  private static void skip(XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    if (paramXmlPullParser.getEventType() != 2) {
      throw new IllegalStateException();
    }
    int i = 1;
    for (;;)
    {
      if (i == 0) {
        return;
      }
      switch (paramXmlPullParser.next())
      {
      default: 
        break;
      case 2: 
        i++;
        break;
      case 3: 
        i--;
      }
    }
  }
}


