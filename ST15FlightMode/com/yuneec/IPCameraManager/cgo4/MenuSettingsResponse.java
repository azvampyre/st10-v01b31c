package com.yuneec.IPCameraManager.cgo4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MenuSettingsResponse
  extends ResponseResult
{
  private final int MAX_LIST_SIZE = 100;
  public final List<MenuSettingsNode> menuNodes;
  
  public MenuSettingsResponse(boolean paramBoolean, List<MenuSettingsNode> paramList)
  {
    super(paramBoolean);
    this.menuNodes = paramList;
  }
  
  public boolean equals(Object paramObject)
  {
    return false;
  }
  
  public MenuSettingsItemContainer findSettingItemWithKey(String paramString)
  {
    if ((paramString == null) || (this.menuNodes == null)) {
      paramString = null;
    }
    for (;;)
    {
      return paramString;
      Iterator localIterator = this.menuNodes.iterator();
      MenuSettingsItemContainer localMenuSettingsItemContainer;
      do
      {
        if (!localIterator.hasNext())
        {
          paramString = null;
          break;
        }
        localMenuSettingsItemContainer = (MenuSettingsItemContainer)((MenuSettingsNode)localIterator.next()).containers.get(paramString);
      } while (localMenuSettingsItemContainer == null);
      paramString = localMenuSettingsItemContainer;
    }
  }
  
  public int hashCode()
  {
    if (this.isOk) {}
    for (int i = 100 % this.menuNodes.size() + 100;; i = 100 % this.menuNodes.size()) {
      return i;
    }
  }
}


