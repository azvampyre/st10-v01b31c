package com.yuneec.IPCameraManager.cgo4;

import android.util.Log;
import android.util.Xml;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MenuSettingsXmlParser
{
  static final String IS_ENABLE = "yes";
  static final String ITEM_ENABLE_TAG = "enable";
  static final String ITEM_ID_TAG = "id";
  static final String ITEM_NODE_NAME = "item";
  static final String ITEM_OPTION2_TAG = "option2";
  static final String ITEM_OPTION_TAG = "option";
  static final String ITEM_VALUE2_TAG = "value2";
  static final String ITEM_VALUE_TAG = "value";
  static final String MAIN_MENU_NODE_NAME = "mainmenu";
  static final String MENU_INFO_NODE_NAME = "menuinfo";
  static final String PHOTOSETTINGS_NODE_NAME = "photosettings";
  static final String QMENU2_NODE_NAME = "qmenu2";
  static final String RESULT_NODE_NAME = "result";
  static final String RESULT_OK = "ok";
  private static final String TAG = MenuSettingsXmlParser.class.getSimpleName();
  private static final String ns = null;
  
  private List<MenuSettingsNode> readMenuInfo(XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    paramXmlPullParser.require(2, ns, "menuinfo");
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      if (paramXmlPullParser.next() == 3)
      {
        paramXmlPullParser.require(3, ns, "menuinfo");
        return localArrayList;
      }
      if (paramXmlPullParser.getEventType() == 2)
      {
        String str = paramXmlPullParser.getName();
        if (str.equals("mainmenu")) {
          localArrayList.add(readMenuNode(paramXmlPullParser, "mainmenu"));
        } else if (str.equals("photosettings")) {
          localArrayList.add(readMenuNode(paramXmlPullParser, "photosettings"));
        } else if (str.equals("qmenu2")) {
          localArrayList.add(readMenuNode(paramXmlPullParser, "qmenu2"));
        } else {
          skip(paramXmlPullParser);
        }
      }
    }
  }
  
  private MenuSettingsNode readMenuNode(XmlPullParser paramXmlPullParser, String paramString)
    throws XmlPullParserException, IOException
  {
    MenuSettingsNode localMenuSettingsNode = new MenuSettingsNode(paramString);
    paramXmlPullParser.require(2, ns, paramString);
    Object localObject2 = null;
    Object localObject1 = " ";
    for (;;)
    {
      if (paramXmlPullParser.next() == 3)
      {
        paramXmlPullParser.require(3, ns, paramString);
        return localMenuSettingsNode;
      }
      if (paramXmlPullParser.getEventType() == 2)
      {
        if (paramXmlPullParser.getName().equals("item"))
        {
          paramXmlPullParser.require(2, ns, "item");
          Object localObject4 = localObject2;
          Object localObject3 = localObject1;
          String str1;
          if (paramXmlPullParser.getName().equals("item"))
          {
            localObject3 = paramXmlPullParser.getAttributeValue(null, "id");
            localObject4 = paramXmlPullParser.getAttributeValue(null, "enable");
            if ((localObject3 == null) || (localObject4 == null)) {
              break label374;
            }
            str1 = paramXmlPullParser.getAttributeValue(null, "value");
            if (str1 == null) {
              break label281;
            }
            localObject1 = paramXmlPullParser.getAttributeValue(null, "value2");
            localObject2 = new MenuSettingsItemContainer((String)localObject3, ((String)localObject4).equals("yes"), str1, (String)localObject1);
            localMenuSettingsNode.containers.put(localObject3, localObject2);
            if (((MenuSettingsItemContainer)localObject2).id.length() <= "menu_item_id_f".length()) {
              break label272;
            }
            localObject1 = ((MenuSettingsItemContainer)localObject2).id.substring(0, ((MenuSettingsItemContainer)localObject2).id.length() - 1);
          }
          for (;;)
          {
            paramXmlPullParser.nextTag();
            localObject3 = localObject1;
            localObject4 = localObject2;
            paramXmlPullParser.require(3, ns, "item");
            localObject2 = localObject4;
            localObject1 = localObject3;
            break;
            label272:
            localObject1 = ((MenuSettingsItemContainer)localObject2).id;
            continue;
            label281:
            str1 = paramXmlPullParser.getAttributeValue(null, "option");
            String str2 = paramXmlPullParser.getAttributeValue(null, "option2");
            localObject3 = new MenuSettingsItem((String)localObject3, ((String)localObject4).equals("yes"), str1, str2);
            if ((localObject2 == null) || (!((MenuSettingsItem)localObject3).id.contains((CharSequence)localObject1)))
            {
              localMenuSettingsNode.items.add(localObject3);
            }
            else
            {
              ((MenuSettingsItemContainer)localObject2).itemList.add(localObject3);
              continue;
              label374:
              Log.e(TAG, "itemId or itemEnable is null!");
            }
          }
        }
        skip(paramXmlPullParser);
      }
    }
  }
  
  private MenuSettingsResponse readResponseMenu(XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    paramXmlPullParser.require(2, ns, "camrply");
    boolean bool = false;
    Object localObject = null;
    for (;;)
    {
      if (paramXmlPullParser.next() == 3) {}
      for (paramXmlPullParser = (XmlPullParser)localObject;; paramXmlPullParser = readMenuInfo(paramXmlPullParser))
      {
        return new MenuSettingsResponse(bool, paramXmlPullParser);
        if (paramXmlPullParser.getEventType() != 2) {
          break;
        }
        String str = paramXmlPullParser.getName();
        if (str.equals("result"))
        {
          bool = readResult(paramXmlPullParser);
          break;
        }
        if (!str.equals("menuinfo")) {
          break label94;
        }
      }
      label94:
      skip(paramXmlPullParser);
    }
  }
  
  private boolean readResult(XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    paramXmlPullParser.require(2, ns, "result");
    String str = readText(paramXmlPullParser);
    paramXmlPullParser.require(3, ns, "result");
    return str.equals("ok");
  }
  
  private String readText(XmlPullParser paramXmlPullParser)
    throws IOException, XmlPullParserException
  {
    String str = "";
    if (paramXmlPullParser.next() == 4)
    {
      str = paramXmlPullParser.getText();
      paramXmlPullParser.nextTag();
    }
    return str;
  }
  
  private void skip(XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    if (paramXmlPullParser.getEventType() != 2) {
      throw new IllegalStateException();
    }
    int i = 1;
    for (;;)
    {
      if (i == 0) {
        return;
      }
      switch (paramXmlPullParser.next())
      {
      default: 
        break;
      case 2: 
        i++;
        break;
      case 3: 
        i--;
      }
    }
  }
  
  public MenuSettingsResponse parse(String paramString)
  {
    ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes());
    for (;;)
    {
      try
      {
        paramString = Xml.newPullParser();
        paramString.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        paramString.setInput(localByteArrayInputStream, null);
        paramString.nextTag();
        paramString = readResponseMenu(paramString);
      }
      catch (Exception paramString)
      {
        paramString = paramString;
        paramString = new MenuSettingsResponse(false, null);
        try
        {
          localIOException1.close();
        }
        catch (IOException localIOException2)
        {
          localIOException2.printStackTrace();
        }
        continue;
      }
      finally {}
      try
      {
        localByteArrayInputStream.close();
        return paramString;
      }
      catch (IOException localIOException1)
      {
        localIOException1.printStackTrace();
      }
    }
    try
    {
      localIOException2.close();
      throw paramString;
    }
    catch (IOException localIOException3)
    {
      for (;;)
      {
        localIOException3.printStackTrace();
      }
    }
  }
}


