package com.yuneec.IPCameraManager.cgo4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MenuSettingsItemContainer
  extends MenuSettingsItem
{
  public final List<MenuSettingsItem> itemList;
  public final String value;
  public String value2;
  
  public MenuSettingsItemContainer(String paramString1, boolean paramBoolean, String paramString2)
  {
    super(paramString1, paramBoolean);
    this.value = paramString2;
    this.itemList = new ArrayList();
  }
  
  public MenuSettingsItemContainer(String paramString1, boolean paramBoolean, String paramString2, String paramString3)
  {
    this(paramString1, paramBoolean, paramString2);
    this.value2 = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (!(paramObject instanceof MenuSettingsItemContainer)) {
      bool = false;
    }
    for (;;)
    {
      return bool;
      if (!this.id.equals(((MenuSettingsItemContainer)paramObject).id))
      {
        bool = false;
      }
      else if (this.isEnable != ((MenuSettingsItemContainer)paramObject).isEnable)
      {
        bool = false;
      }
      else if (!this.value.equals(((MenuSettingsItemContainer)paramObject).value))
      {
        bool = false;
      }
      else
      {
        paramObject = ((MenuSettingsItemContainer)paramObject).itemList;
        if (this.itemList.size() == ((List)paramObject).size()) {
          break;
        }
        bool = false;
      }
    }
    int i = 0;
    Iterator localIterator = this.itemList.iterator();
    for (;;)
    {
      if (!localIterator.hasNext())
      {
        bool = true;
        break;
      }
      if (!((MenuSettingsItem)localIterator.next()).equals(((List)paramObject).get(i)))
      {
        bool = false;
        break;
      }
      i++;
    }
  }
  
  public int hashCode()
  {
    int k = 0;
    int m = this.id.hashCode();
    int i;
    int j;
    if (this.isEnable)
    {
      i = 10000000;
      if (this.value != null) {
        break label51;
      }
      j = 0;
      label30:
      if (this.value2 != null) {
        break label62;
      }
    }
    for (;;)
    {
      return j + (m + i) + k;
      i = 0;
      break;
      label51:
      j = this.value.hashCode();
      break label30;
      label62:
      k = this.value2.hashCode() + this.itemList.size();
    }
  }
}


