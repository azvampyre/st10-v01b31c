package com.yuneec.IPCameraManager.cgo4;

public class MenuSettingsItem
{
  public final String id;
  public final boolean isEnable;
  public String option = null;
  public String option2 = null;
  
  public MenuSettingsItem(String paramString, boolean paramBoolean)
  {
    this.id = paramString;
    this.isEnable = paramBoolean;
  }
  
  public MenuSettingsItem(String paramString1, boolean paramBoolean, String paramString2)
  {
    this(paramString1, paramBoolean);
  }
  
  public MenuSettingsItem(String paramString1, boolean paramBoolean, String paramString2, String paramString3)
  {
    this(paramString1, paramBoolean, paramString2);
    this.option2 = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (!(paramObject instanceof MenuSettingsItem)) {
      bool = false;
    }
    for (;;)
    {
      return bool;
      if (!this.id.equals(((MenuSettingsItem)paramObject).id)) {
        bool = false;
      } else if (this.isEnable != ((MenuSettingsItem)paramObject).isEnable) {
        bool = false;
      } else {
        bool = true;
      }
    }
  }
  
  public int hashCode()
  {
    int j = 0;
    int k = 0;
    int m = this.id.hashCode();
    if (this.isEnable) {}
    for (int i = 10000000; this.option == null; i = 0)
    {
      j = k;
      return i + m + j;
    }
    k = this.option.hashCode();
    if (this.option2 == null) {}
    for (;;)
    {
      j += k;
      break;
      j = this.option2.hashCode();
    }
  }
}


