package com.yuneec.IPCameraManager.cgo4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class MenuSettingsNode
{
  public final HashMap<String, MenuSettingsItemContainer> containers;
  public final List<MenuSettingsItem> items;
  public final String nodeName;
  
  public MenuSettingsNode(String paramString)
  {
    this.nodeName = paramString;
    this.items = new ArrayList();
    this.containers = new HashMap();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (!(paramObject instanceof MenuSettingsNode)) {
      bool = false;
    }
    Object localObject2;
    for (;;)
    {
      return bool;
      if (!this.nodeName.equals(((MenuSettingsNode)paramObject).nodeName))
      {
        bool = false;
      }
      else
      {
        localObject2 = ((MenuSettingsNode)paramObject).items;
        if (this.items.size() == ((List)localObject2).size()) {
          break;
        }
        bool = false;
      }
    }
    int i = 0;
    Object localObject1 = this.items.iterator();
    for (;;)
    {
      if (!((Iterator)localObject1).hasNext())
      {
        localObject1 = ((MenuSettingsNode)paramObject).containers;
        paramObject = this.containers.entrySet().iterator();
      }
      for (;;)
      {
        if (!((Iterator)paramObject).hasNext())
        {
          bool = true;
          break;
          if (((MenuSettingsItem)((Iterator)localObject1).next()).equals(((List)localObject2).get(i))) {
            break label219;
          }
          bool = false;
          break;
        }
        localObject2 = (Map.Entry)((Iterator)paramObject).next();
        MenuSettingsItemContainer localMenuSettingsItemContainer = (MenuSettingsItemContainer)((HashMap)localObject1).get(((Map.Entry)localObject2).getKey());
        if ((localMenuSettingsItemContainer == null) || (!localMenuSettingsItemContainer.equals((MenuSettingsItemContainer)((Map.Entry)localObject2).getValue())))
        {
          bool = false;
          break;
        }
        ((Iterator)paramObject).remove();
      }
      label219:
      i++;
    }
  }
  
  public int hashCode()
  {
    return this.nodeName.hashCode() + this.items.size() + this.containers.size();
  }
}


