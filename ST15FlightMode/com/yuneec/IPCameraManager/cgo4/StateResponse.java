package com.yuneec.IPCameraManager.cgo4;

public class StateResponse
  extends ResponseResult
{
  public double batteryCapacity;
  public boolean isRecording;
  public boolean isSdcardInserted;
  public int phtotoCapacity;
  public int recordedTime;
  public int videoCapacity;
  
  public StateResponse(boolean paramBoolean1, boolean paramBoolean2, double paramDouble, boolean paramBoolean3)
  {
    super(paramBoolean1);
    this.isSdcardInserted = paramBoolean2;
    this.batteryCapacity = paramDouble;
    this.isRecording = paramBoolean3;
  }
  
  public void copyExceptRecordingFlag(StateResponse paramStateResponse)
  {
    paramStateResponse.isSdcardInserted = this.isSdcardInserted;
    paramStateResponse.batteryCapacity = this.batteryCapacity;
    paramStateResponse.phtotoCapacity = this.phtotoCapacity;
    paramStateResponse.videoCapacity = this.videoCapacity;
    paramStateResponse.recordedTime = this.recordedTime;
  }
}


