package com.yuneec.IPCameraManager.cgo4;

import android.util.Log;

public class LensInformation
{
  private static final double FLOAT_CHANGE = 100000.0D;
  public String avMax;
  public String avMin;
  public String calcType;
  public String elecOperated;
  public String focusLensMax;
  public String focusLensMin;
  public String infiPos;
  public boolean isOk;
  public String lensMounting;
  public String mfAvail;
  public String tvMax;
  public String tvMin;
  
  public LensInformation(String paramString)
  {
    paramString = paramString.split(",");
    if (paramString.length != 12) {
      Log.e("LensInformation", "invalid informations");
    }
    for (;;)
    {
      return;
      this.isOk = paramString[0].equals("ok");
      if (this.isOk)
      {
        this.avMax = parseAVrange(paramString[1]);
        this.avMin = parseAVrange(paramString[2]);
        this.tvMax = parseTVrange(paramString[3]);
        this.tvMin = parseTVrange(paramString[4]);
        this.calcType = paramString[5];
        this.elecOperated = paramString[6];
        this.focusLensMax = paramString[7];
        this.focusLensMin = paramString[8];
        this.mfAvail = paramString[9];
        this.infiPos = paramString[10];
        this.lensMounting = paramString[11];
      }
    }
  }
  
  private String parseAVrange(String paramString)
  {
    Object localObject = null;
    String[] arrayOfString = paramString.split("/");
    paramString = (String)localObject;
    if (arrayOfString.length == 2) {
      paramString = String.format("F%.1f", new Object[] { Double.valueOf(Math.pow(2.0D, Integer.parseInt(arrayOfString[0]) / Integer.parseInt(arrayOfString[1]) / 2.0F)) });
    }
    return paramString;
  }
  
  private String parseTVrange(String paramString)
  {
    Object localObject = null;
    String[] arrayOfString = paramString.split("/");
    paramString = (String)localObject;
    double d;
    if (arrayOfString.length == 2)
    {
      d = Math.pow(2.0D, -(Integer.parseInt(arrayOfString[0]) / Integer.parseInt(arrayOfString[1])));
      if (d < 1.0D) {
        break label72;
      }
    }
    label72:
    for (paramString = String.format("%.1f s", new Object[] { Double.valueOf(d) });; paramString = toFraction(d) + " s") {
      return paramString;
    }
  }
  
  private static String toFraction(double paramDouble)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    double d = paramDouble;
    if (paramDouble < 0.0D)
    {
      localStringBuilder.append('-');
      d = -paramDouble;
    }
    localStringBuilder.append(1).append("/");
    if (d >= 0.25D) {
      localStringBuilder.append(Math.round(10.0D / d) / 10L);
    }
    for (;;)
    {
      return localStringBuilder.toString();
      localStringBuilder.append(Math.round(100000.0D / (d * 100000.0D)));
    }
  }
}


