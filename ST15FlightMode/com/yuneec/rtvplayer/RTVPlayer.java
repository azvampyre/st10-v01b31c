package com.yuneec.rtvplayer;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public abstract class RTVPlayer
{
  public static final int IMAGE_FORMAT_RGB565 = 3;
  public static final int IMAGE_FORMAT_RV32 = 2;
  public static final int IMAGE_FORMAT_YV12 = 1;
  public static final int PLAYER_FFMPEG = 2;
  public static final int PLAYER_VLC = 1;
  protected VideoEventCallback mVideoEventCallback;
  
  public static RTVPlayer getPlayer(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("Unknown RTVPlayer :" + paramInt);
    }
    for (Object localObject = new VLC();; localObject = new FFmpeg()) {
      return (RTVPlayer)localObject;
    }
  }
  
  public abstract boolean canRecord();
  
  public abstract void deinit();
  
  public abstract void init(Context paramContext, int paramInt, boolean paramBoolean);
  
  public abstract boolean isPlaying();
  
  public abstract boolean isRecording();
  
  public abstract boolean play();
  
  public abstract boolean play(String paramString);
  
  public abstract void setSurfaceView(SurfaceView paramSurfaceView);
  
  public void setVideoEventCallback(VideoEventCallback paramVideoEventCallback)
  {
    this.mVideoEventCallback = paramVideoEventCallback;
  }
  
  public abstract void setVideoLocation(String paramString);
  
  public abstract int snapShot(int paramInt1, String paramString, int paramInt2, int paramInt3);
  
  public abstract int startRecord(String paramString);
  
  public abstract void stop();
  
  public abstract int stopRecord();
  
  public static abstract interface VideoEventCallback
  {
    public abstract void onPlayerEncoutneredError();
    
    public abstract void onPlayerEndReached();
    
    public abstract void onPlayerPlayerRecordingFinished();
    
    public abstract void onPlayerPlaying();
    
    public abstract void onPlayerRecordableChanged();
    
    public abstract void onPlayerSnapshotTaken();
    
    public abstract void onPlayerStopped();
    
    public abstract void onPlayerSurfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3);
    
    public abstract void onPlayerSurfaceCreated(SurfaceHolder paramSurfaceHolder);
    
    public abstract void onPlayerSurfaceDestroyed(SurfaceHolder paramSurfaceHolder);
  }
}