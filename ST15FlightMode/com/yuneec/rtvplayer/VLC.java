package com.yuneec.rtvplayer;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import org.videolan.libvlc.EventHandler;
import org.videolan.libvlc.IVideoPlayer;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.LibVlcException;

public class VLC
  extends RTVPlayer
  implements SurfaceHolder.Callback, IVideoPlayer
{
  private static final int SURFACE_SIZE_CHANED = 100;
  private static final String TAG = "RTVPlayer_VLC";
  private Handler eventHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.getData().getInt("event"))
      {
      case 263: 
      case 264: 
      case 267: 
      case 269: 
      case 270: 
      case 271: 
      case 273: 
      default: 
        Log.e("RTVPlayer_VLC", String.format("Event not handled (0x%x)", new Object[] { Integer.valueOf(paramAnonymousMessage.getData().getInt("event")) }));
      case 268: 
      case 274: 
        switch (paramAnonymousMessage.what)
        {
        }
        break;
      }
      for (;;)
      {
        return;
        Log.i("RTVPlayer_VLC", "MediaPlayerPlaying");
        break;
        Log.i("RTVPlayer_VLC", "MediaPlayerPaused");
        break;
        Log.i("RTVPlayer_VLC", "MediaPlayerStopped");
        break;
        if (VLC.this.mVideoEventCallback == null) {
          break;
        }
        VLC.this.mVideoEventCallback.onPlayerEndReached();
        break;
        Log.i("RTVPlayer_VLC", "MediaPlayerEncounteredError");
        if (VLC.this.mVideoEventCallback == null) {
          break;
        }
        VLC.this.mVideoEventCallback.onPlayerEncoutneredError();
        break;
        Log.i("RTVPlayer_VLC", "MediaPlayerSnapshotTaken");
        break;
        Log.i("RTVPlayer_VLC", "MediaPlayerRecordableChanged");
        break;
        Log.i("RTVPlayer_VLC", "MediaPlayerRecordingFinished");
        break;
        int i = VLC.this.mVideoWidth;
        int k = VLC.this.mSurfaceAlign;
        int j = VLC.this.mSurfaceAlign;
        VLC.this.mSurfaceView.getHolder().setFixedSize(i + k & (j ^ 0xFFFFFFFF), VLC.this.mVideoHeight);
      }
    }
  };
  private boolean isSurfaceReady;
  private int mImageFormat;
  private LibVLC mLibVLC;
  private int mSurfaceAlign;
  private SurfaceView mSurfaceView;
  private int mVideoHeight;
  private String mVideoLocation;
  private int mVideoWidth;
  
  public boolean canRecord()
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "canRecord -- Initialize the VLC first");
    }
    for (boolean bool = false;; bool = this.mLibVLC.canRecord()) {
      return bool;
    }
  }
  
  public void deinit()
  {
    EventHandler.getInstance().removeHandler(this.eventHandler);
  }
  
  public void init(Context paramContext, int paramInt, boolean paramBoolean)
  {
    if (this.mLibVLC != null)
    {
      Log.i("RTVPlayer_VLC", "VLC Player has already been prepared");
      return;
    }
    switch (paramInt)
    {
    }
    for (;;)
    {
      try
      {
        Log.i("RTVPlayer_VLC", "Unknown image format, set to default RGB8888");
        str = "RV32";
        this.mImageFormat = 2;
        this.mLibVLC = LibVLC.getInstance();
        this.mLibVLC.setIomx(paramBoolean);
        this.mLibVLC.setSubtitlesEncoding("");
        this.mLibVLC.setTimeStretching(false);
        this.mLibVLC.setChroma(str);
        this.mLibVLC.setVerboseMode(true);
        this.mLibVLC.setAout(-1);
        this.mLibVLC.init(paramContext);
        EventHandler.getInstance().addHandler(this.eventHandler);
      }
      catch (LibVlcException paramContext)
      {
        Log.d("RTVPlayer_VLC", "LibVLC initialisation failed");
      }
      String str = "YV12";
      this.mImageFormat = paramInt;
      continue;
      break;
      str = "RV32";
      this.mImageFormat = paramInt;
    }
  }
  
  public boolean isPlaying()
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "isPlaying -- Initialize the VLC first");
    }
    for (boolean bool = false;; bool = this.mLibVLC.isPlaying()) {
      return bool;
    }
  }
  
  public boolean isRecording()
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "isRecording -- Initialize the VLC first");
    }
    for (boolean bool = false;; bool = this.mLibVLC.isRecording()) {
      return bool;
    }
  }
  
  public boolean play()
  {
    boolean bool = false;
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "play -- Initialize the VLC first");
    }
    for (;;)
    {
      return bool;
      if (this.mLibVLC.isPlaying())
      {
        Log.i("RTVPlayer_VLC", "Player has already been played");
        continue;
      }
      int i;
      if (!this.isSurfaceReady)
      {
        i = 0;
        label49:
        if ((this.isSurfaceReady) || (i >= 10))
        {
          if ((i < 10) || (this.isSurfaceReady)) {
            break label98;
          }
          Log.i("RTVPlayer_VLC", "Surface initializing,pls wait");
        }
      }
      try
      {
        Thread.sleep(100L);
        i++;
        break label49;
        label98:
        if (this.mVideoLocation == null)
        {
          Log.e("RTVPlayer_VLC", "tell me the video location first");
          continue;
        }
        this.mLibVLC.readMedia(this.mVideoLocation);
        bool = true;
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
  }
  
  public boolean play(String paramString)
  {
    this.mVideoLocation = paramString;
    return play();
  }
  
  public void setSurfaceSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Log.d("RTVPlayer_VLC", "setSurfaceSize: " + paramInt1 + ", " + paramInt2 + ", " + paramInt3 + ", " + paramInt4);
    this.mVideoWidth = paramInt1;
    this.mVideoHeight = paramInt2;
    Message localMessage = this.eventHandler.obtainMessage(100);
    this.eventHandler.sendMessage(localMessage);
  }
  
  public void setSurfaceView(SurfaceView paramSurfaceView)
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "Initialize the VLC before set SurfaceView");
    }
    for (;;)
    {
      return;
      if (paramSurfaceView == null)
      {
        Log.e("RTVPlayer_VLC", "no surface view was assgined");
      }
      else
      {
        if (!this.isSurfaceReady) {
          break;
        }
        Log.e("RTVPlayer_VLC", "the surface view is ready for play,destroy it before reassign");
      }
    }
    this.mSurfaceView = paramSurfaceView;
    paramSurfaceView.getHolder().addCallback(this);
    if (this.mImageFormat == 1) {
      paramSurfaceView.getHolder().setFormat(842094169);
    }
    for (int i = ImageFormat.getBitsPerPixel(842094169);; i = paramSurfaceView.bytesPerPixel)
    {
      this.mSurfaceAlign = (16 / i - 1);
      break;
      paramSurfaceView.getHolder().setFormat(2);
      paramSurfaceView = new PixelFormat();
      PixelFormat.getPixelFormatInfo(2, paramSurfaceView);
    }
  }
  
  public void setVideoLocation(String paramString)
  {
    this.mVideoLocation = paramString;
  }
  
  public int snapShot(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "snapShot -- Initialize the VLC first");
    }
    for (paramInt1 = -1;; paramInt1 = this.mLibVLC.snapShot(paramInt1, paramString, paramInt2, paramInt3)) {
      return paramInt1;
    }
  }
  
  public int startRecord(String paramString)
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "startRecord -- Initialize the VLC first");
    }
    for (int i = -1;; i = this.mLibVLC.startRecord(paramString)) {
      return i;
    }
  }
  
  public void stop()
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "stop -- Initialize the VLC first");
    }
    for (;;)
    {
      return;
      if (!this.mLibVLC.isPlaying()) {
        Log.i("RTVPlayer_VLC", "Player has already been stopped");
      } else {
        this.mLibVLC.stop();
      }
    }
  }
  
  public int stopRecord()
  {
    if (this.mLibVLC == null) {
      Log.e("RTVPlayer_VLC", "stopRecod -- Initialize the VLC first");
    }
    for (int i = -1;; i = this.mLibVLC.stopRecord()) {
      return i;
    }
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.mVideoEventCallback != null) {
      this.mVideoEventCallback.onPlayerSurfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    }
    if (paramSurfaceHolder != this.mSurfaceView.getHolder()) {
      Log.w("RTVPlayer_VLC", "surfaceChanged -- the surface view has changed!");
    }
    for (;;)
    {
      return;
      Log.d("RTVPlayer_VLC", "Pixel format is " + paramInt1);
      Log.d("RTVPlayer_VLC", "width and height " + paramInt2 + " " + paramInt3);
      this.isSurfaceReady = true;
      this.mLibVLC.attachSurface(paramSurfaceHolder.getSurface(), this, paramInt2, paramInt3);
    }
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    if (this.mVideoEventCallback != null) {
      this.mVideoEventCallback.onPlayerSurfaceCreated(paramSurfaceHolder);
    }
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    if (this.mVideoEventCallback != null) {
      this.mVideoEventCallback.onPlayerSurfaceDestroyed(paramSurfaceHolder);
    }
    if (paramSurfaceHolder != this.mSurfaceView.getHolder()) {
      Log.w("RTVPlayer_VLC", "surfaceDestroyed -- the surface view has changed!");
    }
    for (;;)
    {
      return;
      this.isSurfaceReady = false;
      this.mLibVLC.detachSurface();
    }
  }
}