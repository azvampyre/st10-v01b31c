package com.yuneec.rtvplayer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import com.appunite.ffmpeg.FFmpegError;
import com.appunite.ffmpeg.FFmpegListener;
import com.appunite.ffmpeg.FFmpegPlayer;
import com.appunite.ffmpeg.FFmpegStreamInfo;
import com.appunite.ffmpeg.NotPlayingException;

public class FFmpeg
  extends RTVPlayer
  implements SurfaceHolder.Callback, FFmpegListener
{
  private static final String RECORD_FILE_EXTENSION = ".avc";
  private static final String TAG = "FFmpeg";
  private FFmpegPlayer mMpegPlayer;
  private boolean mSurfaceCreated = false;
  private String mVideoLocation;
  
  public boolean canRecord()
  {
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "canRecord -- Initialize the ffmpeg first");
    }
    for (boolean bool = false;; bool = isPlaying()) {
      return bool;
    }
  }
  
  public void deinit() {}
  
  public void init(Context paramContext, int paramInt, boolean paramBoolean)
  {
    if (!(paramContext instanceof Activity)) {
      throw new IllegalArgumentException("You should pass a Activity instead of other context");
    }
    this.mMpegPlayer = new FFmpegPlayer(null, (Activity)paramContext);
    this.mMpegPlayer.setMpegListener(this);
  }
  
  public boolean isPlaying()
  {
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "isPlaying -- Initialize the ffmpeg first");
    }
    for (boolean bool = false;; bool = this.mMpegPlayer.isPlaying()) {
      return bool;
    }
  }
  
  public boolean isRecording()
  {
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "isRecording -- Initialize the ffmpeg first");
    }
    for (boolean bool = false;; bool = this.mMpegPlayer.isRecording()) {
      return bool;
    }
  }
  
  public void onFFDataSourceLoaded(FFmpegError paramFFmpegError, FFmpegStreamInfo[] paramArrayOfFFmpegStreamInfo)
  {
    if (paramFFmpegError != null)
    {
      Log.i("FFmpeg", "player encounter error:" + paramFFmpegError.toString());
      if (this.mVideoEventCallback != null) {
        this.mVideoEventCallback.onPlayerEncoutneredError();
      }
    }
    for (;;)
    {
      return;
      Log.i("FFmpeg", "player load source OK:");
      if (this.mVideoEventCallback != null) {
        this.mVideoEventCallback.onPlayerPlaying();
      }
    }
  }
  
  public void onFFPause(NotPlayingException paramNotPlayingException) {}
  
  public void onFFResume(NotPlayingException paramNotPlayingException) {}
  
  public void onFFSeeked(NotPlayingException paramNotPlayingException) {}
  
  public void onFFStop()
  {
    if (this.mVideoEventCallback != null)
    {
      Log.i("FFmpeg", "Stream Stopped");
      this.mVideoEventCallback.onPlayerStopped();
    }
  }
  
  public void onFFUpdateTime(long paramLong1, long paramLong2, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      Log.i("FFmpeg", "Stream End Reached");
      if (this.mVideoEventCallback != null) {
        this.mVideoEventCallback.onPlayerEndReached();
      }
    }
  }
  
  public boolean play()
  {
    boolean bool = false;
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "play -- Initialize the ffmpeg first");
    }
    for (;;)
    {
      return bool;
      if (this.mMpegPlayer.isPlaying())
      {
        Log.i("FFmpeg", "Player has already been played");
      }
      else if (this.mVideoLocation == null)
      {
        Log.e("FFmpeg", "tell me the video location first");
      }
      else
      {
        this.mMpegPlayer.setDataSource(this.mVideoLocation);
        bool = true;
      }
    }
  }
  
  public boolean play(String paramString)
  {
    this.mVideoLocation = paramString;
    return play();
  }
  
  public void setSurfaceView(SurfaceView paramSurfaceView)
  {
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "You should call init method first");
    }
    for (;;)
    {
      return;
      if (paramSurfaceView == null)
      {
        Log.e("FFmpeg", "no surface view was assgined");
      }
      else
      {
        paramSurfaceView = paramSurfaceView.getHolder();
        paramSurfaceView.setFormat(1);
        paramSurfaceView.addCallback(this);
      }
    }
  }
  
  public void setVideoLocation(String paramString)
  {
    this.mVideoLocation = paramString;
  }
  
  public int snapShot(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    return -1;
  }
  
  public int startRecord(String paramString)
  {
    int i = -1;
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "startRecord -- Initialize the ffmpeg first");
    }
    for (;;)
    {
      return i;
      if (paramString == null) {
        Log.e("FFmpeg", "startRecord -- fullpathname null!");
      } else {
        i = this.mMpegPlayer.startRecord(paramString + ".avc");
      }
    }
  }
  
  public void stop()
  {
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "stop -- Initialize the ffmpeg first");
    }
    for (;;)
    {
      return;
      if (!this.mMpegPlayer.isPlaying()) {
        Log.i("FFmpeg", "Player has already been stopped");
      } else {
        this.mMpegPlayer.stop();
      }
    }
  }
  
  public int stopRecord()
  {
    if (this.mMpegPlayer == null) {
      Log.e("FFmpeg", "startRecord -- Initialize the ffmpeg first");
    }
    for (int i = -1;; i = this.mMpegPlayer.stopRecord()) {
      return i;
    }
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.mVideoEventCallback != null) {
      this.mVideoEventCallback.onPlayerSurfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    }
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    if (this.mVideoEventCallback != null) {
      this.mVideoEventCallback.onPlayerSurfaceCreated(paramSurfaceHolder);
    }
    if (this.mSurfaceCreated) {
      surfaceDestroyed(paramSurfaceHolder);
    }
    paramSurfaceHolder = paramSurfaceHolder.getSurface();
    this.mMpegPlayer.render(paramSurfaceHolder);
    this.mSurfaceCreated = true;
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    if (this.mVideoEventCallback != null) {
      this.mVideoEventCallback.onPlayerSurfaceDestroyed(paramSurfaceHolder);
    }
    this.mMpegPlayer.renderFrameStop();
    this.mSurfaceCreated = false;
  }
}