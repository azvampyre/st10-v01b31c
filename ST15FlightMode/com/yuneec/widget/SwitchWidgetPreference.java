package com.yuneec.widget;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class SwitchWidgetPreference
  extends Preference
{
  private boolean isOnStates = false;
  private LinearLayout offStatesLayout;
  private LinearLayout onStatesLayout;
  
  public SwitchWidgetPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean getIsOnStates()
  {
    return this.isOnStates;
  }
  
  protected void onBindView(View paramView)
  {
    super.onBindView(paramView);
    this.onStatesLayout = ((LinearLayout)paramView.findViewById(2131689833));
    this.offStatesLayout = ((LinearLayout)paramView.findViewById(2131689832));
    setWidgetStates(this.isOnStates);
  }
  
  public void setWidgetStates(boolean paramBoolean)
  {
    this.isOnStates = paramBoolean;
    if ((this.onStatesLayout == null) || (this.offStatesLayout == null)) {}
    for (;;)
    {
      return;
      if (paramBoolean)
      {
        this.onStatesLayout.setVisibility(0);
        this.offStatesLayout.setVisibility(8);
      }
      else
      {
        this.onStatesLayout.setVisibility(8);
        this.offStatesLayout.setVisibility(0);
      }
    }
  }
}


