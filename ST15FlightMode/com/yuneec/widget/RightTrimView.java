package com.yuneec.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

public class RightTrimView
  extends View
{
  private Bitmap bitmap_bg = null;
  private Bitmap bitmap_h = null;
  private Bitmap bitmap_v = null;
  private float hX;
  private float hY;
  private int mH_value;
  private Paint mPaint = new Paint();
  private Paint mPaintText = new Paint();
  private int mV_value;
  private int startX;
  private int startY;
  private float vX;
  private float vY;
  
  public RightTrimView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public RightTrimView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public RightTrimView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.bitmap_bg = BitmapFactory.decodeResource(getResources(), 2130837784);
    this.bitmap_h = BitmapFactory.decodeResource(getResources(), 2130837625);
    this.bitmap_v = BitmapFactory.decodeResource(getResources(), 2130837821);
    this.startX = this.bitmap_v.getWidth();
    this.startY = this.bitmap_v.getHeight();
  }
  
  public void drawBackground(Canvas paramCanvas)
  {
    this.mPaint.setAntiAlias(true);
    paramCanvas.drawBitmap(this.bitmap_bg, this.startX, this.startY, this.mPaint);
  }
  
  public void drawCursor(Canvas paramCanvas)
  {
    this.mPaint.setAntiAlias(true);
    this.vX = 3.0F;
    this.vY = (this.startY + 60 - this.mV_value * 2.7F - (this.bitmap_v.getHeight() + 1 >> 1));
    paramCanvas.drawBitmap(this.bitmap_v, this.vX, this.vY, this.mPaint);
    this.mPaint.setAntiAlias(true);
    this.hX = (this.startX + this.mH_value * 2.7F + 99.0F - (this.bitmap_h.getWidth() - 1 >> 1));
    this.hY = (this.startY + 126);
    paramCanvas.drawBitmap(this.bitmap_h, this.hX, this.hY, this.mPaint);
    this.mPaintText.setAntiAlias(true);
    this.mPaintText.setTextAlign(Paint.Align.CENTER);
    this.mPaintText.setColor(-1);
    this.mPaintText.setTextSize(14.0F);
    this.mPaintText.setTypeface(Typeface.DEFAULT);
    this.mPaintText.setTextAlign(Paint.Align.LEFT);
    paramCanvas.drawText(String.valueOf(this.mV_value), this.vX + 3.0F, this.vY + 20.0F, this.mPaintText);
    this.mPaintText.setTextAlign(Paint.Align.CENTER);
    paramCanvas.drawText(String.valueOf(this.mH_value), this.hX + 16.0F, this.hY + 24.0F, this.mPaintText);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    drawBackground(paramCanvas);
    drawCursor(paramCanvas);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(this.bitmap_bg.getWidth() + this.bitmap_h.getWidth() + this.bitmap_v.getWidth(), this.bitmap_bg.getHeight() + this.bitmap_h.getHeight() + this.bitmap_v.getHeight());
  }
  
  public void setValue(int paramInt1, int paramInt2)
  {
    if ((this.mV_value != paramInt1) || (this.mH_value != paramInt2))
    {
      this.mV_value = paramInt1;
      this.mH_value = paramInt2;
      invalidate();
    }
  }
}


