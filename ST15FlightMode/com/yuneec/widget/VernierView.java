package com.yuneec.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

public class VernierView
  extends View
{
  private static final int CURSOR_X0 = 23;
  private static final int CURSOR_Y0 = 73;
  private static final int VERNIER_X = 0;
  private static final int VERNIER_Y = 0;
  private Cursor mCursor = new Cursor();
  private Paint mPaint = new Paint();
  
  public VernierView(Context paramContext)
  {
    super(paramContext);
  }
  
  public VernierView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public VernierView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void drawBackground(Canvas paramCanvas)
  {
    Bitmap localBitmap = BitmapFactory.decodeResource(getResources(), 2130837646);
    this.mPaint.setAntiAlias(true);
    paramCanvas.drawBitmap(localBitmap, 0.0F, 0.0F, this.mPaint);
  }
  
  private void drawCursor(Canvas paramCanvas)
  {
    ClipDrawable localClipDrawable1 = (ClipDrawable)getResources().getDrawable(2130837640);
    ClipDrawable localClipDrawable2 = (ClipDrawable)getResources().getDrawable(2130837639);
    Drawable localDrawable = getResources().getDrawable(2130837638);
    this.mPaint.setAntiAlias(true);
    this.mCursor.x = 23.0F;
    this.mCursor.y = ((float)(73.0D - this.mCursor.value * 0.45D));
    float f1 = this.mCursor.x;
    float f3 = this.mCursor.y;
    float f2 = localDrawable.getIntrinsicHeight() / 2;
    int i = (int)(this.mCursor.value * 0.45D * 200.0D);
    if (i > 0)
    {
      localClipDrawable1.setLevel(i);
      localClipDrawable2.setLevel(0);
    }
    for (;;)
    {
      localClipDrawable1.setBounds(23, 73 - localClipDrawable1.getIntrinsicHeight(), localClipDrawable1.getIntrinsicWidth() + 23, 73);
      localClipDrawable1.draw(paramCanvas);
      localClipDrawable2.setBounds(23, 74, localClipDrawable2.getIntrinsicWidth() + 23, localClipDrawable2.getIntrinsicHeight() + 73 + 1);
      localClipDrawable2.draw(paramCanvas);
      paramCanvas.drawBitmap(BitmapFactory.decodeResource(getResources(), 2130837638), f1 - 1.0F, f3 - f2, this.mPaint);
      return;
      if (i < 0)
      {
        localClipDrawable1.setLevel(0);
        localClipDrawable2.setLevel(Math.abs(i));
      }
      else
      {
        localClipDrawable1.setLevel(0);
        localClipDrawable2.setLevel(0);
      }
    }
  }
  
  public int getValue()
  {
    return this.mCursor.value;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    drawBackground(paramCanvas);
    drawCursor(paramCanvas);
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
  }
  
  public void setValue(int paramInt)
  {
    if (this.mCursor.value != paramInt)
    {
      this.mCursor.value = paramInt;
      invalidate();
    }
  }
  
  class Cursor
  {
    int value;
    float x;
    float y;
    
    Cursor() {}
  }
}


