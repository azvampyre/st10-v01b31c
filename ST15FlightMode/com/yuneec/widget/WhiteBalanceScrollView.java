package com.yuneec.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;

public class WhiteBalanceScrollView
  extends ScrollView
{
  private static final String TAG = "WhiteBalanceScrollView";
  private static String[] aa = { "Auto", "Lock", "Sunny", "Cloudy", "Fluorescent", "Incandescent", "Sunset/Sunrise" };
  private static int[] draw = { 2130837512, 2130837660, 2130837576, 2130837560, 2130837590, 2130837651, 2130837810 };
  private int[] itemIds = { 2131690023, 2131690024, 2131690025, 2131690026, 2131690027, 2131690028, 2131690029 };
  public OnItemSelectedListener mItemSelectedListener;
  private View.OnClickListener mWBitemOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      int i = 0;
      if (i >= WhiteBalanceScrollView.this.itemIds.length) {
        return;
      }
      if (paramAnonymousView.equals(WhiteBalanceScrollView.this.mWBitems[i]))
      {
        WhiteBalanceScrollView.this.mWBitems[i].setSelected(true);
        WhiteBalanceScrollView.this.mItemSelectedListener.onItemSelected(i);
      }
      for (;;)
      {
        i++;
        break;
        WhiteBalanceScrollView.this.mWBitems[i].setSelected(false);
      }
    }
  };
  private WhiteBalanceListItem[] mWBitems = new WhiteBalanceListItem[this.itemIds.length];
  
  public WhiteBalanceScrollView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public WhiteBalanceScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public WhiteBalanceScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903121, this);
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    for (int i = 0;; i++)
    {
      if (i >= this.mWBitems.length) {
        return;
      }
      this.mWBitems[i] = ((WhiteBalanceListItem)findViewById(this.itemIds[i]));
      this.mWBitems[i].setImage(getResources().getDrawable(draw[i]));
      this.mWBitems[i].setText(aa[i]);
      this.mWBitems[i].setOnClickListener(this.mWBitemOnClickListener);
    }
  }
  
  public void setItemSelect(int paramInt)
  {
    int i = 0;
    if (i >= this.itemIds.length) {
      return;
    }
    if (paramInt == i) {
      this.mWBitems[i].setSelected(true);
    }
    for (;;)
    {
      i++;
      break;
      this.mWBitems[i].setSelected(false);
    }
  }
  
  public void setOnItemSelectedListener(OnItemSelectedListener paramOnItemSelectedListener)
  {
    this.mItemSelectedListener = paramOnItemSelectedListener;
  }
  
  public static abstract interface OnItemSelectedListener
  {
    public abstract void onItemSelected(int paramInt);
  }
}


