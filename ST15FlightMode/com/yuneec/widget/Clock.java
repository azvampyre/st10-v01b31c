package com.yuneec.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class Clock
  extends TextView
{
  private static final int AM_PM_STYLE = 1;
  private static final int AM_PM_STYLE_GONE = 2;
  private static final int AM_PM_STYLE_NORMAL = 0;
  private static final int AM_PM_STYLE_SMALL = 1;
  private static final String FORMAT_12 = "h:mm:ss a";
  private static final String FORMAT_24 = "HH:mm:ss";
  private static final String TAG = "Clock";
  private boolean mAttached;
  private Calendar mCalendar;
  private SimpleDateFormat mClockFormat;
  private String mClockFormatString;
  private Handler mHandler;
  private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      if (paramAnonymousIntent.getAction().equals("android.intent.action.TIMEZONE_CHANGED"))
      {
        paramAnonymousContext = paramAnonymousIntent.getStringExtra("time-zone");
        Clock.this.mCalendar = Calendar.getInstance(TimeZone.getTimeZone(paramAnonymousContext));
        if (Clock.this.mClockFormat != null) {
          Clock.this.mClockFormat.setTimeZone(Clock.this.mCalendar.getTimeZone());
        }
      }
      if (Clock.this.mHandler != null) {
        Clock.this.mHandler.removeCallbacks(Clock.this.mTicker);
      }
      if (Clock.this.mTicker != null) {
        Clock.this.mTicker.run();
      }
    }
  };
  private Runnable mTicker;
  
  public Clock(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public Clock(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public Clock(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private final CharSequence getSmallTime()
  {
    Object localObject1;
    int k;
    int i;
    label33:
    int j;
    if (DateFormat.is24HourFormat(getContext()))
    {
      localObject1 = "HH:mm:ss";
      if (((String)localObject1).equals(this.mClockFormatString)) {
        break label308;
      }
      int m = -1;
      k = 0;
      i = 0;
      if (i < ((String)localObject1).length()) {
        break label252;
      }
      i = m;
      Object localObject2 = localObject1;
      if (i >= 0)
      {
        j = i;
        label55:
        if ((j > 0) && (Character.isWhitespace(((String)localObject1).charAt(j - 1)))) {
          break label302;
        }
        localObject2 = ((String)localObject1).substring(0, j) + 61184 + ((String)localObject1).substring(j, i) + "a" + 61185 + ((String)localObject1).substring(i + 1);
      }
      localObject1 = new SimpleDateFormat((String)localObject2);
      this.mClockFormat = ((SimpleDateFormat)localObject1);
      this.mClockFormatString = ((String)localObject2);
      label154:
      localObject1 = ((SimpleDateFormat)localObject1).format(this.mCalendar.getTime());
      i = ((String)localObject1).indexOf(61184);
      j = ((String)localObject1).indexOf(61185);
      if ((i < 0) || (j <= i)) {
        break label317;
      }
      localObject1 = new SpannableStringBuilder((CharSequence)localObject1);
      ((SpannableStringBuilder)localObject1).setSpan(new RelativeSizeSpan(0.7F), i, j, 34);
      ((SpannableStringBuilder)localObject1).delete(j, j + 1);
      ((SpannableStringBuilder)localObject1).delete(i, i + 1);
    }
    label252:
    label289:
    label294:
    label302:
    label308:
    label317:
    for (;;)
    {
      return (CharSequence)localObject1;
      localObject1 = "h:mm:ss a";
      break;
      int n = ((String)localObject1).charAt(i);
      j = k;
      if (n == 39) {
        if (k == 0) {
          break label289;
        }
      }
      for (j = 0;; j = 1)
      {
        if ((j != 0) || (n != 97)) {
          break label294;
        }
        break;
      }
      i++;
      k = j;
      break label33;
      j--;
      break label55;
      localObject1 = this.mClockFormat;
      break label154;
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (!this.mAttached)
    {
      this.mAttached = true;
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("android.intent.action.TIME_TICK");
      localIntentFilter.addAction("android.intent.action.TIME_SET");
      localIntentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
      localIntentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
      getContext().registerReceiver(this.mIntentReceiver, localIntentFilter, null, getHandler());
    }
    this.mCalendar = Calendar.getInstance(TimeZone.getDefault());
    this.mHandler = new Handler();
    this.mTicker = new Runnable()
    {
      public void run()
      {
        if (!Clock.this.mAttached) {}
        for (;;)
        {
          return;
          Clock.this.updateClock();
          long l = SystemClock.uptimeMillis();
          Clock.this.mHandler.postAtTime(Clock.this.mTicker, l + (1000L - l % 1000L));
        }
      }
    };
    this.mTicker.run();
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.mAttached)
    {
      getContext().unregisterReceiver(this.mIntentReceiver);
      this.mAttached = false;
    }
    if (this.mHandler != null) {
      this.mHandler.removeCallbacks(this.mTicker);
    }
    this.mTicker = null;
    this.mHandler = null;
  }
  
  final void updateClock()
  {
    this.mCalendar.setTimeInMillis(System.currentTimeMillis());
    setText(getSmallTime());
  }
}


