package com.yuneec.widget;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.LocationManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import com.yuneec.database.DataProvider;
import com.yuneec.database.DataProviderHelper;
import java.util.Iterator;

public class StatusbarView
  extends RelativeLayout
{
  private static final boolean DEBUG = false;
  private static final String TAG = "StatusbarView";
  private ImageView mBatteryImage;
  private BroadcastReceiver mBatteryReceiveer = new BroadcastReceiver()
  {
    int last_level;
    int plugged = -1;
    
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      if (paramAnonymousIntent.getAction().equals("android.intent.action.BATTERY_CHANGED"))
      {
        int i = paramAnonymousIntent.getIntExtra("plugged", -1);
        int j = paramAnonymousIntent.getIntExtra("level", -1);
        if (j != -1)
        {
          if (this.plugged != i)
          {
            this.plugged = i;
            if (i == 0) {
              break label120;
            }
            StatusbarView.this.mBatteryImage.setImageResource(2130837791);
          }
          if (i == 0) {
            StatusbarView.this.mBatteryImage.setImageLevel(j);
          }
          StatusbarView.this.mBatteryText.setText(String.valueOf(j) + "%");
          this.last_level = j;
        }
      }
      for (;;)
      {
        return;
        label120:
        StatusbarView.this.mBatteryImage.setImageResource(2130837790);
        break;
        if (paramAnonymousIntent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED"))
        {
          StatusbarView.this.mBatteryImage.setImageResource(2130837790);
          StatusbarView.this.mBatteryImage.setImageLevel(this.last_level);
          StatusbarView.this.mBatteryText.setText(String.valueOf(this.last_level) + "%");
        }
        else if (paramAnonymousIntent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED"))
        {
          StatusbarView.this.mBatteryImage.setImageResource(2130837791);
        }
      }
    }
  };
  private TextView mBatteryText;
  private ViewAnimator mCenterText;
  private TextView mCenterText_0;
  private TextView mCenterText_1;
  private TextView mGpsIndictor;
  private GpsStatus.Listener mGpsStatusListener = new GpsStatus.Listener()
  {
    public void onGpsStatusChanged(int paramAnonymousInt)
    {
      Object localObject = StatusbarView.this.mLM.getGpsStatus(null);
      int j = -1;
      int i = j;
      if (localObject != null)
      {
        i = j;
        if (paramAnonymousInt == 4)
        {
          paramAnonymousInt = 0;
          localObject = ((GpsStatus)localObject).getSatellites().iterator();
        }
      }
      label124:
      label137:
      for (;;)
      {
        if (!((Iterator)localObject).hasNext())
        {
          i = paramAnonymousInt;
          if (i < 0) {
            break label124;
          }
          StatusbarView.this.mGpsIndictor.setVisibility(0);
          StatusbarView.this.mGpsIndictor.setText(String.valueOf(i));
          StatusbarView.this.mGpsIndictor.getCompoundDrawables()[2].setLevel(i);
        }
        for (;;)
        {
          return;
          if (!((GpsSatellite)((Iterator)localObject).next()).usedInFix()) {
            break label137;
          }
          paramAnonymousInt++;
          break;
          StatusbarView.this.mGpsIndictor.setVisibility(8);
        }
      }
    }
  };
  private Handler mHandler = new Handler();
  private boolean mIsCenterText1 = false;
  private LocationManager mLM;
  private TextView mLeftText0;
  private TextView mLeftText1;
  private String mOwner = "Pilot";
  private ImageView mWifiIndictor;
  private WifiManager mWifiManager;
  private BroadcastReceiver mWifiReceiver = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      paramAnonymousContext = paramAnonymousIntent.getAction();
      int i;
      if (paramAnonymousContext.equals("android.net.wifi.RSSI_CHANGED"))
      {
        i = WifiManager.calculateSignalLevel(paramAnonymousIntent.getIntExtra("newRssi", 65336), 100);
        StatusbarView.this.mWifiIndictor.setImageLevel(i);
      }
      for (;;)
      {
        return;
        if (paramAnonymousContext.equals("android.net.wifi.WIFI_STATE_CHANGED"))
        {
          if (paramAnonymousIntent.getIntExtra("wifi_state", 4) != 3)
          {
            StatusbarView.this.mWifiIndictor.setVisibility(8);
            StatusbarView.this.mWifiIndictor.setImageLevel(0);
          }
        }
        else if (paramAnonymousContext.equals("android.net.wifi.STATE_CHANGE"))
        {
          paramAnonymousContext = ((NetworkInfo)paramAnonymousIntent.getParcelableExtra("networkInfo")).getDetailedState();
          if (NetworkInfo.DetailedState.CONNECTED == paramAnonymousContext)
          {
            StatusbarView.this.mWifiIndictor.setVisibility(0);
            i = WifiManager.calculateSignalLevel(StatusbarView.this.mWifiManager.getConnectionInfo().getRssi(), 100);
            StatusbarView.this.mWifiIndictor.setImageLevel(i);
          }
          else if ((NetworkInfo.DetailedState.DISCONNECTING == paramAnonymousContext) || (NetworkInfo.DetailedState.DISCONNECTED == paramAnonymousContext) || (NetworkInfo.DetailedState.SCANNING == paramAnonymousContext))
          {
            StatusbarView.this.mWifiIndictor.setImageLevel(0);
            StatusbarView.this.mWifiIndictor.setVisibility(8);
          }
        }
      }
    }
  };
  private Runnable showOwnerRunnable = new Runnable()
  {
    public void run()
    {
      StatusbarView.this.setInfoTextInternal(StatusbarView.this.getResources().getString(2131296309, new Object[] { StatusbarView.this.mOwner }), -1, false);
    }
  };
  
  public StatusbarView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public StatusbarView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public StatusbarView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903106, this, true);
    setPadding(4, 0, 4, 0);
    this.mWifiManager = ((WifiManager)paramContext.getSystemService("wifi"));
    this.mLM = ((LocationManager)paramContext.getSystemService("location"));
  }
  
  private void initLeftText(Context paramContext)
  {
    long l = paramContext.getSharedPreferences("flight_setting_value", 0).getLong("current_model_id", -2L);
    if (l == -2L) {
      paramContext = paramContext.getString(2131296532);
    }
    for (;;)
    {
      setLeftText(0, paramContext);
      Cursor localCursor;
      for (;;)
      {
        return;
        localCursor = paramContext.getContentResolver().query(ContentUris.withAppendedId(DataProvider.MODEL_URI, l), new String[] { "name" }, null, null, null);
        if (DataProviderHelper.isCursorValid(localCursor)) {
          break;
        }
        Log.e("StatusbarView", "Get date from MODEL_URI, Cursor is invalid");
      }
      String str = localCursor.getString(localCursor.getColumnIndex("name"));
      localCursor.close();
      if (str != null) {
        paramContext = paramContext.getString(2131296534) + str;
      } else {
        paramContext = paramContext.getString(2131296532);
      }
    }
  }
  
  private void initStatIcon() {}
  
  private void setInfoTextInternal(String paramString, int paramInt, boolean paramBoolean)
  {
    this.mHandler.removeCallbacks(this.showOwnerRunnable);
    TextView localTextView;
    int i;
    boolean bool;
    if (this.mIsCenterText1)
    {
      localTextView = this.mCenterText_0;
      i = 0;
      if (!this.mIsCenterText1) {
        break label100;
      }
      bool = false;
      label37:
      this.mIsCenterText1 = bool;
      localTextView.setText(paramString);
      if (paramInt == 0) {
        break label106;
      }
      localTextView.setTextColor(paramInt);
    }
    for (;;)
    {
      this.mCenterText.setDisplayedChild(i);
      if (paramBoolean) {
        this.mHandler.postDelayed(this.showOwnerRunnable, 10000L);
      }
      return;
      localTextView = this.mCenterText_1;
      i = 1;
      break;
      label100:
      bool = true;
      break label37;
      label106:
      localTextView.setTextColor(-1);
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    IntentFilter localIntentFilter = new IntentFilter("android.intent.action.BATTERY_CHANGED");
    localIntentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
    localIntentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
    getContext().registerReceiver(this.mBatteryReceiveer, localIntentFilter);
    localIntentFilter = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED");
    localIntentFilter.addAction("android.net.wifi.RSSI_CHANGED");
    localIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
    getContext().registerReceiver(this.mWifiReceiver, localIntentFilter);
    this.mLM.addGpsStatusListener(this.mGpsStatusListener);
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    getContext().unregisterReceiver(this.mBatteryReceiveer);
    getContext().unregisterReceiver(this.mWifiReceiver);
    this.mHandler.removeCallbacks(this.showOwnerRunnable);
    if (this.mIsCenterText1) {}
    for (TextView localTextView = this.mCenterText_1;; localTextView = this.mCenterText_0)
    {
      localTextView.setText(getResources().getString(2131296309, new Object[] { this.mOwner }));
      localTextView.setTextColor(-1);
      this.mLM.removeGpsStatusListener(this.mGpsStatusListener);
      return;
    }
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mLeftText0 = ((TextView)findViewById(2131689935));
    this.mLeftText1 = ((TextView)findViewById(2131689936));
    this.mBatteryImage = ((ImageView)findViewById(2131689945));
    this.mBatteryText = ((TextView)findViewById(2131689944));
    this.mCenterText = ((ViewAnimator)findViewById(2131689937));
    this.mCenterText_0 = ((TextView)findViewById(2131689939));
    this.mCenterText_1 = ((TextView)findViewById(2131689940));
    this.mWifiIndictor = ((ImageView)findViewById(2131689943));
    this.mGpsIndictor = ((TextView)findViewById(2131689942));
    String str = getResources().getString(2131296309, new Object[] { this.mOwner });
    this.mCenterText_0.setText(str);
    this.mCenterText.setInAnimation(getContext(), 2130968583);
    this.mCenterText.setOutAnimation(getContext(), 2130968584);
    initLeftText(getContext());
    initStatIcon();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(View.MeasureSpec.makeMeasureSpec(getResources().getDisplayMetrics().widthPixels, View.MeasureSpec.getMode(paramInt1)), View.MeasureSpec.makeMeasureSpec(getResources().getDimensionPixelSize(2131558406), View.MeasureSpec.getMode(paramInt2)));
  }
  
  public void setInfoText(String paramString, int paramInt)
  {
    setInfoTextInternal(paramString, paramInt, true);
  }
  
  public void setLeftText(int paramInt, String paramString)
  {
    setLeftTextVisibility(paramInt, true);
    switch (paramInt)
    {
    }
    for (;;)
    {
      return;
      this.mLeftText0.setVisibility(0);
      this.mLeftText0.setText(paramString);
      continue;
      this.mLeftText1.setVisibility(0);
      this.mLeftText1.setText(paramString);
    }
  }
  
  public void setLeftTextVisibility(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    }
    for (;;)
    {
      return;
      if (paramBoolean)
      {
        this.mLeftText0.setVisibility(0);
      }
      else
      {
        this.mLeftText0.setVisibility(8);
        continue;
        if (paramBoolean) {
          this.mLeftText1.setVisibility(0);
        } else {
          this.mLeftText1.setVisibility(8);
        }
      }
    }
  }
  
  public void setmOwner(String paramString)
  {
    this.mOwner = paramString;
  }
}


