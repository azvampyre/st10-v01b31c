package com.yuneec.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.drawable.Drawable;
import android.media.SoundPool;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import com.yuneec.flightmode15.R.styleable;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class StyledNumberPicker
  extends LinearLayout
{
  private static final int DEFAULT_LAYOUT_RESOURCE_ID = 2130903090;
  private static final long DEFAULT_LONG_PRESS_UPDATE_INTERVAL = 300L;
  private static final char[] DIGIT_CHARACTERS = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45 };
  private static final int SELECTOR_ADJUSTMENT_DURATION_MILLIS = 800;
  private static final int SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT = 8;
  private static final int SELECTOR_MIDDLE_ITEM_INDEX = 1;
  private static final int SELECTOR_WHEEL_ITEM_COUNT = 3;
  private static final int SIZE_UNSPECIFIED = -1;
  private static final int SNAP_SCROLL_DURATION = 300;
  private static final String TAG = "StyledNumberPicker";
  private static final float TOP_AND_BOTTOM_FADING_EDGE_STRENGTH = 0.9F;
  public static final Formatter TWO_DIGIT_FORMATTER = new Formatter()
  {
    final Object[] mArgs = new Object[1];
    final StringBuilder mBuilder = new StringBuilder();
    final Formatter mFmt = new Formatter(this.mBuilder, Locale.US);
    
    public String format(int paramAnonymousInt)
    {
      this.mArgs[0] = Integer.valueOf(paramAnonymousInt);
      this.mBuilder.delete(0, this.mBuilder.length());
      this.mFmt.format("%02d", this.mArgs);
      return this.mFmt.toString();
    }
  };
  private static final int UNSCALED_DEFAULT_SELECTION_DIVIDERS_DISTANCE = 48;
  private static final int UNSCALED_DEFAULT_SELECTION_DIVIDER_HEIGHT = 2;
  private AccessibilityNodeProviderImpl mAccessibilityNodeProvider;
  private final Scroller mAdjustScroller;
  private BeginSoftInputOnLongPressCommand mBeginSoftInputOnLongPressCommand;
  private int mBottomSelectionDividerBottom;
  public OnButtonClickedListener mButtonClickedListener;
  private ChangeCurrentByOneFromLongPressCommand mChangeCurrentByOneFromLongPressCommand;
  private final boolean mComputeMaxWidth;
  private int mCurrentScrollOffset;
  private final Paint mCurrentWheelPaint;
  private final ImageButton mDecrementButton;
  private boolean mDecrementVirtualButtonPressed;
  private int mDidaSoundId;
  private boolean mDisableKeyboard;
  private String[] mDisplayedValues;
  private final Scroller mFlingScroller;
  private Formatter mFormatter;
  private final boolean mHasSelectorWheel;
  private final ImageButton mIncrementButton;
  private boolean mIncrementVirtualButtonPressed;
  private boolean mIngonreMoveEvents;
  private int mInitialScrollOffset = Integer.MIN_VALUE;
  private final EditText mInputText;
  private long mLastDownEventTime;
  private float mLastDownEventY;
  private float mLastDownOrMoveEventY;
  private long mLongPressUpdateInterval = 300L;
  private final int mMaxHeight;
  private int mMaxValue;
  private int mMaxWidth;
  private int mMaximumFlingVelocity;
  private final int mMinHeight;
  private int mMinValue;
  private final int mMinWidth;
  private int mMinimumFlingVelocity;
  private OnScrollListener mOnScrollListener;
  private OnValueChangeListener mOnValueChangeListener;
  private final PressedStateHelper mPressedStateHelper;
  private int mPreviousScrollerY;
  private int mScrollState = 0;
  private final Drawable mSelectionDivider;
  private final int mSelectionDividerHeight;
  private final int mSelectionDividersDistance;
  private int mSelectorElementHeight;
  private final SparseArray<String> mSelectorIndexToStringCache = new SparseArray();
  private final int[] mSelectorIndices = new int[3];
  private int mSelectorTextGapHeight;
  private final Paint mSelectorWheelPaint;
  private SetSelectionCommand mSetSelectionCommand;
  private boolean mShowSoftInputOnTap;
  private final int mSolidColor;
  private SoundPool mSoundPool;
  private int mTextGapPadding;
  private final int mTextSize;
  private int mTopSelectionDividerTop;
  private int mTouchSlop;
  private int mValue;
  private VelocityTracker mVelocityTracker;
  private final Drawable mVirtualButtonPressedDrawable;
  private boolean mWrapSelectorWheel;
  private boolean mWrapSelectorWheelValid;
  
  public StyledNumberPicker(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public StyledNumberPicker(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public StyledNumberPicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.NumberPicker, paramInt, 0);
    int i = paramAttributeSet.getResourceId(8, 2130903090);
    if (i != 2130903090) {}
    for (boolean bool = true;; bool = false)
    {
      this.mHasSelectorWheel = bool;
      this.mSolidColor = paramAttributeSet.getColor(0, 0);
      this.mSelectionDivider = paramAttributeSet.getDrawable(1);
      this.mSelectionDividerHeight = paramAttributeSet.getDimensionPixelSize(2, (int)TypedValue.applyDimension(1, 2.0F, getResources().getDisplayMetrics()));
      this.mSelectionDividersDistance = paramAttributeSet.getDimensionPixelSize(3, (int)TypedValue.applyDimension(1, 48.0F, getResources().getDisplayMetrics()));
      this.mMinHeight = paramAttributeSet.getDimensionPixelSize(4, -1);
      this.mMaxHeight = paramAttributeSet.getDimensionPixelSize(5, -1);
      if ((this.mMinHeight == -1) || (this.mMaxHeight == -1) || (this.mMinHeight <= this.mMaxHeight)) {
        break;
      }
      throw new IllegalArgumentException("minHeight > maxHeight");
    }
    this.mMinWidth = paramAttributeSet.getDimensionPixelSize(6, -1);
    this.mMaxWidth = paramAttributeSet.getDimensionPixelSize(7, -1);
    if ((this.mMinWidth != -1) && (this.mMaxWidth != -1) && (this.mMinWidth > this.mMaxWidth)) {
      throw new IllegalArgumentException("minWidth > maxWidth");
    }
    int j;
    if (this.mMaxWidth == -1)
    {
      bool = true;
      this.mComputeMaxWidth = bool;
      this.mVirtualButtonPressedDrawable = paramAttributeSet.getDrawable(9);
      this.mDisableKeyboard = paramAttributeSet.getBoolean(10, false);
      this.mWrapSelectorWheelValid = paramAttributeSet.getBoolean(11, true);
      paramInt = paramAttributeSet.getColor(12, -16777216);
      j = paramAttributeSet.getColor(13, -13619152);
      this.mTextGapPadding = ((int)paramAttributeSet.getDimension(14, 0.0F));
      paramAttributeSet.recycle();
      this.mPressedStateHelper = new PressedStateHelper();
      if (!this.mHasSelectorWheel) {
        break label734;
      }
      bool = false;
      label376:
      setWillNotDraw(bool);
      ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(i, this, true);
      paramAttributeSet = new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          StyledNumberPicker.this.hideSoftInput();
          StyledNumberPicker.this.mInputText.clearFocus();
          if (paramAnonymousView.getId() == 2131689814) {
            StyledNumberPicker.this.changeValueByOne(true);
          }
          for (;;)
          {
            return;
            StyledNumberPicker.this.changeValueByOne(false);
          }
        }
      };
      new View.OnLongClickListener()
      {
        public boolean onLongClick(View paramAnonymousView)
        {
          StyledNumberPicker.this.hideSoftInput();
          StyledNumberPicker.this.mInputText.clearFocus();
          if (paramAnonymousView.getId() == 2131689814) {
            StyledNumberPicker.this.postChangeCurrentByOneFromLongPress(true, 0L);
          }
          for (;;)
          {
            return true;
            StyledNumberPicker.this.postChangeCurrentByOneFromLongPress(false, 0L);
          }
        }
      };
      if (this.mHasSelectorWheel) {
        break label740;
      }
      this.mIncrementButton = ((ImageButton)findViewById(2131689814));
      this.mIncrementButton.setOnClickListener(paramAttributeSet);
      label450:
      if (this.mHasSelectorWheel) {
        break label748;
      }
      this.mDecrementButton = ((ImageButton)findViewById(2131689816));
      this.mDecrementButton.setOnClickListener(paramAttributeSet);
      label479:
      if (!isInEditMode()) {
        break label756;
      }
    }
    label734:
    label740:
    label748:
    label756:
    for (this.mInputText = new EditText(paramContext);; this.mInputText = ((EditText)findViewById(2131689815)))
    {
      if (this.mDisableKeyboard)
      {
        this.mInputText.setFocusable(false);
        this.mInputText.setFocusableInTouchMode(false);
      }
      this.mInputText.setOnFocusChangeListener(new View.OnFocusChangeListener()
      {
        public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
        {
          if (paramAnonymousBoolean) {
            StyledNumberPicker.this.mInputText.selectAll();
          }
          for (;;)
          {
            return;
            StyledNumberPicker.this.mInputText.setSelection(0, 0);
            StyledNumberPicker.this.validateInputTextView(paramAnonymousView);
          }
        }
      });
      this.mInputText.setRawInputType(2);
      this.mInputText.setImeOptions(6);
      this.mInputText.setTextColor(paramInt);
      paramContext = ViewConfiguration.get(paramContext);
      this.mTouchSlop = paramContext.getScaledTouchSlop();
      this.mMinimumFlingVelocity = paramContext.getScaledMinimumFlingVelocity();
      this.mMaximumFlingVelocity = (paramContext.getScaledMaximumFlingVelocity() / 8);
      this.mTextSize = ((int)this.mInputText.getTextSize());
      paramContext = new Paint();
      paramContext.setAntiAlias(true);
      paramContext.setTextAlign(Paint.Align.CENTER);
      paramContext.setTextSize(this.mTextSize);
      paramContext.setTypeface(this.mInputText.getTypeface());
      paramContext.setColor(j);
      this.mSelectorWheelPaint = paramContext;
      this.mCurrentWheelPaint = new Paint(this.mSelectorWheelPaint);
      this.mCurrentWheelPaint.setColor(paramInt);
      this.mFlingScroller = new Scroller(getContext(), null, true);
      this.mAdjustScroller = new Scroller(getContext(), new DecelerateInterpolator(2.5F));
      updateInputTextView();
      return;
      bool = false;
      break;
      bool = true;
      break label376;
      this.mIncrementButton = null;
      break label450;
      this.mDecrementButton = null;
      break label479;
    }
  }
  
  private void changeValueByOne(boolean paramBoolean)
  {
    if (this.mHasSelectorWheel)
    {
      this.mInputText.setVisibility(4);
      if (!moveToFinalScrollerPosition(this.mFlingScroller)) {
        moveToFinalScrollerPosition(this.mAdjustScroller);
      }
      this.mPreviousScrollerY = 0;
      if (paramBoolean)
      {
        this.mFlingScroller.startScroll(0, 0, 0, -this.mSelectorElementHeight, 300);
        this.mValue = Math.min(this.mValue + 1, this.mMaxValue);
        invalidate();
      }
    }
    for (;;)
    {
      return;
      this.mFlingScroller.startScroll(0, 0, 0, this.mSelectorElementHeight, 300);
      this.mValue = Math.max(this.mValue - 1, this.mMinValue);
      break;
      if (paramBoolean) {
        setValueInternal(this.mValue + 1, true);
      } else {
        setValueInternal(this.mValue - 1, true);
      }
    }
  }
  
  private void decrementSelectorIndices(int[] paramArrayOfInt)
  {
    for (int i = paramArrayOfInt.length - 1;; i--)
    {
      if (i <= 0)
      {
        int j = paramArrayOfInt[1] - 1;
        i = j;
        if (this.mWrapSelectorWheel)
        {
          i = j;
          if (j < this.mMinValue) {
            i = this.mMaxValue;
          }
        }
        paramArrayOfInt[0] = i;
        ensureCachedScrollSelectorValue(i);
        return;
      }
      paramArrayOfInt[i] = paramArrayOfInt[(i - 1)];
    }
  }
  
  private void ensureCachedScrollSelectorValue(int paramInt)
  {
    SparseArray localSparseArray = this.mSelectorIndexToStringCache;
    if ((String)localSparseArray.get(paramInt) != null) {
      return;
    }
    String str;
    if ((paramInt < this.mMinValue) || (paramInt > this.mMaxValue)) {
      str = "";
    }
    for (;;)
    {
      localSparseArray.put(paramInt, str);
      break;
      if (this.mDisplayedValues != null)
      {
        int i = this.mMinValue;
        str = this.mDisplayedValues[(paramInt - i)];
      }
      else
      {
        str = formatNumber(paramInt);
      }
    }
  }
  
  private boolean ensureScrollWheelAdjusted()
  {
    boolean bool = false;
    int j = this.mInitialScrollOffset - this.mCurrentScrollOffset;
    if (j != 0)
    {
      this.mPreviousScrollerY = 0;
      i = j;
      if (Math.abs(j) > this.mSelectorElementHeight / 2) {
        if (j <= 0) {
          break label72;
        }
      }
    }
    label72:
    for (int i = -this.mSelectorElementHeight;; i = this.mSelectorElementHeight)
    {
      i = j + i;
      this.mAdjustScroller.startScroll(0, 0, 0, i, 800);
      invalidate();
      bool = true;
      return bool;
    }
  }
  
  private void fling(int paramInt)
  {
    this.mPreviousScrollerY = 0;
    if (paramInt > 0) {
      this.mFlingScroller.fling(0, 0, 0, paramInt, 0, 0, 0, Integer.MAX_VALUE);
    }
    for (;;)
    {
      invalidate();
      return;
      this.mFlingScroller.fling(0, Integer.MAX_VALUE, 0, paramInt, 0, 0, 0, Integer.MAX_VALUE);
    }
  }
  
  private String formatNumber(int paramInt)
  {
    if (this.mFormatter != null) {}
    for (String str = this.mFormatter.format(paramInt);; str = String.valueOf(paramInt)) {
      return str;
    }
  }
  
  private int getSelectedPos(String paramString)
  {
    if (this.mDisplayedValues == null) {}
    for (;;)
    {
      try
      {
        i = Integer.parseInt(paramString);
        return i;
      }
      catch (NumberFormatException paramString) {}
      int i = 0;
      if (i >= this.mDisplayedValues.length) {}
      try
      {
        i = Integer.parseInt(paramString);
      }
      catch (NumberFormatException paramString)
      {
        for (;;) {}
      }
      paramString = paramString.toLowerCase();
      if (this.mDisplayedValues[i].toLowerCase().startsWith(paramString))
      {
        i = this.mMinValue + i;
      }
      else
      {
        i++;
        continue;
        i = this.mMinValue;
      }
    }
  }
  
  private int getWrappedSelectorIndex(int paramInt)
  {
    int i;
    if (paramInt > this.mMaxValue) {
      i = this.mMinValue + (paramInt - this.mMaxValue) % (this.mMaxValue - this.mMinValue) - 1;
    }
    for (;;)
    {
      return i;
      i = paramInt;
      if (paramInt < this.mMinValue) {
        i = this.mMaxValue - (this.mMinValue - paramInt) % (this.mMaxValue - this.mMinValue) + 1;
      }
    }
  }
  
  private void hideSoftInput()
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)getContext().getSystemService("input_method");
    if ((localInputMethodManager != null) && (localInputMethodManager.isActive(this.mInputText)))
    {
      localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
      if (this.mHasSelectorWheel) {
        this.mInputText.setVisibility(4);
      }
    }
  }
  
  private void incrementSelectorIndices(int[] paramArrayOfInt)
  {
    for (int i = 0;; i++)
    {
      if (i >= paramArrayOfInt.length - 1)
      {
        int j = paramArrayOfInt[(paramArrayOfInt.length - 2)] + 1;
        i = j;
        if (this.mWrapSelectorWheel)
        {
          i = j;
          if (j > this.mMaxValue) {
            i = this.mMinValue;
          }
        }
        paramArrayOfInt[(paramArrayOfInt.length - 1)] = i;
        ensureCachedScrollSelectorValue(i);
        return;
      }
      paramArrayOfInt[i] = paramArrayOfInt[(i + 1)];
    }
  }
  
  private void initializeFadingEdges()
  {
    setVerticalFadingEdgeEnabled(true);
    setFadingEdgeLength((getBottom() - getTop() - this.mTextSize) / 2);
  }
  
  private void initializeSelectorWheel()
  {
    initializeSelectorWheelIndices();
    int[] arrayOfInt = this.mSelectorIndices;
    int j = arrayOfInt.length;
    int i = this.mTextSize;
    this.mSelectorTextGapHeight = ((int)((getBottom() - getTop() - j * i) / arrayOfInt.length + 0.5F));
    this.mSelectorElementHeight = (this.mTextSize + this.mSelectorTextGapHeight - this.mTextGapPadding);
    this.mInitialScrollOffset = (this.mInputText.getBaseline() + this.mInputText.getTop() - this.mSelectorElementHeight * 1);
    this.mCurrentScrollOffset = this.mInitialScrollOffset;
    updateInputTextView();
  }
  
  private void initializeSelectorWheelIndices()
  {
    this.mSelectorIndexToStringCache.clear();
    int[] arrayOfInt = this.mSelectorIndices;
    int m = getValue();
    for (int i = 0;; i++)
    {
      if (i >= this.mSelectorIndices.length) {
        return;
      }
      int k = m + (i - 1);
      int j = k;
      if (this.mWrapSelectorWheel) {
        j = getWrappedSelectorIndex(k);
      }
      arrayOfInt[i] = j;
      ensureCachedScrollSelectorValue(arrayOfInt[i]);
    }
  }
  
  private int makeMeasureSpec(int paramInt1, int paramInt2)
  {
    if (paramInt2 == -1) {}
    for (;;)
    {
      return paramInt1;
      int i = View.MeasureSpec.getSize(paramInt1);
      int j = View.MeasureSpec.getMode(paramInt1);
      switch (j)
      {
      case 1073741824: 
      default: 
        throw new IllegalArgumentException("Unknown measure mode: " + j);
      case -2147483648: 
        paramInt1 = View.MeasureSpec.makeMeasureSpec(Math.min(i, paramInt2), 1073741824);
        break;
      case 0: 
        paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
      }
    }
  }
  
  private boolean moveToFinalScrollerPosition(Scroller paramScroller)
  {
    boolean bool = true;
    paramScroller.forceFinished(true);
    int k = paramScroller.getFinalY() - paramScroller.getCurrY();
    int j = this.mCurrentScrollOffset;
    int i = this.mSelectorElementHeight;
    j = this.mInitialScrollOffset - (j + k) % i;
    if (j != 0)
    {
      i = j;
      if (Math.abs(j) > this.mSelectorElementHeight / 2)
      {
        if (j > 0) {
          i = j - this.mSelectorElementHeight;
        }
      }
      else {
        scrollBy(0, k + i);
      }
    }
    for (;;)
    {
      return bool;
      i = j + this.mSelectorElementHeight;
      break;
      bool = false;
    }
  }
  
  private void notifyChange(int paramInt1, int paramInt2)
  {
    if (this.mSoundPool != null) {
      this.mSoundPool.play(this.mDidaSoundId, 1.0F, 1.0F, 0, 0, 1.0F);
    }
    if (this.mOnValueChangeListener != null) {
      this.mOnValueChangeListener.onValueChange(this, paramInt1, this.mValue);
    }
  }
  
  private void onScrollStateChange(int paramInt)
  {
    if (this.mScrollState == paramInt) {}
    for (;;)
    {
      return;
      this.mScrollState = paramInt;
      if (this.mOnScrollListener != null) {
        this.mOnScrollListener.onScrollStateChange(this, paramInt);
      }
    }
  }
  
  private void onScrollerFinished(Scroller paramScroller)
  {
    if (paramScroller == this.mFlingScroller)
    {
      if (!ensureScrollWheelAdjusted()) {
        updateInputTextView();
      }
      onScrollStateChange(0);
    }
    for (;;)
    {
      return;
      if (this.mScrollState != 1) {
        updateInputTextView();
      }
    }
  }
  
  private void postBeginSoftInputOnLongPressCommand()
  {
    if (this.mBeginSoftInputOnLongPressCommand == null) {
      this.mBeginSoftInputOnLongPressCommand = new BeginSoftInputOnLongPressCommand();
    }
    for (;;)
    {
      postDelayed(this.mBeginSoftInputOnLongPressCommand, ViewConfiguration.getLongPressTimeout());
      return;
      removeCallbacks(this.mBeginSoftInputOnLongPressCommand);
    }
  }
  
  private void postChangeCurrentByOneFromLongPress(boolean paramBoolean, long paramLong)
  {
    if (this.mChangeCurrentByOneFromLongPressCommand == null) {
      this.mChangeCurrentByOneFromLongPressCommand = new ChangeCurrentByOneFromLongPressCommand();
    }
    for (;;)
    {
      this.mChangeCurrentByOneFromLongPressCommand.setStep(paramBoolean);
      postDelayed(this.mChangeCurrentByOneFromLongPressCommand, paramLong);
      return;
      removeCallbacks(this.mChangeCurrentByOneFromLongPressCommand);
    }
  }
  
  private void postSetSelectionCommand(int paramInt1, int paramInt2)
  {
    if (this.mSetSelectionCommand == null) {
      this.mSetSelectionCommand = new SetSelectionCommand();
    }
    for (;;)
    {
      this.mSetSelectionCommand.mSelectionStart = paramInt1;
      this.mSetSelectionCommand.mSelectionEnd = paramInt2;
      post(this.mSetSelectionCommand);
      return;
      removeCallbacks(this.mSetSelectionCommand);
    }
  }
  
  private void removeAllCallbacks()
  {
    if (this.mChangeCurrentByOneFromLongPressCommand != null) {
      removeCallbacks(this.mChangeCurrentByOneFromLongPressCommand);
    }
    if (this.mSetSelectionCommand != null) {
      removeCallbacks(this.mSetSelectionCommand);
    }
    if (this.mBeginSoftInputOnLongPressCommand != null) {
      removeCallbacks(this.mBeginSoftInputOnLongPressCommand);
    }
    this.mPressedStateHelper.cancel();
  }
  
  private void removeBeginSoftInputCommand()
  {
    if (this.mBeginSoftInputOnLongPressCommand != null) {
      removeCallbacks(this.mBeginSoftInputOnLongPressCommand);
    }
  }
  
  private void removeChangeCurrentByOneFromLongPress()
  {
    if (this.mChangeCurrentByOneFromLongPressCommand != null) {
      removeCallbacks(this.mChangeCurrentByOneFromLongPressCommand);
    }
  }
  
  private int resolveSizeAndStateRespectingMinSize(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt2;
    if (paramInt1 != -1) {
      i = resolveSizeAndState(Math.max(paramInt1, paramInt2), paramInt3, 0);
    }
    return i;
  }
  
  private void setValueInternal(int paramInt, boolean paramBoolean)
  {
    if (this.mValue == paramInt) {
      return;
    }
    if (this.mWrapSelectorWheel) {}
    for (paramInt = getWrappedSelectorIndex(paramInt);; paramInt = Math.min(Math.max(paramInt, this.mMinValue), this.mMaxValue))
    {
      int i = this.mValue;
      this.mValue = paramInt;
      updateInputTextView();
      if (paramBoolean) {
        notifyChange(i, paramInt);
      }
      initializeSelectorWheelIndices();
      invalidate();
      break;
    }
  }
  
  private void showSoftInput()
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)getContext().getSystemService("input_method");
    if (localInputMethodManager != null)
    {
      if (this.mHasSelectorWheel) {
        this.mInputText.setVisibility(0);
      }
      this.mInputText.requestFocus();
      localInputMethodManager.showSoftInput(this.mInputText, 0);
    }
  }
  
  private void tryComputeMaxWidth()
  {
    if (!this.mComputeMaxWidth) {}
    int i;
    float f1;
    label23:
    int j;
    label39:
    int k;
    do
    {
      return;
      i = 0;
      if (this.mDisplayedValues != null) {
        break label147;
      }
      f1 = 0.0F;
      i = 0;
      if (i <= 9) {
        break;
      }
      i = 0;
      j = this.mMaxValue;
      if (j > 0) {
        break label134;
      }
      k = (int)(i * f1);
      i = k + (this.mInputText.getPaddingLeft() + this.mInputText.getPaddingRight());
    } while (this.mMaxWidth == i);
    if (i > this.mMinWidth) {}
    for (this.mMaxWidth = i;; this.mMaxWidth = this.mMinWidth)
    {
      invalidate();
      break;
      float f3 = this.mSelectorWheelPaint.measureText(String.valueOf(i));
      float f2 = f1;
      if (f3 > f1) {
        f2 = f3;
      }
      i++;
      f1 = f2;
      break label23;
      label134:
      i++;
      j /= 10;
      break label39;
      label147:
      int m = this.mDisplayedValues.length;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        f1 = this.mSelectorWheelPaint.measureText(this.mDisplayedValues[j]);
        k = i;
        if (f1 > i) {
          k = (int)f1;
        }
        j++;
        i = k;
      }
    }
  }
  
  private boolean updateInputTextView()
  {
    String str;
    if (this.mDisplayedValues == null)
    {
      str = formatNumber(this.mValue);
      if ((TextUtils.isEmpty(str)) || (str.equals(this.mInputText.getText().toString()))) {
        break label72;
      }
      this.mInputText.setText(str);
    }
    label72:
    for (boolean bool = true;; bool = false)
    {
      return bool;
      str = this.mDisplayedValues[(this.mValue - this.mMinValue)];
      break;
    }
  }
  
  private void validateInputTextView(View paramView)
  {
    paramView = String.valueOf(((TextView)paramView).getText());
    if (TextUtils.isEmpty(paramView)) {
      updateInputTextView();
    }
    for (;;)
    {
      return;
      setValueInternal(getSelectedPos(paramView.toString()), true);
    }
  }
  
  public void computeScroll()
  {
    Scroller localScroller2 = this.mFlingScroller;
    Scroller localScroller1 = localScroller2;
    if (localScroller2.isFinished())
    {
      localScroller2 = this.mAdjustScroller;
      localScroller1 = localScroller2;
      if (!localScroller2.isFinished()) {}
    }
    for (;;)
    {
      return;
      localScroller1.computeScrollOffset();
      int i = localScroller1.getCurrY();
      if (this.mPreviousScrollerY == 0) {
        this.mPreviousScrollerY = localScroller1.getStartY();
      }
      scrollBy(0, i - this.mPreviousScrollerY);
      this.mPreviousScrollerY = i;
      if (localScroller1.isFinished()) {
        onScrollerFinished(localScroller1);
      } else {
        invalidate();
      }
    }
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent)
  {
    if (!this.mHasSelectorWheel) {}
    for (boolean bool = super.dispatchHoverEvent(paramMotionEvent);; bool = false) {
      return bool;
    }
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    switch (paramKeyEvent.getKeyCode())
    {
    }
    for (;;)
    {
      return super.dispatchKeyEvent(paramKeyEvent);
      removeAllCallbacks();
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getActionMasked())
    {
    }
    for (;;)
    {
      return super.dispatchTouchEvent(paramMotionEvent);
      removeAllCallbacks();
    }
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getActionMasked())
    {
    }
    for (;;)
    {
      return super.dispatchTrackballEvent(paramMotionEvent);
      removeAllCallbacks();
    }
  }
  
  protected float getBottomFadingEdgeStrength()
  {
    return 0.9F;
  }
  
  public String[] getDisplayedValues()
  {
    return this.mDisplayedValues;
  }
  
  public int getMaxValue()
  {
    return this.mMaxValue;
  }
  
  public int getMinValue()
  {
    return this.mMinValue;
  }
  
  public int getSolidColor()
  {
    return this.mSolidColor;
  }
  
  protected float getTopFadingEdgeStrength()
  {
    return 0.9F;
  }
  
  public int getValue()
  {
    return this.mValue;
  }
  
  public boolean getWrapSelectorWheel()
  {
    return this.mWrapSelectorWheel;
  }
  
  protected void onDetachedFromWindow()
  {
    removeAllCallbacks();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (!this.mHasSelectorWheel)
    {
      super.onDraw(paramCanvas);
      return;
    }
    float f2 = (getRight() - getLeft()) / 2;
    float f1 = this.mCurrentScrollOffset;
    if ((this.mVirtualButtonPressedDrawable != null) && (this.mScrollState == 0))
    {
      if (this.mDecrementVirtualButtonPressed)
      {
        this.mVirtualButtonPressedDrawable.setBounds(0, 0, getRight(), this.mTopSelectionDividerTop);
        this.mVirtualButtonPressedDrawable.draw(paramCanvas);
      }
      if (this.mIncrementVirtualButtonPressed)
      {
        this.mVirtualButtonPressedDrawable.setBounds(0, this.mBottomSelectionDividerBottom, getRight(), getBottom());
        this.mVirtualButtonPressedDrawable.draw(paramCanvas);
      }
    }
    int[] arrayOfInt = this.mSelectorIndices;
    for (int i = 0;; i++)
    {
      if (i >= arrayOfInt.length)
      {
        if (this.mSelectionDivider == null) {
          break;
        }
        i = this.mTopSelectionDividerTop;
        j = this.mSelectionDividerHeight;
        this.mSelectionDivider.setBounds(0, i, getRight(), i + j);
        this.mSelectionDivider.draw(paramCanvas);
        j = this.mBottomSelectionDividerBottom;
        i = this.mSelectionDividerHeight;
        this.mSelectionDivider.setBounds(0, j - i, getRight(), j);
        this.mSelectionDivider.draw(paramCanvas);
        break;
      }
      int j = arrayOfInt[i];
      String str = (String)this.mSelectorIndexToStringCache.get(j);
      if (((i != 1) || (this.mInputText.getVisibility() != 0)) && (i == 1)) {
        paramCanvas.drawText(str, f2, f1, this.mCurrentWheelPaint);
      }
      f1 += this.mSelectorElementHeight;
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool = true;
    if ((!this.mHasSelectorWheel) || (!isEnabled())) {
      bool = false;
    }
    for (;;)
    {
      return bool;
      switch (paramMotionEvent.getActionMasked())
      {
      default: 
        bool = false;
        break;
      case 0: 
        removeAllCallbacks();
        this.mInputText.setVisibility(4);
        float f = paramMotionEvent.getY();
        this.mLastDownEventY = f;
        this.mLastDownOrMoveEventY = f;
        this.mLastDownEventTime = paramMotionEvent.getEventTime();
        this.mIngonreMoveEvents = false;
        this.mShowSoftInputOnTap = false;
        if (this.mLastDownEventY < this.mTopSelectionDividerTop) {
          if (this.mScrollState == 0) {
            this.mPressedStateHelper.buttonPressDelayed(2);
          }
        }
        for (;;)
        {
          getParent().requestDisallowInterceptTouchEvent(true);
          if (this.mFlingScroller.isFinished()) {
            break label197;
          }
          this.mFlingScroller.forceFinished(true);
          this.mAdjustScroller.forceFinished(true);
          onScrollStateChange(0);
          break;
          if ((this.mLastDownEventY > this.mBottomSelectionDividerBottom) && (this.mScrollState == 0)) {
            this.mPressedStateHelper.buttonPressDelayed(1);
          }
        }
        label197:
        if (!this.mAdjustScroller.isFinished())
        {
          this.mFlingScroller.forceFinished(true);
          this.mAdjustScroller.forceFinished(true);
        }
        else if (this.mLastDownEventY < this.mTopSelectionDividerTop)
        {
          hideSoftInput();
          postChangeCurrentByOneFromLongPress(false, ViewConfiguration.getLongPressTimeout());
        }
        else if (this.mLastDownEventY > this.mBottomSelectionDividerBottom)
        {
          hideSoftInput();
          postChangeCurrentByOneFromLongPress(true, ViewConfiguration.getLongPressTimeout());
        }
        else
        {
          this.mShowSoftInputOnTap = true;
          postBeginSoftInputOnLongPressCommand();
        }
        break;
      }
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.mHasSelectorWheel) {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    for (;;)
    {
      return;
      paramInt3 = getMeasuredWidth();
      paramInt4 = getMeasuredHeight();
      paramInt1 = this.mInputText.getMeasuredWidth();
      paramInt2 = this.mInputText.getMeasuredHeight();
      paramInt3 = (paramInt3 - paramInt1) / 2;
      paramInt4 = (paramInt4 - paramInt2) / 2;
      this.mInputText.layout(paramInt3, paramInt4, paramInt3 + paramInt1, paramInt4 + paramInt2);
      if (paramBoolean)
      {
        initializeSelectorWheel();
        initializeFadingEdges();
        this.mTopSelectionDividerTop = ((getHeight() - this.mSelectionDividersDistance) / 2 - this.mSelectionDividerHeight);
        this.mBottomSelectionDividerBottom = (this.mTopSelectionDividerTop + this.mSelectionDividerHeight * 2 + this.mSelectionDividersDistance);
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (!this.mHasSelectorWheel) {
      super.onMeasure(paramInt1, paramInt2);
    }
    for (;;)
    {
      return;
      super.onMeasure(makeMeasureSpec(paramInt1, this.mMaxWidth), makeMeasureSpec(paramInt2, this.mMaxHeight));
      setMeasuredDimension(resolveSizeAndStateRespectingMinSize(this.mMinWidth, getMeasuredWidth(), paramInt1), resolveSizeAndStateRespectingMinSize(this.mMinHeight, getMeasuredHeight(), paramInt2));
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool;
    if ((!isEnabled()) || (!this.mHasSelectorWheel))
    {
      bool = false;
      return bool;
    }
    if (this.mVelocityTracker == null) {
      this.mVelocityTracker = VelocityTracker.obtain();
    }
    this.mVelocityTracker.addMovement(paramMotionEvent);
    switch (paramMotionEvent.getActionMasked())
    {
    }
    for (;;)
    {
      bool = true;
      break;
      if (!this.mIngonreMoveEvents)
      {
        float f = paramMotionEvent.getY();
        if (this.mScrollState != 1) {
          if ((int)Math.abs(f - this.mLastDownEventY) > this.mTouchSlop)
          {
            removeAllCallbacks();
            onScrollStateChange(1);
          }
        }
        for (;;)
        {
          this.mLastDownOrMoveEventY = f;
          break;
          scrollBy(0, (int)(f - this.mLastDownOrMoveEventY));
          invalidate();
        }
        removeBeginSoftInputCommand();
        removeChangeCurrentByOneFromLongPress();
        this.mPressedStateHelper.cancel();
        VelocityTracker localVelocityTracker = this.mVelocityTracker;
        localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumFlingVelocity);
        i = (int)localVelocityTracker.getYVelocity();
        if (Math.abs(i) <= this.mMinimumFlingVelocity) {
          break label224;
        }
        fling(i);
        onScrollStateChange(2);
        this.mVelocityTracker.recycle();
        this.mVelocityTracker = null;
      }
    }
    label224:
    int j = (int)paramMotionEvent.getY();
    int i = (int)Math.abs(j - this.mLastDownEventY);
    long l1 = paramMotionEvent.getEventTime();
    long l2 = this.mLastDownEventTime;
    if ((i <= this.mTouchSlop) && (l1 - l2 < ViewConfiguration.getTapTimeout())) {
      if (this.mShowSoftInputOnTap)
      {
        this.mShowSoftInputOnTap = false;
        showSoftInput();
      }
    }
    for (;;)
    {
      onScrollStateChange(0);
      break;
      i = j / this.mSelectorElementHeight - 1;
      if (i > 0)
      {
        changeValueByOne(true);
        this.mPressedStateHelper.buttonTapped(1);
      }
      for (;;)
      {
        if (this.mButtonClickedListener == null) {
          break label370;
        }
        this.mButtonClickedListener.OnButtonClicked(this, this.mValue);
        break;
        if (i < 0)
        {
          changeValueByOne(false);
          this.mPressedStateHelper.buttonTapped(2);
        }
      }
      label370:
      continue;
      ensureScrollWheelAdjusted();
    }
  }
  
  public void release()
  {
    if (this.mSoundPool != null)
    {
      this.mSoundPool.release();
      this.mSoundPool = null;
    }
  }
  
  public void scrollBy(int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = this.mSelectorIndices;
    if ((!this.mWrapSelectorWheel) && (paramInt2 > 0) && (arrayOfInt[1] <= this.mMinValue)) {}
    for (this.mCurrentScrollOffset = this.mInitialScrollOffset;; this.mCurrentScrollOffset = this.mInitialScrollOffset)
    {
      return;
      if ((this.mWrapSelectorWheel) || (paramInt2 >= 0) || (arrayOfInt[1] < this.mMaxValue)) {
        break;
      }
    }
    for (this.mCurrentScrollOffset += paramInt2;; this.mCurrentScrollOffset = this.mInitialScrollOffset) {
      do
      {
        if (this.mCurrentScrollOffset - this.mInitialScrollOffset <= this.mSelectorTextGapHeight)
        {
          while (this.mCurrentScrollOffset - this.mInitialScrollOffset < -this.mSelectorTextGapHeight)
          {
            this.mCurrentScrollOffset += this.mSelectorElementHeight;
            incrementSelectorIndices(arrayOfInt);
            setValueInternal(arrayOfInt[1], true);
            if ((!this.mWrapSelectorWheel) && (arrayOfInt[1] >= this.mMaxValue)) {
              this.mCurrentScrollOffset = this.mInitialScrollOffset;
            }
          }
          break;
        }
        this.mCurrentScrollOffset -= this.mSelectorElementHeight;
        decrementSelectorIndices(arrayOfInt);
        setValueInternal(arrayOfInt[1], true);
      } while ((this.mWrapSelectorWheel) || (arrayOfInt[1] > this.mMinValue));
    }
  }
  
  public void setDisplayedValues(String[] paramArrayOfString)
  {
    if (this.mDisplayedValues == paramArrayOfString) {}
    for (;;)
    {
      return;
      if (this.mMinValue >= 0) {
        break;
      }
      Log.e("StyledNumberPicker", "mMinValue can not have displayedValues");
    }
    this.mDisplayedValues = paramArrayOfString;
    if (this.mDisplayedValues != null) {
      this.mInputText.setRawInputType(524289);
    }
    for (;;)
    {
      updateInputTextView();
      initializeSelectorWheelIndices();
      tryComputeMaxWidth();
      break;
      this.mInputText.setRawInputType(2);
    }
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    super.setEnabled(paramBoolean);
    if (!this.mHasSelectorWheel) {
      this.mIncrementButton.setEnabled(paramBoolean);
    }
    if (!this.mHasSelectorWheel) {
      this.mDecrementButton.setEnabled(paramBoolean);
    }
    this.mInputText.setEnabled(paramBoolean);
  }
  
  public void setFormatter(Formatter paramFormatter)
  {
    if (paramFormatter == this.mFormatter) {}
    for (;;)
    {
      return;
      this.mFormatter = paramFormatter;
      initializeSelectorWheelIndices();
      updateInputTextView();
    }
  }
  
  public void setMaxValue(int paramInt)
  {
    if (this.mMaxValue == paramInt) {
      return;
    }
    this.mMaxValue = paramInt;
    if (this.mMaxValue < this.mValue) {
      this.mValue = this.mMaxValue;
    }
    if ((this.mMaxValue - this.mMinValue > this.mSelectorIndices.length) && (this.mWrapSelectorWheelValid)) {}
    for (boolean bool = true;; bool = false)
    {
      setWrapSelectorWheel(bool);
      initializeSelectorWheelIndices();
      updateInputTextView();
      tryComputeMaxWidth();
      invalidate();
      break;
    }
  }
  
  public void setMinValue(int paramInt)
  {
    if (this.mMinValue == paramInt) {
      return;
    }
    this.mMinValue = paramInt;
    if (this.mMinValue > this.mValue) {
      this.mValue = this.mMinValue;
    }
    if ((this.mMaxValue - this.mMinValue > this.mSelectorIndices.length) && (this.mWrapSelectorWheelValid)) {}
    for (boolean bool = true;; bool = false)
    {
      setWrapSelectorWheel(bool);
      initializeSelectorWheelIndices();
      updateInputTextView();
      tryComputeMaxWidth();
      invalidate();
      break;
    }
  }
  
  public void setOnButtonClickedListener(OnButtonClickedListener paramOnButtonClickedListener)
  {
    this.mButtonClickedListener = paramOnButtonClickedListener;
  }
  
  public void setOnLongPressUpdateInterval(long paramLong)
  {
    this.mLongPressUpdateInterval = paramLong;
  }
  
  public void setOnScrollListener(OnScrollListener paramOnScrollListener)
  {
    this.mOnScrollListener = paramOnScrollListener;
  }
  
  public void setOnValueChangedListener(OnValueChangeListener paramOnValueChangeListener)
  {
    this.mOnValueChangeListener = paramOnValueChangeListener;
  }
  
  public void setValue(int paramInt)
  {
    setValueInternal(paramInt, false);
  }
  
  public void setWrapSelectorWheel(boolean paramBoolean)
  {
    if (this.mMaxValue - this.mMinValue >= this.mSelectorIndices.length) {}
    for (int i = 1;; i = 0)
    {
      if (((!paramBoolean) || (i != 0)) && (paramBoolean != this.mWrapSelectorWheel)) {
        this.mWrapSelectorWheel = paramBoolean;
      }
      return;
    }
  }
  
  @SuppressLint({"NewApi"})
  class AccessibilityNodeProviderImpl
    extends AccessibilityNodeProvider
  {
    private static final int UNDEFINED = Integer.MIN_VALUE;
    private static final int VIRTUAL_VIEW_ID_DECREMENT = 3;
    private static final int VIRTUAL_VIEW_ID_INCREMENT = 1;
    private static final int VIRTUAL_VIEW_ID_INPUT = 2;
    private int mAccessibilityFocusedView = Integer.MIN_VALUE;
    
    AccessibilityNodeProviderImpl() {}
    
    private String getVirtualDecrementButtonText()
    {
      Object localObject2 = null;
      int j = StyledNumberPicker.this.mValue - 1;
      int i = j;
      if (StyledNumberPicker.this.mWrapSelectorWheel) {
        i = StyledNumberPicker.this.getWrappedSelectorIndex(j);
      }
      Object localObject1 = localObject2;
      if (i >= StyledNumberPicker.this.mMinValue)
      {
        localObject1 = localObject2;
        if (StyledNumberPicker.this.mDisplayedValues == null) {
          localObject1 = StyledNumberPicker.this.formatNumber(i);
        }
      }
      return (String)localObject1;
    }
    
    private String getVirtualIncrementButtonText()
    {
      Object localObject2 = null;
      int j = StyledNumberPicker.this.mValue + 1;
      int i = j;
      if (StyledNumberPicker.this.mWrapSelectorWheel) {
        i = StyledNumberPicker.this.getWrappedSelectorIndex(j);
      }
      Object localObject1 = localObject2;
      if (i <= StyledNumberPicker.this.mMaxValue)
      {
        localObject1 = localObject2;
        if (StyledNumberPicker.this.mDisplayedValues == null) {
          localObject1 = StyledNumberPicker.this.formatNumber(i);
        }
      }
      return (String)localObject1;
    }
    
    private boolean hasVirtualDecrementButton()
    {
      if ((!StyledNumberPicker.this.getWrapSelectorWheel()) && (StyledNumberPicker.this.getValue() <= StyledNumberPicker.this.getMinValue())) {}
      for (boolean bool = false;; bool = true) {
        return bool;
      }
    }
    
    private boolean hasVirtualIncrementButton()
    {
      if ((!StyledNumberPicker.this.getWrapSelectorWheel()) && (StyledNumberPicker.this.getValue() >= StyledNumberPicker.this.getMaxValue())) {}
      for (boolean bool = false;; bool = true) {
        return bool;
      }
    }
    
    private void sendAccessibilityEventForVirtualButton(int paramInt1, int paramInt2, String paramString)
    {
      AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt2);
      localAccessibilityEvent.setClassName(Button.class.getName());
      localAccessibilityEvent.setPackageName(StyledNumberPicker.this.getContext().getPackageName());
      localAccessibilityEvent.getText().add(paramString);
      localAccessibilityEvent.setEnabled(StyledNumberPicker.this.isEnabled());
      localAccessibilityEvent.setSource(StyledNumberPicker.this, paramInt1);
      StyledNumberPicker.this.requestSendAccessibilityEvent(StyledNumberPicker.this, localAccessibilityEvent);
    }
    
    private void sendAccessibilityEventForVirtualText(int paramInt)
    {
      AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt);
      StyledNumberPicker.this.mInputText.onInitializeAccessibilityEvent(localAccessibilityEvent);
      StyledNumberPicker.this.mInputText.onPopulateAccessibilityEvent(localAccessibilityEvent);
      localAccessibilityEvent.setSource(StyledNumberPicker.this, 2);
      StyledNumberPicker.this.requestSendAccessibilityEvent(StyledNumberPicker.this, localAccessibilityEvent);
    }
    
    public boolean performAction(int paramInt1, int paramInt2, Bundle paramBundle)
    {
      boolean bool3 = false;
      boolean bool2 = false;
      boolean bool1;
      switch (paramInt1)
      {
      case 0: 
      default: 
        bool1 = super.performAction(paramInt1, paramInt2, paramBundle);
      }
      for (;;)
      {
        return bool1;
        switch (paramInt2)
        {
        default: 
          break;
        case 64: 
          bool1 = bool2;
          if (this.mAccessibilityFocusedView != paramInt1)
          {
            this.mAccessibilityFocusedView = paramInt1;
            bool1 = true;
          }
          break;
        case 128: 
          bool1 = bool2;
          if (this.mAccessibilityFocusedView == paramInt1)
          {
            this.mAccessibilityFocusedView = Integer.MIN_VALUE;
            bool1 = true;
          }
          break;
        case 4096: 
          bool1 = bool2;
          if (StyledNumberPicker.this.isEnabled()) {
            if (!StyledNumberPicker.this.getWrapSelectorWheel())
            {
              bool1 = bool2;
              if (StyledNumberPicker.this.getValue() >= StyledNumberPicker.this.getMaxValue()) {}
            }
            else
            {
              StyledNumberPicker.this.changeValueByOne(true);
              bool1 = true;
            }
          }
          break;
        case 8192: 
          bool1 = bool2;
          if (StyledNumberPicker.this.isEnabled()) {
            if (!StyledNumberPicker.this.getWrapSelectorWheel())
            {
              bool1 = bool2;
              if (StyledNumberPicker.this.getValue() <= StyledNumberPicker.this.getMinValue()) {}
            }
            else
            {
              StyledNumberPicker.this.changeValueByOne(false);
              bool1 = true;
              continue;
              switch (paramInt2)
              {
              default: 
                bool1 = StyledNumberPicker.this.mInputText.performAccessibilityAction(paramInt2, paramBundle);
                break;
              case 1: 
                bool1 = bool2;
                if (StyledNumberPicker.this.isEnabled())
                {
                  bool1 = bool2;
                  if (!StyledNumberPicker.this.mInputText.isFocused()) {
                    bool1 = StyledNumberPicker.this.mInputText.requestFocus();
                  }
                }
                break;
              case 2: 
                bool1 = bool2;
                if (StyledNumberPicker.this.isEnabled())
                {
                  bool1 = bool2;
                  if (StyledNumberPicker.this.mInputText.isFocused())
                  {
                    StyledNumberPicker.this.mInputText.clearFocus();
                    bool1 = true;
                  }
                }
                break;
              case 16: 
                bool1 = bool2;
                if (StyledNumberPicker.this.isEnabled())
                {
                  StyledNumberPicker.this.showSoftInput();
                  bool1 = true;
                }
                break;
              case 64: 
                bool1 = bool2;
                if (this.mAccessibilityFocusedView != paramInt1)
                {
                  this.mAccessibilityFocusedView = paramInt1;
                  sendAccessibilityEventForVirtualView(paramInt1, 32768);
                  StyledNumberPicker.this.mInputText.invalidate();
                  bool1 = true;
                }
                break;
              case 128: 
                bool1 = bool2;
                if (this.mAccessibilityFocusedView == paramInt1)
                {
                  this.mAccessibilityFocusedView = Integer.MIN_VALUE;
                  sendAccessibilityEventForVirtualView(paramInt1, 65536);
                  StyledNumberPicker.this.mInputText.invalidate();
                  bool1 = true;
                  continue;
                  switch (paramInt2)
                  {
                  default: 
                    bool1 = bool2;
                    break;
                  case 16: 
                    bool1 = bool2;
                    if (StyledNumberPicker.this.isEnabled())
                    {
                      StyledNumberPicker.this.changeValueByOne(true);
                      sendAccessibilityEventForVirtualView(paramInt1, 1);
                      bool1 = true;
                    }
                    break;
                  case 64: 
                    bool1 = bool2;
                    if (this.mAccessibilityFocusedView != paramInt1)
                    {
                      this.mAccessibilityFocusedView = paramInt1;
                      sendAccessibilityEventForVirtualView(paramInt1, 32768);
                      StyledNumberPicker.this.invalidate(0, StyledNumberPicker.this.mBottomSelectionDividerBottom, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.getBottom());
                      bool1 = true;
                    }
                    break;
                  case 128: 
                    bool1 = bool2;
                    if (this.mAccessibilityFocusedView == paramInt1)
                    {
                      this.mAccessibilityFocusedView = Integer.MIN_VALUE;
                      sendAccessibilityEventForVirtualView(paramInt1, 65536);
                      StyledNumberPicker.this.invalidate(0, StyledNumberPicker.this.mBottomSelectionDividerBottom, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.getBottom());
                      bool1 = true;
                      continue;
                      switch (paramInt2)
                      {
                      default: 
                        bool1 = bool2;
                        break;
                      case 16: 
                        bool1 = bool2;
                        if (StyledNumberPicker.this.isEnabled())
                        {
                          bool1 = bool3;
                          if (paramInt1 == 1) {
                            bool1 = true;
                          }
                          StyledNumberPicker.this.changeValueByOne(bool1);
                          sendAccessibilityEventForVirtualView(paramInt1, 1);
                          bool1 = true;
                        }
                        break;
                      case 64: 
                        bool1 = bool2;
                        if (this.mAccessibilityFocusedView != paramInt1)
                        {
                          this.mAccessibilityFocusedView = paramInt1;
                          sendAccessibilityEventForVirtualView(paramInt1, 32768);
                          StyledNumberPicker.this.invalidate(0, 0, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.mTopSelectionDividerTop);
                          bool1 = true;
                        }
                        break;
                      case 128: 
                        bool1 = bool2;
                        if (this.mAccessibilityFocusedView == paramInt1)
                        {
                          this.mAccessibilityFocusedView = Integer.MIN_VALUE;
                          sendAccessibilityEventForVirtualView(paramInt1, 65536);
                          StyledNumberPicker.this.invalidate(0, 0, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.mTopSelectionDividerTop);
                          bool1 = true;
                        }
                        break;
                      }
                    }
                    break;
                  }
                }
                break;
              }
            }
          }
          break;
        }
      }
    }
    
    public void sendAccessibilityEventForVirtualView(int paramInt1, int paramInt2)
    {
      switch (paramInt1)
      {
      }
      for (;;)
      {
        return;
        if (hasVirtualDecrementButton())
        {
          sendAccessibilityEventForVirtualButton(paramInt1, paramInt2, getVirtualDecrementButtonText());
          continue;
          sendAccessibilityEventForVirtualText(paramInt2);
          continue;
          if (hasVirtualIncrementButton()) {
            sendAccessibilityEventForVirtualButton(paramInt1, paramInt2, getVirtualIncrementButtonText());
          }
        }
      }
    }
  }
  
  class BeginSoftInputOnLongPressCommand
    implements Runnable
  {
    BeginSoftInputOnLongPressCommand() {}
    
    public void run()
    {
      StyledNumberPicker.this.showSoftInput();
      StyledNumberPicker.this.mIngonreMoveEvents = true;
    }
  }
  
  class ChangeCurrentByOneFromLongPressCommand
    implements Runnable
  {
    private boolean mIncrement;
    
    ChangeCurrentByOneFromLongPressCommand() {}
    
    private void setStep(boolean paramBoolean)
    {
      this.mIncrement = paramBoolean;
    }
    
    public void run()
    {
      StyledNumberPicker.this.changeValueByOne(this.mIncrement);
      StyledNumberPicker.this.postDelayed(this, StyledNumberPicker.this.mLongPressUpdateInterval);
    }
  }
  
  public static class CustomEditText
    extends EditText
  {
    public CustomEditText(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public void onEditorAction(int paramInt)
    {
      super.onEditorAction(paramInt);
      if (paramInt == 6) {
        clearFocus();
      }
    }
  }
  
  public static abstract interface Formatter
  {
    public abstract String format(int paramInt);
  }
  
  class InputTextFilter
    extends NumberKeyListener
  {
    InputTextFilter() {}
    
    public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
    {
      int i = 0;
      Object localObject1;
      if (StyledNumberPicker.this.mDisplayedValues == null)
      {
        localObject2 = super.filter(paramCharSequence, paramInt1, paramInt2, paramSpanned, paramInt3, paramInt4);
        localObject1 = localObject2;
        if (localObject2 == null) {
          localObject1 = paramCharSequence.subSequence(paramInt1, paramInt2);
        }
        paramCharSequence = String.valueOf(paramSpanned.subSequence(0, paramInt3)) + localObject1 + paramSpanned.subSequence(paramInt4, paramSpanned.length());
        if (!"".equals(paramCharSequence)) {}
      }
      for (;;)
      {
        return paramCharSequence;
        if (StyledNumberPicker.this.getSelectedPos(paramCharSequence) > StyledNumberPicker.this.mMaxValue)
        {
          paramCharSequence = "";
        }
        else
        {
          paramCharSequence = (CharSequence)localObject1;
          continue;
          paramCharSequence = String.valueOf(paramCharSequence.subSequence(paramInt1, paramInt2));
          if (!TextUtils.isEmpty(paramCharSequence)) {
            break;
          }
          paramCharSequence = "";
        }
      }
      Object localObject2 = String.valueOf(paramSpanned.subSequence(0, paramInt3)) + paramCharSequence + paramSpanned.subSequence(paramInt4, paramSpanned.length());
      paramCharSequence = String.valueOf(localObject2).toLowerCase();
      paramSpanned = StyledNumberPicker.this.mDisplayedValues;
      paramInt2 = paramSpanned.length;
      for (paramInt1 = i;; paramInt1++)
      {
        if (paramInt1 >= paramInt2)
        {
          paramCharSequence = "";
          break;
        }
        localObject1 = paramSpanned[paramInt1];
        if (((String)localObject1).toLowerCase().startsWith(paramCharSequence))
        {
          StyledNumberPicker.this.postSetSelectionCommand(((String)localObject2).length(), ((String)localObject1).length());
          paramCharSequence = ((String)localObject1).subSequence(paramInt3, ((String)localObject1).length());
          break;
        }
      }
    }
    
    protected char[] getAcceptedChars()
    {
      return StyledNumberPicker.DIGIT_CHARACTERS;
    }
    
    public int getInputType()
    {
      return 1;
    }
  }
  
  public static abstract interface OnButtonClickedListener
  {
    public abstract void OnButtonClicked(StyledNumberPicker paramStyledNumberPicker, int paramInt);
  }
  
  public static abstract interface OnScrollListener
  {
    public static final int SCROLL_STATE_FLING = 2;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_TOUCH_SCROLL = 1;
    
    public abstract void onScrollStateChange(StyledNumberPicker paramStyledNumberPicker, int paramInt);
  }
  
  public static abstract interface OnValueChangeListener
  {
    public abstract void onValueChange(StyledNumberPicker paramStyledNumberPicker, int paramInt1, int paramInt2);
  }
  
  class PressedStateHelper
    implements Runnable
  {
    public static final int BUTTON_DECREMENT = 2;
    public static final int BUTTON_INCREMENT = 1;
    private final int MODE_PRESS = 1;
    private final int MODE_TAPPED = 2;
    private int mManagedButton;
    private int mMode;
    
    PressedStateHelper() {}
    
    public void buttonPressDelayed(int paramInt)
    {
      cancel();
      this.mMode = 1;
      this.mManagedButton = paramInt;
      StyledNumberPicker.this.postDelayed(this, ViewConfiguration.getTapTimeout());
    }
    
    public void buttonTapped(int paramInt)
    {
      cancel();
      this.mMode = 2;
      this.mManagedButton = paramInt;
      StyledNumberPicker.this.post(this);
    }
    
    public void cancel()
    {
      this.mMode = 0;
      this.mManagedButton = 0;
      StyledNumberPicker.this.removeCallbacks(this);
      if (StyledNumberPicker.this.mIncrementVirtualButtonPressed)
      {
        StyledNumberPicker.this.mIncrementVirtualButtonPressed = false;
        StyledNumberPicker.this.invalidate(0, StyledNumberPicker.this.mBottomSelectionDividerBottom, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.getBottom());
      }
      StyledNumberPicker.this.mDecrementVirtualButtonPressed = false;
      if (StyledNumberPicker.this.mDecrementVirtualButtonPressed) {
        StyledNumberPicker.this.invalidate(0, 0, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.mTopSelectionDividerTop);
      }
    }
    
    public void run()
    {
      switch (this.mMode)
      {
      }
      for (;;)
      {
        return;
        switch (this.mManagedButton)
        {
        default: 
          break;
        case 1: 
          StyledNumberPicker.this.mIncrementVirtualButtonPressed = true;
          StyledNumberPicker.this.invalidate(0, StyledNumberPicker.this.mBottomSelectionDividerBottom, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.getBottom());
          break;
        case 2: 
          StyledNumberPicker.this.mDecrementVirtualButtonPressed = true;
          StyledNumberPicker.this.invalidate(0, 0, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.mTopSelectionDividerTop);
          continue;
          StyledNumberPicker localStyledNumberPicker;
          switch (this.mManagedButton)
          {
          default: 
            break;
          case 1: 
            if (!StyledNumberPicker.this.mIncrementVirtualButtonPressed) {
              StyledNumberPicker.this.postDelayed(this, ViewConfiguration.getPressedStateDuration());
            }
            localStyledNumberPicker = StyledNumberPicker.this;
            localStyledNumberPicker.mIncrementVirtualButtonPressed ^= true;
            StyledNumberPicker.this.invalidate(0, StyledNumberPicker.this.mBottomSelectionDividerBottom, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.getBottom());
            break;
          case 2: 
            if (!StyledNumberPicker.this.mDecrementVirtualButtonPressed) {
              StyledNumberPicker.this.postDelayed(this, ViewConfiguration.getPressedStateDuration());
            }
            localStyledNumberPicker = StyledNumberPicker.this;
            localStyledNumberPicker.mDecrementVirtualButtonPressed ^= true;
            StyledNumberPicker.this.invalidate(0, 0, StyledNumberPicker.this.getRight(), StyledNumberPicker.this.mTopSelectionDividerTop);
          }
          break;
        }
      }
    }
  }
  
  class SetSelectionCommand
    implements Runnable
  {
    private int mSelectionEnd;
    private int mSelectionStart;
    
    SetSelectionCommand() {}
    
    public void run()
    {
      StyledNumberPicker.this.mInputText.setSelection(this.mSelectionStart, this.mSelectionEnd);
    }
  }
}


