package com.yuneec.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class TwoSpeedSwitchView
  extends View
{
  private static final int SELECTED_HEIGHT = 64;
  private static final int SELECTED_WIDTH = 46;
  private static final String TAG = "TwoSpeedSwitchView-->";
  private static final int VIEW_HEIGHT = 126;
  private static final int VIEW_WIDTH = 46;
  Bitmap bitmap = null;
  Bitmap bitmap_bg = null;
  private Paint mPaint = new Paint();
  private Paint mPaintText = new Paint();
  private int mSpeed = 0;
  
  public TwoSpeedSwitchView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public TwoSpeedSwitchView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public TwoSpeedSwitchView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.bitmap_bg = BitmapFactory.decodeResource(getResources(), 2130837634);
    this.bitmap = BitmapFactory.decodeResource(getResources(), 2130837628);
  }
  
  public void drawBackground(Canvas paramCanvas)
  {
    this.mPaint.setAntiAlias(true);
    paramCanvas.drawBitmap(this.bitmap_bg, null, new Rect(0, 0, 46, 126), this.mPaint);
  }
  
  public void drawValue(Canvas paramCanvas)
  {
    this.mPaint.setAntiAlias(true);
    this.mPaintText.setAntiAlias(true);
    this.mPaintText.setTextAlign(Paint.Align.CENTER);
    this.mPaintText.setColor(-1);
    this.mPaintText.setTextSize(18.0F);
    this.mPaintText.setTypeface(Typeface.DEFAULT);
    if (this.mSpeed == 0) {
      paramCanvas.drawBitmap(this.bitmap, null, new Rect(0, 0, 46, 64), this.mPaint);
    }
    for (;;)
    {
      paramCanvas.drawText("0", 23.0F, 38.0F, this.mPaintText);
      paramCanvas.drawText("1", 23.0F, 102.0F, this.mPaintText);
      return;
      if (this.mSpeed == 1) {
        paramCanvas.drawBitmap(this.bitmap, null, new Rect(0, 64, 46, 128), this.mPaint);
      }
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    drawBackground(paramCanvas);
    drawValue(paramCanvas);
  }
  
  public void setValue(int paramInt)
  {
    if ((paramInt == 0) || (paramInt == 1)) {
      if (this.mSpeed != paramInt)
      {
        this.mSpeed = paramInt;
        postInvalidate();
      }
    }
    for (;;)
    {
      return;
      Log.e("TwoSpeedSwitchView-->", "mSpeed is not {0, 1}. value is " + this.mSpeed);
    }
  }
}


