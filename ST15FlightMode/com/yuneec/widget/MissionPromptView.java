package com.yuneec.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class MissionPromptView
  extends FrameLayout
{
  private MissionStep currentStep;
  private String currentStepOriginString;
  private TextView currentStepTextView;
  private Handler mHandler;
  private TextView prepareText;
  private TextView runningText;
  
  public MissionPromptView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public MissionPromptView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  @SuppressLint({"ResourceAsColor"})
  private void init(Context paramContext)
  {
    Object localObject = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    paramContext = new FrameLayout.LayoutParams(-1, -1);
    localObject = ((LayoutInflater)localObject).inflate(2130903077, null);
    addView((View)localObject, paramContext);
    this.prepareText = ((TextView)((View)localObject).findViewById(2131689758));
    this.runningText = ((TextView)((View)localObject).findViewById(2131689759));
    setBackgroundColor(17170445);
    this.mHandler = new Handler();
    this.currentStepTextView = this.prepareText;
    this.currentStepOriginString = this.currentStepTextView.getText().toString();
    updatStep(MissionStep.PREPARE);
  }
  
  public void updatStep(MissionStep paramMissionStep)
  {
    if (this.currentStep == paramMissionStep) {
      return;
    }
    if (this.currentStepTextView != null) {
      this.currentStepTextView.setEnabled(false);
    }
    switch (paramMissionStep)
    {
    }
    for (;;)
    {
      this.currentStepTextView.setEnabled(true);
      this.currentStep = paramMissionStep;
      this.currentStepOriginString = this.currentStepTextView.getText().toString();
      break;
      this.currentStepTextView = this.prepareText;
      continue;
      this.currentStepTextView = this.runningText;
    }
  }
  
  public void updateStepPrompt(MissionStep paramMissionStep, int paramInt)
  {
    updateStepPrompt(paramMissionStep, getContext().getString(paramInt));
  }
  
  public void updateStepPrompt(final MissionStep paramMissionStep, String paramString)
  {
    updatStep(paramMissionStep);
    final TextView localTextView = this.currentStepTextView;
    paramMissionStep = new String(this.currentStepOriginString);
    localTextView.setText(paramString);
    this.mHandler.postDelayed(new Runnable()
    {
      public void run()
      {
        localTextView.setText(paramMissionStep);
      }
    }, 3000L);
  }
  
  public static enum MissionStep
  {
    PREPARE,  RUNING;
  }
}


