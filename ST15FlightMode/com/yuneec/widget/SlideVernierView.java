package com.yuneec.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class SlideVernierView
  extends View
{
  private static final int VIEW_HEIGHT = 126;
  private static final int VIEW_WIDTH = 46;
  Bitmap bitmap = null;
  Bitmap bitmap_bg = null;
  private float cX;
  private float cY;
  private Paint mPaint = new Paint();
  private int mValue;
  
  public SlideVernierView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public SlideVernierView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public SlideVernierView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.bitmap_bg = BitmapFactory.decodeResource(getResources(), 2130837643);
    this.bitmap = BitmapFactory.decodeResource(getResources(), 2130837644);
  }
  
  public void drawBackground(Canvas paramCanvas)
  {
    this.mPaint.setAntiAlias(true);
    paramCanvas.drawBitmap(this.bitmap_bg, null, new Rect(0, 0, 46, 126), this.mPaint);
  }
  
  public void drawCursor(Canvas paramCanvas)
  {
    this.mPaint.setAntiAlias(true);
    this.cX = 0.0F;
    this.cY = (this.mValue * 0.55F);
    paramCanvas.drawBitmap(this.bitmap, this.cX, this.cY, this.mPaint);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    drawBackground(paramCanvas);
    drawCursor(paramCanvas);
  }
  
  public void setValue(int paramInt)
  {
    if ((this.mValue != paramInt) && ((this.mValue - paramInt >= 5) || (this.mValue - paramInt <= -5)))
    {
      this.mValue = paramInt;
      invalidate();
    }
  }
}


