package com.yuneec.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ButtonPicker extends LinearLayout implements View.OnClickListener, View.OnLongClickListener
{
  private static final DecimalFormat DF = new DecimalFormat("###.#");
  private static final String TAG = "ButtonPicker";
  private Runnable longClickRunnable = new Runnable()
  {
    public void run()
    {
      ButtonPicker.this.clickEvent(ButtonPicker.this.mClickedButton);
      if (ButtonPicker.this.mClickedButton.isPressed()) {
        ButtonPicker.this.mHandler.postDelayed(ButtonPicker.this.longClickRunnable, 5L);
      }
      for (;;)
      {
        return;
        ButtonPicker.this.mHandler.removeCallbacks(ButtonPicker.this.longClickRunnable);
        ButtonPicker.this.mClickedButton = null;
      }
    }
  };
  private Button mBtn1;
  private Button mBtn2;
  private Button mClickedButton;
  private Handler mHandler = new Handler();
  private int mIndex = -1;
  private float mIndexF = -1.0F;
  private OnPickerListener mListener;
  private float mMaxF;
  private int mMaxI;
  private float mMinF;
  private int mMinI;
  private List<String> mSelections;
  private float mStepF = 0.1F;
  private int mStepI = 1;
  private TextView mText;
  private int mType;
  
  public ButtonPicker(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public ButtonPicker(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ButtonPicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903046, this);
    setBackgroundResource(2130837537);
  }
  
  private void changeLayoutParamsHeight(View paramView, int paramInt)
  {
    ((LinearLayout.LayoutParams)paramView.getLayoutParams()).height = paramInt;
  }
  
  private void changeLayoutParamsWidth(View paramView, int paramInt)
  {
    ((LinearLayout.LayoutParams)paramView.getLayoutParams()).width = paramInt;
  }
  
  private void clickEvent(Button paramButton)
  {
    Object localObject2 = null;
    Object localObject1;
    switch (this.mType)
    {
    default: 
      localObject1 = localObject2;
    }
    for (;;)
    {
      if (localObject1 != null)
      {
        this.mText.setText((CharSequence)localObject1);
        if (this.mListener != null) {
          this.mListener.onClicked(this, (String)localObject1);
        }
      }
      return;
      if (paramButton.equals(this.mBtn1))
      {
        localObject1 = localObject2;
        if (this.mIndex > 0)
        {
          this.mIndex -= 1;
          localObject1 = (String)this.mSelections.get(this.mIndex);
        }
      }
      else
      {
        localObject1 = localObject2;
        if (paramButton.equals(this.mBtn2))
        {
          localObject1 = localObject2;
          if (this.mIndex < this.mSelections.size() - 1)
          {
            this.mIndex += 1;
            localObject1 = (String)this.mSelections.get(this.mIndex);
            continue;
            int i;
            if (paramButton.equals(this.mBtn1))
            {
              i = this.mIndex - this.mStepI;
              localObject1 = localObject2;
              if (i >= this.mMinI)
              {
                this.mIndex = i;
                localObject1 = String.valueOf(i);
              }
            }
            else
            {
              localObject1 = localObject2;
              if (paramButton.equals(this.mBtn2))
              {
                i = this.mIndex + this.mStepI;
                localObject1 = localObject2;
                if (i <= this.mMaxI)
                {
                  this.mIndex = i;
                  localObject1 = String.valueOf(i);
                  continue;
                  float f;
                  if (paramButton.equals(this.mBtn1))
                  {
                    f = this.mIndexF - this.mStepF;
                    localObject1 = localObject2;
                    if (f >= this.mMinF)
                    {
                      this.mIndexF = f;
                      localObject1 = DF.format(f);
                    }
                  }
                  else
                  {
                    localObject1 = localObject2;
                    if (paramButton.equals(this.mBtn2))
                    {
                      f = this.mIndexF + this.mStepF;
                      localObject1 = localObject2;
                      if (f <= this.mMaxF)
                      {
                        this.mIndexF = f;
                        localObject1 = DF.format(f);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  public float getFloatValue()
  {
    return this.mIndexF;
  }
  
  public int getIntegerValue()
  {
    return this.mIndex;
  }
  
  public String getStringValue()
  {
    return (String)this.mSelections.get(this.mIndex);
  }
  
  public void initiation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.mType = 2;
    this.mMaxF = paramFloat1;
    this.mMinF = paramFloat2;
    this.mStepF = paramFloat4;
    if ((paramFloat3 >= paramFloat2) && (paramFloat3 <= paramFloat1)) {
      this.mIndexF = paramFloat3;
    }
    for (;;)
    {
      this.mText.setText(String.valueOf(this.mIndex));
      return;
      Log.e("ButtonPicker", "Initiate invalid float value");
    }
  }
  
  public void initiation(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.mType = 1;
    this.mMaxI = paramInt1;
    this.mMinI = paramInt2;
    this.mStepI = paramInt4;
    if ((paramInt3 >= paramInt2) && (paramInt3 <= paramInt1)) {
      this.mIndex = paramInt3;
    }
    for (;;)
    {
      this.mText.setText(String.valueOf(this.mIndex));
      return;
      Log.e("ButtonPicker", "Initiate invalid integer value");
    }
  }
  
  public void initiation(String[] paramArrayOfString, String paramString)
  {
    this.mType = 0;
    if (paramArrayOfString != null)
    {
      this.mSelections = Arrays.asList(paramArrayOfString);
      int i = this.mSelections.indexOf(paramString);
      if (i == -1) {
        break label76;
      }
      this.mIndex = i;
      this.mText.setText(paramString);
    }
    for (;;)
    {
      return;
      this.mSelections = new ArrayList();
      this.mSelections.add("INH");
      paramString = "INH";
      break;
      label76:
      Log.e("ButtonPicker", "initiation -- Invalid value");
    }
  }
  
  public void initiationArray(ArrayList<String> paramArrayList, String paramString)
  {
    this.mType = 0;
    if (paramArrayList != null)
    {
      this.mSelections = paramArrayList;
      int i = this.mSelections.indexOf(paramString);
      if (i == -1) {
        break label73;
      }
      this.mIndex = i;
      this.mText.setText(paramString);
    }
    for (;;)
    {
      return;
      this.mSelections = new ArrayList();
      this.mSelections.add("INH");
      paramString = "INH";
      break;
      label73:
      Log.e("ButtonPicker", "initiation -- Invalid value");
    }
  }
  
  public void onClick(View paramView)
  {
    clickEvent((Button)paramView);
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mText = ((TextView)findViewById(2131689513));
    this.mBtn1 = ((Button)findViewById(2131689489));
    this.mBtn1.setOnClickListener(this);
    this.mBtn1.setOnLongClickListener(this);
    this.mBtn2 = ((Button)findViewById(2131689490));
    this.mBtn2.setOnClickListener(this);
    this.mBtn2.setOnLongClickListener(this);
    initiation(null, null);
    initiationArray(null, null);
  }
  
  public boolean onLongClick(View paramView)
  {
    this.mClickedButton = ((Button)paramView);
    this.mHandler.post(this.longClickRunnable);
    return true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int i;
    if (getOrientation() == 0)
    {
      i = View.MeasureSpec.getSize(paramInt1) / 3;
      paramInt1 = View.MeasureSpec.getSize(paramInt2);
      paramInt2 = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
    }
    for (int j = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);; j = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824))
    {
      LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)getLayoutParams();
      if (localLayoutParams.width != -2)
      {
        changeLayoutParamsWidth(this.mBtn1, i);
        changeLayoutParamsWidth(this.mBtn2, i);
        changeLayoutParamsWidth(this.mText, i);
      }
      if (localLayoutParams.height != -2)
      {
        changeLayoutParamsHeight(this.mBtn1, paramInt1);
        changeLayoutParamsHeight(this.mBtn2, paramInt1);
        changeLayoutParamsHeight(this.mText, paramInt1);
      }
      measureChild(this.mBtn1, paramInt2, j);
      measureChild(this.mBtn2, paramInt2, j);
      measureChild(this.mText, paramInt2, j);
      return;
      i = View.MeasureSpec.getSize(paramInt1);
      paramInt1 = View.MeasureSpec.getSize(paramInt2) / 3;
      paramInt2 = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
    }
  }
  
  public void setFloatValue(float paramFloat)
  {
    if ((paramFloat <= this.mMaxF) && (paramFloat >= this.mMinF))
    {
      this.mIndexF = paramFloat;
      this.mText.setText(DF.format(this.mIndexF));
    }
    for (;;)
    {
      return;
      Log.e("ButtonPicker", "setValue -- Invalid float value");
    }
  }
  
  public void setIntegerValue(int paramInt)
  {
    if ((paramInt <= this.mMaxI) && (paramInt >= this.mMinI))
    {
      this.mIndex = paramInt;
      this.mText.setText(String.valueOf(paramInt));
    }
    for (;;)
    {
      return;
      Log.e("ButtonPicker", "setValue -- Invalid integer value");
    }
  }
  
  public void setOnPickerListener(OnPickerListener paramOnPickerListener)
  {
    this.mListener = paramOnPickerListener;
  }
  
  public void setStringValue(String paramString)
  {
    int i = this.mSelections.indexOf(paramString);
    if (i != -1)
    {
      this.mIndex = i;
      this.mText.setText(paramString);
    }
    for (;;)
    {
      return;
      Log.e("ButtonPicker", "setValue -- Invalid String value");
    }
  }
  
  public static abstract interface OnPickerListener
  {
    public abstract void onClicked(ButtonPicker paramButtonPicker, String paramString);
  }
}


