package com.yuneec.widget;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class TextWidgetPreference
  extends ListPreference
{
  private static final String TAG = TextWidgetPreference.class.getSimpleName();
  private TextView mTextView;
  private String textString = "";
  
  public TextWidgetPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Log.i(TAG, "TextWidgetPreference");
  }
  
  protected void onBindView(View paramView)
  {
    super.onBindView(paramView);
    this.mTextView = ((TextView)paramView.findViewById(2131689834));
    this.mTextView.setText(this.textString);
  }
  
  public void setWidgetText(String paramString)
  {
    this.textString = paramString;
    if (this.mTextView != null) {
      this.mTextView.setText(this.textString);
    }
  }
}


