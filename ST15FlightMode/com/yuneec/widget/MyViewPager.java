package com.yuneec.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager
  extends ViewPager
{
  private static final float FLING_THRESHOLD = 25.0F;
  private float mLastX;
  
  public MyViewPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public MyViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool = super.onInterceptTouchEvent(paramMotionEvent);
    switch (paramMotionEvent.getAction())
    {
    }
    for (;;)
    {
      return bool;
      this.mLastX = paramMotionEvent.getX();
      continue;
      float f = paramMotionEvent.getX();
      if (Math.abs(this.mLastX - f) > 25.0F) {
        bool = true;
      }
      this.mLastX = paramMotionEvent.getX();
      continue;
      this.mLastX = 0.0F;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return super.onTouchEvent(paramMotionEvent);
  }
}


