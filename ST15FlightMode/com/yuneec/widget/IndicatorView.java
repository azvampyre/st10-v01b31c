package com.yuneec.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.yuneec.flightmode15.R.styleable;

public class IndicatorView
  extends LinearLayout
{
  private String mLabel;
  private TextView mLabelView;
  private String mValue;
  private TextView mValueView;
  
  public IndicatorView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public IndicatorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public IndicatorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903073, this);
    setGravity(17);
    setOrientation(1);
    setPadding(10, 2, 10, 10);
    setBackgroundResource(2130837655);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Indicator, paramInt, 0);
    this.mLabel = paramContext.getString(0);
    this.mValue = paramContext.getString(1);
    paramContext.recycle();
  }
  
  public CharSequence getValueText()
  {
    return this.mValueView.getText();
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mLabelView = ((TextView)findViewById(2131689690));
    this.mValueView = ((TextView)findViewById(2131689513));
    setLabelText(this.mLabel);
    setValueText(this.mValue);
  }
  
  public void setLabelText(CharSequence paramCharSequence)
  {
    if (this.mLabelView != null) {
      this.mLabelView.setText(paramCharSequence);
    }
  }
  
  public void setValueColor(int paramInt)
  {
    this.mValueView.setTextColor(paramInt);
  }
  
  public void setValueText(CharSequence paramCharSequence)
  {
    if (this.mValueView != null) {
      this.mValueView.setText(paramCharSequence);
    }
  }
  
  public void setValueTextDrawable(Drawable paramDrawable)
  {
    this.mValueView.setCompoundDrawablesWithIntrinsicBounds(null, paramDrawable, null, null);
    this.mValueView.setCompoundDrawablePadding(-4);
    this.mValueView.setPadding(0, 4, 0, 0);
  }
  
  public void setValueTextDrawableLevel(int paramInt)
  {
    Drawable localDrawable = this.mValueView.getCompoundDrawables()[1];
    if (localDrawable != null) {
      localDrawable.setLevel(paramInt);
    }
  }
  
  public void setValueTextSize(float paramFloat)
  {
    if (this.mValueView != null) {
      this.mValueView.setTextSize(paramFloat);
    }
  }
}


