package com.yuneec.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

public class KnobsView
  extends View
{
  private int degree = 0;
  private int mCenterX;
  private int mCenterY;
  private Drawable mKnobBackground;
  private Drawable mKnobDrawable;
  
  public KnobsView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public KnobsView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public KnobsView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.mKnobDrawable = paramContext.getResources().getDrawable(2130837632);
    this.mKnobDrawable.setBounds(0, 0, this.mKnobDrawable.getIntrinsicWidth(), this.mKnobDrawable.getIntrinsicHeight());
    this.mKnobBackground = paramContext.getResources().getDrawable(2130837633);
    this.mKnobBackground.setBounds(0, 0, this.mKnobBackground.getIntrinsicWidth(), this.mKnobBackground.getIntrinsicHeight());
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    this.mKnobBackground.draw(paramCanvas);
    paramCanvas.save();
    paramCanvas.rotate(this.degree, this.mCenterX, this.mCenterY);
    this.mKnobDrawable.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(this.mKnobDrawable.getIntrinsicWidth(), this.mKnobDrawable.getIntrinsicHeight());
    this.mCenterX = ((this.mKnobDrawable.getIntrinsicWidth() >> 1) + 1);
    this.mCenterY = ((this.mKnobDrawable.getIntrinsicHeight() >> 1) + 1);
  }
  
  public void setValue(int paramInt)
  {
    if (this.degree != paramInt)
    {
      this.degree = paramInt;
      invalidate();
    }
  }
}


