package com.yuneec.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import java.util.List;
import java.util.Map;

public class PressKeepListAdapter
  extends SimpleAdapter
{
  private int pressPosition = -1;
  
  public PressKeepListAdapter(Context paramContext, List<? extends Map<String, ?>> paramList, int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super(paramContext, paramList, paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramView = super.getView(paramInt, paramView, paramViewGroup);
    if (this.pressPosition != paramInt) {
      paramView.setBackgroundResource(0);
    }
    for (;;)
    {
      return paramView;
      paramView.setBackgroundResource(2130837659);
    }
  }
  
  public void setPressPosition(int paramInt)
  {
    this.pressPosition = paramInt;
  }
}


