package com.yuneec.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;

public class CounterView
  extends TextView
{
  public static final int COUNT_DOWN = 2;
  public static final int COUNT_UP = 1;
  private static final String TAG = "CounterView";
  private int mAlmostEndThreshold = 60000;
  private long mBase;
  private long mDuration;
  private String mFormatString;
  private boolean mIsAlmostEndPosted = false;
  private int mNormalColor;
  private OnTickListener mOnTickListener;
  private boolean mRunning = false;
  private int mStartTime = 0;
  private int mStyle = 1;
  private int mTickEndColor = -65536;
  private int mTrigger = 0;
  private Timer mUpdateTimer;
  private UpdateTimerTask mUpdateTimerTask;
  
  public CounterView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public CounterView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public CounterView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.mFormatString = paramContext.getString(2131296403);
    this.mNormalColor = getTextColors().getDefaultColor();
  }
  
  private void onTickEndInternal()
  {
    setTextColor(this.mTickEndColor);
  }
  
  private String setFormatTime(long paramLong)
  {
    int k = (int)(paramLong / 1000L);
    int m = 0;
    int j = 0;
    int i = k;
    if (k >= 3600)
    {
      j = k / 3600;
      i = k - j * 3600;
    }
    k = i;
    if (i >= 60)
    {
      m = i / 60;
      k = i - m * 60;
    }
    return String.format(this.mFormatString, new Object[] { Integer.valueOf(j), Integer.valueOf(m), Integer.valueOf(k) });
  }
  
  public int getDuration()
  {
    return (int)(this.mDuration / 1000L);
  }
  
  public int getStyle()
  {
    return this.mStyle;
  }
  
  public int getTrigger()
  {
    return this.mTrigger;
  }
  
  public boolean isStarted()
  {
    return this.mRunning;
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    setText(setFormatTime(0L));
  }
  
  public void reset()
  {
    setTextColor(this.mNormalColor);
    stop();
    setDuration(getDuration());
  }
  
  public void resetCounterColor()
  {
    setTextColor(this.mNormalColor);
  }
  
  public void setDuration(int paramInt)
  {
    if (this.mRunning) {
      Log.e("CounterView", "Counter already running cannot change duration");
    }
    for (;;)
    {
      return;
      this.mDuration = (paramInt * 1000);
      if (this.mStyle == 2) {
        setText(setFormatTime(this.mDuration));
      } else {
        setText(setFormatTime(0L));
      }
    }
  }
  
  public void setStartTime(int paramInt)
  {
    this.mStartTime = paramInt;
  }
  
  public void setStyle(int paramInt)
  {
    if (this.mRunning) {
      Log.e("CounterView", "Counter already running cannot change style");
    }
    for (;;)
    {
      return;
      this.mStyle = paramInt;
      if (this.mStyle == 2) {
        setText(setFormatTime(this.mDuration));
      } else {
        setText(setFormatTime(0L));
      }
    }
  }
  
  public void setTickListener(OnTickListener paramOnTickListener)
  {
    this.mOnTickListener = paramOnTickListener;
  }
  
  public void setTrigger(int paramInt)
  {
    this.mTrigger = paramInt;
  }
  
  public void start()
  {
    if (this.mDuration == 0L) {
      Log.i("CounterView", "duration is 0, no need to start");
    }
    for (;;)
    {
      return;
      if (!this.mRunning) {
        break;
      }
      Log.e("CounterView", "Counter is already running");
    }
    this.mRunning = true;
    if (this.mDuration <= this.mAlmostEndThreshold) {
      this.mIsAlmostEndPosted = true;
    }
    if (this.mStyle == 2) {}
    for (this.mBase = (SystemClock.elapsedRealtime() + this.mDuration);; this.mBase = SystemClock.elapsedRealtime())
    {
      this.mUpdateTimer = new Timer("Counter");
      this.mUpdateTimerTask = new UpdateTimerTask(null);
      this.mUpdateTimer.scheduleAtFixedRate(this.mUpdateTimerTask, 0L, 1000L);
      break;
    }
  }
  
  public void stop()
  {
    this.mRunning = false;
    this.mIsAlmostEndPosted = false;
    if (this.mUpdateTimer != null)
    {
      this.mUpdateTimer.cancel();
      if (this.mUpdateTimerTask != null) {
        this.mUpdateTimerTask.cancel();
      }
      this.mUpdateTimer = null;
    }
    this.mStartTime = 0;
    setText(setFormatTime(0L));
  }
  
  public static abstract interface OnTickListener
  {
    public abstract void onAlmostEnd();
    
    public abstract void onEnd();
    
    public abstract void onTick(int paramInt);
  }
  
  private class UpdateTimerTask
    extends TimerTask
  {
    private UpdateTimerTask() {}
    
    public void run()
    {
      CounterView.this.post(new Runnable()
      {
        public void run()
        {
          if (CounterView.this.mRunning) {
            if (CounterView.this.mStyle != 2) {
              break label419;
            }
          }
          label419:
          for (long l2 = CounterView.this.mBase - SystemClock.elapsedRealtime();; l2 = SystemClock.elapsedRealtime() - CounterView.this.mBase + CounterView.this.mStartTime)
          {
            long l3 = CounterView.this.mDuration;
            if ((!CounterView.this.mIsAlmostEndPosted) && (l2 <= CounterView.this.mAlmostEndThreshold) && (CounterView.this.mStyle == 2))
            {
              CounterView.this.mIsAlmostEndPosted = true;
              if (CounterView.this.mOnTickListener != null) {
                CounterView.this.mOnTickListener.onAlmostEnd();
              }
            }
            if ((!CounterView.this.mIsAlmostEndPosted) && (CounterView.this.mStyle == 1) && (l3 - l2 <= CounterView.this.mAlmostEndThreshold))
            {
              CounterView.this.mIsAlmostEndPosted = true;
              if (CounterView.this.mOnTickListener != null) {
                CounterView.this.mOnTickListener.onAlmostEnd();
              }
            }
            long l1 = l2;
            if (l2 <= 0L)
            {
              l1 = l2;
              if (CounterView.this.mStyle == 2)
              {
                l2 = 0L;
                CounterView.this.stop();
                l1 = l2;
                if (CounterView.this.mOnTickListener != null)
                {
                  CounterView.this.mOnTickListener.onEnd();
                  l1 = l2;
                }
              }
            }
            l2 = l1;
            if (l1 >= l3)
            {
              l2 = l1;
              if (CounterView.this.mStyle == 1)
              {
                l1 = l3;
                CounterView.this.stop();
                l2 = l1;
                if (CounterView.this.mOnTickListener != null)
                {
                  CounterView.this.mOnTickListener.onEnd();
                  l2 = l1;
                }
              }
            }
            String str = CounterView.this.setFormatTime(l2);
            CounterView.this.setText(str);
            if (CounterView.this.mOnTickListener != null) {
              CounterView.this.mOnTickListener.onTick((int)(l2 / 1000L));
            }
            return;
          }
        }
      });
    }
  }
}


