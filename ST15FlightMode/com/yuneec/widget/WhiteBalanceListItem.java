package com.yuneec.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WhiteBalanceListItem
  extends LinearLayout
{
  private static final String TAG = "WhiteBalanceListItem";
  private ImageView mImg;
  private TextView mText;
  
  public WhiteBalanceListItem(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public WhiteBalanceListItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public WhiteBalanceListItem(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903120, this);
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mText = ((TextView)findViewById(2131689514));
    this.mImg = ((ImageView)findViewById(2131690022));
  }
  
  public void setImage(Drawable paramDrawable)
  {
    this.mImg.setImageDrawable(paramDrawable);
  }
  
  public void setSelected(boolean paramBoolean)
  {
    if (paramBoolean) {
      setBackgroundDrawable(getResources().getDrawable(2130837659));
    }
    for (;;)
    {
      return;
      setBackgroundColor(getResources().getColor(2131427339));
    }
  }
  
  public void setText(String paramString)
  {
    this.mText.setText(paramString);
  }
}


