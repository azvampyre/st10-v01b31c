package com.yuneec.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MyToast
  extends Toast
{
  public MyToast(Context paramContext)
  {
    super(paramContext);
  }
  
  public static Toast makeText(Context paramContext, int paramInt1, int paramInt2, int paramInt3)
    throws Resources.NotFoundException
  {
    return makeText(paramContext, paramContext.getResources().getText(paramInt1), paramInt2, paramInt3);
  }
  
  public static Toast makeText(Context paramContext, CharSequence paramCharSequence, int paramInt1, int paramInt2)
  {
    Toast localToast = new Toast(paramContext);
    paramContext = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    if (paramInt1 == 0) {}
    for (paramContext = paramContext.inflate(2130903089, null);; paramContext = paramContext.inflate(paramInt1, null))
    {
      ((TextView)paramContext.findViewById(2131689813)).setText(paramCharSequence);
      localToast.setView(paramContext);
      localToast.setDuration(paramInt2);
      return localToast;
    }
  }
}


