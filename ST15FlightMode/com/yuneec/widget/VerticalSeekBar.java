package com.yuneec.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.SeekBar;

public class VerticalSeekBar
  extends SeekBar
{
  private boolean isInScrollingContainer = false;
  private boolean mIsDragging;
  private int mScaledTouchSlop;
  private float mTouchDownY;
  float mTouchProgressOffset;
  
  public VerticalSeekBar(Context paramContext)
  {
    super(paramContext);
  }
  
  public VerticalSeekBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public VerticalSeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.mScaledTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
  }
  
  private void attemptClaimDrag()
  {
    ViewParent localViewParent = getParent();
    if (localViewParent != null) {
      localViewParent.requestDisallowInterceptTouchEvent(true);
    }
  }
  
  private void trackTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = getHeight();
    int m = getPaddingTop();
    int j = getPaddingBottom();
    int k = i - m - j;
    int n = (int)paramMotionEvent.getY();
    float f2 = 0.0F;
    float f1;
    if (n > i - j) {
      f1 = 0.0F;
    }
    for (;;)
    {
      setProgress((int)(f2 + getMax() * f1));
      return;
      if (n < m)
      {
        f1 = 1.0F;
      }
      else
      {
        f1 = (k - n + m) / k;
        f2 = this.mTouchProgressOffset;
      }
    }
  }
  
  public boolean isInScrollingContainer()
  {
    return this.isInScrollingContainer;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    try
    {
      paramCanvas.rotate(-90.0F);
      paramCanvas.translate(-getHeight(), 0.0F);
      super.onDraw(paramCanvas);
      return;
    }
    finally
    {
      paramCanvas = finally;
      throw paramCanvas;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    try
    {
      super.onMeasure(paramInt2, paramInt1);
      setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt2, paramInt1, paramInt4, paramInt3);
  }
  
  void onStartTrackingTouch()
  {
    this.mIsDragging = true;
  }
  
  void onStopTrackingTouch()
  {
    this.mIsDragging = false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool = false;
    if (!isEnabled()) {
      return bool;
    }
    switch (paramMotionEvent.getAction())
    {
    default: 
    case 0: 
      for (;;)
      {
        bool = true;
        break;
        if (isInScrollingContainer())
        {
          this.mTouchDownY = paramMotionEvent.getY();
        }
        else
        {
          setPressed(true);
          invalidate();
          onStartTrackingTouch();
          trackTouchEvent(paramMotionEvent);
          attemptClaimDrag();
          onSizeChanged(getWidth(), getHeight(), 0, 0);
        }
      }
    case 2: 
      if (this.mIsDragging) {
        trackTouchEvent(paramMotionEvent);
      }
      for (;;)
      {
        onSizeChanged(getWidth(), getHeight(), 0, 0);
        break;
        if (Math.abs(paramMotionEvent.getY() - this.mTouchDownY) > this.mScaledTouchSlop)
        {
          setPressed(true);
          invalidate();
          onStartTrackingTouch();
          trackTouchEvent(paramMotionEvent);
          attemptClaimDrag();
        }
      }
    }
    if (this.mIsDragging)
    {
      trackTouchEvent(paramMotionEvent);
      onStopTrackingTouch();
      setPressed(false);
    }
    for (;;)
    {
      onSizeChanged(getWidth(), getHeight(), 0, 0);
      invalidate();
      break;
      onStartTrackingTouch();
      trackTouchEvent(paramMotionEvent);
      onStopTrackingTouch();
    }
  }
  
  public void setInScrollingContainer(boolean paramBoolean)
  {
    this.isInScrollingContainer = paramBoolean;
  }
  
  public void setProgress(int paramInt)
  {
    try
    {
      super.setProgress(paramInt);
      onSizeChanged(getWidth(), getHeight(), 0, 0);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


