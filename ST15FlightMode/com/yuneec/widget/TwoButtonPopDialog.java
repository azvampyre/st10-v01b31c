package com.yuneec.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

public class TwoButtonPopDialog
  extends Dialog
{
  private TextView mMessage;
  private Button mNegativeButton;
  private Button mPositiveButton;
  private TextView mTitle;
  
  public TwoButtonPopDialog(Context paramContext)
  {
    super(paramContext, 2131230729);
    setContentView(2130903114);
    adjustHeight(520);
    this.mTitle = ((TextView)findViewById(2131689817));
    this.mMessage = ((TextView)findViewById(2131689812));
    this.mPositiveButton = ((Button)findViewById(2131689819));
    this.mNegativeButton = ((Button)findViewById(2131689837));
  }
  
  public void adjustHeight(int paramInt)
  {
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.height = paramInt;
    getWindow().setAttributes(localLayoutParams);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(Integer.MIN_VALUE);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 3) && (!paramKeyEvent.isCanceled())) {}
    for (boolean bool = true;; bool = super.onKeyUp(paramInt, paramKeyEvent)) {
      return bool;
    }
  }
  
  public void setMessage(int paramInt)
  {
    this.mMessage.setText(paramInt);
  }
  
  public void setMessage(CharSequence paramCharSequence)
  {
    this.mMessage.setText(paramCharSequence);
  }
  
  public void setMessageGravity(int paramInt)
  {
    this.mMessage.setGravity(paramInt);
  }
  
  public void setNegativeButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    this.mNegativeButton.setText(paramInt);
    this.mNegativeButton.setOnClickListener(paramOnClickListener);
  }
  
  public void setPositiveButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    this.mPositiveButton.setText(paramInt);
    this.mPositiveButton.setOnClickListener(paramOnClickListener);
  }
  
  public void setTitle(int paramInt)
  {
    this.mTitle.setText(paramInt);
  }
  
  public void setTitle(CharSequence paramCharSequence)
  {
    this.mTitle.setText(paramCharSequence);
  }
}


