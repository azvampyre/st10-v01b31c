package com.yuneec.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.yuneec.flightmode15.WbIsoChangedListener;

public class ToggleFrameView
  extends FrameLayout
  implements WbIsoChangedListener
{
  private int currentWbModeIndex;
  private TextView isoModeText;
  private ImageView isoToggleView;
  private Context mContext;
  private ToggleOnClickListener toggleOnClickListener;
  private ImageView wbModeView;
  private ImageView wbToggleView;
  
  public ToggleFrameView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    initView();
    initListener();
  }
  
  private void initListener()
  {
    this.wbToggleView.setOnClickListener(new View.OnClickListener()
    {
      @SuppressLint({"ResourceAsColor"})
      public void onClick(View paramAnonymousView)
      {
        ToggleFrameView.this.wbToggleView.setImageResource(2130837833);
        switch (ToggleFrameView.this.currentWbModeIndex)
        {
        }
        for (;;)
        {
          ToggleFrameView.this.isoToggleView.setImageResource(2130837588);
          ToggleFrameView.this.isoModeText.setTextColor(-5592406);
          if (ToggleFrameView.this.toggleOnClickListener != null) {
            ToggleFrameView.this.toggleOnClickListener.wbItemOnClick(paramAnonymousView);
          }
          return;
          ToggleFrameView.this.wbModeView.setImageResource(2130837511);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837662);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837578);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837562);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837592);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837653);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837812);
        }
      }
    });
    this.isoToggleView.setOnClickListener(new View.OnClickListener()
    {
      @SuppressLint({"ResourceAsColor"})
      public void onClick(View paramAnonymousView)
      {
        ToggleFrameView.this.wbToggleView.setImageResource(2130837832);
        switch (ToggleFrameView.this.currentWbModeIndex)
        {
        }
        for (;;)
        {
          ToggleFrameView.this.isoToggleView.setImageResource(2130837589);
          ToggleFrameView.this.isoModeText.setTextColor(-47872);
          if (ToggleFrameView.this.toggleOnClickListener != null) {
            ToggleFrameView.this.toggleOnClickListener.isoItemOnClick(paramAnonymousView);
          }
          return;
          ToggleFrameView.this.wbModeView.setImageResource(2130837510);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837661);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837577);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837561);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837591);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837652);
          continue;
          ToggleFrameView.this.wbModeView.setImageResource(2130837811);
        }
      }
    });
  }
  
  private void initView()
  {
    setBackgroundResource(2130837817);
    this.wbModeView = new ImageView(this.mContext);
    this.wbModeView.setImageResource(2130837511);
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(60, 60);
    localLayoutParams.setMargins(2, 32, 0, 0);
    addView(this.wbModeView, localLayoutParams);
    this.currentWbModeIndex = 0;
    this.wbToggleView = new ImageView(this.mContext);
    this.wbToggleView.setImageResource(2130837833);
    localLayoutParams = new FrameLayout.LayoutParams(60, 60);
    localLayoutParams.setMargins(2, 32, 0, 0);
    addView(this.wbToggleView, localLayoutParams);
    this.isoToggleView = new ImageView(this.mContext);
    this.isoToggleView.setImageResource(2130837588);
    localLayoutParams = new FrameLayout.LayoutParams(60, 60);
    localLayoutParams.setMargins(2, 103, 0, 0);
    addView(this.isoToggleView, localLayoutParams);
    this.isoModeText = new TextView(this.mContext);
    this.isoModeText.setText(2131296635);
    this.isoModeText.setTextSize(8.0F);
    this.isoModeText.setGravity(17);
    localLayoutParams = new FrameLayout.LayoutParams(60, 30);
    localLayoutParams.setMargins(2, 132, 0, 0);
    addView(this.isoModeText, localLayoutParams);
  }
  
  public void onIsoModeChanged(boolean paramBoolean)
  {
    if (paramBoolean) {
      this.isoModeText.setText(2131296636);
    }
    for (;;)
    {
      return;
      this.isoModeText.setText(2131296635);
    }
  }
  
  public void onWbModeChanged(int paramInt, boolean paramBoolean)
  {
    if (paramBoolean) {
      switch (paramInt)
      {
      }
    }
    for (;;)
    {
      this.currentWbModeIndex = paramInt;
      return;
      this.wbModeView.setImageResource(2130837511);
      continue;
      this.wbModeView.setImageResource(2130837662);
      continue;
      this.wbModeView.setImageResource(2130837578);
      continue;
      this.wbModeView.setImageResource(2130837562);
      continue;
      this.wbModeView.setImageResource(2130837592);
      continue;
      this.wbModeView.setImageResource(2130837653);
      continue;
      this.wbModeView.setImageResource(2130837812);
      continue;
      switch (this.currentWbModeIndex)
      {
      default: 
        break;
      case 0: 
        this.wbModeView.setImageResource(2130837510);
        break;
      case 1: 
        this.wbModeView.setImageResource(2130837661);
        break;
      case 2: 
        this.wbModeView.setImageResource(2130837577);
        break;
      case 3: 
        this.wbModeView.setImageResource(2130837561);
        break;
      case 4: 
        this.wbModeView.setImageResource(2130837591);
        break;
      case 5: 
        this.wbModeView.setImageResource(2130837652);
        break;
      case 6: 
        this.wbModeView.setImageResource(2130837811);
      }
    }
  }
  
  public void setToggleOnClickListener(ToggleOnClickListener paramToggleOnClickListener)
  {
    this.toggleOnClickListener = paramToggleOnClickListener;
  }
  
  public static abstract interface ToggleOnClickListener
  {
    public abstract void isoItemOnClick(View paramView);
    
    public abstract void wbItemOnClick(View paramView);
  }
}


