package com.yuneec.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

public class MyProgressDialog
  extends Dialog
{
  private TextView mMessage;
  private Object mTitle;
  
  public MyProgressDialog(Context paramContext)
  {
    super(paramContext);
    requestWindowFeature(1);
    setContentView(2130903088);
    this.mMessage = ((TextView)findViewById(2131689812));
  }
  
  public static MyProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramContext = new MyProgressDialog(paramContext);
    paramContext.setTitle(paramCharSequence1);
    paramContext.setMessage(paramCharSequence2);
    paramContext.setCancelable(paramBoolean2);
    paramContext.show();
    return paramContext;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(Integer.MIN_VALUE);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 3) && (!paramKeyEvent.isCanceled())) {}
    for (boolean bool = true;; bool = super.onKeyUp(paramInt, paramKeyEvent)) {
      return bool;
    }
  }
  
  public void setMessage(CharSequence paramCharSequence)
  {
    this.mMessage.setText(paramCharSequence);
  }
}


