package com.yuneec.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.yuneec.flightmode15.Utilities;
import com.yuneec.mission.MissionInterface.MissionType;
import com.yuneec.mission.MissionPresentation;
import com.yuneec.mission.MissionPresentation.MissionCallback;
import com.yuneec.mission.MissionPresentation.MissionState;
import com.yuneec.mission.SaveMissionData;
import com.yuneec.uartcontroller.GPSUpLinkData;
import com.yuneec.uartcontroller.RoiData;
import com.yuneec.uartcontroller.UARTInfoMessage.MissionReply;
import com.yuneec.uartcontroller.WaypointData;
import java.util.ArrayList;

public class MissionView
  extends FrameLayout
  implements View.OnClickListener
{
  private static final String LAST_POINTS_TABLE_NAME = "last_points_table";
  private Button addPointButton;
  private Button backButton;
  private Button cccButton;
  private Button clearPointButton;
  private Button confirmButton;
  private MissionPresentation.MissionState currentMissionState = MissionPresentation.MissionState.NONE;
  private MissionInterface.MissionType currentMissionType = MissionInterface.MissionType.NONE;
  private Button cycleButton;
  private Button deletePointButton;
  private float droneAltitude;
  private float droneLatitude;
  private float droneLongitude;
  private Button exitButton;
  private int fMode = 0;
  private Button journeyButton;
  private Button loadRecordButton;
  private LinearLayout missionMenuView;
  private MissionPresentation missionPresentation;
  private MissionPromptView missionPromptView;
  private MissionViewCallback missionViewCallback;
  private Button pauseButton;
  private ArrayList<WaypointData> recordPoints = new ArrayList();
  private Button resumeButton;
  private Button roiButton;
  private RoiData roiData = new RoiData();
  private Button setCenterButton;
  
  public MissionView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public MissionView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  private void addCCCSettingMenu()
  {
    this.missionMenuView.removeAllViews();
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 6;
    this.missionMenuView.addView(this.backButton, localLayoutParams);
    this.missionMenuView.addView(this.addPointButton, localLayoutParams);
    this.missionMenuView.addView(this.deletePointButton, localLayoutParams);
    this.missionMenuView.addView(this.clearPointButton, localLayoutParams);
    this.missionMenuView.addView(this.loadRecordButton, localLayoutParams);
    localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 0;
    this.missionMenuView.addView(this.confirmButton, localLayoutParams);
    this.missionPromptView.setVisibility(0);
  }
  
  private void addMissionControlMenu()
  {
    this.missionMenuView.removeAllViews();
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(80, 50);
    localLayoutParams.rightMargin = 6;
    this.missionMenuView.addView(this.resumeButton, localLayoutParams);
    this.missionMenuView.addView(this.pauseButton, localLayoutParams);
    localLayoutParams = new LinearLayout.LayoutParams(80, 50);
    localLayoutParams.rightMargin = 0;
    this.missionMenuView.addView(this.exitButton, localLayoutParams);
    this.missionPromptView.setVisibility(0);
  }
  
  private void addMissionSelectMenu()
  {
    this.missionMenuView.removeAllViews();
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 6;
    this.missionMenuView.addView(this.cccButton, localLayoutParams);
    this.missionMenuView.addView(this.journeyButton, localLayoutParams);
    this.missionMenuView.addView(this.roiButton, localLayoutParams);
    localLayoutParams = new LinearLayout.LayoutParams(80, 50);
    localLayoutParams.rightMargin = 0;
    this.missionMenuView.addView(this.cycleButton, localLayoutParams);
    this.missionPromptView.setVisibility(8);
  }
  
  private void addOtherSettingMenu()
  {
    this.missionMenuView.removeAllViews();
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 6;
    this.missionMenuView.addView(this.backButton, localLayoutParams);
    localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 0;
    this.missionMenuView.addView(this.confirmButton, localLayoutParams);
    this.missionPromptView.setVisibility(0);
  }
  
  private void addRoiSettingMenu()
  {
    this.missionMenuView.removeAllViews();
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 6;
    this.missionMenuView.addView(this.backButton, localLayoutParams);
    this.missionMenuView.addView(this.setCenterButton, localLayoutParams);
    localLayoutParams = new LinearLayout.LayoutParams(75, 50);
    localLayoutParams.rightMargin = 0;
    this.missionMenuView.addView(this.confirmButton, localLayoutParams);
    this.missionPromptView.setVisibility(0);
  }
  
  @SuppressLint({"ResourceAsColor"})
  private void init(Context paramContext)
  {
    Object localObject = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    this.missionPresentation = new MissionPresentation(new MissionPresentation.MissionCallback()
    {
      public void onCancel(MissionInterface.MissionType paramAnonymousMissionType, boolean paramAnonymousBoolean, int paramAnonymousInt)
      {
        if (paramAnonymousBoolean)
        {
          MissionView.this.currentMissionState = MissionPresentation.MissionState.NONE;
          MissionView.this.missionPromptView.updatStep(MissionPromptView.MissionStep.PREPARE);
          MissionView.this.addMissionSelectMenu();
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(paramAnonymousInt);
          }
        }
      }
      
      public void onConfirm(MissionInterface.MissionType paramAnonymousMissionType, boolean paramAnonymousBoolean, int paramAnonymousInt)
      {
        if (paramAnonymousBoolean)
        {
          MissionView.this.currentMissionState = MissionPresentation.MissionState.RESUME;
          MissionView.this.missionPromptView.updatStep(MissionPromptView.MissionStep.RUNING);
          MissionView.this.addMissionControlMenu();
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(paramAnonymousInt);
          }
        }
      }
      
      public void onPause(MissionInterface.MissionType paramAnonymousMissionType, boolean paramAnonymousBoolean, int paramAnonymousInt)
      {
        if (paramAnonymousBoolean)
        {
          MissionView.this.currentMissionState = MissionPresentation.MissionState.PAUSE;
          MissionView.this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.RUNING, "Pause");
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(paramAnonymousInt);
          }
        }
      }
      
      public void onPrepare(MissionInterface.MissionType paramAnonymousMissionType, boolean paramAnonymousBoolean, int paramAnonymousInt)
      {
        if (paramAnonymousBoolean)
        {
          MissionView.this.currentMissionState = MissionPresentation.MissionState.PREPARE;
          MissionView.this.missionPresentation.confirm(paramAnonymousMissionType);
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(paramAnonymousInt);
          }
        }
      }
      
      public void onQuery(MissionInterface.MissionType paramAnonymousMissionType, boolean paramAnonymousBoolean, Object paramAnonymousObject)
      {
        if (paramAnonymousBoolean) {
          if ((paramAnonymousMissionType == MissionInterface.MissionType.CRUVE_CABLE_CAM) && ((paramAnonymousObject instanceof WaypointData)))
          {
            paramAnonymousMissionType = (WaypointData)paramAnonymousObject;
            paramAnonymousMissionType.pointerIndex = MissionView.this.recordPoints.size();
            MissionView.this.recordPoints.add(paramAnonymousMissionType);
            MissionView.this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.PREPARE, "Point: " + MissionView.this.recordPoints.size());
          }
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(2131296655);
          }
        }
      }
      
      public void onResume(MissionInterface.MissionType paramAnonymousMissionType, boolean paramAnonymousBoolean, int paramAnonymousInt)
      {
        if (paramAnonymousBoolean)
        {
          MissionView.this.currentMissionState = MissionPresentation.MissionState.RESUME;
          MissionView.this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.RUNING, "Resume");
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(paramAnonymousInt);
          }
        }
      }
      
      public void onTerminate(boolean paramAnonymousBoolean, int paramAnonymousInt)
      {
        if (paramAnonymousBoolean)
        {
          MissionView.this.currentMissionState = MissionPresentation.MissionState.COMPLETE;
          MissionView.this.missionPromptView.updatStep(MissionPromptView.MissionStep.PREPARE);
          MissionView.this.addMissionSelectMenu();
        }
        for (;;)
        {
          return;
          if (MissionView.this.missionViewCallback != null) {
            MissionView.this.missionViewCallback.updateError(paramAnonymousInt);
          }
        }
      }
      
      public void onUpdateState(MissionInterface.MissionType paramAnonymousMissionType, MissionPresentation.MissionState paramAnonymousMissionState, int paramAnonymousInt)
      {
        switch (paramAnonymousMissionState)
        {
        }
        for (;;)
        {
          MissionView.this.missionViewCallback.updateStep(paramAnonymousMissionState);
          MissionView.this.currentMissionState = paramAnonymousMissionState;
          MissionView.this.updateControlButtonsEnable(paramAnonymousMissionState);
          return;
          MissionView.this.missionPromptView.updatStep(MissionPromptView.MissionStep.PREPARE);
          continue;
          MissionView.this.missionPromptView.updatStep(MissionPromptView.MissionStep.PREPARE);
          continue;
          MissionView.this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.RUNING, "Resume");
          continue;
          MissionView.this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.RUNING, "Pause");
          continue;
          MissionView.this.missionPromptView.updatStep(MissionPromptView.MissionStep.PREPARE);
        }
      }
    });
    initButton(paramContext);
    paramContext = new FrameLayout.LayoutParams(-1, -1);
    localObject = ((LayoutInflater)localObject).inflate(2130903076, null);
    addView((View)localObject, paramContext);
    setBackgroundColor(17170445);
    this.missionMenuView = ((LinearLayout)((View)localObject).findViewById(2131689757));
    this.missionPromptView = ((MissionPromptView)((View)localObject).findViewById(2131689756));
    addMissionSelectMenu();
    setVisibility(8);
  }
  
  private void initButton(Context paramContext)
  {
    this.cccButton = new Button(paramContext);
    this.cccButton.setText(2131296656);
    this.cccButton.setTextSize(8.0F);
    this.cccButton.setBackgroundResource(2130837566);
    this.cccButton.setOnClickListener(this);
    this.journeyButton = new Button(paramContext);
    this.journeyButton.setText(2131296657);
    this.journeyButton.setTextSize(8.0F);
    this.journeyButton.setBackgroundResource(2130837566);
    this.journeyButton.setOnClickListener(this);
    this.roiButton = new Button(paramContext);
    this.roiButton.setText(2131296658);
    this.roiButton.setTextSize(8.0F);
    this.roiButton.setBackgroundResource(2130837566);
    this.roiButton.setOnClickListener(this);
    this.cycleButton = new Button(paramContext);
    this.cycleButton.setText(2131296659);
    this.cycleButton.setTextSize(8.0F);
    this.cycleButton.setBackgroundResource(2130837566);
    this.cycleButton.setOnClickListener(this);
    this.backButton = new Button(paramContext);
    this.backButton.setText(2131296660);
    this.backButton.setTextSize(8.0F);
    this.backButton.setBackgroundResource(2130837566);
    this.backButton.setOnClickListener(this);
    this.addPointButton = new Button(paramContext);
    this.addPointButton.setText(2131296661);
    this.addPointButton.setTextSize(8.0F);
    this.addPointButton.setBackgroundResource(2130837566);
    this.addPointButton.setOnClickListener(this);
    this.deletePointButton = new Button(paramContext);
    this.deletePointButton.setText(2131296662);
    this.deletePointButton.setTextSize(8.0F);
    this.deletePointButton.setBackgroundResource(2130837566);
    this.deletePointButton.setOnClickListener(this);
    this.clearPointButton = new Button(paramContext);
    this.clearPointButton.setText(2131296663);
    this.clearPointButton.setTextSize(8.0F);
    this.clearPointButton.setBackgroundResource(2130837566);
    this.clearPointButton.setOnClickListener(this);
    this.loadRecordButton = new Button(paramContext);
    this.loadRecordButton.setText(2131296664);
    this.loadRecordButton.setTextSize(8.0F);
    this.loadRecordButton.setBackgroundResource(2130837566);
    this.loadRecordButton.setOnClickListener(this);
    this.setCenterButton = new Button(paramContext);
    this.setCenterButton.setText(2131296665);
    this.setCenterButton.setTextSize(8.0F);
    this.setCenterButton.setBackgroundResource(2130837566);
    this.setCenterButton.setOnClickListener(this);
    this.confirmButton = new Button(paramContext);
    this.confirmButton.setText(2131296666);
    this.confirmButton.setTextSize(8.0F);
    this.confirmButton.setBackgroundResource(2130837566);
    this.confirmButton.setOnClickListener(this);
    this.resumeButton = new Button(paramContext);
    this.resumeButton.setText(2131296668);
    this.resumeButton.setTextSize(8.0F);
    this.resumeButton.setBackgroundResource(2130837566);
    this.resumeButton.setOnClickListener(this);
    this.pauseButton = new Button(paramContext);
    this.pauseButton.setText(2131296667);
    this.pauseButton.setTextSize(8.0F);
    this.pauseButton.setBackgroundResource(2130837566);
    this.pauseButton.setOnClickListener(this);
    this.exitButton = new Button(paramContext);
    this.exitButton.setText(2131296669);
    this.exitButton.setTextSize(8.0F);
    this.exitButton.setBackgroundResource(2130837566);
    this.exitButton.setOnClickListener(this);
  }
  
  private void updateControlButtonsEnable(MissionPresentation.MissionState paramMissionState)
  {
    switch (paramMissionState)
    {
    }
    for (;;)
    {
      return;
      this.resumeButton.setEnabled(true);
      this.pauseButton.setEnabled(true);
      continue;
      this.resumeButton.setEnabled(false);
      this.pauseButton.setEnabled(false);
      continue;
      this.resumeButton.setEnabled(false);
      this.pauseButton.setEnabled(true);
      continue;
      this.resumeButton.setEnabled(true);
      this.pauseButton.setEnabled(false);
      continue;
      this.resumeButton.setEnabled(true);
      this.pauseButton.setEnabled(true);
    }
  }
  
  public void onClick(View paramView)
  {
    if (paramView == this.cccButton)
    {
      this.currentMissionType = MissionInterface.MissionType.CRUVE_CABLE_CAM;
      addCCCSettingMenu();
      this.recordPoints.clear();
    }
    for (;;)
    {
      return;
      if (paramView == this.journeyButton)
      {
        this.currentMissionType = MissionInterface.MissionType.JOURNEY;
        addOtherSettingMenu();
      }
      else if (paramView == this.roiButton)
      {
        this.currentMissionType = MissionInterface.MissionType.ROI;
        this.roiData.reset();
        addRoiSettingMenu();
      }
      else if (paramView == this.cycleButton)
      {
        this.currentMissionType = MissionInterface.MissionType.CYCLE;
        addOtherSettingMenu();
      }
      else if (paramView == this.backButton)
      {
        this.currentMissionType = MissionInterface.MissionType.NONE;
        this.currentMissionState = MissionPresentation.MissionState.NONE;
        this.missionPresentation.terminate();
        addMissionSelectMenu();
      }
      else if (paramView == this.addPointButton)
      {
        this.missionPresentation.query(MissionInterface.MissionType.CRUVE_CABLE_CAM);
      }
      else if (paramView == this.deletePointButton)
      {
        if (this.recordPoints.size() > 0) {
          this.recordPoints.remove(this.recordPoints.size() - 1);
        }
        this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.PREPARE, "Point: " + this.recordPoints.size());
      }
      else if (paramView == this.clearPointButton)
      {
        this.recordPoints.clear();
        this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.PREPARE, "Clear");
      }
      else if (paramView == this.loadRecordButton)
      {
        this.recordPoints.clear();
        SaveMissionData.loadCruveCableCam("last_points_table", this.recordPoints);
        this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.PREPARE, "Point: " + this.recordPoints.size());
      }
      else if (paramView == this.confirmButton)
      {
        if (this.currentMissionType == MissionInterface.MissionType.CRUVE_CABLE_CAM)
        {
          SaveMissionData.saveCruveCableCam("last_points_table", this.recordPoints);
          this.missionPresentation.prepare(this.currentMissionType, this.recordPoints);
        }
        else if (this.currentMissionType == MissionInterface.MissionType.ROI)
        {
          if ((this.roiData.centerLatitude == 0.0F) || (this.roiData.centerLongitude == 0.0F))
          {
            if (this.missionViewCallback != null) {
              this.missionViewCallback.updateError(2131296653);
            }
          }
          else if (this.missionViewCallback != null)
          {
            paramView = this.missionViewCallback.getControllerGps();
            paramView = Utilities.calculateDistanceAndBearing(this.roiData.centerLatitude, this.roiData.centerLongitude, 0.0F, paramView.lat, paramView.lon, 0.0F);
            if (paramView[0] > 1000.0F)
            {
              if (this.missionViewCallback != null) {
                this.missionViewCallback.updateError(2131296654);
              }
            }
            else
            {
              this.roiData.radius = Math.round(paramView[0]);
              this.roiData.speed = 5;
              this.missionPresentation.prepare(this.currentMissionType, this.roiData);
            }
          }
        }
        else if (this.currentMissionType == MissionInterface.MissionType.JOURNEY)
        {
          this.missionPresentation.prepare(this.currentMissionType, new Integer(10));
        }
        else if (this.currentMissionType == MissionInterface.MissionType.CYCLE)
        {
          this.missionPresentation.prepare(this.currentMissionType, null);
        }
      }
      else if (paramView == this.setCenterButton)
      {
        this.roiData.centerLatitude = this.droneLatitude;
        this.roiData.centerLongitude = this.droneLongitude;
        this.roiData.centerAltitude = this.droneAltitude;
        this.missionPromptView.updateStepPrompt(MissionPromptView.MissionStep.PREPARE, "Set Center");
      }
      else if (paramView == this.resumeButton)
      {
        this.missionPresentation.resume(this.currentMissionType);
      }
      else if (paramView == this.pauseButton)
      {
        this.missionPresentation.pause(this.currentMissionType);
      }
      else if (paramView == this.exitButton)
      {
        this.missionPresentation.terminate();
      }
    }
  }
  
  public void setMissionViewCallback(MissionViewCallback paramMissionViewCallback)
  {
    this.missionViewCallback = paramMissionViewCallback;
  }
  
  public void updateDroneGps(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.droneLatitude = paramFloat1;
    this.droneLongitude = paramFloat2;
    this.droneAltitude = paramFloat3;
    this.fMode = paramInt;
  }
  
  public void updateFeedback(UARTInfoMessage.MissionReply paramMissionReply)
  {
    this.missionPresentation.updateFeedback(this.fMode, paramMissionReply);
  }
  
  public static abstract interface MissionViewCallback
  {
    public abstract GPSUpLinkData getControllerGps();
    
    public abstract void updateError(int paramInt);
    
    public abstract void updateStep(MissionPresentation.MissionState paramMissionState);
  }
}


