package com.yuneec.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

public class HomeCompassView
  extends View
{
  private Drawable mBg;
  private int mCompassHeight;
  private int mCompassWidth;
  private int mDegree;
  private Drawable mHome;
  private boolean mHomeEnabled;
  
  public HomeCompassView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public HomeCompassView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public HomeCompassView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.mBg = getResources().getDrawable(2130837637);
    this.mHome = getResources().getDrawable(2130837636);
    this.mCompassWidth = this.mBg.getIntrinsicWidth();
    this.mCompassHeight = this.mBg.getIntrinsicHeight();
    this.mBg.setBounds(0, 0, this.mCompassWidth, this.mCompassHeight);
    int i = this.mHome.getIntrinsicWidth();
    int j = this.mCompassWidth - i >> 1;
    int k = this.mHome.getIntrinsicHeight();
    this.mHome.setBounds(j, 10, j + i, 10 + k);
    this.mDegree = 0;
    this.mHomeEnabled = false;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.mBg != null) {
      this.mBg.draw(paramCanvas);
    }
    if (this.mHomeEnabled)
    {
      paramCanvas.save();
      paramCanvas.rotate(this.mDegree, this.mCompassWidth >> 1, this.mCompassHeight >> 1);
      this.mHome.draw(paramCanvas);
      paramCanvas.restore();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    setMeasuredDimension(this.mCompassWidth, this.mCompassHeight);
  }
  
  public void setDirection(int paramInt)
  {
    if ((this.mHomeEnabled) && (paramInt != this.mDegree))
    {
      this.mDegree = paramInt;
      invalidate();
    }
  }
  
  public void setHomeEnabled(boolean paramBoolean)
  {
    if (paramBoolean != this.mHomeEnabled)
    {
      this.mHomeEnabled = paramBoolean;
      invalidate();
    }
  }
}


