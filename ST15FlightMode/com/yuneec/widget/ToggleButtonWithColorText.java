package com.yuneec.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

public class ToggleButtonWithColorText
  extends ToggleButton
{
  public ToggleButtonWithColorText(Context paramContext)
  {
    super(paramContext);
  }
  
  public ToggleButtonWithColorText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public ToggleButtonWithColorText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public void setChecked(boolean paramBoolean)
  {
    super.setChecked(paramBoolean);
    playSoundEffect(0);
  }
}


