package com.yuneec.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

public class SyncToggleButton
  extends ToggleButton
{
  private boolean mByUser = true;
  private CompoundButton.OnCheckedChangeListener mListener = new CompoundButton.OnCheckedChangeListener()
  {
    public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
    {
      SyncToggleButton.this.mUpdateChangeListener.onCheckedChanged(paramAnonymousCompoundButton, paramAnonymousBoolean, SyncToggleButton.this.mByUser);
      SyncToggleButton.this.mByUser = true;
    }
  };
  private OnUpdateChangeListener mUpdateChangeListener;
  
  public SyncToggleButton(Context paramContext)
  {
    super(paramContext);
  }
  
  public SyncToggleButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SyncToggleButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public boolean isCheckedByUser()
  {
    return this.mByUser;
  }
  
  public void setChecked(boolean paramBoolean)
  {
    this.mByUser = true;
    super.setChecked(paramBoolean);
  }
  
  public void setOnUpdateChangeListener(OnUpdateChangeListener paramOnUpdateChangeListener)
  {
    super.setOnCheckedChangeListener(this.mListener);
    this.mUpdateChangeListener = paramOnUpdateChangeListener;
  }
  
  public void syncState(boolean paramBoolean)
  {
    this.mByUser = false;
    super.setChecked(paramBoolean);
  }
  
  public void updateToogleButton(boolean paramBoolean)
  {
    super.setChecked(paramBoolean);
  }
  
  public static abstract interface OnUpdateChangeListener
  {
    public abstract void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean1, boolean paramBoolean2);
  }
}


