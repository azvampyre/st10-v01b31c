package com.yuneec.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class ButtonSeekBar
  extends LinearLayout
  implements SeekBar.OnSeekBarChangeListener, View.OnClickListener, View.OnLongClickListener
{
  private static final String TAG = "ButtonSeekBar";
  private Runnable longClickRunnable = new Runnable()
  {
    public void run()
    {
      ButtonSeekBar.this.clickEvent(ButtonSeekBar.this.mClickedButton);
      if (ButtonSeekBar.this.mClickedButton.isPressed()) {
        ButtonSeekBar.this.mHandler.postDelayed(ButtonSeekBar.this.longClickRunnable, 5L);
      }
      for (;;)
      {
        return;
        ButtonSeekBar.this.mHandler.removeCallbacks(ButtonSeekBar.this.longClickRunnable);
        ButtonSeekBar.this.mClickedButton = null;
      }
    }
  };
  private Button mBtnLeft;
  private Button mBtnRight;
  private onButtonSeekChangeListener mButtonSeekChangeListener;
  private Button mClickedButton;
  private Handler mHandler = new Handler();
  private int mMax;
  private int mMin;
  private SeekBar mSeekBar;
  private TextView mText;
  private int mValue;
  
  public ButtonSeekBar(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public ButtonSeekBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ButtonSeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903047, this);
  }
  
  private void clickEvent(Button paramButton)
  {
    if (paramButton.equals(this.mBtnLeft)) {
      if (this.mValue <= this.mMin) {}
    }
    for (this.mValue -= 1;; this.mValue += 1) {
      do
      {
        this.mSeekBar.setProgress(this.mValue - this.mMin);
        if (this.mButtonSeekChangeListener != null) {
          this.mButtonSeekChangeListener.onProgressChanged(this, this.mValue, true);
        }
        return;
      } while ((!paramButton.equals(this.mBtnRight)) || (this.mValue >= this.mMax));
    }
  }
  
  public void initiation(String paramString, int paramInt1, int paramInt2)
  {
    this.mMax = paramInt1;
    this.mMin = paramInt2;
    this.mText.setText(paramString);
    this.mSeekBar.setMax(this.mMax - this.mMin);
  }
  
  public void onClick(View paramView)
  {
    clickEvent((Button)paramView);
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mText = ((TextView)findViewById(2131689514));
    this.mBtnLeft = ((Button)findViewById(2131689515));
    this.mBtnLeft.setOnClickListener(this);
    this.mBtnLeft.setOnLongClickListener(this);
    this.mBtnRight = ((Button)findViewById(2131689517));
    this.mBtnRight.setOnClickListener(this);
    this.mBtnRight.setOnLongClickListener(this);
    this.mSeekBar = ((SeekBar)findViewById(2131689516));
    this.mSeekBar.setOnSeekBarChangeListener(this);
  }
  
  public boolean onLongClick(View paramView)
  {
    this.mClickedButton = ((Button)paramView);
    this.mHandler.post(this.longClickRunnable);
    return true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    paramInt1 = getLayoutParams().width;
    if (paramInt1 != -2)
    {
      LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)this.mSeekBar.getLayoutParams();
      localLayoutParams.width = (paramInt1 - this.mBtnLeft.getLayoutParams().width * 2 - this.mText.getLayoutParams().width - 10);
      paramInt1 = View.MeasureSpec.makeMeasureSpec(localLayoutParams.width, 1073741824);
      this.mSeekBar.measure(paramInt1, paramInt2);
    }
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mValue = (this.mMin + paramInt);
      if (this.mButtonSeekChangeListener == null) {
        break label37;
      }
      this.mButtonSeekChangeListener.onProgressChanged(this, this.mValue, true);
    }
    for (;;)
    {
      return;
      label37:
      Log.w("ButtonSeekBar", "mButtonSeekChangeListener is null");
    }
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {}
  
  public void setOnButtonSeekChangeListener(onButtonSeekChangeListener paramonButtonSeekChangeListener)
  {
    this.mButtonSeekChangeListener = paramonButtonSeekChangeListener;
  }
  
  public void setProgress(int paramInt)
  {
    this.mSeekBar.setProgress(paramInt - this.mMin);
    this.mValue = paramInt;
  }
  
  public static abstract interface onButtonSeekChangeListener
  {
    public abstract void onProgressChanged(ButtonSeekBar paramButtonSeekBar, int paramInt, boolean paramBoolean);
  }
}


