package com.yuneec.curve;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class Expo1LineView
  extends CurveView
{
  private static final int DOUBLE_TOUCH_SPACE = 5;
  private static final int POINT_OFFSET = 30;
  private static final String TAG = "Expo1LineView";
  private static final int TOUCH_LEFT_SIDE = 1;
  private static final int TOUCH_LEFT_SPACE = 3;
  private static final int TOUCH_RIGHT_SIDE = 2;
  private static final int TOUCH_RIGHT_SPACE = 4;
  private boolean isSurfaceReady = false;
  private Point[] mAllPoint;
  private float mB = 0.17F;
  private boolean mChangeCurveExpo = false;
  private boolean mChangeCurveLeftRate = false;
  private boolean mChangeCurveOffset = false;
  private boolean mChangeCurveRightRate = false;
  private float mCurveValue_l;
  private float mCurveValue_r;
  private float mK = 0.67F;
  private float mN = 1.0F;
  private Point mTouchEndP1 = new Point();
  private Point mTouchEndP2 = new Point();
  private Point mTouchStartP1 = new Point();
  private Point mTouchStartP2 = new Point();
  
  public Expo1LineView(Context paramContext)
  {
    super(paramContext);
  }
  
  public Expo1LineView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public Expo1LineView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void ReleaseTouchEvent()
  {
    this.mTouchStartP1.x = 0;
    this.mTouchStartP1.y = 0;
    this.mTouchStartP2.x = 0;
    this.mTouchStartP2.y = 0;
    this.mTouchEndP1.x = 0;
    this.mTouchEndP1.y = 0;
    this.mTouchEndP2.x = 0;
    this.mTouchEndP2.y = 0;
    this.mChangeCurveLeftRate = false;
    this.mChangeCurveRightRate = false;
    this.mChangeCurveExpo = false;
    this.mChangeCurveOffset = false;
  }
  
  public static float convertChannelValueToCurveValue(float paramFloat)
  {
    return 1.0F * paramFloat / 4095.0F;
  }
  
  private void drawCurve()
  {
    int i = this.mAllPoint[0].x;
    int j = this.mAllPoint[0].y;
    Paint localPaint = new Paint();
    localPaint.setAntiAlias(true);
    localPaint.setStyle(Paint.Style.STROKE);
    localPaint.setColor(-65536);
    localPaint.setStrokeWidth(1.0F);
    Path localPath = new Path();
    localPath.moveTo(i, j);
    Canvas localCanvas = getHolder().lockCanvas();
    for (i = 1;; i++)
    {
      if (i >= this.mAllPoint.length)
      {
        localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        this.mCurveCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        this.mCurveCanvas.drawPath(localPath, localPaint);
        localCanvas.drawBitmap(this.mCurveBitmap, 0.0F, 0.0F, null);
        getHolder().unlockCanvasAndPost(localCanvas);
        return;
      }
      localPath.lineTo(this.mAllPoint[i].x, this.mAllPoint[i].y);
    }
  }
  
  private int getTouchSpace(int paramInt1, int paramInt2)
  {
    if (Math.abs(paramInt1 - mAxis.getAxisX()) < 30) {
      paramInt1 = 1;
    }
    for (;;)
    {
      return paramInt1;
      if (Math.abs(paramInt1 - (mAxis.getAxisX() + mAxis_width)) < 30) {
        paramInt1 = 2;
      } else if ((paramInt1 > mAxis.getAxisX()) && (paramInt1 < mAxis.getAxisX() + mAxis_width / 2)) {
        paramInt1 = 3;
      } else {
        paramInt1 = 4;
      }
    }
  }
  
  private void redraw()
  {
    if (this.mOnCurveTouchListener != null)
    {
      this.mOnCurveTouchListener.onCurveValueChanged(1, this.mCurveValue_l);
      this.mOnCurveTouchListener.onCurveValueChanged(2, this.mCurveValue_r);
      this.mOnCurveTouchListener.onCurveValueChanged(3, this.mN);
    }
    this.mK = (this.mCurveValue_r - this.mCurveValue_l);
    this.mB = this.mCurveValue_l;
    if (this.isSurfaceReady)
    {
      drawCurve();
      if (this.mAllPointsUpdateListener != null) {
        this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
      }
    }
  }
  
  private void setEventType(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      Log.w("Expo1LineView", "Invalid touch space");
    }
    for (;;)
    {
      return;
      this.mChangeCurveLeftRate = true;
      continue;
      this.mChangeCurveRightRate = true;
      continue;
      this.mChangeCurveExpo = true;
      continue;
      this.mChangeCurveOffset = false;
    }
  }
  
  public float convertCurveValueToUserValue(float paramFloat)
  {
    return this.mOutputMin + (this.mOutputMax - this.mOutputMin) * paramFloat;
  }
  
  public float convertUserValueToCurveValue(float paramFloat)
  {
    return (paramFloat - this.mOutputMin) * 1.0F / (this.mOutputMax - this.mOutputMin);
  }
  
  public int[] getAllPointsChValue()
  {
    int[] arrayOfInt = new int[this.mAllPoint.length];
    for (int i = 0;; i++)
    {
      if (i >= this.mAllPoint.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = ((mAxis.getAxisY() + mAxis_height - this.mAllPoint[i].y) * 4095 / mAxis_height);
    }
  }
  
  public int[] getAllPointsPosition()
  {
    int[] arrayOfInt = new int[this.mAllPoint.length];
    for (int i = 0;; i++)
    {
      if (i >= this.mAllPoint.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = (mAxis.getAxisY() + mAxis_height - this.mAllPoint[i].y);
    }
  }
  
  public int getValueY(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return (int)(paramInt1 * Math.pow(paramInt4, paramInt2) - paramInt3);
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mAllPoint = new Point[mAxis_width + 1];
    this.mK = (this.mCurveValue_r - this.mCurveValue_l);
    this.mB = this.mCurveValue_l;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onRefresh() {}
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool;
    if (!isEnabled())
    {
      bool = super.onTouchEvent(paramMotionEvent);
      return bool;
    }
    switch (paramMotionEvent.getAction() & 0xFF)
    {
    }
    for (;;)
    {
      bool = true;
      break;
      int j = (int)paramMotionEvent.getX();
      int i = (int)paramMotionEvent.getY();
      setEventType(getTouchSpace(j, i));
      this.mTouchStartP1.x = j;
      this.mTouchStartP1.y = i;
      continue;
      i = (paramMotionEvent.getAction() & 0xFF) >> 8;
      this.mTouchStartP2.x = ((int)paramMotionEvent.getX(i));
      this.mTouchStartP2.y = ((int)paramMotionEvent.getY(i));
      setEventType(5);
      continue;
      float f;
      if (paramMotionEvent.getPointerCount() > 1)
      {
        this.mTouchEndP1.x = ((int)paramMotionEvent.getX(0));
        this.mTouchEndP1.y = ((int)paramMotionEvent.getY(0));
        this.mTouchEndP2.x = ((int)paramMotionEvent.getX(1));
        this.mTouchEndP2.y = ((int)paramMotionEvent.getY(1));
        j = this.mTouchEndP1.y - this.mTouchStartP1.y;
        i = this.mTouchEndP2.y - this.mTouchStartP2.y;
        if ((this.mChangeCurveOffset) && (j * i > 0) && ((Math.abs(j) > 50) || (Math.abs(i) > 50)))
        {
          f = this.mB;
          f = (j + i) / 2;
        }
      }
      label537:
      do
      {
        for (;;)
        {
          redraw();
          this.mTouchStartP1.x = this.mTouchEndP1.x;
          this.mTouchStartP1.y = this.mTouchEndP1.y;
          this.mTouchStartP2.x = this.mTouchEndP2.x;
          this.mTouchStartP2.y = this.mTouchEndP2.y;
          break;
          j = (int)paramMotionEvent.getX(0);
          i = (int)paramMotionEvent.getY(0);
          this.mTouchEndP1.x = j;
          this.mTouchEndP1.y = i;
          j = this.mTouchEndP1.y - this.mTouchStartP1.y;
          if (Math.abs(j) > 10) {
            if (this.mChangeCurveLeftRate)
            {
              this.mCurveValue_l -= j / mAxis_height;
              if (this.mCurveValue_l < 0.0F) {
                this.mCurveValue_l = 0.0F;
              } else if (this.mCurveValue_l > 1.0F) {
                this.mCurveValue_l = 1.0F;
              }
            }
            else
            {
              if (!this.mChangeCurveRightRate) {
                break label537;
              }
              this.mCurveValue_r -= j / mAxis_height;
              if (this.mCurveValue_r < 0.0F) {
                this.mCurveValue_r = 0.0F;
              } else if (this.mCurveValue_r > 1.0F) {
                this.mCurveValue_r = 1.0F;
              }
            }
          }
        }
      } while (!this.mChangeCurveExpo);
      if (this.mN > 1.0F) {}
      for (i = 1000593162;; i = 981668463)
      {
        f = this.mN;
        f = j;
        break;
      }
      ReleaseTouchEvent();
    }
  }
  
  public void setParams(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.mCurveValue_l = convertUserValueToCurveValue(paramFloat1);
    this.mCurveValue_r = convertUserValueToCurveValue(paramFloat2);
    redraw();
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    super.surfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    drawCurve();
    this.isSurfaceReady = true;
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceCreated(paramSurfaceHolder);
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceDestroyed(paramSurfaceHolder);
    this.isSurfaceReady = false;
  }
}


