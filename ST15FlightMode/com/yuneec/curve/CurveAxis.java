package com.yuneec.curve;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class CurveAxis
  extends Drawable
{
  public static final int AXIS_LEFT_OFFSET = 40;
  public static final int AXIS_TOP_OFFSET = 10;
  private static final String[] horizontal_ordinate = { "-100", "-50", "0", "50", "100" };
  private int mAxisY_max;
  private int mAxisY_min;
  private String[] mAxisY_value = null;
  private boolean mCoordinateDisplayed;
  private int mHorLine;
  private Point[] mHorLinesPoint = null;
  private boolean mIsSwitch;
  private int mLonLine;
  private Point[] mLonLinesPoint = null;
  private int mX;
  private int mXLength;
  private int mY;
  private int mYLength;
  
  public CurveAxis(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.mCoordinateDisplayed = paramBoolean1;
    if (this.mCoordinateDisplayed) {
      this.mX = (paramInt1 + 40);
    }
    for (this.mY = (paramInt2 + 10);; this.mY = 1)
    {
      this.mIsSwitch = paramBoolean2;
      this.mXLength = paramInt3;
      this.mYLength = paramInt4;
      this.mHorLine = paramInt5;
      this.mLonLine = paramInt6;
      this.mAxisY_max = paramInt7;
      this.mAxisY_min = paramInt8;
      this.mHorLinesPoint = new Point[paramInt5];
      this.mLonLinesPoint = new Point[paramInt6];
      this.mAxisY_value = new String[paramInt5];
      countLinePosition(this.mHorLine, this.mLonLine);
      return;
      this.mX = 1;
    }
  }
  
  public CurveAxis(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.mCoordinateDisplayed = paramBoolean1;
    if (this.mCoordinateDisplayed) {
      this.mX = (paramInt1 + 40);
    }
    for (this.mY = (paramInt2 + 10);; this.mY = 1)
    {
      this.mIsSwitch = paramBoolean2;
      this.mXLength = paramInt3;
      this.mYLength = paramInt3;
      this.mHorLine = paramInt4;
      this.mLonLine = paramInt4;
      this.mAxisY_max = paramInt5;
      this.mAxisY_min = paramInt6;
      this.mHorLinesPoint = new Point[paramInt4];
      this.mLonLinesPoint = new Point[paramInt4];
      this.mAxisY_value = new String[paramInt4];
      countLinePosition(this.mHorLine, this.mLonLine);
      return;
      this.mX = 1;
    }
  }
  
  private void countLinePosition(int paramInt1, int paramInt2)
  {
    paramInt2 = this.mY;
    int i = this.mX;
    int j = this.mAxisY_max;
    paramInt1 = 0;
    if (paramInt1 >= this.mHorLinesPoint.length)
    {
      paramInt1 = 0;
      paramInt2 = i;
    }
    for (;;)
    {
      if (paramInt1 >= this.mLonLinesPoint.length)
      {
        return;
        this.mHorLinesPoint[paramInt1] = new Point();
        this.mHorLinesPoint[paramInt1].x = this.mX;
        this.mHorLinesPoint[paramInt1].y = paramInt2;
        paramInt2 += this.mYLength / (this.mHorLine - 1);
        this.mAxisY_value[paramInt1] = new String();
        this.mAxisY_value[paramInt1] = String.valueOf(j - (this.mAxisY_max - this.mAxisY_min) * paramInt1 / (this.mHorLine - 1));
        paramInt1++;
        break;
      }
      this.mLonLinesPoint[paramInt1] = new Point();
      this.mLonLinesPoint[paramInt1].x = paramInt2;
      this.mLonLinesPoint[paramInt1].y = this.mY;
      paramInt2 += this.mXLength / (this.mLonLine - 1);
      paramInt1++;
    }
  }
  
  private int getStringWidth(String paramString, Paint paramPaint)
  {
    Rect localRect = new Rect();
    paramPaint.getTextBounds(paramString, 0, paramString.length(), localRect);
    return localRect.width();
  }
  
  public void draw(Canvas paramCanvas)
  {
    Paint localPaint = new Paint();
    localPaint.setAntiAlias(true);
    localPaint.setStyle(Paint.Style.STROKE);
    int j = this.mY + this.mYLength - 1;
    int i = 0;
    int k;
    label103:
    int m;
    if (i >= this.mLonLine)
    {
      k = this.mX;
      j = this.mXLength;
      i = 0;
      if (i < this.mHorLine) {}
    }
    else
    {
      if ((i == 0) || (i == this.mLonLine / 2) || (i == this.mLonLine - 1))
      {
        localPaint.setColor(-1);
        localPaint.setStrokeWidth(1.0F);
        k = this.mLonLinesPoint[i].x;
        m = this.mLonLinesPoint[i].y;
        paramCanvas.drawLine(k, m, k, j, localPaint);
        if (this.mCoordinateDisplayed)
        {
          if (!this.mIsSwitch) {
            break label290;
          }
          if (i != 0) {
            break label210;
          }
          paramCanvas.drawText("0", k - getStringWidth("0", localPaint) / 2, j + 20, localPaint);
        }
      }
      for (;;)
      {
        i++;
        break;
        localPaint.setColor(-7829368);
        localPaint.setStrokeWidth(0.8F);
        break label103;
        label210:
        if (i == this.mLonLine / 2)
        {
          paramCanvas.drawText("1", k - getStringWidth("1", localPaint) / 2, j + 20, localPaint);
        }
        else if (i == this.mLonLine - 1)
        {
          paramCanvas.drawText("2", k - getStringWidth("2", localPaint) / 2, j + 20, localPaint);
          continue;
          label290:
          m = getStringWidth(horizontal_ordinate[i], localPaint);
          paramCanvas.drawText(horizontal_ordinate[i], k - m / 2, j + 20, localPaint);
        }
      }
    }
    if ((i == 0) || (i == this.mHorLine / 2) || (i == this.mHorLine - 1))
    {
      localPaint.setColor(-1);
      localPaint.setStrokeWidth(1.0F);
    }
    for (;;)
    {
      m = this.mHorLinesPoint[i].x;
      int n = this.mHorLinesPoint[i].y;
      paramCanvas.drawLine(m, n, k + j, n, localPaint);
      if (this.mCoordinateDisplayed)
      {
        int i1 = getStringWidth(this.mAxisY_value[i], localPaint);
        paramCanvas.drawText(this.mAxisY_value[i], m - i1 - 10, n + 3, localPaint);
      }
      i++;
      break;
      localPaint.setColor(-7829368);
      localPaint.setStrokeWidth(0.8F);
    }
  }
  
  public int getAxisX()
  {
    return this.mX;
  }
  
  public int getAxisY()
  {
    return this.mY;
  }
  
  public int getHeight()
  {
    return this.mYLength;
  }
  
  public Point[] getHorLinesPosition()
  {
    return this.mHorLinesPoint;
  }
  
  public int getOpacity()
  {
    return 0;
  }
  
  public int getWidth()
  {
    return this.mXLength;
  }
  
  public void setAlpha(int paramInt) {}
  
  public void setColorFilter(ColorFilter paramColorFilter) {}
}


