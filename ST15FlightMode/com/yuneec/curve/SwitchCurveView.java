package com.yuneec.curve;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.view.SurfaceHolder;

public class SwitchCurveView
  extends CurveView
{
  private static final String TAG = "SwitchCurveView";
  
  public SwitchCurveView(Context paramContext)
  {
    super(paramContext);
  }
  
  public SwitchCurveView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SwitchCurveView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void drawCurve()
  {
    Canvas localCanvas = getHolder().lockCanvas();
    this.mCurveCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
    localCanvas.drawBitmap(this.mCurveBitmap, 0.0F, 0.0F, null);
    getHolder().unlockCanvasAndPost(localCanvas);
    onCurveChanges();
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onRefresh() {}
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    super.surfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    drawCurve();
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceCreated(paramSurfaceHolder);
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceDestroyed(paramSurfaceHolder);
  }
}


