package com.yuneec.curve;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.yuneec.flightmode15.Utilities;

public class Expo2LineView
  extends CurveView
{
  private static final int DOUBLE_TOUCH_SPACE = 5;
  private static final int POINT_OFFSET = 30;
  private static final String TAG = "Expo2LineView";
  private static final int TOUCH_LEFT_SIDE = 1;
  private static final int TOUCH_LEFT_SPACE = 3;
  private static final int TOUCH_RIGHT_SIDE = 2;
  private static final int TOUCH_RIGHT_SPACE = 4;
  private boolean isSurfaceReady = false;
  private Point[] mAllPoint;
  private float mB;
  private boolean mChangeCurveLeftExpo = false;
  private boolean mChangeCurveLeftRate = false;
  private boolean mChangeCurveRightExpo = false;
  private boolean mChangeCurveRightRate = false;
  private boolean mChangeWholeCurveExpo = false;
  private boolean mChangeWholeCurveOffset = false;
  private boolean mChangeWholeCurveRate = false;
  private Paint mCurrentPaint;
  private float mCurveValue_l;
  private float mCurveValue_r;
  private float mK1;
  private float mK2;
  private float mN1;
  private float mN2;
  private Paint mPaint;
  private Paint mPaint1;
  private Paint mPaint2;
  private boolean mSeparate = false;
  private Point mTouchEndP1 = new Point();
  private Point mTouchEndP2 = new Point();
  private Point mTouchStartP1 = new Point();
  private Point mTouchStartP2 = new Point();
  
  public Expo2LineView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public Expo2LineView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public Expo2LineView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void ReleaseTouchEvent()
  {
    this.mTouchStartP1.x = 0;
    this.mTouchStartP1.y = 0;
    this.mTouchStartP2.x = 0;
    this.mTouchStartP2.y = 0;
    this.mTouchEndP1.x = 0;
    this.mTouchEndP1.y = 0;
    this.mTouchEndP2.x = 0;
    this.mTouchEndP2.y = 0;
    this.mChangeCurveLeftRate = false;
    this.mChangeCurveRightRate = false;
    this.mChangeCurveLeftExpo = false;
    this.mChangeCurveRightExpo = false;
    this.mChangeWholeCurveRate = false;
    this.mChangeWholeCurveExpo = false;
    this.mChangeWholeCurveOffset = false;
  }
  
  public static float convertChannelValueToCurveValue(float paramFloat)
  {
    float f2 = 0.0F;
    float f1;
    if ((paramFloat >= 0.0F) && (paramFloat < 2048.0F)) {
      f1 = (2048.0F - paramFloat) * 1.0F / 2048.0F;
    }
    for (;;)
    {
      return f1;
      f1 = f2;
      if (paramFloat >= 2048.0F)
      {
        f1 = f2;
        if (paramFloat <= 4095.0F) {
          f1 = (paramFloat - 2048.0F) * 1.0F / 2048.0F;
        }
      }
    }
  }
  
  public static float convertUserValueToCurveValueL(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return 1.0F - 2.0F * (paramFloat3 - paramFloat2) / (paramFloat1 - paramFloat2);
  }
  
  public static float convertUserValueToCurveValueR(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return 2.0F * (paramFloat3 - paramFloat2) / (paramFloat1 - paramFloat2) - 1.0F;
  }
  
  public static int[] getAllPointsChValue(Context paramContext, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7)
  {
    int i = (int)paramContext.getResources().getDimension(2131558422);
    int j = (int)paramContext.getResources().getDimension(2131558423);
    paramContext = Utilities.countAllPointOfExpo2(40, 10, i, j, convertUserValueToCurveValueL(paramFloat1, paramFloat2, paramFloat3) + paramFloat7, convertUserValueToCurveValueR(paramFloat1, paramFloat2, paramFloat4) - paramFloat7, Utilities.convertExpoToCoefficientN(paramFloat5), Utilities.convertExpoToCoefficientN(paramFloat6), paramFloat7);
    int[] arrayOfInt = new int[paramContext.length];
    for (i = 0;; i++)
    {
      if (i >= paramContext.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = ((10 + j - paramContext[i].y) * 4095 / j);
    }
  }
  
  private int getTouchSpace(int paramInt1, int paramInt2)
  {
    if (Math.abs(paramInt1 - mAxis.getAxisX()) < 30) {
      paramInt1 = 1;
    }
    for (;;)
    {
      return paramInt1;
      if (Math.abs(paramInt1 - (mAxis.getAxisX() + mAxis_width)) < 30) {
        paramInt1 = 2;
      } else if ((paramInt1 > mAxis.getAxisX()) && (paramInt1 < mAxis.getAxisX() + mAxis_width / 2)) {
        paramInt1 = 3;
      } else {
        paramInt1 = 4;
      }
    }
  }
  
  private void redraw()
  {
    if (this.mOnCurveTouchListener != null)
    {
      this.mOnCurveTouchListener.onCurveValueChanged(1, this.mCurveValue_l);
      this.mOnCurveTouchListener.onCurveValueChanged(2, this.mCurveValue_r);
      this.mOnCurveTouchListener.onCurveValueChanged(5, this.mN1);
      this.mOnCurveTouchListener.onCurveValueChanged(6, this.mB);
    }
    this.mAllPoint = Utilities.countAllPointOfExpo2(mAxis.getAxisX(), mAxis.getAxisY(), mAxis_width, mAxis_height, this.mK1, this.mK2, this.mN1, this.mN2, this.mB);
    if (this.isSurfaceReady)
    {
      drawCurve();
      if (this.mAllPointsUpdateListener != null) {
        this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
      }
    }
  }
  
  private void setEventType(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      Log.w("Expo2LineView", "Invalid touch space");
    }
    for (;;)
    {
      return;
      this.mChangeCurveLeftRate = true;
      continue;
      this.mChangeCurveRightRate = true;
      continue;
      if (this.mSeparate)
      {
        this.mChangeCurveLeftExpo = true;
      }
      else
      {
        this.mChangeWholeCurveExpo = true;
        continue;
        if (this.mSeparate)
        {
          this.mChangeCurveRightExpo = true;
        }
        else
        {
          this.mChangeWholeCurveExpo = true;
          continue;
          this.mChangeWholeCurveOffset = true;
        }
      }
    }
  }
  
  public void changeCurvePaint(Paint paramPaint)
  {
    this.mCurrentPaint = paramPaint;
  }
  
  public float convertCurveValueToUserValue(float paramFloat)
  {
    return this.mOutputMin + (1.0F + paramFloat) * (this.mOutputMax - this.mOutputMin) / 2.0F;
  }
  
  public float convertCurveValueToUserValueL(float paramFloat)
  {
    return this.mOutputMin + (1.0F - paramFloat) * (this.mOutputMax - this.mOutputMin) / 2.0F;
  }
  
  public float convertCurveValueToUserValueR(float paramFloat)
  {
    return this.mOutputMin + (1.0F + paramFloat) * (this.mOutputMax - this.mOutputMin) / 2.0F;
  }
  
  public float convertUserValueToCurveValue(float paramFloat)
  {
    return 2.0F * (paramFloat - this.mOutputMin) / (this.mOutputMax - this.mOutputMin) - 1.0F;
  }
  
  public void drawCurve()
  {
    int j = this.mAllPoint[0].x;
    int m = this.mAllPoint[0].y;
    int k = this.mAllPoint[(this.mAllPoint.length / 2)].x;
    int i = this.mAllPoint[(this.mAllPoint.length / 2)].y;
    Canvas localCanvas = getHolder().lockCanvas();
    if (!this.mSeparate)
    {
      localPath1 = new Path();
      localPath1.moveTo(j, m);
      for (i = 1;; i++)
      {
        if (i >= this.mAllPoint.length - 1)
        {
          localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
          this.mCurveCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
          this.mCurveCanvas.drawPath(localPath1, this.mCurrentPaint);
          localCanvas.drawBitmap(this.mCurveBitmap, 0.0F, 0.0F, null);
          getHolder().unlockCanvasAndPost(localCanvas);
          return;
        }
        localPath1.lineTo(this.mAllPoint[i].x, this.mAllPoint[i].y);
      }
    }
    Path localPath2 = new Path();
    localPath2.moveTo(j, m);
    Path localPath1 = new Path();
    localPath1.moveTo(k, i);
    j = 1;
    for (i = this.mAllPoint.length / 2;; i++)
    {
      if (j >= this.mAllPoint.length / 2)
      {
        localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        this.mCurveCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        this.mCurveCanvas.drawPath(localPath2, this.mPaint1);
        this.mCurveCanvas.drawPath(localPath1, this.mPaint2);
        break;
      }
      localPath2.lineTo(this.mAllPoint[j].x, this.mAllPoint[j].y);
      localPath1.lineTo(this.mAllPoint[i].x, this.mAllPoint[i].y);
      j++;
    }
  }
  
  public int[] getAllPointsPosition()
  {
    int[] arrayOfInt = new int[this.mAllPoint.length];
    for (int i = 0;; i++)
    {
      if (i >= this.mAllPoint.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = (mAxis.getAxisY() + mAxis_height - this.mAllPoint[i].y);
    }
  }
  
  public float getLeftExpoValue(float paramFloat)
  {
    return Utilities.convertCoefficientNtoExpo(paramFloat);
  }
  
  public float[] getPointForFlightControl()
  {
    return null;
  }
  
  public float getRightExpoValue(float paramFloat)
  {
    return Utilities.convertCoefficientNtoExpo(paramFloat);
  }
  
  public int getValueY(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return (int)(paramInt1 * Math.pow(paramInt4, paramInt2) - paramInt3);
  }
  
  public boolean isSeparated()
  {
    return this.mSeparate;
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mPaint = new Paint();
    this.mPaint.setAntiAlias(true);
    this.mPaint.setStyle(Paint.Style.STROKE);
    this.mPaint.setColor(-65536);
    this.mPaint.setStrokeWidth(1.0F);
    this.mPaint1 = new Paint();
    this.mPaint1.setAntiAlias(true);
    this.mPaint1.setStyle(Paint.Style.STROKE);
    this.mPaint1.setColor(-65536);
    this.mPaint1.setStrokeWidth(1.0F);
    this.mPaint2 = new Paint();
    this.mPaint2.setAntiAlias(true);
    this.mPaint2.setStyle(Paint.Style.STROKE);
    this.mPaint2.setColor(65280);
    this.mPaint2.setStrokeWidth(1.0F);
    this.mCurrentPaint = this.mPaint;
    this.mAllPoint = new Point[mAxis_width + 1];
    this.mAllPoint = Utilities.countAllPointOfExpo2(mAxis.getAxisX(), mAxis.getAxisY(), mAxis_width, mAxis_height, this.mK1, this.mK2, this.mN1, this.mN2, this.mB);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onRefresh() {}
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool;
    if (!isEnabled())
    {
      bool = super.onTouchEvent(paramMotionEvent);
      return bool;
    }
    switch (paramMotionEvent.getAction() & 0xFF)
    {
    }
    for (;;)
    {
      bool = true;
      break;
      int i = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      setEventType(getTouchSpace(i, j));
      this.mTouchStartP1.x = i;
      this.mTouchStartP1.y = j;
      continue;
      i = (paramMotionEvent.getAction() & 0xFF) >> 8;
      this.mTouchStartP2.x = ((int)paramMotionEvent.getX(i));
      this.mTouchStartP2.y = ((int)paramMotionEvent.getY(i));
      setEventType(5);
      continue;
      if (paramMotionEvent.getPointerCount() > 1)
      {
        this.mTouchEndP1.x = ((int)paramMotionEvent.getX(0));
        this.mTouchEndP1.y = ((int)paramMotionEvent.getY(0));
        this.mTouchEndP2.x = ((int)paramMotionEvent.getX(1));
        this.mTouchEndP2.y = ((int)paramMotionEvent.getY(1));
        i = this.mTouchEndP1.y - this.mTouchStartP1.y;
        j = this.mTouchEndP2.y - this.mTouchStartP2.y;
        if ((i * j > 0) && ((Math.abs(i) > 10) || (Math.abs(j) > 10)))
        {
          float f3 = this.mB - (i + j) * 0.05F / mAxis_height;
          f1 = this.mK1 - f3;
          float f2 = this.mK2 + f3;
          if ((f1 >= 0.0F) && (f1 <= 1.0F) && (f2 >= 0.0F) && (f2 <= 1.0F))
          {
            this.mB = f3;
            this.mCurveValue_l = f1;
            this.mCurveValue_r = f2;
          }
        }
      }
      label532:
      label613:
      label672:
      label753:
      do
      {
        do
        {
          redraw();
          this.mTouchStartP1.x = this.mTouchEndP1.x;
          this.mTouchStartP1.y = this.mTouchEndP1.y;
          this.mTouchStartP2.x = this.mTouchEndP2.x;
          this.mTouchStartP2.y = this.mTouchEndP2.y;
          break;
          i = (int)paramMotionEvent.getX(0);
          j = (int)paramMotionEvent.getY(0);
          this.mTouchEndP1.x = i;
          this.mTouchEndP1.y = j;
          i = this.mTouchEndP1.y - this.mTouchStartP1.y;
        } while (Math.abs(i) <= 10);
        if (this.mChangeCurveLeftRate)
        {
          this.mCurveValue_l += i / mAxis_height;
          if (this.mCurveValue_l < -1.0F)
          {
            this.mCurveValue_l = -1.0F;
            this.mCurveValue_r += i / mAxis_height;
            if (this.mCurveValue_r >= -1.0F) {
              break label613;
            }
            this.mCurveValue_r = -1.0F;
          }
          for (;;)
          {
            this.mK1 = (this.mCurveValue_l + this.mB);
            this.mK2 = (this.mCurveValue_r - this.mB);
            break;
            if (this.mCurveValue_l <= 1.0F) {
              break label532;
            }
            this.mCurveValue_l = 1.0F;
            break label532;
            if (this.mCurveValue_r > 1.0F) {
              this.mCurveValue_r = 1.0F;
            }
          }
        }
        if (this.mChangeCurveRightRate)
        {
          this.mCurveValue_l -= i / mAxis_height;
          if (this.mCurveValue_l < -1.0F)
          {
            this.mCurveValue_l = -1.0F;
            this.mCurveValue_r -= i / mAxis_height;
            if (this.mCurveValue_r >= -1.0F) {
              break label753;
            }
            this.mCurveValue_r = -1.0F;
          }
          for (;;)
          {
            this.mK1 = (this.mCurveValue_l + this.mB);
            this.mK2 = (this.mCurveValue_r - this.mB);
            break;
            if (this.mCurveValue_l <= 1.0F) {
              break label672;
            }
            this.mCurveValue_l = 1.0F;
            break label672;
            if (this.mCurveValue_r > 1.0F) {
              this.mCurveValue_r = 1.0F;
            }
          }
        }
        if (this.mChangeCurveLeftExpo)
        {
          if (this.mN1 > 1.0F) {}
          for (f1 = 0.001F;; f1 = 0.005F)
          {
            f1 = this.mN1 - i * f1;
            if ((f1 < 0.3F) || (f1 > 4.0F)) {
              break;
            }
            this.mN1 = f1;
            break;
          }
        }
        if (this.mChangeCurveRightExpo)
        {
          if (this.mN2 > 1.0F) {}
          for (f1 = 0.005F;; f1 = 0.001F)
          {
            f1 = this.mN2 + i * f1;
            if ((f1 < 0.3F) || (f1 > 4.0F)) {
              break;
            }
            this.mN2 = f1;
            break;
          }
        }
      } while (!this.mChangeWholeCurveExpo);
      if (this.mN1 > 1.0F) {}
      for (float f1 = 0.005F;; f1 = 0.001F)
      {
        f1 = this.mN1 + i * f1;
        if ((f1 < 0.3F) || (f1 > 4.0F)) {
          break;
        }
        this.mN2 = f1;
        this.mN1 = f1;
        break;
      }
      ReleaseTouchEvent();
    }
  }
  
  public void setParams(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
  {
    this.mCurveValue_l = convertUserValueToCurveValueL(this.mOutputMax, this.mOutputMin, paramFloat1);
    this.mCurveValue_r = convertUserValueToCurveValueR(this.mOutputMax, this.mOutputMin, paramFloat2);
    this.mN1 = Utilities.convertExpoToCoefficientN(paramFloat3);
    this.mN2 = Utilities.convertExpoToCoefficientN(paramFloat4);
    this.mB = paramFloat5;
    this.mK1 = (this.mCurveValue_l + this.mB);
    this.mK2 = (this.mCurveValue_r - this.mB);
    redraw();
  }
  
  public void setSeparate(boolean paramBoolean)
  {
    this.mSeparate = paramBoolean;
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    super.surfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    drawCurve();
    this.isSurfaceReady = true;
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceCreated(paramSurfaceHolder);
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceDestroyed(paramSurfaceHolder);
    this.isSurfaceReady = false;
  }
}


