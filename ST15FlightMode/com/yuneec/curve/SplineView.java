package com.yuneec.curve;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class SplineView
  extends CurveView
{
  private static final int POINT_OFFSET = 25;
  private static final String TAG = "SplineView";
  private static int mKeyPointNum;
  private boolean isSurfaceReady = false;
  private int[] mAllPoint;
  private float[] mKeyPointRelativeX;
  private float[] mKeyPointRelativeY;
  private int[] mKeyPointX;
  private int[] mKeyPointY;
  private int mMaxX;
  private int mMaxY;
  private int mMinX;
  private int mMinY;
  public boolean mSeparate = false;
  private boolean mTouchEnable = false;
  private int mTouchPoint;
  
  public SplineView(Context paramContext)
  {
    super(paramContext);
  }
  
  public SplineView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SplineView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void clearDraw()
  {
    Canvas localCanvas = getHolder().lockCanvas(null);
    localCanvas.drawColor(-16777216);
    getHolder().unlockCanvasAndPost(localCanvas);
  }
  
  public static Point[] countAllPointOfSpline(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    Point[] arrayOfPoint = new Point[paramInt3 + 1];
    int[] arrayOfInt;
    if ((paramArrayOfInt1 != null) && (paramArrayOfInt2 != null))
    {
      int i = paramArrayOfInt1[(paramArrayOfInt1.length - 1)];
      int j = paramArrayOfInt1[0];
      arrayOfInt = new int[i];
      JniTools.getCurvePointFromJNI(paramArrayOfInt1, paramArrayOfInt2, i, j, paramInt2 + paramInt4, paramInt2, arrayOfInt);
    }
    for (paramInt2 = 0;; paramInt2++)
    {
      if (paramInt2 > paramInt3) {
        return arrayOfPoint;
      }
      arrayOfPoint[paramInt2] = new Point();
      arrayOfPoint[paramInt2].x = (paramInt1 + paramInt2);
      arrayOfPoint[paramInt2].y = arrayOfInt[paramInt2];
    }
  }
  
  private boolean countAllPoints()
  {
    return JniTools.getCurvePointFromJNI(this.mKeyPointX, this.mKeyPointY, this.mMaxX, this.mMinX, this.mMaxY, this.mMinY, this.mAllPoint);
  }
  
  private void countKeyPointsRealPosition()
  {
    if ((this.mKeyPointRelativeX == null) || (this.mKeyPointRelativeY == null)) {
      Log.w("SplineView", "Axis has not been inited");
    }
    for (;;)
    {
      return;
      if ((this.mKeyPointRelativeX.length != mKeyPointNum) || (this.mKeyPointRelativeY.length != mKeyPointNum)) {
        Log.w("SplineView", "Key points value is wrong");
      } else {
        for (int i = 0; i < mKeyPointNum; i++)
        {
          this.mKeyPointX[i] = countRealPositonX(this.mKeyPointRelativeX[i]);
          this.mKeyPointY[i] = countRealPositonY(this.mKeyPointRelativeY[i]);
        }
      }
    }
  }
  
  private void curveDraw()
  {
    if ((this.mKeyPointX.length != 5) || (this.mKeyPointY.length != 5) || (this.mAllPoint.length == 0)) {
      Log.e("SplineView", "Points error");
    }
    Paint localPaint2;
    Paint localPaint1;
    Path localPath;
    Canvas localCanvas;
    for (;;)
    {
      return;
      i = this.mKeyPointX[0];
      j = this.mKeyPointY[0];
      localPaint2 = new Paint();
      localPaint2.setAntiAlias(true);
      localPaint2.setStyle(Paint.Style.STROKE);
      localPaint2.setColor(-65536);
      localPaint2.setStrokeWidth(1.0F);
      localPaint1 = new Paint();
      localPaint1.setAntiAlias(true);
      localPaint1.setStyle(Paint.Style.FILL);
      localPaint1.setShadowLayer(7.0F, 0.0F, 0.0F, -16711936);
      localPaint1.setColor(-16711936);
      localPaint1.setStrokeWidth(1.0F);
      localPaint1.setTextSize(14.0F);
      localPath = new Path();
      localPath.moveTo(i, j);
      localCanvas = getHolder().lockCanvas();
      if (localCanvas != null) {
        break;
      }
      Log.w("SplineView", "Surface not ready,abort draw");
    }
    int j = i + 1;
    int i = 1;
    label183:
    if (j >= this.mMaxX)
    {
      localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      this.mCurveCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      if (!this.mKeyPointDisplayed) {}
    }
    for (i = 0;; i++)
    {
      if (i >= this.mKeyPointX.length)
      {
        this.mCurveCanvas.drawPath(localPath, localPaint2);
        localCanvas.drawBitmap(this.mCurveBitmap, 0.0F, 0.0F, null);
        getHolder().unlockCanvasAndPost(localCanvas);
        break;
        localPath.lineTo(j, this.mAllPoint[i]);
        j++;
        i++;
        break label183;
      }
      this.mCurveCanvas.drawCircle(this.mKeyPointX[i], this.mKeyPointY[i], 4.0F, localPaint1);
      this.mCurveCanvas.drawText(String.valueOf(i + 1), this.mKeyPointX[i] + 7.0F, this.mKeyPointY[i] - 7.0F, localPaint1);
    }
  }
  
  public static int[] getAllPointsChValue(Context paramContext, float paramFloat1, float paramFloat2, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    int i = (int)paramContext.getResources().getDimension(2131558422);
    int j = (int)paramContext.getResources().getDimension(2131558423);
    paramArrayOfFloat1 = countAllPointOfSpline(40, 10, i, j, convertValueXtoPosition(j, i, paramArrayOfFloat1), convertValueYtoPosition(10, j, paramFloat1, paramFloat2, paramArrayOfFloat2));
    paramContext = new int[paramArrayOfFloat1.length];
    for (i = 0;; i++)
    {
      if (i >= paramArrayOfFloat1.length) {
        return paramContext;
      }
      paramContext[i] = ((10 + j - paramArrayOfFloat1[i].y) * 4095 / j);
    }
  }
  
  private void initKeyPointsPosition()
  {
    mKeyPointNum = this.mAxis_lon_lines;
    this.mKeyPointX = new int[mKeyPointNum];
    this.mKeyPointY = new int[mKeyPointNum];
    countKeyPointsRealPosition();
  }
  
  private void redraw()
  {
    countKeyPointsRealPosition();
    countAllPoints();
    if (this.isSurfaceReady)
    {
      curveDraw();
      if (this.mAllPointsUpdateListener != null) {
        this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
      }
    }
  }
  
  public void finish() {}
  
  public int[] getAllPointsPosition()
  {
    int[] arrayOfInt = new int[this.mAllPoint.length];
    for (int i = 0;; i++)
    {
      if (i >= this.mAllPoint.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = (mAxis.getAxisY() + mAxis_height - this.mAllPoint[i]);
    }
  }
  
  public float getPointValue(int paramInt)
  {
    return this.mOutputMin + (mAxis.getAxisY() + mAxis_height - this.mKeyPointY[paramInt]) * (this.mOutputMax - this.mOutputMin) / mAxis_height;
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    initKeyPointsPosition();
    this.mMaxX = (mAxis.getAxisX() + mAxis_width);
    this.mMinX = mAxis.getAxisX();
    this.mMaxY = (mAxis.getAxisY() + mAxis_height);
    this.mMinY = mAxis.getAxisY();
    this.mAllPoint = new int[mAxis_width + 1];
    if ((this.mKeyPointX != null) && (this.mKeyPointY != null))
    {
      if ((this.mKeyPointX.length == 5) && (this.mKeyPointY.length == 5)) {
        break label109;
      }
      Log.e("SplineView", "Point number error!");
    }
    for (;;)
    {
      return;
      label109:
      countAllPoints();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onRefresh() {}
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((this.mKeyPointX == null) || (this.mKeyPointY == null))
    {
      Log.e("SplineView", "Axis has not been inited");
      bool1 = false;
    }
    for (;;)
    {
      return bool1;
      if (!isEnabled())
      {
        bool1 = super.onTouchEvent(paramMotionEvent);
      }
      else
      {
        Point localPoint = new Point();
        localPoint.x = ((int)paramMotionEvent.getX());
        localPoint.y = ((int)paramMotionEvent.getY());
        int i;
        if (paramMotionEvent.getAction() == 0)
        {
          i = 0;
          label83:
          if (i < this.mKeyPointX.length) {}
        }
        else
        {
          if (paramMotionEvent.getAction() == 1) {
            this.mTouchEnable = false;
          }
          bool1 = bool2;
          if (paramMotionEvent.getAction() != 2) {
            continue;
          }
          bool1 = bool2;
          if (!this.mTouchEnable) {
            continue;
          }
          i = this.mKeyPointY[this.mTouchPoint];
          this.mKeyPointY[this.mTouchPoint] = ((int)paramMotionEvent.getY());
          if (this.mKeyPointY[this.mTouchPoint] <= this.mMaxY) {
            break label258;
          }
          this.mKeyPointY[this.mTouchPoint] = this.mMaxY;
        }
        for (;;)
        {
          if (countAllPoints()) {
            break label290;
          }
          this.mKeyPointY[this.mTouchPoint] = i;
          bool1 = bool2;
          break;
          if ((Math.abs(this.mKeyPointX[i] - localPoint.x) < 25) && (Math.abs(this.mKeyPointY[i] - localPoint.y) < 25))
          {
            this.mTouchEnable = true;
            this.mTouchPoint = i;
          }
          i++;
          break label83;
          label258:
          if (this.mKeyPointY[this.mTouchPoint] < this.mMinY) {
            this.mKeyPointY[this.mTouchPoint] = this.mMinY;
          }
        }
        label290:
        curveDraw();
        if (this.mOnCurveTouchListener != null) {
          this.mOnCurveTouchListener.onCurveValueChanged(this.mTouchPoint);
        }
        bool1 = bool2;
        if (this.mAllPointsUpdateListener != null)
        {
          this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
          bool1 = bool2;
        }
      }
    }
  }
  
  public void setParams(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    if (paramArrayOfFloat1.length != paramArrayOfFloat2.length) {
      Log.e("SplineView", "input xy not matched");
    }
    for (;;)
    {
      return;
      if (paramArrayOfFloat1.length < 3)
      {
        Log.e("SplineView", "xy length must larger than 3");
      }
      else
      {
        this.mKeyPointRelativeX = paramArrayOfFloat1;
        this.mKeyPointRelativeY = paramArrayOfFloat2;
        redraw();
      }
    }
  }
  
  public void setPointValue(int paramInt1, int paramInt2)
  {
    this.mKeyPointY[paramInt1] = (mAxis.getAxisY() + mAxis_height - (paramInt2 - this.mOutputMin) * mAxis_height / (this.mOutputMax - this.mOutputMin));
    countAllPoints();
    redraw();
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    super.surfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    curveDraw();
    this.isSurfaceReady = true;
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceCreated(paramSurfaceHolder);
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceDestroyed(paramSurfaceHolder);
    this.isSurfaceReady = false;
    this.mKeyPointRelativeX = null;
    this.mKeyPointRelativeY = null;
  }
}


