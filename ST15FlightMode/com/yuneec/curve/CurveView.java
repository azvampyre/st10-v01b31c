package com.yuneec.curve;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import com.yuneec.flightmode15.R.styleable;

public abstract class CurveView
  extends SurfaceView
  implements SurfaceHolder.Callback
{
  public static final int COE_TYPE_EXPO_L = 3;
  public static final int COE_TYPE_EXPO_R = 4;
  public static final int COE_TYPE_EXPO_W = 5;
  public static final int COE_TYPE_OFFSET = 6;
  public static final int COE_TYPE_RATE_L = 1;
  public static final int COE_TYPE_RATE_R = 2;
  private static final int MSG_DRAW_COORDINATE_X = 2;
  private static final int MSG_DRAW_COORDINATE_Y = 3;
  private static final int MSG_DRAW_CURVE = 0;
  private static final int MSG_DRAW_POINTS = 4;
  private static final int MSG_DRAW_STICK = 1;
  private static final String TAG = "CurveView";
  protected static CurveAxis mAxis;
  protected static int mAxis_height;
  protected static int mAxis_width;
  protected Point[] mAllPoint;
  protected onAllPointsUpdateListener mAllPointsUpdateListener;
  protected int mAxis_hor_lines;
  protected int mAxis_lon_lines;
  protected boolean mCoordinateDisplayed;
  protected Bitmap mCurveBitmap;
  protected Canvas mCurveCanvas;
  private DrawerThread mDrawer;
  private EarlyMessage mEarlyMessage;
  protected boolean mIsSwitch;
  protected boolean mKeyPointDisplayed;
  protected OnCurveTouchListener mOnCurveTouchListener;
  protected int mOutputMax;
  protected int mOutputMin;
  protected boolean mStickDisplayed;
  protected int mStickLastValue;
  
  public CurveView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CurveView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CurveView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CurveView, paramInt, 0);
    mAxis_width = paramAttributeSet.getDimensionPixelSize(0, -1);
    mAxis_height = paramAttributeSet.getDimensionPixelSize(1, -1);
    this.mAxis_hor_lines = paramAttributeSet.getInteger(2, 7);
    this.mAxis_lon_lines = paramAttributeSet.getInteger(3, 9);
    this.mOutputMax = getResources().getInteger(2131492864);
    this.mOutputMin = getResources().getInteger(2131492865);
    this.mCoordinateDisplayed = paramAttributeSet.getBoolean(6, true);
    this.mKeyPointDisplayed = paramAttributeSet.getBoolean(7, true);
    this.mStickDisplayed = paramAttributeSet.getBoolean(8, true);
    this.mIsSwitch = paramAttributeSet.getBoolean(9, false);
    paramContext.getResources().getDimension(2131558420);
    paramContext.getResources().getDimension(2131558421);
    mAxis = new CurveAxis(0, 0, mAxis_width, mAxis_height, this.mAxis_hor_lines, this.mAxis_lon_lines, this.mOutputMax, this.mOutputMin, this.mCoordinateDisplayed, this.mIsSwitch);
  }
  
  public static int[] convertValueXtoPosition(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
  {
    int[] arrayOfInt = new int[paramArrayOfFloat.length];
    for (int i = 0;; i++)
    {
      if (i >= paramArrayOfFloat.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = ((int)(paramInt1 + (paramArrayOfFloat[i] + 100.0F) * paramInt2 / 200.0F));
    }
  }
  
  public static int[] convertValueYtoPosition(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float[] paramArrayOfFloat)
  {
    int[] arrayOfInt = new int[paramArrayOfFloat.length];
    for (int i = 0;; i++)
    {
      if (i >= paramArrayOfFloat.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = ((int)(paramInt1 + paramInt2 - (paramArrayOfFloat[i] - paramFloat2) * paramInt2 / (paramFloat1 - paramFloat2)));
    }
  }
  
  public static int getAxisHeight()
  {
    return mAxis_height;
  }
  
  public static int getAxisWidth()
  {
    return mAxis_width;
  }
  
  public float convertChannelValueToUserValue(float paramFloat)
  {
    return this.mOutputMin + (this.mOutputMax - this.mOutputMin) * paramFloat / 4095.0F;
  }
  
  protected float convertUserValueToChannelValue(float paramFloat)
  {
    return (paramFloat - this.mOutputMin) * 4095.0F / (this.mOutputMax - this.mOutputMin);
  }
  
  protected int countRealPositonX(float paramFloat)
  {
    return (int)(mAxis.getAxisX() + (100.0F + paramFloat) * mAxis_width / 200.0F);
  }
  
  protected int countRealPositonY(float paramFloat)
  {
    return (int)(mAxis.getAxisY() + mAxis_height - (paramFloat - this.mOutputMin) * mAxis_height / (this.mOutputMax - this.mOutputMin));
  }
  
  public void drawStick(int paramInt)
  {
    int i;
    if (this.mStickDisplayed)
    {
      if (paramInt >= 0) {
        break label172;
      }
      i = 0;
    }
    for (;;)
    {
      paramInt = mAxis.getAxisX() + mAxis_width * i / 4095;
      Paint localPaint = new Paint();
      localPaint.setAntiAlias(true);
      localPaint.setStyle(Paint.Style.STROKE);
      localPaint.setColor(getResources().getColor(2131427336));
      localPaint.setStrokeWidth(3.0F);
      Canvas localCanvas = getHolder().lockCanvas();
      if (localCanvas != null)
      {
        Path localPath = new Path();
        localPath.moveTo(paramInt, mAxis.getAxisY());
        localPath.lineTo(paramInt, mAxis.getAxisY() + mAxis_height);
        localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        localCanvas.drawBitmap(this.mCurveBitmap, 0.0F, 0.0F, null);
        localCanvas.drawPath(localPath, localPaint);
        getHolder().unlockCanvasAndPost(localCanvas);
      }
      this.mStickLastValue = i;
      return;
      label172:
      i = paramInt;
      if (paramInt > 4095) {
        i = 4095;
      }
    }
  }
  
  protected int[] getAllPointsPosition()
  {
    int[] arrayOfInt = new int[this.mAllPoint.length];
    for (int i = 0;; i++)
    {
      if (i >= this.mAllPoint.length) {
        return arrayOfInt;
      }
      arrayOfInt[i] = (mAxis.getAxisY() + mAxis_height - this.mAllPoint[i].y);
    }
  }
  
  public CurveAxis getCurveAxis()
  {
    return mAxis;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.mCurveBitmap = Bitmap.createBitmap((int)getResources().getDimension(2131558420), (int)getResources().getDimension(2131558421), Bitmap.Config.ARGB_8888);
    this.mCurveCanvas = new Canvas(this.mCurveBitmap);
  }
  
  protected void onCurveChanges()
  {
    if (this.mStickDisplayed) {
      refreshStick(this.mStickLastValue);
    }
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.mCurveBitmap.recycle();
    this.mCurveCanvas = null;
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    getHolder().addCallback(this);
    getHolder().setFormat(-3);
    setBackgroundDrawable(mAxis);
    this.mAllPoint = new Point[mAxis_width + 1];
    if (this.mAllPointsUpdateListener != null) {
      this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
    }
  }
  
  protected abstract void onRefresh();
  
  public void refresh()
  {
    mAxis = null;
    mAxis = new CurveAxis(0, 0, mAxis_width, mAxis_height, this.mAxis_hor_lines, this.mAxis_lon_lines, this.mOutputMax, this.mOutputMin, this.mCoordinateDisplayed, this.mIsSwitch);
    setBackgroundDrawable(mAxis);
    onRefresh();
  }
  
  public void refreshStick(int paramInt)
  {
    if ((this.mDrawer == null) || (this.mDrawer.mHandler == null))
    {
      Log.w("CurveView", "Drawer Thread Handler is null!!");
      if (this.mEarlyMessage == null) {
        this.mEarlyMessage = new EarlyMessage(paramInt);
      }
    }
    for (;;)
    {
      return;
      Message localMessage = this.mDrawer.mHandler.obtainMessage();
      localMessage.what = 1;
      localMessage.arg1 = paramInt;
      this.mDrawer.mHandler.sendMessage(localMessage);
    }
  }
  
  public void setLongitudinalLines(int paramInt)
  {
    this.mAxis_lon_lines = paramInt;
  }
  
  public void setOnAllPointsUpdateListener(onAllPointsUpdateListener paramonAllPointsUpdateListener)
  {
    this.mAllPointsUpdateListener = paramonAllPointsUpdateListener;
  }
  
  public void setOnCurveTouchListener(OnCurveTouchListener paramOnCurveTouchListener)
  {
    this.mOnCurveTouchListener = paramOnCurveTouchListener;
  }
  
  public void setOutputValueMax(int paramInt)
  {
    this.mOutputMax = paramInt;
    refresh();
  }
  
  public void setOutputValueMin(int paramInt)
  {
    this.mOutputMin = paramInt;
    refresh();
  }
  
  public void sethorizontalLines(int paramInt)
  {
    this.mAxis_hor_lines = paramInt;
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.mAllPointsUpdateListener != null) {
      this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
    }
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    this.mDrawer = new DrawerThread(null);
    this.mDrawer.start();
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    this.mDrawer.stopDrawThread();
    this.mDrawer = null;
  }
  
  private class DrawerThread
    extends Thread
  {
    private Handler mHandler;
    private Looper mLooper;
    
    private DrawerThread() {}
    
    private void stopDrawThread()
    {
      if (this.mLooper == null) {
        Log.i("CurveView", "Looper not running,namely,thread is not running");
      }
      for (;;)
      {
        return;
        this.mLooper.quit();
      }
    }
    
    public void run()
    {
      Looper.prepare();
      this.mLooper = Looper.myLooper();
      this.mHandler = new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          switch (paramAnonymousMessage.what)
          {
          default: 
            Log.i("CurveView", "Unknown Message:" + paramAnonymousMessage.what);
          }
          for (;;)
          {
            return;
            CurveView.this.drawStick(paramAnonymousMessage.arg1);
          }
        }
      };
      if (CurveView.this.mEarlyMessage != null)
      {
        CurveView.this.drawStick(CurveView.this.mEarlyMessage.mValue);
        CurveView.this.mEarlyMessage = null;
      }
      Looper.loop();
    }
  }
  
  private class EarlyMessage
  {
    int mValue;
    
    public EarlyMessage(int paramInt)
    {
      this.mValue = paramInt;
    }
  }
  
  public static abstract interface OnCurveTouchListener
  {
    public abstract void onCurveValueChanged(int paramInt);
    
    public abstract void onCurveValueChanged(int paramInt, float paramFloat);
  }
  
  public static abstract interface onAllPointsUpdateListener
  {
    public abstract void onUpdated(int[] paramArrayOfInt);
  }
}


