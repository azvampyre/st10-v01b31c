package com.yuneec.curve;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class BrokenLineView
  extends CurveView
{
  private static final int POINT_OFFSET = 25;
  private static final String TAG = "BrokenLineView";
  private static int mKeyPointNum;
  private boolean isSurfaceReady = false;
  private float[] mKeyPointRelativeX;
  private float[] mKeyPointRelativeY;
  private int[] mKeyPointX;
  private int[] mKeyPointY;
  private int mMaxY;
  private int mMinY;
  public boolean mSeparate = false;
  private boolean mTouchEnable = false;
  private int mTouchPoint;
  
  public BrokenLineView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public BrokenLineView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public BrokenLineView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public static Point[] countAllPointOfBrokenLine(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    Point[] arrayOfPoint = new Point[paramInt3 + 1];
    int[] arrayOfInt;
    if ((paramArrayOfInt1 != null) && (paramArrayOfInt2 != null))
    {
      int j = paramArrayOfInt1[(paramArrayOfInt1.length - 1)];
      int i = paramArrayOfInt1[0];
      arrayOfInt = new int[j];
      JniTools.getCurvePointFromJNI(paramArrayOfInt1, paramArrayOfInt2, j, i, paramInt2 + paramInt4, paramInt2, arrayOfInt);
    }
    for (paramInt2 = 0;; paramInt2++)
    {
      if (paramInt2 > paramInt3) {
        return arrayOfPoint;
      }
      arrayOfPoint[paramInt2] = new Point();
      arrayOfPoint[paramInt2].x = (paramInt1 + paramInt2);
      arrayOfPoint[paramInt2].y = arrayOfInt[paramInt2];
    }
  }
  
  private void countKeyPointsRealPosition()
  {
    if ((this.mKeyPointRelativeX == null) || (this.mKeyPointRelativeY == null)) {
      Log.w("BrokenLineView", "Axis has not been inited");
    }
    for (;;)
    {
      return;
      if ((this.mKeyPointRelativeX.length != mKeyPointNum) || (this.mKeyPointRelativeY.length != mKeyPointNum)) {
        Log.w("BrokenLineView", "Key points value is wrong");
      } else {
        for (int i = 0; i < mKeyPointNum; i++)
        {
          this.mKeyPointX[i] = countRealPositonX(this.mKeyPointRelativeX[i]);
          this.mKeyPointY[i] = countRealPositonY(this.mKeyPointRelativeY[i]);
        }
      }
    }
  }
  
  private void curveDraw()
  {
    if ((this.mKeyPointX.length == 0) || (this.mKeyPointY.length == 0))
    {
      Log.w("BrokenLineView", "Curve not initialized");
      return;
    }
    int i = this.mKeyPointX[0];
    int j = this.mKeyPointY[0];
    Paint localPaint2 = new Paint();
    localPaint2.setAntiAlias(true);
    localPaint2.setStyle(Paint.Style.STROKE);
    localPaint2.setColor(-65536);
    localPaint2.setStrokeWidth(1.0F);
    Paint localPaint1 = new Paint();
    localPaint1.setAntiAlias(true);
    localPaint1.setStyle(Paint.Style.FILL);
    localPaint1.setShadowLayer(7.0F, 0.0F, 0.0F, -16711936);
    localPaint1.setColor(-16711936);
    localPaint1.setStrokeWidth(1.0F);
    localPaint1.setTextSize(14.0F);
    Path localPath = new Path();
    localPath.moveTo(i, j);
    Canvas localCanvas = getHolder().lockCanvas();
    i = 1;
    label153:
    if (i >= this.mKeyPointX.length)
    {
      localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      this.mCurveCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      if (!this.mKeyPointDisplayed) {}
    }
    for (i = 0;; i++)
    {
      if (i >= this.mKeyPointX.length)
      {
        this.mCurveCanvas.drawPath(localPath, localPaint2);
        localCanvas.drawBitmap(this.mCurveBitmap, 0.0F, 0.0F, null);
        getHolder().unlockCanvasAndPost(localCanvas);
        break;
        localPath.lineTo(this.mKeyPointX[i], this.mKeyPointY[i]);
        i++;
        break label153;
      }
      this.mCurveCanvas.drawCircle(this.mKeyPointX[i], this.mKeyPointY[i], 4.0F, localPaint1);
      this.mCurveCanvas.drawText(String.valueOf(i + 1), this.mKeyPointX[i] + 7.0F, this.mKeyPointY[i] - 7.0F, localPaint1);
    }
  }
  
  public static int[] getAllPointsChValue(Context paramContext, float paramFloat1, float paramFloat2, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    int n = (int)paramContext.getResources().getDimension(2131558422);
    int m = (int)paramContext.getResources().getDimension(2131558423);
    paramContext = convertValueXtoPosition(m, n, paramArrayOfFloat1);
    paramArrayOfFloat1 = convertValueYtoPosition(10, m, paramFloat1, paramFloat2, paramArrayOfFloat2);
    paramArrayOfFloat2 = new int[n + 1];
    int j = 0;
    int i;
    if (((paramContext != null) || (paramArrayOfFloat1 != null)) && (paramArrayOfFloat1.length != 0))
    {
      i = 0;
      if (i >= paramContext.length - 1) {
        paramArrayOfFloat2[n] = ((10 + m - paramArrayOfFloat1[(paramArrayOfFloat1.length - 1)]) * 4095 / m);
      }
    }
    else
    {
      return paramArrayOfFloat2;
    }
    for (int k = paramContext[i];; k++)
    {
      if (k >= paramContext[(i + 1)])
      {
        i++;
        break;
      }
      double d2 = (paramArrayOfFloat1[(i + 1)] - paramArrayOfFloat1[i]) / (paramContext[(i + 1)] - paramContext[i]);
      double d1 = paramArrayOfFloat1[i];
      double d3 = paramContext[i];
      double d4 = k;
      if (j < n) {
        paramArrayOfFloat2[j] = ((int)(10 + m - (d4 * d2 + (d1 - d3 * d2))) * 4095 / m);
      }
      j++;
    }
  }
  
  private void initPosition()
  {
    mKeyPointNum = this.mAxis_lon_lines;
    this.mKeyPointX = new int[mKeyPointNum];
    this.mKeyPointY = new int[mKeyPointNum];
    countKeyPointsRealPosition();
  }
  
  private void redraw()
  {
    if (this.isSurfaceReady)
    {
      curveDraw();
      if (this.mAllPointsUpdateListener != null) {
        this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
      }
    }
  }
  
  public int[] getAllPointsPosition()
  {
    int j = 0;
    int[] arrayOfInt = new int[mAxis_width + 1];
    int i;
    if (((this.mKeyPointX != null) || (this.mKeyPointY != null)) && (this.mKeyPointY.length != 0))
    {
      i = 0;
      if (i < mKeyPointNum - 1) {
        break label84;
      }
      if (j != mAxis_width) {
        break label230;
      }
      arrayOfInt[j] = (mAxis.getAxisY() + mAxis_height - this.mKeyPointY[(mKeyPointNum - 1)]);
    }
    for (;;)
    {
      return arrayOfInt;
      label84:
      float f1 = (this.mKeyPointY[(i + 1)] - this.mKeyPointY[i]) / (this.mKeyPointX[(i + 1)] - this.mKeyPointX[i]);
      float f3 = this.mKeyPointY[i];
      float f2 = this.mKeyPointX[i];
      int k = this.mKeyPointX[i];
      if (k >= this.mKeyPointX[(i + 1)])
      {
        i++;
        break;
      }
      int m = (int)(k * f1 + (f3 - f2 * f1));
      if (j < mAxis_width) {
        arrayOfInt[j] = (mAxis.getAxisY() + mAxis_height - m);
      }
      for (;;)
      {
        j++;
        k++;
        break;
        Log.e("BrokenLineView", "Point index > mAxis_width in loop");
      }
      label230:
      Log.e("BrokenLineView", "Point index > mAxis_width at the last point");
    }
  }
  
  public float getPointValue(int paramInt)
  {
    return this.mOutputMin + (mAxis.getAxisY() + mAxis_height - this.mKeyPointY[paramInt]) * (this.mOutputMax - this.mOutputMin) / mAxis_height;
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    initPosition();
    this.mMaxY = (mAxis.getAxisY() + mAxis_height);
    this.mMinY = mAxis.getAxisY();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onRefresh() {}
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool2 = true;
    Point localPoint = new Point();
    localPoint.x = ((int)paramMotionEvent.getX());
    localPoint.y = ((int)paramMotionEvent.getY());
    boolean bool1;
    if (!isEnabled())
    {
      bool1 = super.onTouchEvent(paramMotionEvent);
      return bool1;
    }
    if (paramMotionEvent.getAction() == 0) {}
    for (int i = 0;; i++)
    {
      if (i >= this.mKeyPointX.length)
      {
        if (paramMotionEvent.getAction() == 1) {
          this.mTouchEnable = false;
        }
        bool1 = bool2;
        if (paramMotionEvent.getAction() != 2) {
          break;
        }
        bool1 = bool2;
        if (!this.mTouchEnable) {
          break;
        }
        i = (int)paramMotionEvent.getY();
        bool1 = bool2;
        if (i > this.mMaxY) {
          break;
        }
        bool1 = bool2;
        if (i < this.mMinY) {
          break;
        }
        this.mKeyPointY[this.mTouchPoint] = i;
        curveDraw();
        if (this.mOnCurveTouchListener != null) {
          this.mOnCurveTouchListener.onCurveValueChanged(this.mTouchPoint);
        }
        bool1 = bool2;
        if (this.mAllPointsUpdateListener == null) {
          break;
        }
        this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
        bool1 = bool2;
        break;
      }
      if ((Math.abs(this.mKeyPointX[i] - localPoint.x) < 25) && (Math.abs(this.mKeyPointY[i] - localPoint.y) < 25))
      {
        this.mTouchEnable = true;
        this.mTouchPoint = i;
      }
    }
  }
  
  public void setParams(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    if (paramArrayOfFloat1.length != paramArrayOfFloat2.length) {
      Log.e("BrokenLineView", "input xy not matched");
    }
    for (;;)
    {
      return;
      if (paramArrayOfFloat1.length < 3)
      {
        Log.e("BrokenLineView", "xy length must larger than 3");
      }
      else
      {
        this.mKeyPointRelativeX = paramArrayOfFloat1;
        this.mKeyPointRelativeY = paramArrayOfFloat2;
        countKeyPointsRealPosition();
        redraw();
      }
    }
  }
  
  public void setPointValue(int paramInt1, int paramInt2)
  {
    if (paramInt1 < this.mKeyPointY.length)
    {
      this.mKeyPointY[paramInt1] = (mAxis.getAxisY() + mAxis_height - (paramInt2 - this.mOutputMin) * mAxis_height / (this.mOutputMax - this.mOutputMin));
      if (this.mAllPointsUpdateListener != null) {
        this.mAllPointsUpdateListener.onUpdated(getAllPointsPosition());
      }
      curveDraw();
    }
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    super.surfaceChanged(paramSurfaceHolder, paramInt1, paramInt2, paramInt3);
    curveDraw();
    this.isSurfaceReady = true;
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceCreated(paramSurfaceHolder);
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    super.surfaceDestroyed(paramSurfaceHolder);
    this.isSurfaceReady = false;
    this.mKeyPointRelativeX = null;
    this.mKeyPointRelativeY = null;
  }
}


