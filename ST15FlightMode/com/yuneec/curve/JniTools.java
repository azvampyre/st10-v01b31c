package com.yuneec.curve;

public class JniTools
{
  private static int mObject = 0;
  
  static
  {
    System.loadLibrary("CurveFunction");
  }
  
  public static native void destoryFromJNI();
  
  public static native boolean getCurvePointFromJNI(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt3);
  
  public static native int getValueYFromJNI(int paramInt);
}


